# Reign of Gilgor

Reign of Gilgor (RoG) is a 2D MMORPG game inspired by [Realm of the Mad God](https://www.realmofthemadgod.com)

In it's current stage, RoG development is focused on game mechanics and optimization.
Content is added slowly, but will be ramped up when the mechanics are polished.
The codebase is optimized continuously, and the RoG client is usable on nearly any modern computer.

## RoG Core

RoG Core is a [.NET Standard 2.0](https://learn.microsoft.com/en-us/dotnet/standard/net-standard?tabs=net-standard-1-0)+[C# 13](https://docs.microsoft.com/en-us/dotnet/csharp/whats-new/csharp-13) Class Library that is used both in the RoG Server and the RoG Client for shared code and game utilities.

## Client

The RoG Client is made using [Unity](https://unity.com), and compiled with Unity's [IL2CPP](https://docs.unity3d.com/Manual/IL2CPP.html).
The RoG Client is written in [C# 8](https://docs.microsoft.com/en-us/dotnet/csharp/whats-new/csharp-8), but compiled to C++.

## Server

The RoG Server is written in [C# 13](https://docs.microsoft.com/en-us/dotnet/csharp/whats-new/csharp-13) and uses the [.NET 9 Runtime](https://dotnet.microsoft.com/download/dotnet/9.0). It is composed of three parts.

### Database

The Database project connects to a [Redis](https://redis.io) database and handles all database operations for the AppEngine and the GameServer. Redis provides a Pub/Sub network, which allows the AppEngine and the GameServer to communicate with each other.

### API

The API is responsible for handling all HTTP requests the client (or external services) needs to function.

### GameServer

The GameServer handles the ingame logic and client connections for RoG. It can be configured to be multithreaded, and can use of [SIMD](https://en.wikipedia.org/wiki/SIMD) Intrinsics to handle the large amount of data it processes.

## Roadmap
To see the status and planned features of RoG, check out the [Trello](https://trello.com/b/2yvHd8Ga/reign-of-gilgor)!
