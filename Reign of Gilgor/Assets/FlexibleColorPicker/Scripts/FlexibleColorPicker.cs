using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class FlexibleColorPicker : MonoBehaviour
{

    /*----------------------------------------------------------
    * ----------------------- PARAMETERS -----------------------
    * ----------------------------------------------------------
    */

    //Unity connections
    [Tooltip("Connections to the FCP's picker images, this should not be adjusted unless in advanced use cases.")]
    public Picker[] pickers;
    [Serializable]
    public struct Picker
    {
        public Image image;
        public Sprite dynamicSprite;
        public Sprite staticSpriteHor;
        public Sprite staticSpriteVer;
        public Material dynamicMaterial;
    }
    private enum PickerType
    {
        Main, R, G, B, H, S, V, A, Preview, PreviewAlpha
    }

    [Tooltip("Connection to the FCP's hexadecimal input field.")]
    public InputField hexInput;

    [Tooltip("Connection to the FCP's mode dropdown menu.")]
    public Dropdown modeDropdown;
    private Canvas canvas;

    [Tooltip("The (starting) 2D picking mode, i.e. the 2 color values that can be picked with the large square picker.")]
    public MainPickingMode mode;

    [Tooltip("Sprites to be used in static mode on the main picker, one for each 2D mode.")]
    public Sprite[] staticSpriteMain;
    public enum MainPickingMode
    {
        HS, HV, SH, SV, VH, VS
    }

    //private state
    private BufferedColor bufferedColor;
    private Picker focusedPicker;
    private PickerType focusedPickerType;
    private MainPickingMode lastUpdatedMode;
    private bool typeUpdate;
    private bool triggeredStaticMode;
    private bool materialsSeperated;

    //public settings
    [Tooltip("Color set to the color picker on Start(). Before the start function, the standard public color variable is redirected to this value, so it may be changed at run time.")]
    public Color startingColor = Color.white;
    [Tooltip("Use static mode: picker images are replaced by static images in stead of adaptive Unity shaders.")]
    public bool staticMode = false;
    [Tooltip("Make sure FCP seperates its picker materials so that the dynamic mode works consistently, even when multiple FPCs are active at the same time. Turning this off yields a slight performance boost.")]
    public bool multiInstance = true;

    //constants
    private const float HUE_LOOP = 5.9999f;
    private const string SHADER_MODE = "_Mode";
    private const string SHADER_C1 = "_Color1";
    private const string SHADER_C2 = "_Color2";
    private const string SHADER_DOUBLE_MODE = "_DoubleMode";
    private const string SHADER_HSV = "_HSV";
    private const string SHADER_HSV_MIN = "_HSV_MIN";
    private const string SHADER_HSV_MAX = "_HSV_MAX";

    //advanced settings
    [Tooltip("More specific settings for color picker. Changes are not applied immediately, but require an FCP update to trigger.")]
    public AdvancedSettings advancedSettings;
    [Serializable]
    public class AdvancedSettings
    {
        public PSettings r;
        public PSettings g;
        public PSettings b;
        public PSettings h;
        public PSettings s;
        public PSettings v;
        public PSettings a;

        [Serializable]
        public class PSettings
        {
            [Tooltip("Normalized minimum for this color value, for restricting the slider range")]
            [Range(0f, 1f)]
            public float min = 0f;
            [Tooltip("Normalized maximum for this color value, for restricting the slider range")]
            [Range(0f, 1f)]
            public float max = 1f;
            [Tooltip("Make the picker associated with this value act static, even in a dynamic color picker setup")]
            public bool overrideStatic = false;
        }

        /// <summary>
        /// Get PSettings for value associated with the given picker index.
        /// Returns default PSettings if none exists.
        /// </summary>
        public PSettings Get(int i)
        {
            if (i <= 0 | i > 7)
            {
                return new PSettings();
            }

            var p = new PSettings[] { r, g, b, h, s, v, a };
            return p[i - 1];
        }
    }
    private AdvancedSettings AS => advancedSettings;

    /*----------------------------------------------------------
    * ------------------- MAIN COLOR GET/SET -------------------
    * ----------------------------------------------------------
    */

    public Color color
    {
        /* if called before init in Start(), the color state
         * is equivalent to the starting color parameter.
         */
        get => bufferedColor == null ? startingColor : bufferedColor.color;
        set
        {
            if (bufferedColor == null)
            {
                startingColor = value;
                return;
            }

            bufferedColor.Set(value);
            UpdateMarkers();
            UpdateTextures();
            UpdateHex();
            typeUpdate = true;
        }
    }

    /// <summary>
    /// Equivalent to fcp.color
    /// Returns starting color if FCP is not initialized.
    /// </summary>
    public Color GetColor() => color;

    /// <summary>
    /// Equivalent to fcp.color
    /// Propogates color change to picker images, hex input and other components.
    /// Modifies starting color if FCP is not initialized.
    /// </summary>
    public void SetColor(Color color) => this.color = color;

    /// <summary>
    /// Returns current fcp color, but with its alpha channel value set to max.
    /// </summary>
    public Color GetColorFullAlpha()
    {
        var toReturn = color;
        toReturn.a = 1f;
        return toReturn;
    }

    /// <summary>
    /// Changes fcp color while maintaining its current alpha value.
    /// </summary>
    public void SetColorNoAlpha(Color color)
    {
        color.a = this.color.a;
        this.color = color;
    }

    /*----------------------------------------------------------
    * ------------------------- UPKEEP -------------------------
    * ----------------------------------------------------------
    */

    private void Start()
    {
        bufferedColor = new BufferedColor(startingColor);
        canvas = GetComponentInParent<Canvas>();
    }

    private void OnEnable()
    {
        if (bufferedColor == null)
        {
            bufferedColor = new BufferedColor(startingColor);
        }

        if (multiInstance && !materialsSeperated)
        {
            SeperateMaterials();
            materialsSeperated = true;
        }

        triggeredStaticMode = staticMode;
        UpdateTextures();
        MakeModeOptions();
        UpdateMarkers();
    }

    private void Update()
    {
        typeUpdate = false;
        if (lastUpdatedMode != mode)
        {
            ChangeMode(mode);
        }

        if (staticMode != triggeredStaticMode)
        {
            UpdateTextures();
            triggeredStaticMode = staticMode;
        }

        if (multiInstance && !materialsSeperated)
        {
            SeperateMaterials();
            materialsSeperated = true;
        }
    }

    /// <summary>
    /// Change picker that is being focused (and edited) using the pointer.
    /// </summary>
    /// <param name="i">Index of the picker image.</param>
    public void SetPointerFocus(int i)
    {
        if (i < 0 || i >= pickers.Length)
        {
            Debug.LogWarning("No picker image available of type " + (PickerType)i +
                ". Did you assign all the picker images in the editor?");
        }
        else
        {
            focusedPicker = pickers[i];
        }

        focusedPickerType = (PickerType)i;
    }

    /// <summary>
    /// Update color based on the pointer position in the currently focused picker.
    /// </summary>
    /// <param name="e">Pointer event</param>
    public void PointerUpdate(BaseEventData e)
    {
        var v = GetNormalizedPointerPosition(canvas, focusedPicker.image.rectTransform, e);
        bufferedColor = PickColor(bufferedColor, focusedPickerType, v);

        UpdateMarkers();
        UpdateTextures();

        typeUpdate = true;
        UpdateHex();
    }

    /// <summary>
    /// Softly sanitize hex color input and apply it
    /// </summary>
    public void TypeHex(string input)
    {
        TypeHex(input, false);

        UpdateTextures();
        UpdateMarkers();
    }

    /// <summary>
    /// Strongly sanitize hex color input and apply it.
    /// (appends zeroes to fit proper length in the text box).
    /// </summary>
    public void FinishTypeHex(string input)
    {
        TypeHex(input, true);

        UpdateTextures();
        UpdateMarkers();
    }

    /// <summary>
    /// Change mode of the main, 2D picking image
    /// </summary>
    public void ChangeMode(int newMode) => ChangeMode((MainPickingMode)newMode);

    /// <summary>
    /// Change mode of the main, 2D picking image
    /// </summary>
    public void ChangeMode(MainPickingMode mode)
    {
        this.mode = mode;

        triggeredStaticMode = false;
        UpdateTextures();
        UpdateMarkers();
        UpdateMode(mode);
    }

    private void SeperateMaterials()
    {
        for (var i = 0; i < pickers.Length; i++)
        {
            var p = pickers[i];
            if (IsPickerAvailable(i) & p.dynamicMaterial is not null)
            {
                var original = p.dynamicMaterial;
                var seperate = new Material(original);
                p.dynamicMaterial = seperate;
                pickers[i] = p;
                if (!staticMode)
                {
                    p.image.material = seperate;
                }
            }
        }
    }

    /*----------------------------------------------------------
    * --------------------- COLOR PICKING ----------------------
    * ----------------------------------------------------------
    * 
    * Get a new color that is the currently selected color but with 
    * one or two values changed. This is the core functionality of 
    * the picking images and the entire color picker script.
    */

    /// <summary>
    /// Get a color that is the current color, but changed by the given picker and value.
    /// </summary>
    /// <param name="type">Picker type to base change on</param>
    /// <param name="v">normalized x and y values (both values may not be used)</param>
    private BufferedColor PickColor(BufferedColor color, PickerType type, Vector2 v) => type switch
    {
        PickerType.Main => PickColorMain(color, v),
        PickerType.Preview or PickerType.PreviewAlpha => color,
        _ => PickColor1D(color, type, v),
    };

    private BufferedColor PickColorMain(BufferedColor color, Vector2 v) => PickColorMain(color, mode, v);

    private BufferedColor PickColor1D(BufferedColor color, PickerType type, Vector2 v)
    {
        var horizontal = IsHorizontal(pickers[(int)type]);
        var value = horizontal ? v.x : v.y;
        return PickColor1D(color, type, value);
    }

    private BufferedColor PickColorMain(BufferedColor color, MainPickingMode mode, Vector2 v) => mode switch
    {
        MainPickingMode.HS => PickColor2D(color, PickerType.H, v.x, PickerType.S, v.y),
        MainPickingMode.HV => PickColor2D(color, PickerType.H, v.x, PickerType.V, v.y),
        MainPickingMode.SH => PickColor2D(color, PickerType.S, v.x, PickerType.H, v.y),
        MainPickingMode.SV => PickColor2D(color, PickerType.S, v.x, PickerType.V, v.y),
        MainPickingMode.VH => PickColor2D(color, PickerType.V, v.x, PickerType.H, v.y),
        MainPickingMode.VS => PickColor2D(color, PickerType.V, v.x, PickerType.S, v.y),
        _ => bufferedColor,
    };

    private BufferedColor PickColor2D(BufferedColor color, PickerType type1, float value1, PickerType type2, float value2)
    {
        color = PickColor1D(color, type1, value1);
        color = PickColor1D(color, type2, value2);
        return color;
    }

    private BufferedColor PickColor1D(BufferedColor color, PickerType type, float value) => type switch
    {
        PickerType.R => color.PickR(Mathf.Lerp(AS.r.min, AS.r.max, value)),
        PickerType.G => color.PickG(Mathf.Lerp(AS.g.min, AS.g.max, value)),
        PickerType.B => color.PickB(Mathf.Lerp(AS.b.min, AS.b.max, value)),
        PickerType.H => color.PickH(Mathf.Lerp(AS.h.min, AS.h.max, value) * HUE_LOOP),
        PickerType.S => color.PickS(Mathf.Lerp(AS.s.min, AS.s.max, value)),
        PickerType.V => color.PickV(Mathf.Lerp(AS.v.min, AS.v.max, value)),
        PickerType.A => color.PickA(Mathf.Lerp(AS.a.min, AS.a.max, value)),
        _ => throw new Exception("Picker type " + type + " is not associated with a single color value."),
    };

    /*----------------------------------------------------------
    * -------------------- MARKER UPDATING ---------------------
    * ----------------------------------------------------------
    * 
    * Update positions of markers on each picking texture, 
    * indicating the currently selected values.
    */

    private void UpdateMarkers()
    {
        for (var i = 0; i < pickers.Length; i++)
        {
            if (IsPickerAvailable(i))
            {
                var type = (PickerType)i;
                var v = GetValue(type);
                UpdateMarker(pickers[i], type, v);
            }
        }
    }

    private void UpdateMarker(Picker picker, PickerType type, Vector2 v)
    {
        switch (type)
        {
            case PickerType.Main:
                SetMarker(picker.image, v, true, true);
                break;

            case PickerType.Preview:
            case PickerType.PreviewAlpha:
                break;

            default:
                var horizontal = IsHorizontal(picker);
                SetMarker(picker.image, v, horizontal, !horizontal);
                break;
        }
    }

    private void SetMarker(Image picker, Vector2 v, bool setX, bool setY)
    {
        RectTransform marker = null;
        RectTransform offMarker = null;
        if (setX && setY)
        {
            marker = GetMarker(picker, null);
        }
        else if (setX)
        {
            marker = GetMarker(picker, "hor");
            offMarker = GetMarker(picker, "ver");
        }
        else if (setY)
        {
            marker = GetMarker(picker, "ver");
            offMarker = GetMarker(picker, "hor");
        }

        if (offMarker is not null)
        {
            offMarker.gameObject.SetActive(false);
        }

        if (marker == null)
        {
            return;
        }

        marker.gameObject.SetActive(true);
        var parent = picker.rectTransform;
        var parentSize = parent.rect.size;
        Vector2 localPos = marker.localPosition;

        if (setX)
        {
            localPos.x = (v.x - parent.pivot.x) * parentSize.x;
        }

        if (setY)
        {
            localPos.y = (v.y - parent.pivot.y) * parentSize.y;
        }

        marker.localPosition = localPos;
    }

    private RectTransform GetMarker(Image picker, string search)
    {
        for (var i = 0; i < picker.transform.childCount; i++)
        {
            var candidate = picker.transform.GetChild(i).GetComponent<RectTransform>();
            var candidateName = candidate.name.ToLower();
            var match = candidateName.Contains("marker");
            match &= string.IsNullOrEmpty(search)
                || candidateName.Contains(search);
            if (match)
            {
                return candidate;
            }
        }

        return null;
    }

    /*----------------------------------------------------------
    * -------------------- VALUE RETRIEVAL ---------------------
    * ----------------------------------------------------------
    * 
    * Get individual values associated with a picker image from the 
    * currently selected color.
    * This is needed to properly update markers.
    */

    private Vector2 GetValue(PickerType type)
    {
        switch (type)
        {

            case PickerType.Main: return GetValue(mode);

            case PickerType.Preview:
            case PickerType.PreviewAlpha:
                return Vector2.zero;

            default:
                var value = GetValue1D(type);
                return new Vector2(value, value);

        }
    }

    /// <summary>
    /// Get normalized value of the current color according to the given picker.
    /// This value can be used to adjust the position of the marker on a slider.
    /// </summary>
    private float GetValue1D(PickerType type) => type switch
    {
        PickerType.R => Mathf.InverseLerp(AS.r.min, AS.r.max, bufferedColor.r),
        PickerType.G => Mathf.InverseLerp(AS.g.min, AS.g.max, bufferedColor.g),
        PickerType.B => Mathf.InverseLerp(AS.b.min, AS.b.max, bufferedColor.b),
        PickerType.H => Mathf.InverseLerp(AS.h.min, AS.h.max, bufferedColor.h / HUE_LOOP),
        PickerType.S => Mathf.InverseLerp(AS.s.min, AS.s.max, bufferedColor.s),
        PickerType.V => Mathf.InverseLerp(AS.v.min, AS.v.max, bufferedColor.v),
        PickerType.A => Mathf.InverseLerp(AS.a.min, AS.a.max, bufferedColor.a),
        _ => throw new Exception("Picker type " + type + " is not associated with a single color value."),
    };

    /// <summary>
    /// Get normalized 2D value of the current color according to the main picker mode.
    /// This value can be used to adjust the position of the 2D marker.
    /// </summary>
    private Vector2 GetValue(MainPickingMode mode) => mode switch
    {
        MainPickingMode.HS => new Vector2(GetValue1D(PickerType.H), GetValue1D(PickerType.S)),
        MainPickingMode.HV => new Vector2(GetValue1D(PickerType.H), GetValue1D(PickerType.V)),
        MainPickingMode.SH => new Vector2(GetValue1D(PickerType.S), GetValue1D(PickerType.H)),
        MainPickingMode.SV => new Vector2(GetValue1D(PickerType.S), GetValue1D(PickerType.V)),
        MainPickingMode.VH => new Vector2(GetValue1D(PickerType.V), GetValue1D(PickerType.H)),
        MainPickingMode.VS => new Vector2(GetValue1D(PickerType.V), GetValue1D(PickerType.S)),
        _ => throw new Exception("Unkown main picking mode: " + mode),
    };

    /*----------------------------------------------------------
    * -------------------- TEXTURE UPDATING --------------------
    * ----------------------------------------------------------
    * 
    * Update picker image textures that show gradients of colors 
    * that the user can pick.
    */

    private void UpdateTextures()
    {
        foreach (PickerType type in Enum.GetValues(typeof(PickerType)))
        {
            if (staticMode || AS.Get((int)type).overrideStatic)
            {
                UpdateStatic(type);
            }
            else
            {
                UpdateDynamic(type);
            }
        }
    }

    private void UpdateStatic(PickerType type)
    {
        if (!IsPickerAvailable(type))
        {
            return;
        }

        var p = pickers[(int)type];

        var hor = IsHorizontal(p);
        var s = hor ? p.staticSpriteHor : p.staticSpriteVer;
        if (s == null)
        {
            s = hor ? p.staticSpriteVer : p.staticSpriteHor;
        }

        p.image.sprite = s;
        p.image.material = null;
        p.image.color = Color.white;

        var prvw = color;

        switch (type)
        {
            case PickerType.Main:
                p.image.sprite = staticSpriteMain[(int)mode];
                break;

            case PickerType.Preview:
                prvw.a = 1f;
                p.image.color = prvw;
                break;

            case PickerType.PreviewAlpha:
                p.image.color = prvw;
                break;
        }
    }

    private void UpdateDynamic(PickerType type)
    {
        if (!IsPickerAvailable(type))
        {
            return;
        }

        var p = pickers[(int)type];
        if (p.dynamicMaterial == null)
        {
            return;
        }

        var m = p.dynamicMaterial;
        p.image.material = m;
        p.image.color = Color.white;
        p.image.sprite = p.dynamicSprite;

        var bc = bufferedColor;

        var alpha = IsAlphaType(type);
        m.SetInt(SHADER_MODE, GetGradientMode(type));
        var c1 = PickColor(bc, type, Vector2.zero).color;
        var c2 = PickColor(bc, type, Vector2.one).color;
        if (!alpha)
        {
            c1 = new Color(c1.r, c1.g, c1.b);
            c2 = new Color(c2.r, c2.g, c2.b);
        }

        m.SetColor(SHADER_C1, c1);
        m.SetColor(SHADER_C2, c2);
        if (type == PickerType.Main)
        {
            m.SetInt(SHADER_DOUBLE_MODE, (int)mode);
        }

        m.SetVector(SHADER_HSV, new Vector4(bc.h / HUE_LOOP, bc.s, bc.v, alpha ? bc.a : 1f));
        m.SetVector(SHADER_HSV_MIN, new Vector4(AS.h.min, AS.s.min, AS.v.min));
        m.SetVector(SHADER_HSV_MAX, new Vector4(AS.h.max, AS.s.max, AS.v.max));
    }

    private int GetGradientMode(PickerType type)
    {
        var o = IsHorizontal(pickers[(int)type]) ? 0 : 1;
        return type switch
        {
            PickerType.Main => 2,
            PickerType.H => 3 + o,
            _ => o,
        };
    }

    private bool IsPickerAvailable(PickerType type) => IsPickerAvailable((int)type);

    private bool IsPickerAvailable(int index)
    {
        if (index < 0 || index >= pickers.Length)
        {
            return false;
        }

        var p = pickers[index];
        return p.image != null && p.image.gameObject.activeInHierarchy;
    }

    /*----------------------------------------------------------
    * ------------------ HEX INPUT UPDATING --------------------
    * ----------------------------------------------------------
    * 
    * Provides an input field for hexadecimal color values.
    * The user can type new values, or use this field to copy 
    * values picked via the picker images.
    */

    private void UpdateHex()
    {
        if (hexInput == null || !hexInput.gameObject.activeInHierarchy)
        {
            return;
        }

        hexInput.text = "#" + ColorUtility.ToHtmlStringRGB(color);
    }

    private void TypeHex(string input, bool finish)
    {
        if (typeUpdate)
        {
            return;
        }
        else
        {
            typeUpdate = true;
        }

        var newText = GetSanitizedHex(input, finish);
        var parseText = GetSanitizedHex(input, true);

        var cp = hexInput.caretPosition;
        hexInput.text = newText;
        if (hexInput.caretPosition == 0)
        {
            hexInput.caretPosition = 1;
        }
        else if (newText.Length == 2)
        {
            hexInput.caretPosition = 2;
        }
        else if (input.Length > newText.Length && cp < input.Length)
        {
            hexInput.caretPosition = cp - input.Length + newText.Length;
        }

        ColorUtility.TryParseHtmlString(parseText, out var newColor);
        bufferedColor.Set(newColor);
        UpdateMarkers();
        UpdateTextures();
    }

    /*----------------------------------------------------------
    * ---------------------- MODE UPDATING ---------------------
    * ----------------------------------------------------------
    * 
    * Allows user to change the 'Main picking mode' which determines 
    * the values shown on the main, 2D picking image.
    */

    private void MakeModeOptions()
    {
        if (modeDropdown == null || !modeDropdown.gameObject.activeInHierarchy)
        {
            return;
        }

        modeDropdown.ClearOptions();
        var options = new List<string>();
        foreach (MainPickingMode mode in Enum.GetValues(typeof(MainPickingMode)))
        {
            options.Add(mode.ToString());
        }

        modeDropdown.AddOptions(options);

        UpdateMode(mode);
    }

    private void UpdateMode(MainPickingMode mode)
    {
        lastUpdatedMode = mode;
        if (modeDropdown == null || !modeDropdown.gameObject.activeInHierarchy)
        {
            return;
        }

        modeDropdown.value = (int)mode;
    }

    /*----------------------------------------------------------
    * ---------------- STATIC HELPER FUNCTIONS -----------------
    * ----------------------------------------------------------
    */

    private static bool IsPreviewType(PickerType type) => type switch
    {
        PickerType.Preview => true,
        PickerType.PreviewAlpha => true,
        _ => false,
    };

    private static bool IsAlphaType(PickerType type) => type switch
    {
        PickerType.A => true,
        PickerType.PreviewAlpha => true,
        _ => false,
    };

    /// <summary>
    /// Should given picker image be controlled horizontally?
    /// Returns true if the image is bigger in the horizontal direction.
    /// </summary>
    private static bool IsHorizontal(Picker p)
    {
        var size = p.image.rectTransform.rect.size;
        return size.x >= size.y;
    }

    /// <summary>
    /// Santiive a given string so that it encodes a valid hex color string
    /// </summary>
    /// <param name="input">Input string</param>
    /// <param name="full">Insert zeroes to match #RRGGBB format </param>
    public static string GetSanitizedHex(string input, bool full)
    {
        if (string.IsNullOrEmpty(input))
        {
            return "#";
        }

        var toReturn = new List<char>
        {
            '#'
        };
        var i = 0;
        var chars = input.ToCharArray();
        while (toReturn.Count < 7 && i < input.Length)
        {
            var nextChar = char.ToUpper(chars[i++]);
            if (IsValidHexChar(nextChar))
            {
                toReturn.Add(nextChar);
            }
        }

        while (full && toReturn.Count < 7)
        {
            toReturn.Insert(1, '0');
        }

        return new string(toReturn.ToArray());
    }

    private static bool IsValidHexChar(char c)
    {
        var valid = char.IsNumber(c);
        valid |= c >= 'A' & c <= 'F';
        return valid;
    }

    /// <summary>
    /// tries to parse given string input as hexadecimal color e.g.
    /// "#FF00FF" or "223344" returns black if string failed to 
    /// parse.
    /// </summary>
    public static Color ParseHex(string input) => ParseHex(input, Color.black);

    /// <summary>
    /// tries to parse given string input as hexadecimal color e.g.
    /// "#FF00FF" or "223344" returns default color if string failed to 
    /// parse.
    /// </summary>
    public static Color ParseHex(string input, Color defaultColor)
    {
        var parseText = GetSanitizedHex(input, true);
        return ColorUtility.TryParseHtmlString(parseText, out var toReturn) ? toReturn : defaultColor;
    }

    /// <summary>
    /// Get normalized position of the given pointer event relative to the given rect.
    /// (e.g. return [0,1] for top left corner). This method correctly takes into 
    /// account relative positions, canvas render mode and general transformations, 
    /// including rotations and scale.
    /// </summary>
    /// <param name="canvas">parent canvas of the rect (and therefore the FCP)</param>
    /// <param name="rect">Rect to find relative position to</param>
    /// <param name="e">Pointer event for which to find the relative position</param>
    private static Vector2 GetNormalizedPointerPosition(Canvas canvas, RectTransform rect, BaseEventData e)
    {
        switch (canvas.renderMode)
        {

            case RenderMode.ScreenSpaceCamera:
                return canvas.worldCamera == null ? GetNormScreenSpace(rect, e) : GetNormWorldSpace(canvas, rect, e);

            case RenderMode.ScreenSpaceOverlay:
                return GetNormScreenSpace(rect, e);

            case RenderMode.WorldSpace:
                if (canvas.worldCamera == null)
                {
                    Debug.LogError("FCP in world space render mode requires an event camera to be set up on the parent canvas!");
                    return Vector2.zero;
                }

                return GetNormWorldSpace(canvas, rect, e);

            default: return Vector2.zero;

        }
    }

    /// <summary>
    /// Get normalized position in the case of a screen space (overlay) 
    /// type canvas render mode
    /// </summary>
    private static Vector2 GetNormScreenSpace(RectTransform rect, BaseEventData e)
    {
        var screenPoint = ((PointerEventData)e).position;
        Vector2 localPos = rect.worldToLocalMatrix.MultiplyPoint(screenPoint);
        var x = Mathf.Clamp01((localPos.x / rect.rect.size.x) + rect.pivot.x);
        var y = Mathf.Clamp01((localPos.y / rect.rect.size.y) + rect.pivot.y);
        return new Vector2(x, y);
    }

    /// <summary>
    /// Get normalized position in the case of a world space (or screen space camera) 
    /// type cavnvas render mode.
    /// </summary>
    private static Vector2 GetNormWorldSpace(Canvas canvas, RectTransform rect, BaseEventData e)
    {
        var screenPoint = ((PointerEventData)e).position;
        var pointerRay = canvas.worldCamera.ScreenPointToRay(screenPoint);
        var canvasPlane = new Plane(canvas.transform.forward, canvas.transform.position);
        canvasPlane.Raycast(pointerRay, out var enter);
        var worldPoint = pointerRay.origin + (enter * pointerRay.direction);
        Vector2 localPoint = rect.worldToLocalMatrix.MultiplyPoint(worldPoint);

        var x = Mathf.Clamp01((localPoint.x / rect.rect.size.x) + rect.pivot.x);
        var y = Mathf.Clamp01((localPoint.y / rect.rect.size.y) + rect.pivot.y);
        return new Vector2(x, y);
    }

    /// <summary>
    /// Get color from hue, saturation, value format
    /// </summary>
    /// <param name="hsv">Vector containing h, s and v values.</param>
    public static Color HSVToRGB(Vector3 hsv) => HSVToRGB(hsv.x, hsv.y, hsv.z);

    /// <summary>
    /// Get color from hue, saturation, value format
    /// </summary>
    /// <param name="h">hue value, ranging from 0 to 6; red to red</param>
    /// <param name="s">saturation, 0 to 1; gray to colored</param>
    /// <param name="v">value, 0 to 1; black to white</param>
    public static Color HSVToRGB(float h, float s, float v)
    {
        var c = s * v;
        var m = v - c;
        var x = (c * (1f - Mathf.Abs((h % 2f) - 1f))) + m;
        c += m;

        var range = Mathf.FloorToInt(h % 6f);

        return range switch
        {
            0 => new Color(c, x, m),
            1 => new Color(x, c, m),
            2 => new Color(m, c, x),
            3 => new Color(m, x, c),
            4 => new Color(x, m, c),
            5 => new Color(c, m, x),
            _ => Color.black,
        };
    }

    /// <summary>
    /// Get hue, saturation and value of a color.
    /// Complementary to HSVToRGB
    /// </summary>
    public static Vector3 RGBToHSV(Color color)
    {
        var r = color.r;
        var g = color.g;
        var b = color.b;
        return RGBToHSV(r, g, b);
    }

    /// <summary>
    /// Get hue, saturation and value of a color.
    /// Complementary to HSVToRGB
    /// </summary>
    public static Vector3 RGBToHSV(float r, float g, float b)
    {
        var cMax = Mathf.Max(r, g, b);
        var cMin = Mathf.Min(r, g, b);
        var delta = cMax - cMin;
        var h = 0f;
        if (delta > 0f)
        {
            if (r >= b && r >= g)
            {
                h = Mathf.Repeat((g - b) / delta, 6f);
            }
            else if (g >= r && g >= b)
            {
                h = ((b - r) / delta) + 2f;
            }
            else if (b >= r && b >= g)
            {
                h = ((r - g) / delta) + 4f;
            }
        }

        var s = cMax == 0f ? 0f : delta / cMax;
        var v = cMax;
        return new Vector3(h, s, v);
    }

    /*----------------------------------------------------------
    * --------------------- HELPER CLASSES ---------------------
    * ----------------------------------------------------------
    */

    /// <summary>
    /// Encodes a color while buffering hue and saturation values.
    /// This is necessary since these values are singular for some 
    /// colors like unsaturated grays and would lead to undesirable 
    /// behaviour when moving sliders towards such colors.
    /// </summary>
    [Serializable]
    private class BufferedColor
    {
        public Color color;
        private float bufferedHue;
        private float bufferedSaturation;

        public float r => color.r;
        public float g => color.g;
        public float b => color.b;
        public float a => color.a;
        public float h => bufferedHue;
        public float s => bufferedSaturation;
        public float v => RGBToHSV(color).z;

        public BufferedColor()
        {
            bufferedHue = 0f;
            bufferedSaturation = 0f;
            color = Color.black;
        }

        public BufferedColor(Color color) : this()
        {
            Set(color);
        }

        public BufferedColor(Color color, float hue, float sat) : this(color)
        {
            bufferedHue = hue;
            bufferedSaturation = sat;
        }

        public BufferedColor(Color color, BufferedColor source)
            : this(color, source.bufferedHue, source.bufferedSaturation)
        {
            Set(color);
        }

        public void Set(Color color) => Set(color, bufferedHue, bufferedSaturation);

        public void Set(Color color, float bufferedHue, float bufferedSaturation)
        {
            this.color = color;
            var hsv = RGBToHSV(color);

            var hueSingularity = hsv.y == 0f || hsv.z == 0f;
            this.bufferedHue = hueSingularity ? bufferedHue : hsv.x;

            var saturationSingularity = hsv.z == 0f;
            this.bufferedSaturation = saturationSingularity ? bufferedSaturation : hsv.y;
        }

        public BufferedColor PickR(float value)
        {
            var toReturn = color;
            toReturn.r = value;
            return new BufferedColor(toReturn, this);
        }

        public BufferedColor PickG(float value)
        {
            var toReturn = color;
            toReturn.g = value;
            return new BufferedColor(toReturn, this);
        }

        public BufferedColor PickB(float value)
        {
            var toReturn = color;
            toReturn.b = value;
            return new BufferedColor(toReturn, this);
        }

        public BufferedColor PickA(float value)
        {
            var toReturn = color;
            toReturn.a = value;
            return new BufferedColor(toReturn, this);
        }

        public BufferedColor PickH(float value)
        {
            var hsv = RGBToHSV(color);
            var toReturn = HSVToRGB(value, hsv.y, hsv.z);
            toReturn.a = color.a;
            return new BufferedColor(toReturn, value, bufferedSaturation);
        }

        public BufferedColor PickS(float value)
        {
            var hsv = RGBToHSV(color);
            var toReturn = HSVToRGB(bufferedHue, value, hsv.z);
            toReturn.a = color.a;
            return new BufferedColor(toReturn, bufferedHue, value);
        }

        public BufferedColor PickV(float value)
        {
            var toReturn = HSVToRGB(bufferedHue, bufferedSaturation, value);
            toReturn.a = color.a;
            return new BufferedColor(toReturn, bufferedHue, bufferedSaturation);
        }
    }
}
