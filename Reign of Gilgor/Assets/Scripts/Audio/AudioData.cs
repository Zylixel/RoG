using System.Collections.Generic;
using Newtonsoft.Json;
using RoGCore.Assets;
using UnityEngine;

namespace Client.Audio
{
    internal static class AudioData
    {
        private static readonly Dictionary<string, AudioDescription> AudioDictionary = new Dictionary<string, AudioDescription>();

        private static readonly AudioDescription DefaultInfo = new AudioDescription()
        {
            ClipName = string.Empty,
            Type = RoGCore.Assets.AudioType.Sound,
            VolumeMulitplier = 1f,
            Pitch = 1f,
            PitchVariation = 0f,
        };

        static AudioData()
        {
            var jsonString = Resources.Load<TextAsset>("Sounds/DefaultAudioDescription").text;

            foreach (var sound in JsonConvert.DeserializeObject<AudioDescription[]>(jsonString))
            {
                if (sound.ClipName is null)
                {
                    continue;
                }

                AudioDictionary.Add(sound.ClipName, sound);
            }
        }

        internal static AudioDescription GetDefaultSoundInfo(string clipName)
        {
            if (AudioDictionary.TryGetValue(clipName, out var ret))
            {
                return ret;
            }
            else
            {
                ret = DefaultInfo;
                ret.ClipName = clipName;
                return ret;
            }
        }
    }
}
