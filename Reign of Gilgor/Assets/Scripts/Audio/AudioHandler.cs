﻿using System;
using System.Collections.Generic;
using System.Linq;
using Client.Entity.Player;
using Client.Game;
using Microsoft.Extensions.Logging;
using RoGCore.Assets;
using RoGCore.Utils;
using UnityEngine;

#nullable enable

namespace Client.Audio
{
    internal sealed class AudioHandler : Initializable<AudioHandler>
    {
        public override int InitializeOrder() => 1;

        private const int MaxAudios = 30;
        private const int AudioCooldown = 20;

        private readonly List<AudioSource> Audios = new List<AudioSource>(MaxAudios);
        private readonly Dictionary<string, KeyValuePair<AudioSource, DateTime>> lastPlayed = new Dictionary<string, KeyValuePair<AudioSource, DateTime>>();

        private Player? player;

        public override void Initialize()
        {
            OptionsHandler.SubscribeToOptionsChange<float>("masterVolume", UpdateVolume);
            OptionsHandler.SubscribeToOptionsChange<float>("sfxVolume", UpdateVolume, true);

            GameWorldManager.PlayerCreated.Begin += (player) => this.player = player;

            base.Initialize();
        }

        private AudioSource? GetAudioSource()
        {
            var readyAudios = Audios.Where(x => x != null && !x.isPlaying).ToArray();
            return readyAudios.Length > 0 ? readyAudios[0] : Audios.Count >= Audios.Capacity ? null : CreateAudioSource();
        }

        private AudioSource CreateAudioSource()
        {
            var go = new GameObject("AudioPlayer");
            go.transform.SetParent(Instance.transform);
            var audioSource = go.AddComponent<AudioSource>();
            audioSource.volume = OptionsHandler.GetOptionValue<float>("masterVolume");
            audioSource.priority = Audios.Count;
            Audios.Add(audioSource);
            return audioSource;
        }

        internal static void PlayPitchedSound(string soundName, float pitch) => Instance.Play(AudioData.GetDefaultSoundInfo(soundName), 1, pitch);

        internal static void PlaySound(string soundName, float maxPitchDiff = 0) => Instance.Play(AudioData.GetDefaultSoundInfo(soundName), 1, 1 + UnityEngine.Random.Range(-maxPitchDiff, maxPitchDiff));

        internal static void PlaySound(string soundName, Vector2 origin, float maxPitchDiff = 0) => Instance.Play(AudioData.GetDefaultSoundInfo(soundName), Instance.GetSoundVolume(origin), 1 + UnityEngine.Random.Range(-maxPitchDiff, maxPitchDiff));

        internal static void PlaySound(string soundName, Transform origin, float maxPitchDiff = 0) => Instance.Play(AudioData.GetDefaultSoundInfo(soundName), Instance.GetSoundVolume(origin.position), 1 + UnityEngine.Random.Range(-maxPitchDiff, maxPitchDiff));

        internal static void PlaySound(AudioDescription audioDescription, Vector2 origin) => Instance.Play(audioDescription, Instance.GetSoundVolume(origin));

        private float GetSoundVolume(Vector2 origin)
        {
            if (!OptionsHandler.GetOptionValue<bool>("dynamicSound") || player == null)
            {
                return 1;
            }

            Vector2 playerPosition = player.Transform.localPosition;
            var dist = (float)MathsUtils.Dist(origin.x, origin.y, playerPosition.x, playerPosition.y);
            return Mathf.Clamp(1 - (dist * 0.05f), 0f, 1f);
        }

        private void Play(AudioDescription audioDescription, float volume, float? pitch = null)
        {
            if (volume == 0 || audioDescription.ClipName is null)
            {
                return;
            }

            var source = GetAudioSource();
            if (source == null)
            {
                Logger.LogError("Out of audio sources");
                return;
            }

            var sourceVolume = volume * audioDescription.VolumeMulitplier * OptionsHandler.GetOptionValue<float>("masterVolume") * OptionsHandler.GetOptionValue<float>("sfxVolume");

            var now = DateTime.UtcNow;
            if (lastPlayed.TryGetValue(audioDescription.ClipName, out var audioInfo) && (now - audioInfo.Value).TotalMilliseconds < AudioCooldown)
            {
                if (audioInfo.Key.volume < sourceVolume)
                {
                    audioInfo.Key.volume = sourceVolume;
                }

                return;
            }

            lastPlayed[audioDescription.ClipName] = new KeyValuePair<AudioSource, DateTime>(source, now);

            if (pitch is null)
            {
                pitch = audioDescription.Pitch + UnityEngine.Random.Range(-audioDescription.PitchVariation, audioDescription.PitchVariation);
            }

            source.pitch = pitch.Value;
            source.clip = Resources.Load<AudioClip>($"Sounds/{(audioDescription.Type == RoGCore.Assets.AudioType.Sound ? "Sounds" : "Music")}/{audioDescription.ClipName}");
            source.volume = sourceVolume;
            source.Play();
        }

        public void UIPlaySound(string soundName) => PlaySound(soundName, 0.33f);

        private void UpdateVolume(object sender, float e)
        {
            var master = OptionsHandler.GetOptionValue<float>("masterVolume");
            var sfx = OptionsHandler.GetOptionValue<float>("sfxVolume");
            foreach (var audioSource in Audios.Where(x => x != null))
            {
                audioSource.volume = master * sfx * AudioData.GetDefaultSoundInfo(audioSource.clip.name).VolumeMulitplier;
            }
        }
    }
}