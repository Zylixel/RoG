using System;
using System.Linq;
using Client.Game;
using Client.Networking;
using DG.Tweening;
using UnityEngine;

#nullable enable

namespace Client.Audio
{
    internal sealed class MusicHandler : Initializable<MusicHandler>
    {
        private static AudioSource[] musicPlayers = Array.Empty<AudioSource>();

        public override int InitializeOrder() => 1;

        public override void Initialize()
        {
            musicPlayers = gameObject.GetComponents<AudioSource>();

            OptionsHandler.SubscribeToOptionsChange<float>("masterVolume", UpdateVolume);
            OptionsHandler.SubscribeToOptionsChange<float>("musicVolume", UpdateVolume);

            MapInfoHandler.MapInfoRecieved.Begin += (mapInfo) => SetMusic(mapInfo.Music, 2f);

            base.Initialize();
        }

        internal static void SetMusic(string playAfter, float fadeSeconds)
        {
            var currentMusicPlayer = Array.Find(musicPlayers, x => x.isPlaying);
            if (currentMusicPlayer == null)
            {
                currentMusicPlayer = musicPlayers[0];
            }

            // Don't switch if the clip is already playing
            if (currentMusicPlayer.clip != null && currentMusicPlayer.clip.name == playAfter)
            {
                return;
            }

            var soundInfo = AudioData.GetDefaultSoundInfo(playAfter);

            var newMusicPlayer = musicPlayers.First(x => x != currentMusicPlayer);
            newMusicPlayer.clip = Resources.Load<AudioClip>($"Sounds/Music/{soundInfo.ClipName}");
            newMusicPlayer.volume = 0;
            newMusicPlayer.Play();

            var tween = currentMusicPlayer.DOFade(0, fadeSeconds);
            tween.onComplete += () => currentMusicPlayer.Stop();

            newMusicPlayer.DOFade(OptionsHandler.GetOptionValue<float>("masterVolume") * OptionsHandler.GetOptionValue<float>("musicVolume") * soundInfo.VolumeMulitplier, fadeSeconds);
        }

        private void UpdateVolume(object sender, float e)
        {
            var master = OptionsHandler.GetOptionValue<float>("masterVolume");

            var currentMusicPlayer = Array.Find(musicPlayers, x => x.isPlaying);

            if (currentMusicPlayer != null)
            {
                currentMusicPlayer.volume = master * OptionsHandler.GetOptionValue<float>("musicVolume") * AudioData.GetDefaultSoundInfo(currentMusicPlayer.clip.name).VolumeMulitplier;
            }
        }
    }
}