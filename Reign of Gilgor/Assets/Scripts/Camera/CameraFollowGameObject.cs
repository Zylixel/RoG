﻿using Client.Entity.Player;
using Client.Game;
using UnityEngine;

#nullable enable

namespace Client.Camera
{
    internal sealed class CameraFollowGameObject : MonoBehaviour
    {
        [SerializeField]
        private bool followPlayer;
        [SerializeField]
        private Transform? @object;

        private void Start()
        {
            if (followPlayer)
            {
                GameWorldManager.PlayerCreated.Begin += PlayerCreated;
                @object = GameWorldManager.PlayerCreated?.LastValue?.Transform;
            }
        }

        private void PlayerCreated(Player player)
        {
            @object = player.Transform;
            Update();
        }

        private void Update()
        {
            if (@object == null)
            {
                return;
            }

            if (transform.parent != @object)
            {
                transform.SetParent(@object);
            }

            transform.SetLocalPositionAndRotation(new Vector3(0, 0, -1) + CameraShake.Value, Quaternion.identity);

            if (OptionsHandler.GetOptionValue<bool>("cameraOffset"))
            {
                transform.Translate(new Vector3(0, 3, 0));
            }

            var mousePosition = UnityEngine.Input.mousePosition;
            var middleScreen = new Vector3(Screen.width / 2f, Screen.height / 2f);
            var diff = middleScreen - mousePosition;
            var angle = -Mathf.Atan2(diff.x, diff.y) - (Mathf.PI * 0.5f);

            transform.Translate(OptionsHandler.GetOptionValue<float>("mouseFollowStrength") * Vector3.Distance(mousePosition, middleScreen) * new Vector3(Mathf.Cos(angle), Mathf.Sin(angle), 0));
        }

        private void OnDestroy() => GameWorldManager.PlayerCreated.Begin -= PlayerCreated;
    }
}
