﻿using Client.Game;
using UnityEngine;

namespace Client.Camera
{
    [RequireComponent(typeof(UnityEngine.Camera))]
    [RequireComponent(typeof(RenderTextureGamescreenProvider))]
    internal sealed class CameraOutput : StackableCamera
    {
        [SerializeField]
        private Shader overrideShader;

        private Material generatedOverrideMaterial;
        private RenderTextureGamescreenProvider textureProvider;

        private void Start()
        {
            Camera = GetComponent<UnityEngine.Camera>();
            textureProvider = GetComponent<RenderTextureGamescreenProvider>();

            if (overrideShader)
            {
                generatedOverrideMaterial = new Material(overrideShader);
            }
        }

        internal override void OnBeginCameraRendering() => Camera.targetTexture = textureProvider.Texture;

        internal override void AddToTexture(ref RenderTexture renderTexture)
        {
            if (generatedOverrideMaterial)
            {
                Graphics.Blit(textureProvider.Texture, renderTexture, generatedOverrideMaterial);
            }
            else
            {
                Graphics.Blit(textureProvider.Texture, renderTexture);
            }
        }

        internal override void OnEndCameraRendering() => Camera.targetTexture = null;
    }
}