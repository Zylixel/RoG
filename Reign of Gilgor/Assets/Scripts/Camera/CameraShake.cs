﻿using System;
using Client.Game;
using UnityEngine;

namespace Client.Camera
{
    internal static class CameraShake
    {
        private static DateTime stopTime;
        private static float strength;

        internal static void OnExplosion()
        {
            stopTime = DateTime.Now.AddMilliseconds(350);
            strength = .5f;
        }

        internal static void OnCritialHit()
        {
            stopTime = DateTime.Now.AddMilliseconds(200);
            strength = .2f;
        }

        internal static void OnEnemyKilled()
        {
            stopTime = DateTime.Now.AddMilliseconds(150);
            strength = .15f;
        }

        internal static void OnRareLoot()
        {
            stopTime = DateTime.Now.AddMilliseconds(350);
            strength = .5f;
        }

        internal static Vector3 Value => OptionsHandler.GetOptionValue<bool>("screenShake") && stopTime > DateTime.Now
                    ? new Vector3(UnityEngine.Random.Range(-strength, strength), UnityEngine.Random.Range(-strength, strength), 0)
                    : Vector3.zero;
    }
}
