using System.Linq;
using Client.Game;
using Client.Structures;
using Client.UI;
using UnityEngine;
using UnityEngine.Rendering;

#nullable enable

namespace Client.Camera
{
    public class CameraStack : MonoBehaviour
    {
        [SerializeField]
        [System.Diagnostics.CodeAnalysis.AllowNull]
        private StackableCamera[] stack;

        [SerializeField]
        private ShaderQuality[]? postProcessing;

        [SerializeField]
        private PostProcessingEffect[]? postProcessingEffects;

        [System.Diagnostics.CodeAnalysis.AllowNull]
        private UnityEngine.Camera[] cameras;

        [System.Diagnostics.CodeAnalysis.AllowNull]
        internal RenderTexture OutputTexture;

        internal Game.Event.GameEvent<RenderTexture> OutputUpdated = new Game.Event.GameEvent<RenderTexture>();

        private void Start()
        {
            cameras = GetComponentsInChildren<UnityEngine.Camera>(true);

            AddTexture();
        }

        private void OnEnable()
        {
            cameras = GetComponentsInChildren<UnityEngine.Camera>(true);

            WindowManager.ScreenSizeChangeEvent += ResetTexture;
            RenderPipelineManager.endCameraRendering += OnEndCameraRendering;
            RenderPipelineManager.beginCameraRendering += OnBeginCameraRendering;
            RenderPipelineManager.endFrameRendering += OnEndFrameRendering;

            WindowManager.ScreenSizeChangeEvent += UpdateViewDistance;
            OptionsHandler.SubscribeToOptionsChange<float>("viewDistance", (_, __) => UpdateViewDistance());
            OptionsHandler.SubscribeToOptionsChange<bool>("pixelPerfect", (_, __) => UpdateViewDistance(), true);
        }

        private void OnDisable()
        {
            WindowManager.ScreenSizeChangeEvent -= ResetTexture;
            RenderPipelineManager.endCameraRendering -= OnEndCameraRendering;
            RenderPipelineManager.beginCameraRendering -= OnBeginCameraRendering;
            RenderPipelineManager.endFrameRendering -= OnEndFrameRendering;

            WindowManager.ScreenSizeChangeEvent -= UpdateViewDistance;
            OptionsHandler.UnsubscribeToOptionsChange<float>("viewDistance", (_, __) => UpdateViewDistance());
            OptionsHandler.UnsubscribeToOptionsChange<bool>("pixelPerfect", (_, __) => UpdateViewDistance());
        }

        private void UpdateViewDistance()
        {
            var pixelPerfect = OptionsHandler.GetOptionValue<bool>("pixelPerfect");
            var desiredViewDistance = OptionsHandler.GetOptionValue<float>("viewDistance");
            var newSize = desiredViewDistance;

            if (pixelPerfect)
            {
                // https://blogs.unity3d.com/2015/06/19/pixel-perfect-2d/
                // Orthographic size = ((Vert Resolution)/(PPUScale * PPU)) * 0.5

                const float ppuScale = 2f;
                const float ppu = 8f;
                var zoom = Mathf.RoundToInt(desiredViewDistance) switch
                {
                    6 => 1 / 16f,
                    7 => 1 / 8f,
                    8 => 1 / 8f,
                    9 => 1 / 4f,
                    _ => 1 / 4f,
                };
                newSize = Screen.height / (ppuScale * ppu) * zoom;
            }

            foreach (var camera in cameras)
            {
                camera.orthographicSize = newSize;
            }
        }

        private UnityEngine.Camera? GetLastCameraInStack()
        {
            for (var i = stack.Length - 1; i >= 0; i--)
            {
                if (stack[i].isActiveAndEnabled)
                {
                    return stack[i].Camera;
                }
            }

            return null;
        }

        private StackableCamera? GetStackableCamera(UnityEngine.Camera camera)
        {
            for (var i = 0; i < stack.Length; i++)
            {
                if (stack[i].Camera == camera)
                {
                    return stack[i];
                }
            }

            return null;
        }

        private void OnEndCameraRendering(ScriptableRenderContext arg1, UnityEngine.Camera arg2)
        {
            var stackableCamera = GetStackableCamera(arg2);
            if (stackableCamera != null)
            {
                stackableCamera.OnEndCameraRendering();
            }

            if (arg2 != GetLastCameraInStack())
            {
                return;
            }

            for (var i = 0; i < stack.Length; i++)
            {
                if (stack[i].isActiveAndEnabled)
                {
                    stack[i].AddToTexture(ref OutputTexture);
                }
            }

            OutputUpdated.Invoke(OutputTexture);
        }

        private void OnBeginCameraRendering(ScriptableRenderContext arg1, UnityEngine.Camera arg2)
        {
            var stackableCamera = GetStackableCamera(arg2);
            if (stackableCamera != null)
            {
                stackableCamera.OnBeginCameraRendering();
            }
        }

        private void OnEndFrameRendering(ScriptableRenderContext arg1, UnityEngine.Camera[] arg2)
        {
            if (postProcessing is not null)
            {
                var doPostProcessing = postProcessing.Contains(OptionsHandler.GetOptionValue<ShaderQuality>("graphicsQuality"));
                if (doPostProcessing && postProcessingEffects is not null)
                {
                    for (var i = 0; i < postProcessingEffects.Length; i++)
                    {
                        postProcessingEffects[i].ApplyEffect(OutputTexture);
                    }
                }
            }

            Graphics.Blit(OutputTexture, null as RenderTexture);
        }

        private void ResetTexture()
        {
            Destroy(OutputTexture);
            AddTexture();
        }

        private void AddTexture() => OutputTexture = new RenderTexture(Screen.width, Screen.height, 0, UnityEngine.Experimental.Rendering.DefaultFormat.HDR)
        {
            name = "CameraStackOutput",
        };

        private void OnDestroy()
        {
            OnDisable();
            Destroy(OutputTexture);
        }
    }
}