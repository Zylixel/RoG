﻿using Client.Camera.PostProcessing;
using Client.Game;
using Client.UI;
using UnityEngine;

namespace Client.Camera
{
    [RequireComponent(typeof(CameraStack))]
    internal class CameraStackBlurEffect : MonoBehaviour
    {
        private const float Scale = 0.5f;

        private CameraStack cameraStack;

        [SerializeField]
        private Material[] outputMaterials;

        [SerializeField]
        private Blur blurEffect;

        private RenderTexture downScaledTexture;
        private RenderTexture horizontalBlurTexture;
        private RenderTexture blurTexture;

        internal static Game.Event.GameEvent<RenderTexture> NewOutputCreated = new Game.Event.GameEvent<RenderTexture>();

        private void Start()
        {
            cameraStack = GetComponent<CameraStack>();

            AddTexture();

            WindowManager.ScreenSizeChangeEvent += ResetTexture;
            cameraStack.OutputUpdated.Begin += OnOutputUpdated;
        }

        private void OnOutputUpdated(RenderTexture texture)
        {
            if (!this || !isActiveAndEnabled)
            {
                return;
            }

            TextureScale.GPUScale(ref texture, ref downScaledTexture, FilterMode.Bilinear);
            blurEffect.ApplyEffect(downScaledTexture, blurTexture);
        }

        private void ResetTexture()
        {
            Destroy(downScaledTexture);
            Destroy(blurTexture);
            Destroy(horizontalBlurTexture);

            AddTexture();
        }

        private void AddTexture()
        {
            downScaledTexture = new RenderTexture((int)(Screen.width * Scale), (int)(Screen.height * Scale), 0, RenderTextureFormat.ARGB32)
            {
                name = "DownscaledTexture",
            };

            horizontalBlurTexture = new RenderTexture((int)(Screen.width * Scale), (int)(Screen.height * Scale), 0, RenderTextureFormat.ARGB32)
            {
                name = "horizontalBlurTexture",
            };

            blurTexture = new RenderTexture((int)(Screen.width * Scale), (int)(Screen.height * Scale), 0, RenderTextureFormat.ARGB32)
            {
                name = "BlurTexture",
            };

            foreach (var outputMaterial in outputMaterials)
            {
                outputMaterial.SetTexture("_BlurTexture", blurTexture);
            }

            NewOutputCreated.Invoke(blurTexture);
        }

        private void OnDestroy()
        {
            WindowManager.ScreenSizeChangeEvent -= ResetTexture;
            cameraStack.OutputUpdated.Begin -= OnOutputUpdated;

            Destroy(downScaledTexture);
            Destroy(blurTexture);
            Destroy(horizontalBlurTexture);
        }
    }
}
