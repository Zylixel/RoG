using Client.Game;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Tilemaps;

namespace Client.Camera
{
    [RequireComponent(typeof(UnityEngine.Camera))]
    [RequireComponent(typeof(RenderTextureGamescreenProvider))]
    public class LiquidMaskCamera : MonoBehaviour
    {
        [SerializeField]
        private TilemapRenderer tilemapRenderer;
        [SerializeField]
        private Material maskGeneratorMaterial;

        internal RenderTextureGamescreenProvider TextureProvider;

        private Material previousTilemapRendererMaterial;

        private new UnityEngine.Camera camera;

        private void Start()
        {
            camera = GetComponent<UnityEngine.Camera>();
            TextureProvider = GetComponent<RenderTextureGamescreenProvider>();

            RenderPipelineManager.beginCameraRendering += OnBeginCameraRendering;
            RenderPipelineManager.endCameraRendering += OnEndCameraRendering;
        }

        private void OnBeginCameraRendering(ScriptableRenderContext context, UnityEngine.Camera camera)
        {
            if (camera != this.camera)
            {
                return;
            }

            camera.targetTexture = TextureProvider.Texture;
            previousTilemapRendererMaterial = tilemapRenderer.sharedMaterial;

            tilemapRenderer.sharedMaterial = maskGeneratorMaterial;
            tilemapRenderer.sharedMaterial.SetTexture(ShaderProperties.MaskNoUnderline, previousTilemapRendererMaterial.GetTexture(ShaderProperties.MaskNoUnderline));
        }

        private void OnEndCameraRendering(ScriptableRenderContext context, UnityEngine.Camera camera)
        {
            if (camera != this.camera)
            {
                return;
            }

            camera.targetTexture = null;
            tilemapRenderer.sharedMaterial = previousTilemapRendererMaterial;
        }

        private void OnDestroy()
        {
            RenderPipelineManager.beginCameraRendering -= OnBeginCameraRendering;
            RenderPipelineManager.endCameraRendering -= OnEndCameraRendering;
        }
    }
}
