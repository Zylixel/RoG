using UnityEngine;

namespace Client.Camera.PostProcessing
{
    [CreateAssetMenu(fileName = "BloomScript", menuName = "ScriptableObjects/PostProcessing/Bloom")]
    internal class Bloom : PostProcessingEffect
    {
        [SerializeField]
        private PostProcessingEffect blurEffect;

        [SerializeField]
        private Material extractHdrMaterial;

        [SerializeField]
        private Material bloomAddMaterial;

        [SerializeField]
        private float scaleMultiplier = 1f;

        internal override void ApplyEffect(RenderTexture inputTexure, RenderTexture outputTexture)
        {
            var temp = RenderTexture.GetTemporary(
                (int)(inputTexure.width * scaleMultiplier),
                (int)(inputTexure.height * scaleMultiplier),
                inputTexure.depth,
                inputTexure.graphicsFormat);

            Graphics.Blit(inputTexure, temp, extractHdrMaterial);
            blurEffect.ApplyEffect(temp);
            Graphics.Blit(temp, outputTexture, bloomAddMaterial);

            RenderTexture.ReleaseTemporary(temp);
        }

        private void OnValidate() => scaleMultiplier = Mathf.Max(0.01f, scaleMultiplier);
    }
}