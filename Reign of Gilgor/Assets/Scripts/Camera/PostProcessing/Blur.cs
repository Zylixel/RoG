using Client.Game;
using UnityEngine;

#nullable enable

namespace Client.Camera.PostProcessing
{
    [CreateAssetMenu(fileName = "BlurScript", menuName = "ScriptableObjects/PostProcessing/Blur")]
    internal class Blur : PostProcessingEffect
    {
        [SerializeField]
        private Shader? verticalBlurShader;

        [SerializeField]
        private Shader? horizontalBlurShader;

        [SerializeField]
        private int sampleSize = 5;

        [SerializeField]
        private float spread = 1;

        private Material? generatedVerticleBlurMaterial;

        private Material? generatedHorizontalBlurMaterial;

        internal override void ApplyEffect(RenderTexture inputTexture, RenderTexture outputTexture)
        {
            EnsureGeneration();

            var temp = RenderTexture.GetTemporary(inputTexture.descriptor);
            Graphics.Blit(inputTexture, temp, generatedVerticleBlurMaterial);
            Graphics.Blit(temp, outputTexture, generatedHorizontalBlurMaterial);
            RenderTexture.ReleaseTemporary(temp);
        }

        private void OnValidate()
        {
            spread = Mathf.Max(0.01f, spread);

            if (generatedHorizontalBlurMaterial)
            {
                generatedHorizontalBlurMaterial?.SetFloat(ShaderProperties.SampleSize, sampleSize);
                generatedHorizontalBlurMaterial?.SetFloat(ShaderProperties.Spread, spread);
            }

            if (generatedVerticleBlurMaterial)
            {
                generatedVerticleBlurMaterial?.SetFloat(ShaderProperties.SampleSize, sampleSize);
                generatedVerticleBlurMaterial?.SetFloat(ShaderProperties.Spread, spread);
            }
        }

        private void EnsureGeneration()
        {
            if (generatedVerticleBlurMaterial == null)
            {
                generatedVerticleBlurMaterial = new Material(verticalBlurShader);
                generatedVerticleBlurMaterial.SetFloat(ShaderProperties.SampleSize, sampleSize);
                generatedVerticleBlurMaterial.SetFloat(ShaderProperties.Spread, spread);
            }

            if (generatedHorizontalBlurMaterial == null)
            {
                generatedHorizontalBlurMaterial = new Material(horizontalBlurShader);
                generatedHorizontalBlurMaterial.SetFloat(ShaderProperties.SampleSize, sampleSize);
                generatedHorizontalBlurMaterial.SetFloat(ShaderProperties.Spread, spread);
            }
        }
    }
}