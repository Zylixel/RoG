using UnityEngine;

internal abstract class PostProcessingEffect : ScriptableObject
{
    internal abstract void ApplyEffect(RenderTexture inputTexture, RenderTexture outputTexture);

    internal void ApplyEffect(RenderTexture texture) => ApplyEffect(texture, texture);
}
