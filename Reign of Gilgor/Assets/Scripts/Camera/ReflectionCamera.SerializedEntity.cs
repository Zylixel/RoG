using UnityEngine;

namespace Client.Camera
{
    internal sealed partial class ReflectionCamera
    {
        private struct SerializedEntity
        {
            private Vector3 position;
            private Quaternion rotation;
            private Vector3 localScale;

            internal SerializedEntity(Transform origin)
            {
                position = origin.position;
                rotation = origin.rotation;
                localScale = origin.localScale;
            }

            internal void Apply(ref Transform transform)
            {
                transform.position = position;
                transform.rotation = rotation;
                transform.localScale = localScale;
            }
        }
    }
}