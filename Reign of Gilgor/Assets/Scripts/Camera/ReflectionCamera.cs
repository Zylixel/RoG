﻿using System;
using System.Collections.Generic;
using Client.Camera.PostProcessing;
using Client.Entity.Player;
using Client.Game;
using Client.Game.Map;
using Microsoft.Extensions.Logging;
using RoGCore.Logging;
using UnityEngine;

#nullable enable

namespace Client.Camera
{
    [RequireComponent(typeof(UnityEngine.Camera))]
    [RequireComponent(typeof(RenderTextureGamescreenProvider))]
    internal sealed partial class ReflectionCamera : StackableCamera
    {
        [SerializeField]
        private Shader? overrideShader;
        [SerializeField]
        private Material? reflectionMaterial;
        [SerializeField]
        private LiquidMaskCamera? liquidMaskCamera;
        [SerializeField]
        private Blur? blurFilter;
        [SerializeField]
        private float scaleMultiplier;

        [Range(0, 360)]
        [SerializeField]
        private float shadowDirection;

        [System.Diagnostics.CodeAnalysis.AllowNull]
        private Material overrideMaterial;

        [System.Diagnostics.CodeAnalysis.AllowNull]
        private RenderTextureGamescreenProvider[] textureProvider;

        private static readonly ILogger<ReflectionCamera> Logger = LogFactory.LoggingInstance<ReflectionCamera>();

        private readonly Queue<SerializedEntity> previousTransforms = new Queue<SerializedEntity>();

        internal override void AddToTexture(ref RenderTexture renderTexture) => Graphics.Blit(textureProvider[0].Texture, renderTexture, overrideMaterial);

        private void Start()
        {
            Camera = GetComponent<UnityEngine.Camera>();
            textureProvider = GetComponents<RenderTextureGamescreenProvider>();

            for (var i = 0; i < textureProvider.Length; i++)
            {
                textureProvider[i].ScaleMultiplier = scaleMultiplier;
            }

            if (textureProvider.Length < 2)
            {
                Logger.LogError("Reflection camera requires 2 texture providers to function");
            }

            overrideMaterial = new Material(overrideShader);
            OptionsHandler.SubscribeToOptionsChange<bool>("shadows", ShadowsOptionChanged, true);
        }

        private void ShadowsOptionChanged(object sender, bool e)
        {
            gameObject.SetActive(e);

            if (liquidMaskCamera != null)
            {
                liquidMaskCamera.gameObject.SetActive(e);
            }
        }

        internal override void OnBeginCameraRendering()
        {
            var euler = transform.eulerAngles;
            FlipEntities();
            transform.eulerAngles = euler;

            Camera.targetTexture = textureProvider[0].Texture;
        }

        internal override void OnEndCameraRendering()
        {
            if (reflectionMaterial == null || liquidMaskCamera == null)
            {
                return;
            }

            UnFlipEntities();
            transform.localEulerAngles = Vector3.zero;

            GL.LoadOrtho();
            for (var i = 1; i < textureProvider.Length; i++)
            {
                Graphics.SetRenderTarget(textureProvider[i].Texture);
                GL.Clear(false, true, Color.clear);
            }

            reflectionMaterial.SetTexture(ShaderProperties.MaskNoUnderline, liquidMaskCamera.TextureProvider.Texture);
            Graphics.Blit(textureProvider[0].Texture, textureProvider[1].Texture, reflectionMaterial);
            blurFilter?.ApplyEffect(textureProvider[1].Texture);
            Graphics.Blit(textureProvider[1].Texture, textureProvider[0].Texture);

            Camera.targetTexture = null;
        }

        private void FlipEntities()
        {
            foreach (var item in GameWorldManager.Instance.Map.Entities)
            {
                if (item.Value is Player)
                {
                    continue;
                }

                FlipEntity(item.Value.Transform);
            }

            foreach (var item in GameWorldManager.Instance.Map.StaticEntityMap)
            {
                FlipEntity(item.Transform);
            }
        }

        private void UnFlipEntities()
        {
            foreach (var item in GameWorldManager.Instance.Map.Entities)
            {
                if (item.Value is Player)
                {
                    continue;
                }

                UnFlipEntity(item.Value.Transform);
            }

            foreach (var item in GameWorldManager.Instance.Map.StaticEntityMap)
            {
                UnFlipEntity(item.Transform);
            }
        }

        private void FlipEntity(Transform transform)
        {
            previousTransforms.Enqueue(new SerializedEntity(transform));

            var rotationDeg = (-Map.Rotation.Deg + shadowDirection);

            transform.eulerAngles += new Vector3(0, rotationDeg, rotationDeg);
        }

        private void UnFlipEntity(Transform transform) => previousTransforms.Dequeue().Apply(ref transform);

        private void OnValidate()
        {
            scaleMultiplier = Mathf.Max(0.01f, scaleMultiplier);

            /*if (textureProvider != null)
            {
                for (var i = 0; i < textureProvider.Length; i++)
                {
                    textureProvider[i].scaleMultiplier = scaleMultiplier;
                    textureProvider[i].OnValidate();
                }
            }*/
        }

        private void OnDestroy() => OptionsHandler.UnsubscribeToOptionsChange<bool>("shadows", ShadowsOptionChanged);
    }
}
