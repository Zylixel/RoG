﻿using UnityEngine;

namespace Client.Camera
{
    internal abstract class StackableCamera : MonoBehaviour
    {
        internal abstract void AddToTexture(ref RenderTexture texture);

        internal abstract void OnBeginCameraRendering();

        internal abstract void OnEndCameraRendering();

        internal UnityEngine.Camera Camera;

        public bool Enabled => Camera.isActiveAndEnabled && isActiveAndEnabled;
    }
}
