﻿using System.Collections.Generic;
using Client.Game;
using Client.Structures;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RoGCore.Assets;
using RoGCore.Logging;
using UnityEngine;

#nullable enable

namespace Client
{
    internal class EmbeddedData
    {
        internal static string? Objectjson;
        internal static string? Groundjson;
        internal static string? Itemjson;
        internal static string? Projectilejson;

        internal static Material PlayerMaterial = Resources.Load<Material>("Materials/Entity/PlayerMaterial");
        internal static Material GameEntityMaterial = Resources.Load<Material>("Materials/Entity/GameEntityMaterial");
        internal static Material WindMaterial = Resources.Load<Material>("Materials/Entity/WindMaterial");
        internal static Material PortalMaterial = Resources.Load<Material>("Materials/Entity/PortalMaterial");
        internal static Material WallMaterial = Resources.Load<Material>("Materials/Entity/WallMaterial");

        internal static Sprite ParticleSprite = Resources.Load<Sprite>("Sprites/Particle");

        internal readonly Dictionary<string, ObjectDescription> BetterObjects;

        internal readonly Dictionary<string, ushort> Id2TypeItem;

        internal readonly Dictionary<string, ushort> Id2TypeObject;

        internal readonly Dictionary<string, ushort> Id2TypeTile;
        internal readonly Dictionary<ushort, JsonItem> Items;
        internal readonly Dictionary<ushort, ObjectDescription> Objects;

        internal readonly Dictionary<string, ProjectileDescription> Projectiles;
        internal readonly Dictionary<ushort, GroundDescription> Tiles;

        private static readonly ILogger<EmbeddedData> Logger = LogFactory.LoggingInstance<EmbeddedData>();

        private readonly Dictionary<string, Sprite> sprites = new Dictionary<string, Sprite>();
        private readonly GameAtlas atlas;

        internal EmbeddedData()
        {
            // group similar spritesheets together to allow the atlas to perform better
            var spriteSheets = new KeyValuePair<string, SpriteSheet>[]
            {
                new KeyValuePair<string, SpriteSheet>("Players", new SpriteSheet("Players")),
                new KeyValuePair<string, SpriteSheet>("Objects", new SpriteSheet("Objects")),
                new KeyValuePair<string, SpriteSheet>("Items", new SpriteSheet("Items")),
                new KeyValuePair<string, SpriteSheet>("BlankEquipment", new SpriteSheet("equipmentOutlines")),
                new KeyValuePair<string, SpriteSheet>("Projectiles", new SpriteSheet("Projectiles")),
                new KeyValuePair<string, SpriteSheet>("Enemies", new SpriteSheet("Enemies")),
                new KeyValuePair<string, SpriteSheet>("Ground", new SpriteSheet("Ground", false)),
                new KeyValuePair<string, SpriteSheet>("Walls", new SpriteSheet("Walls", type: SpriteSheet.SpriteSheetType.Expand)),
                new KeyValuePair<string, SpriteSheet>("Enemies16", new SpriteSheet("Enemies16")),
                new KeyValuePair<string, SpriteSheet>("ConditionalEffects", new SpriteSheet("ConditionalEffects")),
                new KeyValuePair<string, SpriteSheet>("Objects16", new SpriteSheet("Objects16")),
                new KeyValuePair<string, SpriteSheet>("HpInside", new SpriteSheet("HpInside", outline: false)),
                new KeyValuePair<string, SpriteSheet>("HpOutline", new SpriteSheet("HpOutline")),
                new KeyValuePair<string, SpriteSheet>("Shadow", new SpriteSheet("Shadow", outline: false)),
                new KeyValuePair<string, SpriteSheet>("Blank", new SpriteSheet("Blank", outline: false)),
            };

            atlas = new GameAtlas(512, true);

            foreach (var pair in spriteSheets)
            {
                for (var i = 0; i < pair.Value.Sprites.Length; i++)
                {
                    if (!(pair.Value.UseAtlas && atlas.Add(pair.Key, i, pair.Value)))
                    {
                        sprites.Add(pair.Key + " " + i, pair.Value.Sprites[i]);
                    }
                }
            }

            atlas.Apply(new Material[] { WallMaterial }, new Material[] { PlayerMaterial, GameEntityMaterial, PortalMaterial }, new Material[] { WindMaterial, PlayerMaterial, GameEntityMaterial, PortalMaterial });

            Id2TypeTile = new Dictionary<string, ushort>();
            Tiles = new Dictionary<ushort, GroundDescription>();
            if (Groundjson is not null)
            {
                AddGrounds(JsonConvert.DeserializeObject<GroundDescription[]>(Groundjson));
            }
            else
            {
                Logger.LogError("Cannot load ground data... File not received");
            }

            Id2TypeObject = new Dictionary<string, ushort>();
            Objects = new Dictionary<ushort, ObjectDescription>();
            BetterObjects = new Dictionary<string, ObjectDescription>();
            if (Objectjson is not null)
            {
                AddObjects(JsonConvert.DeserializeObject<List<ObjectDescription>>(Objectjson));
            }
            else
            {
                Logger.LogError("Cannot load object data... File not received");
            }

            Projectiles = new Dictionary<string, ProjectileDescription>();
            if (Projectilejson is not null)
            {
                AddProjectiles(JsonConvert.DeserializeObject<ProjectileDescription[]>(Projectilejson));
            }
            else
            {
                Logger.LogError("Cannot load projectile data... File not received");
            }

            Id2TypeItem = new Dictionary<string, ushort>();
            Items = new Dictionary<ushort, JsonItem>();
            if (Itemjson is not null)
            {
                AddItems(JsonConvert.DeserializeObject<JsonItem[]>(Itemjson));
            }
            else
            {
                Logger.LogError("Cannot load item data... File not received");
            }
        }

        internal Sprite? GetSprite(string file, int index)
        {
            var atlasSprite = atlas.GetSprite(file, index);

            if (atlasSprite != null)
            {
                return atlasSprite;
            }

            if (sprites.TryGetValue(file + " " + index, out var ret))
            {
                return ret;
            }

            Logger.LogError("Program is trying to load sprite " + file + " " + index +
                           ", but is not located in TextureData");

            return null;
        }

        private void AddGrounds(IReadOnlyList<GroundDescription>? data)
        {
            if (data is null)
            {
                Logger.LogError("GroundData is null");
                return;
            }

            for (ushort i = 0; i < data.Count; i++)
            {
                var desc = data[i];
                desc.ObjectType = i;

                desc.PostInitialization(this);

                if (desc.Name is null)
                {
                    continue;
                }

                Id2TypeTile[desc.Name] = desc.ObjectType;
                Tiles[desc.ObjectType] = desc;
            }
        }

        private void AddObjects(IReadOnlyList<ObjectDescription>? data)
        {
            if (data is null)
            {
                Logger.LogError("ObjectData is null");
                return;
            }

            for (ushort i = 0; i < data.Count; i++)
            {
                var desc = data[i];
                desc.ObjectType = i;
                desc.PostInitialization(this);

                if (desc.Name is null)
                {
                    continue;
                }

                Id2TypeObject[desc.Name] = desc.ObjectType;
                BetterObjects[desc.Name] = desc;
                Objects[desc.ObjectType] = desc;
            }
        }

        private void AddItems(IEnumerable<JsonItem>? data)
        {
            if (data is null)
            {
                Logger.LogError("ProjectileData is null");
                return;
            }

            foreach (var desc in data)
            {
                desc.PostInitialization(this);

                if (desc.Name is null)
                {
                    continue;
                }

                Id2TypeItem[desc.Name] = desc.ObjectType;
                Items[desc.ObjectType] = desc;
            }
        }

        private void AddProjectiles(IEnumerable<ProjectileDescription>? data)
        {
            if (data is null)
            {
                Logger.LogError("ProjectileData is null");
                return;
            }

            foreach (var desc in data)
            {
                desc.PostInitialization(this);

                if (desc.Name is null)
                {
                    continue;
                }

                Projectiles[desc.Name] = desc;
            }
        }
    }
}