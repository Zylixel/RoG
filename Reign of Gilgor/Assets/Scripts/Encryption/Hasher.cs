﻿using System.Security.Cryptography;
using System.Text;

namespace Client.Encryption
{
    internal static class Hasher
    {
        public static string GetHashSha256(string text, string salt)
        {
            var bytes = Encoding.UTF8.GetBytes(text + salt);
            var hashstring = SHA256.Create();
            var hash = hashstring.ComputeHash(bytes);

            StringBuilder hashStringBuilder = new();
            foreach (var x in hash)
            {
                hashStringBuilder.Append(string.Format("{0:x2}", x));
            }

            return hashStringBuilder.ToString();
        }

        public static string GeneratePasswordHash(string username, string password)
        {
            return GetHashSha256(password, username.ToUpperInvariant());
        }
    }
}
