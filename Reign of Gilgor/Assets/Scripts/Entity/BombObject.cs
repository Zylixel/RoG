﻿using System.Collections;
using System.Linq;
using Client.Audio;
using Client.Camera;
using Client.Entity.Helpers;
using Client.Particle;
using RoGCore.Assets;
using RoGCore.Utils;
using UnityEngine;
using static Client.Structures.GroundExtensions;

#nullable enable

namespace Client.Entity
{
    public class BombObject : TossedObject
    {
        private const float BeginCycleSpeed = 250f;
        private float lifetime;
        private float cycleSpeed;

        public override float SpriteCycleSpeed => cycleSpeed;

        internal override void Initialize(GameWorldManager manager, Vector2 startPosition, Vector2 endPosition, ObjectDescription objectDescription, float tossTime)
        {
            base.Initialize(manager, startPosition, endPosition, objectDescription, tossTime);

            lifetime = objectDescription.LifeTime;
        }

        public override void Toss()
        {
            cycleSpeed = BeginCycleSpeed;
            StartCoroutine(this.SpriteCycle(objectDescription, Manager));

            base.Toss();
        }

        protected override bool DestroyOnComplete() => false;

        protected override void TossComplete()
        {
            base.TossComplete();
            StartCoroutine(BeginExplosion());
        }

        private IEnumerator BeginExplosion()
        {
            var elapsedTime = 0f;
            while (elapsedTime < lifetime)
            {
                cycleSpeed = BeginCycleSpeed - (BeginCycleSpeed * (elapsedTime / lifetime));

                yield return null;

                elapsedTime += Time.deltaTime * 1000f;
            }

            Explode();
        }

        private void Explode()
        {
            Vector2 position = transform.position;
            var particleSystem = ParticleFactory.CreateParticle("ExplosionParticle", new CreateParticleProperties { AutoReturn = true, LocalPosition = transform.position });

            if (particleSystem != null)
            {
                particleSystem.transform.eulerAngles = new Vector3(180f, 0f, -Game.Map.Map.Rotation.Deg);

                var groundExplosionSystem = particleSystem.GetComponentsInChildren<ParticleSystem>().First(x => x.gameObject.name == "GroundParticles");

                var main = groundExplosionSystem.main;

                var ground = Manager.Map.GroundMap[position.Convert().ToInt()]?.Description;
                main.startColor = ground is not null
                    ? new ParticleSystem.MinMaxGradient(
                        ground.GetRandomColor(Manager.GlobalRandom),
                        ground.GetRandomColor(Manager.GlobalRandom))
                    : new ParticleSystem.MinMaxGradient(
                        Color.red,
                        Color.yellow);
            }

            if (Manager.Map.InViewDistance(transform.position))
            {
                CameraShake.OnExplosion();
                AudioHandler.PlaySound("Explosion", position);
            }

            Destroy(gameObject);
        }
    }
}
