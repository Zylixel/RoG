﻿using System.Collections;
using System.Linq;
using Client.Audio;
using Client.Camera;
using Client.Entity.Helpers;
using Client.Entity.Interfaces;
using Client.Entity.Notifications;
using Client.Game;
using Client.Particle;
using DG.Tweening;
using RoGCore.Assets;
using RoGCore.Models;
using RoGCore.Networking.Packets;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Vector2 = System.Numerics.Vector2;

namespace Client.Entity
{
    internal class Character : SimpleEntity, INotificationHolder
    {
        protected new EntityAnimator animation;

        private Image messagePopup;
        private float messageTimer;

        private bool overridingSize;
        private TintEntityHandler tint;
        private bool overridingSprite;

        internal override void Init(in ObjectStats stats, ObjectDescription objectDesc, GameObjectComponentHolder componentHolder, GameWorldManager manager, bool doOnUpdate)
        {
            base.Init(stats, objectDesc, componentHolder, manager, doOnUpdate);

            tint = new TintEntityHandler(this);

            // Wait to start animation until the titlescreenloader is done
            if (this is Player.Player)
            {
                GameObject.transform.localScale = new Vector3(0.01f, 0.01f);
                overridingSize = true;
            }
            else if (CanAppear())
            {
                StartCoroutine(Appear());
            }
        }

        internal HpBar HpBar { get; private set; }

        public DamageNotification DamageNotification { get; set; }

        internal virtual bool CanAppear() => false;

        internal virtual bool CanVanish() => true;

        protected override Material Material() => EmbeddedData.GameEntityMaterial;

        protected override void SetSprite(ObjectDescription objectDescription)
        {
            if (objectDescription.Texture is null)
            {
                SpriteRenderer.enabled = false;
                return;
            }

            SpriteRenderer.sortingLayerName = "Entity";

            switch (objectDescription.Texture.Type)
            {
                case TextureData.TEXTURETYPERANDOMCYCLE:
                case TextureData.TEXTURETYPECYCLE:
                case TextureData.TEXTURETYPEPINGPONG:
                    StartCoroutine(this.SpriteCycle(objectDescription, Manager));
                    break;
                case TextureData.TEXTURETYPERANDOM:
                    SpriteRenderer.sprite = objectDescription.Textures[Manager.GlobalRandom.Next(0, objectDescription.Textures.Length)];
                    break;
                case TextureData.TEXTURETYPEANIMATED:
                    SpriteRenderer.sprite = objectDescription.Textures.FirstOrDefault();
                    animation = new EntityAnimator(this, objectDescription.Textures);
                    break;
                default:
                    SpriteRenderer.sprite = objectDescription.Textures.FirstOrDefault();
                    break;
            }
        }

        public override void SetSideSpriteByIndex(int textureCycleIndex)
        {
            if (overridingSprite)
            {
                return;
            }

            base.SetSideSpriteByIndex(textureCycleIndex);
        }

        protected override void Update()
        {
            base.Update();
            if (animation is not null && !overridingSprite)
            {
                SpriteRenderer.sprite = animation.GetSprite();
            }

            tint.Tick();
            if (messagePopup == null)
            {
                return;
            }

            messageTimer += Time.deltaTime;
            if (!(messageTimer >= 5))
            {
                return;
            }

            Destroy(messagePopup.gameObject);
            messagePopup = null;
        }

        internal IEnumerator Appear()
        {
            overridingSize = true;
            tint.Transitioning(true);
            GameObject.transform.localScale = new Vector3(0f, 0f, 1f);

            yield return DOTween.Sequence()
                .Append(SpriteRenderer.transform.DOScaleX(Size.x, 0.33f).SetEase(Ease.InCubic))
                .AppendCallback(() =>
                {
                    if (this is Player.Player)
                    {
                        AudioHandler.PlaySound("Fantasy/Special Click 15");
                    }
                })
                .AppendInterval(0.1f)
                .AppendCallback(() =>
                {
                    if (this is Player.Player)
                    {
                        ParticleFactory.CreatePlayerSpawnProjectile(Transform.localPosition);
                    }
                })
                .Append(SpriteRenderer.transform.DOScaleY(Size.y, 0.33f).SetEase(Ease.InCubic))
                .WaitForCompletion();

            overridingSize = false;
            tint.Transitioning(false);
        }

        internal IEnumerator TeleportAnimation()
        {
            overridingSize = true;
            tint.Transitioning(true);
            GameObject.transform.localScale = new Vector3(0f, 0f, 1f);

            yield return DOTween.Sequence()
                .Append(SpriteRenderer.transform.DOScaleX(Size.x, 0.33f).SetEase(Ease.InCubic))
                .AppendInterval(0.1f)
                .Append(SpriteRenderer.transform.DOScaleY(Size.y, 0.33f).SetEase(Ease.InCubic))
                .WaitForCompletion();

            overridingSize = false;
            tint.Transitioning(false);
        }

        internal override void OnUpdate(in ObjectStats stat)
        {
            base.OnUpdate(stat);

            if (HpBar is not null)
            {
                HpBar.Update();
            }
            else if (MaxHP > 0)
            {
                HpBar = new HpBar(this);
            }

            for (var i = 0; i < stat.Stats.Length; i++)
            {
                var type = stat.Stats[i].Key;
                var data = stat.Stats[i].Value;
                switch (type)
                {
                    case StatsType.Glowing:
                        {
                            var color = (int)data;
                            SetOutlineColor(2f * (Color)new RGB(color));
                            break;
                        }

                    case StatsType.Texture1:
                        {
                            if (Material() != EmbeddedData.PlayerMaterial)
                            {
                                break;
                            }

                            var color = (int)data;
                            if (color != -1)
                            {
                                SetRendererColor(ShaderProperties.BigColor, new RGB(color));
                                SetRendererBool(ShaderProperties.UseBigColor, true);
                            }
                            else
                            {
                                SetRendererBool(ShaderProperties.UseBigColor, false);
                            }

                            break;
                        }

                    case StatsType.Texture2:
                        {
                            if (Material() != EmbeddedData.PlayerMaterial)
                            {
                                break;
                            }

                            var color = (int)data;
                            if (color != -1)
                            {
                                SetRendererColor(ShaderProperties.SmallColor, new RGB(color));
                                SetRendererBool(ShaderProperties.UseSmallColor, true);
                            }
                            else
                            {
                                SetRendererBool(ShaderProperties.UseSmallColor, false);
                            }

                            break;
                        }
                }
            }
        }

        protected virtual void SetOutlineColor(Color color) => SetRendererColor(ShaderProperties.OutlineColor, color);

        protected override void SizeChanged(Vector3 size)
        {
            if (overridingSize)
            {
                Size = size;

                HitRadius = Projectile.Projectile.GetHitRadius(this);

                return;
            }

            base.SizeChanged(size);
        }

        internal virtual void Vanish()
        {
            if (Canvas != null)
            {
                Canvas.gameObject.SetActive(false);
            }

            HpBar?.Dispose();
            HpBar = null;

            tint.Transitioning(true);
            tint.Tick();

            overridingSprite = true;

            DOTween.Sequence()
                .Append(SpriteRenderer.transform.DOScaleY(0.05f, 0.33f).SetEase(Ease.InCubic))
                .AppendInterval(0.1f)
                .AppendCallback(() => SpriteRenderer.sprite = EmbeddedData.ParticleSprite)
                .Append(SpriteRenderer.transform.DOScaleX(0f, 0.33f).SetEase(Ease.InCubic))
                .AppendCallback(() => EntityFactory.DestroyEntity(this));
        }

        internal virtual void Shoot(
            ushort bulletId,
            CustomProjectile customDesc,
            Angle angle,
            long createTime,
            bool hitsPlayer,
            Item? from = null,
            Vector2? customPosition = null)
        {
            var shot = EntityFactory.CreateProjectile(this, bulletId, customDesc, angle, createTime,
                customPosition ?? Position, from, hitsPlayer);

            if (ObjectDesc.ShootSound is not null)
            {
                AudioHandler.PlaySound(ObjectDesc.ShootSound, Transform.localPosition);
            }

            Manager.Map.AddProjectile(shot.FullId, shot);
            animation?.OnAttack(angle);
        }

        internal void SwordSweep(Angle angle)
        {
            animation?.OnAttack(angle);
            StartCoroutine(SweepAnimation(angle));
        }

        private IEnumerator SweepAnimation(Angle angle)
        {
            var sweep = EntityPool.LeaseObject(GameObjectArchetype.SpriteRenderer);
            var sweepRenderer = sweep.ComponentHolder.SpriteRenderer;
            var sprites = Resources.LoadAll<Sprite>("Sprites/SwordSlash");
            var currentAlpha = Color.white;
            currentAlpha.a = 0.25f;
            sweepRenderer.sprite = sprites[0];
            sweepRenderer.color = currentAlpha;
            sweepRenderer.sortingLayerName = "UI";
            sweep.GameObject.transform.localScale = Vector3.one;
            const float offset = 0.75f;
            Vector3 trig = new UnityEngine.Vector2(angle.Cos, angle.Sin) * offset;
            sweep.GameObject.transform.position = Transform.localPosition + trig;
            sweep.GameObject.transform.eulerAngles = new Vector3(0, 0, angle.Deg - 90f);
            yield return new WaitForSeconds(0.033f);
            sweepRenderer.sprite = sprites[1];
            currentAlpha.a = 0.5f;
            sweepRenderer.color = currentAlpha;
            yield return new WaitForSeconds(0.033f);
            sweepRenderer.sprite = sprites[2];
            currentAlpha.a = 0.75f;
            sweepRenderer.color = currentAlpha;
            yield return new WaitForSeconds(0.033f);
            sweepRenderer.sprite = sprites[3];
            currentAlpha.a = 0.5f;
            sweepRenderer.color = currentAlpha;
            yield return new WaitForSeconds(0.033f);
            sweepRenderer.sprite = sprites[4];
            currentAlpha.a = 0.5f;
            sweepRenderer.color = currentAlpha;
            yield return new WaitForSeconds(0.033f);
            EntityPool.ReturnObject(sweep);
        }

        internal override bool OnHit(Projectile.Projectile proj)
        {
            if (!base.OnHit(proj))
            {
                return false;
            }

            tint.Hit();
            return true;
        }

        internal void OnText(in RoGCore.Networking.Packets.Text packet)
        {
            Debug.Assert(Canvas != null);

            if (messagePopup == null)
            {
                messagePopup = Instantiate(GamePrefabs.MessagePopup).GetComponent<Image>();
                messagePopup.transform.SetParent(Canvas.transform);
                messagePopup.transform.localPosition = new Vector3(0, 0.6f * Size.x);
                messagePopup.transform.eulerAngles = Game.Map.Map.Rotation.ToUnity3();
                var message = messagePopup.GetComponentInChildren<TextMeshProUGUI>();
                message.text = packet.Message;
                LayoutRebuilder.ForceRebuildLayoutImmediate(message.rectTransform);
                messagePopup.rectTransform.sizeDelta =
                    new UnityEngine.Vector2(5f, 0.75f + message.rectTransform.sizeDelta.y);
            }
            else
            {
                var message = messagePopup.GetComponentInChildren<TextMeshProUGUI>();
                message.text = packet.Message;
                LayoutRebuilder.ForceRebuildLayoutImmediate(message.rectTransform);
                messagePopup.rectTransform.sizeDelta =
                    new UnityEngine.Vector2(5f, 0.75f + message.rectTransform.sizeDelta.y);
            }

            messageTimer = 0;
        }

        internal virtual void OnDamage(in Damage packet)
        {
            if (packet.Critical)
            {
                CameraShake.OnCritialHit();
            }

            if (packet.Killed)
            {
                ParticleFactory.CreateDeathParticle(Transform.localPosition);

                if (Manager.Map.Entities.TryGetValue(packet.ObjectId, out var entity) && entity is Player.Player)
                {
                    CameraShake.OnEnemyKilled();
                }
            }

            if (ObjectDesc.HitSound is not null)
            {
                AudioHandler.PlaySound(ObjectDesc.HitSound, Transform.localPosition);
            }

            if (Canvas == null)
            {
                return;
            }

            if (packet.Critical)
            {
                EntityFactory.CreateNotification<Notifications.Notification>(this, "Critical -" + packet.DamageAmount, GameColors.ArmorBreak);

                if (DamageNotification != null && DamageNotification.Stackable)
                {
                    DamageNotification.UpdateText(packet.DamageAmount);
                }
            }
            else
            {
                if (DamageNotification != null && DamageNotification.Stackable)
                {
                    DamageNotification.UpdateText(packet.DamageAmount);
                }
                else
                {
                    DamageNotification = EntityFactory.CreateNotification<DamageNotification>(this, packet.DamageAmount.ToString(), packet.Pierce ? GameColors.ArmorBreak : GameColors.NOALPHA_RED);
                }
            }
        }

        protected override void OnDestroy()
        {
            HpBar?.Dispose();

            base.OnDestroy();
        }
    }
}