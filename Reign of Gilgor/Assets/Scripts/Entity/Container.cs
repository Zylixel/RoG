﻿using System.Collections.Generic;
using System.Linq;
using Client.Audio;
using Client.Game.Event;
using Client.UI;
using RoGCore.Assets;
using RoGCore.Models;
using Structures;
using UnityEngine;

namespace Client.Entity
{
    internal sealed class Container : SimpleEntity, IContainer
    {
        private static readonly HashSet<long> FoundContainers = new HashSet<long>();

        static Container()
        {
            ReconnectEvent.Begin += () => FoundContainers.Clear();
        }

        internal override void Init(in ObjectStats stats, ObjectDescription objectDesc, GameObjectComponentHolder componentHolder, GameWorldManager manager, bool doOnUpdate)
        {
            base.Init(stats, objectDesc, componentHolder, manager, true);

            if (FoundContainers.Add(stats.Id))
            {
                Notify();
            }
        }

        public Item?[] Inventory { get; set; }

        protected override Material Material() => EmbeddedData.GameEntityMaterial;

        internal override void OnUpdate(in ObjectStats stat)
        {
            base.OnUpdate(stat);
            for (var i = 0; i < stat.Stats.Length; i++)
            {
                var data = stat.Stats[i].Value;
                Inventory = stat.Stats[i].Key == StatsType.Inventory
                    ? RawItem.Cook(RawItem.Read((byte[])data), Manager.GameData.Items).ToArray()
                    : Inventory;
            }
        }

        internal void Notify()
        {
            AudioHandler.PlaySound("Fantasy/Bag 15", Transform.localPosition);

            var bestItem = 0;
            foreach (var item in Inventory)
            {
                if (item is null || item.Value.BaseItem is null)
                {
                    continue;
                }

                bestItem = Mathf.Min(item.Value.BaseItem.Tier);
            }

            if (bestItem < -1)
            {
                ItemNotificationEvent.Invoke(bestItem);
            }
        }
    }
}