using Client.Particle;
using RoGCore.Assets;
using RoGCore.Models;
using UnityEngine;

namespace Client.Entity
{
    internal sealed class Decor : SimpleEntity
    {
        private ParticleSystem particleEffect;

        internal override void Init(in ObjectStats stats, ObjectDescription objectDesc, GameObjectComponentHolder componentHolder, GameWorldManager manager, bool doOnUpdate)
        {
            var position = stats.Pos;
            if (objectDesc.StaticRandomOffset)
            {
                position += new System.Numerics.Vector2(Random.Range(-0.25f, 0.25f), Random.Range(-0.25f, 0.25f));
            }

            var newStats = new ObjectStats(stats.Id, position, stats.Stats);

            base.Init(newStats, objectDesc, componentHolder, manager, doOnUpdate);

            if (objectDesc.Name == "Snowy Pine Tree")
            {
                // particleEffect = ParticleFactory.CreateParticle("SnowParticle", new CreateProjectileProperties { Parent = Child.transform });
            }

            SpriteRenderer.flipX = Random.Range(0, 2) == 0;
        }

        private void RemoveParticleEffect()
        {
            if (particleEffect != null)
            {
                ParticleFactory.ReturnParticle(particleEffect);
                particleEffect = null;
            }
        }

        protected override void OnDestroy()
        {
            RemoveParticleEffect();

            base.OnDestroy();
        }

        protected override void Update()
        {
            base.Update();

            if (particleEffect != null)
            {
                particleEffect.transform.eulerAngles = Game.Map.Map.Rotation.ToUnity3();
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            if (particleEffect == null && ObjectDesc?.Name == "Snowy Pine Tree")
            {
                // particleEffect = ParticleFactory.CreateParticle("SnowParticle", new CreateProjectileProperties { Parent = Child.transform });
            }
        }

        private void OnDisable() => RemoveParticleEffect();
    }
}