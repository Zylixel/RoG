﻿using RoGCore.Networking.Packets;

namespace Client.Entity
{
    internal class Enemy : Character
    {
        internal delegate void OnVanishDelegate();

        internal event OnVanishDelegate OnVanish;

        internal float DamagePercentFromPlayer;

        internal override void OnDamage(in Damage packet)
        {
            if (packet.ObjectId == Manager.Map.PlayerId && MaxHP > 0)
            {
                DamagePercentFromPlayer += 100f * ((float)packet.DamageAmount / MaxHP);
            }

            base.OnDamage(packet);
        }

        internal override void Vanish()
        {
            TriggerOnVanish();
            base.Vanish();
        }

        protected override void OnDestroy()
        {
            TriggerOnVanish();
            base.OnDestroy();
        }

        private void TriggerOnVanish()
        {
            if (OnVanish is not null)
            {
                OnVanish.Invoke();

                if (OnVanish is not null)
                {
                    foreach (var d in OnVanish.GetInvocationList())
                    {
                        OnVanish -= (OnVanishDelegate)d;
                    }
                }
            }
        }
    }
}
