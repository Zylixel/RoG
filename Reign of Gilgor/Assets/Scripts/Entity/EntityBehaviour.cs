﻿using System.Diagnostics.CodeAnalysis;
using Client.Entity.Helpers;
using Client.Game;
using RoGCore.Assets;
using RoGCore.Models;
using RoGCore.Utils;
using UnityEngine;
using Vector2 = System.Numerics.Vector2;
using Vector3 = UnityEngine.Vector3;

#nullable enable

namespace Client.Entity
{
    internal class EntityBehaviour : MonoBehaviour
    {
        [AllowNull]
        internal GameObject GameObject;
        [AllowNull]
        internal Transform Transform;
        [AllowNull]
        internal GameWorldManager Manager;
        [AllowNull]
        internal ObjectDescription ObjectDesc;

        internal long Id;
        internal Vector3 Size;

        private new string? name;
        private EntityMovement? movement;

        public ObjectDescription ObjectDescProperty => ObjectDesc;

        public Vector3 SizeProperty => Size;

        internal Vector2 Position
        {
            get
            {
                return new Vector2(Transform.localPosition.x, Transform.localPosition.y);
            }

            set
            {
                movement?.SetTargetPoint(Transform.position, value.ToUnity3());
                ServerPosition = value.ToUnity3();
            }
        }

        internal ObjectStats Stats { get; private set; }

        internal string Name => name is null ? ObjectDesc.Name ?? "Unknown Name" : name;

        internal int HP { get; private set; }

        internal int MaxHP { get; private set; }

        protected Vector3 ServerPosition { get; private set; }

        internal virtual void OnUpdate(in ObjectStats stat)
        {
            Stats = stat;
            Position = stat.Pos;
            for (var i = 0; i < stat.Stats.Length; i++)
            {
                var data = stat.Stats[i].Value;
                switch (stat.Stats[i].Key)
                {
                    case StatsType.Name:
                        name = data.ToString();
                        break;
                    case StatsType.Size:
                        var s = (int)data * 0.01f;

                        if (OptionsHandler.GetOptionValue<bool>("pixelPerfect"))
                        {
                            s = Mathf.Round(s * 4) / 4f;
                        }

                        var size = new Vector3(s, s, 1);
                        if (Size != size)
                        {
                            SizeChanged(size);
                        }

                        break;
                    case StatsType.Hp:
                        HP = (int)data;
                        break;
                    case StatsType.MaximumHp:
                        MaxHP = (int)data;
                        break;
                }
            }
        }

        protected void Init(in ObjectStats stats, ObjectDescription objectDesc, GameWorldManager manager, bool doOnUpdate)
        {
            Stats = stats;
            Id = stats.Id;
            ObjectDesc = objectDesc;
            Manager = manager;

            Transform = transform;
            GameObject = gameObject;

            Transform.localPosition = stats.Pos.ToUnity3();

            if (objectDesc.Static)
            {
                movement = new EntityMovement();
            }

            if (doOnUpdate)
            {
                OnUpdate(Stats);
            }
        }

        protected virtual void SizeChanged(Vector3 size) => Size = size;

        protected virtual void Update()
        {
            RenderVisualPosition();
        }

        protected virtual void RenderVisualPosition()
        {
            if (movement is not null)
            {
                Transform.position = movement.CurrentVisualPosition();
            }
            else
            {
                Transform.position = ServerPosition;
            }
        }
    }
}