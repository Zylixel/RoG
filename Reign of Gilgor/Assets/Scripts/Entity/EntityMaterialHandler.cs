﻿using System.Runtime.CompilerServices;
using UnityEngine;

namespace Client.Entity
{
    internal partial class SimpleEntity
    {
        private MaterialPropertyBlock MaterialPropertyBlock { get; set; }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal void ApplyMaterial()
        {
            var material = Material();

            SpriteRenderer.sharedMaterial = material;

            if (material == EmbeddedData.WindMaterial)
            {
                SetRendererVector(PositionHash, GameObject.transform.position);
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal Color GetRendererColor(int hash) => SpriteRenderer.sharedMaterial.GetColor(hash);

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal void SetRendererColor(int hash, Color color)
        {
            if (MaterialPropertyBlock is null)
            {
                return;
            }

            MaterialPropertyBlock.SetColor(hash, color);
            SpriteRenderer.SetPropertyBlock(MaterialPropertyBlock);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal float GetRendererFloat(int hash) => SpriteRenderer.sharedMaterial.GetFloat(hash);

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal void SetRendererFloat(int hash, float value)
        {
            if (MaterialPropertyBlock is null)
            {
                return;
            }

            MaterialPropertyBlock.SetFloat(hash, value);
            SpriteRenderer.SetPropertyBlock(MaterialPropertyBlock);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal void SetRendererBool(int hash, bool value)
        {
            if (MaterialPropertyBlock is null)
            {
                return;
            }

            MaterialPropertyBlock.SetInt(hash, value ? 1 : 0);
            SpriteRenderer.SetPropertyBlock(MaterialPropertyBlock);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal void SetRendererVector(int hash, Vector4 value)
        {
            if (MaterialPropertyBlock is null)
            {
                return;
            }

            MaterialPropertyBlock.SetVector(hash, value);
            SpriteRenderer.SetPropertyBlock(MaterialPropertyBlock);
        }
    }
}