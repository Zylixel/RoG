﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.Rendering.Universal;

namespace Client.Entity
{
    [Flags]
    internal enum GameObjectArchetype : byte
    {
        None = 0,
        SpriteRenderer = 1,
        Light = 2,
        TextMeshProUGUI = 4,
        Canvas = 8,
    }

    internal class GameObjectComponentHolder
    {
        public GameObjectComponentHolder(GameObjectArchetype archetype)
        {
            Archetype = archetype;
        }

        internal Light2D Light2d { get; private set; }

        internal SpriteRenderer SpriteRenderer { get; private set; }

        internal TextMeshProUGUI TextMeshProUGUI { get; private set; }

        internal Canvas Canvas { get; private set; }

        internal GameObjectArchetype Archetype { get; }

        internal void Populate(GameObject gameObject)
        {
            TextMeshProUGUI = gameObject.GetComponent<TextMeshProUGUI>();
            SpriteRenderer = gameObject.GetComponent<SpriteRenderer>();

            if (Archetype.HasFlag(GameObjectArchetype.Light))
            {
                var lightObject = UnityEngine.Object.Instantiate(GamePrefabs.GetObject("LightObject"));
                Light2d = lightObject.GetComponent<Light2D>();
                lightObject.transform.SetParent(gameObject.transform);
            }

            if (Archetype.HasFlag(GameObjectArchetype.Canvas))
            {
                Canvas = UnityEngine.Object.Instantiate(GamePrefabs.GameEntityCanvas).GetComponent<Canvas>();
                Canvas.transform.SetParent(gameObject.transform);
                Canvas.worldCamera = UnityEngine.Camera.main;
                Canvas.transform.localPosition = Vector3.zero;
            }
        }

        internal void SetToDefault(GameObject gameObject)
        {
            gameObject.transform.position = Vector3.zero;
            gameObject.transform.eulerAngles = Vector3.zero;
            gameObject.transform.localScale = Vector3.one;

#if UNITY_EDITOR || DEVELOPMENT_BUILD
            gameObject.name = "heldEntity";
#endif

            if (SpriteRenderer != null)
            {
                SpriteRenderer.enabled = true;
                SpriteRenderer.SetPropertyBlock(new MaterialPropertyBlock());
                SpriteRenderer.color = Color.white;
                SpriteRenderer.flipX = false;
                SpriteRenderer.flipY = false;
                SpriteRenderer.drawMode = SpriteDrawMode.Simple;
                SpriteRenderer.sortingOrder = 0;
            }

            if (Canvas != null)
            {
                Canvas.gameObject.SetActive(true);
            }

            if (Light2d != null)
            {
                Light2d.gameObject.SetActive(true);
            }
        }
    }
}