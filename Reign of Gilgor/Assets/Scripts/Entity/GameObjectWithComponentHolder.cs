﻿using UnityEngine;

namespace Client.Entity
{
    internal class GameObjectWithComponentHolder
    {
        internal readonly GameObject GameObject;
        internal readonly GameObjectComponentHolder ComponentHolder;

        public GameObjectWithComponentHolder(GameObject gameObject, GameObjectComponentHolder componentHolder)
        {
            GameObject = gameObject;
            ComponentHolder = componentHolder;
        }
    }
}