﻿#if UNITY_EDITOR || DEBUG

using System;
using System.Linq;
using Client.Game.Hitbox;
using UnityEngine;

namespace Assets.Scripts.Entity.Helpers
{
    internal sealed class DebugHitboxDisplay : MonoBehaviour
    {
        [SerializeField]
        private bool RandomCollisionColor = false;
        [Range(0, 1)]
        [SerializeField]
        private float CircleResolution = 0.5f;

        private void Update()
        {
            var characters = FindObjectsOfType<MonoBehaviour>().OfType<IHitbox>();

            var circlePrecision = Mathf.Lerp(Mathf.PI / 4f, Mathf.PI / 16f, CircleResolution);

            foreach (var character in characters)
            {
                var hitboxColor = Color.white;
                foreach (var otherCharacter in characters)
                {
                    if (otherCharacter.Equals(character))
                    {
                        continue;
                    }

                    if (character.Colliding(otherCharacter))
                    {
                        if (RandomCollisionColor)
                        {
                            var random = new System.Random(Mathf.Max(character.GetHashCode(), otherCharacter.GetHashCode()));
                            hitboxColor = new Color(
                                (float)random.NextDouble(),
                                (float)random.NextDouble(),
                                (float)random.NextDouble());
                        }
                        else
                        {
                            hitboxColor = Color.red;
                        }

                        break;
                    }
                }

                switch (character.HitboxType)
                {
                    case HitboxType.Square:
                        Debug.DrawLine(
                            character.HitboxCenter + new Vector2(-character.HitboxSize, -character.HitboxSize),
                            character.HitboxCenter + new Vector2(-character.HitboxSize, character.HitboxSize),
                            hitboxColor);

                        Debug.DrawLine(
                           character.HitboxCenter + new Vector2(-character.HitboxSize, -character.HitboxSize),
                           character.HitboxCenter + new Vector2(character.HitboxSize, -character.HitboxSize),
                           hitboxColor);

                        Debug.DrawLine(
                            character.HitboxCenter + new Vector2(character.HitboxSize, -character.HitboxSize),
                            character.HitboxCenter + new Vector2(character.HitboxSize, character.HitboxSize),
                            hitboxColor);

                        Debug.DrawLine(
                            character.HitboxCenter + new Vector2(-character.HitboxSize, character.HitboxSize),
                            character.HitboxCenter + new Vector2(character.HitboxSize, character.HitboxSize),
                            hitboxColor);
                        break;
                    case HitboxType.Circle:
                        Vector2 newPosition;
                        var previousPosition = new Vector2(Mathf.Cos(0), Mathf.Sin(0)) * character.HitboxSize;
                        var rad = circlePrecision;
                        for (; rad < 2 * Mathf.PI; rad += circlePrecision)
                        {
                            newPosition = new Vector2(Mathf.Cos(rad), Mathf.Sin(rad)) * character.HitboxSize;
                            Debug.DrawLine(character.HitboxCenter + previousPosition, character.HitboxCenter + newPosition, hitboxColor);
                            previousPosition = newPosition;
                        }

                        rad = 2 * Mathf.PI;
                        newPosition = new Vector2(Mathf.Cos(rad), Mathf.Sin(rad)) * character.HitboxSize;
                        Debug.DrawLine(character.HitboxCenter + previousPosition, character.HitboxCenter + newPosition, hitboxColor);
                        break;
                    default:
                        throw new ArgumentException(nameof(character.HitboxType) + "cannot be rendered in DebugHitboxDisplay");
                }
            }
        }
    }
}

#endif