﻿using System;
using System.Collections;
using Microsoft.Extensions.Logging;
using RoGCore.Logging;
using RoGCore.Models;
using RoGCore.Utils;
using UnityEngine;

namespace Client.Entity.Helpers
{
    internal sealed class EntityAnimator
    {
        private static readonly int[] STILL = new int[3] { 0, 4, 9 };
        private static readonly int[] MOVING_SIDE = new int[2] { 0, 1 };
        private static readonly int[] MOVING_DOWN = new int[2] { 5, 6 };
        private static readonly int[] MOVING_UP = new int[2] { 10, 11 };
        private static readonly int[] SHOOTING_SIDE = new int[2] { 2, 3 };
        private static readonly int[] SHOOTING_DOWN = new int[2] { 7, 8 };
        private static readonly int[] SHOOTING_UP = new int[2] { 12, 13 };

        private static readonly int[] BASIC_STILL = new int[1] { 0 };
        private static readonly int[] BASIC_MOVING = new int[2] { 1, 2 };
        private static readonly int[] BASIC_SHOOTING = new int[2] { 3, 4 };

        private readonly Character self;
        private readonly Sprite[] textures;
        private readonly AnimationStyle type;

        private Coroutine currentMoveTimer;

        private Coroutine currentShootTimer;

        private Direction lastFace;
        internal float LastShotAngle;
        private int moveStep;
        private Vector2 previousRender;
        private int shootStep;

        internal float ShotAnimationCooldown;
        internal bool ShotThisTick;

        private static readonly ILogger<EntityAnimator> Logger = LogFactory.LoggingInstance<EntityAnimator>();

        internal EntityAnimator(Character self, Sprite[] textures = null)
        {
            this.self = self;
            this.textures = textures ?? textures;

            if (textures.Length < 5)
            {
                type = AnimationStyle.Invalid;
                Logger.LogWarning(self.Name +
                                 " has been loaded as an animation entity, but doesn't contain enough textures");
            }

            type = textures.Length >= 14 ? AnimationStyle.Detailed : AnimationStyle.Basic;
        }

        private IEnumerator ShootTimer(float waitTimeMS)
        {
            shootStep = 0;
            while (true)
            {
                shootStep++;
                yield return new WaitForSeconds(waitTimeMS * 0.001f);
            }
        }

        private IEnumerator MoveTimer()
        {
            moveStep = 0;
            while (true)
            {
                var speed = 3f;
                if (self is ServerPlayer player)
                {
                    speed = Math.Max(player.GetSpeed() * 0.5f, 0);
                }

                moveStep++;
                yield return new WaitForSeconds(1f / speed);
            }
        }

        internal void OnAttack(Angle angle)
        {
            LastShotAngle = MathsUtils.ClampAngle(angle.Rad);
            if (self is ServerPlayer player)
            {
                ShotAnimationCooldown = player.GetShootCooldown(player.Inventory[0]) + 50f;
            }
            else
            {
                ShotThisTick = true;
                ShotAnimationCooldown = 333f;
            }
        }

        internal Sprite GetSprite()
        {
            if (type == AnimationStyle.Invalid)
            {
                return textures[0];
            }

            ShotAnimationCooldown -= Time.deltaTime * 1000;
            var state = ShotAnimationCooldown > 0 ? State.Shooting : !previousRender.Equals(self.Transform.localPosition) ? State.Moving : State.Still;

            if (currentShootTimer is not null)
            {
                if (ShotThisTick)
                {
                    shootStep = 0;
                }
                else if (state != State.Shooting)
                {
                    self.StopCoroutine(currentShootTimer);
                    currentShootTimer = null;
                }
            }

            if (state != State.Moving && currentMoveTimer is not null)
            {
                self.StopCoroutine(currentMoveTimer);
                currentMoveTimer = null;
            }

            var facing = Direction.Down;
            switch (state)
            {
                case State.Shooting:
                    if (currentShootTimer == null)
                    {
                        var shotAnimationSpeed = 333f;
                        if (self is ServerPlayer player)
                        {
                            shotAnimationSpeed = player.GetShootCooldown(player.Inventory[0]) / 2f;
                        }

                        currentShootTimer = self.StartCoroutine(ShootTimer(shotAnimationSpeed));
                    }

                    facing = AngleToDirection(LastShotAngle);
                    break;
                case State.Moving:
                    currentMoveTimer ??= self.StartCoroutine(MoveTimer());

                    facing = type == AnimationStyle.Detailed
                        ? self is Player.Player ? GetFacingFromInput() : GetFacingFromPositionHistory()
                        : self.Transform.localPosition.x < previousRender.x ? Direction.Left : Direction.Right;

                    break;
                case State.Still:
                    facing = lastFace;
                    break;
            }

            lastFace = facing;

            facing = AdjustFacing(facing);
            self.SpriteRenderer.flipX = facing == Direction.Left;
            previousRender = self.Transform.localPosition;
            ShotThisTick = false;
            switch (type)
            {
                case AnimationStyle.Basic:
                    switch (state)
                    {
                        case State.Shooting:
                            return textures[BASIC_SHOOTING[shootStep % BASIC_SHOOTING.Length]];
                        case State.Moving:
                            return textures[BASIC_MOVING[moveStep % BASIC_MOVING.Length]];
                        case State.Still:
                            return textures[BASIC_STILL[0]];
                    }

                    break;
                case AnimationStyle.Detailed:
                    switch (facing)
                    {
                        case Direction.Left:
                        case Direction.Right:
                            switch (state)
                            {
                                case State.Shooting:
                                    return textures[SHOOTING_SIDE[shootStep % SHOOTING_SIDE.Length]];
                                case State.Moving:
                                    return textures[MOVING_SIDE[moveStep % MOVING_SIDE.Length]];
                                case State.Still:
                                    return textures[STILL[0]];
                            }

                            break;
                        case Direction.Down:
                            switch (state)
                            {
                                case State.Shooting:
                                    return textures[SHOOTING_DOWN[shootStep % SHOOTING_DOWN.Length]];
                                case State.Moving:
                                    return textures[MOVING_DOWN[moveStep % MOVING_DOWN.Length]];
                                case State.Still:
                                    return textures[STILL[1]];
                            }

                            break;
                        case Direction.Up:
                            switch (state)
                            {
                                case State.Shooting:
                                    return textures[SHOOTING_UP[shootStep % SHOOTING_UP.Length]];
                                case State.Moving:
                                    return textures[MOVING_UP[moveStep % MOVING_UP.Length]];
                                case State.Still:
                                    return textures[STILL[2]];
                            }

                            break;
                    }

                    break;
            }

            return textures[STILL[0]];
        }

        private Direction GetFacingFromPositionHistory()
        {
            Direction facing;

            if (previousRender.x == self.Transform.localPosition.x && previousRender.y == self.Transform.localPosition.y)
            {
                return lastFace;
            }
            else
            {
                facing = Math.Round(Math.Abs(previousRender.x - self.Transform.localPosition.x), 1) >
                                          Math.Round(Math.Abs(previousRender.y - self.Transform.localPosition.y), 1)
                    ? self.Transform.localPosition.x < previousRender.x ? Direction.Left : Direction.Right
                    : self.Transform.localPosition.y > previousRender.y ? Direction.Up : Direction.Down;
            }

            return facing;
        }

        private Direction GetFacingFromInput()
        {
            Direction facing;
            var horizontal = UnityEngine.Input.GetAxisRaw("Horizontal");
            var vertical = UnityEngine.Input.GetAxisRaw("Vertical");

            if (horizontal == 0 && vertical == 0)
            {
                return lastFace;
            }

            facing = Math.Abs(horizontal) > Math.Abs(vertical)
                ? horizontal < 0 ? Direction.Left : Direction.Right
                : vertical > 0 ? Direction.Up : Direction.Down;

            return AdjustFacing2(facing);
        }

        private Direction AngleToDirection(float angle)
        {
            switch (type)
            {
                case AnimationStyle.Detailed:
                    if (angle is >= (7 * Mathf.PI / 4) or (>= 0 and <= (Mathf.PI / 4)))
                    {
                        return Direction.Right;
                    }

                    if (angle is >= (Mathf.PI / 4) and <= (3 * Mathf.PI / 4))
                    {
                        return Direction.Up;
                    }

                    if (angle is >= (3 * Mathf.PI / 4) and <= (5 * Mathf.PI / 4))
                    {
                        return Direction.Left;
                    }

                    return Direction.Down;
                case AnimationStyle.Basic:
                    if (LastShotAngle is >= (Mathf.PI / 2) and <= (3 * Mathf.PI / 2))
                    {
                        return Direction.Left;
                    }

                    return Direction.Right;
                default:
                    return Direction.Up;
            }
        }

        private static Direction AdjustFacing(Direction facingBeforeZ)
        {
            // Opposite
            if (Game.Map.Map.Rotation.Deg is >= 135 and <= 225)
            {
                switch (facingBeforeZ)
                {
                    case Direction.Down:
                        return Direction.Up;
                    case Direction.Up:
                        return Direction.Down;
                    case Direction.Left:
                        return Direction.Right;
                    case Direction.Right:
                        return Direction.Left;
                }
            }
            else if (Game.Map.Map.Rotation.Deg is >= 45 and <= 135)
            {
                switch (facingBeforeZ)
                {
                    case Direction.Down:
                        return Direction.Left;
                    case Direction.Up:
                        return Direction.Right;
                    case Direction.Left:
                        return Direction.Up;
                    case Direction.Right:
                        return Direction.Down;
                }
            }
            else if (Game.Map.Map.Rotation.Deg is >= 225 and <= 315)
            {
                switch (facingBeforeZ)
                {
                    case Direction.Down:
                        return Direction.Right;
                    case Direction.Up:
                        return Direction.Left;
                    case Direction.Left:
                        return Direction.Down;
                    case Direction.Right:
                        return Direction.Up;
                }
            }

            return facingBeforeZ;
        }

        private static Direction AdjustFacing2(Direction facingBeforeZ)
        {
            // Opposite
            if (Game.Map.Map.Rotation.Deg is >= 135 and <= 225)
            {
                switch (facingBeforeZ)
                {
                    case Direction.Down:
                        return Direction.Up;
                    case Direction.Up:
                        return Direction.Down;
                    case Direction.Left:
                        return Direction.Right;
                    case Direction.Right:
                        return Direction.Left;
                }
            }
            else if (Game.Map.Map.Rotation.Deg is >= 225 and <= 315)
            {
                switch (facingBeforeZ)
                {
                    case Direction.Down:
                        return Direction.Left;
                    case Direction.Up:
                        return Direction.Right;
                    case Direction.Left:
                        return Direction.Up;
                    case Direction.Right:
                        return Direction.Down;
                }
            }
            else if (Game.Map.Map.Rotation.Deg is >= 45 and <= 135)
            {
                switch (facingBeforeZ)
                {
                    case Direction.Down:
                        return Direction.Right;
                    case Direction.Up:
                        return Direction.Left;
                    case Direction.Left:
                        return Direction.Down;
                    case Direction.Right:
                        return Direction.Up;
                }
            }

            return facingBeforeZ;
        }

        private enum AnimationStyle
        {
            Detailed,
            Basic,
            Invalid,
        }

        private enum Direction
        {
            Up,
            Down,
            Left,
            Right,
        }

        private enum State
        {
            Moving,
            Shooting,
            Still,
        }
    }
}