﻿#nullable enable

namespace Client.Entity.Helpers
{
    internal sealed partial class EntityConditionHandler
    {
        private readonly struct ConditionalEffectData
        {
            internal readonly int? SpriteIndex;
            internal readonly bool Negative;

            public ConditionalEffectData(int? spriteIndex, bool negative)
            {
                SpriteIndex = spriteIndex;
                Negative = negative;
            }
        }
    }
}