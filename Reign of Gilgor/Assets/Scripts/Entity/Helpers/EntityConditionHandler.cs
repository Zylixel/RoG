using System.Collections.Generic;
using Client.Camera;
using Client.Entity.Notifications;
using Client.Game;
using Client.Particle;
using RoGCore.Models;
using UnityEngine;

#nullable enable

namespace Client.Entity.Helpers
{
    internal sealed partial class EntityConditionHandler
    {
        // Effect : { Sprite Index, Negative }
        private static readonly KeyValuePair<ConditionEffectIndex, ConditionalEffectData>[] ConditionalEffectDataLookup =
            new KeyValuePair<ConditionEffectIndex, ConditionalEffectData>[]
            {
                new KeyValuePair<ConditionEffectIndex, ConditionalEffectData>(ConditionEffectIndex.Recovering, new ConditionalEffectData(0, false)),
                new KeyValuePair<ConditionEffectIndex, ConditionalEffectData>(ConditionEffectIndex.Armored, new ConditionalEffectData(2, false)),
                new KeyValuePair<ConditionEffectIndex, ConditionalEffectData>(ConditionEffectIndex.Invincible, new ConditionalEffectData(1, false)),
                new KeyValuePair<ConditionEffectIndex, ConditionalEffectData>(ConditionEffectIndex.Invisible, new ConditionalEffectData(null, false)),
                new KeyValuePair<ConditionEffectIndex, ConditionalEffectData>(ConditionEffectIndex.Slowed, new ConditionalEffectData(3, true)),
                new KeyValuePair<ConditionEffectIndex, ConditionalEffectData>(ConditionEffectIndex.Bleeding, new ConditionalEffectData(4, true)),
                new KeyValuePair<ConditionEffectIndex, ConditionalEffectData>(ConditionEffectIndex.Confused, new ConditionalEffectData(19, true)),
                new KeyValuePair<ConditionEffectIndex, ConditionalEffectData>(ConditionEffectIndex.Dazed, new ConditionalEffectData(20, true)),
                new KeyValuePair<ConditionEffectIndex, ConditionalEffectData>(ConditionEffectIndex.Paralyzed, new ConditionalEffectData(24, true)),
                new KeyValuePair<ConditionEffectIndex, ConditionalEffectData>(ConditionEffectIndex.SpdBoost, new ConditionalEffectData(25, false)),
            };

        internal int ConditionEffects1;
        internal int ConditionEffects2;

        private static readonly int OutlineThicknessProperty = Shader.PropertyToID("OutlineThickness");

        private ConditionEffects conditionEffects;
        private readonly Dictionary<ConditionEffectIndex, GameObjectWithComponentHolder> objects = new Dictionary<ConditionEffectIndex, GameObjectWithComponentHolder>();
        private ParticleSystem? healParticle;
        private ParticleSystemRenderer? healParticleRenderer;

        private readonly SimpleEntity parent;

        internal EntityConditionHandler(SimpleEntity parent)
        {
            this.parent = parent;
        }

        internal void Dispose()
        {
            foreach (var effect in objects)
            {
                EntityPool.ReturnObject(effect.Value);
            }

            if (healParticle == null)
            {
                return;
            }

            ParticleFactory.ReturnParticle(healParticle);
        }

        internal bool HasConditionEffect(ConditionEffects eff) => (conditionEffects & eff) != 0;

        internal bool HasConditionEffect(ConditionEffectIndex eff) => (conditionEffects & (ConditionEffects)((ulong)1 << (int)eff)) != 0;

        internal void OnUpdate()
        {
            conditionEffects =
                (ConditionEffects)(ulong)(((long)ConditionEffects2 << 32) | (ConditionEffects1 & 0xFFFFFFFFL));
            foreach (var conditionalEffect in ConditionalEffectDataLookup)
            {
                if (!objects.ContainsKey(conditionalEffect.Key))
                {
                    if (!HasConditionEffect(conditionalEffect.Key) || conditionalEffect.Value.SpriteIndex is null)
                    {
                        continue;
                    }

                    var entity = EntityPool.LeaseObject(GameObjectArchetype.SpriteRenderer);

#if UNITY_EDITOR || DEVELOPMENT_BUILD
                    entity.GameObject.name = "ConditionalEffectIcon";
#endif
                    entity.GameObject.layer = LayerMask.NameToLayer("UI");

                    entity.GameObject.transform.SetParent(parent.SortingGroup.transform);
                    entity.GameObject.transform.localScale = new Vector3(1f / parent.Size.x, 1f / parent.Size.y, 1);
                    entity.GameObject.transform.eulerAngles = Game.Map.Map.Rotation.ToUnity3();

                    var sr = entity.ComponentHolder.SpriteRenderer;
                    sr.sharedMaterial = EmbeddedData.GameEntityMaterial;

                    var materialPropertyBlock = new MaterialPropertyBlock();
                    sr.GetPropertyBlock(materialPropertyBlock);
                    materialPropertyBlock.SetFloat(OutlineThicknessProperty, 0.5f);
                    sr.SetPropertyBlock(materialPropertyBlock);

                    sr.sprite = parent.Manager.GameData.GetSprite("ConditionalEffects", conditionalEffect.Value.SpriteIndex.Value);

                    objects.Add(conditionalEffect.Key, entity);
                    if (conditionalEffect.Value.Negative && parent is Character character)
                    {
                        EntityFactory.CreateNotification<Notification>(character, conditionalEffect.Key.ToString(), Color.red);

                        if (parent is Player.Player)
                        {
                            CameraShake.OnCritialHit();
                        }
                    }
                }
                else
                {
                    if (HasConditionEffect(conditionalEffect.Key))
                    {
                        continue;
                    }

                    objects[conditionalEffect.Key].GameObject.layer = 0;
                    EntityPool.ReturnObject(objects[conditionalEffect.Key]);

                    objects.Remove(conditionalEffect.Key);
                }
            }

            var i = 0;
            foreach (var effect in objects.Values)
            {
                effect.GameObject.transform.localScale = new Vector3(1f / parent.Size.x, 1f / parent.Size.y, 1);
                effect.GameObject.transform.localPosition = new Vector3(-((objects.Count - 1) * 0.15f) + (i * 0.3f), 1.3f);
                i++;
            }

            if (parent is not Player.Player)
            {
                return;
            }

            if (OptionsHandler.GetOptionValue<bool>("particleMaster"))
            {
                if (healParticle == null)
                {
                    if (HasConditionEffect(ConditionEffectIndex.Recovering))
                    {
                        healParticle = ParticleFactory.CreateParticle(
                            "HealParticle",
                            new CreateParticleProperties { Parent = parent.GameObject.transform });

                        healParticleRenderer = healParticle != null ? healParticle.GetComponent<ParticleSystemRenderer>() : null;
                    }
                }
                else
                {
                    if (!HasConditionEffect(ConditionEffectIndex.Recovering))
                    {
                        ParticleFactory.ReturnParticle(healParticle);
                        healParticle = null;
                        healParticleRenderer = null;
                    }
                    else if (healParticleRenderer != null)
                    {
                        healParticleRenderer.sortingOrder = parent.SpriteRenderer.sortingOrder - 1;
                    }
                }
            }
        }
    }
}