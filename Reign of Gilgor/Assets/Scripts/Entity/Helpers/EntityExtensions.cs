﻿using System.Collections;
using Client.Entity.Interfaces;
using RoGCore.Assets;
using RoGCore.Models;
using RoGCore.Utils;
using Structures;
using UnityEngine;
using Vector2 = System.Numerics.Vector2;

#nullable enable

namespace Client.Entity.Helpers
{
    internal static class EntityExtensions
    {
        internal static BgInfoResult GetItemInfo(this Player.Player player, IContainer container, byte index)
        {
            if (container.Inventory == null || index >= container.Inventory.Length || container.Inventory[index] == null)
            {
                return BgInfoResult.NoItem;
            }

            var item = container.Inventory[index]!.Value;

            if (!item.BaseItem.Usable)
            {
                return BgInfoResult.Unusable;
            }

            for (var i = 0; i < 4; i++)
            {
                if (player.ObjectDesc.SlotTypes?[i] == item.BaseItem.Type)
                {
                    return BgInfoResult.Valid;
                }
            }

            return BgInfoResult.Invalid;
        }

        internal static BgInfoResult GetItemInfo(this Player.Player player, in Item? item)
        {
            if (item is null)
            {
                return BgInfoResult.NoItem;
            }

            if (!item.Value.BaseItem.Usable)
            {
                return BgInfoResult.Unusable;
            }

            for (var i = 0; i < 4; i++)
            {
                if (player.ObjectDesc.SlotTypes?[i] == item.Value.BaseItem.Type)
                {
                    return BgInfoResult.Valid;
                }
            }

            return BgInfoResult.Invalid;
        }

        internal static bool AuditItem(this IContainer container, in Item? item, int slot) => item is null
|| container.ObjectDescProperty.SlotTypes == null
|| slot >= container.ObjectDescProperty.SlotTypes.Length
|| container.ObjectDescProperty.SlotTypes[slot] == "All"
|| item.Value.BaseItem.Type == container.ObjectDescProperty.SlotTypes[slot];

        internal static double DistSqr(this SimpleEntity a, float x, float y) => MathsUtils.DistSqr(a.Position.X, a.Position.Y, x, y);

        internal static double DistSqr(this SimpleEntity a, Vector2 pos) => MathsUtils.DistSqr(a.Position.X, a.Position.Y, pos.X, pos.Y);

        internal static double DistSqr(this SimpleEntity a, UnityEngine.Vector2 pos) => MathsUtils.DistSqr(a.Position.X, a.Position.Y, pos.x, pos.y);

        internal static double DistSqr(this SimpleEntity a, Transform transform) => MathsUtils.DistSqr(a.Transform.position.x, a.Transform.position.y, transform.position.x, transform.position.y);

        internal enum BgInfoResult
        {
            NoItem,
            Unusable,
            Valid,
            Invalid,
        }

        internal static IEnumerator SpriteCycle(this ISpriteEntity entity, ObjectDescription objectDescription, GameWorldManager manager)
        {
            if (objectDescription.Texture is null)
            {
                yield break;
            }

            var textureIndex = 0;
            var increasing = true;
            while (true)
            {
                switch (objectDescription.Texture.Type)
                {
                    case TextureData.TEXTURETYPERANDOMCYCLE:
                        textureIndex = manager.GlobalRandom.Next(0, objectDescription.Textures.Length);
                        break;
                    case TextureData.TEXTURETYPECYCLE:
                        textureIndex++;
                        textureIndex %= objectDescription.Textures.Length;
                        break;
                    case TextureData.TEXTURETYPEPINGPONG:
                        textureIndex += increasing ? 1 : -1;

                        if (increasing)
                        {
                            if (textureIndex == objectDescription.Textures.Length - 1)
                            {
                                increasing = false;
                            }
                        }
                        else if (textureIndex == 0)
                        {
                            increasing = true;
                        }

                        break;
                }

                entity.SetSideSpriteByIndex(textureIndex);

                yield return new WaitForSeconds(entity.SpriteCycleSpeed * 0.001f);
            }
        }
    }
}