﻿using Client.Entity.Interfaces;
using Client.Entity.Notifications;
using RoGCore.Assets;
using RoGCore.Models;
using RoGCore.Utils;
using UnityEngine;
using Vector2 = System.Numerics.Vector2;

namespace Client.Entity.Helpers
{
    internal static class EntityFactory
    {
        private static bool HasCanvas<T>()
            where T : SimpleEntity
            => typeof(T) != typeof(Decor);

        internal static T CreateEntity<T>(ObjectStats stats, ObjectDescription objectDesc, GameWorldManager manager, bool doOnUpdate = true)
            where T : SimpleEntity
        {
            var hasLight = manager.Map.Info.Lighting &&
                            !objectDesc.LightColor.IsNullOrWhiteSpace() && (objectDesc.LightProb <= 0 ||
                                                                            (objectDesc.LightProb > 0 &&
                                                                            manager.GlobalRandom.Next(0, 100) <
                                                                            objectDesc.LightProb));

            var archetype = GameObjectArchetype.SpriteRenderer;
            if (hasLight)
            {
                archetype |= GameObjectArchetype.Light;
            }

            if (HasCanvas<T>())
            {
                archetype |= GameObjectArchetype.Canvas;
            }

            var entity = EntityPool.LeaseObject(archetype);

            var behaviour = entity.GameObject.AddComponent<T>();
            entity.GameObject.layer = LayerMask.NameToLayer(archetype.HasFlag(GameObjectArchetype.Light) ? "ReflectionIgnore" : "Entity");

            behaviour.Init(stats, objectDesc, entity.ComponentHolder, manager, doOnUpdate);

            return behaviour;
        }

        internal static Projectile.Projectile CreateProjectile(Character owner, ushort id, CustomProjectile customDesc, Angle angle, long createTime, Vector2 startPos, Item? item, bool hitsPlayer)
        {
            var description = owner.Manager.GameData.Projectiles[customDesc.Name];

            var hasLight = owner.Manager.Map.Info.Lighting && !description.LightColor.IsNullOrWhiteSpace();

            var archetype = GameObjectArchetype.SpriteRenderer;
            if (hasLight)
            {
                archetype |= GameObjectArchetype.Light;
            }

            var entity = EntityPool.LeaseObject(archetype);

            var projectile = entity.GameObject.AddComponent<Projectile.Projectile>();
            entity.GameObject.layer = LayerMask.NameToLayer(archetype.HasFlag(GameObjectArchetype.Light) ? "ReflectionIgnore" : "Entity");

            projectile.Init(owner, id, customDesc, description, angle, createTime, startPos.ToUnity2(), item, hitsPlayer, entity.ComponentHolder);

            return projectile;
        }

        internal static T CreateNotification<T>(INotificationHolder notificationHolder, string text, Color color)
            where T : Notification
        {
            var entity = EntityPool.LeaseObject(GameObjectArchetype.TextMeshProUGUI);
            entity.GameObject.transform.SetParent(GameObject.Find("NotificationCanvas").transform);

            var behaviour = entity.GameObject.AddComponent<T>();
            entity.GameObject.layer = LayerMask.NameToLayer("UI");

            behaviour.Init(notificationHolder, entity.ComponentHolder, text, color);

            return behaviour;
        }

        internal static void DestroyEntity(MonoBehaviour behavior)
        {
            if (behavior is IPoolableObject poolableObject)
            {
                EntityPool.ReturnObject(new GameObjectWithComponentHolder(behavior.gameObject, poolableObject.ComponentHolder));
            }

            Object.Destroy(behavior);
        }
    }
}
