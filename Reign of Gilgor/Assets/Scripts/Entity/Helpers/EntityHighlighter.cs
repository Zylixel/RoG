﻿#nullable enable

using Client.Game;
using UnityEngine;

namespace Client.Entity.Helpers
{
    internal static class EntityHighlighter
    {
        internal static SimpleEntity? HightlightedEntity;

        private static Color hightlightedPreviousOutlineColor;

        internal static void HighlightEntity(Player.Player player, SimpleEntity entity)
        {
            if (HightlightedEntity == entity)
            {
                return;
            }

            UnHighlightEntities();

            HightlightedEntity = entity;
            hightlightedPreviousOutlineColor = entity.GetRendererColor(ShaderProperties.OutlineColor);

            entity.SetRendererColor(ShaderProperties.OutlineColor, Color.white * 1.5f);
            entity.Highlighted = true;
        }

        internal static void UnHighlightEntities()
        {
            if (HightlightedEntity == null)
            {
                return;
            }

            HightlightedEntity.SetRendererColor(ShaderProperties.OutlineColor, hightlightedPreviousOutlineColor);
            HightlightedEntity.Highlighted = false;
            HightlightedEntity = null;
        }
    }
}
