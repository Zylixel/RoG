﻿using UnityEngine;

namespace Client.Entity.Helpers
{
    internal class EntityMovement
    {
        private Vector3 lastRenderedPosition;
        private Vector3 serverPosition;
        private float setTime;

        internal void SetTargetPoint(Vector3 lastRenderedPosition, Vector3 serverPosition)
        {
            this.lastRenderedPosition = lastRenderedPosition;
            this.serverPosition = serverPosition;
            this.setTime = Time.unscaledTime;
        }

        internal Vector3 CurrentVisualPosition()
        {
            float lerpAmount = Mathf.Clamp01(Time.unscaledTime - setTime) * Networking.NewTickHandler.TPS * 0.5f;

            return Vector3.Lerp(lastRenderedPosition, serverPosition, lerpAmount);
        }
    }
}
