﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Client.Entity.Helpers
{
    internal sealed class EntityPool : Initializable<EntityPool>
    {
        private readonly Dictionary<GameObjectArchetype, List<GameObjectWithComponentHolder>> pools = new Dictionary<GameObjectArchetype, List<GameObjectWithComponentHolder>>();

        [SerializeField]
        private GameObject baseGameEntity;
        [SerializeField]
        private GameObject baseGameEntityWithLight;
        [SerializeField]
        private GameObject notification;

        private void AddObjectToPool(GameObjectWithComponentHolder gameObject)
        {
            gameObject.GameObject.SetActive(false);

            if (!pools.TryGetValue(gameObject.ComponentHolder.Archetype, out var pool))
            {
                pool = new List<GameObjectWithComponentHolder>();
                pools.Add(gameObject.ComponentHolder.Archetype, pool);
            }

            pool.Add(gameObject);
        }

        private GameObjectWithComponentHolder GenerateObject(GameObjectArchetype archetype)
        {
            GameObject obj;
            var componentHolder = new GameObjectComponentHolder(archetype);

            obj = archetype.HasFlag(GameObjectArchetype.SpriteRenderer)
                ? Instantiate(baseGameEntity)
                : archetype.HasFlag(GameObjectArchetype.TextMeshProUGUI)
                    ? Instantiate(notification)
                    : throw new ArgumentException($"Archetype not supported: {archetype}", nameof(archetype));

            componentHolder.Populate(obj);
            obj.transform.SetParent(gameObject.transform);

            return new GameObjectWithComponentHolder(obj, componentHolder);
        }

        internal static GameObjectWithComponentHolder LeaseObject(GameObjectArchetype archetype)
        {
            if (!Instance.pools.TryGetValue(archetype, out var pool) || pool.Count == 0)
            {
                return Instance.GenerateObject(archetype);
            }
            else
            {
                var ret = pool[0];
                pool.RemoveAt(0);
                ret.GameObject.SetActive(true);
                return ret;
            }
        }

        internal static void ReturnObject(GameObjectWithComponentHolder entity)
        {
            if (!Instance.isActiveAndEnabled)
            {
                return;
            }

            entity.ComponentHolder.SetToDefault(entity.GameObject);
            entity.GameObject.transform.SetParent(Instance.gameObject.transform);
            Instance.AddObjectToPool(entity);
        }
    }
}