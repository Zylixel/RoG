﻿using Client.UI;
using RoGCore.Models;

namespace Client.Entity.Helpers
{
    internal static class EntityStats
    {
        private const float MIN_ATTACK_FREQ = 0.0015f;
        private const float MAX_ATTACK_FREQ = 0.008f;

        internal static int GetStats(this ServerPlayer player, int id) => player.PlayerStats[id] + player.Boosts[id];

        internal static float GetAttackFrequency(this ServerPlayer player)
        {
            if (player.ConditionEffects.HasConditionEffect(ConditionEffects.Dazed))
            {
                return MIN_ATTACK_FREQ;
            }

            var rof = MIN_ATTACK_FREQ + (player.GetStats(7) / 75f * (MAX_ATTACK_FREQ - MIN_ATTACK_FREQ));
            /*if (player.HasConditionEffect(ConditionEffects.Berserk))
            {
                rof *= 1.5f;
            }

            if (player.ItemEffectReady(RawItem.ItemEffect.Warriors_Confidence))
            {
                rof *= 1 + (player.ItemEffects[RawItem.ItemEffect.Warriors_Confidence].strength * 0.025f * player.ItemEffects[RawItem.ItemEffect.Warriors_Confidence].amount);
            }*/
            return rof;
        }

        //todo: fix skills for serverplayers
        internal static float GetShootCooldown(this ServerPlayer player, in Item? item) => item is null ? 0 : 1 / player.GetAttackFrequency() * 1 / item.Value.BaseItem.GetRateOfFire(SkillTreeUI.ActiveSkills());

        internal static float GetSpeed(this ServerPlayer serverPlayer)
        {
            var ret = 6 + (serverPlayer.GetStats(4) / 14f);
            if (serverPlayer.ConditionEffects.HasConditionEffect(ConditionEffectIndex.Speedy))
            {
                ret *= 1.5f;
            }

            if (serverPlayer.ConditionEffects.HasConditionEffect(ConditionEffectIndex.Slowed))
            {
                ret /= 3f;
            }

            if (serverPlayer is Player.Player player && player.Sneaking)
            {
                ret *= 0.5f;
            }

            return serverPlayer.ConditionEffects.HasConditionEffect(ConditionEffectIndex.Paralyzed) ? 0 : ret;
        }
    }
}