﻿using UnityEngine;

namespace Client.Entity.Helpers
{
    internal class HighlightLine : MonoBehaviour
    {
        [SerializeField]
        private float speed = 1f;
        [Range(0f, 1f)]
        [SerializeField]
        private float size = 1f;
        [SerializeField]
        private int amount = 1;
        [SerializeField]
        private Material material;

        private GameObjectWithComponentHolder[] points;

        private void GeneratePoints()
        {
            if (points is not null)
            {
                RemovePoints();
            }

            points = new GameObjectWithComponentHolder[amount];
            for (var i = 0; i < points.Length; i++)
            {
                var point = EntityPool.LeaseObject(GameObjectArchetype.SpriteRenderer);

                point.ComponentHolder.SpriteRenderer.sortingLayerName = "Ground";
                point.ComponentHolder.SpriteRenderer.sortingOrder = 1;
                point.ComponentHolder.SpriteRenderer.sprite = GameWorldManager.Instance.GameData.GetSprite("Blank", 0);
                point.ComponentHolder.SpriteRenderer.sharedMaterial = material;

                point.GameObject.transform.SetParent(transform);
                point.GameObject.transform.localScale = Vector3.one * size;
                point.GameObject.layer = LayerMask.NameToLayer("ReflectionIgnore");

                points[i] = point;
            }
        }

        private void RemovePoints()
        {
            if (points is null)
            {
                return;
            }

            for (var i = 0; i < points.Length; i++)
            {
                EntityPool.ReturnObject(points[i]);
            }

            points = null;
        }

        private void OnValidate()
        {
            speed = Mathf.Max(0, speed);
            amount = Mathf.Max(1, amount);

            if (points is not null)
            {
                if (amount != points.Length)
                {
                    GeneratePoints();
                }

                for (var i = 0; i < points.Length; i++)
                {
                    if (points[i] is not null)
                    {
                        points[i].GameObject.transform.localScale = Vector3.one * size;
                    }
                }
            }
        }

        private void Update()
        {
            var player = GameWorldManager.Instance.Map.Player;
            if (EntityHighlighter.HightlightedEntity != null && player != null)
            {
                if (points is null)
                {
                    GeneratePoints();
                }

                for (var i = 0; i < points!.Length; i++)
                {
                    var point = points[i];

                    point.GameObject.transform.position = Vector3.Lerp(
                        player.SpriteRenderer.bounds.center,
                        EntityHighlighter.HightlightedEntity.SpriteRenderer.bounds.center,
                        ((float)i / points.Length) + (((Time.time * speed) % 1) / points.Length));
                }
            }
            else
            {
                RemovePoints();
            }
        }
    }
}
