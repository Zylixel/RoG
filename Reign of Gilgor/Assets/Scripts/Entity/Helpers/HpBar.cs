﻿using System.Linq;
using Client.Game;
using RoGCore.Models;
using UnityEngine;

namespace Client.Entity.Helpers
{
    internal class HpBar
    {
        private readonly Character owner;
        private readonly SpriteRenderer indicatorImage;
        private readonly SpriteRenderer outlineImage;

        internal HpBar(Character owner)
        {
            this.owner = owner;
            var gameObject = Object.Instantiate(GamePrefabs.HpOutline);
            gameObject.transform.SetParent(owner.SortingGroup.transform);
            gameObject.transform.localEulerAngles = Vector3.zero;
            gameObject.transform.localPosition = new Vector3(0f, -0.25f);
            outlineImage = gameObject.GetComponent<SpriteRenderer>();
            outlineImage.sprite = owner.Manager.GameData.GetSprite("HpOutline", 0);

            var materialPropertyBlock = new MaterialPropertyBlock();
            outlineImage.GetPropertyBlock(materialPropertyBlock);
            materialPropertyBlock.SetFloat(SimpleEntity.ShadowThicknessHash, 0.5f);
            outlineImage.SetPropertyBlock(materialPropertyBlock);

            indicatorImage = gameObject.GetComponentsInChildren<SpriteRenderer>().First(x => x.gameObject.name == "HpInside");
            indicatorImage.sprite = owner.Manager.GameData.GetSprite("HpInside", 0);

            OptionsHandler.SubscribeToOptionsChange<bool>("drawHPBars", DrawOptionChanged, true);
        }

        private void DrawOptionChanged(object sender, bool e) => Update();

        internal void Update()
        {
            var enabled = owner.MaxHP != 0 && OptionsHandler.GetOptionValue<bool>("drawHPBars") &&
                           !owner.ConditionEffects.HasConditionEffect(ConditionEffectIndex.Invincible);

            outlineImage.gameObject.SetActive(enabled);

            if (!enabled)
            {
                return;
            }

            outlineImage.transform.localScale = new Vector3(1f / owner.Size.x, 1f / owner.Size.y, 1);

            var percentageFull = (float)owner.HP / owner.MaxHP;
            indicatorImage.color = GetHealthColor(percentageFull);
            outlineImage.color = GetOutlineColor(percentageFull);

            indicatorImage.transform.localScale = new Vector3(percentageFull, 1, 1);
        }

        private static Color GetOutlineColor(float percentageFull) => percentageFull >= .5
                ? Color.Lerp(GameColors.HP_OUT_GREEN, GameColors.HP_OUT_ORANGE, (1 - percentageFull) * 2)
                : percentageFull >= .25
                ? Color.Lerp(GameColors.HP_OUT_ORANGE, GameColors.HP_OUT_RED, (0.5f - percentageFull) * 2)
                : GameColors.HP_OUT_RED;

        internal static Color GetHealthColor(float percentageFull) => percentageFull >= .5
                ? Color.Lerp(GameColors.HP_GREEN, GameColors.HP_ORANGE, (1 - percentageFull) * 2)
                : percentageFull >= .25 ? Color.Lerp(GameColors.HP_ORANGE, GameColors.HP_RED, (0.5f - percentageFull) * 2) : GameColors.HP_RED;

        internal void Dispose()
        {
            OptionsHandler.UnsubscribeToOptionsChange<bool>("drawHPBars", DrawOptionChanged);
            Object.Destroy(outlineImage.gameObject);
        }
    }
}