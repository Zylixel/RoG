using System.Collections;
using Client.Game;
using UnityEngine;

namespace Client.Entity
{
    internal partial class SimpleEntity
    {
        internal bool Highlighted;
        private Color underlyingFlashColor;
        private float underlyingFlashRate;

        internal void StartFlash(Color color, float rate, float ms = 0)
        {
            StopCoroutine(nameof(DisableFlash));
            underlyingFlashRate = rate;
            underlyingFlashColor = color;

            if (ms > 0)
            {
                StartCoroutine(nameof(DisableFlash), ms);
            }
        }

        internal IEnumerator DisableFlash(float ms)
        {
            yield return new WaitForSeconds(ms * 0.001f);
            underlyingFlashRate = 0;
            underlyingFlashColor = Color.clear;
        }

        private void UpdateFlashRate()
        {
            if (this is ServerPlayer && HP / (float)MaxHP <= 0.2f)
            {
                SetRendererFloat(ShaderProperties.FlashRate, 12);
                SetRendererColor(ShaderProperties.FlashColor, Color.red * 5);
            }
            else if (Highlighted)
            {
                SetRendererColor(ShaderProperties.FlashColor, Color.white * 1.1f);
                SetRendererFloat(ShaderProperties.FlashRate, 10f);
            }
            else
            {
                SetRendererColor(ShaderProperties.FlashColor, underlyingFlashColor);
                SetRendererFloat(ShaderProperties.FlashRate, underlyingFlashRate);
            }
        }
    }
}