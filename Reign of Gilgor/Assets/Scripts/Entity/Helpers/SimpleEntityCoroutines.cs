﻿using UnityEngine;

namespace Client.Entity
{
    internal partial class SimpleEntity
    {
        public virtual void SetSideSpriteByIndex(int textureCycleIndex) => SpriteRenderer.sprite = ObjectDesc.Textures[textureCycleIndex];
    }
}
