﻿using RoGCore.Models;
using RoGCore.Utils;
using UnityEngine;

namespace Entity.Helpers
{
    internal struct SortingOrder
    {
        private const int Increase = 32767 / 2;
        private const int Precision = 5;
        private const int UpdateDistance = 16 * 16;

        private Vector2 playerRenderedPosition;
        private Vector3 playerRenderedPositionThree;
        private int xMask;
        private int yMask;
        private bool inXQuadrant;

        internal void Update(Vector2 playerPosition, Angle angle)
        {
            if (MathsUtils.DistSqr(playerPosition.x, playerPosition.y, playerRenderedPosition.x, playerRenderedPosition.y) > UpdateDistance)
            {
                playerRenderedPosition = playerPosition;
                playerRenderedPositionThree = playerPosition;
            }

            var ang = (int)(MathsUtils.ClampAngle(angle.Rad) * Mathf.Rad2Deg);
            var quadrant = (((ang + 45) / 90) % 4) + 1;
            var subQuadrant = ((ang / 90) % 4) + 1;

            inXQuadrant = quadrant is 2 or 4;

            yMask = inXQuadrant
                ? subQuadrant is 4 or 1 ? Precision : -Precision
                : subQuadrant is 1 or 2 ? Precision : -Precision;

            xMask = quadrant is 2 or 3 ? -(Precision * 100) : Precision * 100;
        }

        /*internal int Calculate(Vector2 pos)
        {
           // Down
           3 => -(int) (adjusted.Y* 100f -
           SortingOrderOffset.X* (Map.Rotation.Sin > 0 ? -adjusted.X : adjusted.X)),
           // Left
           2 => -(int) (adjusted.X* 100f -
           SortingOrderOffset.Y* (Map.Rotation.Cos > 0 ? adjusted.Y : -adjusted.Y)),
           // Right
           4 => (int) (adjusted.X* 100f -
           SortingOrderOffset.Y* (Map.Rotation.Cos > 0 ? -adjusted.Y : adjusted.Y)),
           // Forward
           _ => (int) (adjusted.Y* 100f - SortingOrderOffset.X* (Map.Rotation.Sin > 0 ? adjusted.X : -adjusted.X))
            Vector2 adjusted = playerRenderedPositionOffset - pos;
            return quadrant switch
            {
                3 => -(int) (adjusted.Y * 100f - SortingOrderOffset.X * (subQuadrantMask ? -adjusted.X : adjusted.X)),
                2 => -(int) (adjusted.X * 100f - SortingOrderOffset.Y * (subQuadrantMask ? adjusted.Y : -adjusted.Y)),
                4 => (int) (adjusted.X * 100f - SortingOrderOffset.Y * (subQuadrantMask ? -adjusted.Y : adjusted.Y)),
                _ => (int) (adjusted.Y * 100f - SortingOrderOffset.X * (subQuadrantMask ? adjusted.X : -adjusted.X))
            };
        }*/

        internal int Calculate(Vector2 pos)
        {
            var adjusted = playerRenderedPosition - pos;

            return inXQuadrant
                ? (int)((adjusted.x * xMask) + (adjusted.y * yMask)) + Increase
                : (int)((adjusted.y * xMask) - (adjusted.x * yMask)) + Increase;
        }

        internal int Calculate(System.Numerics.Vector2 pos)
        {
            var adjusted = new Vector2(playerRenderedPosition.x - pos.X, playerRenderedPosition.y - pos.Y);

            return inXQuadrant
                ? (int)((adjusted.x * xMask) + (adjusted.y * yMask)) + Increase
                : (int)((adjusted.y * xMask) - (adjusted.x * yMask)) + Increase;
        }

        internal int Calculate(Vector3 pos)
        {
            var adjusted = playerRenderedPositionThree - pos;

            return inXQuadrant
                ? (int)((adjusted.x * xMask) + (adjusted.y * yMask)) + Increase
                : (int)((adjusted.y * xMask) - (adjusted.x * yMask)) + Increase;
        }
    }
}
