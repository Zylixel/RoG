﻿using System.Collections;
using Client.Game;
using UnityEngine;

namespace Client.Entity
{
    internal sealed class TintEntityHandler
    {
        private readonly Character self;

        private Coroutine hitTimerStorage;
        private bool hitting;
        private int lastUpdate;

        private bool transitionAnimationActive;

        private int updateCount;

        internal TintEntityHandler(Character owner)
        {
            self = owner;
        }

        internal void Tick()
        {
            if (updateCount == lastUpdate)
            {
                return;
            }

            if (transitionAnimationActive)
            {
                self.SetRendererColor(ShaderProperties.Tint, Color.white * 2);
            }
            else if (hitting)
            {
                self.SetRendererColor(ShaderProperties.Tint, Color.red * 2);
            }
            else
            {
                self.SetRendererColor(ShaderProperties.Tint, Color.clear);
            }

            lastUpdate = updateCount;
        }

        internal void Transitioning(bool value)
        {
            transitionAnimationActive = value;
            updateCount++;
        }

        internal void Hit()
        {
            hitting = true;
            updateCount++;
            if (hitTimerStorage is not null)
            {
                self.StopCoroutine(hitTimerStorage);
            }

            hitTimerStorage = self.StartCoroutine(HitTimer());
        }

        private IEnumerator HitTimer()
        {
            yield return new WaitForSeconds(0.100f);
            hitting = false;
            updateCount++;
        }
    }
}