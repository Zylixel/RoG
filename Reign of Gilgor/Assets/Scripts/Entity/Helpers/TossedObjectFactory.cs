﻿using System;
using RoGCore.Assets;
using RoGCore.Utils;
using UnityEngine;

namespace Client.Entity.Helpers
{
    internal static class TossedObjectFactory
    {
        internal static TossedObject CreateTossedObject(ObjectDescription desc, EntityBehaviour sender, Vector2 endPosition, float tossTime)
        {
            var tossedObject = UnityEngine.Object.Instantiate(GamePrefabs.TossedObject);
            var behaviour = desc.Class switch
            {
                "BombObject" => tossedObject.AddComponent<BombObject>(),
                "Enemy" => tossedObject.AddComponent<TossedObject>(),
                _ => throw new ArgumentException(desc.Class),
            };

            behaviour.Initialize(sender.Manager, sender.Position.ToUnity2(), endPosition, desc, tossTime);
            behaviour.Toss();

            return behaviour;
        }
    }
}
