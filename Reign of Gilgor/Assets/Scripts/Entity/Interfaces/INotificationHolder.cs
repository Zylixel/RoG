﻿using UnityEngine;

namespace Client.Entity.Interfaces
{
    internal interface INotificationHolder
    {
        Vector3 SizeProperty { get; }

        Canvas Canvas { get; }

        MonoBehaviour Behaviour { get; }
    }
}