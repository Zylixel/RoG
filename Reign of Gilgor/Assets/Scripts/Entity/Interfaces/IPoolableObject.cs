﻿namespace Client.Entity.Interfaces
{
    internal interface IPoolableObject
    {
        public GameObjectComponentHolder ComponentHolder { get; }
    }
}
