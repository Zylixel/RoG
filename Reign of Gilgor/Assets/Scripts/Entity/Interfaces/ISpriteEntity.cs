﻿namespace Client.Entity.Interfaces
{
    internal interface ISpriteEntity
    {
        float SpriteCycleSpeed { get; }

        void SetSideSpriteByIndex(int index);
    }
}
