﻿using Client.Entity.Interfaces;
using UnityEngine;

namespace Client.Entity.Notifications
{
    internal sealed class DamageNotification : Notification
    {
        internal int TotalDamage;

        internal void UpdateText(int damageAmount)
        {
            TotalDamage += damageAmount;
            UpdateText("-" + TotalDamage);
        }

        internal override void Init(INotificationHolder entity, GameObjectComponentHolder componentHolder, string damage, Color color)
        {
            base.Init(entity, componentHolder, "-" + damage, color);
            TotalDamage = int.Parse(damage);
        }
    }
}
