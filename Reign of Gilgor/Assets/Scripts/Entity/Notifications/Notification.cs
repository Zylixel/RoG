﻿using System;
using System.Collections;
using Client.Entity.Helpers;
using Client.Entity.Interfaces;
using Client.UI.TitleScreen;
using Client.Utils;
using UnityEngine;

namespace Client.Entity.Notifications
{
    internal class Notification : MonoBehaviour, IPoolableObject
    {
        internal DateTime StackChangeTime;
        internal bool Stackable;
        internal INotificationHolder Holder;

        internal virtual void Init(INotificationHolder holder, GameObjectComponentHolder componentHolder, string text, Color color)
        {
            this.Holder = holder;
            ComponentHolder = componentHolder;

            ComponentHolder.TextMeshProUGUI.color = color;
            ComponentHolder.TextMeshProUGUI.transform.position = holder.Behaviour.transform.position + new Vector3(0, holder.SizeProperty.x * 0.5f);
            ComponentHolder.TextMeshProUGUI.transform.eulerAngles = Game.Map.Map.Rotation.ToUnity3();
            ComponentHolder.TextMeshProUGUI.text = text;

            StackChangeTime = DateTime.Now;
            Stackable = true;

            StartCoroutine(Animate());
        }

        internal void UpdateText(string newText)
        {
            ComponentHolder.TextMeshProUGUI.text = newText;
            StackChangeTime = DateTime.Now;
        }

        private enum AnimateTextStage : byte
        {
            Appearing,
            Appeared,
            Disappearing,
            Destroy,
        }

        private Vector3 holderPosition;

        private Vector3 LastHolderPosition
        {
            get
            {
                if (Holder.Behaviour != null)
                {
                    holderPosition = Holder.Behaviour.transform.position;
                }

                return holderPosition;
            }
        }

        public GameObjectComponentHolder ComponentHolder { get; private set; }

        internal IEnumerator Animate()
        {
            var stage = AnimateTextStage.Appearing;
            var velocity = new Vector3(UnityEngine.Random.Range(-0.3f, 0.3f), 1f, 0);
            var yOffset = new Vector3(0, Holder.SizeProperty.y * 0.5f, 0);
            float time = 0;

            while (true)
            {
                yield return null;

                var speedScale = Holder.Behaviour != null ? 1f : 5f;

                time += Time.deltaTime * speedScale;

                var mapRotation = Quaternion.Euler(0, 0, Game.Map.Map.Rotation.Deg);
                var totalDisplacement = velocity * time;

                transform.position = LastHolderPosition + (mapRotation * (yOffset + totalDisplacement));
                transform.eulerAngles = Game.Map.Map.Rotation.ToUnity3();

                var textScale = GetTextScale((DateTime.Now - StackChangeTime).TotalMilliseconds);

                switch (stage)
                {
                    case AnimateTextStage.Appearing:
                        ComponentHolder.TextMeshProUGUI.ChangeUIAlpha(5, false);
                        AnimateNotificationTextScale(ComponentHolder.TextMeshProUGUI.transform, new Vector3(textScale, textScale, 1));

                        if (ComponentHolder.TextMeshProUGUI.color.a == 1)
                        {
                            stage = AnimateTextStage.Appeared;
                        }

                        break;
                    case AnimateTextStage.Appeared:
                        AnimateNotificationTextScale(transform, new Vector3(textScale, textScale, 1));

                        if (time > 1.75f)
                        {
                            stage = AnimateTextStage.Disappearing;
                            Stackable = false;
                        }

                        break;
                    case AnimateTextStage.Disappearing:
                        ComponentHolder.TextMeshProUGUI.ChangeUIAlpha(-5, false);
                        AnimateNotificationTextScale(transform, Vector3.zero);

                        if (ComponentHolder.TextMeshProUGUI.color.a == 0)
                        {
                            stage = AnimateTextStage.Destroy;
                        }

                        break;
                    case AnimateTextStage.Destroy:
                        EntityFactory.DestroyEntity(this);
                        yield break;
                }
            }
        }

        private static void AnimateNotificationTextScale(Transform child, Vector3 desiredPosition)
        {
            var diff = Time.deltaTime * TitleScreenController.AnimationSpeed * (desiredPosition - new Vector3(child.localScale.x, child.localScale.y, 0));
            child.localScale += diff;
        }

        private static float GetTextScale(double timeSinceHit) => timeSinceHit < 100f ? (float)(1 + (0.03 * timeSinceHit) - (0.0003 * (timeSinceHit * timeSinceHit))) : 1;
    }
}