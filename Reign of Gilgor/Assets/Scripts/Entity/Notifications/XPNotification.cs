﻿using Client.Entity.Interfaces;
using UnityEngine;

namespace Client.Entity.Notifications
{
    internal sealed class XPNotification : Notification
    {
        internal int TotalXP;

        internal void UpdateText(int xpAmount)
        {
            TotalXP += xpAmount;
            UpdateText(TotalXP + " xp");
        }

        internal override void Init(INotificationHolder entity, GameObjectComponentHolder componentHolder, string xp, Color color)
        {
            base.Init(entity, componentHolder, xp + " xp", color);
            TotalXP = int.Parse(xp);
        }
    }
}
