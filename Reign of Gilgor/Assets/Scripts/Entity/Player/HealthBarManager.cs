﻿using Client.Entity.Helpers;
using Client.Game;
using Client.Utils;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Client.Entity.Player
{
    internal class HealthBarManager : Initializable<HealthBarManager>
    {
        [SerializeField]
        private Image indicator;
        [SerializeField]
        private TextMeshProUGUI healthText;

        private const float FullHealthAmountX = 560f;
        private const float FullHealthAmountY = 44f;

        private EntityBehaviour player;

        public override void Initialize()
        {
            Instance = this;
            OptionsHandler.SubscribeToOptionsChange<bool>("drawStatText", (_, e) => SetActive(e), true);
        }

        internal static void OnUpdate(EntityBehaviour stats)
        {
            Instance.player = stats;
            Instance.healthText.SetText($"{Instance.player.HP}/{Instance.player.MaxHP}");
        }

        private void Update()
        {
            if (player == null)
            {
                return;
            }

            var percentageFull = (float)player.HP / player.MaxHP;

            indicator.color = HpBar.GetHealthColor(percentageFull);

            indicator.AnimateSizeDelta(new Vector2(percentageFull * FullHealthAmountX, FullHealthAmountY));
        }

        private void SetActive(bool value) => healthText.gameObject.SetActive(value);
    }
}