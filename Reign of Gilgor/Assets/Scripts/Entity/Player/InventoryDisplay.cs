﻿using Client.Game;
using Client.UI.Dialog;
using Client.UI.Item;
using RoGCore.Models.ObjectSlot;
using RoGCore.Networking.Packets;
using Structures;
using UnityEngine;
using UnityEngine.UI;
using static Client.Entity.Helpers.EntityExtensions;

namespace Client.Entity.Player
{
    [RequireComponent(typeof(CanvasGroup))]
    internal sealed class InventoryDisplay : BaseDialog<InventoryDisplay>
    {
        [SerializeField]
        private GameObject keybindTip;
        [SerializeField]
        private Button closeButton;

        internal const int Inventory_Size = 28;

        private readonly ItemHolder[] items = new ItemHolder[Inventory_Size];

        private Player owner;

        public override void Initialize()
        {
            base.Initialize();

            closeButton.onClick.AddListener(() => Close());
            InputMaster.SubscribeToKeyPress(OptionsHandler.GetKeyCodeOption("invToggleKey"), (_, __) => Toggle());

            for (byte i = 0; i < Inventory_Size; i++)
            {
                var newBackground = Instantiate(GamePrefabs.ItemHolder, transform).GetComponent<ItemHolder>();
                newBackground.Index = (byte)(i + 4);
                newBackground.transform.localPosition = new Vector3(-275 + ((i % 7) * 105), 140 + (Mathf.FloorToInt(i / 7f) * -105));
                newBackground.SetSize(100);

                items[i] = newBackground;
            }
        }

        internal static void OnUpdate(Player owner)
        {
            Instance.owner = owner;
            Instance.UpdateItems(owner);
        }

        private void UpdateItems(IContainer container)
        {
            for (var i = 0; i < Inventory_Size; i++)
            {
                items[i].Set(container.Inventory[i + 4], owner.Manager);
            }
        }

        internal static void Display() => Instance.Open();

        internal override void Open()
        {
            Instance.owner.EquipmentDisplay.InventoryDisplayOpen();
            base.Open();
        }

        internal override void Close()
        {
            Instance.owner.EquipmentDisplay.InventoryDisplayClose();
            base.Close();
        }

        internal static Transform GetTransform() => Instance.gameObject.transform;

        private void OnDisable() => HeldItemHandler.Dispatch();

        private void Update()
        {
            keybindTip.SetActive(false);

            foreach (var bg in ItemHolder.GetItemsInRaycastResults(owner.Manager.Map.CurrentRaycast, items))
            {
                if (!HeldItemHandler.HasItem())
                {
                    if (bg.Item == null)
                    {
                        continue;
                    }

                    keybindTip.SetActive(true);
                    if (UnityEngine.Input.GetKeyDown(KeyCode.X))
                    {
                        GameWorldManager.CurrentOutgoingNetwork.OutgoingMessage(new InventoryMove(
                            new ContainerSlot(owner.Id, bg.Index),
                            new GroundSlot()));
                    }

                    if (UnityEngine.Input.GetKey(KeyCode.C))
                    {
                        GameWorldManager.CurrentOutgoingNetwork.OutgoingMessage(new InventoryMove(
                            new ContainerSlot(owner.Id, bg.Index),
                            new VaultSlot()));
                    }

                    if (UnityEngine.Input.GetMouseButtonDown(2))
                    {
                        GameWorldManager.CurrentOutgoingNetwork.OutgoingMessage(new UseItem(
                            new ContainerSlot(owner.Id, bg.Index),
                            System.Numerics.Vector2.Zero));
                    }
                }

                if (UnityEngine.Input.GetMouseButtonDown(0))
                {
                    if (!HeldItemHandler.HasItem())
                    {
                        if (bg.Sprite != null)
                        {
                            HeldItemHandler.SetItem(owner, bg.Index, bg.Sprite);
                        }
                    }
                    else if (owner.AuditItem(HeldItemHandler.GetItem(), bg.Index) &&
                             ((HeldItemHandler.HeldItemData)HeldItemHandler.Data).Container.AuditItem(
                                 bg.Item, ((HeldItemHandler.HeldItemData)HeldItemHandler.Data).SlotId))
                    {

                        GameWorldManager.CurrentOutgoingNetwork.OutgoingMessage(new InventoryMove(
                            HeldItemHandler.Dispatch(),
                            new ContainerSlot(owner.Id, bg.Index)));
                    }
                }
            }
        }
    }
}