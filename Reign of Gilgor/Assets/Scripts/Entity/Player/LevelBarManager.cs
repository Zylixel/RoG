﻿using Client.Utils;
using TMPro;
using UnityEngine;

namespace Client.Entity.Player
{
    // TODO: REMOVE
    public class LevelBarManager : Initializable<LevelBarManager>
    {
        private static readonly Vector2 FullLevelAmount = new Vector2(230f, 20f);

        [SerializeField]
        private GameObject indicator;
        [SerializeField]
        private TextMeshProUGUI levelText;

        private Player player;

        public override void Initialize() => Instance = this;

        private void Update()
        {
            if (player == null)
            {
                return;
            }

            var percentageFull = 0;

            indicator.AnimateSizeDelta(new Vector2(percentageFull * FullLevelAmount.x, FullLevelAmount.y));
        }

        internal static void OnUpdate(Player stats)
        {
            Instance.player = stats;
            Instance.levelText.text = "Level 0";
        }
    }
}