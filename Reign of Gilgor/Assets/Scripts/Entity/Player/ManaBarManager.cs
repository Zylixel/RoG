﻿using Client.Game;
using Client.Utils;
using TMPro;
using UnityEngine;

namespace Client.Entity.Player
{
    public class ManaBarManager : Initializable<ManaBarManager>
    {
        private static readonly Vector2 FullManaAmount = new Vector2(230f, 20f);

        [SerializeField]
        private GameObject indicator;
        [SerializeField]
        private TextMeshProUGUI manaText;
        private Player player;

        public override void Initialize()
        {
            Instance = this;
            OptionsHandler.SubscribeToOptionsChange<bool>("drawStatText", (_, e) => SetActive(e), true);
        }

        internal static void OnUpdate(Player stats)
        {
            Instance.player = stats;
            Instance.manaText.text = $"{stats.Mp}/{stats.MaxMp}";
        }

        private void SetActive(bool value) => manaText.gameObject.SetActive(value);

        private void Update()
        {
            if (player == null)
            {
                return;
            }

            var percentageFull = (float)player.Mp / player.MaxMp;

            indicator.AnimateSizeDelta(new Vector2(percentageFull * FullManaAmount.x, FullManaAmount.y));
        }
    }
}