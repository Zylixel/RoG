﻿using System;
using System.Collections.Generic;
using System.Linq;
using Client.Camera;
using Client.Entity.Helpers;
using Client.Entity.Notifications;
using Client.Game;
using Client.Networking.API;
using Client.UI;
using Client.UI.Sidebar;
using Client.UI.Vault;
using Entity.Helpers;
using RoGCore.Assets;
using RoGCore.Models;
using RoGCore.Models.ObjectSlot;
using RoGCore.Networking.Packets;
using RoGCore.SettingsModels;
using RoGCore.Utils;
using UnityEngine;
using Vector2 = System.Numerics.Vector2;

#nullable enable

namespace Client.Entity.Player
{
    internal sealed class Player : ServerPlayer
    {
        [System.Diagnostics.CodeAnalysis.AllowNull]
        internal EquipmentDisplay EquipmentDisplay;

        internal UnityEngine.Vector2 LastPacket = UnityEngine.Vector2.zero;

        private ushort nextBulletId;
        private float shotCooldown;
        private float abilityCooldown;
        private XPNotification? xpNotification;
        private HashSet<Wall> hiddenWalls = new HashSet<Wall>();

        internal override void Init(in ObjectStats stats, ObjectDescription objectDesc, GameObjectComponentHolder componentHolder, GameWorldManager manager, bool doOnUpdate)
        {
            base.Init(stats, objectDesc, componentHolder, manager, false);

            EquipmentDisplay = new EquipmentDisplay(this);
            OnUpdate(Stats);
            Transform.localPosition = Position.ToUnity2();
            GameWorldManager.PlayerCreated.Invoke(this);

            InputMaster.SubscribeToKeyPress(OptionsHandler.GetKeyCodeOption("escapeKey"), Escape);
            InputMaster.SubscribeToKeyPress(KeyCode.Space, UseAbility);
            InputMaster.SubscribeToKeyPress(OptionsHandler.GetKeyCodeOption("summonKey"), SetSummonPosition);
            InputMaster.SubscribeToKeyPress(OptionsHandler.GetKeyCodeOption("sneakKey"), ToggleSneak);
        }

        internal int Mp { get; private set; }

        internal int MaxMp { get; private set; }

        internal int Silver { get; private set; }

        internal int BigColor { get; private set; }

        internal int SmallColor { get; private set; }

        internal bool IgnoreGroundSpeed { get; private set; }

        internal bool Sneaking { get; private set; }

        // Server will only tell player where they are on first update
        // All updates after will update their position but will not be rendered into visuals
        internal override void OnUpdate(in ObjectStats stat)
        {
            base.OnUpdate(stat);
            for (var i = 0; i < stat.Stats.Length; i++)
            {
                var data = stat.Stats[i].Value;
                switch (stat.Stats[i].Key)
                {
                    case StatsType.Mp:
                        {
                            Mp = (int)data;
                            break;
                        }

                    case StatsType.MaximumMp:
                        {
                            MaxMp = (int)data;
                            break;
                        }

                    case StatsType.Silver:
                        {
                            Manager.SilverDisplay.SetAmount((int)data, true);
                            Silver = (int)data;
                            break;
                        }

                    case StatsType.Texture1:
                        {
                            BigColor = (int)data;
                            break;
                        }

                    case StatsType.Texture2:
                        {
                            SmallColor = (int)data;
                            break;
                        }
                }
            }

            EquipmentDisplay.OnUpdate();
            InventoryDisplay.OnUpdate(this);
            VaultDisplay.OnUpdate(this);
            HealthBarManager.OnUpdate(this);
            ManaBarManager.OnUpdate(this);
            LevelBarManager.OnUpdate(this);
            PostEffects.OnUpdate(this);
            Manager.GuildDisplay.Set(GuildName, GuildRank);
            IgnoreGroundSpeed = Inventory[3]?.BaseItem.Name == "Ocean Artifact";

            // TODO: Reinvision how this is displayed.
            // BossStatusBar.CurrentBoss = quest;
        }

        internal void OnMove(in Move packet)
        {
            Transform.localPosition = Position.ToUnity2();
            GameObject.transform.position = Transform.localPosition;
        }

        internal bool InAbilityCooldown() => abilityCooldown > 0;

        protected override void Update()
        {
            this.UpdateMovement();
            EquipmentDisplay.Tick();

            HideNearbyWalls();

            shotCooldown -= Time.deltaTime * 1000f;
            abilityCooldown -= Time.deltaTime * 1000f;

            TryShoot();

            /*if (Manager.Map.Info.Name == "Gilgor's Realm")
            {
                GroundDescription tile = Manager.Map.GetTile(transform.localPosition);

                switch (tile.Name)
                {
                    case "Beach Sand":
                    case "Not Flowing Sand":
                        AudioHandler.musicVolumes[0] = Mathf.Clamp(AudioHandler.musicVolumes[0] + Time.deltaTime / 3f, 0, 1);
                        AudioHandler.musicVolumes[1] = Mathf.Clamp(AudioHandler.musicVolumes[1] - Time.deltaTime / 3f, 0, 1);
                        AudioHandler.musicVolumes[2] = Mathf.Clamp(AudioHandler.musicVolumes[2] - Time.deltaTime / 3f, 0, 1);
                        AudioHandler.musicVolumes[3] = Mathf.Clamp(AudioHandler.musicVolumes[3] - Time.deltaTime / 3f, 0, 1);
                        AudioHandler.UpdateVolume();
                        break;
                    case "Jungle Grass":
                    case "Forest Grass":
                    case "Swamp Grass":
                        AudioHandler.musicVolumes[0] = Mathf.Clamp(AudioHandler.musicVolumes[0] - Time.deltaTime / 3f, 0, 1);
                        AudioHandler.musicVolumes[1] = Mathf.Clamp(AudioHandler.musicVolumes[1] + Time.deltaTime / 3f, 0, 1);
                        AudioHandler.musicVolumes[2] = Mathf.Clamp(AudioHandler.musicVolumes[2] - Time.deltaTime / 3f, 0, 1);
                        AudioHandler.musicVolumes[3] = Mathf.Clamp(AudioHandler.musicVolumes[3] - Time.deltaTime / 3f, 0, 1);
                        AudioHandler.UpdateVolume();
                        break;
                    case "Dead Grass":
                        AudioHandler.musicVolumes[0] = Mathf.Clamp(AudioHandler.musicVolumes[0] - Time.deltaTime / 3f, 0, 1);
                        AudioHandler.musicVolumes[1] = Mathf.Clamp(AudioHandler.musicVolumes[1] + Time.deltaTime / 3f, 0, 1);
                        AudioHandler.musicVolumes[2] = Mathf.Clamp(AudioHandler.musicVolumes[2] - Time.deltaTime / 3f, 0, 1);
                        AudioHandler.musicVolumes[3] = Mathf.Clamp(AudioHandler.musicVolumes[3] - Time.deltaTime / 3f, 0, 1);
                        AudioHandler.UpdateVolume();
                        break;
                    case "Mountain Rock":
                        AudioHandler.musicVolumes[0] = Mathf.Clamp(AudioHandler.musicVolumes[0] - Time.deltaTime / 3f, 0, 1);
                        AudioHandler.musicVolumes[1] = Mathf.Clamp(AudioHandler.musicVolumes[1] + Time.deltaTime / 3f, 0, 1);
                        AudioHandler.musicVolumes[2] = Mathf.Clamp(AudioHandler.musicVolumes[2] - Time.deltaTime / 3f, 0, 1);
                        AudioHandler.musicVolumes[3] = Mathf.Clamp(AudioHandler.musicVolumes[3] - Time.deltaTime / 3f, 0, 1);
                        AudioHandler.UpdateVolume();
                        break;
                }
            }*/

            base.Update();
        }

        protected override void RenderVisualPosition()
        {
        }

        private void HideNearbyWalls()
        {
            var newHiddenWalls = new HashSet<Wall>();

            double ang = MathsUtils.ClampAngle(Game.Map.Map.Rotation.Rad);

            for (var i = ang - 0.45; i <= ang + 0.45; i += 0.05)
            {
                var vectorToCheck = (new Vector2(Transform.localPosition.x, Transform.localPosition.y) + new Vector2((float)Math.Sin(i), -(float)Math.Cos(i))).ToInt();
                var wall2 = Manager.Map.WallMap[vectorToCheck];
                if (wall2 == null)
                {
                    continue;
                }

                if (!newHiddenWalls.Add(wall2) || hiddenWalls.Contains(wall2))
                {
                    continue;
                }

                wall2.SetAlpha(0.33f);
            }

            foreach (var wall in hiddenWalls.Where(wall => !newHiddenWalls.Contains(wall)))
            {
                wall.RemoveAlpha();
            }

            hiddenWalls = newHiddenWalls;
        }

        private void TryShoot()
        {
            if (shotCooldown > 0 || !InputMaster.InGameplay())
            {
                return;
            }

            var shooting = (!UnityEngine.Input.GetKey(KeyCode.LeftControl) && UnityEngine.Input.GetKey(OptionsHandler.GetKeyCodeOption("shootKey").Value))
                || OptionsHandler.GetOptionValue<bool>("autoShoot");

            if (!shooting)
            {
                return;
            }

            var worldPoint = UnityEngine.Camera.main.ScreenToWorldPoint(UnityEngine.Input.mousePosition);
            Vector3 pos = new UnityEngine.Vector2(SpriteRenderer.bounds.center.x, SpriteRenderer.bounds.center.y);

            UnityEngine.Vector2 diff = pos - worldPoint;
            double angle = -Mathf.Atan2(diff.x, diff.y) - (Mathf.PI * 0.5f);

            if (Shoot(new Angle(angle)))
            {
                shotCooldown = this.GetShootCooldown(Inventory[0]);
            }
        }

        private void Escape(object sender, EventArgs e)
        {
            if (InputMaster.Focus != typeof(Game.Map.Map))
            {
                return;
            }

            GameWorldManager.CurrentOutgoingNetwork.OutgoingMessage(new Escape());
            LoadingHandler.Load("Nexus", 0, Manager);
            Game.Event.ReconnectEvent.Invoke();
            GameWorldManager.CurrentOutgoingNetwork.OutgoingMessage(new Hello(
                NetworkingSettings.FULLBUILD,
                -1,
                AccountStore.Username,
                AccountStore.Password));
        }

        private void UseAbility(object sender, EventArgs e)
        {
            if (!InputMaster.InGameplay() || InAbilityCooldown())
            {
                return;
            }

            var worldPoint = UnityEngine.Camera.main.ScreenToWorldPoint(UnityEngine.Input.mousePosition).Convert();

            var abilityItem = Inventory[1];

            if (abilityItem is null || abilityItem.Value.GetMpCost(SkillTreeUI.ActiveSkills()) > PlayerStats[(int)RoGCore.Models.PlayerStats.Mp])
            {
                return;
            }

            GameWorldManager.CurrentOutgoingNetwork.OutgoingMessage(new UseItem(new ContainerSlot(Id, 1), worldPoint));

            abilityCooldown = abilityItem.Value.GetCooldown(SkillTreeUI.ActiveSkills());
        }

        private void SetSummonPosition(object sender, EventArgs e)
        {
            var worldPoint = UnityEngine.Camera.main.ScreenToWorldPoint(UnityEngine.Input.mousePosition).Convert();

            GameWorldManager.CurrentOutgoingNetwork.OutgoingMessage(new SetSummonPosition(worldPoint));
        }

        private void ToggleSneak(object sender, EventArgs e)
        {
            if (!InputMaster.InGameplay())
            {
                return;
            }

            Sneaking = !Sneaking;

            EntityFactory.CreateNotification<Notifications.Notification>(this, $"Sneak {(Sneaking ? "Enabled" : "Disabled")}", GameColors.LIGHT_GREEN);
        }

        private void IncrementBulletId() => nextBulletId = (ushort)((nextBulletId + 1) % ObjectDesc.MaxProjectiles);

        private bool Shoot(Angle angle)
        {
            var item = Inventory[0];

            if (item is null || item.Value.BaseItem.Projectiles is null || item.Value.BaseItem.Projectiles.Length < 1)
            {
                return false;
            }

            var arcGap = item.Value.BaseItem.ArcGap * Mathf.Deg2Rad;
            var shots = item.Value.BaseItem.Shots;

            var startAngle = new Angle(angle.Rad - (arcGap * ((shots - 1) * 0.5f)));

            for (var i = 0; i < shots; i++)
            {
                var currentAngle = startAngle.GetCopy();
                GameWorldManager.CurrentOutgoingNetwork.OutgoingMessage(new PlayerShoot(nextBulletId, Position, currentAngle, HighResolutionDateTime.UtcNow.Ticks));
                Shoot(nextBulletId, item.Value.BaseItem.Projectiles[0], currentAngle, HighResolutionDateTime.UtcNow.Ticks, false, item, Position);
                IncrementBulletId();
                startAngle.Rad += arcGap;
            }

            return true;
        }

        internal override void OnDamage(in Damage packet)
        {
            if (packet.DamageAmount >= MaxHP / 4)
            {
                CameraShake.OnCritialHit();
            }

            base.OnDamage(packet);
        }

        internal override void CalculateSortingOrder(SortingOrder sortingOrder)
        {
            var pivot = new Vector3(Game.Map.Map.Rotation.Sin * 0.5f, Game.Map.Map.Rotation.Cos * -0.5f);

            var order = sortingOrder.Calculate(Transform.localPosition + pivot);

            if (sortingOrderCache == order)
            {
                return;
            }

            sortingOrderCache = order;

            SpriteRenderer.sortingOrder = order;

            if (SortingGroup != null)
            {
                SortingGroup.sortingOrder = order;
            }
        }

        internal void OnXPNotification(in RoGCore.Networking.Packets.Notification packet)
        {
            var value = packet.Text.Replace(" xp", string.Empty).IntParseFast();

            if (xpNotification == null || !xpNotification.Stackable)
            {
                xpNotification = EntityFactory.CreateNotification<XPNotification>(this, value.ToString(), packet.Color);
            }
            else
            {
                xpNotification.UpdateText(value);
            }
        }

        protected override void OnDestroy()
        {
            InputMaster.UnsubscribeToKeyPress(OptionsHandler.GetKeyCodeOption("escapeKey"), Escape);
            InputMaster.UnsubscribeToKeyPress(KeyCode.Space, UseAbility);
            InputMaster.UnsubscribeToKeyPress(OptionsHandler.GetKeyCodeOption("summonKey"), SetSummonPosition);
            InputMaster.UnsubscribeToKeyPress(OptionsHandler.GetKeyCodeOption("sneakKey"), ToggleSneak);

            base.OnDestroy();
        }
    }
}