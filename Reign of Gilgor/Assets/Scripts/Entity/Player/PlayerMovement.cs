﻿using System;
using Client.Entity.Helpers;
using Client.Game;
using RoGCore.Models;
using RoGCore.Networking.Packets;
using RoGCore.Utils;
using UnityEngine;

namespace Client.Entity.Player
{
    internal static class PlayerMovement
    {
        private const float MoveThreshold = 0.4f;
        private const int RotateSpeed = 150;

        private static bool qDown;
        private static bool eDown;
        private static DateTime lastMovementPacket;

        private static Player player;

        static PlayerMovement()
        {
            GameWorldManager.PlayerCreated.Begin += (p) => player = p;

            InputMaster.SubscribeToKeyPress(KeyCode.Z, (sender, e) =>
            {
                if (player != null)
                {
                    ResetRotation(player);
                }
            });
        }

        internal static void UpdateMovement(this Player player)
        {
            if (player == null)
            {
                return;
            }

            var totalRot = UpdateRotation();
            if (totalRot != 0)
            {
                player.GameObject.transform.Rotate(player.GameObject.transform.forward, totalRot);
                player.Manager.Map.OnCameraRotate(player.GameObject.transform.eulerAngles.z);
            }

            Vector3 moveVector = new Vector2(GetMovement("Horizontal"), GetMovement("Vertical"));
            if (moveVector.x != 0 || moveVector.y != 0)
            {
                var speed = player.GetSpeed() *
                              (player.IgnoreGroundSpeed ? 1 : player.Manager.Map.GroundMap[player.Transform.localPosition.Convert().ToInt()]?.Description?.Speed ?? 0);
                var moveAngle = Game.Map.Map.Rotation.Rad + Mathf.Atan2(moveVector.y, moveVector.x);
                var trig = new Vector2(Mathf.Cos(moveAngle), Mathf.Sin(moveAngle));

                moveVector = speed * Time.deltaTime * trig;
                player.Transform.localPosition = ResolveNewLocation(player, player.Transform.localPosition + moveVector);
            }

            // If player has moved since the last "tick"
            if (player.LastPacket.Equals(player.Transform.localPosition))
            {
                return;
            }

            // Running at around 25 tps for better accuracy
            var now = DateTime.Now;
            if (lastMovementPacket.AddMilliseconds(40) >= now)
            {
                return;
            }

            player.LastPacket = player.Transform.localPosition;
            lastMovementPacket = now;
            GameWorldManager.CurrentOutgoingNetwork.OutgoingMessage(new Move(player.Transform.localPosition.Convert()));
        }

        private static Vector2 ResolveNewLocation(Player player, Vector2 location)
        {
            Vector2 newPosition = player.Transform.localPosition;
            if (player.ConditionEffects.HasConditionEffect(ConditionEffectIndex.Paralyzed) ||
                player.ConditionEffects.HasConditionEffect(ConditionEffectIndex.Petrify))
            {
                return newPosition;
            }

            var d = location - newPosition;
            if (d.x < MoveThreshold &&
                d.x > -MoveThreshold &&
                d.y < MoveThreshold &&
                d.y > -MoveThreshold)
            {
                return CalcNewLocation(player, location);
            }

            var absD = new Vector2(Mathf.Abs(d.x), Mathf.Abs(d.y));
            var ds = MoveThreshold / Math.Max(absD.x, absD.y);
            var tds = 0f;
            var done = false;
            while (!done)
            {
                if (tds + ds >= 1)
                {
                    ds = 1 - tds;
                    done = true;
                }

                newPosition = CalcNewLocation(player, newPosition + (d * ds));
                tds += ds;
            }

            return newPosition;
        }

        private static Vector2 CalcNewLocation(Player player, Vector2 location)
        {
            float fx = 0;
            float fy = 0;
            var isFarX = (player.Transform.localPosition.x % .5f == 0 && location.x != player.Transform.localPosition.x) ||
                          (int)(player.Transform.localPosition.x * 2f) != (int)(location.x * 2f);

            var isFarY = (player.Transform.localPosition.y % .5f == 0 && location.y != player.Transform.localPosition.y) ||
                          (int)(player.Transform.localPosition.y * 2f) != (int)(location.y * 2f);

            if ((!isFarX && !isFarY) || player.Manager.Map.RegionUnblocked(location.x, location.y, TileOccupency.NoWalk | TileOccupency.OccupyHalf))
            {
                return location;
            }

            if (isFarX)
            {
                fx = location.x > player.Transform.localPosition.x
                    ? (int)(location.x * 2) * 0.5f
                    : (int)(player.Transform.localPosition.x * 2) * 0.5f;
                if ((int)fx > (int)player.Transform.localPosition.x)
                {
                    fx -= 0.01f;
                }
            }

            if (isFarY)
            {
                fy = location.y > player.Transform.localPosition.y
                    ? (int)(location.y * 2) * 0.5f
                    : (int)(player.Transform.localPosition.y * 2) * 0.5f;
                if ((int)fy > (int)player.Transform.localPosition.y)
                {
                    fy -= 0.01f;
                }
            }

            if (!isFarX)
            {
                return new Vector2(location.x, fy);
            }

            if (!isFarY)
            {
                return new Vector2(fx, location.y);
            }

            var ax = location.x > player.Transform.localPosition.x ? location.x - fx : fx - location.x;
            var ay = location.y > player.Transform.localPosition.y ? location.y - fy : fy - location.y;
            if (ax > ay)
            {
                if (player.Manager.Map.RegionUnblocked(location.x, fy, TileOccupency.NoWalk | TileOccupency.OccupyHalf))
                {
                    return new Vector2(location.x, fy);
                }

                if (player.Manager.Map.RegionUnblocked(fx, location.y, TileOccupency.NoWalk | TileOccupency.OccupyHalf))
                {
                    return new Vector2(fx, location.y);
                }
            }
            else
            {
                if (player.Manager.Map.RegionUnblocked(fx, location.y, TileOccupency.NoWalk | TileOccupency.OccupyHalf))
                {
                    return new Vector2(fx, location.y);
                }

                if (player.Manager.Map.RegionUnblocked(location.x, fy, TileOccupency.NoWalk | TileOccupency.OccupyHalf))
                {
                    return new Vector2(location.x, fy);
                }
            }

            return new Vector2(fx, fy);
        }

        internal static void ResetRotation(this Player player)
        {
            if (player == null)
            {
                return;
            }

            player.GameObject.transform.eulerAngles = Vector3.zero;
            player.Manager.Map.OnCameraRotate(0);
        }

        private static float UpdateRotation() => RotateSpeed * Time.deltaTime * GetRotationDirection();

        private static bool FocusedOnMovement() => InputMaster.Focus == typeof(Client.Game.Map.Map) || InputMaster.Focus == typeof(InventoryDisplay);

        private static float GetMovement(string axis) => FocusedOnMovement() ? UnityEngine.Input.GetAxisRaw(axis) : 0;

        private static float GetRotationDirection()
        {
            if (!FocusedOnMovement())
            {
                return 0;
            }

            var leftKey = OptionsHandler.GetOptionValue<KeyCode>("rotLeftKey");
            if (UnityEngine.Input.GetKeyDown(leftKey))
            {
                qDown = true;
            }
            else if (UnityEngine.Input.GetKeyUp(leftKey))
            {
                qDown = false;
            }

            var rightKey = OptionsHandler.GetOptionValue<KeyCode>("rotRightKey");
            if (UnityEngine.Input.GetKeyDown(rightKey))
            {
                eDown = true;
            }
            else if (UnityEngine.Input.GetKeyUp(rightKey))
            {
                eDown = false;
            }

            return (qDown && eDown) || (!qDown && !eDown) ? 0 : qDown ? 1 : -1;
        }
    }
}