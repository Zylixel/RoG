﻿using Client.Particle;
using RoGCore.Assets;
using RoGCore.Models;
using UnityEngine;

namespace Client.Entity
{
    internal sealed class Portal : SimpleEntity
    {
        private ParticleSystem particleEffect;

        internal override void Init(in ObjectStats stats, ObjectDescription objectDesc, GameObjectComponentHolder componentHolder, GameWorldManager manager, bool doOnUpdate)
        {
            base.Init(stats, objectDesc, componentHolder, manager, doOnUpdate);

            if (objectDesc.Name == "Gilgor's Realm Portal")
            {
                AddName();
                StartFlash(GameColors.PortalFlash, 1);
            }

            particleEffect = ParticleFactory.CreateParticle(
                "PortalParticle",
                new CreateParticleProperties { Parent = GameObject.transform, LocalPosition = new Vector2(0f, 0.5f) });
            if (particleEffect != null)
            {
                particleEffect.transform.localScale = GameObject.transform.localScale;
            }
        }

        protected override void OnDestroy()
        {
            if (particleEffect != null)
            {
                ParticleFactory.ReturnParticle(particleEffect);
            }

            base.OnDestroy();
        }

        protected override Material Material() => EmbeddedData.PortalMaterial;
    }
}