﻿using System;
using System.Collections.Generic;
using System.Linq;
using Client.Entity.Interfaces;
using Client.Particle;
using Client.UI;
using RoGCore.Assets;
using RoGCore.Models;
using RoGCore.Utils;
using UnityEngine;
using static Client.Entity.Projectile.ProjectileCalculator;

namespace Client.Entity.Projectile
{
    internal class Projectile : MonoBehaviour, IProjectile, IPoolableObject
    {
        internal GameObject GameObject;
        internal Transform Transform;

        internal ProjectileCalculation CalculatedData;

        private const float HitRadius = 0.5f;

        private readonly List<long> hit = new List<long>();

        private ProjectileDescription description;
        private GameWorldManager manager;
        private ParticleSystem particleEffect;

        private float cachedRenderedTheta;

        private float cachedStrength = float.PositiveInfinity;

        public GameObjectComponentHolder ComponentHolder { get; private set; }

        public Angle Angle { get; private set; }

        public CustomProjectile CustomDesc { get; private set; }

        public Vector2 BeginPosition { get; private set; }

        public float OrbitThetaChange { get; set; }

        public float RenderedTheta { get; set; }

        public float Lifetime { get; set; }

        public float Speed { get; set; }

        public ushort Id { get; set; }

        public ushort Damage => throw new ArgumentException("Security: Damage should not be accessed on the client");

        internal bool HitsPlayer { get; private set; }

        internal Character Owner { get; private set; }

        internal Tuple<long, ushort> FullId { get; private set; }

        internal DateTime BeginTime { get; private set; }

        internal static bool HittingEntity(Vector2 pos, SimpleEntity entity)
        {
            var dist = new Vector2(entity.Position.X - pos.x, entity.Position.Y - pos.y);
            return dist.x * (dist.x > 0 ? 1 : -1) <= entity.HitRadius && dist.y * (dist.y > 0 ? 1 : -1) <= entity.HitRadius;
        }

        internal static float GetHitRadius(SimpleEntity entity) => HitRadius * Mathf.Clamp(entity.Size.x, 1f, float.MaxValue);

        internal void Init(Character owner, ushort id, CustomProjectile customDesc, ProjectileDescription description, Angle angle, long createTime, Vector2 startPos, Item? item, bool hitsPlayer, GameObjectComponentHolder componentHolder)
        {
            Transform = transform;
            GameObject = gameObject;

            ComponentHolder = componentHolder;
            CustomDesc = customDesc;
            this.description = description;
            HitsPlayer = hitsPlayer;
            Id = id;
            Owner = owner;
            Angle = angle;
            BeginPosition = startPos;
            Transform.localPosition = BeginPosition;

            // TODO: implement skills for serverplayer
            if (owner is Player.Player)
            {
                Lifetime = CustomDesc.GetLifetime(item?.BaseItem, SkillTreeUI.ActiveSkills());
                Speed = CustomDesc.GetSpeed(item?.BaseItem, SkillTreeUI.ActiveSkills());
            }
            else
            {
                Lifetime = CustomDesc.BaseLifeTime;
                Speed = CustomDesc.BaseSpeed;
            }

            FullId = Tuple.Create(Owner.Id, Id);
            manager = Owner.Manager;
            BeginTime = new DateTime(createTime);

            if (description.Texture is not null)
            {
                Transform.eulerAngles = new Vector3(0, 0, Angle.Deg - description.Texture.Rotation);
            }

            UpdateStrength(0f);
            SetSprite();
            SetParticle();
            SetLight();
        }

        internal void HitCharacter(Character character)
        {
            character.OnHit(this);
            if (!CustomDesc.MultiHit)
            {
                Destroy();
            }
            else
            {
                hit.Add(character.Id);
            }
        }

        private void SetLight()
        {
            if (ComponentHolder.Light2d == null)
            {
                return;
            }

            ComponentHolder.Light2d.color = description.LightColorProcessed;
            ComponentHolder.Light2d.intensity = description.LightIntensity * 0.01f;
            ComponentHolder.Light2d.pointLightOuterRadius = 5f;
        }

        private void SetSprite()
        {
            if (description.Texture is null)
            {
                ComponentHolder.SpriteRenderer.enabled = false;
                return;
            }

            ComponentHolder.SpriteRenderer.sortingLayerName = "Projectile";
            ComponentHolder.SpriteRenderer.sharedMaterial = EmbeddedData.GameEntityMaterial;
            ComponentHolder.SpriteRenderer.sprite = description.Texture.Type switch
            {
                TextureData.TEXTURETYPERANDOM => description.Textures[manager.GlobalRandom.Next(0, description.Textures.Length)],
                TextureData.TEXTURETYPERANDOMCYCLE => description.Textures[manager.GlobalRandom.Next(0, description.Textures.Length)],
                _ => description.Textures.FirstOrDefault(),
            };
        }

        private void SetParticle()
        {
            if (description.ParticleEffect.IsNullOrWhiteSpace())
            {
                return;
            }

            particleEffect = ParticleFactory.CreateParticle(
                description.ParticleEffect + "Particle",
                new CreateParticleProperties { LocalPosition = Transform.localPosition, AutoReturn = true, Force = true, DontAutoPlay = true });

            if (description.ParticleEffect == "Magic")
            {
                var main = particleEffect.main;
                main.duration = Lifetime * 0.001f;
            }

            particleEffect.Play();

            particleEffect.GetComponent<ParticleSystemRenderer>().material
                .SetColor("_Tint", description.TintColorProcessed);
        }

        private void Update()
        {
            if (CalculatedData.HitsPlayer)
            {
                GameWorldManager.CurrentOutgoingNetwork.OutgoingMessage(new RoGCore.Networking.Packets.PlayerHit(Id, Owner.Id));
                Destroy();
                return;
            }

            if (CalculatedData.Destroy)
            {
                Destroy();
                return;
            }

            UpdateStrength(CalculatedData.StrengthMultiplier);

            Transform.localPosition = CalculatedData.Position;

            if (particleEffect != null)
            {
                particleEffect.transform.localPosition = CalculatedData.Position;
            }

            if (description.Texture is not null)
            {
                if (description.RotateSpeed != 0)
                {
                    Transform.Rotate(0, 0, description.RotateSpeed * 10 * Time.deltaTime);
                }
                else if (RenderedTheta != cachedRenderedTheta)
                {
                    Transform.eulerAngles =
                        new Vector3(0, 0, (RenderedTheta * Mathf.Rad2Deg) - description.Texture.Rotation);

                    cachedRenderedTheta = RenderedTheta;
                }
            }
        }

        private void Destroy()
        {
            ParticleFactory.CreateProjectileDeathParticle(Transform.localPosition, description);
            manager.Map.RemoveProjectile(FullId);
        }

        private void UpdateStrength(float newStrength)
        {
            if (cachedStrength != newStrength)
            {
                Transform.localScale = 0.01f * CustomDesc.Size * newStrength * Owner.Size;

                if (ComponentHolder.Light2d != null)
                {
                    ComponentHolder.Light2d.intensity = newStrength * description.LightIntensity * 0.01f;
                }

                cachedStrength = newStrength;
            }
        }

        private void OnDestroy()
        {
            if (particleEffect != null)
            {
                var emmision = particleEffect.emission;
                emmision.enabled = false;
                particleEffect.gameObject.AddComponent<ParticleAutoEmission>();
            }
        }
    }
}