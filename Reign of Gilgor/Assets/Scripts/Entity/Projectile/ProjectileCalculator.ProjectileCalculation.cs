﻿using UnityEngine;

#nullable enable

namespace Client.Entity.Projectile
{
    internal static partial class ProjectileCalculator
    {
        internal struct ProjectileCalculation
        {
            internal Vector2 Position;
            internal bool Destroy;
            internal bool HitsPlayer;
            internal float StrengthMultiplier;
        }
    }
}
