﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using Client.Game.Map;
using Client.UI.Titlecard;
using Microsoft.Extensions.Logging;
using RoGCore.Game;
using RoGCore.Logging;
using RoGCore.Models;
using UnityEngine;

#nullable enable

namespace Client.Entity.Projectile
{
    internal static partial class ProjectileCalculator
    {
        private const float Tau = Mathf.PI * 2f;

        private static readonly Microsoft.Extensions.Logging.ILogger Logger = LogFactory.LoggingInstance(nameof(ProjectileCalculator));

        internal static void CalculatePositions(Map map, ConcurrentDictionary<Tuple<long, ushort>, Projectile> projectiles)
        {
            var processors = Environment.ProcessorCount;

            if (projectiles.Count < processors * 3)
            {
                CalculateBatch(map, projectiles);
                return;
            }

            var batches = projectiles.Split(processors);

            var countdownEvent = new CountdownEvent(batches.Length);

            foreach (var batch in batches)
            {
                if (batch.Count == 0)
                {
                    Logger.LogWarning("0 batch");
                    countdownEvent.Signal();
                    continue;
                }

                ThreadPool.QueueUserWorkItem(_ =>
                {
                    CalculateBatch(map, batch);

                    countdownEvent.Signal();
                });
            }

            // Wait for threads to finish
            countdownEvent.Wait();
        }

        private static void CalculateBatch(Map map, IEnumerable<KeyValuePair<Tuple<long, ushort>, Projectile>> batch)
        {
            var player = map.Player;

            var now = DateTime.UtcNow;

            foreach (var projectile in batch)
            {
                var elapsedTime = (float)(now - projectile.Value.BeginTime).TotalMilliseconds;

                var position = projectile.Value.GetPosition(elapsedTime);
                projectile.Value.CalculatedData = new ProjectileCalculation
                {
                    Position = position,
                    Destroy = Destroy(projectile.Value, elapsedTime, position, map),
                    HitsPlayer = player != null && HitsPlayer(projectile.Value, position, player),
                    StrengthMultiplier = CalculateStrengthMultiplier(projectile.Value, elapsedTime),
                };
            }
        }

        private static List<T>[] Split<T>(this ICollection<T> source, int splits)
        {
            var ret = new List<T>[splits];

            var listApprox = Mathf.CeilToInt((float)source.Count / splits);
            for (var j = 0; j < ret.Length; j++)
            {
                ret[j] = new List<T>(listApprox);
            }

            var i = 0;
            foreach (var item in source)
            {
                ret[i].Add(item);

                i++;
                i %= splits;
            }

            return ret;
        }

        private static bool Destroy(Projectile projectile, float elapsedTime, Vector3 position, Map map) => elapsedTime > projectile.Lifetime || projectile.OrbitThetaChange > Tau || map.TileOccupied(position.x, position.y, TileOccupency.OccupyFull);

        private static bool HitsPlayer(Projectile projectile, Vector2 projectilePosition, Player.Player player) => projectile.HitsPlayer && Projectile.HittingEntity(projectilePosition, player) && !TitlecardEvent.InProgess;

        private static float CalculateStrengthMultiplier(Projectile projectile, float elapsedTime) => Mathf.Min(elapsedTime * 0.0075f, 1);
    }
}
