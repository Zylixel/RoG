﻿using Client.Utils;
using RoGCore.Assets;
using RoGCore.Models;
using RoGCore.Utils;
using Structures;
using UnityEngine;

namespace Client.Entity
{
    internal partial class ServerPlayer : Character, IContainer
    {
        public int[] Boosts = new int[8];
        public int[] PlayerStats = new int[8];

        internal override void Init(in ObjectStats stats, ObjectDescription objectDesc, GameObjectComponentHolder componentHolder, GameWorldManager manager, bool doOnUpdate)
        {
            base.Init(stats, objectDesc, componentHolder, manager, doOnUpdate);

            if (this is not Player.Player)
            {
                AddName();
            }
        }

        internal int AccountId { get; private set; }

        internal string GuildName { get; private set; }

        internal int GuildRank { get; private set; }

        public Item?[] Inventory { get; set; }

        internal PlayerSkills SkillLevels { get; private set; } = new PlayerSkills();

        internal override bool CanAppear() => true;

        protected override Material Material() => EmbeddedData.PlayerMaterial;

        protected override void AddName()
        {
            base.AddName();
            NameText.fontSize = 0.2f;
            NameText.transform.localPosition = new Vector3(0f, -1.1f);
        }

        private void UpdateNameColor()
        {
            if (NameText != null && Manager.Map.Player != null)
            {
                NameText.color = GameColors.GetOtherPlayerColor(Manager.Map.Player, this);
            }
        }

        internal override void OnUpdate(in ObjectStats stat)
        {
            base.OnUpdate(stat);
            SetVariables(stat);
        }

        private void SetVariables(ObjectStats stat)
        {
            for (var i = 0; i < stat.Stats.Length; i++)
            {
                var data = stat.Stats[i].Value;
                switch (stat.Stats[i].Key)
                {
                    case StatsType.Inventory:
                        Inventory = RawItem.Cook(RawItem.Read((byte[])data), Manager.GameData.Items);
                        break;
                    case StatsType.Guild:
                        GuildName = data.ToString();
                        break;
                    case StatsType.GuildRank:
                        GuildRank = (int)data;
                        break;
                    case StatsType.Defense:
                        PlayerStats[(int)RoGCore.Models.PlayerStats.Defense] = (int)data;
                        break;
                    case StatsType.DefenseBonus:
                        Boosts[(int)RoGCore.Models.PlayerStats.Defense] = (int)data;
                        break;
                    case StatsType.Attack:
                        PlayerStats[(int)RoGCore.Models.PlayerStats.Attack] = (int)data;
                        break;
                    case StatsType.AttackBonus:
                        Boosts[(int)RoGCore.Models.PlayerStats.Attack] = (int)data;
                        break;
                    case StatsType.Dexterity:
                        PlayerStats[(int)RoGCore.Models.PlayerStats.Dexterity] = (int)data;
                        break;
                    case StatsType.DexterityBonus:
                        Boosts[(int)RoGCore.Models.PlayerStats.Dexterity] = (int)data;
                        break;
                    case StatsType.Speed:
                        PlayerStats[(int)RoGCore.Models.PlayerStats.Speed] = (int)data;
                        break;
                    case StatsType.SpeedBonus:
                        Boosts[(int)RoGCore.Models.PlayerStats.Speed] = (int)data;
                        break;
                    case StatsType.Hp:
                        PlayerStats[(int)RoGCore.Models.PlayerStats.Hp] = (int)data;
                        break;
                    case StatsType.HpBoost:
                        Boosts[(int)RoGCore.Models.PlayerStats.Hp] = (int)data;
                        break;
                    case StatsType.Mp:
                        PlayerStats[(int)RoGCore.Models.PlayerStats.Mp] = (int)data;
                        break;
                    case StatsType.MpBoost:
                        Boosts[(int)RoGCore.Models.PlayerStats.Mp] = (int)data;
                        break;
                    case StatsType.Vitality:
                        PlayerStats[(int)RoGCore.Models.PlayerStats.Vitality] = (int)data;
                        break;
                    case StatsType.VitalityBonus:
                        Boosts[(int)RoGCore.Models.PlayerStats.Vitality] = (int)data;
                        break;
                    case StatsType.Wisdom:
                        PlayerStats[(int)RoGCore.Models.PlayerStats.Wisdom] = (int)data;
                        break;
                    case StatsType.WisdomBonus:
                        Boosts[(int)RoGCore.Models.PlayerStats.Wisdom] = (int)data;
                        break;
                    case StatsType.AccountId:
                        AccountId = (int)data;
                        break;
                    case StatsType.SkillLevels:
                        SkillLevels.Read((byte[])data);
                        break;
                }
            }
        }

        protected override void Update()
        {
            base.Update();
            UpdateNameColor();
            if (Manager.Map.GroundMap[Transform.localPosition.Convert().ToInt()]?.Description?.Liquid == true)
            {
                SpriteRenderer.sprite = SpriteRenderer.sprite.GetSinkingSprite();
            }
        }
    }
}