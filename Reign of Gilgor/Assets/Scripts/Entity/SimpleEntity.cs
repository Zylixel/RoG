﻿using System.Linq;
using Client.Entity.Helpers;
using Client.Entity.Interfaces;
using Client.Game;
using Client.Game.Hitbox;
using Client.Particle;
using Client.Structures;
using Entity.Helpers;
using Microsoft.Extensions.Logging;
using RoGCore.Assets;
using RoGCore.Logging;
using RoGCore.Models;
using RoGCore.Utils;
using TMPro;
using UnityEngine;
using UnityEngine.Rendering;

#nullable enable

namespace Client.Entity
{
    internal partial class SimpleEntity : EntityBehaviour, ISpriteEntity, IPoolableObject, IHitbox
    {
        internal static readonly int PositionHash = Shader.PropertyToID("ObjectPosition");
        internal static readonly int ShadowThicknessHash = Shader.PropertyToID("OutlineThickness");

        internal double DistToPlayerSqr = double.PositiveInfinity;

        internal float HitRadius;

        protected TextMeshProUGUI? NameText;

        [System.Diagnostics.CodeAnalysis.AllowNull]
        public GameObjectComponentHolder ComponentHolder { get; private set; }

        public float SpriteCycleSpeed => ObjectDesc.Texture?.CycleSpeed ?? 0;

        private static readonly ILogger<SimpleEntity> Logger = LogFactory.LoggingInstance<SimpleEntity>();

        internal virtual void Init(in ObjectStats stats, ObjectDescription objectDesc, GameObjectComponentHolder componentHolder, GameWorldManager manager, bool doOnUpdate)
        {
            Init(stats, objectDesc, manager, false);
            ComponentHolder = componentHolder;

#if UNITY_EDITOR || DEVELOPMENT_BUILD
            GameObject.name = objectDesc.Name + "_" + Id;
#endif

            if (ComponentHolder.Light2d != null)
            {
                SetLight();
            }

            ApplyMaterial();

            SetSprite(objectDesc);

            MaterialPropertyBlock = new MaterialPropertyBlock();
            SpriteRenderer.GetPropertyBlock(MaterialPropertyBlock);

            UpdateRotation();

            ViewDistanceChanged(OptionsHandler.GetOptionValue<float>("viewDistance"));

            if (doOnUpdate)
            {
                OnUpdate(Stats);
            }
        }

        public Canvas? Canvas => ComponentHolder.Canvas;

        private SortingGroup? sortingGroup;

        internal SortingGroup SortingGroup => sortingGroup = sortingGroup != null ? sortingGroup : AddSortingGroup();

        public MonoBehaviour Behaviour
        {
            get => this;
            set { }
        }

        public SpriteRenderer SpriteRenderer => ComponentHolder.SpriteRenderer;

        protected int sortingOrderCache;

        private EntityConditionHandler? conditionEffects;

        internal EntityConditionHandler ConditionEffects => conditionEffects ??= new EntityConditionHandler(this);

        public HitboxType HitboxType => HitboxType.Circle;

        public int HitboxSize => (int)Size.x;

        public Vector2 HitboxCenter => Position.ToUnity2();

        private void SetLight()
        {
            ComponentHolder.Light2d.color = ObjectDesc.LightColorProcessed;
            ComponentHolder.Light2d.intensity = ObjectDesc.LightIntensity / 100f;
        }

        protected virtual Material Material() => ObjectDesc.AffectedByWind && OptionsHandler.GetOptionValue<ShaderQuality>("graphicsQuality") == ShaderQuality.High
                ? EmbeddedData.WindMaterial
                : EmbeddedData.GameEntityMaterial;

        protected virtual void SetSprite(ObjectDescription objectDescription)
        {
            if (objectDescription.Texture is null)
            {
                SpriteRenderer.enabled = false;
                return;
            }

            SpriteRenderer.sortingLayerName = "Entity";
            switch (objectDescription.Texture.Type)
            {
                case TextureData.TEXTURETYPERANDOM:
                    SpriteRenderer.sprite = objectDescription.Textures[Manager.GlobalRandom.Next(0, objectDescription.Textures.Length)];
                    break;
                case TextureData.TEXTURETYPEANIMATED:
                    SpriteRenderer.sprite = objectDescription.Textures.FirstOrDefault();
                    Logger.LogError($"SimpleEntities cannot use animated textures. Entity: {objectDescription.Name}");
                    break;
                case TextureData.TEXTURETYPERANDOMCYCLE:
                case TextureData.TEXTURETYPECYCLE:
                case TextureData.TEXTURETYPEPINGPONG:
                    StartCoroutine(this.SpriteCycle(objectDescription, Manager));
                    break;
                default:
                    SpriteRenderer.sprite = objectDescription.Textures.FirstOrDefault();
                    break;
            }
        }

        protected virtual void AddName()
        {
            if (Canvas == null)
            {
                return;
            }

            NameText = Instantiate(GamePrefabs.GameEntityNameText).GetComponent<TextMeshProUGUI>();
            NameText.transform.SetParent(Canvas.transform);
            NameText.transform.localScale = Vector3.one;
            NameText.transform.localPosition = new Vector3(0f, -0.3f, 0f);
            NameText.transform.eulerAngles = Game.Map.Map.Rotation.ToUnity3();
            NameText.text = Name;
        }

        protected virtual void OnEnable()
        {
            if (ObjectDesc is not null)
            {
                SetSprite(ObjectDesc);
                UpdateRotation();
            }
        }

        protected override void Update()
        {
            base.Update();

            CalculateSortingOrder(Manager.Map.SortingOrder);
            UpdateFlashRate();
        }

        internal void RecalculateDistanceToPlayer(Player.Player p) => DistToPlayerSqr = p.DistSqr(Position);

        internal override void OnUpdate(in ObjectStats stat)
        {
            base.OnUpdate(stat);
            if (NameText != null)
            {
                NameText.text = Name;
            }

            var conditionEffectsUpdated = false;
            for (var i = 0; i < stat.Stats.Length; i++)
            {
                var data = stat.Stats[i].Value;
                switch (stat.Stats[i].Key)
                {
                    case StatsType.Effects:
                        {
                            ConditionEffects.ConditionEffects1 = (int)data;
                            conditionEffectsUpdated = true;
                            break;
                        }

                    case StatsType.Effects2:
                        {
                            ConditionEffects.ConditionEffects2 = (int)data;
                            conditionEffectsUpdated = true;
                            break;
                        }
                }
            }

            if (Material() == EmbeddedData.WindMaterial)
            {
                SetRendererVector(PositionHash, GameObject.transform.position);
            }

            if (conditionEffectsUpdated)
            {
                SpriteRenderer.color = ConditionEffects.HasConditionEffect(ConditionEffectIndex.Invisible) ? GameColors.ENTITY_INVISIBLE : Color.white;
                ConditionEffects.OnUpdate();
            }
        }

        protected override void SizeChanged(Vector3 size)
        {
            base.SizeChanged(size);

            Transform.localScale = Size;

            HitRadius = Projectile.Projectile.GetHitRadius(this);

            if (Size.x > 0 && Size.y > 0)
            {
                if (Canvas != null)
                {
                    Canvas.transform.localScale = new Vector3(1 / Size.x, 1 / Size.y, 1);
                }
            }

            if (SpriteRenderer == null || SpriteRenderer.sprite == null)
            {
                return;
            }

            SetMaterialObjectScale();
        }

        internal void SetMaterialObjectScale() => SetRendererFloat(ShaderProperties.ObjectScale, Size.x / (SpriteRenderer.sprite.pixelsPerUnit / 8f));

        internal virtual void UpdateRotation()
        {
            var mapRotation = Game.Map.Map.Rotation.ToUnity3();

            GameObject.transform.eulerAngles = mapRotation;

            if (Canvas != null)
            {
                Canvas.transform.eulerAngles = mapRotation;
            }

            if (SortingGroup != null)
            {
                SortingGroup.transform.eulerAngles = mapRotation;
            }
        }

        internal SortingGroup AddSortingGroup()
        {
            var go = new GameObject("SortingGroup");

            go.transform.SetParent(Transform);
            go.transform.localScale = Vector3.one;
            go.transform.localPosition = Vector3.zero;
            go.transform.eulerAngles = Game.Map.Map.Rotation.ToUnity3();

            var sortingGroup = go.AddComponent<SortingGroup>();
            sortingGroup.sortingOrder = sortingOrderCache;
            sortingGroup.sortingLayerName = "Entity";
            return sortingGroup;
        }

        internal virtual void CalculateSortingOrder(SortingOrder sortingOrder)
        {
            var order = sortingOrder.Calculate(Position);

            if (sortingOrderCache == order)
            {
                return;
            }

            sortingOrderCache = order;

            SpriteRenderer.sortingOrder = order;

            if (SortingGroup != null)
            {
                SortingGroup.sortingOrder = order;
            }
        }

        internal virtual bool OnHit(Projectile.Projectile proj)
        {
            if (ConditionEffects.HasConditionEffect(ConditionEffectIndex.Invincible))
            {
                return false;
            }

            ParticleFactory.CreateHitParticle(Transform.localPosition, ObjectDesc);
            return true;
        }

        protected virtual void OnDestroy()
        {
            if (NameText != null)
            {
                Destroy(NameText.gameObject);
            }

            if (SortingGroup != null)
            {
                Destroy(SortingGroup.gameObject);
            }

            StopAllCoroutines();

            conditionEffects?.Dispose();
        }

        internal void ViewDistanceChanged(float e)
        {
            var shadowOutline = Mathf.RoundToInt(e) switch
            {
                6 => 1 / 16f,
                7 => 1 / 8f,
                8 => 1 / 8f,
                9 => 1 / 4f,
                _ => 1 / 4f,
            };

            SetRendererFloat(ShadowThicknessHash, shadowOutline);
        }
    }
}