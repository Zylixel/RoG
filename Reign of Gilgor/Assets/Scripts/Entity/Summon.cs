﻿using RoGCore.Assets;
using RoGCore.Models;
using UnityEngine;

namespace Client.Entity
{
    internal sealed class Summon : Character
    {
        private static readonly int OutlineColorProperty = Shader.PropertyToID("OutlineColor");

        [SerializeField]
        private ObjectDescription summonerDescription;

        internal override void Init(in ObjectStats stats, ObjectDescription objectDesc, GameObjectComponentHolder componentHolder, GameWorldManager manager, bool doOnUpdate)
        {
            foreach (var stat in stats.Stats)
            {
                if (stat.Key == StatsType.AltTextureIndex)
                {
                    manager.GameData.Objects.TryGetValue((ushort)(int)stat.Value, out summonerDescription);
                }
            }

            base.Init(stats, objectDesc, componentHolder, manager, true);
        }

        protected override Material Material() => EmbeddedData.PlayerMaterial;

        protected override void SetSprite(ObjectDescription objectDescription)
        {
            if (ObjectDesc.Texture is null && summonerDescription is not null)
            {
                base.SetSprite(summonerDescription);
                return;
            }

            base.SetSprite(objectDescription);
        }

        protected override void SetOutlineColor(Color color) =>

            // All Summons must use a white outline.
            SetRendererColor(OutlineColorProperty, Color.white);
    }
}