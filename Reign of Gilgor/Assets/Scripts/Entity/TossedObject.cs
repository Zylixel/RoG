using System.Linq;
using Client.Entity.Interfaces;
using RoGCore.Assets;
using UnityEngine;

namespace Client.Entity
{
    public class TossedObject : MonoBehaviour, ISpriteEntity
    {
        internal GameWorldManager Manager;

        protected ObjectDescription objectDescription;

        protected SpriteRenderer spriteRenderer;

        private static readonly Vector2 ShadowScale = new Vector2(1, 0.25f);

        [SerializeField]
        private GameObject shadow;
        [SerializeField]
        private Vector2 startPosition;
        [SerializeField]
        private Vector2 endPosition;
        [SerializeField]
        private float durationInMilliseconds;
        private bool tossing;
        private float elapsedTime;

        public SpriteRenderer SpriteRendererProperty => spriteRenderer;

        public virtual float SpriteCycleSpeed => objectDescription.Texture.CycleSpeed;

        public virtual void Toss()
        {
            tossing = true;
            elapsedTime = 0;
            spriteRenderer.sortingLayerName = "TossedObjects";
        }

        public void SetSideSpriteByIndex(int textureCycleIndex) => spriteRenderer.sprite = objectDescription.Textures[textureCycleIndex];

        internal virtual void Initialize(GameWorldManager manager, Vector2 startPosition, Vector2 endPosition, ObjectDescription objectDescription, float tossTime)
        {
            Manager = manager;
            this.startPosition = startPosition;
            this.endPosition = endPosition;
            this.objectDescription = objectDescription;
            durationInMilliseconds = tossTime;

            if (objectDescription.Textures is not null)
            {
                spriteRenderer.sprite = objectDescription.Textures[0];
            }
        }

        protected virtual void Awake()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();

            if (shadow == null)
            {
                shadow = GetComponentsInChildren<SpriteRenderer>().First(x => x.name.Contains("Shadow")).gameObject;
            }
        }

        protected virtual bool DestroyOnComplete() => true;

        protected virtual void TossComplete()
        {
            tossing = false;
            spriteRenderer.sortingLayerName = "Entity";

            if (DestroyOnComplete())
            {
                Destroy(gameObject);
            }
        }

        private void Update()
        {
            if (tossing)
            {
                if (elapsedTime > durationInMilliseconds)
                {
                    SetRotation(1f);
                    SetPosition(1f);
                    SetScale(1f);
                    TossComplete();
                    return;
                }

                var completionValue = elapsedTime / durationInMilliseconds;
                SetRotation(completionValue);
                SetPosition(completionValue);
                SetScale(completionValue);

                elapsedTime += Time.deltaTime * 1000f;
            }

            SetRotation(1f);

            spriteRenderer.sortingOrder = Manager.Map.SortingOrder.Calculate(transform.position);
        }

        private void SetScale(float completionValue) => shadow.transform.localScale = ShadowScale / (1f + (GetYOffset(completionValue) * 0.25f));

        private void SetRotation(float completionValue)
        {
            transform.eulerAngles = new Vector3(0, 0, ((1f - completionValue) * 180f) + Game.Map.Map.Rotation.Deg);

            shadow.transform.eulerAngles = Game.Map.Map.Rotation.ToUnity3();
        }

        private void SetPosition(float completionValue)
        {
            Vector2 yOffsetRotated = Quaternion.Euler(0, 0, Game.Map.Map.Rotation.Deg) * new Vector2(0, GetYOffset(completionValue));

            var displacement = (endPosition - startPosition) * completionValue;

            transform.position = startPosition + displacement + yOffsetRotated;
            shadow.transform.position = startPosition + displacement;
        }

        private float GetYOffset(float completionValue)
        {
            if (completionValue <= 0.625f)
            {
                var yOffset = (2.5f * completionValue) - (2 * Mathf.Pow(completionValue, 2));
                return 6f * yOffset;
            }
            else
            {
                var yOffset = (2 * completionValue) - (2 * Mathf.Pow(completionValue, 2));
                return 10f * yOffset;
            }
        }
    }
}