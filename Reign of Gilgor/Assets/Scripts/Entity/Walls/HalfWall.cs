﻿using RoGCore.Assets;
using RoGCore.Models;
using UnityEngine;
using Vector2 = System.Numerics.Vector2;

namespace Client.Entity
{
    internal sealed class HalfWall : Wall
    {
        #region Instance

        internal new void Init(ObjectStats stats, ObjectDescription desc, GameWorldManager manager, WallPool.WallGameObjects objects)
        {
            base.Init(stats, desc, manager, objects);

            GameObjects.XMeshFilter.sharedMesh = XMesh;
            GameObjects.YMeshFilter.sharedMesh = YMesh;
        }

        #endregion

        #region Static

        private static Vector2 cachedTopPosition;
        private static readonly Mesh XMesh;
        private static readonly Mesh YMesh;

        /// <summary>
        ///     Stores some base values
        /// </summary>
        static HalfWall()
        {
            cachedTopPosition = Vector2.Zero;
            YMesh = Mesh();
            YMesh.MarkDynamic();
            YMesh.Optimize();
            XMesh = Mesh();
            XMesh.MarkDynamic();
            XMesh.Optimize();
            PreRotation();
        }

        /// <summary>
        ///     Used before Instance members are rotated
        ///     Caches the top side position to help CPU
        /// </summary>
        internal static new void PreRotation()
        {
            cachedTopPosition = new Vector2(Game.Map.Map.Rotation.Sin, Game.Map.Map.Rotation.Cos) * 0.5f;

            // These values are very arbitrary, do not tamper with them unless you know what you are doing. 
            // Also don't ask Mason how because he doesn't know
            YMesh.SetVertices(new[]
            {
                new Vector3(cachedTopPosition.X, 0.5f - cachedTopPosition.Y),
                new Vector3(1 + cachedTopPosition.X, 0.5f - cachedTopPosition.Y),
                new Vector3(0, 0.5f),
                new Vector3(1, 0.5f),
            });
            YMesh.RecalculateBounds();
            YMesh.Optimize();
            XMesh.SetVertices(new[]
            {
                new Vector3(0 - cachedTopPosition.Y, 1 - cachedTopPosition.X),
                new Vector3(1 - cachedTopPosition.Y, 1 - cachedTopPosition.X),
                new Vector3(0, 1),
                new Vector3(1, 1),
            });
            XMesh.RecalculateBounds();
            XMesh.Optimize();
        }

        protected override Vector3 PositionTopSide()
        {
            var topSidePosition = new Vector3(Position.X - cachedTopPosition.X, Position.Y + cachedTopPosition.Y, 0);
            GameObjects.TopFace.transform.position = topSidePosition;
            return topSidePosition;
        }

        protected override void PositionXSide(Vector3 topPosition) => GameObjects.XFace.transform.position = topPosition + new Vector3(XFacingWest ? 0.5f : 1.5f, -0.5f);

        protected override void PositionYSide(Vector3 topPosition) => GameObjects.YFace.transform.position = topPosition + new Vector3(-0.5f, YFacingSouth ? -1f : -0f);
        #endregion
    }
}