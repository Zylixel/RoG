﻿using System;
using Client.Entity.Helpers;
using Client.Entity.Interfaces;
using Client.Entity.Notifications;
using Microsoft.Extensions.Logging;
using RoGCore.Assets;
using RoGCore.Logging;
using RoGCore.Models;
using RoGCore.Utils;
using UnityEngine;
using UnityEngine.Rendering;
using static Client.WallPool;

namespace Client.Entity
{
    internal partial class Wall : EntityBehaviour, INotificationHolder, ISpriteEntity
    {
        internal static readonly GameObject BaseFace;
        internal static readonly GameObject BaseWall;

        private static Vector2 cachedTopPosition;
        private static readonly Mesh XMesh;
        private static readonly Mesh YMesh;
        private static readonly ILogger<Wall> Logger = LogFactory.LoggingInstance<Wall>();

        protected static bool XFacingWest;
        protected static bool YFacingSouth;
        protected static Vector2 SortingOrderMaskX;
        protected static Vector2 SortingOrderMaskY;

        private const string SortingLayer = "Entity";

        /// <summary>
        /// Initializes static members of the <see cref="Wall"/> class.
        /// Stores some base values.
        /// </summary>
        static Wall()
        {
            BaseFace = new GameObject("baseFace", typeof(SpriteRenderer));
            BaseWall = new GameObject("baseWall", typeof(MeshRenderer), typeof(MeshFilter));

            // Move behind camera
            // Possible to disable, but requires more code after Object.Instantiate calls
            BaseFace.transform.position = BaseWall.transform.position = new Vector3(0, 0, -100);

            // Sets material
            var wallRenderer = BaseWall.GetComponent<MeshRenderer>();
            wallRenderer.sharedMaterial = EmbeddedData.WallMaterial;
            wallRenderer.allowOcclusionWhenDynamic = false;
            wallRenderer.shadowCastingMode = ShadowCastingMode.Off;
            BaseFace.GetComponent<SpriteRenderer>().sharedMaterial = EmbeddedData.GameEntityMaterial;

            // Sets sorting layers
            wallRenderer.sortingLayerName = SortingLayer;
            BaseWall.layer = LayerMask.NameToLayer("Wall");
            BaseFace.GetComponent<SpriteRenderer>().sortingLayerName = SortingLayer;
            BaseFace.layer = LayerMask.NameToLayer("Wall");
            cachedTopPosition = Vector2.zero;
            YMesh = Mesh();
            YMesh.MarkDynamic();
            YMesh.Optimize();
            XMesh = Mesh();
            XMesh.MarkDynamic();
            XMesh.Optimize();
            PreRotation();
        }

        protected static Mesh Mesh() => new Mesh()
        {
            vertices = Vertices(),
            triangles = Triangles,
            uv = Uv,
        };

        private static Vector3[] Vertices() => new[]
            {
                new Vector3(0, 0, 0),
                new Vector3(1, 0, 0),
                new Vector3(0, 1, 0),
                new Vector3(1, 1, 0),
            };

        private static readonly int[] Triangles = {
            0, 1, 2, 1, 2, 3,
        };

        private static readonly Vector2[] Uv = {
            new Vector2(0, 0),
            new Vector2(1, 0),
            new Vector2(0, 1),
            new Vector2(1, 1),
        };

        /// <summary>
        ///     Used before Instance members are rotated
        ///     Caches the top side position to help CPU
        /// </summary>
        internal static void PreRotation()
        {
            cachedTopPosition = new Vector2(Game.Map.Map.Rotation.Sin, Game.Map.Map.Rotation.Cos);

            // These values are very arbitrary, do not tamper with them unless you know what you are doing. 
            // Also don't ask Mason how because he doesn't know
            YMesh.SetVertices(new[]
            {
                new Vector3(cachedTopPosition.x, 1 - cachedTopPosition.y),
                new Vector3(1 + cachedTopPosition.x, 1 - cachedTopPosition.y),
                new Vector3(0, 1),
                new Vector3(1, 1),
            });
            YMesh.RecalculateBounds();
            YMesh.Optimize();
            XMesh.SetVertices(new[]
            {
                new Vector3(0 - cachedTopPosition.y, 1 - cachedTopPosition.x),
                new Vector3(1 - cachedTopPosition.y, 1 - cachedTopPosition.x),
                new Vector3(0, 1),
                new Vector3(1, 1),
            });
            XMesh.RecalculateBounds();
            XMesh.Optimize();

            // Determine which face will be rendered
            XFacingWest = Game.Map.Map.Rotation.Deg is not (> 0 and < 180);
            SortingOrderMaskX = new Vector2(XFacingWest ? -0.5f : 0.5f, 0);

            YFacingSouth = Game.Map.Map.Rotation.Deg is > 270 or < 90;
            SortingOrderMaskY = new Vector2(0, YFacingSouth ? -0.5f : 0.5f);
        }

        internal enum Faces : byte
        {
            North,
            South,
            East,
            West,
        }

        public DamageNotification DamageNotification { get; set; }

        public Canvas Canvas { get; private set; }

        internal WallGameObjects GameObjects;

        [SerializeField]
        private bool xFaceActive;
        [SerializeField]
        private bool yFaceActive;

        [SerializeField]
        protected bool[] disabledFaces;

        private bool[] darkFaces;

        public MonoBehaviour Behaviour
        {
            get => this;
            set { }
        }

        public float SpriteCycleSpeed => ObjectDesc.Texture.CycleSpeed;

        public SpriteRenderer SpriteRendererProperty => GameObjects.TopRenderer;

        private MaterialPropertyBlock northMaterialPropertyBlock;
        private MaterialPropertyBlock southMaterialPropertyBlock;
        private MaterialPropertyBlock eastMaterialPropertyBlock;
        private MaterialPropertyBlock westMaterialPropertyBlock;

        private static readonly Vector3 Rot90 = new Vector3(0, 0, 90);

        internal void Init(ObjectStats stats, ObjectDescription desc, GameWorldManager manager, WallGameObjects gameObjects)
        {
            Init(stats, desc, manager, true);

            // Initialize array of disabled faces
            disabledFaces = new bool[Enum.GetNames(typeof(Faces)).Length];
            darkFaces = new bool[Enum.GetNames(typeof(Faces)).Length];
            northMaterialPropertyBlock = new MaterialPropertyBlock();
            southMaterialPropertyBlock = new MaterialPropertyBlock();
            eastMaterialPropertyBlock = new MaterialPropertyBlock();
            westMaterialPropertyBlock = new MaterialPropertyBlock();
            GameObjects = gameObjects;

            // Initialize Walls
            GameObjects.XMeshFilter.sharedMesh = XMesh;
            var textureIndex = desc.Textures.Length > 1 ? manager.GlobalRandom.Next(0, desc.Textures.Length) : 0;
            GameObjects.XFace.transform.eulerAngles = Rot90;
            GameObjects.YMeshFilter.sharedMesh = YMesh;

            // Sets top sprite if it is different than walls
            GameObjects.TopRenderer.sprite = desc.TopTextures[0] != null ? desc.TopTextures[0] : desc.Textures[textureIndex];

            // Calculates Sides
            Rebuild();

            SetSprite();
        }

        internal void OnEnable() => SetSprite();

        internal void SetState(bool state)
        {
            if (GameObjects.TopFace.activeSelf != state)
            {
                GameObjects.TopFace.SetActive(state);
            }
        }

        internal void EnableFaces()
        {
            for (var i = 0; i < disabledFaces.Length; i++)
            {
                disabledFaces[i] = false;
            }
        }

        internal void DisableFace(Faces face) => disabledFaces[(int)face] = true;

        internal void DarkenFace(Faces face, bool value)
        {
            darkFaces[(int)face] = value;
            switch (face)
            {
                case Faces.East:
                    eastMaterialPropertyBlock.SetInteger(HideTileHash, value ? 1 : 0);
                    break;
                case Faces.South:
                    southMaterialPropertyBlock.SetInteger(HideTileHash, value ? 1 : 0);
                    break;
                case Faces.North:
                    northMaterialPropertyBlock.SetInteger(HideTileHash, value ? 1 : 0);
                    break;
                case Faces.West:
                    westMaterialPropertyBlock.SetInteger(HideTileHash, value ? 1 : 0);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(face), face, null);
            }
        }

        protected virtual Vector3 PositionTopSide()
        {
            var topSidePosition = new Vector3(ServerPosition.x - cachedTopPosition.x, ServerPosition.y + cachedTopPosition.y, 0);
            GameObjects.TopFace.transform.position = topSidePosition;
            return topSidePosition;
        }

        protected virtual void PositionXSide(Vector3 topPosition) => GameObjects.XFace.transform.position = topPosition + new Vector3(XFacingWest ? 0.5f : 1.5f, -0.5f);

        protected virtual void PositionYSide(Vector3 topPosition) => GameObjects.YFace.transform.position = topPosition + new Vector3(-0.5f, YFacingSouth ? -1.5f : -0.5f);

        /// <summary>
        ///     Moves sides according to cached data,
        ///     Disables a side if necessary
        /// </summary>
        private static readonly int HideTileHash = Shader.PropertyToID("_HideTile");

        internal void Rebuild()
        {
            var topSidePosition = PositionTopSide();

            xFaceActive = (!XFacingWest && !disabledFaces[(int)Faces.East]) || (XFacingWest && !disabledFaces[(int)Faces.West]);
            if (xFaceActive)
            {
                PositionXSide(topSidePosition);
            }

            GameObjects.XFace.SetActive(xFaceActive);

            yFaceActive = (!YFacingSouth && !disabledFaces[(int)Faces.North]) || (YFacingSouth && !disabledFaces[(int)Faces.South]);
            if (yFaceActive)
            {
                PositionYSide(topSidePosition);
            }

            GameObjects.YFace.SetActive(yFaceActive);

            if (Canvas != null)
            {
                Canvas.transform.eulerAngles = Game.Map.Map.Rotation.ToUnity3();
            }

            UpdateMaterial();
        }

        protected override void Update()
        {
            base.Update();

            UpdateXFaceSortingOrder();
            UpdateYFaceSortingOrder();
            UpdateTopFaceSortingOrder();
        }

        protected override void RenderVisualPosition()
        {
        }

        private void UpdateTopFaceSortingOrder()
        {
            var newTopSortingOrder = Manager.Map.SortingOrder.Calculate(Position.ToUnity2());

            if (GameObjects.TopSortingOrderCached == newTopSortingOrder)
            {
                return;
            }

            GameObjects.TopSortingOrderCached = newTopSortingOrder;
            GameObjects.TopRenderer.sortingOrder = newTopSortingOrder;
        }

        private void UpdateYFaceSortingOrder()
        {
            if (!yFaceActive)
            {
                return;
            }

            var newSortingOrder = Manager.Map.SortingOrder.Calculate(Position.ToUnity2() + SortingOrderMaskY);

            if (GameObjects.YSortingOrderCached == newSortingOrder)
            {
                return;
            }

            GameObjects.YSortingOrderCached = newSortingOrder;
            GameObjects.YMeshRenderer.sortingOrder = newSortingOrder;
        }

        private void UpdateXFaceSortingOrder()
        {
            if (!xFaceActive)
            {
                return;
            }

            var newSortingOrder = Manager.Map.SortingOrder.Calculate(Position.ToUnity2() + SortingOrderMaskX);

            if (GameObjects.XSortingOrderCached == newSortingOrder)
            {
                return;
            }

            GameObjects.XSortingOrderCached = newSortingOrder;
            GameObjects.XMeshRenderer.sortingOrder = newSortingOrder;
        }

        private void UpdateMaterial()
        {
            switch (XFacingWest)
            {
                case false when !disabledFaces[(int)Faces.East]:
                    GameObjects.XMeshRenderer.SetPropertyBlock(eastMaterialPropertyBlock);
                    break;
                case true when !disabledFaces[(int)Faces.West]:
                    GameObjects.XMeshRenderer.SetPropertyBlock(westMaterialPropertyBlock);
                    break;
            }

            switch (YFacingSouth)
            {
                case false when !disabledFaces[(int)Faces.North]:
                    GameObjects.YMeshRenderer.SetPropertyBlock(northMaterialPropertyBlock);
                    break;
                case true when !disabledFaces[(int)Faces.South]:
                    GameObjects.YMeshRenderer.SetPropertyBlock(southMaterialPropertyBlock);
                    break;
            }
        }

        private static readonly int TintHash = Shader.PropertyToID("_Tint");

        internal void SetAlpha(float alpha)
        {
            GameObjects.TopRenderer.color = new Color(1, 1, 1, alpha);

            const float sideWallDim = 225f / 255f;
            var newColor = new Color(sideWallDim, sideWallDim, sideWallDim, alpha);
            var regularColor = new Color(sideWallDim, sideWallDim, sideWallDim, 1f);

            northMaterialPropertyBlock.SetColor(
                TintHash,
                !darkFaces[(int)Faces.North]
                    ? newColor
                    : regularColor);

            southMaterialPropertyBlock.SetColor(
                TintHash,
                !darkFaces[(int)Faces.South]
                     ? newColor
                     : regularColor);

            eastMaterialPropertyBlock.SetColor(
                TintHash,
                !darkFaces[(int)Faces.East]
                    ? newColor
                    : regularColor);

            westMaterialPropertyBlock.SetColor(
                TintHash,
                !darkFaces[(int)Faces.West]
                    ? newColor
                    : regularColor);

            UpdateMaterial();
        }

        internal void RemoveAlpha()
        {
            GameObjects.TopRenderer.color = Color.white;

            const float sideWallDim = 225f / 255f;
            var regularColor = new Color(sideWallDim, sideWallDim, sideWallDim, 1f);

            northMaterialPropertyBlock.SetColor(TintHash, regularColor);
            southMaterialPropertyBlock.SetColor(TintHash, regularColor);
            eastMaterialPropertyBlock.SetColor(TintHash, regularColor);
            westMaterialPropertyBlock.SetColor(TintHash, regularColor);

            UpdateMaterial();
        }

        internal void Damage(ushort damage)
        {
            if (Canvas == null)
            {
                Canvas = Instantiate(GamePrefabs.GameEntityCanvas).GetComponent<Canvas>();
                Canvas.transform.SetParent(GameObjects.TopFace.transform);
                Canvas.worldCamera = UnityEngine.Camera.main;
                Canvas.transform.localPosition = Vector3.zero;
                Canvas.sortingLayerName = "UI";
                Canvas.transform.eulerAngles = Game.Map.Map.Rotation.ToUnity3();
            }

            if (DamageNotification != null && DamageNotification.Stackable)
            {
                DamageNotification.UpdateText(damage);
            }
            else
            {
                EntityFactory.CreateNotification<DamageNotification>(this, damage.ToString(), GameColors.NOALPHA_RED);
            }
        }

        internal void OnDestroy()
        {
            if (Canvas != null)
            {
                Destroy(Canvas.gameObject);
            }
        }
    }
}