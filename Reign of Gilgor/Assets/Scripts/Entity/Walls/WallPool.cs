﻿using System.Collections.Generic;
using Client.Entity;
using RoGCore.Assets;
using RoGCore.Models;
using UnityEngine;

namespace Client
{
    internal sealed class WallPool : Initializable<WallPool>
    {
        public override bool NeedsGameData() => true;

        private const int GenerateAmount = 50;

        private readonly List<WallGameObjects> heldObjects = new List<WallGameObjects>(GenerateAmount);

        public override void Initialize()
        {
            Instance = this;
            for (var i = 0; i < GenerateAmount; i++)
            {
                AddWallToPool();
            }
        }

        private void AddWallToPool()
        {
            var go = new WallGameObjects()
            {
                TopFace = Instantiate(Wall.BaseFace),
                XFace = Instantiate(Wall.BaseWall),
                YFace = Instantiate(Wall.BaseWall),
            };

            go.XMeshRenderer = go.XFace.GetComponent<MeshRenderer>();
            go.YMeshRenderer = go.YFace.GetComponent<MeshRenderer>();
            go.XMeshFilter = go.XFace.GetComponent<MeshFilter>();
            go.YMeshFilter = go.YFace.GetComponent<MeshFilter>();
            go.TopRenderer = go.TopFace.GetComponent<SpriteRenderer>();
            go.TopFace.transform.SetParent(gameObject.transform);
            go.XFace.transform.SetParent(go.TopFace.transform);
            go.YFace.transform.SetParent(go.TopFace.transform);
            heldObjects.Add(go);
        }

        private static WallGameObjects GetWallGameObjects()
        {
            if (Instance.heldObjects.Count == 0)
            {
                Instance.AddWallToPool();
            }

            return Instance.heldObjects[0];
        }

        internal static Wall CreateWall(ObjectStats stats, ObjectDescription desc, GameWorldManager manager)
        {
            var gameObjects = GetWallGameObjects();
            Instance.heldObjects.Remove(gameObjects);
            gameObjects.TopFace.SetActive(true);

            var wall = gameObjects.TopFace.AddComponent<Wall>();
            wall.Init(stats, desc, manager, gameObjects);

            return wall;
        }

        internal static HalfWall CreateHalfWall(ObjectStats stats, ObjectDescription desc, GameWorldManager manager)
        {
            var gameObjects = GetWallGameObjects();
            Instance.heldObjects.Remove(gameObjects);
            gameObjects.TopFace.SetActive(true);

            var wall = gameObjects.TopFace.AddComponent<HalfWall>();
            wall.Init(stats, desc, manager, gameObjects);

            return wall;
        }

        internal static void DestroyWall(Wall obj)
        {
            obj.GameObject.SetActive(false);

            Instance.heldObjects.Add(obj.GameObjects);

            Destroy(obj);
        }

        internal struct WallGameObjects
        {
            internal GameObject TopFace;
            internal GameObject XFace;
            internal GameObject YFace;
            internal MeshRenderer XMeshRenderer;
            internal MeshRenderer YMeshRenderer;
            internal SpriteRenderer TopRenderer;
            internal MeshFilter XMeshFilter;
            internal MeshFilter YMeshFilter;
            internal int XSortingOrderCached;
            internal int YSortingOrderCached;
            internal int TopSortingOrderCached;
        }
    }
}