﻿using Client.Entity.Helpers;
using Microsoft.Extensions.Logging;
using RoGCore.Assets;
using UnityEngine;

namespace Client.Entity
{
    internal partial class Wall
    {
        private void SetSprite()
        {
            if (ObjectDesc is null)
            {
                return;
            }

            switch (ObjectDesc.Texture.Type)
            {
                case TextureData.TEXTURETYPECYCLE:
                case TextureData.TEXTURETYPERANDOMCYCLE:
                case TextureData.TEXTURETYPEPINGPONG:
                    StartCoroutine(this.SpriteCycle(ObjectDesc, Manager));
                    break;
                case TextureData.TEXTURETYPERANDOM:
                    SetSideSpriteByIndex(Manager.GlobalRandom.Next(0, ObjectDesc.Textures.Length));
                    break;
                case TextureData.TEXTURETYPEANIMATED:
                    SetSideSpriteByIndex(0);
                    Logger.LogError($"Walls cannot use animated textures. Entity: {ObjectDesc.Name}");
                    break;
                default:
                    SetSideSpriteByIndex(0);
                    break;
            }
        }

        private static readonly int OffsetHash = Shader.PropertyToID("_Offset");

        public void SetSideSpriteByIndex(int textureIndex)
        {
            Texture texture = ObjectDesc.Textures[textureIndex].texture;
            var textureRect = ObjectDesc.Textures[textureIndex].textureRect;

            float xOffset = (int)textureRect.x;
            float yOffset = (int)textureRect.y;

            var offset = new Vector4(xOffset / texture.width, yOffset / texture.height, 0, 0);

            northMaterialPropertyBlock.SetVector(OffsetHash, offset);
            southMaterialPropertyBlock.SetVector(OffsetHash, offset);
            eastMaterialPropertyBlock.SetVector(OffsetHash, offset);
            westMaterialPropertyBlock.SetVector(OffsetHash, offset);
            UpdateMaterial();
        }
    }
}
