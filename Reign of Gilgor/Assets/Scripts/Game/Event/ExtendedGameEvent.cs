﻿#nullable enable

namespace Client.Game.Event
{
    internal sealed class ExtendedGameEvent<T> : GameEvent<T>
    {
        internal event GameEventHandler? End;

        internal bool InProgress;

        internal override void Invoke(T sender)
        {
            InProgress = true;
            base.Invoke(sender);
        }

        internal void Stop(T sender)
        {
            InProgress = false;
            End?.Invoke(sender);
        }
    }
}
