﻿#nullable enable

namespace Client.Game.Event
{
    internal class GameEvent<T>
    {
        internal T? LastValue;

        internal delegate void GameEventHandler(T sender);

        internal event GameEventHandler? Begin;

        internal virtual void Invoke(T sender)
        {
            LastValue = sender;
            Begin?.Invoke(sender);
        }
    }
}
