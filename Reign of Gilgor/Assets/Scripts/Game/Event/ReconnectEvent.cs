﻿#nullable enable

namespace Client.Game.Event
{
    internal static class ReconnectEvent
    {
        internal delegate void ReconnectEventHandler();

        internal static event ReconnectEventHandler? Begin;

        internal static void Invoke() => Begin?.Invoke();
    }
}
