using UnityEngine;

namespace Client.Game
{
    internal sealed class FollowRotation : MonoBehaviour
    {
        [SerializeField]
        private bool reverse;

        [SerializeField]
        private Transform followTransform;

        private new Transform transform;

        private void Awake() => transform = base.transform;

        private void Update() => transform.eulerAngles = reverse ? -followTransform.eulerAngles : followTransform.eulerAngles;
    }
}