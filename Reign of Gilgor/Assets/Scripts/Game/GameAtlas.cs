﻿using System.Collections.Generic;
using System.Linq;
using Client.Structures;
using Microsoft.Extensions.Logging;
using RoGCore.Logging;
using UnityEngine;

#nullable enable

namespace Client.Game
{
    internal class GameAtlas
    {
        private const int Padding = 1;

        private static readonly ILogger<GameAtlas> Logger = LogFactory.LoggingInstance<GameAtlas>();
        private static readonly int MaskIndex = Shader.PropertyToID("_Mask");
        private static readonly int ShadowMaskIndex = Shader.PropertyToID("_ShadowMask");

        private readonly Texture2D atlas;
        private readonly Texture2D shadowAtlas;
        private readonly Texture2D? secondaryAtlas;

        private readonly int atlasSize;

        // Uses hashcode to search for items in dictionary faster.
        // Race condition where an incorrect sprite is returned.
        private readonly Dictionary<int, Sprite> dictionary = new Dictionary<int, Sprite>();

        private readonly List<RectInt> occupiedSpace = new List<RectInt>();

        private readonly Dictionary<Vector2Int, Vector2Int> searchCache = new Dictionary<Vector2Int, Vector2Int>();

        private int updateCount;

        internal GameAtlas(int size, bool useSecondaryTextures)
        {
            atlasSize = size;

            atlas = new Texture2D(atlasSize, atlasSize, TextureFormat.ARGB32, false)
            {
                filterMode = FilterMode.Point,
            };

            shadowAtlas = new Texture2D(atlasSize, atlasSize, TextureFormat.ARGB32, false)
            {
                filterMode = FilterMode.Point,
            };

            var clearColors = Utils.ColorExtensions.GetBlankColors(Color.clear, atlasSize * atlasSize);

            atlas.SetPixels32(clearColors);
            shadowAtlas.SetPixels32(clearColors);

            if (useSecondaryTextures)
            {
                secondaryAtlas = new Texture2D(atlasSize, atlasSize, TextureFormat.ARGB32, false)
                {
                    filterMode = FilterMode.Point,
                };

                secondaryAtlas.SetPixels32(clearColors);
            }
        }

        internal bool Add(string fileAlias, int spriteIndex, in SpriteSheet spriteSheet)
        {
            var hashCode = GetHashCode(fileAlias, spriteIndex);

            if (dictionary.ContainsKey(hashCode))
            {
                Logger.LogWarning("Atlas cannot add sprite because the sprite is already in the atlas");
                return false;
            }

            var sprite = spriteSheet.Sprites[spriteIndex];

            var spriteWidth = (int)sprite.textureRect.width;
            var spriteHeight = (int)sprite.textureRect.height;

            var colors = sprite.texture.GetPixels((int)sprite.textureRect.x, (int)sprite.textureRect.y, spriteWidth, spriteHeight);

            if (!colors.Any(color => color.a > 0))
            {
                return false;
            }

            if (spriteSheet.Type == SpriteSheet.SpriteSheetType.Expand)
            {
                colors = Utils.ColorExtensions.Expand(colors);
                spriteWidth += 2;
                spriteHeight += 2;
            }

            var paddedSpriteWidth = spriteWidth + (2 * Padding);
            var paddedSpriteHeight = spriteHeight + (2 * Padding);

            var searchKey = new Vector2Int(spriteWidth, spriteHeight);
            var lastSearch = Vector2Int.zero;
            searchCache.TryGetValue(searchKey, out lastSearch);

            var atlasRect = FindAtlasSpot(paddedSpriteWidth, paddedSpriteHeight, ref searchKey, ref lastSearch);

            if (atlasRect is null)
            {
                Logger.LogError("Atlas is out of space");
                return false;
            }

            var atlasRectValue = new RectInt(atlasRect.Value.x, atlasRect.Value.y, atlasRect.Value.width, atlasRect.Value.height);

            AddToOccupiedSpace(atlasRectValue);

            if (updateCount % 10 == 0)
            {
                CleanupOccupiedSpace();
            }

            atlas.SetPixels(atlasRectValue.x + Padding, atlasRectValue.y + Padding, atlasRectValue.width - (2 * Padding), atlasRectValue.height - (2 * Padding), colors);

            if (spriteSheet.Outline)
            {
                AddShadowMask(ref atlasRectValue);
            }

            TryAddSecondaryTexture(sprite, spriteWidth, spriteHeight, ref atlasRectValue);

            var pivot = new Vector2(-sprite.bounds.center.x / sprite.bounds.extents.x, -sprite.bounds.center.y / sprite.bounds.extents.y) * 0.5f;
            pivot += new Vector2(0.5f, 0.5f);
            if (spriteSheet.Type == SpriteSheet.SpriteSheetType.Expand)
            {
                atlasRectValue = new RectInt(atlasRectValue.x + Padding + 1, atlasRectValue.y + Padding + 1, atlasRectValue.width - (2 * Padding) - 2, atlasRectValue.height - (2 * Padding) - 2);
            }
            else
            {
                pivot *= new Vector2(spriteWidth, spriteHeight);
                pivot += Vector2.one;
                pivot /= new Vector2(atlasRectValue.width, atlasRectValue.height);
            }

            dictionary.Add(hashCode, Sprite.Create(atlas, new Rect(atlasRectValue.x, atlasRectValue.y, atlasRectValue.width, atlasRectValue.height), pivot, sprite.pixelsPerUnit, 0, SpriteMeshType.Tight));

            updateCount++;

            return true;
        }

        internal void Apply(Material[] mainTextureMaterials, Material[] secondaryTextureMaterials, Material[] shadowMaskMaterials)
        {
            atlas.Apply();
            foreach (var material in mainTextureMaterials)
            {
                material.SetTexture("_Main", atlas);
            }

            shadowAtlas.Apply();
            foreach (var material in shadowMaskMaterials)
            {
                material.SetTexture(ShadowMaskIndex, shadowAtlas);
            }

            if (secondaryAtlas != null)
            {
                secondaryAtlas.Apply();

                foreach (var material in secondaryTextureMaterials)
                {
                    material.SetTexture(MaskIndex, secondaryAtlas);
                }
            }

            /*string fileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), $"RoG/GameAtlas{atlasSize}.png");

            File.WriteAllBytes(fileName, shadowAtlas.EncodeToPNG());*/
        }

        internal Sprite? GetSprite(string fileAlias, int spriteIndex)
        {
            var hashCode = GetHashCode(fileAlias, spriteIndex);

            return dictionary.TryGetValue(hashCode, out var ret) ? ret : null;
        }

        private void TryAddSecondaryTexture(Sprite sprite, int spriteWidth, int spriteHeight, ref RectInt atlasRectValue)
        {
            if (secondaryAtlas != null && sprite.GetSecondaryTexture(0) is Texture2D secondaryTexture)
            {
                var secondaryColors = secondaryTexture.GetPixels((int)sprite.textureRect.x, (int)sprite.textureRect.y, spriteWidth, spriteHeight);
                secondaryAtlas.SetPixels(atlasRectValue.x + Padding, atlasRectValue.y + Padding, atlasRectValue.width - (2 * Padding), atlasRectValue.height - (2 * Padding), secondaryColors);
            }
        }

        private void AddShadowMask(ref RectInt atlasRectValue)
        {
            var shadowColors = Utils.ColorExtensions.GetBlankColors(Color.white, atlasRectValue.width * atlasRectValue.height);
            shadowAtlas.SetPixels32(atlasRectValue.x, atlasRectValue.y, atlasRectValue.width, atlasRectValue.height, shadowColors);
        }

        private RectInt? FindAtlasSpot(int paddedSpriteWidth, int paddedSpriteHeight, ref Vector2Int searchKey, ref Vector2Int lastSearch)
        {
            for (var y = lastSearch.y; y <= atlasSize - paddedSpriteHeight; y++)
            {
                for (var x = y == lastSearch.y ? lastSearch.x : 0; x <= atlasSize - paddedSpriteWidth; x++)
                {
                    var hitCheck = new RectInt(x, y, paddedSpriteWidth, paddedSpriteHeight);

                    if (!occupiedSpace.Any(occupiedRect => occupiedRect.Overlaps(hitCheck)))
                    {
                        searchCache[searchKey] = new Vector2Int(x + 1, y);
                        return hitCheck;
                    }
                }
            }

            return null;
        }

        private void CleanupOccupiedSpace()
        {
            var optimizedInLastRun = true;

            while (optimizedInLastRun)
            {
                optimizedInLastRun = false;
                foreach (var rect in new List<RectInt>(occupiedSpace))
                {
                    occupiedSpace.Remove(rect);

                    if (AddToOccupiedSpace(rect))
                    {
                        optimizedInLastRun = true;
                    }
                }
            }
        }

        private bool AddToOccupiedSpace(RectInt rect)
        {
            for (var i = occupiedSpace.Count - 1; i >= 0; i--)
            {
                var a = occupiedSpace[i];

                if (a.xMax == rect.x && a.y == rect.y && a.height == rect.height)
                {
                    occupiedSpace[i] = new RectInt(a.x, a.y, a.width + rect.width, a.height);
                    return true;
                }

                if (a.yMax == rect.y && a.x == rect.x && a.width == rect.width)
                {
                    occupiedSpace[i] = new RectInt(a.x, a.y, a.width, a.height + rect.height);
                    return true;
                }
            }

            occupiedSpace.Add(rect);
            return false;
        }

        private int GetHashCode(string fileAlias, int spriteIndex)
        {
            var hashCode = -1851648953;
            hashCode = (hashCode * -1521134295) + fileAlias.GetHashCode();
            hashCode = (hashCode * -1521134295) + spriteIndex;
            return hashCode;
        }
    }
}
