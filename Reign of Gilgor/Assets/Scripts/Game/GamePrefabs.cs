﻿using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

internal static class GamePrefabs
{
    private static readonly Dictionary<string, GameObject> PrefabCache = new Dictionary<string, GameObject>();

    static GamePrefabs()
    {
        HpOutline = Resources.Load<GameObject>("Prefabs/HpOutline");
        OptionLine = Resources.Load<GameObject>("Prefabs/OptionLine");
        OptionSlider = Resources.Load<GameObject>("Prefabs/OptionSlider");
        GameEntityCanvas = Resources.Load<GameObject>("Prefabs/GameEntityCanvas");
        GameEntityNameText = Resources.Load<GameObject>("Prefabs/GameEntityNameText");
        MessagePopup = Resources.Load<GameObject>("Prefabs/MessagePopup");
        GameEntityShadow = Resources.Load<GameObject>("Prefabs/GameEntityShadow");
        TossedObject = Resources.Load<GameObject>("Prefabs/TossedObject");
        ItemHolder = Resources.Load<GameObject>("Prefabs/ItemHolder");
        SilverCoin = Resources.Load<GameObject>("Prefabs/SilverCoin");
        SilverStatusText = Resources.Load<GameObject>("Prefabs/SilverStatusText");
        TitleCharacterButton = Resources.Load<GameObject>("Prefabs/TitleScreen/CharacterButton");
        TitleNewCharacterButton = Resources.Load<GameObject>("Prefabs/TitleScreen/NewCharacterButton");
    }

    public static GameObject HpOutline { get; set; }

    public static GameObject OptionLine { get; set; }

    public static GameObject OptionSlider { get; set; }

    public static GameObject GameEntityCanvas { get; set; }

    public static GameObject GameEntityNameText { get; set; }

    public static GameObject MessagePopup { get; set; }

    public static GameObject GameEntityShadow { get; set; }

    public static GameObject TossedObject { get; set; }

    public static GameObject ItemHolder { get; set; }

    public static GameObject SilverCoin { get; set; }

    public static GameObject SilverStatusText { get; set; }

    public static GameObject TitleCharacterButton { get; set; }

    public static GameObject TitleNewCharacterButton { get; set; }

    internal static GameObject GetObject(string objectName, bool useCache = true)
    {
        if (!useCache)
        {
            var prop = typeof(GamePrefabs).GetProperty(objectName, BindingFlags.Public | BindingFlags.Static);
            return (GameObject)prop.GetValue(null, null);
        }

        if (PrefabCache.TryGetValue(objectName, out var cachedObject))
        {
            return cachedObject;
        }

        var loadedObject = Resources.Load<GameObject>("Prefabs/" + objectName);
        PrefabCache.Add(objectName, loadedObject);
        return loadedObject;
    }
}