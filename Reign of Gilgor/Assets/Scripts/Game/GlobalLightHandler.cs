﻿using UnityEngine;

namespace Client.Game
{
    internal sealed class GlobalLightHandler : Initializable<GlobalLightHandler>
    {
        [SerializeField]
        private UnityEngine.Rendering.Universal.Light2D globalLight;
        [SerializeField]
        private bool editorEditingOnly = false;

        internal float Intensity => globalLight.intensity;

        internal void SetGlobalLight(float value)
        {
            if (editorEditingOnly || !globalLight)
            {
                return;
            }

            globalLight.intensity = value;
        }
    }
}