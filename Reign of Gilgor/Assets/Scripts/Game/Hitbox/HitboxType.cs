﻿namespace Client.Game.Hitbox
{
    internal enum HitboxType : byte
    {
        Square,
        Circle,
    }
}