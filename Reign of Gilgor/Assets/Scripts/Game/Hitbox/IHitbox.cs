﻿using UnityEngine;

namespace Client.Game.Hitbox
{
    internal interface IHitbox
    {
        HitboxType HitboxType { get; }

        int HitboxSize { get; }

        Vector2 HitboxCenter { get; }
    }
}
