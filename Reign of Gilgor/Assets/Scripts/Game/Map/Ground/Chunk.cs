﻿using System.Collections;
using System.Collections.Generic;
using RoGCore.Models;

#nullable enable

namespace Client.Game.Map.Ground
{
    public sealed partial class Chunk<T> : IEnumerable<PositionalObject<T>>
        where T : class
    {
        private readonly int chunkSize;
        private readonly PositionalObject<T>[] chunkData;

        public Chunk(int size)
        {
            chunkSize = size;
            chunkData = new PositionalObject<T>[chunkSize * chunkSize];
        }

        public int Count { get; private set; }

        public T? this[in Vector2Int position]
        {
            get => chunkData[position.ChunkDataIndex(chunkSize)].Object;
            set
            {
                var index = position.ChunkDataIndex(chunkSize);

                if (value is not null)
                {
                    Count++;
                }

                if (chunkData[index].Object is not null)
                {
                    Count--;
                }

                chunkData[index] = new PositionalObject<T>(position, value);
            }
        }

        public void Clear()
        {
            Count = 0;

            for (var i = 0; i < chunkData.Length; i++)
            {
                chunkData[i] = new PositionalObject<T>(chunkData[i].Position, null);
            }
        }

        public IEnumerator<PositionalObject<T>> GetEnumerator()
        {
            if (Count == 0)
            {
                yield break;
            }

            // Count can change during enumeration if the chunk is being modified
            var originalCount = Count;
            var returned = 0;
            for (var i = 0; i < chunkData.Length; i++)
            {
                var obj = chunkData[i];

                if (obj.Object is not null)
                {
                    yield return obj;
                    returned++;

                    if (returned == originalCount)
                    {
                        yield break;
                    }
                }
            }
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
