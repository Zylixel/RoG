﻿using System.Collections;
using System.Collections.Generic;
using RoGCore.Vector;
using Vector2Int = RoGCore.Models.Vector2Int;

#nullable enable

namespace Client.Game.Map.Ground
{
    public sealed class ChunkMap<T> : IEnumerable<T>
        where T : class
    {
        private readonly int chunkSize;
        private readonly int mapSize;
        private readonly int mapSizeSqr;
        private readonly Chunk<T>[] chunkMap;

        public ChunkMap(int mapSize, int chunkSize)
        {
            this.mapSize = mapSize;
            this.chunkSize = chunkSize;
            mapSizeSqr = mapSize * mapSize;
            chunkMap = new Chunk<T>[mapSizeSqr];

            for (var i = 0; i < mapSizeSqr; i++)
            {
                chunkMap[i] = new Chunk<T>(chunkSize);
            }
        }

        public T? this[in Vector2Int position]
        {
            get
            {
                var containingChunk = GetContainingChunk(position);
                return containingChunk[position];
            }

            set
            {
                var containingChunk = GetContainingChunk(position);
                containingChunk[position] = value;
            }
        }

        public IEnumerable<T?> GetGroundMultiple(IntVector2S positions)
        {
            for (var i = 0; i < positions.Count; i++)
            {
                var containingChunk = GetContainingChunk(positions[i]);
                yield return containingChunk[positions[i]];
            }
        }

        public IEnumerable<PositionalObject<T>> GetFilledChunkObjects()
        {
            for (var i = 0; i < mapSizeSqr; i++)
            {
                foreach (var ground in chunkMap[i])
                {
                    yield return ground;
                }
            }
        }

        public void Clear()
        {
            for (var i = 0; i < mapSizeSqr; i++)
            {
                chunkMap[i].Clear();
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            for (var i = 0; i < mapSizeSqr; i++)
            {
                foreach (var ground in chunkMap[i])
                {
                    yield return ground.Object!;
                }
            }
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        private Chunk<T> GetContainingChunk(in Vector2Int position) => chunkMap[position.ChunkPosition(chunkSize).ChunkDataIndex(mapSize)];
    }
}
