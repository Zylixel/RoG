﻿using UnityEngine;
using Vector2Int = RoGCore.Models.Vector2Int;

namespace Client.Game.Map.Ground
{
    public static class ChunkVectorExtensions
    {
        public static Vector2Int VectorMod(this Vector2Int globalPosition, int chunkSize) =>
            new Vector2Int(Mod(globalPosition.X, chunkSize), Mod(globalPosition.Y, chunkSize));

        public static Vector2Int ChunkPosition(this Vector2Int globalPosition, int chunkSize) =>
            new Vector2Int(Mathf.FloorToInt(globalPosition.X / (float)chunkSize), Mathf.FloorToInt(globalPosition.Y / (float)chunkSize));

        public static int ChunkDataIndex(this Vector2Int globalPosition, int chunkSize)
        {
            var inChunkPosition = globalPosition.VectorMod(chunkSize);
            return (inChunkPosition.Y * chunkSize) + inChunkPosition.X;
        }

        private static int Mod(int k, int n) => (k %= n) < 0 ? (k + n) : k;
    }
}
