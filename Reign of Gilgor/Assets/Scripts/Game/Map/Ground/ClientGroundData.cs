using System;
using Client.Particle;
using RoGCore.Assets;
using UnityEngine;

namespace Client.Game.Map.Ground
{
    internal class ClientGroundData : IDisposable
    {
        internal GroundDescription Description;

        internal ParticleSystem ParticleSystem;

        public ClientGroundData(GroundDescription description)
        {
            this.Description = description;
        }

        public void Dispose()
        {
            if (ParticleSystem)
            {
                ParticleSystem.Stop();
                ParticleFactory.ReturnParticle(ParticleSystem);
            }

            GC.SuppressFinalize(this);
        }
    }
}