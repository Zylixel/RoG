﻿using System;
using System.Collections.Generic;
using System.Threading;
using Client.Particle;
using RoGCore.Assets;
using RoGCore.Models;
using RoGCore.Vector;
using UnityEngine;
using UnityEngine.Tilemaps;
using Vector2Int = RoGCore.Models.Vector2Int;

#nullable enable

namespace Client.Game.Map.Ground
{
    internal static class NewGroundDataHandler
    {
        private const int MaxTilesPerHandle = 5_000;

        private static readonly HashSet<Vector2Int> QueuedRenderTiles = new HashSet<Vector2Int>(MaxTilesPerHandle);
        private static readonly PositionalObject<GroundDescription>[] RenderList = new PositionalObject<GroundDescription>[MaxTilesPerHandle];

        private static readonly TileSpriteData[] CalculatedTileSpriteData = new TileSpriteData[MaxTilesPerHandle];
        private static readonly Tile?[] CalculatedTiles = new Tile[MaxTilesPerHandle];
        private static readonly CountdownEvent CountdownEvent = new CountdownEvent(0);
        private static readonly TileManager TileManager = new TileManager();

        internal static void HandleNewTiles(in SizedArray<RoGCore.Models.TileData> tiles, GameWorldManager manager)
        {
            int i;
            var renderListCount = 0;
            var tilePositions = new IntVector2S(tiles.Length);

            for (i = 0; i < tiles.Length; i++)
            {
                var tile = tiles[i];
                tilePositions[i] = tile.Position;

                if (!manager.GameData.Tiles.TryGetValue(tile.Tile, out var desc))
                {
                    continue;
                }

                manager.Map.GroundMap[tile.Position] = new ClientGroundData(desc);

                QueuedRenderTiles.Add(tile.Position);
                RenderList[renderListCount++] = new PositionalObject<GroundDescription>(tile.Position, desc);
            }

            GenerateSurroundingPoints(ref tilePositions, out var surroundingPoints);

            for (i = 0; i < surroundingPoints.Length; i++)
            {
                var subPoints = surroundingPoints[i];
                for (var j = 0; j < subPoints.Count; j++)
                {
                    var point = subPoints[j];
                    if (!QueuedRenderTiles.Contains(point))
                    {
                        var ground = manager.Map.GroundMap[point];

                        if (ground is not null)
                        {
                            if (ground.Description.Name?.Contains("Sand") == true)
                            {
                                CheckWaterSplash(manager, point, ground);
                            }

                            QueuedRenderTiles.Add(point);
                            RenderList[renderListCount++] = new PositionalObject<GroundDescription>(point, ground.Description);
                        }
                    }
                }
            }

            if (renderListCount > 100)
            {
                RenderMultithreaded(manager, RenderList, renderListCount);
            }
            else
            {
                Render(manager, RenderList, renderListCount);
            }

            TileManager.Apply();
            QueuedRenderTiles.Clear();
        }

        private static void CheckWaterSplash(GameWorldManager manager, Vector2Int position, ClientGroundData ground)
        {
            foreach (var cardinalDirection in CardinalOrdinalDirections.CardinalDirections)
            {
                var cardinalTile = manager.Map.GroundMap[cardinalDirection + position];

                if (cardinalTile?.Description?.Name?.Contains("Water") == true)
                {
                    ground.ParticleSystem = ParticleFactory.CreateParticle(
                        "WaterSplashParticle",
                        new CreateParticleProperties()
                        {
                            LocalPosition = new Vector3(position.X + 0.5f, position.Y + 0.5f) + (new Vector3(cardinalDirection.X, cardinalDirection.Y) * 0.5f),
                            Rotation = Vector3.zero,
                            AutoZRotation = true,
                        });

                    return;
                }
            }
        }

        private static void GenerateSurroundingPoints(ref IntVector2S tilePositions, out IntVector2S[] surroundingPoints)
        {
            surroundingPoints = new IntVector2S[]
            {
                new IntVector2S(tilePositions.Count),
                new IntVector2S(tilePositions.Count),
                new IntVector2S(tilePositions.Count),
                new IntVector2S(tilePositions.Count),
            };

            for (var i = 0; i < 4; i++)
            {
                UnityBurstSIMD.Math.Addition.Add(tilePositions.X, CardinalOrdinalDirections.CardinalDirections[i].X, ref surroundingPoints[i].X);
                UnityBurstSIMD.Math.Addition.Add(tilePositions.Y, CardinalOrdinalDirections.CardinalDirections[i].Y, ref surroundingPoints[i].Y);
            }
        }

        private static void RenderMultithreaded(GameWorldManager manager, PositionalObject<GroundDescription>[] renderList, int renderListLength)
        {
            var threadCount = Environment.ProcessorCount;
            int i;
            var workLoad = renderListLength / threadCount;

            CountdownEvent.Reset(threadCount);
            for (i = 0; i < threadCount; i++)
            {
                var startIndex = i * workLoad;
                var endIndex = i == 7 ? renderListLength : (i + 1) * workLoad;
                ThreadPool.QueueUserWorkItem(callback =>
                {
                    for (var j = startIndex; j < endIndex; j++)
                    {
                        var tileSpriteData = new TileSpriteData(renderList[j], manager.Map);

                        CalculatedTileSpriteData[j] = tileSpriteData;
                        CalculatedTiles[j] = TileManager.GetTile(tileSpriteData, false);
                    }

                    CountdownEvent.Signal();
                });
            }

            CountdownEvent.Wait();

            for (i = 0; i < renderListLength; i++)
            {
                var calculatedTile = CalculatedTiles[i];
                if (calculatedTile == null)
                {
                    calculatedTile = TileManager.GetTile(CalculatedTileSpriteData[i]);
                }

                var ground = renderList[i];

                manager.Map.TileMap.SetTile(new Vector3Int(ground.Position.X, ground.Position.Y, 0), calculatedTile);
            }
        }

        private static void Render(GameWorldManager manager, PositionalObject<GroundDescription>[] renderList, int renderListLength)
        {
            for (var i = 0; i < renderListLength; i++)
            {
                var renderGround = renderList[i];
                var tileSpriteData = new TileSpriteData(renderGround, manager.Map);

                manager.Map.TileMap.SetTile(
                    new Vector3Int(renderGround.Position.X, renderGround.Position.Y, 0),
                    TileManager.GetTile(tileSpriteData));
            }
        }
    }
}
