﻿using System.Linq;
using UnityEngine;
using UnityEngine.Tilemaps;

#nullable enable

namespace Client.Game.Map.Ground
{
    internal sealed class TileAtlas
    {
        private const int AtlasSize = 10 * 75;

        private static readonly Color32[] SolidRed = new Color32[100];
        private static readonly Color32[] NoRed = new Color32[AtlasSize * AtlasSize];

        private readonly Texture2D atlas = new Texture2D(AtlasSize, AtlasSize, TextureFormat.RGB24, false)
        {
            filterMode = FilterMode.Point,
        };

        private readonly Texture2D maskAtlas = new Texture2D(AtlasSize, AtlasSize, TextureFormat.R8, false)
        {
            filterMode = FilterMode.Point,
        };

        private readonly TilemapRenderer tilemap;

        private int atlasIndex;

        internal TileAtlas()
        {
            tilemap = Resources.FindObjectsOfTypeAll<TilemapRenderer>().First(x => x.gameObject.name == "GameTilemap");

            Color32 red = Color.red;
            for (var j = 0; j < SolidRed.Length; j++)
            {
                SolidRed[j] = red;
            }

            Color32 clear = Color.clear;
            for (var j = 0; j < NoRed.Length; j++)
            {
                NoRed[j] = clear;
            }
        }

        internal void Apply()
        {
            maskAtlas.Apply();
            atlas.Apply();
            tilemap.sharedMaterial.SetTexture(ShaderProperties.MaskNoUnderline, maskAtlas);
        }

        internal Sprite Add(TileSpriteData tileSpriteData, Color[] tileColors)
        {
            var expandedTiles = Utils.ColorExtensions.ExpandTo32(tileColors);

            var rect = new Rect((atlasIndex * 10) % AtlasSize, Mathf.FloorToInt(atlasIndex / (AtlasSize / 10)) * 10, 10, 10);
            atlas.SetPixels32((int)rect.x, (int)rect.y, (int)rect.width, (int)rect.height, expandedTiles);

            if (tileSpriteData.Desc.Liquid)
            {
                maskAtlas.SetPixels32((int)rect.x, (int)rect.y, (int)rect.width, (int)rect.height, SolidRed);
            }

            atlasIndex = (atlasIndex + 1) % (AtlasSize * AtlasSize);

            var spriteRect = new Rect(rect.x + 1, rect.y + 1, 8, 8);
            return Sprite.Create(atlas, spriteRect, new Vector2(0.5f, 0.5f), 8f);
        }

        internal void Clear()
        {
            maskAtlas.SetPixels32(NoRed);
            atlasIndex = 0;
        }
    }
}
