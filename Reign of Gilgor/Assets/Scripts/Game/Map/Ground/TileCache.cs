using System.Collections.Concurrent;
using UnityEngine.Tilemaps;

#nullable enable

namespace Client.Game.Map.Ground
{
    internal sealed class TileCache
    {
        // Uses ground hashcode to search for items in dictionary better.
        // Race condition where an incorrect tile is returned if two distinct ground types share the same hashcode
        private readonly ConcurrentDictionary<int, Tile> cache = new ConcurrentDictionary<int, Tile>();

        internal void Clear()
        {
            cache.Clear();
        }

        internal bool TryGetValueIfFull(TileSpriteData tileSpriteData, out Tile? tile)
        {
            return cache.TryGetValue(tileSpriteData.GetHashCode(), out tile);
        }

        internal void TryAdd(TileSpriteData tileSpriteData, Tile renderTile)
        {
            cache.TryAdd(tileSpriteData.GetHashCode(), renderTile);
        }
    }
}