﻿using RoGCore.Assets;
using RoGCore.Utils;
using UnityEngine;
using UnityEngine.EventSystems;

#nullable enable

namespace Client.Game.Map.Ground
{
    internal static class TileColorGenerator
    {
        internal static Color[] Generate(in TileSpriteData tileSpriteData)
        {
            var sprite = tileSpriteData.Desc.Textures[tileSpriteData.TileTextureIndex];
            var tileColors = GetColors(sprite.texture, sprite.textureRect);

            for (var j = 0; j < tileSpriteData.RotateAmount; j++)
            {
                tileColors = RotateClockwise(tileColors, 8);
            }

            AddEdges(tileSpriteData, ref tileColors);

            return tileColors;
        }

        private static T[] RotateClockwise<T>(T[] matrix, int sideLength)
        {
            var result = new T[matrix.Length];

            Debug.Assert(sideLength * sideLength == result.Length);

            for (var i = 0; i < sideLength; ++i)
            {
                for (var j = 0; j < sideLength; ++j)
                {
                    result[(i * sideLength) + j] = matrix[((sideLength - j - 1) * sideLength) + i];
                }
            }

            return result;
        }

        private static void AddEdges(TileSpriteData tileSpriteData, ref Color[] tileColors)
        {
            if (tileSpriteData.TopDesc is not null)
            {
                AddEdge(tileSpriteData.TopDesc, MoveDirection.Up, ref tileColors);
            }

            if (tileSpriteData.BottomDesc is not null)
            {
                AddEdge(tileSpriteData.BottomDesc, MoveDirection.Down, ref tileColors);
            }

            if (tileSpriteData.LeftDesc is not null)
            {
                AddEdge(tileSpriteData.LeftDesc, MoveDirection.Left, ref tileColors);
            }

            if (tileSpriteData.RightDesc is not null)
            {
                AddEdge(tileSpriteData.RightDesc, MoveDirection.Right, ref tileColors);
            }
        }

        private static void AddEdge(GroundDescription sideDesc, MoveDirection dir, ref Color[] colors)
        {
            const int textureSideLength = 8;

            var seed = Random.Range(0, int.MaxValue);

            float GetPixelAlpha(int depth)
            {
                Random.InitState(seed - (depth % textureSideLength));
                var randomVariance = Random.value.Remap(0, 1f, 0.6f, 1f);

                if (depth > textureSideLength - 1)
                {
                    randomVariance *= 0.25f;
                }

                return randomVariance;
            }

            void ChangePixel(ref Color baseColor, Color edgeColor, int depth) => baseColor = Color.Lerp(baseColor, edgeColor, GetPixelAlpha(depth));

            var sprite = sideDesc.Textures[Random.Range(0, sideDesc.Textures.Length)];
            var edgeColors = GetColors(sprite.texture, sprite.rect);

            for (var j = 0; j < textureSideLength; j++)
            {
                switch (dir)
                {
                    case MoveDirection.Left:
                        ChangePixel(ref colors[j * textureSideLength], edgeColors[((j + 1) * textureSideLength) - 1], j);
                        ChangePixel(ref colors[(j * textureSideLength) + 1], edgeColors[((j + 1) * textureSideLength) - 1], j + textureSideLength);
                        break;
                    case MoveDirection.Right:
                        ChangePixel(ref colors[((j + 1) * textureSideLength) - 1], edgeColors[j * textureSideLength], j);
                        ChangePixel(ref colors[((j + 1) * textureSideLength) - 2], edgeColors[j * textureSideLength], j + textureSideLength);
                        break;
                    case MoveDirection.Up:
                        ChangePixel(ref colors[j + (textureSideLength * textureSideLength) - textureSideLength], edgeColors[j], j);
                        ChangePixel(ref colors[j + (textureSideLength * textureSideLength) - (2 * textureSideLength)], edgeColors[j], j + textureSideLength);
                        break;
                    case MoveDirection.Down:
                        ChangePixel(ref colors[j], edgeColors[j + (textureSideLength * textureSideLength) - textureSideLength], j);
                        ChangePixel(ref colors[j + textureSideLength], edgeColors[j + (textureSideLength * textureSideLength) - textureSideLength], j + textureSideLength);
                        break;
                }
            }
        }

        private static Color[] GetColors(Texture2D tex, Rect rect) => tex.GetPixels((int)rect.x, (int)rect.y, (int)rect.width, (int)rect.height);
    }
}
