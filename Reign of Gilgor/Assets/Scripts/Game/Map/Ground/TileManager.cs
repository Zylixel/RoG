using Client.Game.Event;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Client.Game.Map.Ground
{
    internal sealed class TileManager
    {
        private readonly TileCache tileCache = new TileCache();
        private readonly TileAtlas tileAtlas = new TileAtlas();

        public TileManager()
        {
            ReconnectEvent.Begin += () => ClearData();
        }

        internal Tile GetTile(in TileSpriteData tileSpriteData, bool generateIfNeeded = true)
        {
            tileCache.TryGetValueIfFull(tileSpriteData, out var tile);

            if (tile == null && generateIfNeeded)
            {
                var tileColors = TileColorGenerator.Generate(tileSpriteData);

                tile = ScriptableObject.CreateInstance<Tile>();
                tile.sprite = tileAtlas.Add(tileSpriteData, tileColors);

                tileCache.TryAdd(tileSpriteData, tile);
            }

            return tile;
        }

        internal void ClearData()
        {
            tileCache.Clear();
            tileAtlas.Clear();
        }

        internal void Apply()
        {
            tileAtlas.Apply();
        }
    }
}