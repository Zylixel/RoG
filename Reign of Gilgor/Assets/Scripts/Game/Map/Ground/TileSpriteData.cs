﻿using System;
using System.Diagnostics;
using RoGCore.Assets;

#nullable enable

namespace Client.Game.Map.Ground
{
    internal readonly struct TileSpriteData
    {
        internal readonly GroundDescription? TopDesc;
        internal readonly GroundDescription? BottomDesc;
        internal readonly GroundDescription? RightDesc;
        internal readonly GroundDescription? LeftDesc;
        internal readonly GroundDescription Desc;
        internal readonly int RotateAmount;
        internal readonly int TileTextureIndex;
        internal readonly int Variation;

        private const int MaxVariations = 2;

        internal TileSpriteData(PositionalObject<GroundDescription> ground, Map map)
        {
            Debug.Assert(ground.Object is not null);
            var desc = ground.Object!;
            this.Desc = desc;

            var tileHash = ground.Position.GetHashCode();
            TileTextureIndex = tileHash % desc.Textures.Length;
            RotateAmount = desc.RandomOrientation ? tileHash % 3 : 0;

            TopDesc = null;
            BottomDesc = null;
            RightDesc = null;
            LeftDesc = null;

            if (desc.Layer == 0)
            {
                Variation = 0;
                return;
            }

            TopDesc = SideDesc(map.GroundMap[ground.Position + new RoGCore.Models.Vector2Int(0, 1)]?.Description);
            BottomDesc = SideDesc(map.GroundMap[ground.Position + new RoGCore.Models.Vector2Int(0, -1)]?.Description);
            RightDesc = SideDesc(map.GroundMap[ground.Position + new RoGCore.Models.Vector2Int(1, 0)]?.Description);
            LeftDesc = SideDesc(map.GroundMap[ground.Position + new RoGCore.Models.Vector2Int(-1, 0)]?.Description);

            Variation = tileHash % MaxVariations;

            GroundDescription? SideDesc(GroundDescription? sideDesc) => sideDesc is not null && (desc == sideDesc || OverlappedBy(sideDesc)) ? null : sideDesc;

            bool OverlappedBy(GroundDescription otherDesc) => otherDesc.Layer < desc.Layer || (otherDesc.Layer == desc.Layer && otherDesc.GetHashCode() > desc.GetHashCode());
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(TopDesc, BottomDesc, RightDesc, LeftDesc, Desc, RotateAmount, TileTextureIndex, Variation);
        }
    }
}
