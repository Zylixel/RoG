﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Client.Entity;
using Client.Entity.Helpers;
using Client.Entity.Player;
using Client.Entity.Projectile;
using Client.Game.Event;
using Client.Game.Map.Ground;
using Client.Networking;
using Client.Structures;
using Entity.Helpers;
using Microsoft.Extensions.Logging;
using RoGCore.Assets;
using RoGCore.Logging;
using RoGCore.Models;
using RoGCore.Networking.Packets;
using RoGCore.Utils;
using RoGCore.Vector;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Tilemaps;
using Vector2 = System.Numerics.Vector2;
using Vector2Int = RoGCore.Models.Vector2Int;

#nullable enable

namespace Client.Game.Map
{
    internal sealed class Map : IDisposable
    {
        private const float InteractDist = 4f;
        private const float InteractDistSqr = InteractDist * InteractDist;

        internal static Angle Rotation = new Angle(0);

        internal SortingOrder SortingOrder;

        private static readonly ILogger<Map> Logger = LogFactory.LoggingInstance<Map>();
        private static readonly int WindRotateHash = Shader.PropertyToID("_ObjectRotation");

        internal int CurrentPlayers;

        internal List<RaycastResult>? CurrentRaycast;

        /// <summary>
        ///     Dictionary of entites, where their id is the key.
        ///     Walls are not included in this dictionary.
        /// </summary>
        internal ConcurrentDictionary<long, SimpleEntity> Entities;

        internal HashSet<SimpleEntity> Interactables;

        /// <summary>
        ///     Dictionary of Projectiles, where their id is the key.
        /// </summary>
        internal ConcurrentDictionary<Tuple<long, ushort>, Projectile> Projectiles;

        internal bool UpdateWalls;

        internal delegate void NewEntityDelegate(SimpleEntity entity);

        internal event NewEntityDelegate? EntityAdded;

        internal Map(Tilemap tileMap)
        {
            TileMap = tileMap;
            PlayerId = -1;
            Entities = new ConcurrentDictionary<long, SimpleEntity>();
            WallMap = new ChunkMap<Wall>(8, 8);
            GroundMap = new ChunkMap<ClientGroundData>(8, 8);
            StaticEntityMap = new ChunkMap<SimpleEntity>(8, 8);
            Projectiles = new ConcurrentDictionary<Tuple<long, ushort>, Projectile>();
            Interactables = new HashSet<SimpleEntity>();

            SortingOrder = new SortingOrder();

            ReconnectEvent.Begin += () => Dispose();
            OptionsHandler.SubscribeToOptionsChange<ShaderQuality>("graphicsQuality", UpdateWorldShaders, true);
            OptionsHandler.SubscribeToOptionsChange<float>("viewDistance", ViewDistanceChanged, true);
            MapInfoHandler.MapInfoRecieved.Begin += (mapInfo) => Info = mapInfo;
        }

        internal long PlayerId { get; private set; }

        internal Player? Player { get; private set; }

        internal Tilemap TileMap { get; }

        internal ChunkMap<ClientGroundData> GroundMap { get; private set; }

        internal ChunkMap<SimpleEntity> StaticEntityMap { get; private set; }

        internal ChunkMap<Wall> WallMap { get; private set; }

        internal MapInfo Info { get; private set; }

        public void Dispose()
        {
            Player.ResetRotation();

            // Remove all Entities, don't vanish
            foreach (var entity in Entities)
            {
                EntityFactory.DestroyEntity(entity.Value);
            }

            Entities = new ConcurrentDictionary<long, SimpleEntity>();

            // Remove all StaticEntities
            foreach (var entity in StaticEntityMap)
            {
                EntityFactory.DestroyEntity(entity);
            }

            StaticEntityMap.Clear();

            // Remove all Walls
            foreach (var wall in WallMap)
            {
                WallPool.DestroyWall(wall);
            }

            WallMap.Clear();

            // Remove all projectiles
            foreach (var projectile in Projectiles.Values)
            {
                EntityFactory.DestroyEntity(projectile);
            }

            Projectiles = new ConcurrentDictionary<Tuple<long, ushort>, Projectile>();
            Interactables = new HashSet<SimpleEntity>();

            // Clear TileMap
            TileMap.ClearAllTiles();

            foreach (var ground in GroundMap)
            {
                ground.Dispose();
            }

            GroundMap.Clear();
            Player = null;
            PlayerId = -1;
            GC.Collect();
        }

        internal void Tick()
        {
            var pointerEventData = new PointerEventData(EventSystem.current)
            {
                position = UnityEngine.Input.mousePosition,
            };
            CurrentRaycast = new List<RaycastResult>();
            EventSystem.current.RaycastAll(pointerEventData, CurrentRaycast);

            if (Player == null)
            {
                return;
            }

            SortingOrder.Update(Player.Transform.localPosition, Rotation);

            if (UpdateWalls)
            {
                foreach (var wall in WallMap)
                {
                    wall.Rebuild();
                }

                UpdateWalls = false;
            }

            foreach (var entity in Entities.Values.Concat(StaticEntityMap))
            {
                entity.RecalculateDistanceToPlayer(Player);
            }

            ProjectileCalculator.CalculatePositions(this, Projectiles);
        }

        internal void UpdateWorldShaders(object sender, ShaderQuality quality)
        {
            foreach (var entity in StaticEntityMap)
            {
                entity.ApplyMaterial();
            }

            foreach (var entity in Entities.Values)
            {
                entity.ApplyMaterial();
            }
        }

        internal void ViewDistanceChanged(object sender, float e)
        {
            foreach (var entity in StaticEntityMap)
            {
                entity.ViewDistanceChanged(e);
            }

            foreach (var entity in Entities.Values)
            {
                entity.ViewDistanceChanged(e);
            }
        }

        internal void RecalculateWalls() => ThreadPool.QueueUserWorkItem(_ => RecalculateWallSides());

        private const float ViewDistanceThreshold = 0.1f;

        internal bool InViewDistance(Vector3 worldPosition)
        {
            var viewPortPosition = UnityEngine.Camera.main.WorldToViewportPoint(worldPosition, UnityEngine.Camera.MonoOrStereoscopicEye.Mono);

            return (viewPortPosition.x <= 1 + ViewDistanceThreshold)
                && (viewPortPosition.x >= 0 - ViewDistanceThreshold)
                && (viewPortPosition.y <= 1 + ViewDistanceThreshold)
                && (viewPortPosition.y >= 0 - ViewDistanceThreshold);
        }

        /// <summary>
        ///     Checks all walls against each other to see if a side should be disabled or not.
        /// </summary>
        private void RecalculateWallSides()
        {
            foreach (var wall in WallMap)
            {
                wall.EnableFaces();
                var wallPosition = wall.Position.ToInt();
                if (wall is HalfWall)
                {
                    if (WallPresent(new Vector2Int(wallPosition.X - 1, wallPosition.Y)))
                    {
                        wall.DisableFace(Wall.Faces.West);
                    }

                    if (WallPresent(new Vector2Int(wallPosition.X + 1, wallPosition.Y)))
                    {
                        wall.DisableFace(Wall.Faces.East);
                    }

                    if (WallPresent(new Vector2Int(wallPosition.X, wallPosition.Y - 1)))
                    {
                        wall.DisableFace(Wall.Faces.South);
                    }

                    if (WallPresent(new Vector2Int(wallPosition.X, wallPosition.Y + 1)))
                    {
                        wall.DisableFace(Wall.Faces.North);
                    }
                }
                else
                {
                    var currentCheck = new Vector2Int(wallPosition.X - 1, wallPosition.Y);
                    if (FullWallPresent(currentCheck))
                    {
                        wall.DisableFace(Wall.Faces.West);
                    }
                    else
                    {
                        wall.DarkenFace(Wall.Faces.West, GroundMap[currentCheck] == null);
                    }

                    currentCheck = new Vector2Int(wallPosition.X + 1, wallPosition.Y);
                    if (FullWallPresent(currentCheck))
                    {
                        wall.DisableFace(Wall.Faces.East);
                    }
                    else
                    {
                        wall.DarkenFace(Wall.Faces.East, GroundMap[currentCheck] == null);
                    }

                    currentCheck = new Vector2Int(wallPosition.X, wallPosition.Y - 1);
                    if (FullWallPresent(currentCheck))
                    {
                        wall.DisableFace(Wall.Faces.South);
                    }
                    else
                    {
                        wall.DarkenFace(Wall.Faces.South, GroundMap[currentCheck] == null);
                    }

                    currentCheck = new Vector2Int(wallPosition.X, wallPosition.Y + 1);
                    if (FullWallPresent(currentCheck))
                    {
                        wall.DisableFace(Wall.Faces.North);
                    }
                    else
                    {
                        wall.DarkenFace(Wall.Faces.North, GroundMap[currentCheck] == null);
                    }
                }
            }

            UpdateWalls = true;
        }

        /// <summary>
        ///     Handles world after a camera rotation.
        /// </summary>
        /// <param name="z">New rotation value</param>
        internal void OnCameraRotate(float z)
        {
            Rotation.Rad = z * Mathf.Deg2Rad;
            Wall.PreRotation();
            HalfWall.PreRotation();
            UpdateWalls = true;

            EmbeddedData.WindMaterial.SetFloat(WindRotateHash, Rotation.Rad);
            foreach (var entity in Entities.Values)
            {
                entity.UpdateRotation();
            }

            foreach (var entity in StaticEntityMap)
            {
                entity.UpdateRotation();
            }
        }

        /// <summary>
        ///     Returns all interactables near the player
        /// </summary>
        internal IEnumerable<SimpleEntity> GetInteractablesSorted()
        {
            return Interactables
                .Where(entity => entity.DistToPlayerSqr <= InteractDistSqr)
                .OrderBy(x => x.DistToPlayerSqr);
        }

        /// <summary>
        ///     Returns all interactables near the player
        /// </summary>
        internal IEnumerable<SimpleEntity> GetInteractablesUnsorted() => Interactables.Where(entity => entity.DistToPlayerSqr <= InteractDistSqr);

        /// <summary>
        ///     Returns all entities within a given distance of another
        /// </summary>
        /// <param name="source">Source entity to check from</param>
        /// <param name="maxDistance">Maximum distance from source for entity to be returned</param>
        /// <param name="includeStatic">Whether to include static entities or not</param>
        internal IEnumerable<SimpleEntity> GetNearbyEntities(SimpleEntity source, float maxDistance,
            bool includeStatic = true)
        {
            var maxDistSqr = maxDistance * maxDistance;
            if (includeStatic)
            {
                foreach (var entity in StaticEntityMap.Where(entity =>
                    entity.DistSqr(source.Position) <= maxDistSqr))
                {
                    yield return entity;
                }
            }

            foreach (var entity in Entities.Values.Where(entity =>
                entity.DistSqr(source.Position) <= maxDistSqr))
            {
                yield return entity;
            }
        }

        /// <summary>
        ///     Returns all entities within a given distance of the player
        /// </summary>
        /// <param name="maxDistance">Maximum distance from source for entity to be returned</param>
        /// <param name="includeStatic">Whether to include static entities or not</param>
        internal IEnumerable<SimpleEntity> GetNearbyEntities(float maxDistance, bool includeStatic = true)
        {
            var maxDistSqr = maxDistance * maxDistance;
            if (includeStatic)
            {
                foreach (var entity in StaticEntityMap.Where(entity => entity.DistToPlayerSqr <= maxDistSqr))
                {
                    yield return entity;
                }
            }

            foreach (var entity in Entities.Values.Where(entity => entity.DistToPlayerSqr <= maxDistSqr))
            {
                yield return entity;
            }
        }

        /// <summary>
        ///     Returns all entities within a given distance of another in order of smallest distance to largest
        /// </summary>
        /// <param name="source">Source entity to check from</param>
        /// <param name="maxDistance">Maximum distance from source for entity to be returned</param>
        /// <param name="include">Predicate to test an entity against</param>
        /// <param name="includeStatic">Whether to include static entities or not</param>
        internal IEnumerable<SimpleEntity> GetNearbyEntitiesOrdered(SimpleEntity source, float maxDistance,
            Predicate<SimpleEntity> include, bool includeStatic = true)
        {
            var maxDistSqr = maxDistance * maxDistance;
            var unsorted = new Dictionary<SimpleEntity, double>();
            if (includeStatic)
            {
                foreach (var entity in StaticEntityMap.Where(entity => include(entity)))
                {
                    var distSqr = entity.DistSqr(source.Position.X, source.Position.Y);
                    if (distSqr > maxDistSqr)
                    {
                        continue;
                    }

                    unsorted.Add(entity, distSqr);
                }
            }

            foreach (var entity in Entities.Values.Where(entity => include(entity)))
            {
                var distSqr = entity.DistSqr(source.Position.X, source.Position.Y);
                if (distSqr > maxDistSqr)
                {
                    continue;
                }

                unsorted.Add(entity, distSqr);
            }

            return unsorted.OrderBy(x => x.Value).Select(x => x.Key);
        }

        /// <summary>
        ///     Returns all entities within a given distance of the player in order of smallest distance to largest
        /// </summary>
        /// <param name="maxDistance">Maximum distance from source for entity to be returned</param>
        /// <param name="include">Predicate to test an entity against</param>
        /// <param name="includeStatic">Whether to include static entities or not</param>
        internal IEnumerable<SimpleEntity> GetNearbyEntitiesOrdered(float maxDistance, Predicate<SimpleEntity> include,
            bool includeStatic = true)
        {
            var maxDistSqr = maxDistance * maxDistance;
            var unsorted = new Dictionary<SimpleEntity, double>();
            if (includeStatic)
            {
                foreach (var entity in StaticEntityMap.Where(entity => include(entity)))
                {
                    if (entity.DistToPlayerSqr > maxDistSqr)
                    {
                        continue;
                    }

                    unsorted.Add(entity, entity.DistToPlayerSqr);
                }
            }

            foreach (var entity in Entities.Values.Where(entity => include(entity)))
            {
                if (entity.DistToPlayerSqr > maxDistSqr)
                {
                    continue;
                }

                unsorted.Add(entity, entity.DistToPlayerSqr);
            }

            return unsorted.OrderBy(x => x.Value).Select(x => x.Key);
        }

        /// <summary>
        ///     Returns all entities within a given distance of another in order of smallest distance to largest
        /// </summary>
        /// <param name="source">Source entity to check from</param>
        /// <param name="maxDistance">Maximum distance from source for entity to be returned</param>
        /// <param name="includeStatic">Whether to include static entities or not</param>
        internal IEnumerable<SimpleEntity> GetNearbyEntitiesOrdered(SimpleEntity source, float maxDistance,
            bool includeStatic = true)
        {
            var maxDistSqr = maxDistance * maxDistance;
            var unsorted = new Dictionary<SimpleEntity, double>();
            if (includeStatic)
            {
                foreach (var entity in StaticEntityMap)
                {
                    var distSqr = entity.DistSqr(source.Position.X, source.Position.Y);
                    if (distSqr > maxDistSqr)
                    {
                        continue;
                    }

                    unsorted.Add(entity, distSqr);
                }
            }

            foreach (var entity in Entities.Values)
            {
                var distSqr = entity.DistSqr(source.Position.X, source.Position.Y);
                if (distSqr > maxDistSqr)
                {
                    continue;
                }

                unsorted.Add(entity, distSqr);
            }

            return unsorted.OrderBy(x => x.Value).Select(x => x.Key);
        }

        /// <summary>
        ///     Returns all entities within a given distance of the player in order of smallest distance to largest
        /// </summary>
        /// <param name="maxDistance">Maximum distance from source for entity to be returned</param>
        /// <param name="includeStatic">Whether to include static entities or not</param>
        internal IEnumerable<SimpleEntity> GetNearbyEntitiesOrdered(float maxDistance, bool includeStatic = true)
        {
            var maxDistSqr = maxDistance * maxDistance;
            var unsorted = new Dictionary<SimpleEntity, double>();
            if (includeStatic)
            {
                foreach (var entity in StaticEntityMap.Where(entity => !(entity.DistToPlayerSqr > maxDistSqr)))
                {
                    unsorted.Add(entity, entity.DistToPlayerSqr);
                }
            }

            foreach (var entity in Entities.Values.Where(entity => !(entity.DistToPlayerSqr > maxDistSqr)))
            {
                unsorted.Add(entity, entity.DistToPlayerSqr);
            }

            return unsorted.OrderBy(x => x.Value).Select(x => x.Key);
        }

        /// <summary>
        ///     Returns all entities in order of smallest distance to largest
        /// </summary>
        internal IEnumerable<SimpleEntity> GetAllEntitiesOrdered(Predicate<SimpleEntity> include, bool includeStatic = true)
        {
            var unsorted = new Dictionary<SimpleEntity, double>();
            if (includeStatic)
            {
                foreach (var entity in StaticEntityMap.Where(entity => include(entity)))
                {
                    unsorted.Add(entity, entity.DistToPlayerSqr);
                }
            }

            foreach (var entity in Entities.Values.Where(entity => include(entity)))
            {
                unsorted.Add(entity, entity.DistToPlayerSqr);
            }

            return unsorted.OrderBy(x => x.Value).Select(x => x.Key);
        }

        internal bool RegionUnblocked(float x, float y, TileOccupency occupency)
        {
            Vector2Int intVector = new((int)x, (int)y);

            IntVector2S collisionArea = new(CardinalOrdinalDirections.NineByNineArea.Count);

            UnityBurstSIMD.Math.Addition.Add(CardinalOrdinalDirections.NineByNineArea.X, intVector.X, ref collisionArea.X);
            UnityBurstSIMD.Math.Addition.Add(CardinalOrdinalDirections.NineByNineArea.Y, intVector.Y, ref collisionArea.Y);

            var occupiedTiles = TilesOccupied(collisionArea, occupency);

            if (occupiedTiles[(int)CardinalOrdinalDirections.Direction.Center])
            {
                return false;
            }

            var xFrac = x - intVector.X;
            var yFrac = y - intVector.Y;
            if (xFrac < 0.5)
            {
                if (occupiedTiles[(int)CardinalOrdinalDirections.Direction.Left])
                {
                    return false;
                }

                if (yFrac < 0.5)
                {
                    if (occupiedTiles[(int)CardinalOrdinalDirections.Direction.Bottom] ||
                        occupiedTiles[(int)CardinalOrdinalDirections.Direction.BottomLeft])
                    {
                        return false;
                    }
                }
                else if (yFrac > 0.5 && (occupiedTiles[(int)CardinalOrdinalDirections.Direction.Top] ||
                     occupiedTiles[(int)CardinalOrdinalDirections.Direction.TopLeft]))
                {
                    return false;
                }

                return true;
            }

            if (xFrac > 0.5)
            {
                if (occupiedTiles[(int)CardinalOrdinalDirections.Direction.Right])
                {
                    return false;
                }

                if (yFrac < 0.5)
                {
                    if (occupiedTiles[(int)CardinalOrdinalDirections.Direction.Bottom] ||
                        occupiedTiles[(int)CardinalOrdinalDirections.Direction.BottomRight])
                    {
                        return false;
                    }
                }
                else if (yFrac > 0.5 && (occupiedTiles[(int)CardinalOrdinalDirections.Direction.Top] ||
                     occupiedTiles[(int)CardinalOrdinalDirections.Direction.TopRight]))
                {
                    return false;
                }

                return true;
            }

            return yFrac < 0.5
                ? !occupiedTiles[(int)CardinalOrdinalDirections.Direction.Bottom]
                : yFrac <= 0.5 || !occupiedTiles[(int)CardinalOrdinalDirections.Direction.Top];
        }

        private bool[] TilesOccupied(
            IntVector2S positions,
            TileOccupency occupency)
        {
            var ret = new bool[positions.Count];

            for (var i = 0; i < positions.Count; i++)
            {
                ret[i] = TileOccupied(positions[i].X, positions[i].Y, occupency);
            }

            return ret;
        }

        internal bool TileOccupied(float x, float y, TileOccupency occupency)
        {
            var pos = new Vector2Int((int)x, (int)y);

            var ground = GroundMap[pos]?.Description;
            var staticEntity = StaticEntityMap[pos];
            ObjectDescription? objectDescription = null;

            if (staticEntity != null)
            {
                objectDescription = staticEntity.ObjectDesc;
            }
            else
            {
                var wall = WallMap[pos];

                if (wall != null)
                {
                    objectDescription = wall.ObjectDesc;
                }
            }

            return Occupied(ground, objectDescription, occupency);
        }

        private bool Occupied(GroundDescription? ground, ObjectDescription? @object, TileOccupency occupency) => (occupency & TileOccupency.OccupyFull) != 0 && @object?.OccupyFull == true
|| ((occupency & TileOccupency.OccupyHalf) != 0 && (@object?.OccupyFull == true || @object?.OccupyHalf == true))
|| ((occupency & TileOccupency.NoWalk) != 0 && (ground?.NoWalk == true)) || (ground is null && @object is null);

        /// <summary>
        ///     Checks to see if a full wall is in a certain location on the map.
        /// </summary>
        /// <param name="pos">Position to check</param>
        /// <returns>whether a full wall is present</returns>
        internal bool FullWallPresent(Vector2Int pos)
        {
            var wall = WallMap[pos];
            return wall is not null and not HalfWall;
        }

        /// <summary>
        ///     Checks to see if a wall is in a certain location on the map.
        /// </summary>
        /// <param name="pos">Position to check</param>
        /// <returns>whether a wall is present</returns>
        internal bool WallPresent(Vector2Int pos) => WallMap[pos] is not null;

        /// <summary>
        ///     Adds a wall into a specific location on the map.
        ///     If a wall is already in the location, it is disposed and replaced.
        /// </summary>
        /// <param name="pos">Position in which wall will be placed</param>
        /// <param name="wall">Initialized wall object</param>
        internal void AddWall(Vector2Int pos, Wall wall)
        {
            RemoveWall(pos);
            WallMap[pos] = wall;
        }

        internal void RemoveWall(Vector2Int pos)
        {
            var oldWall = WallMap[pos];
            if (oldWall != null)
            {
                WallPool.DestroyWall(oldWall);
            }

            WallMap[pos] = null;
        }

        /// <summary>
        ///     Sets the ID of the player to know which object is the player during the update packet
        /// </summary>
        /// <param name="id">objectId of incoming player</param>
        internal void SetPlayerId(long id)
        {
            Logger.LogInformation("Incoming Player ID: " + id);

            if (PlayerId == -1)
            {
                PlayerId = id;
            }
            else
            {
                Logger.LogError("The incoming playerID has attempted to be switched! Ignoring request");
            }
        }

        /// <summary>
        ///     Adds the player to the map.
        ///     Only allows one player to be created.
        /// </summary>
        /// <param name="p">Initialized Player object</param>
        internal void AddPlayer(Player p)
        {
            if (Player == null && PlayerId != -1)
            {
                Logger.LogInformation($"Adding player {p} to map");
                Player = p;
                Entities.TryAdd(Player.Id, Player);
            }
            else
            {
                Logger.LogError(
                    "A player was added to the map, which already contains a player! Ignoring request and disposing player");

                EntityFactory.DestroyEntity(p);
            }
        }

        /// <summary>
        ///     Adds a GameEntity to the map.
        ///     If a GameEntity has the same ID, it is disposed and replaced.
        /// </summary>
        /// <param name="entity">Initialized GameEntity object</param>
        internal void AddEntity(SimpleEntity entity)
        {
            if (Entities.ContainsKey(entity.Id))
            {
                RemoveEntity(entity.Id);
            }

            Entities.TryAdd(entity.Id, entity);
            if (entity.ObjectDesc.Interactable)
            {
                Interactables.Add(entity);
            }

            EntityAdded?.Invoke(entity);
        }

        /// <summary>
        ///     Removes an Entity from the map. Disposed Properly.
        /// </summary>
        /// <param name="id">Unique ID of entity to remove.</param>
        internal void RemoveEntity(long id)
        {
            if (!Entities.TryRemove(id, out var entity))
            {
                return;
            }

            if (entity is Character character && character.CanVanish())
            {
                entity.StopAllCoroutines();
                character.Vanish();
            }
            else
            {
                EntityFactory.DestroyEntity(entity);
            }

            if (entity.ObjectDesc.Interactable)
            {
                Interactables.Remove(entity);
            }
        }

        internal void AddStatic(Vector2 pos, SimpleEntity entity)
        {
            var intPosition = pos.ToInt();

            if (StaticEntityMap[intPosition] is not null)
            {
                RemoveStatic(intPosition);
            }

            StaticEntityMap[intPosition] = entity;
            if (entity.ObjectDesc.Interactable)
            {
                Interactables.Add(entity);
            }
        }

        /// <summary>
        ///     Removes a entity from the world.
        ///     If it can vanish, it will vanish. Else it will be instantly disposed
        /// </summary>
        /// <param name="pos">Position to remove entity</param>
        internal void RemoveStatic(Vector2Int pos)
        {
            var entity = StaticEntityMap[pos];
            if (entity != null)
            {
                StaticEntityMap[pos] = null;
                if (entity is Character character && character.CanVanish())
                {
                    entity.StopAllCoroutines();
                    character.Vanish();
                }
                else
                {
                    EntityFactory.DestroyEntity(entity);
                }

                if (entity.ObjectDesc.Interactable)
                {
                    Interactables.Remove(entity);
                }

                return;
            }

            RemoveWall(pos);
        }

        /// <summary>
        ///     Adds a Projectile to the map.
        ///     If a Projectile has the same ID, it is disposed and replaced.
        /// </summary>
        /// <param name="id">Projectile's unique ID provided by server</param>
        /// <param name="proj">Initialized Projectile object</param>
        internal void AddProjectile(Tuple<long, ushort> id, Projectile proj)
        {
            if (!Projectiles.TryGetValue(id, out var oldProjectile))
            {
                Projectiles.TryAdd(id, proj);
            }
            else
            {
                EntityFactory.DestroyEntity(oldProjectile);
                Projectiles[id] = proj;
            }
        }

        /// <summary>
        ///     Removes a Projectile from the map. Disposed Properly.
        /// </summary>
        /// <param name="id">Unique ID of Projectile to remove.</param>
        internal void RemoveProjectile(Tuple<long, ushort> id)
        {
            EntityFactory.DestroyEntity(Projectiles[id]);
            Projectiles.TryRemove(id, out _);
        }
    }
}