using System;
using Client.Game;
using Client.Structures;
using UnityEngine;

public class MaterialQualityHandler : MonoBehaviour
{
    [SerializeField]
    private Material[] materials;

    private void Start() => OptionsHandler.SubscribeToOptionsChange<ShaderQuality>("graphicsQuality", QualityChanged, true);

    private void QualityChanged(object sender, ShaderQuality quality)
    {
        foreach (var qual in Enum.GetNames(typeof(ShaderQuality)))
        {
            if (qual == quality.ToString())
            {
                for (var i = 0; i < materials.Length; i++)
                {
                    materials[i].EnableKeyword("GAMEQUALITY_" + qual.ToUpperInvariant());
                }
            }
            else
            {
                for (var i = 0; i < materials.Length; i++)
                {
                    materials[i].DisableKeyword("GAMEQUALITY_" + qual.ToUpperInvariant());
                }
            }
        }
    }

    private void OnDestroy() => OptionsHandler.UnsubscribeToOptionsChange<ShaderQuality>("graphicsQuality", QualityChanged);
}
