﻿using System;
using System.Collections.Generic;
using Client.Structures;
using Client.UI.Options;
using UnityEngine;
using static Client.UI.Options.OptionsScreen;

namespace Client.Game
{
    internal static class OptionsHandler
    {
        internal static readonly List<BaseOption> Options = new List<BaseOption>()
        {
            new BoolOption("drawHPBars") { Tab = Tab.General, ClientName = "HP Bars", DefaultValue = true },
            new BoolOption("drawStatText")
            {
                Tab = Tab.General,
                ClientName = "Health Meter Text",
                DefaultValue = true,
            },
            new BoolOption("cameraOffset") { Tab = Tab.General, ClientName = "Offset Camera" },
            new BoolOption("autoShoot") { Tab = Tab.General, ClientName = "Auto Shoot" },
            new KeyCodeOption("shootKey") { Tab = Tab.Controls, ClientName = "Shoot Weapon", DefaultValue = KeyCode.Mouse0 },
            new KeyCodeOption("escapeKey") { Tab = Tab.Controls, ClientName = "Nexus", DefaultValue = KeyCode.R },
            new KeyCodeOption("interactKey")
            { Tab = Tab.Controls, ClientName = "Interact", DefaultValue = KeyCode.LeftShift },
            new KeyCodeOption("rotLeftKey") { Tab = Tab.Controls, ClientName = "Rotate Left", DefaultValue = KeyCode.Q },
            new KeyCodeOption("rotRightKey") { Tab = Tab.Controls, ClientName = "Rotate Right", DefaultValue = KeyCode.E },
            new KeyCodeOption("invToggleKey") { Tab = Tab.Controls, ClientName = "Inventory", DefaultValue = KeyCode.T },
            new KeyCodeOption("chatKey") { Tab = Tab.Controls, ClientName = "Open Chat", DefaultValue = KeyCode.Return },
            new KeyCodeOption("sneakKey") { Tab = Tab.Controls, ClientName = "Toggle Sneak", DefaultValue = KeyCode.LeftControl },
            new KeyCodeOption("HPBarToggleKey")
            { Tab = Tab.Controls, ClientName = "Toggle HP Bars", DefaultValue = KeyCode.H },
            new KeyCodeOption("weaponOpenKey")
            { Tab = Tab.Controls, ClientName = "Open Weapon Selector", DefaultValue = KeyCode.Alpha1 },
            new KeyCodeOption("garmentOpenKey")
            { Tab = Tab.Controls, ClientName = "Open Garment Selector", DefaultValue = KeyCode.Alpha2 },
            new KeyCodeOption("ringOpenKey")
            { Tab = Tab.Controls, ClientName = "Open Ring Selector", DefaultValue = KeyCode.Alpha3 },
            new KeyCodeOption("artifactOpenKey")
            { Tab = Tab.Controls, ClientName = "Open Artifact Selector", DefaultValue = KeyCode.Alpha4 },
            new KeyCodeOption("summonKey")
            { Tab = Tab.Controls, ClientName = "Set Summon Spot", DefaultValue = KeyCode.Mouse1 },
            new EnumOption<ShaderQuality>("graphicsQuality")
            {
                Tab = Tab.Video,
                ClientName = "Graphics Quality",
                DefaultValue = ShaderQuality.High,
            },
            new BoolOption("vSync")
            {
                Tab = Tab.Video,
                ClientName = "VSync",
                DefaultValue = true,
            },
            new BoolOption("pixelPerfect")
            {
                Tab = Tab.Video,
                ClientName = "Pixel Perfect View Distance",
                DefaultValue = true,
            },
            new SliderOption("viewDistance")
            {
                Tab = Tab.Video,
                ClientName = "Viewing Distance",
                MinValue = 6,
                MaxValue = 12,
                DefaultValue = 9f,
            },
            new SliderOption("mouseFollowStrength")
            {
                Tab = Tab.Video,
                ClientName = "Mouse Follow Strength",
                MinValue = 0f,
                MaxValue = 0.025f,
                DefaultValue = 0f,
            },
            new BoolOption("particleMaster") { Tab = Tab.Video, ClientName = "Particle Master", DefaultValue = true },
            new BoolOption("shadows") { Tab = Tab.Video, ClientName = "Shadows", DefaultValue = true },
            new BoolOption("screenShake")
            { Tab = Tab.Video, ClientName = "Screen Shake", DefaultValue = true },
            new SliderOption("masterVolume")
            { Tab = Tab.Audio, ClientName = "Master Volume", DefaultValue = 1f, MaxValue = 1 },
            new SliderOption("musicVolume")
            { Tab = Tab.Audio, ClientName = "Music Volume", DefaultValue = 1f, MaxValue = 1 },
            new SliderOption("sfxVolume")
            { Tab = Tab.Audio, ClientName = "SFX Volume", DefaultValue = 1f, MaxValue = 1 },
            new BoolOption("dynamicSound")
            { Tab = Tab.Audio, ClientName = "Dynamic Sound", DefaultValue = true },
        };

        private static readonly Dictionary<string, BaseOption> OptionDictionary;

        static OptionsHandler()
        {
            OptionDictionary = new Dictionary<string, BaseOption>();
            foreach (var option in Options)
            {
                OptionDictionary.Add(option.InternalName, option);
            }

            InputMaster.SubscribeToKeyPress(GetKeyCodeOption("HPBarToggleKey"), (sender, e) =>
            {
                if (InputMaster.InGameplay())
                {
                    ToggleOption("drawHPBars");
                }
            });
            InputMaster.SubscribeToKeyPress(KeyCode.I, (sender, e) =>
            {
                if (InputMaster.InGameplay())
                {
                    ToggleOption("autoShoot");
                }
            });
            InputMaster.SubscribeToKeyPress(KeyCode.X, (sender, e) =>
            {
                if (InputMaster.InGameplay())
                {
                    ToggleOption("cameraOffset");
                }
            });
        }

        internal static bool ToggleOption(string internalOptionName)
        {
            var option = OptionDictionary[internalOptionName];

            if (option is GenericOption<bool> boolOption)
            {
                boolOption.Value = !boolOption.Value;
                return boolOption.Value;
            }

            throw new Exception($"Cannot toggle option [{internalOptionName}] because it is not a [{typeof(BoolOption)}]");
        }

        internal static KeyCodeOption GetKeyCodeOption(string internalName)
        {
            var option = OptionDictionary[internalName];

            return option is not KeyCodeOption keyCodeOption
                ? throw new Exception($"Cannot get option value [{internalName}] because it is not a [{typeof(KeyCodeOption)}]")
                : keyCodeOption;
        }

        internal static T GetOptionValue<T>(string internalName)
        {
            var option = OptionDictionary[internalName];

            return option is not GenericOption<T> genericOption
                ? throw new Exception($"Cannot get option value [{internalName}] because it is not a [{typeof(GenericOption<T>)}]")
                : genericOption.Value;
        }

        internal static void SubscribeToOptionsChange<T>(string internalName, EventHandler<T> action, bool invokeAction = false)
        {
            var option = OptionDictionary[internalName];

            if (option is not GenericOption<T> genericOption)
            {
                throw new Exception($"Cannot get option value [{internalName}] because it is not a [{typeof(GenericOption<T>)}]");
            }

            genericOption.OptionChangedEvent += action;

            if (invokeAction)
            {
                action.Invoke(null, genericOption.Value);
            }
        }

        internal static void UnsubscribeToOptionsChange<T>(string internalName, EventHandler<T> action)
        {
            var option = OptionDictionary[internalName];

            if (option is not GenericOption<T> genericOption)
            {
                throw new Exception($"Cannot get option value [{internalName}] because it is not a [{typeof(GenericOption<T>)}]");
            }

            genericOption.OptionChangedEvent -= action;
        }
    }
}