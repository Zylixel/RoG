﻿#nullable enable
using RoGCore.Models;

namespace Client.Game
{
    public readonly struct PositionalObject<T>
    {
        public readonly Vector2Int Position;
        public readonly T? Object;

        public PositionalObject(Vector2Int position, T? obj)
        {
            Position = position;
            Object = obj;
        }
    }
}
