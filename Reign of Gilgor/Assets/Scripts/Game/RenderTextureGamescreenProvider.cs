using Client.UI;
using UnityEngine;

namespace Client.Game
{
    internal sealed class RenderTextureGamescreenProvider : MonoBehaviour
    {
        [SerializeField]
        private string textureName;
        [SerializeField]
        internal float ScaleMultiplier = 1;
        [SerializeField]
        private RenderTextureFormat format;

        private RenderTexture texture;

        internal RenderTexture Texture
        {
            get
            {
                if (texture == null)
                {
                    GenerateTexture();
                }

                return texture;
            }
        }

        internal event Event.Delegates.EmptyGameEvent TextureUpdated;

        private void Start() => WindowManager.ScreenSizeChangeEvent += ResetTexture;

        private void GenerateTexture()
        {
            texture = new RenderTexture((int)(Screen.width * ScaleMultiplier), (int)(Screen.height * ScaleMultiplier), 0, format)
            {
                name = textureName,
            };

            TextureUpdated?.Invoke();
        }

        internal void OnValidate()
        {
            ScaleMultiplier = Mathf.Max(0.01f, ScaleMultiplier);

            if (texture != null)
            {
                ResetTexture();
            }
        }

        private void ResetTexture()
        {
            if (texture == null)
            {
                return;
            }

            Destroy(texture);
            GenerateTexture();
        }

        private void OnDestroy()
        {
            Destroy(texture);
            WindowManager.ScreenSizeChangeEvent -= ResetTexture;
        }
    }
}