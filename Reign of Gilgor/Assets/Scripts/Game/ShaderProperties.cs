﻿using UnityEngine;

namespace Client.Game
{
    internal static class ShaderProperties
    {
        internal static readonly int Tint = Shader.PropertyToID("_TintColor");
        internal static readonly int Alpha = Shader.PropertyToID("_Alpha");
        internal static readonly int FlashColor = Shader.PropertyToID("_FlashColor");
        internal static readonly int OutlineColor = Shader.PropertyToID("OutlineColor");
        internal static readonly int FlashRate = Shader.PropertyToID("_FlashRate");
        internal static readonly int BigColor = Shader.PropertyToID("_BigColor");
        internal static readonly int UseBigColor = Shader.PropertyToID("_UseBigColor");
        internal static readonly int SmallColor = Shader.PropertyToID("_SmallColor");
        internal static readonly int UseSmallColor = Shader.PropertyToID("_UseSmallColor");
        internal static readonly int HighlightColor = Shader.PropertyToID("Highlight_Color");
        internal static readonly int Mask = Shader.PropertyToID("_Mask");
        internal static readonly int BlurTexture = Shader.PropertyToID("_BlurTexture");
        internal static readonly int MaskNoUnderline = Shader.PropertyToID("Mask");
        internal static readonly int SampleSize = Shader.PropertyToID("_SampleSize");
        internal static readonly int Spread = Shader.PropertyToID("_Spread");
        internal static readonly int ObjectScale = Shader.PropertyToID("ObjectScale");
    }
}
