﻿using UnityEngine;

namespace Client.Game
{
    internal sealed class TextureScale
    {
        // Internal unility that renders the source texture into the RTT - the scaling method itself.
        internal static void GPUScale(ref RenderTexture src, ref RenderTexture output, FilterMode fmode)
        {
            // We need the source texture in VRAM because we render with it
            src.filterMode = fmode;

            var lastTarget = RenderTexture.active;

            // Set the RTT in order to render to it
            RenderTexture.active = output;

            // Setup 2D matrix in range 0..1, so nobody needs to care about sized
            GL.LoadPixelMatrix(0, 1, 1, 0);

            // Then clear & draw the texture to fill the entire RTT.
            GL.Clear(true, true, Color.clear);
            Graphics.DrawTexture(new Rect(0, 0, 1, 1), src);

            RenderTexture.active = lastTarget;
        }
    }
}