﻿using System.Collections;
using System.Net;
using System.Net.Sockets;
using Client.Entity.Player;
using Client.Game.Event;
using Client.Networking.API;
using Client.UI;
using Client.UI.Dialog;
using Client.UI.TitleScreen;
using Client.Utils;
using RoGCore.Networking;
using RoGCore.Networking.Packets;
using RoGCore.SettingsModels;
using UnityEngine;
using UnityEngine.Tilemaps;
using static RoGCore.Utils.ResultExtenstions;

namespace Client
{
    public class GameWorldManager : Initializable<GameWorldManager>
    {
        // TODO: Networking code should not be here
        internal static OutgoingNetworkHandler CurrentOutgoingNetwork;
        internal static IncomingNetworkHandler CurrentIncomingNetwork;
        internal static GameEvent<Player> PlayerCreated = new GameEvent<Player>();

        [SerializeField]
        internal GuildLabel GuildDisplay;
        internal EmbeddedData GameData;
        internal System.Random GlobalRandom = new System.Random();
        internal Game.Map.Map Map;
        internal SilverDisplay SilverDisplay;

        [SerializeField]
        private GameObject sidebarHolder;
        [SerializeField]
        private Tilemap tilemap;
        private UdpClient udpClient;
        private bool silverDisplayEnabled;
        private bool bossStatusBarEnabled;

        internal static void Disconnect()
        {
            Instance.Map.Dispose();
        }

        internal void EnterSafeZone()
        {
            silverDisplayEnabled = true;
            GuildDisplay.Visible = true;
        }

        internal void EnterRealm()
        {
            silverDisplayEnabled = true;
            GuildDisplay.Visible = false;
        }

        internal void EnterDungeon()
        {
            silverDisplayEnabled = false;
            GuildDisplay.Visible = false;
        }

        private void Start()
        {
            BossStatusBar.BossStatusBarChanged += BossStatusBarChanged;

            GameData = TitleScreenController.Instance.GameData;
            Map = new Game.Map.Map(tilemap);
            SilverDisplay = new SilverDisplay(sidebarHolder.transform, new Vector2(-210, 158f));
            SilverDisplay.SetScale(1f, 1f);
        }

        private void OnEnable()
        {
            LoadingHandler.Load("Nexus", 0, this);

            StartCoroutine(ConnectToServer());
        }

        private IEnumerator ConnectToServer()
        {
            var client = new Networking.Client(this);
            udpClient = new UdpClient();

            var endpointResult = new CoroutineResultWrapper<IPEndPoint>();
            yield return GameServerStore.GetPrimaryEndpoint(endpointResult);

            if (endpointResult.IsFailed)
            {
                ErrorDialog.Display(endpointResult.Result.ErrorString(), InterSceneData.GoToTitleScreen);
                yield break;
            }

            CurrentOutgoingNetwork = new OutgoingNetworkHandler(udpClient, endpointResult.Value);
            CurrentIncomingNetwork = new IncomingNetworkHandler(client);

            var udpListener = new UDPListener(udpClient);
            udpListener.BeginListening();
            udpListener.PacketReceived += (sender, e) => CurrentIncomingNetwork.IncomingMessage(e.Item2);

            CurrentOutgoingNetwork.OutgoingMessage(new Hello(
                NetworkingSettings.FULLBUILD,
                -1,
                AccountStore.Username,
                AccountStore.Password));
            ReconnectEvent.Invoke();
        }

        private void Update()
        {
            if (CurrentIncomingNetwork == null)
            {
                return;
            }

            SilverDisplay.Enabled = silverDisplayEnabled && !bossStatusBarEnabled;

            IncomingNetworkHandler.ProcessMessages();

            Map.Tick();
        }

        private void OnApplicationQuit() => Disconnect();

        private void BossStatusBarChanged(bool status) => bossStatusBarEnabled = status;
    }
}