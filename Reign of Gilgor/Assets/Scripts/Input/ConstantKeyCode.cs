﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Client.Input
{
    internal readonly struct ConstantKeyCode : IKeyCodeProvider, IEquatable<ConstantKeyCode>, IEqualityComparer<ConstantKeyCode>
    {
        private readonly KeyCode keyCode;

        public ConstantKeyCode(KeyCode keyCode)
        {
            this.keyCode = keyCode;
        }

        KeyCode IKeyCodeProvider.GetKeyCode() => keyCode;

        public bool Equals(ConstantKeyCode other) => keyCode == other.keyCode;

        public bool Equals(ConstantKeyCode x, ConstantKeyCode y) => x.keyCode == y.keyCode;

        public int GetHashCode(ConstantKeyCode obj) => (int)keyCode;
    }
}
