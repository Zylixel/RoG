﻿using UnityEngine;

namespace Client.Input
{
    internal interface IKeyCodeProvider
    {
        internal KeyCode GetKeyCode();
    }
}
