﻿using System;
using System.Collections.Generic;
using Client.Input;
using UnityEngine;

namespace Client
{
    internal sealed class InputMaster : MonoBehaviour
    {
        private static readonly Dictionary<IKeyCodeProvider, KeyDetector> KeyDetectors = new Dictionary<IKeyCodeProvider, KeyDetector>();

        private static bool frameLocked = true;
        private static Type focus = typeof(LoadingHandler);

        private static event EventHandler ControlClick;

        internal static Type Focus
        {
            get => focus;
            set
            {
                focus = value;
                frameLocked = true;
            }
        }

        internal static void SubscribeToKeyPress(KeyCode code, EventHandler action) => SubscribeToKeyPress(new ConstantKeyCode(code), action);

        internal static void SubscribeToKeyPress(IKeyCodeProvider keyCodeProvider, EventHandler action)
        {
            if (KeyDetectors.TryGetValue(keyCodeProvider, out var keyDetector))
            {
                keyDetector.AddSubscriber(action);
            }
            else
            {
                keyDetector = new KeyDetector(keyCodeProvider);
                keyDetector.AddSubscriber(action);
                KeyDetectors[keyCodeProvider] = keyDetector;
            }
        }

        internal static void UnsubscribeToKeyPress(IKeyCodeProvider keyCodeProvider, EventHandler action)
        {
            if (KeyDetectors.TryGetValue(keyCodeProvider, out var keyDetector))
            {
                keyDetector.RemoveSubscriber(action);
            }
        }

        internal static void UnsubscribeToKeyPress(KeyCode keyCode, EventHandler action)
        {
            if (KeyDetectors.TryGetValue(new ConstantKeyCode(keyCode), out var keyDetector))
            {
                keyDetector.RemoveSubscriber(action);
            }
        }

        internal static void SubscribeToControlClick(EventHandler action) => ControlClick += action;

        internal static void UnsubscribeToControlClick(EventHandler action) => ControlClick -= action;

        internal static bool InGameplay() => Focus == typeof(Client.Game.Map.Map) && !frameLocked;

        private void Update()
        {
            frameLocked = false;

            // Make a copy of keyDetectors as it will be modified
            foreach (var item in KeyDetectors.Values)
            {
                item.CheckForPress();
            }

            if (UnityEngine.Input.GetMouseButtonDown(0) && UnityEngine.Input.GetKey(KeyCode.LeftControl))
            {
                ControlClick.Invoke(this, null);
            }
        }
    }
}
