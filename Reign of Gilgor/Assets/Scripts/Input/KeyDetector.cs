﻿using System;

namespace Client.Input
{
    internal sealed class KeyDetector
    {
        private readonly IKeyCodeProvider keyCodeProvider;

        internal KeyDetector(IKeyCodeProvider keyCodeProvider)
        {
            this.keyCodeProvider = keyCodeProvider;
        }

        private event EventHandler KeyPressedEvent;

        internal void AddSubscriber(EventHandler action) => KeyPressedEvent += action;

        internal void RemoveSubscriber(EventHandler action) => KeyPressedEvent -= action;

        internal void CheckForPress()
        {
            if (UnityEngine.Input.GetKeyDown(keyCodeProvider.GetKeyCode()))
            {
                Invoke();
            }
        }

        private void Invoke() => KeyPressedEvent?.Invoke(this, null);
    }
}
