﻿using System.Linq;
using Client;
using Client.Audio;
using Client.Networking.API;
using Client.UI.TitleScreen;
using UnityEngine;
using UnityEngine.Tilemaps;

internal static class InterSceneData
{
    internal static GameObject TitleScene =
        Resources.FindObjectsOfTypeAll<GameObject>().First(x => x.name.Equals("TitleScreen"));

    internal static GameObject GameWorldScene =
        Resources.FindObjectsOfTypeAll<GameObject>().First(x => x.name.Equals("GameWorld"));

    internal static bool EnterSelectScreen;
    internal static ushort CharacterId;
    internal static bool NewCharacter;

    internal static void GoToTitleScreen()
    {
        GameWorldManager.Disconnect();
        EnterSelectScreen = true;
        MusicHandler.SetMusic("YouAndMe", 2f);
        AccountStore.AccountData.Invalidate();
        GameServerStore.GameServerData.Invalidate();
        GameWorldScene.SetActive(false);
        TitleScene.SetActive(true);

        TitleScene.GetComponentInChildren<StarRandomizer>().gameObject.GetComponent<Tilemap>().color = Color.white;
    }
}