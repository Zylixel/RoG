using UnityEngine;
using static RoGCore.Utils.StringUtils;

namespace Client.Logging
{
    internal static class LoggingInitializer
    {
        internal static void Initialize()
        {
            TextAsset appSettingsFile = (TextAsset)Resources.Load("appsettings");
            using var jsonStream = appSettingsFile.text.GenerateStream();
            RoGCore.Logging.InitializeLogger.Initialize(jsonStream);

            RoGCore.Logging.LogFactory.Reload();
        }
    }
}