#nullable enable

using System.Collections;
using Client.Utils;
using FluentResults;
using Newtonsoft.Json;
using UnityEngine.Networking;

namespace Client.Networking.API
{
    internal static class APIRequest
    {
        private const string APIDNSSource = "https://raw.githubusercontent.com/MasonRotMG/ReignOfGilgor/master/AppengineIP";

        private static string? APIDNS = null;

        internal static IEnumerator Get<T>(string path, CoroutineResultWrapper<T> result)
        {
            if (APIDNS is null)
            {
                yield return LoadAPIDNS();
            }

            var gameDataRequest = UnityWebRequest.Get($"{APIDNS}/{path}");

            yield return gameDataRequest.SendWebRequest();
            if (gameDataRequest.result == UnityWebRequest.Result.ConnectionError)
            {
                result.Result = Result.Fail("Connection Error");
                yield break;
            }

            if (gameDataRequest.result != UnityWebRequest.Result.Success)
            {
                result.Result = Result.Fail(gameDataRequest.downloadHandler.text);
                yield break;
            }

            var stringResponse = gameDataRequest.downloadHandler.text;
            var jsonResponse = JsonConvert.DeserializeObject<T>(stringResponse);

            if (jsonResponse is null)
            {
                result.Result = Result.Fail("Could not deserialize JSON");
                yield break;
            }

            result.Result = jsonResponse;
        }

        private static IEnumerator LoadAPIDNS()
        {
            var request = UnityWebRequest.Get(APIDNSSource);
            yield return request.SendWebRequest();
            APIDNS = request.result != UnityWebRequest.Result.Success
                ? "https://localhost:3001"
                : request.downloadHandler.text.TrimEnd();
        }
    }
}