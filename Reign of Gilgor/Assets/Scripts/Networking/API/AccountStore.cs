using System.Collections;
using Client.Utils;
using FluentResults;
using RoGCore.Networking.APIViewModels;
using RoGCore.Utils;
using UnityEngine;

namespace Client.Networking.API
{
    internal static class AccountStore
    {
        internal static CachedResponse<AccountViewModel> AccountData = new CachedResponse<AccountViewModel>(LoadData);

        internal static string Username => username ?? PlayerPrefs.GetString(UsernameKey);

        internal static string Password => password ?? PlayerPrefs.GetString(UsernameKey);

        private const string UsernameKey = "validUsername";
        private const string PasswordKey = "validPassword";
        private const string Route = "account";

        private static string username;
        private static string password;

        internal static bool LoadSavedCredentials()
        {
            username = PlayerPrefs.GetString(UsernameKey, null);
            password = PlayerPrefs.GetString(PasswordKey, null);

            return !Username.IsNullOrWhiteSpace() && !Password.IsNullOrWhiteSpace();
        }

        internal static void Logout()
        {
            username = null;
            password = null;
            PlayerPrefs.SetString(UsernameKey, string.Empty);
            PlayerPrefs.SetString(PasswordKey, string.Empty);
            AccountData.SetValue(Result.Fail("Logged out"));
        }

        internal static IEnumerator Login(string username, string password, bool save, CoroutineResultWrapper<AccountViewModel> apiResponse)
        {
            AccountStore.username = username;
            AccountStore.password = password;

            if (save)
            {
                PlayerPrefs.SetString(UsernameKey, username);
                PlayerPrefs.SetString(PasswordKey, password);
            }

            AccountData.Invalidate();
            yield return AccountData.GetValue(apiResponse);
        }

        internal static IEnumerator Register(string username, string password, bool save, CoroutineResultWrapper<AccountViewModel> apiResponse)
        {
            yield return APIRequest.Get($"{Route}/register?username={username}&password={password}", apiResponse);
            AccountData.SetValue(apiResponse);

            if (apiResponse.Result.IsFailed)
            {
                Logout();
                yield break;
            }

            AccountStore.username = username;
            AccountStore.password = password;

            if (save)
            {
                PlayerPrefs.SetString(UsernameKey, username);
                PlayerPrefs.SetString(PasswordKey, password);
            }
        }

        internal static IEnumerator IsNewAccount(CoroutineOutputWrapper<bool> result)
        {
            var apiResponse = new CoroutineResultWrapper<AccountViewModel>();
            yield return AccountData.GetValue(apiResponse);

            if (apiResponse.Result.IsFailed)
            {
                result.Value = true;
                yield break;
            }

            var account = apiResponse.Value;
            result.Value = account.Characters.Count == 0;
        }

        private static IEnumerator LoadData(CoroutineResultWrapper<AccountViewModel> apiResponse)
        {
            yield return APIRequest.Get($"{Route}/{Username}?password={Password}", apiResponse);

            if (apiResponse.Result.IsFailed)
            {
                Logout();
                yield break;
            }
        }
    }
}