using System.Collections;
using System.Collections.Generic;
using Client.Utils;
using FluentResults;
using RoGCore.Networking.APIViewModels;

namespace Client.Networking.API
{
    internal static class CharacterStore
    {
        internal static CachedResponse<IList<CharacterViewModel>> CharacterData = new CachedResponse<IList<CharacterViewModel>>(LoadData);

        private static IEnumerator LoadData(CoroutineResultWrapper<IList<CharacterViewModel>> characterResult)
        {
            var accountResult = new CoroutineResultWrapper<AccountViewModel>();
            yield return AccountStore.AccountData.GetValue(accountResult);

            if (accountResult.IsFailed)
            {
                characterResult.Result = Result.Fail(characterResult.Result.Errors);
            }

            characterResult.Result = Result.Ok(accountResult.Value.Characters);
        }
    }
}