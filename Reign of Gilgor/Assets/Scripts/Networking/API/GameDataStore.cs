﻿using System.Collections;
using Client.Game.Event;
using Client.Utils;
using RoGCore.Networking.APIViewModels;

namespace Client.Networking.API
{
    internal static class GameDataStore
    {
        internal static GameEvent<GameDataViewModel> GameDataUpdated = new GameEvent<GameDataViewModel>();

        private const string Route = "game-data";

        internal static IEnumerator LoadGameData(CoroutineResultWrapper<GameDataViewModel> apiResponse)
        {
            yield return APIRequest.Get(Route, apiResponse);

            if (apiResponse.Result.IsFailed)
            {
                yield break;
            }

            GameDataUpdated.Invoke(apiResponse.Value);
        }
    }
}