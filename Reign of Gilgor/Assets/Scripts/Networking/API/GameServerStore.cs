﻿#nullable enable

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Client.Utils;
using FluentResults;
using RoGCore.Models;

namespace Client.Networking.API
{
    internal static class GameServerStore
    {
        internal static CachedResponse<List<GameServer>> GameServerData = new CachedResponse<List<GameServer>>(LoadData);

        private const string Route = "game-server";

        internal static IEnumerator GetPrimaryEndpoint(CoroutineResultWrapper<IPEndPoint> result)
        {
            var gameServers = new CoroutineResultWrapper<List<GameServer>>();
            yield return GameServerData.GetValue(gameServers);

            if (gameServers.IsFailed)
            {
                result.Result = Result.Fail(gameServers.Result.Errors);
                yield break;
            }

            var primaryServer = gameServers.Value.FirstOrDefault();
            if (primaryServer is null)
            {
                result.Result = Result.Fail("No GameServer online");
                yield break;
            }

            var addresses = Dns.GetHostAddresses(primaryServer.Dns);

            var ipAddress = addresses.FirstOrDefault(ip => ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork);
            if (ipAddress == null)
            {
                result.Result = Result.Fail("Cannot resolve GameServer IP");
                yield break;
            }

            result.Result = new IPEndPoint(ipAddress, primaryServer.Port);
        }

        private static IEnumerator LoadData(CoroutineResultWrapper<List<GameServer>> apiResponse)
        {
            yield return APIRequest.Get(Route, apiResponse);

            if (apiResponse.Result.IsFailed)
            {
                yield break;
            }
        }
    }
}