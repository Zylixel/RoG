#nullable enable

using System;
using System.Collections;
using Client.Utils;
using FluentResults;

namespace Client.Networking
{
    internal sealed class CachedResponse<T>
    {
        private readonly Func<CoroutineResultWrapper<T>, IEnumerator> valueGetter;
        private bool valid = false;
        private CoroutineResultWrapper<T> currentValue = new CoroutineResultWrapper<T>();

        public CachedResponse(Func<CoroutineResultWrapper<T>, IEnumerator> valueGetter)
        {
            this.valueGetter = valueGetter;
        }

        internal delegate void CachedResponseChangedHandler(Result<T> value);

        internal event CachedResponseChangedHandler? ResponseChanged;

        internal IEnumerator GetValue(CoroutineResultWrapper<T> value)
        {
            if (!valid)
            {
                yield return valueGetter.Invoke(value);
                currentValue = value;
                valid = true;
                ResponseChanged?.Invoke(currentValue.Result);
            }

            value.Result = currentValue.Result;
        }

        internal void SetValue(Result<T> newValue)
        {
            currentValue.Result = newValue;
            valid = true;
            ResponseChanged?.Invoke(newValue);
        }

        internal void Invalidate()
        {
            valid = false;
        }
    }
}