﻿using System;
using Microsoft.Extensions.Logging;
using RoGCore.Logging;
using RoGCore.Models;
using RoGCore.Networking;

#nullable enable

namespace Client.Networking
{
    internal class Client : IClient
    {
        internal readonly GameWorldManager Manager;

        private static readonly ILogger<Client> Logger = LogFactory.LoggingInstance<Client>();

        public Client(GameWorldManager manager)
        {
            Manager = manager;
        }

        public void Disconnect(DisconnectReason reason) => IncomingNetworkHandler.PacketWorkQueue.Enqueue(InterSceneData.GoToTitleScreen);

        public void ProcessPacket(in IPacket packet)
        {
            try
            {
                var packetHandler = PacketHandlers.Handlers[(int)packet.Id];
                if (packetHandler is not null)
                {
                    packetHandler.Handle(this, packet);
                }
                else
                {
                    Logger.LogWarning($"Unhandled packet '{packet.Id}'.");
                }
            }
            catch (Exception e)
            {
                Logger.LogWarning($"Error when handling packet... {e}");
                Disconnect(DisconnectReason.ErrorWhenHandlingPacket);
            }
        }
    }
}
