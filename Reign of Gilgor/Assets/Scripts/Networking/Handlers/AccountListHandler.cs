﻿using System.Collections.Generic;
using RoGCore.Models;
using RoGCore.Networking.Packets;

namespace Client.Networking
{
    internal sealed class AccountListHandler : PacketHandler<AccountList>
    {
        private static HashSet<int> ignored;
        private static HashSet<int> locked;

        public override MessageId Id => MessageId.AccountList;

        public override bool RunOutsideOfLogicThread => true;

        internal static bool AccountLocked(int accountId) => locked is not null && locked.Contains(accountId);

        internal static bool AccountBlocked(int accountId) => ignored is not null && ignored.Contains(accountId);

        protected override void HandlePacket(in AccountList packet, GameWorldManager manager)
        {
            ignored = new HashSet<int>(packet.IgnoredIds);
            locked = new HashSet<int>(packet.LockedIds);
        }
    }
}