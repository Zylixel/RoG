﻿using Client.Entity;
using RoGCore.Models;
using RoGCore.Networking.Packets;
using Structures;

namespace Client.Networking
{
    internal class AllyShootHandler : PacketHandler<AllyShoot>
    {
        public override MessageId Id => MessageId.AllyShoot;

        public override bool RunOutsideOfLogicThread => false;

        protected override void HandlePacket(in AllyShoot packet, GameWorldManager manager)
        {
            if (!manager.Map.Entities.TryGetValue(packet.OwnerId, out var owner))
            {
                return;
            }

            if (owner is not IContainer container)
            {
                return;
            }

            var item = container.Inventory[packet.InventorySlot];

            if (item is null)
            {
                return;
            }

            if (owner is Character character)
            {
                character.Shoot(
                    packet.BulletId,
                    item.Value.BaseItem.Projectiles[0],
                    packet.Angle,
                    packet.Time,
                    false,
                    item,
                    character.Position);
            }
        }
    }
}