﻿using System;
using Client.Audio;
using RoGCore.Models;

namespace Client.Networking
{
    internal class AudioHandler : PacketHandler<RoGCore.Networking.Packets.Audio>
    {
        public override MessageId Id => MessageId.Audio;

        public override bool RunOutsideOfLogicThread => false;

        protected override void HandlePacket(in RoGCore.Networking.Packets.Audio packet, GameWorldManager manager)
        {
            if (packet.AudioType == RoGCore.Networking.Packets.Audio.Type.Music)
            {
                MusicHandler.SetMusic(packet.Name, 2f);
                return;
            }

            throw new NotImplementedException();
        }
    }
}