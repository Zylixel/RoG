﻿using System;
using Client.Entity;
using Microsoft.Extensions.Logging;
using RoGCore.Logging;
using RoGCore.Models;
using RoGCore.Networking.Packets;

namespace Client.Networking
{
    internal class CharacterHitHandler : PacketHandler<CharacterHit>
    {
        private static readonly ILogger<CharacterHitHandler> Logger = LogFactory.LoggingInstance<CharacterHitHandler>();

        public override MessageId Id => MessageId.CharacterHit;

        public override bool RunOutsideOfLogicThread => false;

        protected override void HandlePacket(in CharacterHit packet, GameWorldManager manager)
        {
            if (!manager.Map.Entities.TryGetValue(packet.CharacterId, out var hitEntity))
            {
                Logger.LogError("Cannot find hitEntity: " + packet.CharacterId);
                return;
            }

            if (hitEntity is not Character hitPlayer)
            {
                Logger.LogError("hitEntity: " + packet.CharacterId + " is not a Character");
                return;
            }

            if (manager.Map.Projectiles.TryGetValue(
                Tuple.Create(packet.ObjectId, packet.BulletId),
                out var projectile))
            {
                projectile.HitCharacter(hitPlayer);
            }
        }
    }
}