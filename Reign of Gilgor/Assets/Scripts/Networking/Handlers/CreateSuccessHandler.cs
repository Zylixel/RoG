﻿using RoGCore.Models;
using RoGCore.Networking.Packets;

namespace Client.Networking
{
    internal class CreateSuccessHandler : PacketHandler<CreateSuccess>
    {
        public override MessageId Id => MessageId.CreateSuccess;

        public override bool RunOutsideOfLogicThread => true;

        protected override void HandlePacket(in CreateSuccess packet, GameWorldManager manager)
        {
            InterSceneData.CharacterId = packet.CharacterId;
            manager.Map.SetPlayerId(packet.ObjectId);
        }
    }
}