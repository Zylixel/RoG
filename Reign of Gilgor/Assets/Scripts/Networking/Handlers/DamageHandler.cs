﻿using Client.Entity;
using RoGCore.Models;
using RoGCore.Networking.Packets;

namespace Client.Networking
{
    internal class DamageHandler : PacketHandler<Damage>
    {
        public override MessageId Id => MessageId.Damage;

        public override bool RunOutsideOfLogicThread => false;

        protected override void HandlePacket(in Damage packet, GameWorldManager manager)
        {
            if (manager.Map.Entities.TryGetValue(packet.TargetId, out var entity) && entity is Character character)
            {
                character.OnDamage(packet);
            }
        }
    }
}