﻿using Client.Entity;
using RoGCore.Models;
using RoGCore.Networking.Packets;
using RoGCore.Utils;

namespace Client.Networking
{
    internal class EnemyShootHandler : PacketHandler<EnemyShoot>
    {
        public override MessageId Id => MessageId.EnemyShoot;

        public override bool RunOutsideOfLogicThread => false;

        protected override void HandlePacket(in EnemyShoot packet, GameWorldManager manager)
        {
            if (!manager.Map.Entities.TryGetValue(packet.OwnerId, out var owner) ||
                (owner is not Character character))
            {
                return;
            }

            var customDesc = owner.ObjectDesc.Projectiles[packet.BulletIndex];

            // TODO: This is super weird...
            customDesc.BaseDamage = packet.Damage;
            for (var i = 0; i < packet.NumShots; i++)
            {
                var angle = new Angle(packet.Angle.Rad + (packet.AngleInc * i));
                if (packet.Offset)
                {
                    character.Shoot(
                        (ushort)((packet.BulletId + i) % owner.ObjectDesc.MaxProjectiles),
                        customDesc,
                        angle,
                        packet.Time,
                        true,
                        null,
                        character.Transform.localPosition.Convert());
                }
                else
                {
                    character.Shoot((ushort)((packet.BulletId + i) % owner.ObjectDesc.MaxProjectiles), customDesc, angle, packet.Time, true);
                }
            }
        }
    }
}