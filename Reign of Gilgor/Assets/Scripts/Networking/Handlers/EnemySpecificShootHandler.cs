﻿using Client.Entity;
using RoGCore.Models;
using RoGCore.Networking.Packets;

namespace Client.Networking
{
    internal class EnemySpecificShootHandler : PacketHandler<EnemySpecificShoot>
    {
        public override MessageId Id => MessageId.EnemySpecificShoot;

        public override bool RunOutsideOfLogicThread => false;

        protected override void HandlePacket(in EnemySpecificShoot packet, GameWorldManager manager)
        {
            if (!manager.Map.Entities.TryGetValue(packet.OwnerId, out var owner) ||
                owner.ObjectDesc.Projectiles.Length < packet.BulletIndex + 1 ||
                owner is not Character character)
            {
                return;
            }

            var customDesc = owner.ObjectDesc.Projectiles[packet.BulletIndex];

            // TODO: Why is it being done like this? This is really weird...
            customDesc.BaseDamage = packet.Damage;

            character.Shoot(packet.BulletId, customDesc, packet.Angle, packet.Time, true, null, packet.StartPosition);
        }
    }
}