﻿using Client.UI.Dialog;
using Microsoft.Extensions.Logging;
using RoGCore.Logging;
using RoGCore.Models;
using RoGCore.Networking.Packets;

namespace Client.Networking
{
    internal sealed class FailureHandler : PacketHandler<Failure>
    {
        public override bool RunOutsideOfLogicThread => false;

        public override MessageId Id => MessageId.Failure;

        private static readonly ILogger<FailureHandler> Logger = LogFactory.LoggingInstance<FailureHandler>();

        protected override void HandlePacket(in Failure packet, GameWorldManager manager)
        {
            ErrorDialog.Display(packet.Text, packet.Title);
            Logger.LogWarning($"{packet.Title} - {packet.Text}");
        }
    }
}