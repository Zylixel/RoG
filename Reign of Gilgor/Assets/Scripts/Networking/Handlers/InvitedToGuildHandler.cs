﻿using RoGCore.Models;
using RoGCore.Networking.Packets;

namespace Client.Networking
{
    internal sealed class InvitedToGuildHandler : PacketHandler<GuildInvite>
    {
        private static GuildInvite? request;

        public override MessageId Id => MessageId.GuildInvite;

        public override bool RunOutsideOfLogicThread => true;

        protected override void HandlePacket(in GuildInvite packet, GameWorldManager manager) => request = packet;

        internal static GuildInvite? Peek() => request;

        internal static void Accept()
        {
            if (request is null)
            {
                return;
            }

            GameWorldManager.CurrentOutgoingNetwork.OutgoingMessage(new GuildJoin(request.Value.GuildName));
            request = null;
        }

        internal static void Decline() => request = null;
    }
}