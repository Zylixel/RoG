﻿using Client.Game.Event;
using RoGCore.Models;
using RoGCore.Networking.Packets;

namespace Client.Networking
{
    internal class MapInfoHandler : PacketHandler<MapInfo>
    {
        public override MessageId Id => MessageId.MapInfo;

        public override bool RunOutsideOfLogicThread => false;

        public static GameEvent<MapInfo> MapInfoRecieved = new GameEvent<MapInfo>();

        protected override void HandlePacket(in MapInfo packet, GameWorldManager manager)
        {
            if (InterSceneData.NewCharacter)
            {
                GameWorldManager.CurrentOutgoingNetwork.OutgoingMessage(new Create(InterSceneData.CharacterId));
                InterSceneData.NewCharacter = false;
            }
            else
            {
                GameWorldManager.CurrentOutgoingNetwork.OutgoingMessage(new Load(InterSceneData.CharacterId));
            }

            MapInfoRecieved.Invoke(packet);
        }
    }
}