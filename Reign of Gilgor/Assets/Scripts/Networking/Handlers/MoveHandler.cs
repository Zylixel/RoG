﻿using RoGCore.Models;
using RoGCore.Networking.Packets;

namespace Client.Networking
{
    internal class MoveHandler : PacketHandler<Move>
    {
        public override MessageId Id => MessageId.Move;

        public override bool RunOutsideOfLogicThread => false;

        protected override void HandlePacket(in Move packet, GameWorldManager manager)
        {
            manager.Map.Player.OnMove(packet);
        }
    }
}