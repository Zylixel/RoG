﻿using System.Diagnostics;
using Client.Game;
using Client.UI.Debugging;
using RoGCore.Models;
using RoGCore.Networking.Packets;

namespace Client.Networking
{
    internal class NewTickHandler : PacketHandler<NewTick>
    {
        internal static float MPST;
        internal static float TPS = 1;

        public override MessageId Id => MessageId.NewTick;

        public override bool RunOutsideOfLogicThread => false;

        protected override void HandlePacket(in NewTick packet, GameWorldManager manager)
        {
            var sw = Stopwatch.StartNew();
            MPST = packet.Mspt;
            TPS = packet.Tps;

            manager.Map.CurrentPlayers = packet.PlayersInWorld;
            GlobalLightHandler.Instance.SetGlobalLight(manager.Map.Info.Lighting ? packet.LightLevel : 1);
            foreach (var stat in packet.UpdateStatuses.Data)
            {
                if (manager.Map.Player == null)
                {
                    return;
                }

                if (manager.Map.Entities.TryGetValue(stat.Id, out var entity))
                {
                    entity.OnUpdate(stat);
                }
            }

            sw.StopAndSet(ref TimingMonitor.NewTickHandler);
        }
    }
}