﻿using Client.Entity;
using Client.Entity.Helpers;
using Client.Entity.Player;
using RoGCore.Models;
using RoGCore.Networking.Packets;

namespace Client.Networking
{
    internal class NotificationHandler : PacketHandler<Notification>
    {
        public override MessageId Id => MessageId.Notification;

        public override bool RunOutsideOfLogicThread => false;

        protected override void HandlePacket(in Notification packet, GameWorldManager manager)
        {
            if (manager.Map.Entities.TryGetValue(packet.ObjectId, out var entity) && entity is Character @char)
            {
                if (packet.Text.Contains("xp") && @char is Player player)
                {
                    player.OnXPNotification(packet);
                }
                else
                {
                    EntityFactory.CreateNotification<Entity.Notifications.Notification>(@char, packet.Text, packet.Color);
                }
            }
        }
    }
}