﻿using RoGCore.Models;
using RoGCore.Networking.Packets;

#nullable enable

namespace Client.Networking
{
    internal class ObjectDamageHandler : PacketHandler<ObjectDamage>
    {
        public override MessageId Id => MessageId.ObjectDamage;

        public override bool RunOutsideOfLogicThread => false;

        protected override void HandlePacket(in ObjectDamage packet, GameWorldManager manager)
        {
            var wall = manager.Map.WallMap[packet.TilePosition];
            if (wall != null)
            {
                wall.Damage(packet.Damage);
            }
        }
    }
}