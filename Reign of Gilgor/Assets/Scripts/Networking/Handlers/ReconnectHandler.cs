﻿using Client.Game.Event;
using Client.Networking.API;
using RoGCore.Models;
using RoGCore.Networking.Packets;
using RoGCore.SettingsModels;

namespace Client.Networking
{
    internal class ReconnectHandler : PacketHandler<Reconnect>
    {
        public override MessageId Id => MessageId.Reconnect;

        public override bool RunOutsideOfLogicThread => false;

        protected override void HandlePacket(in Reconnect packet, GameWorldManager manager)
        {
            LoadingHandler.Load(packet.Name, packet.Difficulty, manager);

            ReconnectEvent.Invoke();

            GameWorldManager.CurrentOutgoingNetwork.OutgoingMessage(new Hello(
                NetworkingSettings.FULLBUILD,
                packet.GameId,
                AccountStore.Username,
                AccountStore.Password));
        }
    }
}