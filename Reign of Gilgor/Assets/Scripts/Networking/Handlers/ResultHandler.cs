﻿using Client.UI.Dialog;
using RoGCore.Models;
using RoGCore.Networking.Packets;

namespace Client.Networking
{
    internal class ResultHandler : PacketHandler<Result>
    {
        public override MessageId Id => MessageId.Result;

        public override bool RunOutsideOfLogicThread => false;

        protected override void HandlePacket(in Result packet, GameWorldManager manager) => QuickDialog.OnResult(packet);
    }
}