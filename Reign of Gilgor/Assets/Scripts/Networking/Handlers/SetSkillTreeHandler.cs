﻿using Client.UI;
using RoGCore.Models;
using RoGCore.Networking.Packets;

namespace Client.Networking
{
    internal class SetSkillTreeHandler : PacketHandler<SetSkillTree>
    {
        public override MessageId Id => MessageId.SetSkillTree;

        public override bool RunOutsideOfLogicThread => false;

        protected override void HandlePacket(in SetSkillTree packet, GameWorldManager manager) => SkillTreeUI.Set(packet.TreeData);
    }
}