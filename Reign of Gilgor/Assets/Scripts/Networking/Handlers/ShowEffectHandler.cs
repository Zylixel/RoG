﻿using Client.Entity;
using Client.Entity.Helpers;
using Client.Particle;
using Microsoft.Extensions.Logging;
using RoGCore.Logging;
using RoGCore.Models;
using RoGCore.Networking.Packets;
using RoGCore.Utils;
using UnityEngine;

namespace Client.Networking
{
    internal class ShowEffectHandler : PacketHandler<ShowEffect>
    {
        public override MessageId Id => MessageId.ShowEffect;

        public override bool RunOutsideOfLogicThread => false;

        private static readonly ILogger<ShowEffectHandler> Logger = LogFactory.LoggingInstance<ShowEffectHandler>();

        protected override void HandlePacket(in ShowEffect packet, GameWorldManager manager)
        {
            if (!manager.Map.Entities.TryGetValue(packet.TargetId, out var entity))
            {
                return;
            }

            switch (packet.EffectType)
            {
                case ShowEffect.Type.Flash:
                    {
                        if (packet.PosA.Y > 0)
                        {
                            entity.StartFlash(packet.Color, packet.PosA.X, packet.PosA.Y);
                        }

                        break;
                    }

                case ShowEffect.Type.Heal:
                    {
                        ParticleFactory.CreateParticle(
                            "InstantHealParticle",
                            new CreateParticleProperties { Parent = entity.GameObject.transform, AutoReturn = true });
                        break;
                    }

                case ShowEffect.Type.Teleport:
                    {
                        if (entity is Character @char)
                        {
                            @char.StartCoroutine(@char.TeleportAnimation());
                        }

                        ParticleFactory.CreateParticle(
                            "TeleportParticle",
                            new CreateParticleProperties { LocalPosition = packet.PosA.ToUnity3(), AutoReturn = true });
                        ParticleFactory.CreateParticle(
                            "TeleportParticle",
                            new CreateParticleProperties { Parent = entity.GameObject.transform, AutoReturn = true });
                        break;
                    }

                case ShowEffect.Type.Scepter:
                    {
                        var particle = ParticleFactory.CreateParticle(
                            "ScepterParticle",
                            new CreateParticleProperties
                            { Parent = entity.GameObject.transform, AutoReturn = true, Force = true });
                        particle.transform.eulerAngles =
                            new Vector3(0, 0, Game.Map.Map.Rotation.Deg + (packet.PosA.X * Mathf.Rad2Deg));
                        break;
                    }

                case ShowEffect.Type.Toss:
                    {
                        if (!manager.GameData.BetterObjects.TryGetValue(packet.Data, out var objectDescription))
                        {
                            break;
                        }

                        TossedObjectFactory.CreateTossedObject(objectDescription, entity, packet.PosA.ToUnity2(), packet.PosB.X);
                        break;
                    }

                case ShowEffect.Type.Nova:
                    var p = ParticleFactory.CreateParticle(
                        "NovaParticle",
                        new CreateParticleProperties { LocalPosition = entity.Transform.localPosition, AutoReturn = true });
                    var main = p.main;
                    main.startColor = new ParticleSystem.MinMaxGradient(packet.Color);
                    break;
                default:
                    {
                        Logger.LogError($"Cannot show {packet.EffectType} Effect");
                        break;
                    }
            }
        }
    }
}