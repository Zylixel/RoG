﻿using System.Linq;
using System.Text;
using Client.Entity;
using Client.UI.Chat;
using RoGCore.Models;
using RoGCore.Networking.Packets;
using RoGCore.Utils;
using UnityEngine;

namespace Client.Networking
{
    internal class TextHandler : PacketHandler<Text>
    {
        private static readonly RecentChatHandler RecentChatHandler =
            Resources.FindObjectsOfTypeAll<RecentChatHandler>()[0];

        private static readonly UnityEngine.UI.Text GameChatText = Resources.FindObjectsOfTypeAll<GameObject>()
            .First(x => x.name.Equals("GameChat")).GetComponentInChildren<UnityEngine.UI.Text>(true);

        public override MessageId Id => MessageId.Text;

        public override bool RunOutsideOfLogicThread => false;

        protected override void HandlePacket(in Text packet, GameWorldManager manager)
        {
            AddText(packet);
            if (manager.Map.Entities.TryGetValue(packet.ObjectId, out var sender) && sender is Character chr)
            {
                chr.OnText(packet);
            }
        }

        internal static void AddText(in Text packet)
        {
            RecentChatHandler.OnNewChat(packet);
            var toDisplay = new StringBuilder(GameChatText.text);
            if (!packet.Name.IsNullOrWhiteSpace())
            {
                toDisplay.Append("<color=#").AppendFormat("{0:X}", packet.NameColor).Append('>');
                toDisplay.Append(packet.Name);
                toDisplay.Append("</color>");
                toDisplay.Append(": ");
            }

            toDisplay.Append("<color=#").AppendFormat("{0:X}", packet.TextColor).Append('>');
            toDisplay.Append(packet.Message);
            toDisplay.Append("</color>");
            toDisplay.Append("\n");
            GameChatText.text = toDisplay.ToString();
        }
    }
}