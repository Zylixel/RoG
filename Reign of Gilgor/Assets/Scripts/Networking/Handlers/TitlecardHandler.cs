﻿using Client.UI.Titlecard;
using RoGCore.Models;

namespace Client.Networking
{
    internal sealed class TitlecardHandler : PacketHandler<RoGCore.Networking.Packets.TitleCard>
    {
        public override MessageId Id => MessageId.TitleCard;

        public override bool RunOutsideOfLogicThread => false;

        protected override void HandlePacket(in RoGCore.Networking.Packets.TitleCard packet, GameWorldManager manager) => TitlecardEvent.Invoke(packet.Title);
    }
}