﻿using Client.Entity;
using Client.Entity.Helpers;
using Client.Entity.Player;
using Client.Game.Map.Ground;
using Client.UI;
using Client.UI.Debugging;
using Microsoft.Extensions.Logging;
using RoGCore.Logging;
using RoGCore.Models;
using RoGCore.Networking.Packets;
using RoGCore.Utils;
using UnityEngine;

#nullable enable

namespace Client.Networking
{
    internal sealed class UpdateHandler : PacketHandler<Update>
    {
        private static readonly ILogger<UpdateHandler> Logger = LogFactory.LoggingInstance<UpdateHandler>();

        public override MessageId Id => MessageId.Update;

        public override bool RunOutsideOfLogicThread => false;

        protected override void HandlePacket(in Update packet, GameWorldManager manager)
        {
            var sw = System.Diagnostics.Stopwatch.StartNew();

            for (var i = 0; i < packet.RemovedStatics.Length; i++)
            {
                manager.Map.RemoveStatic(packet.RemovedStatics[i]);
            }

            foreach (var ground in manager.Map.GroundMap.GetFilledChunkObjects())
            {
                if (!packet.ViewRectangle.Contains(ground.Position))
                {
                    ground.Object?.Dispose();
                    manager.Map.GroundMap[ground.Position] = null;
                    manager.Map.TileMap.SetTile(new Vector3Int(ground.Position.X, ground.Position.Y, 0), null);
                }
            }

            foreach (var staticEntity in manager.Map.StaticEntityMap.GetFilledChunkObjects())
            {
                if (!packet.ViewRectangle.Contains(staticEntity.Position))
                {
                    manager.Map.RemoveStatic(staticEntity.Position);
                }
            }

            foreach (var wall in manager.Map.WallMap.GetFilledChunkObjects())
            {
                if (!packet.ViewRectangle.Contains(wall.Position))
                {
                    manager.Map.RemoveWall(wall.Position);
                }
            }

            for (var i = 0; i < packet.RemovedObjectIds.Length; i++)
            {
                manager.Map.RemoveEntity(packet.RemovedObjectIds[i]);
            }

            if (packet.Tiles.Length > 0)
            {
                var sw2 = System.Diagnostics.Stopwatch.StartNew();
                NewGroundDataHandler.HandleNewTiles(packet.Tiles, manager);
                sw2.StopAndSet(ref TimingMonitor.NewGroundDataHandler);
            }

            {
                var sw2 = System.Diagnostics.Stopwatch.StartNew();
                HandleNewObjects(packet, manager);
                sw2.StopAndSet(ref TimingMonitor.NewObjects);
            }

            {
                var sw2 = System.Diagnostics.Stopwatch.StartNew();
                HandleNewStatics(packet, manager);
                sw2.StopAndSet(ref TimingMonitor.NewStatics);
            }

            manager.Map.RecalculateWalls();
            {
                var sw2 = System.Diagnostics.Stopwatch.StartNew();
                MinimapHandler.OnUpdate(packet);
                sw2.StopAndSet(ref TimingMonitor.Minimap);
            }

            sw.StopAndSet(ref TimingMonitor.UpdateHandler);
        }

        private static void HandleNewStatics(in Update packet, GameWorldManager manager)
        {
            for (var i = 0; i < packet.NewStatics.Count; i++)
            {
                var gameObject = packet.NewStatics[i];
                var desc = manager.GameData.Objects[gameObject.ObjectType];
                switch (desc.Class)
                {
                    case "Decor":
                        manager.Map.AddStatic(gameObject.Stats.Pos, EntityFactory.CreateEntity<Decor>(gameObject.Stats, desc, manager));
                        break;
                    case "GameObject":
                        manager.Map.AddStatic(gameObject.Stats.Pos, EntityFactory.CreateEntity<Character>(gameObject.Stats, desc, manager));
                        break;

                        // Unsupported
                    case "Container":
                    case "Portal":
                    case "Player":
                    case "Summon":
                        Logger.LogError($"Cannot load {desc.Class} as static object");
                        break;
                    case "Wall":
                        manager.Map.AddWall(gameObject.Stats.Pos.ToInt(), WallPool.CreateWall(gameObject.Stats, desc, manager));
                        break;
                    case "HalfWall":
                        manager.Map.AddWall(gameObject.Stats.Pos.ToInt(), WallPool.CreateHalfWall(gameObject.Stats, desc, manager));
                        break;
                    default:
                        manager.Map.AddStatic(gameObject.Stats.Pos, EntityFactory.CreateEntity<Character>(gameObject.Stats, desc, manager));
                        break;
                }
            }
        }

        private static void HandleNewObjects(in Update packet, GameWorldManager manager)
        {
            for (var i = 0; i < packet.NewObjects.Count; i++)
            {
                var gameObject = packet.NewObjects[i];
                var desc = manager.GameData.Objects[gameObject.ObjectType];
                if (gameObject.Stats.Id == manager.Map.PlayerId)
                {
                    // initialize self
                    manager.Map.AddPlayer(EntityFactory.CreateEntity<Player>(gameObject.Stats, desc, manager));
                    continue;
                }

                switch (desc.Class)
                {
                    case "Portal":
                        manager.Map.AddEntity(EntityFactory.CreateEntity<Portal>(gameObject.Stats, desc, manager));
                        break;
                    case "GameObject":
                        manager.Map.AddEntity(EntityFactory.CreateEntity<Character>(gameObject.Stats, desc, manager));
                        break;
                    case "Player":
                        manager.Map.AddEntity(EntityFactory.CreateEntity<ServerPlayer>(gameObject.Stats, desc, manager));
                        break;

                        // unsupported
                    case "Wall":
                    case "Decor":
                    case "HalfWall":
                        Logger.LogError($"Cannot load {desc.Class} as dynamic object");
                        break;
                    case "Container":
                        manager.Map.AddEntity(EntityFactory.CreateEntity<Container>(gameObject.Stats, desc, manager));
                        break;
                    case "Summon":
                        manager.Map.AddEntity(EntityFactory.CreateEntity<Summon>(gameObject.Stats, desc, manager));
                        break;
                    case "Enemy":
                        manager.Map.AddEntity(EntityFactory.CreateEntity<Enemy>(gameObject.Stats, desc, manager));
                        break;
                    default:
                        manager.Map.AddEntity(EntityFactory.CreateEntity<Character>(gameObject.Stats, desc, manager));
                        break;
                }
            }
        }
    }
}