﻿using Client.UI.Vault;
using RoGCore.Models;
using RoGCore.Networking.Packets;

namespace Client.Networking
{
    internal sealed class VaultResultHandler : PacketHandler<VaultResult>
    {
        public override MessageId Id => MessageId.VaultResult;

        public override bool RunOutsideOfLogicThread => false;

        protected override void HandlePacket(in VaultResult packet, GameWorldManager manager) => VaultDisplay.OnResult(packet);
    }
}