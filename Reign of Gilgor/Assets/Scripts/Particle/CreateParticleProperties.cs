﻿using UnityEngine;

namespace Client.Particle
{
    internal struct CreateParticleProperties
    {
        internal bool Force;
        internal bool AutoZRotation;
        internal bool AutoReturn;
        internal Vector2 LocalPosition;
        internal Vector3? Rotation;
        internal Transform Parent;
        internal bool DontAutoPlay;
    }
}