﻿using UnityEngine;

namespace Client.Particle
{
    [RequireComponent(typeof(ParticleSystem))]
    internal sealed class ParticleAutoEmission : MonoBehaviour
    {
        public void OnDestroy()
        {
            var emission = GetComponent<ParticleSystem>().emission;
            emission.enabled = true;
        }
    }
}