﻿using UnityEngine;

namespace Client.Particle
{
    [RequireComponent(typeof(ParticleSystem))]
    internal sealed class ParticleAutoReturn : MonoBehaviour
    {
        public void OnParticleSystemStopped() => ParticleFactory.ReturnParticle(transform.GetComponent<ParticleSystem>(), this);
    }
}