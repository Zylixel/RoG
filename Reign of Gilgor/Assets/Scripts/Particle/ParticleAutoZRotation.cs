using Client.Game.Map;
using UnityEngine;

namespace Client.Particle
{
    internal sealed class ParticleAutoZRotation : MonoBehaviour
    {
        private new Transform transform;

        private void OnEnable() => transform = base.transform;

        private void Update() => transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, Map.Rotation.Deg);
    }
}