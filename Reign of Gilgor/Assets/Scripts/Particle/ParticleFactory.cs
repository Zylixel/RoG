﻿using System.Collections;
using System.Collections.Generic;
using Client.Game;
using Microsoft.Extensions.Logging;
using RoGCore.Assets;
using UnityEngine;
using static Client.Structures.ObjectExtensions;
using static Client.Structures.ProjectileExtensions;
using Random = System.Random;

#nullable enable

namespace Client.Particle
{
    internal sealed class ParticleFactory : Initializable<ParticleFactory>
    {
        private static readonly Random ParticleRandom = new Random();

        private static readonly Dictionary<string, List<ParticleSystem>> ParticleSystems = new Dictionary<string, List<ParticleSystem>>();

        public override void Initialize() => Instance = this;

        private static ParticleSystem? LoadParticle(string type, bool force = false)
        {
            if (!force && !OptionsHandler.GetOptionValue<bool>("particleMaster"))
            {
                return null;
            }

            ParticleSystem ret;
            if (!ParticleSystems.TryGetValue(type, out var systemList))
            {
                systemList = new List<ParticleSystem>();
                ParticleSystems.Add(type, systemList);
            }

            if (systemList.Count > 0)
            {
                ret = systemList[0];
                systemList.Remove(ret);
                ret.gameObject.SetActive(true);
            }
            else
            {
                ret = Instantiate(GamePrefabs.GetObject("Particle/" + type)).GetComponent<ParticleSystem>();
                ret.transform.SetParent(Instance.transform);
            }

            return ret;
        }

        internal static void CreateHitParticle(Vector3 pos, ObjectDescription objectDescription)
        {
            var particle = LoadParticle("HitParticle");
            if (particle == null)
            {
                return;
            }

            particle.transform.position = pos;
            particle.transform.eulerAngles = new Vector3(180f, 0f, -Game.Map.Map.Rotation.Deg);
            var main = particle.main;

            main.startColor = UnityEngine.Random.value > 0.5f
                ? new ParticleSystem.MinMaxGradient(
                    objectDescription.GetRandomColor(ParticleRandom),
                    objectDescription.GetRandomColor(ParticleRandom))
                : new ParticleSystem.MinMaxGradient(darkRedColor, Color.red);

            var autoReturn = particle.gameObject.AddComponent<ParticleAutoReturn>();

            var renderer = particle.GetComponent<ParticleSystemRenderer>();
            renderer.sortingLayerName = "Projectile";
            autoReturn.StartCoroutine(ChangeHitParticleSortingLayer(renderer));

            particle.Play();
        }

        internal static void CreateDeathParticle(Vector3 pos)
        {
            var particle = LoadParticle("DeathParticle");
            if (particle == null)
            {
                return;
            }

            particle.transform.position = pos;
            particle.transform.eulerAngles = new Vector3(180f, 0f, -Game.Map.Map.Rotation.Deg);

            var autoReturn = particle.gameObject.AddComponent<ParticleAutoReturn>();

            var renderer = particle.GetComponent<ParticleSystemRenderer>();
            renderer.sortingLayerName = "Projectile";
            autoReturn.StartCoroutine(ChangeHitParticleSortingLayer(renderer));

            particle.Play();
        }

        private static Color darkRedColor = new Color(0.33f, 0f, 0f, 1f);

        private static readonly WaitForSeconds WaitForSeconds = new WaitForSeconds(0.5f);

        private static IEnumerator ChangeHitParticleSortingLayer(ParticleSystemRenderer renderer)
        {
            yield return WaitForSeconds;

            renderer.sortingLayerName = "Ground";
        }

        internal static void CreateProjectileDeathParticle(Vector2 pos, ProjectileDescription objectDescription)
        {
            var particle = LoadParticle("ProjectileDeathParticle");
            if (particle == null)
            {
                return;
            }

            particle.transform.position = new Vector3(pos.x, pos.y, -0.2f);
            particle.transform.eulerAngles = new Vector3(180f, 0f, Game.Map.Map.Rotation.Deg);
            particle.gameObject.AddComponent<ParticleAutoReturn>();
            var main = particle.main;
            main.startColor = new ParticleSystem.MinMaxGradient(
                objectDescription.GetRandomColor(),
                objectDescription.GetRandomColor());
            particle.Play();
        }

        internal static void CreatePlayerSpawnProjectile(Vector2 pos)
        {
            var particle = LoadParticle("PlayerSpawnParticle");
            if (particle == null)
            {
                return;
            }

            particle.transform.position = new Vector3(pos.x, pos.y, -0.2f);
            particle.gameObject.AddComponent<ParticleAutoReturn>();
            particle.Play();
        }

        internal static ParticleSystem? CreateParticle(
            string name,
            CreateParticleProperties createProperties = default)
        {
            var particle = LoadParticle(name, createProperties.Force);
            if (particle == null)
            {
                return null;
            }

            particle.gameObject.SetActive(true);

            if (createProperties.Parent)
            {
                particle.transform.SetParent(createProperties.Parent);
            }

            particle.transform.localPosition = createProperties.LocalPosition;
            particle.transform.eulerAngles = createProperties.Rotation ?? Game.Map.Map.Rotation.ToUnity3();
            particle.transform.localScale = Vector2.one;
            if (createProperties.AutoReturn)
            {
                particle.gameObject.AddComponent<ParticleAutoReturn>();
            }

            if (createProperties.AutoZRotation)
            {
                particle.gameObject.AddComponent<ParticleAutoZRotation>();
            }

            if (!createProperties.DontAutoPlay && !particle.isPlaying)
            {
                particle.Play();
            }

            return particle;
        }

        internal static void ReturnParticle(ParticleSystem particle, ParticleAutoReturn? autoBehavior = null)
        {
            if (autoBehavior != null)
            {
                Destroy(autoBehavior);
            }

            var particleAutoEmission = particle.GetComponent<ParticleAutoEmission>();
            if (particleAutoEmission != null)
            {
                Destroy(particleAutoEmission);
            }

            var particleAutoZRotation = particle.GetComponent<ParticleAutoZRotation>();
            if (particleAutoZRotation != null)
            {
                Destroy(particleAutoZRotation);
            }

            particle.gameObject.transform.SetParent(Instance.transform);
            particle.gameObject.SetActive(false);
            if (!ParticleSystems.TryGetValue(
                particle.gameObject.name.Replace("(Clone)", string.Empty),
                out var systemList))
            {
                Logger.LogError(
                    $"Couldn't find particle system list with the name {particle.gameObject.name.Replace("(Clone)", string.Empty)}");
            }
            else
            {
                systemList.Add(particle);
            }
        }
    }
}