using UnityEngine;

namespace Client.Particle
{
    [RequireComponent(typeof(ParticleSystem))]
    public class ParticleOnBegin : MonoBehaviour
    {
        private new ParticleSystem particleSystem;
        private UnityEngine.ParticleSystem.Particle[] particles;
        private int lastParticleCount;

        internal event Client.Game.Event.Delegates.EmptyGameEvent SystemBegin;

        private void Awake()
        {
            particleSystem = GetComponent<ParticleSystem>();
            lastParticleCount = particleSystem.particleCount;
            particles = new UnityEngine.ParticleSystem.Particle[particleSystem.main.maxParticles];
        }

        private void LateUpdate()
        {
            var particleCount = particleSystem.particleCount;

            if (lastParticleCount == 0 && particleCount > 0)
            {
                SystemBegin?.Invoke();
            }

            lastParticleCount = particleCount;
        }
    }
}