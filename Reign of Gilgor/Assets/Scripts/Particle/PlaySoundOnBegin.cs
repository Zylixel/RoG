using Client.Audio;
using UnityEngine;

namespace Client.Particle
{
    [RequireComponent(typeof(ParticleOnBegin))]
    public class PlaySoundOnBegin : MonoBehaviour
    {
        public string Sound;

        public bool Positional = true;

        private ParticleOnBegin beginHandler;

        private void Start()
        {
            beginHandler = GetComponent<ParticleOnBegin>();
            beginHandler.SystemBegin += PlaySound;
        }

        private void OnDestroy() => beginHandler.SystemBegin -= PlaySound;

        private void PlaySound()
        {
            if (Positional)
            {
                AudioHandler.PlaySound(Sound, transform);
            }
            else
            {
                AudioHandler.PlaySound(Sound);
            }
        }
    }
}