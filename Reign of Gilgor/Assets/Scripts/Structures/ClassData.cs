﻿using System.Collections.Generic;

internal class ClassData
{
    internal static readonly Dictionary<string, ClassData> CharacterDescriptions = new Dictionary<string, ClassData>()
    {
        {
            "Deceiver",
            new ClassData(
                "The Deceiver uses her skills to trick the enemy. Art by BLOODQWEN.",
                WeaponType.Dagger, GarmentType.Leather)
        },
        {
            "Huntress",
            new ClassData(
                "The Huntress uses her a long-range attack with the skills of a experienced mercenary. Art by BLOODQWEN.",
                WeaponType.Bow, GarmentType.Leather)
        },
        {
            "Sorcerer",
            new ClassData(
                "The Sorcerer uses the dark arts to defeat his foes. Art by BLOODQWEN.",
                WeaponType.Wand, GarmentType.Robe)
        },
        {
            "Warrior",
            new ClassData(
                "The Warrior gets close and personal to enemies and uses their skills to demolish monsters. Art by BLOODQWEN.",
                WeaponType.Sword, GarmentType.Armor)
        },
        {
            "Warlock",
            new ClassData(
                "The Warlock uses his short range weapon with long range attacks. Art by BLOODQWEN.",
                WeaponType.Sword, GarmentType.Robe)
        },
    };

    internal string Description;
    internal GarmentType Garment;
    internal WeaponType Weapon;

    internal ClassData(string description, WeaponType firstSlot, GarmentType thirdSlot)
    {
        Description = description;
        Weapon = firstSlot;
        Garment = thirdSlot;
    }
}

internal enum WeaponType
{
    Bow,
    Staff,
    Dagger,
    Sword,
    Katana,
    Wand,
}

internal enum GarmentType
{
    Robe,
    Leather,
    Armor,
}