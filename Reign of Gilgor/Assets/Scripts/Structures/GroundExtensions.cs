﻿using System.Collections.Generic;
using System.Linq;
using Client.Utils;
using Microsoft.Extensions.Logging;
using RoGCore.Assets;
using RoGCore.Logging;
using UnityEngine;

namespace Client.Structures
{
    internal static class GroundExtensions
    {
        private static readonly Microsoft.Extensions.Logging.ILogger Logger = LogFactory.LoggingInstance(nameof(GroundExtensions));

        internal static void PostInitialization(this GroundDescription ground, EmbeddedData embedded)
        {
            var textures = new List<Sprite>();
            var averageColors = new List<Color32>();

            if (ground.TextureIndex is null)
            {
                Logger.LogError("TextureIndex is null for: " + (ground.Name ?? string.Empty));
            }

            for (var i = 0; i < ground.TextureIndex.Length; i++)
            {
                var sprite = embedded.GetSprite("Ground", ground.TextureIndex[i]);

                textures.Add(sprite);

                if (ground.Name == "Nothing")
                {
                    averageColors.Add(Color.clear);
                    continue;
                }

                var colors = sprite.texture.GetPixels(
                     (int)sprite.rect.x,
                     (int)sprite.rect.y,
                     (int)sprite.rect.width,
                     (int)sprite.rect.height);

                // Store how many times a color appears
                var dict = new Dictionary<Color32, int>();
                for (var j = 0; j < colors.Length; j++)
                {
                    if (dict.ContainsKey(colors[j]))
                    {
                        dict[colors[j]]++;
                    }
                    else
                    {
                        dict.Add(colors[j], 1);
                    }
                }

                // Order from most used to least used
                var sorted = dict.OrderByDescending(x => x.Value);

                // Grab the most used color
                averageColors.Add(sorted.First().Key);

                ground.RandomColors = sprite.GetRandomColors();
            }

            ground.Textures = textures.ToArray();
            ground.AverageColors = averageColors.ToArray();
        }

        internal static Color GetRandomColor(this GroundDescription ground, System.Random rand) => ground.RandomColors[rand.Next(0, ground.RandomColors.Length)];
    }
}