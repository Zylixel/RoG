﻿using RoGCore.Assets;
using RoGCore.Models;

namespace Structures
{
    internal interface IContainer
    {
        Item?[] Inventory { get; set; }

        ObjectDescription ObjectDescProperty { get; }
    }
}