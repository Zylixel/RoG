﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using Microsoft.Extensions.Logging;
using RoGCore.Logging;
using TMPro;
using UnityEngine;

namespace Client
{
    public abstract class Initializable<T> : Initializable
        where T : class
    {
#if UNITY_EDITOR || DEBUG || DEVELOPMENT_BUILD
        private static T instance;

        internal static T Instance
        {
            get => instance;
            set
            {
                if (instance is not null)
                {
                    Logger.LogWarning($"Re-assigning instance for {value.GetType()}, this behaviour is typically unsupported");
                }

                instance = value;
            }
        }

#else
        internal static T Instance;
#endif
        protected static readonly ILogger<T> Logger = LogFactory.LoggingInstance<T>();

        public override void Initialize() => Instance = this as T;
    }

    public abstract class Initializable : MonoBehaviour
    {
        public abstract void Initialize();

        public virtual int InitializeOrder() => 100;

        public virtual bool NeedsGameData() => false;
    }

    internal static class Initializer
    {
        private static readonly Dictionary<int, List<Initializable>> InitializeOrder;
        private static readonly Microsoft.Extensions.Logging.ILogger Logger = LogFactory.LoggingInstance(nameof(Initializer));

        static Initializer()
        {
            InitializeOrder = new Dictionary<int, List<Initializable>>();

            foreach (var initializableClass in GetInitializableClasses())
            {
                var @object = GetInitializableObject(initializableClass);

                if (@object == null)
                {
                    continue;
                }

                if (InitializeOrder.ContainsKey(@object.InitializeOrder()))
                {
                    InitializeOrder[@object.InitializeOrder()].Add(@object);
                }
                else
                {
                    InitializeOrder.Add(@object.InitializeOrder(), new List<Initializable> { @object });
                }
            }
        }

        internal static void InitializeStaticClasses(TMP_Text output)
        {
            foreach (var type in Assembly.GetExecutingAssembly().GetTypes().Where(x => x.IsAbstract && x.IsSealed))
            {
                output.text = $"Initializing {type.Name}";

                RuntimeHelpers.RunClassConstructor(type.TypeHandle);
            }
        }

        internal static void InitializeObjects(TMP_Text output)
        {
            foreach (var initializableClasses in InitializeOrder.OrderBy(x => x.Key))
            {
                foreach (var initializableClass in initializableClasses.Value.Where(initializableClass => !initializableClass.NeedsGameData()))
                {
                    initializableClass.Initialize();
                    output.text = $"Initializing {initializableClass}";
                }
            }
        }

        internal static void InitializeGameDataObjects(TMP_Text output)
        {
            foreach (var initializableClasses in InitializeOrder.OrderBy(x => x.Key))
            {
                foreach (var initializableClass in initializableClasses.Value.Where(initializableClass => initializableClass.NeedsGameData()))
                {
                    initializableClass.Initialize();
                    output.text = $"Initializing {initializableClass}";
                }
            }
        }

        private static IEnumerable<Type> GetInitializableClasses()
        {
            var t = typeof(Initializable);
            return t.Assembly.GetTypes().Where(x => t.IsAssignableFrom(x) && x != t);
        }

        internal static Initializable GetInitializableObject(Type @class)
        {
            if (@class == typeof(Initializable<>) || @class.GetTypeInfo().IsAbstract)
            {
                return null;
            }

            var objects = Resources.FindObjectsOfTypeAll(@class);

            if (objects.Length != 0)
            {
                return objects[0] as Initializable;
            }

            Logger.LogError($"Cannot find class {@class} while loading the game");
            return null;
        }
    }
}