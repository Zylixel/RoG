﻿using System.Collections.Generic;
using RoGCore.Assets;
using UnityEngine;

namespace Client.Structures
{
    internal static class JsonItemExtensions
    {
        internal static void PostInitialization(this JsonItem item, EmbeddedData embedded)
        {
            item.Textures = new List<Sprite>();
            for (var i = 0; i < item.TextureIndex.Length; i++)
            {
                item.Textures.Add(embedded.GetSprite(item.TextureFile, item.TextureIndex[i]));
            }
        }
    }
}