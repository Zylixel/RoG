﻿using System.Collections.Generic;
using System.Linq;
using Client.Utils;
using Microsoft.Extensions.Logging;
using RoGCore.Assets;
using RoGCore.Logging;
using RoGCore.Utils;
using UnityEngine;

#nullable enable

namespace Client.Structures
{
    internal static class ObjectExtensions
    {
        private static readonly Microsoft.Extensions.Logging.ILogger Logger = LogFactory.LoggingInstance(nameof(ObjectExtensions));

        internal static void PostInitialization(this ObjectDescription objectDescription, EmbeddedData embedded)
        {
            if (objectDescription.Texture is not null)
            {
                objectDescription.Textures = new Sprite[objectDescription.Texture.Index?.Length ?? 0];
                objectDescription.AverageColors = new Color32[objectDescription.Texture.Index?.Length ?? 0];

                if (objectDescription.Textures.Length == 1)
                {
                    objectDescription.Texture.Type = TextureData.TEXTURETYPEFIRST;
                }

                for (var i = 0; i < objectDescription.Textures.Length; i++)
                {
                    if (objectDescription.Texture.File is null || objectDescription.Texture.Index is null)
                    {
                        continue;
                    }

                    var sprite = embedded.GetSprite(
                        objectDescription.Texture.File,
                        objectDescription.Texture.Index[i]);

                    if (sprite == null)
                    {
                        continue;
                    }

                    objectDescription.Textures[i] = sprite;

                    var colors = sprite.texture.GetPixels(
                        (int)sprite.textureRect.x,
                        (int)sprite.textureRect.y,
                        (int)sprite.textureRect.width,
                        (int)sprite.textureRect.height);

                    // Store how many times a color appears
                    var dict = new Dictionary<Color32, int>();
                    for (var j = 0; j < colors.Length; j++)
                    {
                        if (colors[j].a != 1 || colors[j].r + colors[j].g + colors[j].b == 0)
                        {
                            continue;
                        }

                        if (dict.ContainsKey(colors[j]))
                        {
                            dict[colors[j]]++;
                        }
                        else
                        {
                            dict.Add(colors[j], 1);
                        }
                    }

                    // Order from most used to least used
                    var sorted = dict.OrderByDescending(x => x.Value);
                    if (sorted.Any())
                    {
                        // Grab the most used color
                        objectDescription.AverageColors[i] = sorted.First().Key;
                    }
                    else
                    {
                        objectDescription.AverageColors[i] = Color.clear;
                    }
                }

                objectDescription.TopTextures = new Sprite[objectDescription.Texture.TopIndex?.Length ?? 0];
                objectDescription.AverageTopColors = new Color32[objectDescription.Texture.TopIndex?.Length ?? 0];
                for (var i = 0; i < objectDescription.TopTextures.Length; i++)
                {
                    if (objectDescription.Texture.TopFile is null || objectDescription.Texture.TopIndex is null)
                    {
                        continue;
                    }

                    var sprite = embedded.GetSprite(
                        objectDescription.Texture.TopFile,
                        objectDescription.Texture.TopIndex[i]);

                    if (sprite == null)
                    {
                        continue;
                    }

                    objectDescription.TopTextures[i] = sprite;

                    var colors = sprite.texture.GetPixels(
                        (int)sprite.rect.x,
                        (int)sprite.rect.y,
                        (int)sprite.rect.width,
                        (int)sprite.rect.height);

                    // Store how many times a color appears
                    var dict = new Dictionary<Color32, int>();
                    for (var j = 0; j < colors.Length; j++)
                    {
                        if (colors[j].a != 1 || colors[j].r + colors[j].g + colors[j].b == 0)
                        {
                            continue;
                        }

                        if (dict.ContainsKey(colors[j]))
                        {
                            dict[colors[j]]++;
                        }
                        else
                        {
                            dict.Add(colors[j], 1);
                        }
                    }

                    if (dict.Count == 0)
                    {
#if UNITY_EDITOR || DEVELOPMENT_BUILD
                        Logger.LogWarning(objectDescription.Name + "Has no average top colors");
#endif
                        continue;
                    }

                    // Order from most used to least used
                    var sorted = dict.OrderByDescending(x => x.Value);

                    // Grab the most used color
                    objectDescription.AverageTopColors[i] = sorted.First().Key;
                }

                objectDescription.RandomColors = objectDescription.Textures[0].GetRandomColors();
            }

            if (StringUtils.IsNullOrWhiteSpace(objectDescription.LightColor))
            {
                return;
            }
#if UNITY_EDITOR || DEVELOPMENT_BUILD
            if (!ColorUtility.TryParseHtmlString(
                $"#{objectDescription.LightColor}",
                out objectDescription.LightColorProcessed))
            {
                Logger.LogError(
                    $"Error loading color {objectDescription.LightColor} in Object.cs. ObjectType: {objectDescription.ObjectType}");
            }
#else
            ColorUtility.TryParseHtmlString($"#{objectDescription.LightColor}", out objectDescription.LightColorProcessed);
#endif

        }

        internal static Color GetRandomColor(this ObjectDescription objectDescription, System.Random rand) => objectDescription.RandomColors[rand.Next(0, objectDescription.RandomColors.Length)];
    }
}