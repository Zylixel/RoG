﻿using Client.Utils;
using Microsoft.Extensions.Logging;
using RoGCore.Assets;
using RoGCore.Logging;
using RoGCore.Utils;
using UnityEngine;

namespace Client.Structures
{
    internal static class ProjectileExtensions
    {
        private static readonly Microsoft.Extensions.Logging.ILogger Logger = LogFactory.LoggingInstance(nameof(ProjectileExtensions));

        internal static void PostInitialization(this ProjectileDescription projectile, EmbeddedData embedded)
        {
            ProcessTextures(projectile, embedded);
            ProcessLightColor(projectile);
            ProcessTintColor(projectile);
            ProcessRandomColors(projectile);
        }

        private static void ProcessTextures(ProjectileDescription projectile, EmbeddedData embedded)
        {
            if (projectile.Texture is null)
            {
                return;
            }

            projectile.Textures = new Sprite[projectile.Texture.Index.Length];
            for (var i = 0; i < projectile.Texture.Index.Length; i++)
            {
                projectile.Textures[i] = embedded.GetSprite(projectile.Texture.File, projectile.Texture.Index[i]);
            }
        }

        private static void ProcessTintColor(ProjectileDescription projectile)
        {
            if (projectile.TintColor.IsNullOrWhiteSpace())
            {
                return;
            }

            if (!ColorUtility.TryParseHtmlString($"#{projectile.TintColor}", out projectile.TintColorProcessed))
            {
                Logger.LogError(
                    $"Error loading color {projectile.TintColor} in Projectile.cs. Projectile: {projectile.Name}");
            }

            projectile.TintColorProcessed *= Mathf.Pow(2f, projectile.TintIntensity);
        }

        private static void ProcessLightColor(ProjectileDescription projectile)
        {
            if (projectile.LightColor.IsNullOrWhiteSpace())
            {
                return;
            }

            if (!ColorUtility.TryParseHtmlString($"#{projectile.LightColor}", out projectile.LightColorProcessed))
            {
                Logger.LogError(
                    $"Error loading color {projectile.LightColor} in Projectile.cs. Projectile: {projectile.Name}");
            }
        }

        private static void ProcessRandomColors(ProjectileDescription projectile)
        {
            if (projectile.Texture is not null)
            {
                projectile.RandomColors = projectile.Textures[0].GetRandomColors();
            }
        }

        internal static Color GetRandomColor(this ProjectileDescription objectDescription) => objectDescription.RandomColors is null
                ? Color.clear
                : objectDescription.RandomColors[Random.Range(0, objectDescription.RandomColors.Length)];
    }
}