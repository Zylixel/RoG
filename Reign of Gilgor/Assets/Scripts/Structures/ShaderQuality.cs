﻿namespace Client.Structures
{
    internal enum ShaderQuality : byte
    {
        Low,
        Medium,
        High,
    }
}
