﻿using System.Collections.Generic;
using System.IO;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RoGCore.Logging;
using RoGCore.Models;

public sealed class SkillTree
{
    internal static Dictionary<SkillType, Skill> SkillLookup;

    private static readonly ILogger<SkillTree> Logger = LogFactory.LoggingInstance<SkillTree>();

    private static List<Skill> Skills;

    public static void Initialize(string json)
    {
        // load skills
        Skills = JsonConvert.DeserializeObject<List<Skill>>(json);

        // populate lookup dict
        SkillLookup = new Dictionary<SkillType, Skill>(new SkillTypeComparer());
        foreach (var skill in Skills)
        {
            SkillLookup.Add(skill.Type, skill);
        }

        // resolve skill network
        foreach (var skill in Skills)
        {
            if (skill.Requirements is null)
            {
                continue;
            }

            foreach (var req in skill.Requirements)
            {
                SkillLookup[req].Next.Add(skill);
            }
        }
    }

    internal static HashSet<Skill> Read(byte[] bytes)
    {
        var ret = new HashSet<Skill>();
        if (bytes == null)
        {
            return ret;
        }

        try
        {
            var reader = new NReader(new MemoryStream(bytes));
            while (reader.BaseStream.Position != reader.BaseStream.Length)
            {
                var type = (SkillType)reader.ReadUInt16();
                if (SkillLookup.TryGetValue(type, out var skill))
                {
                    ret.Add(skill);
                }
                else
                {
                    Logger.LogError($"Failed to load skill {type}");
                }
            }
        }
        catch
        {
            Logger.LogError("Failed to load skills");
            return new HashSet<Skill>();
        }

        return ret;
    }

    internal static byte[] Write(HashSet<Skill> skills)
    {
        var ret = new MemoryStream();
        using (var wtr = new NWriter(ret))
        {
            foreach (var skill in skills)
            {
                wtr.Write((ushort)skill.Type);
            }
        }

        return ret.ToArray();
    }
}

internal sealed class SkillTypeComparer : IEqualityComparer<SkillType>
{
    public bool Equals(SkillType x, SkillType y) => x == y;

    public int GetHashCode(SkillType x) => (int)x;
}