﻿using UnityEngine;

namespace Client.Structures
{
    internal readonly struct SpriteSheet
    {
        internal readonly string FileName;
        internal readonly Sprite[] Sprites;
        internal readonly bool UseAtlas;
        internal readonly SpriteSheetType Type;
        internal readonly bool Outline;

        public SpriteSheet(string fileName, bool useAtlas = true, SpriteSheetType type = SpriteSheetType.Pad, bool outline = true)
        {
            FileName = fileName;
            Sprites = Resources.LoadAll<Sprite>($"Sprites/{fileName}");
            UseAtlas = useAtlas;
            Type = type;
            Outline = outline;
        }

        internal enum SpriteSheetType : byte
        {
            Pad,
            Expand,
        }
    }
}
