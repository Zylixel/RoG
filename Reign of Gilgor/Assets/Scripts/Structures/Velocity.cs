﻿using RoGCore.Models;
using UnityEngine;

namespace Structures
{
    internal struct Velocity
    {
        internal Vector2 Vector { get; }

        internal Angle Angle { get; }

        internal Velocity(float x, float y)
        {
            Vector = new Vector2(x, y);
            Angle = new Angle(Mathf.Atan2(y, x));
        }

        internal Velocity(Vector2 vector) : this(vector.x, vector.y)
        {
        }
    }
}