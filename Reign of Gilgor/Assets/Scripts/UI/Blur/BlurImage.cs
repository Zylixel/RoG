﻿using Client.Camera;
using UnityEngine;
using UnityEngine.UI;

namespace Client.UI.Blur
{
    internal class BlurImage : RawImage
    {
        protected override void Start()
        {
            texture = CameraStackBlurEffect.NewOutputCreated.LastValue;
            CameraStackBlurEffect.NewOutputCreated.Begin += (RenderTexture newTexture) => texture = newTexture;
            base.Start();
        }
    }
}
