﻿#if UNITY_EDITOR

using UnityEditor;
using UnityEditor.UI;

namespace Client.UI.Blur
{
    [CustomEditor(typeof(BlurImage))]
    internal sealed class BlurImageEditor : RawImageEditor
    {
        public override void OnInspectorGUI()
        {
            var targetBlurImage = (BlurImage)target;

            targetBlurImage.color = EditorGUILayout.ColorField("Color:", targetBlurImage.color);
        }
    }
}

#endif