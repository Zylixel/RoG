﻿using Client.Game;
using UnityEngine;

#nullable enable

namespace Client.UI.Blur
{
    [RequireComponent(typeof(RenderTextureGamescreenProvider))]
    public class BlurScreen : Initializable<BlurScreen>
    {
        private const float SCALE = 0.25f;

        [SerializeField]
        private Material? blurMaterial;

        [System.Diagnostics.CodeAnalysis.AllowNull]
        private RenderTextureGamescreenProvider textureProvider;

        private RenderTexture? scaledTexture;

        private void Start()
        {
            textureProvider = GetComponent<RenderTextureGamescreenProvider>();
            textureProvider.TextureUpdated += UpdateScaledTexture;

            UpdateScaledTexture();
        }

        private void UpdateScaledTexture()
        {
            if (scaledTexture != null)
            {
                scaledTexture.Release();
                Destroy(scaledTexture);
            }

            scaledTexture = new RenderTexture(
                (int)(textureProvider.Texture.width * SCALE),
                (int)(textureProvider.Texture.height * SCALE),
                textureProvider.Texture.depth,
                textureProvider.Texture.format);

            if (blurMaterial != null)
            {
                blurMaterial.SetTexture("_BlurTexture", scaledTexture);
            }
        }

        internal void TakeScreenshot()
        {
            var screenshotTexture = textureProvider.Texture;
            ScreenCapture.CaptureScreenshotIntoRenderTexture(screenshotTexture);

            TextureScale.GPUScale(ref scaledTexture, ref screenshotTexture, FilterMode.Bilinear);
        }
    }
}