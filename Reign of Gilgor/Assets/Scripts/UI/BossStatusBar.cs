using System.Linq;
using Client.Camera;
using Client.Entity;
using Client.Entity.Helpers;
using Client.Game;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Client.UI
{
    [RequireComponent(typeof(CanvasGroup))]
    [RequireComponent(typeof(Image))]
    public class BossStatusBar : Initializable<BossStatusBar>
    {
        [SerializeField]
        private Image icon;
        [SerializeField]
        private Image hpBar;
        [SerializeField]
        private Image comboBar;
        [SerializeField]
        private TextMeshProUGUI hpText;
        [SerializeField]
        private TextMeshProUGUI percentage;

        internal delegate void BossStatusBarDelegate(bool status);

        internal static event BossStatusBarDelegate BossStatusBarChanged;

        private CanvasGroup canvasGroup;
        private Image background;

        private Vector2 hpBarFullVector;
        private float hpBarTween;
        private int hp;
        private int maxHp;
        private int comboDesiredHp;
        private int comboHp;

        private int lastHp;
        private Sequence hitEffect;

        private bool removing;

        private static Enemy currentBoss;

        internal static Enemy CurrentBoss
        {
            get => currentBoss;
            set
            {
                if (currentBoss != null)
                {
                    currentBoss.OnVanish -= Instance.Remove;
                }

                currentBoss = value;

                if (currentBoss != null)
                {
                    Instance.gameObject.SetActive(true);
                }
            }
        }

        public override void Initialize()
        {
            Instance = this;
            canvasGroup = GetComponent<CanvasGroup>();
            background = GetComponent<Image>();

            // Create a copy of the material so the alpha can be edited.
            background.material = new Material(background.material);
            background.material.SetTexture(ShaderProperties.BlurTexture, CameraStackBlurEffect.NewOutputCreated.LastValue);
            CameraStackBlurEffect.NewOutputCreated.Begin += (RenderTexture newTexture) => background.material.SetTexture(ShaderProperties.BlurTexture, newTexture);

            hpBarFullVector = hpBar.rectTransform.sizeDelta;

            gameObject.SetActive(false);
        }

        private void Update()
        {
            background.material.SetFloat(ShaderProperties.Alpha, canvasGroup.alpha);

            if (CurrentBoss != null)
            {
                if (!removing && CurrentBoss.SpriteRenderer.sprite != null && CurrentBoss.SpriteRenderer.sprite.rect.height == CurrentBoss.SpriteRenderer.sprite.rect.width)
                {
                    icon.sprite = CurrentBoss.SpriteRenderer.sprite;
                    icon.transform.eulerAngles = new Vector3(0, CurrentBoss.SpriteRenderer.flipX ? 180 : 0, 0);
                }

                if (lastHp != CurrentBoss.HP)
                {
                    if (!hitEffect.IsActive())
                    {
                        hitEffect = DOTween.Sequence()
                        .Append(hpText.transform.DOScale(new Vector3(1.1f, 1.1f, 1f), 0.05f))
                        .Append(hpText.transform.DOScale(new Vector3(1f, 1f, 1f), 0.05f));
                    }

                    lastHp = CurrentBoss.HP;
                }

                var shownHpDiff = (CurrentBoss.HP - hp) * Mathf.Min(5f * Time.deltaTime, 1f);
                hp += shownHpDiff > 0 ? Mathf.CeilToInt(shownHpDiff) : Mathf.FloorToInt(shownHpDiff);

                if (shownHpDiff >= 0f)
                {
                    comboDesiredHp = hp;
                }

                if (shownHpDiff > 0)
                {
                    // Instant
                    comboHp = hp;
                }
                else
                {
                    // Interpolate
                    shownHpDiff = (comboDesiredHp - comboHp) * Mathf.Min(10f * Time.deltaTime, 1f);
                    comboHp += shownHpDiff > 0 ? Mathf.CeilToInt(shownHpDiff) : Mathf.FloorToInt(shownHpDiff);
                }

                shownHpDiff = (CurrentBoss.MaxHP - maxHp) * Mathf.Min(5f * Time.deltaTime, 1f);
                maxHp += shownHpDiff > 0 ? Mathf.CeilToInt(shownHpDiff) : Mathf.FloorToInt(shownHpDiff);

                percentage.text = (int)CurrentBoss.DamagePercentFromPlayer + "%";
            }

            var hpPercent = hp * hpBarTween / maxHp;
            hpBar.rectTransform.sizeDelta = new Vector2(hpPercent * hpBarFullVector.x, hpBarFullVector.y);
            hpBar.color = CurrentBoss.ConditionEffects.HasConditionEffect(RoGCore.Models.ConditionEffects.Invincible) ? GameColors.HP_BLUE : HpBar.GetHealthColor(hpPercent);

            var comboPercent = comboHp * hpBarTween / maxHp;
            comboBar.rectTransform.sizeDelta = new Vector2(comboPercent * hpBarFullVector.x, hpBarFullVector.y);

            hpText.text = HumanizeHp((int)(hp * hpBarTween)) + "/" + HumanizeHp(maxHp);
        }

        private string HumanizeHp(int hp)
        {
            var mag = (int)(Mathf.Floor(Mathf.Log10(hp)) / 3);
            var divisor = Mathf.Pow(10, mag * 3);

            var shortNumber = hp / divisor;
            var suffix = mag switch
            {
                0 => string.Empty,
                1 => "K",
                2 => "M",
                3 => "B",
                _ => string.Empty,
            };

            return shortNumber.ToString("N1") + suffix;
        }

        private void OnEnable()
        {
            BossStatusBarChanged.Invoke(true);
            lastHp = 0;

            canvasGroup.alpha = 0;
            DOTween.To(() => canvasGroup.alpha, (val) => canvasGroup.alpha = val, 1f, 1f);

            hpBarTween = 0;
            DOTween.To(() => hpBarTween, (val) => hpBarTween = val, 1f, 1.5f);

            CurrentBoss.OnVanish += Remove;

            icon.sprite = CurrentBoss.SpriteRenderer.sprite;
            Update();
        }

        private void Remove()
        {
            removing = true;

            DOTween.Sequence()
                .Append(DOTween.To(() => canvasGroup.alpha, (val) => canvasGroup.alpha = val, 0f, 1f))
                .Join(DOTween.To(() => hpBarTween, (val) => hpBarTween = val, 0f, 1f))
                .AppendCallback(() =>
                {
                    gameObject.SetActive(false);
                    BossStatusBarChanged.Invoke(false);
                    CurrentBoss = null;
                    removing = false;
                });
        }
    }
}