﻿using Client.Audio;
using Microsoft.Extensions.Logging;
using RoGCore.Logging;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

#nullable enable

namespace Client.UI.Button
{
    internal class EnhancedButton : UnityEngine.UI.Button
    {
        [SerializeField]
        internal bool PlaySoundOnClick = true;

        private Text? text;
        private TextMeshProUGUI? textMP;

        private static readonly ILogger<EnhancedButton> Logger = LogFactory.LoggingInstance<EnhancedButton>();

        private new void Awake()
        {
            base.Awake();

            if (PlaySoundOnClick)
            {
                onClick.AddListener(() => AudioHandler.PlaySound("Fantasy/Special Click 03", 0.33f));
            }
        }

        internal void SetTextColor(Color color)
        {
            TryLoadTextObject();

            if (text != null)
            {
                text.color = color;
            }
            else if (textMP != null)
            {
                textMP.color = color;
            }
        }

        internal void SetText(string value)
        {
            TryLoadTextObject();

            if (text != null)
            {
                text.text = value;
            }
            else if (textMP != null)
            {
                textMP.text = value;
            }
        }

        internal string GetTextValue()
        {
            TryLoadTextObject();

            if (text != null)
            {
                return text.text;
            }
            else if (textMP != null)
            {
                return textMP.text;
            }
            else
            {
                Logger.LogWarning($"GameObject: {gameObject.name} - There is no text component in children to get.");
                return string.Empty;
            }
        }

        private void TryLoadTextObject()
        {
            if (text == null && textMP == null)
            {
                text = GetComponentInChildren<Text>(true);
                textMP = GetComponentInChildren<TextMeshProUGUI>(true);
            }
            else
            {
                return;
            }

            if (text == null && textMP == null)
            {
                Logger.LogWarning($"GameObject: {gameObject.name} - There is no text component in children to set.");
                return;
            }
        }
    }
}
