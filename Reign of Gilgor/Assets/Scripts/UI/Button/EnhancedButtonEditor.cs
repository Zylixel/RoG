﻿#if UNITY_EDITOR

using UnityEditor;
using UnityEditor.UI;

namespace Client.UI.Button
{
    [CustomEditor(typeof(EnhancedButton), true)]
    internal class EnhancedButtonEditor : ButtonEditor
    {
        public override void OnInspectorGUI()
        {
            var targetMyButton = (EnhancedButton)target;

            targetMyButton.PlaySoundOnClick = EditorGUILayout.Toggle("Play Sound On Click:", targetMyButton.PlaySoundOnClick);

            base.OnInspectorGUI();
        }
    }
}

#endif