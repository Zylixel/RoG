using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Client.UI.Button
{
    [RequireComponent(typeof(UnityEngine.UI.Button))]
    public class HoverBubbleEffect : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField]
        private float period = 1;
        [SerializeField]
        private float amplitude = 1;
        [SerializeField]
        private float returnSpeed = 1;

        private UnityEngine.UI.Button button;
        private float startTime = 0;
        private bool mouseOver = false;

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (!button.interactable)
            {
                return;
            }

            mouseOver = true;
            startTime = Time.time;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            mouseOver = false;
        }

        private void Start()
        {
            button = GetComponent<UnityEngine.UI.Button>();
        }

        private void Update()
        {
            if (mouseOver)
            {
                float wave = MathF.Sin(((Time.time - startTime) / period) + (1.5f * MathF.PI));
                wave *= amplitude;
                wave += amplitude;
                wave *= amplitude * 0.5f;

                float scale = 1 + wave;

                transform.localScale = new Vector3(scale, scale, 1);
            }
            else
            {
                float oldScale = transform.localScale.x;
                float newScale = MathF.Max(oldScale - (Time.deltaTime * returnSpeed), 1);

                transform.localScale = new Vector3(newScale, newScale, 1);
            }
        }

        private void OnDisable()
        {
            transform.localScale = Vector3.one;
            mouseOver = false;
        }
    }
}