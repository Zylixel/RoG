using Client.Game;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Client.UI.Button
{
    [RequireComponent(typeof(UnityEngine.UI.Button))]
    public class HoverTooltipHighlightEffect : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField]
        private Color normalColor = Color.white;
        [SerializeField]
        private Color hoverColor = Color.white;

        private UnityEngine.UI.Button button;

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (!button.interactable)
            {
                return;
            }

            SetColor(hoverColor);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            SetColor(normalColor);
        }

        private void Start()
        {
            button = GetComponent<UnityEngine.UI.Button>();

            button.image.material = new Material(button.image.material);
        }

        private void OnDisable()
        {
            SetColor(normalColor);
        }

        private void SetColor(Color color)
        {
            if (button != null)
            {
                button.image.material.SetColor(ShaderProperties.HighlightColor, color);
            }
        }
    }
}