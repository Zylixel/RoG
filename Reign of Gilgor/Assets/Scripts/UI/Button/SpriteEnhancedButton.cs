﻿using Client.Game;
using Client.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace Client.UI.Button
{
    [RequireComponent(typeof(Image))]
    internal class SpriteEnhancedButton : EnhancedButton
    {
        private static SpriteState? sprites = null;
        private static SpriteState? flashMask = null;

        [SerializeField]
        internal bool Flash;
        [SerializeField]
        internal Color FlashColor;

        private new Image image;

        protected override void Awake()
        {
            transition = Transition.SpriteSwap;
            image = GetComponent<Image>();
            image.material = new Material(Resources.Load<Material>("Materials/UI/OutlineFlashMaterial"));

            ResolveSprites();
            image.sprite = sprites.Value.selectedSprite;

            UpdateMaterial();
            spriteState = sprites.Value;
            base.Awake();
        }

        private static void ResolveSprites()
        {
            if (!sprites.HasValue)
            {
                var buttonSprites = Resources.LoadAll<Sprite>("Sprites/PurpleButton");

                sprites = new SpriteState
                {
                    selectedSprite = UnityUtils.ExpandSprite(buttonSprites[0], 2),
                    highlightedSprite = UnityUtils.ExpandSprite(buttonSprites[1], 2),
                    disabledSprite = UnityUtils.ExpandSprite(buttonSprites[2], 2),
                    pressedSprite = UnityUtils.ExpandSprite(buttonSprites[3], 2),
                };
            }

            if (!flashMask.HasValue)
            {
                var buttonFlashMasks = Resources.LoadAll<Sprite>("Sprites/PurpleButtonFlashMask");

                flashMask = new SpriteState
                {
                    selectedSprite = UnityUtils.ExpandSprite(buttonFlashMasks[0], 2),
                    highlightedSprite = UnityUtils.ExpandSprite(buttonFlashMasks[1], 2),
                    disabledSprite = UnityUtils.ExpandSprite(buttonFlashMasks[2], 2),
                    pressedSprite = UnityUtils.ExpandSprite(buttonFlashMasks[3], 2),
                };
            }
        }

        protected override void DoStateTransition(SelectionState state, bool instant)
        {
            if (image && flashMask.HasValue)
            {
                var mask = state switch
                {
                    SelectionState.Selected => flashMask.Value.selectedSprite,
                    SelectionState.Highlighted => flashMask.Value.highlightedSprite,
                    SelectionState.Pressed => flashMask.Value.pressedSprite,
                    SelectionState.Disabled => flashMask.Value.disabledSprite,
                    _ => flashMask.Value.selectedSprite,
                };

                if (mask != null)
                {
                    image.material.SetTexture(ShaderProperties.Mask, mask.texture);
                }
            }

            base.DoStateTransition(state, instant);
        }

        private void UpdateMaterial()
        {
            if (!image)
            {
                return;
            }

            if (Flash)
            {
                image.material.SetColor(ShaderProperties.OutlineColor, FlashColor);
                image.material.SetFloat(ShaderProperties.FlashRate, 10);
                image.material.SetColor(ShaderProperties.FlashColor, FlashColor);
            }
            else
            {
                image.material.SetColor(ShaderProperties.OutlineColor, Color.black);
                image.material.SetFloat(ShaderProperties.FlashRate, 0);
            }
        }

#if UNITY_EDITOR
        internal new void OnValidate()
        {
            FlashColor.a = 1f;
            UpdateMaterial();

            base.OnValidate();
        }
#endif
    }
}
