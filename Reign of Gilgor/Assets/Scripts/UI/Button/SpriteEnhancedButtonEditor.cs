﻿#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;

namespace Client.UI.Button
{
    [CustomEditor(typeof(SpriteEnhancedButton))]
    internal class SpriteEnhancedButtonEditor : EnhancedButtonEditor
    {
        private SerializedProperty m_OnClickProperty;

        protected override void OnEnable()
        {
            base.OnEnable();
            m_OnClickProperty = serializedObject.FindProperty("m_OnClick");
        }

        public override void OnInspectorGUI()
        {
            var targetButton = (SpriteEnhancedButton)target;

            EditorGUI.BeginChangeCheck();

            targetButton.PlaySoundOnClick = EditorGUILayout.Toggle("Play Sound On Click", targetButton.PlaySoundOnClick);
            targetButton.interactable = EditorGUILayout.Toggle("Interactable", targetButton.interactable);
            targetButton.Flash = EditorGUILayout.Toggle("Flash", targetButton.Flash);
            if (targetButton.Flash)
            {
                targetButton.FlashColor = EditorGUILayout.ColorField(new GUIContent("Flash Color"), targetButton.FlashColor, false, false, false);
            }

            serializedObject.Update();
            EditorGUILayout.PropertyField(m_OnClickProperty);
            serializedObject.ApplyModifiedProperties();

            if (EditorGUI.EndChangeCheck())
            {
                targetButton.OnValidate();
            }
        }
    }
}

#endif