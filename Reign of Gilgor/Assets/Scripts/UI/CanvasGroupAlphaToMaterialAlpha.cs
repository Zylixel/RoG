using Client.Game;
using Microsoft.Extensions.Logging;
using RoGCore.Logging;
using UnityEngine;
using UnityEngine.UI;

namespace Client.UI
{
    [RequireComponent(typeof(Image))]
    public class CanvasGroupAlphaToMaterialAlpha : MonoBehaviour
    {
        private Image image;
        private CanvasGroup canvasGroup;

        private static readonly ILogger<CanvasGroupAlphaToMaterialAlpha> Logger = LogFactory.LoggingInstance<CanvasGroupAlphaToMaterialAlpha>();

        private void Start()
        {
            canvasGroup = GetComponentInParent<CanvasGroup>();

            if (!canvasGroup)
            {
                Logger.LogWarning("No CanvasGroup in parent");
            }

            image = GetComponent<Image>();
        }

        private void Update()
        {
            if (image.material.GetFloat(ShaderProperties.Alpha) != canvasGroup.alpha)
            {
                image.material.SetFloat(ShaderProperties.Alpha, canvasGroup.alpha);
            }
        }
    }
}