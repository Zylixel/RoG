﻿using Client.Entity.Player;
using Client.UI.Dialog;
using RoGCore.Models;
using RoGCore.Networking.Packets;
using UnityEngine;
using UnityEngine.UI;

namespace Client.UI
{
    internal sealed class CharacterPainterUI : BaseDialog<CharacterPainterUI>
    {
        [SerializeField]
        private UnityEngine.UI.Button bigColorBuy;

        [SerializeField]
        private FlexibleColorPicker bigColorPallete;

        [SerializeField]
        private UnityEngine.UI.Button bigColorRemove;

        [SerializeField]
        private UnityEngine.UI.Button closeButton;

        [SerializeField]
        private Image preview;

        [SerializeField]
        private UnityEngine.UI.Button smallColorBuy;

        [SerializeField]
        private FlexibleColorPicker smallColorPallete;

        [SerializeField]
        private UnityEngine.UI.Button smallColorRemove;

        private Player player;

        public override void Initialize()
        {
            base.Initialize();

            bigColorBuy.onClick.AddListener(delegate
            {
                GameWorldManager.CurrentOutgoingNetwork.OutgoingMessage(new Paint(Paint.RequestType.SetBig, bigColorPallete.color));
            });
            smallColorBuy.onClick.AddListener(delegate
            {
                GameWorldManager.CurrentOutgoingNetwork.OutgoingMessage(new Paint(Paint.RequestType.SetSmall, smallColorPallete.color));
            });
            bigColorRemove.onClick.AddListener(delegate
            {
                GameWorldManager.CurrentOutgoingNetwork.OutgoingMessage(new Paint(Paint.RequestType.RemoveBig, RGB.White));
            });
            smallColorRemove.onClick.AddListener(delegate
            {
                GameWorldManager.CurrentOutgoingNetwork.OutgoingMessage(new Paint(Paint.RequestType.RemoveBig, RGB.White));
            });
            closeButton.onClick.AddListener(() => Close());

            preview.material.SetInt("_UseBigColor", 1);
            preview.material.SetInt("_UseSmallColor", 1);
        }

        private void Update()
        {
            preview.sprite = player.ObjectDesc.Textures[4];
            bigColorRemove.interactable = player.BigColor > -1 && player.Silver >= 100;
            bigColorBuy.interactable = player.Silver >= 500;
            preview.material.SetColor("_BigColor", bigColorPallete.color);

            smallColorRemove.interactable = player.SmallColor > -1 && player.Silver >= 100;
            smallColorBuy.interactable = player.Silver >= 500;
            preview.material.SetColor("_SmallColor", smallColorPallete.color);
        }

        internal static void Display(Player player)
        {
            Instance.player = player;
            Instance.smallColorPallete.SetColor(new RGB(player.SmallColor));
            Instance.bigColorPallete.SetColor(new RGB(player.BigColor));
            Instance.Open();
        }
    }
}