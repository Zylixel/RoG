using System;
using System.Collections;
using Client.Game;
using RoGCore.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace Client.UI.Chat
{
    internal sealed class ChatHandler : Initializable<ChatHandler>
    {
        [SerializeField]
        private GameObject gameChat;

        [SerializeField]
        private InputField chatInput;

        public override void Initialize() => Instance = this;

        private void OnEnable()
        {
            InputMaster.SubscribeToKeyPress(OptionsHandler.GetKeyCodeOption("chatKey"), ChatKeyPressed);
            InputMaster.SubscribeToKeyPress(KeyCode.Slash, CommandKeyPressed);
        }

        private void OnDisable()
        {
            InputMaster.UnsubscribeToKeyPress(OptionsHandler.GetKeyCodeOption("chatKey"), ChatKeyPressed);
            InputMaster.UnsubscribeToKeyPress(KeyCode.Slash, CommandKeyPressed);
        }

        private void ChatKeyPressed(object sender, EventArgs e)
        {
            if (InputMaster.InGameplay())
            {
                EnterChat();
            }
            else
            {
                if (!chatInput.text.IsNullOrWhiteSpace())
                {
                    GameWorldManager.CurrentOutgoingNetwork.OutgoingMessage(new RoGCore.Networking.Packets.Text(chatInput.text));
                }

                ExitChat();
            }
        }

        private void CommandKeyPressed(object sender, EventArgs e)
        {
            if (InputMaster.InGameplay())
            {
                EnterChat("/");
            }
        }

        internal static void EnterChat(string startChars = "") => Instance.EnterChatInternal(startChars);

        private void EnterChatInternal(string startChars)
        {
            if (InputMaster.Focus != typeof(Client.Game.Map.Map))
            {
                return;
            }

            InputMaster.Focus = typeof(RecentChatHandler);

            gameChat.SetActive(true);
            gameChat.GetComponentInChildren<Scrollbar>(true).value = 0;

            RecentChatHandler.SetActive(false);

            chatInput.ActivateInputField();
            chatInput.text = startChars;
            chatInput.StartCoroutine(FixText());
        }

        /// <summary>
        ///     This will move the caret to the end of the text after one frame
        /// </summary>
        private IEnumerator FixText()
        {
            yield return null;
            chatInput.MoveTextEnd(false);
        }

        private void ExitChat()
        {
            InputMaster.Focus = typeof(Client.Game.Map.Map);
            gameChat.SetActive(false);
            RecentChatHandler.SetActive(true);
            chatInput.text = string.Empty;
        }
    }
}