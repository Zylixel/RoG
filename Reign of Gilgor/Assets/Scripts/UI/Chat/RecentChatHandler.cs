﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Client.Entity;
using Client.Entity.Player;
using Client.Game.Event;
using Client.UI.Tooltip;
using RoGCore.Networking.Packets;
using RoGCore.Utils;
using TMPro;
using UnityEngine;

namespace Client.UI.Chat
{
    internal sealed class RecentChatHandler : Initializable<RecentChatHandler>
    {
        private static readonly List<TextMeshProUGUI> Children = new List<TextMeshProUGUI>(5);
        private readonly TextData[] data = new TextData[5];

        private Player player;

        public override void Initialize()
        {
            Instance = this;

            for (var i = 0; i < Children.Capacity; i++)
            {
                Children.Add(GetComponentsInChildren<TextMeshProUGUI>(true).First(x => x.gameObject.name == i.ToString()));
                data[i] = new TextData { Time = 0, Owner = -1 };
            }

            InputMaster.SubscribeToControlClick(OnControlClick);

            ReconnectEvent.Begin += () => Clear();
            GameWorldManager.PlayerCreated.Begin += (player) => this.player = player;
        }

        private void Update()
        {
            for (var i = 0; i < Children.Count; i++)
            {
                data[i].Time -= Time.deltaTime * 1000;
                if (data[i].Time <= 0)
                {
                    Children[i].text = string.Empty;
                }
            }
        }

        private void OnControlClick(object sender, EventArgs e)
        {
            if (InputMaster.Focus != typeof(Client.Game.Map.Map))
            {
                return;
            }

            foreach (var result in player.Manager.Map.CurrentRaycast.Where(x =>
                Children.ConvertAll(y => y.gameObject).Contains(x.gameObject)))
            {
                var text = result.gameObject.GetComponent<TextMeshProUGUI>();

                var owner = data[Children.IndexOf(text)].Owner;

                if (player.Manager.Map.Entities.TryGetValue(owner, out var entity)
                    && (entity is ServerPlayer serverPlayer))
                {
                    PlayerTooltip.Display(serverPlayer);
                }
                else
                {
                    break;
                }
            }
        }

        internal static void Clear()
        {
            for (var i = 0; i < Children.Count; i++)
            {
                Children[i].text = string.Empty;
            }
        }

        internal static void SetActive(bool value) => Instance.gameObject.SetActive(value);

        internal void OnNewChat(in Text packet)
        {
            MoveUp();
            var toDisplay = new StringBuilder();
            if (!packet.Name.IsNullOrWhiteSpace())
            {
                toDisplay.Append("<color=#").AppendFormat("{0:X}", packet.NameColor).Append('>');
                toDisplay.Append(packet.Name);
                toDisplay.Append("</color>");
                toDisplay.Append(": ");
            }

            toDisplay.Append("<color=#").AppendFormat("{0:X}", packet.TextColor).Append('>');
            toDisplay.Append(packet.Message);
            toDisplay.Append("</color>");
            toDisplay.Append("\n");
            Children[0].text = toDisplay.ToString();
            data[0].Time = 10000;
            data[0].Owner = packet.ObjectId;
        }

        private void MoveUp()
        {
            for (var i = Children.Count - 1; i > 0; i--)
            {
                Children[i].text = Children[i - 1].text;
                data[i] = data[i - 1];
            }
        }

        private struct TextData
        {
            internal float Time;
            internal long Owner;
        }
    }
}