using System;
using System.Text;
using Baracuda.Monitoring;
using Baracuda.Monitoring.Modules;
using UnityEngine;

namespace Client.UI.Debugging
{
    public class FPSMonitor : MonitorModuleBase
    {
        [Range(0.1f, 2f)]
        [SerializeField]
        private float measurePeriod = 0.25f;

        private const int THRESHOLD_ONE = 30;
        private const int THRESHOLD_TWO = 60;

        private float timer = 0;
        private int frameCount = 0;

        private readonly string colorMinMarkup = "<color=#07fc03>";
        private readonly string colorMidMarkup = "<color=#fcba03>";
        private readonly string colorMaxMarkup = "<color=#07fc03>";

        private readonly StringBuilder fpsBuilder = new StringBuilder();

        [Monitor]
        [MOrder(1000)]
        [MFontSize(16)]
        [MGroupElement(false)]
        [MPosition(UIPosition.UpperLeft)]
        [MValueProcessor(nameof(FPSProcessor))]
        private float fps;

        private string FPSProcessor(float value)
        {
            fpsBuilder.Clear();
            fpsBuilder.Append('[');
            fpsBuilder.Append(value >= THRESHOLD_TWO ? colorMaxMarkup :
                value >= THRESHOLD_ONE ? colorMidMarkup : colorMinMarkup);
            fpsBuilder.Append(value.ToString("00.00"));
            fpsBuilder.Append("</color>]");
            return fpsBuilder.ToString();
        }

        private void Update()
        {
            frameCount++;
            timer += Time.deltaTime / Time.timeScale;

            if (timer < measurePeriod)
            {
                return;
            }

            fps = (frameCount / timer);
            frameCount = 0;

            var rest = measurePeriod - timer;
            timer = rest;
        }
    }
}