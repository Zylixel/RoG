using Baracuda.Monitoring;
using Baracuda.Monitoring.Modules;

namespace Client.UI.Debugging
{
    public class ProjectileMonitor : MonitorModuleBase
    {
        [Monitor]
        [MOrder(1000)]
        [MFontSize(12)]
        [MGroupElement(false)]
        [MPosition(UIPosition.UpperLeft)]
        private int projectiles;

        private void Update()
        {
            projectiles = GameWorldManager.Instance.Map.Projectiles.Count;
        }
    }
}