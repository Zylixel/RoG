using Baracuda.Monitoring;
using Baracuda.Monitoring.Modules;
using Client.Networking;

namespace Client.UI.Debugging
{
    public class TPSMonitor : MonitorModuleBase
    {
        [Monitor]
        [MOrder(1000)]
        [MFontSize(12)]
        [MGroupElement(false)]
        [MPosition(UIPosition.UpperLeft)]
        private string tps;

        private void Update()
        {
            tps = string.Format("{0:0.} | {1:0.0} ms", NewTickHandler.TPS, NewTickHandler.MPST);
        }
    }
}