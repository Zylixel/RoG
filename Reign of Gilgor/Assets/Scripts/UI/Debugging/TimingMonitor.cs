using Baracuda.Monitoring;
using Baracuda.Monitoring.Modules;

namespace Client.UI.Debugging
{
    internal sealed class TimingMonitor : MonitorModuleBase
    {
        internal static double UpdateHandler = 0;
        internal static double NewGroundDataHandler = 0;
        internal static double NewObjects = 0;
        internal static double NewStatics = 0;
        internal static double Minimap = 0;
        internal static double NewTickHandler = 0;

        [Monitor]
        [MOrder(1000)]
        [MFontSize(12)]
        [MGroupElement(false)]
        [MPosition(UIPosition.UpperLeft)]
        private string updateHandler;

        [Monitor]
        [MOrder(1000)]
        [MFontSize(8)]
        [MGroupElement(false)]
        [MPosition(UIPosition.UpperLeft)]
        private string newGroundDataHandler;

        [Monitor]
        [MOrder(1000)]
        [MFontSize(8)]
        [MGroupElement(false)]
        [MPosition(UIPosition.UpperLeft)]
        private string newObjects;

        [Monitor]
        [MOrder(1000)]
        [MFontSize(8)]
        [MGroupElement(false)]
        [MPosition(UIPosition.UpperLeft)]
        private string newStatics;

        [Monitor]
        [MOrder(1000)]
        [MFontSize(8)]
        [MGroupElement(false)]
        [MPosition(UIPosition.UpperLeft)]
        private string minimap;

        [Monitor]
        [MOrder(1000)]
        [MFontSize(12)]
        [MGroupElement(false)]
        [MPosition(UIPosition.UpperLeft)]
        private string newTickHandler;

        private void Update()
        {
            updateHandler = string.Format("{0:0.0} ms", UpdateHandler);
            newGroundDataHandler = string.Format("{0:0.0} ms", NewGroundDataHandler);
            newObjects = string.Format("{0:0.0} ms", NewObjects);
            newStatics = string.Format("{0:0.0} ms", NewStatics);
            minimap = string.Format("{0:0.0} ms", Minimap);
            newTickHandler = string.Format("{0:0.0} ms", NewTickHandler);
        }
    }
}