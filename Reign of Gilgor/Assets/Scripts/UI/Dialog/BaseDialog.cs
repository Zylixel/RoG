﻿using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Client.UI.Dialog
{
    [RequireComponent(typeof(CanvasGroup))]
    internal abstract class BaseDialog<T> : Initializable<T>
        where T : class
    {
        private static readonly Vector2 BeginScale = new Vector2(0.6f, 0.6f);

        private CanvasGroup canvasGroup;
        protected bool open;

        private bool processingData;

        public bool ProcessingData
        {
            get => processingData;
            protected set
            {
                processingData = value;
                foreach (var button in GetComponentsInChildren<UnityEngine.UI.Button>())
                {
                    button.interactable = !value;
                }

                foreach (var toggle in GetComponentsInChildren<Toggle>())
                {
                    toggle.interactable = !value;
                }
            }
        }

        private void Awake() => canvasGroup = GetComponent<CanvasGroup>();

        public override void Initialize()
        {
            base.Initialize();

            gameObject.SetActive(false);
        }

        protected virtual Transform GetScaledObject() => gameObject.transform;

        internal virtual void Open()
        {
            if (open || ProcessingData)
            {
                return;
            }

            ProcessingData = true;
            open = true;

            InputMaster.Focus = typeof(T);

            InputMaster.SubscribeToKeyPress(KeyCode.Escape, CloseEvent);

            gameObject.SetActive(true);

            var transform = GetScaledObject();
            transform.localScale = BeginScale;

            transform.DOScale(Vector3.one, 0.25f).SetEase(Ease.OutBack);
            canvasGroup.DOFade(1, 0.1f);

            ProcessingData = false;
        }

        private void CloseEvent(object sender, EventArgs e) => Close();

        internal virtual void Close()
        {
            if (!open || ProcessingData)
            {
                return;
            }

            ProcessingData = true;
            open = false;

            InputMaster.UnsubscribeToKeyPress(KeyCode.Escape, CloseEvent);

            InputMaster.Focus = typeof(Game.Map.Map);

            var transform = GetScaledObject();
            var sequence = DOTween.Sequence()
                .Append(transform.DOScale(Vector3.zero, 0.25f))
                .Join(canvasGroup.DOFade(0, 0.125f));

            sequence.onComplete += () =>
            {
                ProcessingData = false;
                gameObject.SetActive(false);
            };
        }

        protected void Toggle()
        {
            if (open)
            {
                Close();
            }
            else if (InputMaster.InGameplay())
            {
                Open();
            }
        }
    }
}