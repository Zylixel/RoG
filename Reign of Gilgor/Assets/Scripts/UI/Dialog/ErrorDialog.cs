﻿using System;
using Client.Game;
using TMPro;
using UnityEngine;

namespace Client.UI.Dialog
{
    internal sealed class ErrorDialog : BaseDialog<ErrorDialog>
    {
        public override int InitializeOrder() => 0;

        public UnityEngine.UI.Button CloseButton;
        public TextMeshProUGUI MainText;
        public TextMeshProUGUI HeaderText;

        private Action callback;

        public override void Initialize()
        {
            base.Initialize();

            CloseButton.onClick.AddListener(() => TriggerAndCloseEvent(null, null));
        }

        private void OnEnable()
        {
            InputMaster.SubscribeToKeyPress(KeyCode.Return, TriggerAndCloseEvent);
            InputMaster.SubscribeToKeyPress(OptionsHandler.GetKeyCodeOption("interactKey"), TriggerAndCloseEvent);
        }

        private void OnDisable()
        {
            InputMaster.UnsubscribeToKeyPress(KeyCode.Return, TriggerAndCloseEvent);
            InputMaster.UnsubscribeToKeyPress(OptionsHandler.GetKeyCodeOption("interactKey"), TriggerAndCloseEvent);
        }

        private void TriggerAndCloseEvent(object sender, EventArgs e)
        {
            if (ProcessingData)
            {
                return;
            }

            callback?.Invoke();

            Close();
        }

        internal static void Display(string text) => Instance.InternalDisplay(text, null, null);

        internal static void Display(string text, string title) => Instance.InternalDisplay(text, title, null);

        internal static void Display(string text, Action callback) => Instance.InternalDisplay(text, null, callback);

        private void InternalDisplay(string text, string title, Action callback)
        {
            if (ProcessingData)
            {
                return;
            }

            Open();
            MainText.text = text;
            HeaderText.text = title ?? "Error";
            this.callback = callback;
        }
    }
}