﻿using System;
using System.Collections;
using Client.Encryption;
using Client.Networking.API;
using Client.Utils;
using RoGCore.Networking.APIViewModels;
using RoGCore.Utils;
using UnityEngine;
using UnityEngine.UI;
using static RoGCore.Utils.ResultExtenstions;

namespace Client.UI.Dialog
{
    internal sealed class LoginDialog : BaseDialog<LoginDialog>
    {
        [SerializeField]
        private UnityEngine.UI.Button closeButton;

        [SerializeField]
        private Text errorText;

        [SerializeField]
        private UnityEngine.UI.Button loginButton;

        [SerializeField]
        private InputField passwordField;

        [SerializeField]
        private UnityEngine.UI.Button registerButton;

        [SerializeField]
        private Toggle rememberToggle;

        [SerializeField]
        private InputField usernameField;

        public override void Initialize()
        {
            base.Initialize();

            registerButton.onClick.AddListener(() =>
            {
                if (ProcessingData)
                {
                    return;
                }

                Close();
                RegisterDialog.Display();
            });

            loginButton.onClick.AddListener(() => LoginEvent(null, null));
            closeButton.onClick.AddListener(() => Close());
        }

        private void OnEnable()
        {
            InputMaster.SubscribeToKeyPress(KeyCode.Return, LoginEvent);

            errorText.text = string.Empty;
            usernameField.text = string.Empty;
            passwordField.text = string.Empty;
        }

        private void OnDisable() => InputMaster.UnsubscribeToKeyPress(KeyCode.Return, LoginEvent);

        private void LoginEvent(object sender, EventArgs e)
        {
            if (ProcessingData)
            {
                return;
            }

            if (usernameField.text.IsNullOrWhiteSpace())
            {
                errorText.text = "Enter your username.";
                return;
            }

            if (passwordField.text.IsNullOrWhiteSpace())
            {
                errorText.text = "Enter your password.";
                return;
            }

            StartCoroutine(Login());
        }

        private IEnumerator Login()
        {
            ProcessingData = true;

            CoroutineResultWrapper<AccountViewModel> apiResponse = new CoroutineResultWrapper<AccountViewModel>();
            yield return AccountStore.Login(
                    usernameField.text,
                    Hasher.GeneratePasswordHash(usernameField.text, passwordField.text),
                    rememberToggle.isOn,
                    apiResponse);

            ProcessingData = false;

            if (apiResponse.Result.IsFailed)
            {
                errorText.text = apiResponse.Result.ErrorString();
                yield break;
            }

            Close();
        }

        internal static void Display() => Instance.Open();
    }
}