﻿using System;
using System.Collections.Generic;
using Client.Game;
using RoGCore.Networking.Packets;
using RoGCore.Utils;
using TMPro;
using UnityEngine;
using Text = UnityEngine.UI.Text;

namespace Client.UI.Dialog
{
    internal sealed class QuickDialog : BaseDialog<QuickDialog>
    {
        public override int InitializeOrder() => 0;

        public UnityEngine.UI.Button Button1;
        public TextMeshProUGUI Button1Text;
        public UnityEngine.UI.Button CloseButton;
        public Text HeaderText;
        public TextMeshProUGUI MainText;

        private Dictionary<string, string> resultConversions;
        private Action callback;
        private bool hasResult;

        public override void Initialize()
        {
            base.Initialize();

            CloseButton.onClick.AddListener(() => Close());

            Button1.onClick.AddListener(() => TriggerAndCloseEvent(null, null));
        }

        private void OnEnable()
        {
            InputMaster.SubscribeToKeyPress(KeyCode.Return, TriggerAndCloseEvent);
            InputMaster.SubscribeToKeyPress(OptionsHandler.GetKeyCodeOption("interactKey"), TriggerAndCloseEvent);
        }

        private void OnDisable()
        {
            InputMaster.UnsubscribeToKeyPress(KeyCode.Return, TriggerAndCloseEvent);
            InputMaster.UnsubscribeToKeyPress(OptionsHandler.GetKeyCodeOption("interactKey"), TriggerAndCloseEvent);
        }

        private void TriggerAndCloseEvent(object sender, EventArgs e)
        {
            if (ProcessingData)
            {
                return;
            }

            callback?.Invoke();

            if (hasResult)
            {
                ProcessingData = true;
            }
            else
            {
                Close();
            }
        }

        internal static void Display(string Header, string Dialog, string ButtonText, Action callback, bool hasResult) =>
            Instance.InternalDisplay(Header, Dialog, ButtonText, callback, null, hasResult);

        internal static void Display(string Header, string Dialog, string ButtonText, Action callback,
            Dictionary<string, string> results, bool hasResult) => Instance.InternalDisplay(Header, Dialog, ButtonText, callback, results, hasResult);

        private void InternalDisplay(string Header, string Dialog, string ButtonText, Action callback,
            Dictionary<string, string> results, bool hasResult)
        {
            if (ProcessingData)
            {
                return;
            }

            Open();
            HeaderText.text = Header;
            MainText.text = Dialog;
            Button1Text.text = ButtonText;
            resultConversions = results;
            this.callback = callback;
            this.hasResult = hasResult;
        }

        internal static void OnResult(in Result packet)
        {
            if (!Instance.ProcessingData)
            {
                return;
            }

            Instance.ProcessingData = false;

            var message = packet.Message;

            // Typically means Result is successful,
            // But in case of a failure with no message,
            // the dialog will close aswell
            if (message.IsNullOrWhiteSpace())
            {
                Instance.Close();
                return;
            }

            Instance.resultConversions?.TryGetValue(packet.Message, out message);

            Instance.MainText.text = message;
            Instance.Button1Text.text = "Close";
        }
    }

    internal static class DialogResultConversions
    {
        internal static Dictionary<string, string> OldBlacksmithConversion = new Dictionary<string, string>()
        {
            { "Insufficient Funds", "Come back when you have 10 <sprite=6> for me!" },
            { "No Polishable Items", "You have no ancient gear for me to polish. Find some and come back." },
            { string.Empty, "I've polished your gear for you. Come again soon with more!" },
        };
    }
}