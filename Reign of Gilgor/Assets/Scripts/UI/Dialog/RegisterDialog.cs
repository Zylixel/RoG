﻿using System;
using System.Collections;
using Client.Encryption;
using Client.Networking.API;
using Client.Utils;
using Microsoft.Extensions.Logging;
using RoGCore.Networking.APIViewModels;
using RoGCore.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace Client.UI.Dialog
{
    internal sealed class RegisterDialog : BaseDialog<RegisterDialog>
    {
        [SerializeField]
        private UnityEngine.UI.Button closeButton;

        [SerializeField]
        private UnityEngine.UI.Button confirmButton;

        [SerializeField]
        private Text errorText;

        [SerializeField]
        private InputField password2Field;

        [SerializeField]
        private InputField passwordField;

        [SerializeField]
        private Toggle rememberToggle;

        [SerializeField]
        private InputField usernameField;

        public override void Initialize()
        {
            base.Initialize();

            closeButton.onClick.AddListener(() => Close());
            confirmButton.onClick.AddListener(() => RegisterEvent(null, null));
        }

        private void OnEnable()
        {
            InputMaster.SubscribeToKeyPress(KeyCode.Return, RegisterEvent);

            usernameField.text = string.Empty;
            passwordField.text = string.Empty;
            password2Field.text = string.Empty;
            errorText.text = string.Empty;
        }

        private void OnDisable() => InputMaster.UnsubscribeToKeyPress(KeyCode.Return, RegisterEvent);

        private void RegisterEvent(object sender, EventArgs e)
        {
            if (ProcessingData)
            {
                return;
            }

            if (usernameField.text.IsNullOrWhiteSpace())
            {
                errorText.text = "Choose a username.";
                return;
            }

            if (passwordField.text.IsNullOrWhiteSpace())
            {
                errorText.text = "Choose a password.";
                return;
            }

            if (password2Field.text != passwordField.text)
            {
                errorText.text = "Passwords do not match.";
                return;
            }

            StartCoroutine(Register());
        }

        private IEnumerator Register()
        {
            ProcessingData = true;

            CoroutineResultWrapper<AccountViewModel> registerResult = new CoroutineResultWrapper<AccountViewModel>();
            yield return AccountStore.Register(
                usernameField.text,
                Hasher.GeneratePasswordHash(passwordField.text, usernameField.text),
                rememberToggle.isOn,
                registerResult);

            ProcessingData = false;

            if (registerResult.Result.IsFailed)
            {
                errorText.text = "An unknown error has occurred.";
                Logger.LogError(registerResult.Result.ErrorString());
                yield break;
            }

            Close();
        }

        internal static void Display() => Instance.Open();
    }
}