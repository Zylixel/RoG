﻿#if UNITY_EDITOR || DEVELOPMENT_BUILD
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Ionic.Zlib;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RoGCore.Assets;
using RoGCore.Logging;
using RoGCore.Models;
using RoGCore.Utils;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Tilemaps;
using UnityEngine.UI;
using Random = UnityEngine.Random;
using TileData = UnityEngine.Tilemaps.TileData;
using Vector2Int = UnityEngine.Vector2Int;

namespace Client.UI
{
    internal sealed class Editor : MonoBehaviour
    {
        private static readonly ILogger<Editor> Logger = LogFactory.LoggingInstance<Editor>();

        private DrawMode currentDrawMode;
        private List currentType;

        private EmbeddedData data;
        public UnityEngine.UI.Button DrawButton;

        private Tilemap groundTileMap;
        private Dictionary<Vector3Int, ObjectDescription> mapObjects;
        private Dictionary<Vector3Int, TileRegion> mapRegions;
        private Dictionary<Vector3Int, GroundDescription> mapTiles;
        public UnityEngine.UI.Button ModeButton;
        private List<GameObject> objects;
        private Tilemap objectTileMap;
        private Dictionary<TileRegion, Color> regionColors;
        private List<GameObject> regions;
        private Tilemap regionTileMap;
        public InputField SearchField;

        private string selected;

        private List<GameObject> tiles;
        public GameObject Tooltip;

        // Start is called before the first frame update
        private void Start()
        {
            SearchField.onValueChanged.AddListener(delegate { OnSearchChanged(); });
            ModeButton.onClick.AddListener(OnModeButtonClicked);
            DrawButton.onClick.AddListener(OnDrawButtonClicked);
            groundTileMap = GameObject.Find("GroundTilemap").GetComponent<Tilemap>();
            objectTileMap = GameObject.Find("ObjectTilemap").GetComponent<Tilemap>();
            regionTileMap = GameObject.Find("RegionTilemap").GetComponent<Tilemap>();
            data = new EmbeddedData();
            tiles = new List<GameObject>();
            objects = new List<GameObject>();
            regions = new List<GameObject>();
            mapObjects = new Dictionary<Vector3Int, ObjectDescription>();
            mapTiles = new Dictionary<Vector3Int, GroundDescription>();
            mapRegions = new Dictionary<Vector3Int, TileRegion>();
            regionColors = new Dictionary<TileRegion, Color>();
            Tooltip.SetActive(false);

            var c = 0;
            var tileContent = GameObject.Find("TileContent");
            foreach (var tile in data.Tiles.Values.Where(x => x.Name != "Nothing"))
            {
                var go = new GameObject(tile.ObjectType.ToString());
                go.transform.SetParent(tileContent.transform);
                go.transform.localPosition = new Vector3(c % 3 == 0 ? -40 : c % 3 == 1 ? 0 : 40, 164 - (c / 3 * 40));
                c++;
                go.transform.localScale = Vector3.one;
                var image = go.AddComponent<Image>();
                image.rectTransform.sizeDelta = new Vector2(34, 34);
                tiles.Add(go);
                var child = new GameObject("Image");
                child.transform.SetParent(go.transform);
                child.transform.localPosition = Vector3.zero;
                child.transform.localScale = Vector3.one;
                image = child.AddComponent<Image>();
                image.sprite = tile.Textures[0];
                image.rectTransform.sizeDelta = new Vector2(32, 32);
                image.raycastTarget = false;
            }

            groundTileMap.SetTile(Vector3Int.zero, null);
            c = 0;
            var objectContent = GameObject.Find("ObjectContent");
            foreach (var obj in data.BetterObjects.Values)
            {
                var go = new GameObject(obj.Name);
                go.transform.SetParent(objectContent.transform);
                go.transform.localPosition = new Vector3(c % 3 == 0 ? -40 : c % 3 == 1 ? 0 : 40, 164 - (c / 3 * 40));
                c++;
                go.transform.localScale = Vector3.one;
                var image = go.AddComponent<Image>();
                image.rectTransform.sizeDelta = new Vector2(34, 34);
                objects.Add(go);
                var child = new GameObject("Image");
                child.transform.SetParent(go.transform);
                child.transform.localPosition = Vector3.zero;
                child.transform.localScale = Vector3.one;
                image = child.AddComponent<Image>();

                if (obj.Textures.Length > 0)
                {
                    image.sprite = obj.Textures[0];
                }

                image.rectTransform.sizeDelta = new Vector2(32, 32);
                image.raycastTarget = false;
            }

            c = 0;
            var regionContent = GameObject.Find("RegionContent");
            foreach (TileRegion region in Enum.GetValues(typeof(TileRegion)))
            {
                if (region == TileRegion.None)
                {
                    continue;
                }

                var go = new GameObject(region.ToString());
                go.transform.SetParent(regionContent.transform);
                go.transform.localPosition = new Vector3(c % 3 == 0 ? -40 : c % 3 == 1 ? 0 : 40, 164 - (c / 3 * 40));
                c++;
                go.transform.localScale = Vector3.one;
                var image = go.AddComponent<Image>();
                image.rectTransform.sizeDelta = new Vector2(34, 34);
                regions.Add(go);
                var child = new GameObject("Image");
                child.transform.SetParent(go.transform);
                child.transform.localPosition = Vector3.zero;
                child.transform.localScale = Vector3.one;
                image = child.AddComponent<Image>();
                image.color = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
                image.rectTransform.sizeDelta = new Vector2(32, 32);
                image.raycastTarget = false;
                regionColors.Add(region, image.color);
            }

            currentType = List.Ground;
            currentDrawMode = DrawMode.Draw;
            SetList();
        }

        // Update is called once per frame
        private void Update()
        {
            var pointerEventData = new PointerEventData(EventSystem.current)
            {
                position = UnityEngine.Input.mousePosition,
            };
            var raycastResults = new List<RaycastResult>();
            EventSystem.current.RaycastAll(pointerEventData, raycastResults);
            if (UnityEngine.Input.GetMouseButton(0))
            {
                if (raycastResults.Count == 0)
                {
                    var pos = UnityEngine.Camera.main.ScreenToWorldPoint(UnityEngine.Input.mousePosition);
                    var coordinate = groundTileMap.WorldToCell(pos);
                    if (currentDrawMode == DrawMode.Draw && selected is not null)
                    {
                        switch (currentType)
                        {
                            case List.Ground:
                                SetTile(coordinate, data.Tiles[selected.UShortParseFast()]);
                                break;
                            case List.Object:
                                SetObject(coordinate, data.BetterObjects[selected]);
                                break;
                            case List.Region:
                                SetRegion(coordinate, (TileRegion)Enum.Parse(typeof(TileRegion), selected));
                                break;
                        }
                    }
                    else if (currentDrawMode == DrawMode.Erase)
                    {
                        switch (currentType)
                        {
                            case List.Ground:
                                SetTile(coordinate, null);
                                break;
                            case List.Object:
                                SetObject(coordinate, null);
                                break;
                            case List.Region:
                                SetRegion(coordinate, TileRegion.None);
                                break;
                        }
                    }
                    else if (currentDrawMode == DrawMode.Select)
                    {
                        switch (currentType)
                        {
                            case List.Ground:
                                selected = groundTileMap.GetTile(coordinate) is not null
                                    ? groundTileMap.GetTile(coordinate).name
                                    : selected;
                                break;
                            case List.Object:
                                selected = objectTileMap.GetTile(coordinate) is not null
                                    ? objectTileMap.GetTile(coordinate).name
                                    : selected;
                                break;
                            case List.Region:
                                selected = regionTileMap.GetTile(coordinate) is not null
                                    ? regionTileMap.GetTile(coordinate).name
                                    : selected;
                                break;
                        }

                        foreach (var go in GetCurrentList())
                        {
                            go.GetComponent<Image>().color = go.name == selected ? Color.green : Color.white;
                        }

                        currentDrawMode = DrawMode.Draw;
                        DrawButton.GetComponentInChildren<Text>().text = currentDrawMode.ToString();
                    }
                }
            }

            if (UnityEngine.Input.GetMouseButtonDown(0))
            {
                for (var i = 0; i < raycastResults.Count; i++)
                {
                    if (!GetCurrentList().Contains(raycastResults[i].gameObject))
                    {
                        continue;
                    }

                    var image = raycastResults[i].gameObject.GetComponent<Image>();
                    foreach (var go in GetCurrentList())
                    {
                        go.GetComponent<Image>().color = Color.white;
                    }

                    image.color = Color.green;
                    selected = raycastResults[i].gameObject.name;
                }
            }

            for (var i = 0; i < raycastResults.Count; i++)
            {
                if (!GetCurrentList().Contains(raycastResults[i].gameObject))
                {
                    Tooltip.SetActive(false);
                    continue;
                }

                Tooltip.transform.position = UnityEngine.Input.mousePosition;
                Tooltip.GetComponentInChildren<Text>().text = currentType == List.Ground
                    ? data.Tiles[raycastResults[i].gameObject.name.UShortParseFast()].Name
                    : raycastResults[i].gameObject.name;
                Tooltip.SetActive(true);
                break;
            }

            if (raycastResults.Count == 0)
            {
                gameObject.transform.Translate(new Vector2(
                    UnityEngine.Input.GetAxis("Horizontal") * 0.5f,
                    UnityEngine.Input.GetAxis("Vertical") * 0.5f));
                if (UnityEngine.Input.GetAxis("Mouse ScrollWheel") != 0)
                {
                    gameObject.transform.GetComponent<UnityEngine.Camera>().orthographicSize = Mathf.Clamp(
                        gameObject.transform.GetComponent<UnityEngine.Camera>().orthographicSize -
                        (Time.deltaTime * UnityEngine.Input.GetAxis("Mouse ScrollWheel") * 2000), 5, 250);
                }
            }
        }

        public void OnSearchChanged()
        {
            var c = 0;
            foreach (var go in GetCurrentList())
            {
                var name = string.Empty;
                switch (currentType)
                {
                    case List.Ground:
                        name = data.Tiles[go.name.UShortParseFast()].Name;
                        break;
                    case List.Object:
                        name = go.name;
                        break;
                }

                go.SetActive(name.IndexOf(SearchField.text, StringComparison.OrdinalIgnoreCase) >= 0 || SearchField.text.IsNullOrWhiteSpace());
                if (go.activeSelf)
                {
                    go.transform.localPosition = new Vector3(c % 3 == 0 ? -40 : c % 3 == 1 ? 0 : 40, 164 - (c / 3 * 40));
                    c++;
                }
            }
        }

        private void SetList()
        {
            foreach (var go in tiles.Concat(objects).Concat(regions))
            {
                go.SetActive(false);
            }

            foreach (var go in GetCurrentList())
            {
                go.SetActive(true);
            }

            regionTileMap.gameObject.SetActive(currentType == List.Region);
            OnSearchChanged();
        }

        private List<GameObject> GetCurrentList() => currentType switch
        {
            List.Ground => tiles,
            List.Object => objects,
            List.Region => regions,
            _ => tiles,
        };

        private void OnModeButtonClicked()
        {
            switch (currentType)
            {
                case List.Ground:
                    currentType = List.Object;
                    break;
                case List.Object:
                    currentType = List.Region;
                    break;
                case List.Region:
                    currentType = List.Ground;
                    break;
            }

            ModeButton.GetComponentInChildren<Text>().text = currentType.ToString();
            SetList();
        }

        private void OnDrawButtonClicked()
        {
            switch (currentDrawMode)
            {
                case DrawMode.Draw:
                    currentDrawMode = DrawMode.Erase;
                    break;
                case DrawMode.Erase:
                    currentDrawMode = DrawMode.Select;
                    break;
                case DrawMode.Select:
                    currentDrawMode = DrawMode.Draw;
                    break;
            }

            DrawButton.GetComponentInChildren<Text>().text = currentDrawMode.ToString();
        }

        private void SetTile(Vector3Int pos, GroundDescription desc)
        {
            if (mapTiles.ContainsKey(pos))
            {
                if (desc == null)
                {
                    mapTiles.Remove(pos);
                }
                else
                {
                    mapTiles[pos] = desc;
                }
            }
            else if (desc is not null)
            {
                mapTiles.Add(pos, desc);
            }

            if (desc?.Textures[0] == null)
            {
                groundTileMap.SetTile(pos, null);
            }
            else
            {
                var tile = ScriptableObject.CreateInstance<Tile>();
                tile.sprite = desc.Textures[0];
                tile.name = desc.ObjectType.ToString();
                groundTileMap.SetTile(pos, tile);
            }
        }

        private void SetObject(Vector3Int pos, ObjectDescription desc)
        {
            if (mapObjects.ContainsKey(pos))
            {
                if (desc == null)
                {
                    mapObjects.Remove(pos);
                }
                else
                {
                    mapObjects[pos] = desc;
                }
            }
            else if (desc is not null)
            {
                mapObjects.Add(pos, desc);
            }

            if (desc is not null)
            {
                var tile = (CustomTileBase)ScriptableObject.CreateInstance(typeof(CustomTileBase));

                if (desc.Textures.Length > 0)
                {
                    tile.Sprite = desc.Textures[0];
                }

                if (desc.Texture is not null)
                {
                    tile.PixelsPerUnit = desc.Texture.File == "Walls" ? 8f : 32f;
                }

                tile.name = desc.Name;
                objectTileMap.SetTile(pos, tile);
            }
            else
            {
                objectTileMap.SetTile(pos, null);
            }
        }

        private void SetRegion(Vector3Int pos, TileRegion region)
        {
            if (mapRegions.ContainsKey(pos))
            {
                if (region == TileRegion.None)
                {
                    mapRegions.Remove(pos);
                }
                else
                {
                    mapRegions[pos] = region;
                }
            }
            else if (region != TileRegion.None)
            {
                mapRegions.Add(pos, region);
            }

            if (region != TileRegion.None)
            {
                var tile = (CustomTileBase)ScriptableObject.CreateInstance(typeof(CustomTileBase));
                tile.Color = new Color(regionColors[region].r, regionColors[region].g, regionColors[region].b, 0.5f);
                tile.name = region.ToString();
                regionTileMap.SetTile(pos, null);
                regionTileMap.SetTile(pos, tile);
            }
            else
            {
                regionTileMap.SetTile(pos, null);
            }
        }

        public void LoadJson()
        {
            objectTileMap.ClearAllTiles();
            mapObjects.Clear();
            groundTileMap.ClearAllTiles();
            mapTiles.Clear();
            regionTileMap.ClearAllTiles();
            mapRegions.Clear();
            var path = Application.dataPath + "/Resources/Maps/Current.json";
            var obj = JsonConvert.DeserializeObject<JsonDat>(File.ReadAllText(path));
            var dat = ZlibStream.UncompressBuffer(obj.Data);
            var tileDict = new Dictionary<short, TerrainTile>();
            for (var i = 0; i < obj.Dict.Length; i++)
            {
                var o = obj.Dict[i];
                o.Ground ??= "Nothing";
                if (!data.Id2TypeTile.TryGetValue(o.Ground, out var tileId))
                {
                    Logger.LogError(
                        $"ERROR loading ground: '{o.Ground}' whilst converting a json map. Converting to Nothing");
                    o.Ground = "Nothing";
                    data.Id2TypeTile.TryGetValue(o.Ground, out tileId);
                }

                tileDict[(short)i] = new TerrainTile
                {
                    TileId = tileId,
                    TileObj = o.Obj == null ? string.Empty : o.Obj.Id ?? string.Empty,
                    Name = o.Obj == null ? string.Empty : o.Obj.Name ?? string.Empty,
                    Region = o.Region == null
                        ? TileRegion.None
                        : (TileRegion)Enum.Parse(typeof(TileRegion), o.Region.Replace(' ', '_')),
                };
            }

            var tiles = new TerrainTile[obj.Width, obj.Height];
            using var rdr = new NReader(new MemoryStream(dat));
            for (var y = 0; y < obj.Height; y++)
            {
                for (var x = 0; x < obj.Width; x++)
                {
                    tiles[x, y] = tileDict[rdr.ReadInt16()];
                    if (data.Tiles[tiles[x, y].TileId].Name != "Nothing")
                    {
                        SetTile(new Vector3Int(x, y, 0), data.Tiles[tiles[x, y].TileId]);
                    }

                    if (tiles[x, y].TileObj is not null && data.BetterObjects.ContainsKey(tiles[x, y].TileObj))
                    {
                        SetObject(new Vector3Int(x, y, 0), data.BetterObjects[tiles[x, y].TileObj]);
                    }

                    if (tiles[x, y].Region != TileRegion.None)
                    {
                        SetRegion(new Vector3Int(x, y, 0), tiles[x, y].Region);
                    }
                }
            }
        }

        private RectInt GetMapRect()
        {
            var lowestX = int.MaxValue;
            var lowestY = int.MaxValue;
            var highestX = int.MinValue;
            var highestY = int.MinValue;
            foreach (var pos in mapObjects.Keys.Concat(mapTiles.Keys))
            {
                if (pos.x < lowestX)
                {
                    lowestX = pos.x;
                }

                if (pos.x > highestX)
                {
                    highestX = pos.x;
                }

                if (pos.y < lowestY)
                {
                    lowestY = pos.y;
                }

                if (pos.y > highestY)
                {
                    highestY = pos.y;
                }
            }

            return new RectInt(
                new Vector2Int(lowestX, lowestY),
                new Vector2Int(highestX - lowestX + 1, highestY - lowestY + 1));
        }

        public void Save()
        {
            JsonDat json;
            var mapRect = GetMapRect();
            json.Width = mapRect.width;
            json.Height = mapRect.height;
            var dict = new List<Loc>();
            var s = new MemoryStream();
            var wtr = new NWriter(s);
            for (var y = mapRect.yMin; y < mapRect.yMax; y++)
            {
                for (var x = mapRect.xMin; x < mapRect.xMax; x++)
                {
                    var currentCheck = new Loc();
                    var currentCoord = new Vector3Int(x, y, 0);
                    if (mapTiles.TryGetValue(currentCoord, out var groundDescription))
                    {
                        currentCheck.Ground = groundDescription.Name;
                    }

                    if (mapObjects.TryGetValue(currentCoord, out var objectDescription))
                    {
                        currentCheck.Obj = new MapObject { Id = objectDescription.Name };
                    }

                    if (mapRegions.TryGetValue(currentCoord, out var region))
                    {
                        currentCheck.Region = region.ToString();
                    }

                    if (!dict.Contains(currentCheck))
                    {
                        dict.Add(currentCheck);
                    }

                    wtr.Write((short)dict.IndexOf(currentCheck));
                }
            }

            json.Dict = dict.ToArray();
            json.Data = ZlibStream.CompressBuffer(s.ToArray());
            var path = Application.dataPath + "/Resources/Maps/Current.json";
            if (File.Exists(path))
            {
                var time = File.GetCreationTime(path).ToFileTime();
                File.Move(path, Application.dataPath + $"/Resources/Maps/Backups/{time}.json");
            }

            File.WriteAllText(
                path,
                JsonConvert.SerializeObject(
                    json,
                    new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }));
        }

        private enum List
        {
            Ground,
            Object,
            Region,
        }

        private enum DrawMode
        {
            Draw,
            Erase,
            Select,
        }

        private struct Loc : IEquatable<Loc>
        {
            public string Ground;
            public MapObject Obj;
            public string Region;

            public static bool operator ==(Loc a, Loc b) => a.Equals(b);

            public static bool operator !=(Loc a, Loc b) => !a.Equals(b);

            public override bool Equals(object obj) => obj.GetType() == GetType() && Equals((Loc)obj);

            public bool Equals(Loc other) => Ground == other.Ground &&
                       Obj == other.Obj &&
                       Region == other.Region;

            public override int GetHashCode() => (Obj.GetHashCode() / 3) + (Region.GetHashCode() / 3) + (Ground.GetHashCode() / 3);
        }

        private struct JsonDat
        {
            public byte[] Data;
            public int Width;
            public int Height;
            public Loc[] Dict;
        }

        private struct TerrainTile : IEquatable<TerrainTile>
        {
            public string Name { get; set; }

            public TileRegion Region { get; set; }

            public ushort TileId { get; set; }

            public string TileObj { get; set; }

            public int X { get; set; }

            public int Y { get; set; }

            public bool Equals(TerrainTile other) => TileId == other.TileId &&
                       TileObj == other.TileObj &&
                       Name == other.Name &&
                       Region == other.Region;
        }

        internal enum TileRegion : byte
        {
            None,
            Spawn,
            RealmPortals,
            Store1,
            Store2,
            Store3,
            Store4,
            Store5,
            Store6,
            Vault,
            Loot,
            Defender,
            Hallway,
            Enemy,
            Hallway1,
            Hallway2,
            Hallway3,
            Store7,
            Store8,
            Store9,
            GiftingChest,
            Store10,
            Store11,
            Store12,
            Store13,
            Store14,
            Store15,
            Store16,
            Store17,
            Store18,
            Store19,
            Store20,
            Store21,
            Store22,
            Store23,
            Store24,
            ItemSpawnPoint,
            Store25,
            Store26,
            Store27,
            Store28,
            Store29,
            Store30,
            QuestMonsterRegion,
            Store32,
            Store33,
            Store34,
            Store35,
            Store36,
            Store37,
            Store38,
            Store39,
            Store40,
            SupporterRegion,
            Door,
        }
    }

    internal class CustomTileBase : TileBase
    {
        internal Color Color = Color.white;
        internal float PixelsPerUnit = 32f;
        internal Sprite Sprite;

        public override void GetTileData(Vector3Int position, ITilemap tilemap, ref TileData tileData)
        {
            tileData.sprite = Sprite != null ? Sprite.Create(Sprite.texture, Sprite.rect, new Vector2(0.5f, 0.5f), PixelsPerUnit)
                : Resources.Load<Sprite>("Sprites/Blank");
            tileData.color = Color;
        }
    }

    // Class, so nullable
    internal class MapObject : IEquatable<MapObject>
    {
        public string Id;
        public string Name;

        public bool Equals(MapObject other) => other is not null && Name == other.Name && Id == other.Id;

        public static bool operator ==(MapObject a, MapObject b) => a is null ? b is null : a.Equals(b);

        public static bool operator !=(MapObject a, MapObject b) => a is null ? b is not null : !a.Equals(b);

        public override bool Equals(object obj) => obj.GetType() == GetType() && Equals((MapObject)obj);

        public override int GetHashCode() => (Id.GetHashCode() / 2) + (Name.GetHashCode() / 2);
    }
}
#endif