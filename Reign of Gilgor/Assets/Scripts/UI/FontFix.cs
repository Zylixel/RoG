using UnityEngine;

public class FontFix : MonoBehaviour
{
    [SerializeField]
    private Font[] fonts;

    private void Awake()
    {
        for (var i = 0; i < fonts.Length; i++)
        {
            fonts[i].material.mainTexture.filterMode = FilterMode.Point;
        }

        Destroy(gameObject);
    }
}
