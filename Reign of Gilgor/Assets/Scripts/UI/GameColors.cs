﻿using Client.Entity;
using Client.Entity.Player;
using Client.Networking;
using Microsoft.Extensions.Logging;
using RoGCore.Logging;
using UnityEngine;

internal sealed class GameColors : MonoBehaviour
{
    internal static Color GILGOR_PURPLE;
    internal static Color MINIMAP_SELF;
    internal static Color MINIMAP_OTHERPLAYER;
    internal static Color INVENTORY_HIGHLIGHT;
    internal static Color MINIMAP_ENEMY;
    internal static Color HP_GREEN;
    internal static Color HP_OUT_GREEN;
    internal static Color HP_ORANGE;
    internal static Color HP_OUT_ORANGE;
    internal static Color HP_RED;
    internal static Color HP_OUT_RED;
    internal static Color HIT;
    internal static Color ArmorBreak;
    internal static Color NoItemBackground;
    internal static Color ItemBackground;
    internal static Color ItemBackgroundHover;
    internal static Color ItemLockedBackground;
    internal static Color PortalFlash;
    internal static Color SKILL_ENABLED;
    internal static Color SKILL_DISABLED;
    internal static Color SKILL_CONNECTOR_ENABLED;
    internal static Color COMMON;
    internal static Color UNCOMMON;
    internal static Color NOALPHA_RED = new Color(1f, 0f, 0f, 0f);
    internal static Color ENTITY_INVISIBLE = new Color(1f, 1f, 1f, 0.1f);
    internal static Color LIGHT_GREEN;
    internal static Color HP_BLUE = new Color32(63, 0, 255, 255);

    private static readonly ILogger<GameColors> Logger = LogFactory.LoggingInstance<GameColors>();

    internal static Color GetOtherPlayerColor(Player self, ServerPlayer otherPlayer) => self == null || otherPlayer == null || self == otherPlayer
            ? Color.white
            : AccountListHandler.AccountLocked(otherPlayer.AccountId)
            ? GILGOR_PURPLE
            : self.GuildName != string.Empty && self.GuildName == otherPlayer.GuildName ? Color.green : Color.white;

    private void Awake()
    {
        if (!ColorUtility.TryParseHtmlString("#8900FF", out GILGOR_PURPLE))
        {
            Logger.LogError("Error loading color 8900FF in GameColors.cs");
        }

        if (!ColorUtility.TryParseHtmlString("#00F9FF", out MINIMAP_SELF))
        {
            Logger.LogError("Error loading color 00F9FF in GameColors.cs");
        }

        if (!ColorUtility.TryParseHtmlString("#007AFF", out MINIMAP_OTHERPLAYER))
        {
            Logger.LogError("Error loading color 007AFF in GameColors.cs");
        }

        if (!ColorUtility.TryParseHtmlString("#FFF8A8", out INVENTORY_HIGHLIGHT))
        {
            Logger.LogError("Error loading color FFF8A8 in GameColors.cs");
        }

        if (!ColorUtility.TryParseHtmlString("#FF0F0F", out MINIMAP_ENEMY))
        {
            Logger.LogError("Error loading color FF0F0F in GameColors.cs");
        }

        if (!ColorUtility.TryParseHtmlString("#00ee00", out HP_GREEN))
        {
            Logger.LogError("Error loading color 00cc00 in GameColors.cs");
        }

        if (!ColorUtility.TryParseHtmlString("#003500", out HP_OUT_GREEN))
        {
            Logger.LogError("Error loading color 003500 in GameColors.cs");
        }

        if (!ColorUtility.TryParseHtmlString("#FF8000", out HP_ORANGE))
        {
            Logger.LogError("Error loading color FF8000 in GameColors.cs");
        }

        if (!ColorUtility.TryParseHtmlString("#4e2700", out HP_OUT_ORANGE))
        {
            Logger.LogError("Error loading color 4e2700 in GameColors.cs");
        }

        if (!ColorUtility.TryParseHtmlString("#e60000", out HP_RED))
        {
            Logger.LogError("Error loading color e60000 in GameColors.cs");
        }

        if (!ColorUtility.TryParseHtmlString("#350000", out HP_OUT_RED))
        {
            Logger.LogError("Error loading color 350000 in GameColors.cs");
        }

        if (!ColorUtility.TryParseHtmlString("#FF7070", out HIT))
        {
            Logger.LogError("Error loading color FF7070 in GameColors.cs");
        }

        if (!ColorUtility.TryParseHtmlString("#A300A3", out ArmorBreak))
        {
            Logger.LogError("Error loading color A300A3 in GameColors.cs");
        }

        if (!ColorUtility.TryParseHtmlString("#6e6e6e", out ItemBackground))
        {
            Logger.LogError("Error loading color 6e6e6e in GameColors.cs");
        }

        if (!ColorUtility.TryParseHtmlString("#5c5c5c", out NoItemBackground))
        {
            Logger.LogError("Error loading color 5c5c5c in GameColors.cs");
        }

        if (!ColorUtility.TryParseHtmlString("#c9c9c9", out ItemBackgroundHover))
        {
            Logger.LogError("Error loading color c9c9c9 in GameColors.cs");
        }

        if (!ColorUtility.TryParseHtmlString("#808080", out ItemLockedBackground))
        {
            Logger.LogError("Error loading color 808080 in GameColors.cs");
        }

        if (!ColorUtility.TryParseHtmlString("#553763", out PortalFlash))
        {
            Logger.LogError("Error loading color 553763 in GameColors.cs");
        }

        if (!ColorUtility.TryParseHtmlString("#00FFFF", out SKILL_ENABLED))
        {
            Logger.LogError("Error loading color 00FFFF in GameColors.cs");
        }

        if (!ColorUtility.TryParseHtmlString("#404040", out SKILL_DISABLED))
        {
            Logger.LogError("Error loading color 404040 in GameColors.cs");
        }

        if (!ColorUtility.TryParseHtmlString("#007272", out SKILL_CONNECTOR_ENABLED))
        {
            Logger.LogError("Error loading color 007272 in GameColors.cs");
        }

        if (!ColorUtility.TryParseHtmlString("#3FFF00", out COMMON))
        {
            Logger.LogError("Error loading color 404040 in GameColors.cs");
        }

        if (!ColorUtility.TryParseHtmlString("#278C8A", out UNCOMMON))
        {
            Logger.LogError("Error loading color 007272 in GameColors.cs");
        }

        if (!ColorUtility.TryParseHtmlString("#66FF66", out LIGHT_GREEN))
        {
            Logger.LogError("Error loading color 66FF66 in GameColors.cs");
        }

        Destroy(gameObject);
    }
}