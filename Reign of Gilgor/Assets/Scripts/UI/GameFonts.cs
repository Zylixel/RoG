﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

internal static class GameFonts
{
    internal static readonly Font Default = Resources.Load<Font>("Fonts/dlxfont");
    internal static readonly TMP_FontAsset DefaultTMP = Resources.Load<TMP_FontAsset>("Fonts/dlxfont SDF");

    internal static void Configure(this Text text, TextAnchor anchor, int size = 12)
    {
        text.alignment = anchor;
        text.font = Default;
        text.fontSize = size;
    }
}