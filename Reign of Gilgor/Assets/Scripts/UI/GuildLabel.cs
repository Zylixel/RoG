﻿using System.Collections.Generic;
using RoGCore.Utils;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GuildLabel : MonoBehaviour
{
    [SerializeField]
    private Image icon;

    [SerializeField]
    private List<Sprite> icons;

    [SerializeField]
    private TextMeshProUGUI text;

    private bool inGuild;
    private bool visible = true;

    internal bool Visible
    {
        get => visible;
        set
        {
            visible = value;
            UpdateVisibility();
        }
    }

    private void UpdateVisibility() => gameObject.SetActive(visible && inGuild);

    internal void Set(string name, int rank)
    {
        inGuild = !name.IsNullOrWhiteSpace();
        if (inGuild)
        {
            text.text = name;
            icon.sprite = rank switch
            {
                0 => icons[0],
                10 => icons[1],
                20 => icons[2],
                30 => icons[3],
                40 => icons[4],
                _ => null,
            };
        }

        UpdateVisibility();
    }
}