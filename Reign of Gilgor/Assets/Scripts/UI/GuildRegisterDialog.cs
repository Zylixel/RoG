﻿using System;
using System.Collections;
using Client.UI.Dialog;
using RoGCore.Networking.Packets;
using UnityEngine;
using UnityEngine.UI;
using Text = UnityEngine.UI.Text;

namespace Client.UI
{
    internal sealed class GuildRegisterDialog : BaseDialog<GuildRegisterDialog>
    {
        [SerializeField]
        private UnityEngine.UI.Button closeButton;

        [SerializeField]
        private Text errorText;

        [SerializeField]
        private InputField guildNameField;

        [SerializeField]
        private UnityEngine.UI.Button registerButton;

        public override void Initialize()
        {
            base.Initialize();

            closeButton.onClick.AddListener(() => Close());
            registerButton.onClick.AddListener(() => RegisterEvent(null, null));
        }

        private void OnEnable()
        {
            InputMaster.SubscribeToKeyPress(KeyCode.Return, RegisterEvent);

            guildNameField.text = string.Empty;
        }

        private void OnDisable() => InputMaster.UnsubscribeToKeyPress(KeyCode.Return, RegisterEvent);

        private void RegisterEvent(object sender, EventArgs e) => StartCoroutine(Register());

        private IEnumerator Register()
        {
            if (ProcessingData)
            {
                yield break;
            }

            GameWorldManager.CurrentOutgoingNetwork.OutgoingMessage(new GuildCreate(guildNameField.text));
            ProcessingData = true;

            // TODO: Use Result Packet to resolve this
            /*GuildResult result;
            while (!GuildResultHandler.Pop(out result))
            {
                yield return null;
            }
            */

            ProcessingData = false;

            /*if (!result.Success)
            {
                errorText.text = result.ErrorText;
            }
            else*/
            {
                Close();
            }
        }

        internal static void Display() => Instance.Open();
    }
}