﻿using System.Runtime.CompilerServices;
using Client;
using Client.Entity;
using Client.UI.Item;
using RoGCore.Models;
using RoGCore.Models.ObjectSlot;
using Structures;
using UnityEngine;
using UnityEngine.UI;

#nullable enable

[RequireComponent(typeof(Image))]
internal sealed class HeldItemHandler : Initializable<HeldItemHandler>
{
    internal static HeldType CurrentHeld;

    internal static object? Data { get; private set; }

#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
    private static Image icon;
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

    public override void Initialize()
    {
        Instance = this;
        icon = GetComponent<Image>();
    }

    private void Update() => Instance.transform.position = Input.mousePosition;

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    internal static bool HasItem() => Instance.gameObject.activeSelf;

    internal static bool SetItem(SimpleEntity owner, byte slotId, Sprite? image)
    {
        if (HasItem() || owner is not IContainer container)
        {
            return false;
        }

        Data = new HeldItemData { Owner = owner, Container = container, SlotId = slotId };
        CurrentHeld = HeldType.Container;
        icon.sprite = image;
        Instance.gameObject.SetActive(true);
        Instance.transform.position = Input.mousePosition;
        EquipmentTooltip.ClearItem();
        return true;
    }

    // This is used when grabbing items from vault  
    internal static bool SetItem(in Item item, Sprite image)
    {
        if (HasItem())
        {
            return false;
        }

        Data = item;
        CurrentHeld = HeldType.Vault;
        icon.sprite = image;
        Instance.gameObject.SetActive(true);
        Instance.transform.position = Input.mousePosition;
        EquipmentTooltip.ClearItem();
        return true;
    }

    internal static Item? GetItem()
    {
        if (Data is null)
        {
            return null;
        }

        switch (CurrentHeld)
        {
            case HeldType.Container:
                var data = (HeldItemData)Data;
                if (data.Container == null)
                {
                    return null;
                }

                return data.Container.Inventory[data.SlotId];
            case HeldType.Vault:
                return (Item)Data;
            default: return null;
        }
    }

    internal static IObjectSlot? Dispatch()
    {
        if (Data is null)
        {
            return null;
        }

        Instance.gameObject.SetActive(false);
        switch (CurrentHeld)
        {
            case HeldType.Container:
                CurrentHeld = HeldType.None;
                var data = (HeldItemData)Data;
                Data = null;
                return new ContainerSlot(data.Owner.Id, data.SlotId);
            case HeldType.Vault:
                CurrentHeld = HeldType.None;
                var item = (Item)Data;
                Data = null;
                return new VaultSlot(item.Data.Uuid);
            default: return null;
        }
    }

    internal struct HeldItemData
    {
        internal SimpleEntity Owner;
        internal IContainer Container;
        internal byte SlotId;
    }

    internal enum HeldType
    {
        None,
        Container,
        Vault,
    }
}