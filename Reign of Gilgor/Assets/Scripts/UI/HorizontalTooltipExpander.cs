using UnityEngine;

namespace Client.UI
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(RectTransform))]
    public class HorizontalTooltipExpander : MonoBehaviour
    {
        [SerializeField]
        private Vector2 padding;
        [SerializeField]
        private RectTransform[] childRectTransforms;
        private RectTransform rectTransform;

        private void Awake() => rectTransform = transform as RectTransform;

        internal void Update()
        {
            if (!Application.isPlaying)
            {
                Awake();
            }

            var boundingBox = GetBoundingBox();

            rectTransform.position = new Vector2(boundingBox.x, boundingBox.y) - new Vector2(padding.x, -padding.y);
            rectTransform.sizeDelta = new Vector2((boundingBox.z - boundingBox.x) / rectTransform.lossyScale.x, (boundingBox.y - boundingBox.w) / rectTransform.lossyScale.y) + (2 * padding / rectTransform.lossyScale.y);
        }

        private Vector4 GetBoundingBox()
        {
            var boundingBox = Vector4.zero;

            for (var i = 0; i < childRectTransforms.Length; i++)
            {
                var currentRectTransform = childRectTransforms[i];

                if (!currentRectTransform.gameObject.activeSelf)
                {
                    continue;
                }

                var x = currentRectTransform.position.x - (currentRectTransform.sizeDelta.x * currentRectTransform.pivot.x * currentRectTransform.lossyScale.x);
                var y = currentRectTransform.position.y + (currentRectTransform.sizeDelta.y * (1 - currentRectTransform.pivot.y) * currentRectTransform.lossyScale.y);
                var xMax = x + (currentRectTransform.sizeDelta.x * currentRectTransform.lossyScale.x);
                var yMax = y - (currentRectTransform.sizeDelta.y * currentRectTransform.lossyScale.y);

                if (i == 0)
                {
                    boundingBox.x = x;
                    boundingBox.y = y;
                    boundingBox.z = xMax;
                    boundingBox.w = yMax;
                }
                else
                {
                    boundingBox.x = Mathf.Min(x, boundingBox.x);
                    boundingBox.y = Mathf.Max(y, boundingBox.y);
                    boundingBox.z = Mathf.Max(xMax, boundingBox.z);
                    boundingBox.w = Mathf.Min(yMax, boundingBox.w);
                }
            }

            return boundingBox;
        }
    }
}