﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Client.UI.Item
{
    [CreateAssetMenu(fileName = "ItemNotification", menuName = "ScriptableObjects/CommonItemNotification")]
    internal sealed class CommonItemNotification : ItemNotificationAnimation
    {
        internal override Sequence Animate(Image image)
        {
            Rotation = 0f;

            return base.Animate(image)
                .Append(image.rectTransform.DOSizeDelta(image.rectTransform.sizeDelta * ScaleMultiplier, 0.33f).SetEase(Ease.InSine))
                .AppendInterval(2f)
                .Append(image.rectTransform.DOSizeDelta(Vector2.zero, 0.33f).SetEase(Ease.OutSine));
        }

        internal override int Tier() => -2;
    }
}
