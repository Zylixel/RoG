using UnityEngine;

namespace Client.UI.Item
{
    internal class EquipmentHolder : MonoBehaviour
    {
        [SerializeField]
        private ItemHolder weapon;
        [SerializeField]
        private ItemHolder ability;
        [SerializeField]
        private ItemHolder garment;
        [SerializeField]
        private ItemHolder ring;

        internal ItemHolder Weapon => weapon;

        internal ItemHolder Ability => ability;

        internal ItemHolder Garment => garment;

        internal ItemHolder Ring => ring;

        internal ItemHolder[] AsArray()
        {
            return new ItemHolder[] { Weapon, Ability, Garment, Ring };
        }
    }
}