﻿using System.Collections.Generic;

namespace Client.UI.Item
{
    internal static class EquipmentOutlines
    {
        private static readonly Dictionary<string, int> Lookup = new Dictionary<string, int>()
        {
            { "Bow", 0 },
            { "Quiver", 1 },
            { "Leather", 2 },
            { "Ring", 3 },
            { "Artifact", 4 },
            { "Sword", 5 },
            { "Wand", 6 },
            { "Dagger", 7 },
            { "Staff", 8 },
            { "Robe", 9 },
            { "Armor", 10 },
            { "Katana", 11 },
            { "Helmet", 17 },
            { "Prism", 22 },
            { "Scepter", 23 },
            { "Cloak", 13 },
        };

        internal static int GetIndex(string itemType)
        {
            Lookup.TryGetValue(itemType, out int ret);
            return ret;
        }
    }
}
