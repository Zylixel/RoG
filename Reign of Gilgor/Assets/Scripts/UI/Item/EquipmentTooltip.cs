﻿using System.Linq;
using System.Text;
using Client.Game;
using Client.UI.Tooltip;
using RoGCore.Assets;
using RoGCore.Models;
using RoGCore.Utils;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Client.UI.Item
{
    internal sealed class EquipmentTooltip : AdvancedBaseTooltip<EquipmentTooltip>
    {
        [SerializeField]
        private HorizontalOrVerticalLayoutGroup layoutGroup;

        private static Image itemIcon;

        private static readonly StringBuilder DataStringBuilder = new StringBuilder();

        private RoGCore.Models.Item? currentItem;

        public override void Initialize()
        {
            itemIcon = GetComponentsInChildren<Image>(true).First(x => x.gameObject.name == "Image");

            base.Initialize();
        }

        private static bool ValidForClass(JsonItem item, ObjectDescription playerDescription)
        {
            if (!item.Usable)
            {
                return true;
            }

            for (var i = 0; i < 4; i++)
            {
                if (playerDescription.SlotTypes[i] == item.Type)
                {
                    return true;
                }
            }

            return false;
        }

        internal static void ClearItem()
        {
            Instance.currentItem = null;
            Instance.Close();
        }

        internal static void SetItem(RoGCore.Models.Item newItem, Sprite icon)
        {
            itemIcon.sprite = icon;
            Instance.currentItem = newItem;

            Instance.Open();
            Instance.Update();

            LayoutRebuilder.ForceRebuildLayoutImmediate(Instance.layoutGroup.transform as RectTransform);
        }

        private static string GenerateAttributeInfo(float before, float after, bool moreIsBetter)
        {
            var baseInfo = $"<color=#DBDE64>{after}</color>";
            if (before == after)
            {
                return baseInfo;
            }

            string differenceColor;
            var differencePolarity = after > before ? "+" : string.Empty;

            differenceColor = moreIsBetter ? after > before ? "66FF66" : "FF6666" : after < before ? "66FF66" : "FF6666";

            return baseInfo + $" <color=#{differenceColor}>({differencePolarity}{(decimal)after - (decimal)before})</color>";
        }

        private void OnEnable()
        {
            if (currentItem is null)
            {
                Close();
                return;
            }

            var valid = ValidForClass(currentItem.Value.BaseItem, Instance.gamePlayer.ObjectDesc);

            foreach (var comp in Instance.GetComponentsInChildren<Image>(true))
            {
                switch (comp.gameObject.name)
                {
                    case "LevelBackground":
                        comp.gameObject.SetActive(currentItem.Value.BaseItem.Usable);
                        break;
                    case "LevelProgressBar":

                        ((RectTransform)comp.transform).sizeDelta = new Vector2(
                                Mathf.Clamp((float)currentItem.Value.Data.Xp / currentItem.Value.Data.GetExpGoal() * 250f, 0f, 250f),
                                ((RectTransform)comp.transform).sizeDelta.y);
                        break;
                    case "Divider":
                        comp.color = valid ? Color.white : GameColors.HIT;
                        break;
                }
            }

            foreach (var comp in Instance.GetComponentsInChildren<Text>(true))
            {
                switch (comp.gameObject.name)
                {
                    case "Description":
                        comp.text = currentItem.Value.BaseItem.Description;
                        break;

                    case "Shots":
                        comp.gameObject.SetActive(currentItem.Value.BaseItem.Shots > 1);
                        comp.text = $"Shots: <color=#DBDE64>{currentItem.Value.BaseItem.Shots}</color>";
                        break;

                    case "Damage":
                        {
                            if (currentItem.Value.BaseItem.Projectiles == null)
                            {
                                comp.gameObject.SetActive(false);
                                break;
                            }

                            comp.gameObject.SetActive(true);

                            float before = currentItem.Value.BaseItem.Projectiles[0].BaseDamage;
                            float after = currentItem.Value.BaseItem.Projectiles[0].GetDamage(currentItem.Value, SkillTreeUI.ActiveSkills());

                            comp.text = $"Damage: {GenerateAttributeInfo(before, after, true)}";
                            break;
                        }

                    case "RateOfFire":
                        {
                            var before = currentItem.Value.BaseItem.BaseRateOfFire;
                            float after = currentItem.Value.BaseItem.GetRateOfFire(SkillTreeUI.ActiveSkills());

                            comp.gameObject.SetActive(after != 1);

                            comp.text = $"Fire Rate: {GenerateAttributeInfo(before, after, true)}";
                            break;
                        }

                    case "Range":
                        {
                            if (currentItem.Value.BaseItem.Projectiles == null)
                            {
                                comp.gameObject.SetActive(false);
                                break;
                            }

                            comp.gameObject.SetActive(true);

                            var before = currentItem.Value.BaseItem.Projectiles[0].BaseSpeed / 10f * currentItem.Value.BaseItem.Projectiles[0].BaseLifeTime / 1000f;
                            var after = currentItem.Value.BaseItem.Projectiles[0].GetSpeed(currentItem.Value.BaseItem, SkillTreeUI.ActiveSkills()) / 10f * currentItem.Value.BaseItem.Projectiles[0].GetLifetime(currentItem.Value.BaseItem, SkillTreeUI.ActiveSkills()) / 1000f;

                            comp.text = $"Range: {GenerateAttributeInfo(before, after, true)}";
                            break;
                        }

                    case "StatBoosts":
                        if (currentItem.Value.BaseItem.StatsBoost == null)
                        {
                            comp.gameObject.SetActive(false);
                            break;
                        }

                        DataStringBuilder.Clear();
                        comp.gameObject.SetActive(currentItem.Value.BaseItem.StatsBoost.Count > 0);
                        var scaledStatsBoost = currentItem.Value.GetStatBoosts();
                        foreach (var pair in currentItem.Value.BaseItem.StatsBoost)
                        {
                            float before = pair.Value;
                            float after = scaledStatsBoost[pair.Key];

                            comp.text = $"Cooldown: {GenerateAttributeInfo(before, after, false)} seconds";

                            switch (pair.Key)
                            {
                                case PlayerStats.Hp:
                                    DataStringBuilder.Append($"Health Points: {GenerateAttributeInfo(before, after, true)}\n");
                                    break;
                                case PlayerStats.Mp:
                                    DataStringBuilder.Append($"Mana Points: {GenerateAttributeInfo(before, after, true)}\n");
                                    break;
                                case PlayerStats.Attack:
                                    DataStringBuilder.Append($"Attack: {GenerateAttributeInfo(before, after, true)}\n");
                                    break;
                                case PlayerStats.Defense:
                                    DataStringBuilder.Append($"Defense: {GenerateAttributeInfo(before, after, true)}\n");
                                    break;
                                case PlayerStats.Speed:
                                    DataStringBuilder.Append($"Speed: {GenerateAttributeInfo(before, after, true)}\n");
                                    break;
                                case PlayerStats.Vitality:
                                    DataStringBuilder.Append($"Vitality: {GenerateAttributeInfo(before, after, true)}\n");
                                    break;
                                case PlayerStats.Wisdom:
                                    DataStringBuilder.Append($"Wisdom: {GenerateAttributeInfo(before, after, true)}\n");
                                    break;
                                case PlayerStats.Dexterity:
                                    DataStringBuilder.Append($"Dexterity: {GenerateAttributeInfo(before, after, true)}\n");
                                    break;
                            }
                        }

                        comp.text = DataStringBuilder.ToString().TrimEnd('\n'); ;
                        break;

                    case "Tips":
                        if (!currentItem.Value.BaseItem.Consumable)
                        {
                            comp.gameObject.SetActive(false);
                            break;
                        }

                        comp.gameObject.SetActive(true);
                        break;
                    case "Usable":
                        if (valid)
                        {
                            comp.gameObject.SetActive(false);
                            break;
                        }

                        comp.gameObject.SetActive(true);
                        comp.text = $"Not Usable by {Instance.gamePlayer.ObjectDesc.Name}";
                        break;
                    case "MpCost":
                        float beforeCost = currentItem.Value.BaseItem.MpCost;
                        float afterCost = currentItem.Value.GetMpCost(SkillTreeUI.Instance.SelectedSkills);

                        comp.gameObject.SetActive(afterCost > 0);
                        comp.text = $"Mana Cost: {GenerateAttributeInfo(beforeCost, afterCost, false)}";
                        break;
                    case "Cooldown":
                        {
                            var before = currentItem.Value.BaseItem.Cooldown / 1000f;
                            var after = currentItem.Value.GetCooldown(SkillTreeUI.Instance.SelectedSkills) / 1000f;

                            comp.gameObject.SetActive(after > 0);
                            comp.text = $"Cooldown: {GenerateAttributeInfo(before, after, false)} seconds";
                            break;
                        }

                    case "ExtData":
                        if (currentItem.Value.BaseItem.ExtData == null || currentItem.Value.BaseItem.ExtData.Length == 0)
                        {
                            comp.gameObject.SetActive(false);
                            break;
                        }

                        comp.gameObject.SetActive(true);
                        DataStringBuilder.Clear();
                        for (var i = 0; i < currentItem.Value.BaseItem.ExtData.Length; i++)
                        {
                            DataStringBuilder.Append(currentItem.Value.BaseItem.ExtData[i]);
                            if (i != currentItem.Value.BaseItem.ExtData.Length - 1)
                            {
                                DataStringBuilder.Append("\n");
                            }
                        }

                        comp.text = DataStringBuilder.ToString();
                        break;

                    case "Effects":
                        if (currentItem.Value.BaseItem.ActivateEffects == null || currentItem.Value.BaseItem.ActivateEffects.Length == 0)
                        {
                            comp.gameObject.SetActive(false);
                            break;
                        }

                        comp.gameObject.SetActive(true);
                        DataStringBuilder.Clear();

                        for (var i = 0; i < currentItem.Value.BaseItem.ActivateEffects.Length; i++)
                        {
                            if (i > 0)
                            {
                                DataStringBuilder.Append('\n');
                            }

                            var activateEffect = currentItem.Value.BaseItem.ActivateEffects[i];

                            var disabled = activateEffect.RequiredSkills is not null && !activateEffect.RequiredSkills.Any(requiredSkill => SkillTreeUI.Instance.HasSkill(requiredSkill));

                            if (disabled)
                            {
                                DataStringBuilder.Append("<color=#454545>Locked - ");
                            }

                            if (activateEffect.RevokeOnDamage.HasValue)
                            {
                                DataStringBuilder.Append(GenerateColorHeader("FF6666"));

                                if (activateEffect.RevokeOnDamage.Value > 1)
                                {
                                    DataStringBuilder.Append($"Removed If Damage Exceeds {activateEffect.RevokeOnDamage.Value} Hitpoints - </color>");
                                }
                                else
                                {
                                    DataStringBuilder.Append($"Removed On Damage - </color>");
                                }
                            }

                            switch (activateEffect.Effect)
                            {
                                case ActivateEffects.Scepter:
                                    {
                                        DataStringBuilder.Append(GenerateColorHeader("8900FF"));
                                        DataStringBuilder.Append("Scepter</color>\n");
                                        DataStringBuilder.Append(
                                            $"Pulls in an enemy {GenerateColorHeader("DBDE64")}{activateEffect.Strength}</color> tiles from {GenerateColorHeader("DBDE64")}{activateEffect.MaximumDistance}</color> tiles away\n");
                                        DataStringBuilder.Append(
                                            $"Only pulls enemies with {GenerateColorHeader("DBDE64")}{activateEffect.MaximumHitPoints}</color> Maximum HP or less\n");
                                        DataStringBuilder.Append(
                                            $"Deals {GenerateColorHeader("DBDE64")}{activateEffect.Damage}</color> damage");
                                        break;
                                    }

                                case ActivateEffects.ConditionEffectSelf:
                                    {
                                        DataStringBuilder.Append(GenerateColorHeader("8900FF"));
                                        DataStringBuilder.Append("On Use:</color>\n");
                                        DataStringBuilder.Append(
                                            $"Applies {GenerateColorHeader("DBDE64")}{activateEffect.ConditionEffect}</color> for {GenerateDurationInfo(activateEffect)}");
                                        break;
                                    }

                                case ActivateEffects.Summon:
                                    {
                                        DataStringBuilder.Append(GenerateColorHeader("8900FF"));
                                        DataStringBuilder.Append("On Use:</color>\n");
                                        var controllable = SkillTreeUI.Instance.HasSkill(SkillType.Clone3) ? $"{GenerateColorHeader("DBDE64")}controllable </color>" : string.Empty;
                                        DataStringBuilder.Append(
                                            $"Summons a {controllable}{GenerateColorHeader("DBDE64")}{activateEffect.Id}</color> for {GenerateDurationInfo(activateEffect)}");
                                        break;
                                    }
                            }

                            if (disabled)
                            {
                                DataStringBuilder.Append("</color>");
                            }

                            string GenerateDurationInfo(ActivateEffect effect)
                            {
                                var baseDuration = effect.BaseDurationMs * 0.001f;
                                var trueDuration = effect.GetDurationMs(SkillTreeUI.Instance.SelectedSkills) * 0.001f;

                                var baseInfo = $"{GenerateColorHeader("DBDE64")}{trueDuration}</color> ";
                                if (trueDuration == baseDuration)
                                {
                                    return baseInfo + "seconds";
                                }

                                var differenceColor = trueDuration > baseDuration ? "66FF66" : "FF6666";
                                var differencePolarity = trueDuration > baseDuration ? "+" : string.Empty;

                                return baseInfo + $"{GenerateColorHeader(differenceColor)}({differencePolarity}{(decimal)trueDuration - (decimal)baseDuration})</color> seconds";
                            }

                            string GenerateColorHeader(string color) => disabled ? "<color=#454545>" : $"<color=#{color}>";
                        }

                        comp.text = DataStringBuilder.ToString();
                        break;
                }
            }

            foreach (var tmp in Instance.GetComponentsInChildren<TextMeshProUGUI>(true))
            {
                switch (tmp.gameObject.name)
                {
                    case "LevelText":
                        tmp.text = $"Level {currentItem.Value.Data.Level}";
                        break;
                    case "Name":
                        var tierName = JsonItem.TierName(currentItem.Value.BaseItem.Tier);
                        if (currentItem.Value.BaseItem.Tier > 0)
                        {
                            tmp.text = tierName;
                            Instance.background.material.SetColor(ShaderProperties.HighlightColor, Color.white);
                        }
                        else if (currentItem.Value.BaseItem.Tier == -2)
                        {
                            tmp.text = $"<color=#3FFF00>{tierName}</color>";
                            Instance.background.material.SetColor(ShaderProperties.HighlightColor, GameColors.COMMON);
                        }
                        else if (currentItem.Value.BaseItem.Tier == -3)
                        {
                            tmp.text = $"<color=#278C8A>{tierName}</color>";
                            Instance.background.material.SetColor(ShaderProperties.HighlightColor, GameColors.UNCOMMON);
                        }
                        else
                        {
                            tmp.text = string.Empty;
                            Instance.background.material.SetColor(ShaderProperties.HighlightColor, Color.white);
                        }

                        if (!tmp.text.IsNullOrWhiteSpace())
                        {
                            tmp.text += " ";
                        }

                        tmp.text += currentItem.Value.Data.Name.IsNullOrWhiteSpace()
                            ? currentItem.Value.BaseItem.Name
                            : $"<color=#8900FF>{currentItem.Value.Data.Name}</color>";
                        break;
                }
            }

            LayoutRebuilder.ForceRebuildLayoutImmediate(Instance.layoutGroup.transform as RectTransform);
        }
    }
}