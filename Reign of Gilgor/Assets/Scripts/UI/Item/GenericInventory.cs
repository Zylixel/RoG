﻿using Client.Audio;
using Client.Entity;
using Client.Entity.Helpers;
using Client.Game;
using RoGCore.Models.ObjectSlot;
using RoGCore.Networking.Packets;
using Structures;
using UnityEngine;

#nullable enable

namespace Client.UI.Item
{
    [RequireComponent(typeof(CanvasGroup))]
    internal sealed class GenericInventory : MonoBehaviour
    {
        [SerializeField]
        internal bool AutoDisable;

        private const int Size = 8;

        private readonly ItemHolder[] items = new ItemHolder[Size];

        private SimpleEntity? owner;

#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
        private CanvasGroup canvasGroup;
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

        internal float Alpha
        {
            get => canvasGroup.alpha;
            set
            {
                canvasGroup.alpha = value;

                if (AutoDisable)
                {
                    gameObject.SetActive(value > 0);
                }
            }
        }

        private void Awake()
        {
            for (byte i = 0; i < Size; i++)
            {
                var newBackground = Instantiate(GamePrefabs.ItemHolder, transform).GetComponent<ItemHolder>();
                newBackground.transform.localPosition = new Vector3((i % 4) * 80, Mathf.FloorToInt(i * 0.25f) * -80);
                newBackground.Index = i;
                items[i] = newBackground;
            }

            canvasGroup = GetComponent<CanvasGroup>();

            if (AutoDisable)
            {
                gameObject.SetActive(Alpha > 0);
            }
        }

        internal void Set(SimpleEntity entity)
        {
            if (entity is not IContainer container)
            {
                return;
            }

            owner = entity;
            for (byte i = 0; i < Size; i++)
            {
                items[i].Set(container.Inventory[i], entity.Manager);

                if (i <= container.Inventory.Length - 1 && container.Inventory[i] is not null)
                {
                    items[i].Set(container.Inventory[i], entity.Manager);
                }
            }
        }

        private void Update()
        {
            if (owner == null)
            {
                return;
            }

            if (owner.Id != owner.Manager.Map.PlayerId
                && UnityEngine.Input.GetKeyDown(OptionsHandler.GetOptionValue<KeyCode>("interactKey")))
            {
                GameWorldManager.CurrentOutgoingNetwork.OutgoingMessage(new ContainerClaim(owner.Id, -1));
                AudioHandler.PlaySound("Fantasy/Special Click 03");
                return;
            }

            foreach (var itemHolder in ItemHolder.GetItemsInRaycastResults(owner.Manager.Map.CurrentRaycast, items))
            {
                if (!HeldItemHandler.HasItem())
                {
                    if (itemHolder.Item == null)
                    {
                        continue;
                    }

                    if (UnityEngine.Input.GetMouseButtonDown(2))
                    {
                        GameWorldManager.CurrentOutgoingNetwork.OutgoingMessage(new UseItem(
                            new ContainerSlot(owner.Id, itemHolder.Index),
                            System.Numerics.Vector2.Zero));
                    }
                }

                if (!UnityEngine.Input.GetMouseButtonDown(0))
                {
                    continue;
                }

                if (HeldItemHandler.HasItem())
                {
                    if (owner is IContainer container
                            && HeldItemHandler.Data is HeldItemHandler.HeldItemData heldItemData
                            && container.AuditItem(HeldItemHandler.GetItem(), itemHolder.Index)
                            && heldItemData.Container.AuditItem(itemHolder.Item, heldItemData.SlotId)
                            && HeldItemHandler.Dispatch() is IObjectSlot heldItem)
                    {
                        GameWorldManager.CurrentOutgoingNetwork.OutgoingMessage(new InventoryMove(
                            heldItem,
                            new ContainerSlot(owner.Id, itemHolder.Index)));
                    }
                }
                else
                {
                    if (UnityEngine.Input.GetKey(KeyCode.LeftControl) && owner.Id != owner.Manager.Map.PlayerId)
                    {
                        GameWorldManager.CurrentOutgoingNetwork.OutgoingMessage(new ContainerClaim(owner.Id, itemHolder.Index));
                        AudioHandler.PlaySound("Fantasy/Special Click 03");
                        return;
                    }

                    if (itemHolder.Item is not null)
                    {
                        HeldItemHandler.SetItem(owner, itemHolder.Index, itemHolder.Sprite);
                    }
                }
            }
        }
    }
}