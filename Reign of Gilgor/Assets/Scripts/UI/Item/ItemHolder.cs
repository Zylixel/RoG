﻿using System.Collections.Generic;
using Client.Entity.Helpers;
using Microsoft.Extensions.Logging;
using RoGCore.Logging;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using static Client.Entity.Helpers.EntityExtensions;

#nullable enable

namespace Client.UI.Item
{
    [RequireComponent(typeof(Image))]
    internal sealed class ItemHolder : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        internal byte Index;

#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
        [SerializeField]
        private Image itemImage;
        [SerializeField]
        private Image backgroundImage;
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

        [SerializeField]
        private bool Ability;
        [SerializeField]
        private bool HideBackground;

        private bool hovered;
        private Color underlyingColor = GameColors.NoItemBackground;
        private GameWorldManager? manager;

        private static readonly ILogger<ItemHolder> Logger = LogFactory.LoggingInstance<ItemHolder>();

        internal RoGCore.Models.Item? Item { get; private set; }

        internal Sprite? Sprite => itemImage.sprite;

        public void OnPointerEnter(PointerEventData eventData)
        {
            hovered = true;
            UpdateColor();

            if (Item is not null && !HeldItemHandler.HasItem())
            {
                EquipmentTooltip.SetItem(Item.Value, itemImage.sprite);
            }
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            hovered = false;
            UpdateColor();
            EquipmentTooltip.ClearItem();
        }

        private void OnValidate()
        {
            if (itemImage == null)
            {
                Logger.LogError($"GameObject: {gameObject.name} - Item Image cannot be null");
            }

            if (backgroundImage == null)
            {
                backgroundImage = GetComponent<Image>();
            }

            UpdateColor();
        }

        private void Awake()
        {
            itemImage.gameObject.SetActive(Item is not null);
            UpdateColor();
        }

        private void Update()
        {
            if (manager == null || manager.Map.Player == null || Item is null)
            {
                return;
            }

            if (Ability)
            {
                itemImage.color =
                Item.Value.GetMpCost(SkillTreeUI.ActiveSkills()) > manager.Map.Player.PlayerStats[(int)RoGCore.Models.PlayerStats.Mp] || manager.Map.Player.InAbilityCooldown()
                    ? Color.gray
                    : Color.white;
            }
        }

        internal void SetSize(int value)
        {
            Debug.Assert(value % 10 == 0);

            backgroundImage.rectTransform.sizeDelta = new Vector2(value, value);
            itemImage.rectTransform.sizeDelta = new Vector2(value - 20, value - 20);
        }

        internal void Set(RoGCore.Models.Item? item, GameWorldManager manager, Sprite? emptySprite = null)
        {
            this.manager = manager;

            underlyingColor = manager.Map.Player == null ? GameColors.NoItemBackground
                : manager.Map.Player.GetItemInfo(item) switch
                {
                    BgInfoResult.NoItem => GameColors.NoItemBackground,
                    BgInfoResult.Invalid => GameColors.HIT,
                    _ => GameColors.ItemBackground
                };

            Set(item, emptySprite);
        }

        internal void Set(RoGCore.Models.Item? item, Sprite? emptySprite = null)
        {
            Item = item;

            if (item is null)
            {
                if (emptySprite == null)
                {
                    itemImage.gameObject.SetActive(false);
                }
                else
                {
                    itemImage.gameObject.SetActive(true);
                    itemImage.sprite = emptySprite;
                }
            }
            else
            {
                itemImage.gameObject.SetActive(true);
                itemImage.sprite = item.Value.BaseItem?.Textures?[0];
            }

            UpdateColor();
        }

        private void UpdateColor() => backgroundImage.color = HideBackground ? Color.clear : hovered ? GameColors.ItemBackgroundHover : underlyingColor;

        internal static IEnumerable<ItemHolder> GetItemsInRaycastResults(List<RaycastResult>? raycastResults, IList<ItemHolder> itemBackgrounds)
        {
            if (raycastResults is null)
            {
                yield break;
            }

            for (var i = 0; i < raycastResults.Count; i++)
            {
                var item = raycastResults[i];
                var background = item.gameObject.GetComponent<ItemHolder>();

                if (background == null || !itemBackgrounds.Contains(background))
                {
                    continue;
                }

                yield return background;
            }
        }
    }
}
