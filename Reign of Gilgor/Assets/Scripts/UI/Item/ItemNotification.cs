﻿using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

#nullable enable

namespace Client.UI.Item
{
    [RequireComponent(typeof(Image))]
    internal sealed class ItemNotification : Initializable<ItemNotification>
    {
        [SerializeField]
        private ItemNotificationAnimation[] animations = Array.Empty<ItemNotificationAnimation>();

#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

        private Image image;
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

        private Sequence? currentSequence;
        private ItemNotificationAnimation? currentAnimation;

        public override void Initialize()
        {
            image = GetComponent<Image>();

            ItemNotificationEvent.Begin += Display;
        }

        private void Display(int tier)
        {
            var animation = TryGetAnimation(tier);
            if (animation == null)
            {
                return;
            }

            currentAnimation = animation;

            if (currentSequence.IsActive())
            {
                currentSequence.Complete();
            }

            gameObject.SetActive(true);

            currentSequence = currentAnimation.Animate(image);
            currentSequence.AppendCallback(() => gameObject.SetActive(false));
        }

        private ItemNotificationAnimation? TryGetAnimation(int tier)
        {
            foreach (var animation in animations)
            {
                if (animation.Tier() == tier)
                {
                    return animation;
                }
            }

            return null;
        }

        private void Update()
        {
            if (currentAnimation == null)
            {
                return;
            }

            transform.eulerAngles = new Vector3(0, 0, currentAnimation.Rotation + Game.Map.Map.Rotation.Deg);
        }
    }
}
