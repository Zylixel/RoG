﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Client.UI.Item
{
    internal abstract class ItemNotificationAnimation : ScriptableObject
    {
        internal float Rotation;

        protected const float ScaleMultiplier = 8;

        [SerializeField]
        protected Sprite icon;

        [SerializeField]
        protected Material material;

        internal virtual Sequence Animate(Image image)
        {
            image.sprite = icon;
            image.material = material;
            image.rectTransform.sizeDelta = new Vector2(icon.rect.width, icon.rect.height);

            return DOTween.Sequence();
        }

        internal abstract int Tier();
    }
}
