﻿#nullable enable

namespace Client.UI
{
    internal static class ItemNotificationEvent
    {
        internal delegate void ItemNotificationHandler(int tier);

        internal static event ItemNotificationHandler? Begin;

        internal static void Invoke(int tier) => Begin?.Invoke(tier);
    }
}
