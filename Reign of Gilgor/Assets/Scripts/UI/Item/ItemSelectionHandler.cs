﻿using System.Linq;
using Client.Entity;
using Client.Game;
using RoGCore.Models.ObjectSlot;
using RoGCore.Networking.Packets;
using RoGCore.Utils;
using UnityEngine;
using UnityEngine.UI;
using Vector2 = System.Numerics.Vector2;

namespace Client.UI.Item
{
    internal sealed class ItemSelectionHandler : Initializable<ItemSelectionHandler>
    {
        [SerializeField]
        private Image BottomLeftSection;

        [SerializeField]
        private Image BottomRightSection;

        [SerializeField]
        private Image BottomSection;

        [SerializeField]
        private Image TopLeftSection;

        [SerializeField]
        private Image TopRightSection;

        [SerializeField]
        private Image TopSection;

        private Image[] all;
        private Image[] allItems;

        private byte currentSlotSelected;
        private int displaying;
        private Color selectedColor;
        private byte[] slots;

        public override void Initialize()
        {
            all = new Image[6];
            all[0] = TopSection;
            all[1] = TopRightSection;
            all[2] = BottomRightSection;
            all[3] = BottomSection;
            all[4] = BottomLeftSection;
            all[5] = TopLeftSection;
            allItems = new Image[all.Length];
            for (var i = 0; i < all.Length; i++)
            {
                allItems[i] = all[i].GetComponentsInChildren<Image>(true).First(x => x.name.Contains("Item"));
            }

            slots = new byte[all.Length];
            for (var i = 0; i < slots.Length; i++)
            {
                slots[i] = byte.MaxValue;
            }

            for (var i = 0; i < all.Length; i++)
            {
                all[i].gameObject.SetActive(false);
            }

            selectedColor = GameColors.GILGOR_PURPLE;
            selectedColor.a = 0.5f;
        }

        private void Set(int numPressed, ServerPlayer player)
        {
            if (displaying == numPressed)
            {
                return;
            }

            for (var i = 0; i < all.Length; i++)
            {
                all[i].gameObject.SetActive(numPressed != 0);
            }

            if (numPressed == 0)
            {
                GameWorldManager.CurrentOutgoingNetwork.OutgoingMessage(new InventoryMove(
                    new ContainerSlot(player.Id, currentSlotSelected),
                    new ContainerSlot(player.Id, (byte)(displaying - 1))));

                displaying = numPressed;
                return;
            }

            var cur = 0;
            var type = player.ObjectDesc.SlotTypes[numPressed - 1];
            for (byte i = 0; i < player.Inventory.Length; i++)
            {
                if (cur >= allItems.Length)
                {
                    break;
                }

                var item = player.Inventory[i];

                if (item is null || item.Value.BaseItem.Type != type)
                {
                    continue;
                }

                allItems[cur].gameObject.SetActive(true);
                allItems[cur].sprite = item.Value.BaseItem.Textures[0];
                slots[cur] = i;
                currentSlotSelected = i;
                cur++;
            }

            // Deactivate ones without an item
            for (var i = cur; i < all.Length; i++)
            {
                allItems[i].gameObject.SetActive(false);
                slots[i] = byte.MaxValue;
            }

            displaying = numPressed;
        }

        private void Update()
        {
            if (GameWorldManager.Instance == null)
            {
                return;
            }

            var player = GameWorldManager.Instance.Map.Player;
            if (player == null)
            {
                return;
            }

            var active = GetActiveSelection();
            Set(active, player);
            if (active == 0)
            {
                return;
            }

            for (var i = 0; i < all.Length; i++)
            {
                all[i].color = new Color(1, 1, 1, 0.5f);
            }

            var distFromCenter = (new Vector2(Screen.width, Screen.height) * 0.5f) -
                                     UnityEngine.Input.mousePosition.Convert();
            var angle = MathsUtils.ClampAngle(-Mathf.Atan2(distFromCenter.Y, distFromCenter.X));
            if (angle > Mathf.PI / 3 && angle < 2 * Mathf.PI / 3 && slots[0] != byte.MaxValue)
            {
                TopSection.color = selectedColor;
                currentSlotSelected = slots[0];
            }
            else if (angle > 2 * Mathf.PI / 3 && angle < Mathf.PI && slots[1] != byte.MaxValue)
            {
                TopRightSection.color = selectedColor;
                currentSlotSelected = slots[1];
            }
            else if (angle > 0 && angle < Mathf.PI / 3 && slots[5] != byte.MaxValue)
            {
                TopLeftSection.color = selectedColor;
                currentSlotSelected = slots[5];
            }
            else if (angle > Mathf.PI && angle < 4 * Mathf.PI / 3 && slots[2] != byte.MaxValue)
            {
                BottomRightSection.color = selectedColor;
                currentSlotSelected = slots[2];
            }
            else if (angle > 4 * Mathf.PI / 3 && angle < 5 * Mathf.PI / 3 && slots[3] != byte.MaxValue)
            {
                BottomSection.color = selectedColor;
                currentSlotSelected = slots[3];
            }
            else if (angle > 5 * Mathf.PI / 3 && angle < 2 * Mathf.PI && slots[4] != byte.MaxValue)
            {
                BottomLeftSection.color = selectedColor;
                currentSlotSelected = slots[4];
            }
        }

        private static int GetActiveSelection()
        {
            // Ugly to read, but returns the current numerical key held down from 1-4. 
            // If the focus is not on the map, then 0 is returned.
            // If no key is held down, then 0 is returned.
            // If two keys are held down, it chooses the lowest one.

            if (InputMaster.Focus != typeof(Client.Game.Map.Map))
            {
                return 0;
            }

            var active =
                UnityEngine.Input.GetKey(OptionsHandler.GetOptionValue<KeyCode>("weaponOpenKey")) ? 1 :
                UnityEngine.Input.GetKey(OptionsHandler.GetOptionValue<KeyCode>("garmentOpenKey")) ? 2 :
                UnityEngine.Input.GetKey(OptionsHandler.GetOptionValue<KeyCode>("ringOpenKey")) ? 3 :
                UnityEngine.Input.GetKey(OptionsHandler.GetOptionValue<KeyCode>("artifactOpenKey")) ? 4 : 0;

            return active;
        }
    }
}