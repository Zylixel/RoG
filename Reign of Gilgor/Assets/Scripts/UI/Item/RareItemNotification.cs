﻿using Client.Camera;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Client.UI.Item
{
    [CreateAssetMenu(fileName = "ItemNotification", menuName = "ScriptableObjects/RareItemNotification")]
    internal sealed class RareItemNotification : ItemNotificationAnimation
    {
        internal override Sequence Animate(Image image)
        {
            Rotation = 25f;

            CameraShake.OnRareLoot();

            return base.Animate(image)
                .Append(image.rectTransform.DOSizeDelta(image.rectTransform.sizeDelta * ScaleMultiplier * (5 / 4f), 0.2f).SetEase(Ease.InSine))
                .Join(DOTween.To(() => Rotation, z => Rotation = z, 0f, 0.4f).SetEase(Ease.OutBack))
                .Append(image.rectTransform.DOSizeDelta(image.rectTransform.sizeDelta * ScaleMultiplier, 0.2f).SetEase(Ease.OutCirc))
                .AppendInterval(2f)
                .Append(image.rectTransform.DOSizeDelta(Vector3.zero, 0.33f).SetEase(Ease.InBack));
        }

        internal override int Tier() => -4;
    }
}
