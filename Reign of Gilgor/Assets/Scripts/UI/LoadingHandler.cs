﻿using System.Collections;
using System.Collections.Generic;
using Client;
using Client.Audio;
using Client.Entity.Player;
using Client.UI.TitleScreen;
using Client.Utils;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public sealed class LoadingHandler : Initializable<LoadingHandler>
{
    private static readonly Vector3 DesiredWorldTextPosition = new Vector3(0, 50);

    private static readonly Dictionary<int, Vector3> DifficultyPositions = new Dictionary<int, Vector3>()
    {
        { 0, Vector3.zero },
        { 1, new Vector3(-133, 0) },
        { 2, new Vector3(-80, 0) },
        { 3, new Vector3(-27, 0) },
        { 4, new Vector3(26, 0) },
        { 5, new Vector3(79, 0) },
        { 6, new Vector3(132, 0) },
    };

    [SerializeField]
    private Image blackBackground;

    [SerializeField]
    private RawImage difficultyBackground;

    [SerializeField]
    private RawImage difficultyCursor;

    [SerializeField]
    private Image whiteBackground;

    private bool worldDataCoroutineRunning;

    [SerializeField]
    private Text worldText;

    public override void Initialize()
    {
        Instance = this;

        GameWorldManager.PlayerCreated.Begin += (player) => StartCoroutine(Animate(player));
    }

    private IEnumerator AnimateWorldData(string t, int difficulty)
    {
        worldDataCoroutineRunning = true;
        blackBackground.ChangeUIAlpha(1000000, false);
        whiteBackground.ChangeUIAlpha(-1000000, false);
        whiteBackground.transform.localScale = Vector3.one;
        worldText.text = t;
        worldText.gameObject.SetActive(true);
        worldText.transform.localPosition = new Vector3(0, 400f);
        worldText.SetUIAlpha(0, false);
        difficultyBackground.SetUIAlpha(0, true);
        difficultyCursor.transform.localPosition = new Vector3(-160, 0);
        var desiredCursorPosition = DifficultyPositions[difficulty];
        while (worldText.transform.localPosition != DesiredWorldTextPosition &&
               difficultyCursor.transform.localPosition != desiredCursorPosition)
        {
            yield return new WaitForFixedUpdate();
            worldText.transform.AnimateMoveFixedLocal(DesiredWorldTextPosition);
            worldText.ChangeUIAlphaFixed(TitleScreenController.AnimationSpeed * 0.25f, false);
            if (difficulty > 0)
            {
                difficultyBackground.ChangeUIAlphaFixed(TitleScreenController.AnimationSpeed * 0.25f, true);
            }

            difficultyCursor.transform.AnimateMoveFixedLocal(desiredCursorPosition);
        }

        worldDataCoroutineRunning = false;
    }

    internal static void Load(string worldName, int difficulty, GameWorldManager manager) => Instance.InternalLoad(worldName, difficulty, manager);

    private void InternalLoad(string worldName, int difficulty, GameWorldManager manager)
    {
        switch (worldName)
        {
            case "Nexus":
                manager.EnterSafeZone();
                break;
            case "Home":
                manager.EnterSafeZone();
                break;
            case "Gilgor's Realm":
                manager.EnterRealm();
                break;
            default:
                manager.EnterDungeon();
                break;
        }

        InputMaster.Focus = typeof(LoadingHandler);
        gameObject.SetActive(true);
        StartCoroutine(AnimateWorldData(worldName, difficulty));
        Resources.UnloadUnusedAssets();
    }

    private IEnumerator Animate(Player player)
    {
        while (worldDataCoroutineRunning)
        {
            yield return new WaitForFixedUpdate();
        }

        var fadeWhiteBackground = DOTween.ToAlpha(() => whiteBackground.color, x => whiteBackground.color = x, 1f, 0.25f).SetEase(Ease.InCubic);
        yield return fadeWhiteBackground.WaitForCompletion();

        difficultyBackground.SetUIAlpha(0, true);
        blackBackground.color = Color.clear;
        worldText.gameObject.SetActive(false);

        AudioHandler.PlayPitchedSound("Fantasy/Special Click 13", 1.25f);

        yield return DOTween.Sequence()
                .Append(whiteBackground.transform.DOScaleY(0.005f, 0.4f).SetEase(Ease.InQuart))
                .AppendInterval(0.25f)
                .AppendCallback(() => AudioHandler.PlayPitchedSound("Fantasy/Special Click 13", 0.75f))
                .Append(whiteBackground.transform.DOScaleX(0.005f, 0.4f).SetEase(Ease.InQuart))
                .WaitForCompletion();

        gameObject.SetActive(false);
        player.StartCoroutine(player.Appear());
        InputMaster.Focus = typeof(Client.Game.Map.Map);
    }
}