﻿using System;
using System.Collections.Generic;
using System.Linq;
using Client.Entity;
using Client.Entity.Player;
using Client.Game.Event;
using Client.Networking;
using Client.UI.Tooltip;
using Client.Utils;
using RoGCore.Models;
using RoGCore.Networking.Packets;
using RoGCore.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace Client.UI
{
    public class MinimapHandler : Initializable<MinimapHandler>
    {
        private static readonly Vector4 RaycastPadding = new Vector4(2, 2, 2, 2);
        private readonly List<GameObject> objectPool = new List<GameObject>();
        private readonly Dictionary<long, GameObject> objects = new Dictionary<long, GameObject>();

        [SerializeField]
        private RectTransform mask;

        [SerializeField]
        private RawImage output;

        private Player player;
        private Texture2D mapTexture;
        private bool update;
        private float viewDistance = 10f;

        public override void Initialize()
        {
            Instance = this;

            for (var i = 0; i < 10; i++)
            {
                objectPool.Add(CreateDefaultObject());
            }

            ReconnectEvent.Begin += () => Clear();
            GameWorldManager.PlayerCreated.Begin += (player) => this.player = player;
            MapInfoHandler.MapInfoRecieved.Begin += InternalOnMapInfo;
        }

        private GameObject CreateDefaultObject()
        {
            var go = new GameObject("MinimapObject");
            var image = go.AddComponent<RawImage>();
            image.rectTransform.sizeDelta = Vector2.one;
            go.transform.SetParent(output.transform);
            image.transform.localScale = Vector3.one;
            image.raycastPadding = RaycastPadding;
            go.AddComponent<MinimapObjectData>();
            go.SetActive(false);
            return go;
        }

        private GameObject GetObjectFromPool()
        {
            GameObject ret;
            if (objectPool.Count > 0)
            {
                ret = objectPool[0];
                objectPool.Remove(ret);
            }
            else
            {
                ret = CreateDefaultObject();
            }

            ret.SetActive(true);
            return ret;
        }

        private void ReturnObjectToPool(GameObject go)
        {
            go.SetActive(false);
            go.GetComponent<MinimapObjectData>().Entity = null;
            objectPool.Add(go);
        }

        private void InternalOnMapInfo(MapInfo packet)
        {
            update = packet.Width != 0 && packet.Height != 0;
            output.gameObject.SetActive(update);
            if (!update)
            {
                return;
            }

            mapTexture = new Texture2D(packet.Width, packet.Height, TextureFormat.RGBA32, false)
            {
                filterMode = FilterMode.Point,
            };
            var clearColors = new Color32[packet.Width * packet.Height];
            Color32 clearColor = Color.clear;
            for (var i = 0; i < clearColors.Length; i++)
            {
                clearColors[i] = clearColor;
            }

            mapTexture.SetPixels32(clearColors);
            mapTexture.Apply();
            output.rectTransform.sizeDelta = new Vector2(packet.Width, packet.Height);
            output.texture = mapTexture;
        }

        internal static void OnUpdate(in Update packet) => Instance.InternalOnUpdate(packet);

        private void InternalOnUpdate(in Update packet)
        {
            if (!update)
            {
                return;
            }

            SetTiles(packet.Tiles, out var applyTexture);
            NewStatics(packet.NewStatics, out var applyTextureTwo);
            if (applyTexture || applyTextureTwo)
            {
                mapTexture.Apply();
            }

            NewObjects(packet.NewObjects);
            RemoveObjects(packet.RemovedObjectIds);
            PositionEntities();
        }

        private void RemoveObjects(in SizedArray<long> removedObjects)
        {
            for (var i = 0; i < removedObjects.Length; i++)
            {
                if (objects.TryGetValue(removedObjects[i], out var go))
                {
                    ReturnObjectToPool(go);
                    objects.Remove(removedObjects[i]);
                }
            }
        }

        private void NewObjects(List<ObjectDef> newObjects)
        {
            for (var i = 0; i < newObjects.Count; i++)
            {
                var newObject = newObjects[i];
                var desc = GameWorldManager.Instance.GameData.Objects[newObject.ObjectType];

                if (desc.Texture is null)
                {
                    continue;
                }

                var go = GetObjectFromPool();
                var image = go.GetComponent<RawImage>();

                var data = go.GetComponent<MinimapObjectData>();
                GameWorldManager.Instance.Map.Entities.TryGetValue(newObject.Stats.Id, out data.Entity);

                if (desc.Enemy)
                {
                    image.color = Color.red;
                }
                else if (data.Entity is ServerPlayer serverPlayer)
                {
                    image.color = GameColors.GetOtherPlayerColor(player, serverPlayer);

                    if (image.color == Color.white)
                    {
                        image.color = Color.yellow;
                    }
                }
                else
                {
                    image.color = Color.blue;
                }

                objects.Add(newObject.Stats.Id, go);
            }
        }

        private void NewStatics(List<ObjectDef> newStatics, out bool updatedTexture)
        {
            updatedTexture = false;
            for (var i = 0; i < newStatics.Count; i++)
            {
                var def = newStatics[i];
                var desc = GameWorldManager.Instance.GameData.Objects[def.ObjectType];
                if (desc.AverageTopColors.Length > 0)
                {
                    mapTexture.SetPixel((int)def.Stats.Pos.X, (int)def.Stats.Pos.Y, desc.AverageTopColors[0]);
                    updatedTexture = true;
                }
                else if (desc.AverageColors.Length > 0)
                {
                    mapTexture.SetPixel((int)def.Stats.Pos.X, (int)def.Stats.Pos.Y, desc.AverageColors[0]);
                    updatedTexture = true;
                }
            }
        }

        private void SetTiles(in SizedArray<TileData> data, out bool updatedTexture)
        {
            updatedTexture = data.Length > 0;
            for (var i = 0; i < data.Length; i++)
            {
                var tile = data[i];
                var desc = GameWorldManager.Instance.GameData.Tiles[tile.Tile];
                mapTexture.SetPixel(tile.Position.X, tile.Position.Y, desc.AverageColors[0]);
            }
        }

        private void PositionEntities()
        {
            if (player == null)
            {
                return;
            }

            output.transform.localScale = new Vector3(viewDistance, viewDistance);
            output.transform.localPosition = (new Vector2(player.Transform.localPosition.x, player.Transform.localPosition.y) - (output.rectTransform.sizeDelta * 0.5f)) * viewDistance;

            var clampMax = new Vector2(
                (mask.sizeDelta.x - 10) * 0.5f / viewDistance,
                (mask.sizeDelta.y - 10) * 0.5f / viewDistance);

            var clampMin = clampMax * -1;
            var adjustedMiniMapViewDist = 10 / viewDistance;
            var cornerClampOffset = mask.localPosition.x * 10 * adjustedMiniMapViewDist;
            foreach (var entity in objects.Values)
            {
                var self = entity.GetComponent<MinimapObjectData>().Entity;
                entity.transform.localScale = new Vector3(adjustedMiniMapViewDist, adjustedMiniMapViewDist);

                Vector2 v = self.Transform.localPosition - player.Transform.localPosition;
                var vClamped = v.Clamp(clampMin, clampMax);

                // Corner Clamps
                if (vClamped.x > clampMax.x - cornerClampOffset)
                {
                    // TL
                    if (vClamped.y > clampMax.y - cornerClampOffset)
                    {
                        if (v.x < v.y)
                        {
                            vClamped.x = clampMax.x - cornerClampOffset;
                        }
                        else
                        {
                            vClamped.y = clampMax.y - cornerClampOffset;
                        }
                    }

                    // BL
                    else if (vClamped.y < clampMin.y + cornerClampOffset)
                    {
                        if (v.x < v.y)
                        {
                            vClamped.x = clampMax.x - cornerClampOffset;
                        }
                        else
                        {
                            vClamped.y = clampMin.y + cornerClampOffset;
                        }
                    }
                }
                else if (vClamped.x < clampMin.x + cornerClampOffset)
                {
                    // TR
                    if (vClamped.y > clampMax.y - cornerClampOffset)
                    {
                        if (v.x > v.y)
                        {
                            vClamped.x = clampMin.x + cornerClampOffset;
                        }
                        else
                        {
                            vClamped.y = clampMax.y - cornerClampOffset;
                        }
                    }

                    // BR
                    else if (vClamped.y < clampMin.y + cornerClampOffset)
                    {
                        if (v.x > v.y)
                        {
                            vClamped.x = clampMin.x + cornerClampOffset;
                        }
                        else
                        {
                            vClamped.y = clampMin.y + cornerClampOffset;
                        }
                    }
                }

                vClamped +=
                    new Vector2(output.transform.localPosition.x, output.transform.localPosition.y) /
                    viewDistance;
                entity.transform.localPosition = vClamped;
            }
        }

        private void OnEnable() => InputMaster.SubscribeToControlClick(OnControlClick);

        private void OnDisable() => InputMaster.UnsubscribeToControlClick(OnControlClick);

        private void Update()
        {
            if (player == null)
            {
                return;
            }

            PositionEntities();

            if (InputMaster.Focus != typeof(Client.Game.Map.Map))
            {
                return;
            }

            if (UnityEngine.Input.GetAxis("Mouse ScrollWheel") != 0)
            {
                viewDistance =
                    Mathf.Clamp(viewDistance + (UnityEngine.Input.GetAxis("Mouse ScrollWheel") * 2), 0.5f, 10);
            }
        }

        private void OnControlClick(object sender, EventArgs e)
        {
            foreach (var bg in player.Manager.Map.CurrentRaycast.Where(x =>
                                objects.ContainsValue(x.gameObject)))
            {
                var entity = bg.gameObject.GetComponent<MinimapObjectData>().Entity;
                if (entity is ServerPlayer splayer and not Player)
                {
                    PlayerTooltip.Display(splayer);
                }
            }
        }

        internal static void Clear()
        {
            foreach (var item in Instance.objects)
            {
                Instance.ReturnObjectToPool(item.Value);
            }

            Instance.objects.Clear();
            Destroy(Instance.mapTexture);
            Instance.mapTexture = null;
        }
    }
}