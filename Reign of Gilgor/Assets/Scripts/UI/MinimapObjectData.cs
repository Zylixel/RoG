﻿using Client.Entity;
using Client.Entity.Player;
using Client.UI.Tooltip;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Client.UI
{
    internal sealed class MinimapObjectData : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        internal SimpleEntity Entity;

        public void OnPointerEnter(PointerEventData data)
        {
            if (Entity is ServerPlayer player and not Player)
            {
                PlayerTooltipPreview.Display(player);
            }
        }

        public void OnPointerExit(PointerEventData data) => PlayerTooltipPreview.CloseTooltip();
    }
}