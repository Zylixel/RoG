﻿using UnityEngine;
using UnityEngine.EventSystems;

public class MouseDragBehavior : MonoBehaviour, IDragHandler, IBeginDragHandler
{
    private Vector2 lastMousePosition;

    /// <summary>
    ///     This method will be called on the start of the mouse drag
    /// </summary>
    /// <param name="eventData">mouse pointer event data</param>
    public void OnBeginDrag(PointerEventData eventData) => lastMousePosition = eventData.position;

    /// <summary>
    ///     This method will be called during the mouse drag
    /// </summary>
    /// <param name="eventData">mouse pointer event data</param>
    public void OnDrag(PointerEventData eventData)
    {
        var currentMousePosition = eventData.position;
        var diff = currentMousePosition - lastMousePosition;
        var rect = GetComponent<RectTransform>();
        var newPosition = rect.position + new Vector3(diff.x, diff.y, transform.position.z);
        rect.position = newPosition;
        lastMousePosition = currentMousePosition;
    }

    /// <summary>
    ///     This methods will check is the rect transform is inside the screen or not
    /// </summary>
    /// <param name="rectTransform">Rect Trasform</param>
    /// <returns></returns>
    private bool IsRectTransformInsideSreen(RectTransform rectTransform)
    {
        var isInside = false;
        var corners = new Vector3[4];
        rectTransform.GetWorldCorners(corners);
        var visibleCorners = 0;
        var rect = new Rect(0, 0, Screen.width, Screen.height);
        foreach (var corner in corners)
        {
            if (rect.Contains(corner))
            {
                visibleCorners++;
            }
        }

        if (visibleCorners == 4)
        {
            isInside = true;
        }

        return isInside;
    }
}