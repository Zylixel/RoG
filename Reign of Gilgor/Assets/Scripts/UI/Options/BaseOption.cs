﻿using Microsoft.Extensions.Logging;
using RoGCore.Logging;
using RoGCore.Utils;

namespace Client.UI.Options
{
    internal abstract class BaseOption
    {
        internal string InternalName;
        internal string ClientName;
        internal OptionsScreen.Tab Tab;
        internal string DependentOn;

        private static readonly ILogger<BaseOption> Logger = LogFactory.LoggingInstance<BaseOption>();

#if UNITY_EDITOR || DEVELOPMENT_BUILD
        protected void ValidateOption()
        {
            if (InternalName.IsNullOrWhiteSpace())
            {
                Logger.LogError("Cannot find option with name: " + InternalName);
            }
        }
#endif
    }
}