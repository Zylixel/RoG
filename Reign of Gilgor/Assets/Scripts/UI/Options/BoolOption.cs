﻿using UnityEngine;

namespace Client.UI.Options
{
    internal sealed class BoolOption : GenericOption<bool>
    {
        internal BoolOption(string internalName)
        {
            InternalName = internalName;
            ClientName = string.Empty;
            DefaultValue = false;
            DependentOn = string.Empty;
        }

        protected override bool GetValue()
        {
#if UNITY_EDITOR || DEVELOPMENT_BUILD
            ValidateOption();
#endif
            return !cacheSet ? SetCache(PlayerPrefs.GetInt(InternalName, DefaultValue ? 1 : 0) == 1) : cachedValue;
        }

        protected override void SetValue(bool value)
        {
            PlayerPrefs.SetInt(InternalName, value ? 1 : 0);
            base.SetValue(value);
        }
    }
}