﻿using System;
using UnityEngine;

namespace Client.UI.Options
{
    internal class EnumOption<T> : GenericOption<T>
        where T : Enum
    {
        internal EnumOption(string internalName)
        {
            InternalName = internalName;
            ClientName = string.Empty;
            DefaultValue = default;
            DependentOn = string.Empty;
        }

        protected override T GetValue()
        {
#if UNITY_EDITOR || DEVELOPMENT_BUILD
            ValidateOption();
#endif
            return !cacheSet
                ? SetCache((T)Enum.Parse(typeof(T), PlayerPrefs.GetString(InternalName, DefaultValue.ToString())) ?? DefaultValue)
                : cachedValue;
        }

        protected override void SetValue(T value)
        {
            PlayerPrefs.SetString(InternalName, value.ToString());
            base.SetValue(value);
        }
    }
}