﻿using System;

namespace Client.UI.Options
{
    internal abstract class GenericOption<T> : BaseOption
    {
        protected bool cacheSet;
        protected T cachedValue;
        internal T DefaultValue;

        internal event EventHandler<T> OptionChangedEvent;

        internal virtual T SetCache(T value)
        {
            cacheSet = true;
            cachedValue = value;
            return value;
        }

        internal T Value { get => GetValue(); set => SetValue(value); }

        protected abstract T GetValue();

        protected virtual void SetValue(T value)
        {
            SetCache(value);

            OptionChangedEvent?.Invoke(this, value);
        }
    }
}