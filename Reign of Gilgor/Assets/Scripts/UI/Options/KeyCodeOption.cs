﻿using Client.Input;
using UnityEngine;

namespace Client.UI.Options
{
    internal sealed class KeyCodeOption : EnumOption<KeyCode>, IKeyCodeProvider
    {
        internal KeyCodeOption(string internalName)
            : base(internalName)
        {
        }

        KeyCode IKeyCodeProvider.GetKeyCode() => Value;
    }
}