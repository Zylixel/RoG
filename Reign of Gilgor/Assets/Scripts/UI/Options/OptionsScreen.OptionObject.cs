﻿using UnityEngine;

namespace Client.UI.Options
{
    internal sealed partial class OptionsScreen
    {
        private struct OptionObject
        {
            internal BaseOption Option;
            internal GameObject GameObject;
        }
    }
}
