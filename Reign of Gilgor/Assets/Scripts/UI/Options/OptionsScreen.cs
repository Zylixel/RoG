using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Client.Audio;
using Client.Game;
using Client.Structures;
using Client.UI.Dialog;
using Client.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace Client.UI.Options
{
    internal sealed partial class OptionsScreen : BaseDialog<OptionsScreen>
    {
        [SerializeField]
        private GameObject content;
        [SerializeField]
        private ScrollRect scrollRect;
        [SerializeField]
        private Transform background;

        private static int updateCount;
        private static Tab currentTab;
        private static readonly List<OptionObject> CurrentView = new List<OptionObject>();

        private Tuple<GenericOption<KeyCode>, GameObject> currentKeyOption;

        internal enum Tab
        {
            General,
            Controls,
            Video,
            Audio,
        }

        public override void Initialize()
        {
            base.Initialize();

            SetView(0, true);

            InputMaster.SubscribeToKeyPress(KeyCode.O, (_, __) => Toggle());
        }

        public void CloseDialog() => Close();

        protected override Transform GetScaledObject() => background;

        private void Update()
        {
            if (currentKeyOption == null)
            {
                return;
            }

            foreach (KeyCode key in Enum.GetValues(typeof(KeyCode)))
            {
                if (!UnityEngine.Input.GetKey(key))
                {
                    continue;
                }

                PlayerPrefs.SetInt(currentKeyOption.Item1.InternalName, (int)key);
                currentKeyOption.Item2.GetComponentsInChildren<Text>()
                    .First(x => x.gameObject.name == "OptionLineValue").text = HumanizeKeyCode(key);
                currentKeyOption.Item2.GetComponentInChildren<Image>().color = GameColors.GILGOR_PURPLE;
                currentKeyOption = null;
                break;
            }
        }

        private string HumanizeKeyCode(KeyCode keyCode) => keyCode switch
        {
            KeyCode.Mouse0 => "Left Mouse",
            KeyCode.Mouse1 => "Right Mouse",
            KeyCode.Alpha0 or KeyCode.Alpha1 or KeyCode.Alpha2 or KeyCode.Alpha3 or KeyCode.Alpha4 or KeyCode.Alpha5 or KeyCode.Alpha6 or KeyCode.Alpha7 or KeyCode.Alpha8 or KeyCode.Alpha9 => keyCode.ToString().Remove(0, 5),
            _ => SplitCamelCase(keyCode.ToString()),
        };

        private static string SplitCamelCase(string input)
        {
            if (input.Length <= 1)
            {
                return input;
            }

            var stringBuilder = new StringBuilder();

            for (var i = 0; i < input.Length; i++)
            {
                var character = input[i];

                if (character is >= (char)65 and <= (char)90)
                {
                    stringBuilder.Append(' ');
                }

                stringBuilder.Append(character);
            }

            return stringBuilder.ToString();
        }

        private void OnDisable()
        {
            currentKeyOption = null;
            InputMaster.Focus = typeof(Client.Game.Map.Map);
        }

        private void OnEnable()
        {
            UpdateViewOptions();
            InputMaster.Focus = typeof(OptionsHandler);
        }

        private void UpdateViewOptions()
        {
            foreach (var optionObject in CurrentView)
            {
                if (optionObject.Option is GenericOption<bool> boolOption)
                {
                    var value = boolOption.Value;
                    optionObject.GameObject.GetComponentsInChildren<Text>().First(x => x.gameObject.name == "OptionLineValue")
                        .text = value ? "Enabled" : "Disabled";
                    optionObject.GameObject.GetComponentInChildren<Image>().color = value ? Color.green : Color.red;
                }
                else if (optionObject.Option is GenericOption<KeyCode> keyCodeOption)
                {
                    optionObject.GameObject.GetComponentsInChildren<Text>().First(x => x.gameObject.name == "OptionLineValue")
                        .text = HumanizeKeyCode(keyCodeOption.Value);
                    optionObject.GameObject.GetComponentInChildren<Image>().color = GameColors.GILGOR_PURPLE;
                }
                else if (optionObject.Option is GenericOption<ShaderQuality> shaderQualityOption)
                {
                    optionObject.GameObject.GetComponentsInChildren<Text>().First(x => x.gameObject.name == "OptionLineValue")
                        .text = shaderQualityOption.Value.ToString();
                }
                else if (optionObject.Option is SliderOption sliderOption)
                {
                    var slider = optionObject.GameObject.GetComponentInChildren<Slider>();
                    slider.value = sliderOption.Value;
                }
            }
        }

        internal static void UpdateDependencies()
        {
            foreach (var optionObject in CurrentView.Where(optionObject => optionObject.Option.DependentOn != string.Empty))
            {
                var dependencyActive = OptionsHandler.GetOptionValue<bool>(optionObject.Option.DependentOn);

                optionObject.GameObject.GetComponent<Text>().SetUIAlpha(dependencyActive ? 0.25f : 1f, false);

                if (optionObject.Option is GenericOption<bool> or GenericOption<KeyCode> or GenericOption<ShaderQuality>)
                {
                    foreach (var button in optionObject.GameObject.GetComponentsInChildren<UnityEngine.UI.Button>())
                    {
                        button.interactable = !dependencyActive;
                    }
                }
                else if (optionObject.Option is SliderOption)
                {
                    foreach (var slider in optionObject.GameObject.GetComponentsInChildren<Slider>())
                    {
                        slider.interactable = !dependencyActive;
                    }
                }
            }
        }

        private void OptionClicked(BaseOption option, GameObject obj)
        {
            AudioHandler.PlaySound("Fantasy/Special Click 03");

            if (option is GenericOption<bool>)
            {
                BoolOptionClicked(option, obj);
            }
            else if (option is GenericOption<KeyCode> keyCodeOption)
            {
                KeyCodeOptionClicked(keyCodeOption, obj);
            }
            else if (option is GenericOption<ShaderQuality> shaderQualityOption)
            {
                ShaderQualityOptionClicked(shaderQualityOption, obj);
            }
        }

        private void KeyCodeOptionClicked(GenericOption<KeyCode> option, GameObject obj)
        {
            if (currentKeyOption is not null)
            {
                currentKeyOption.Item2.GetComponentsInChildren<Text>()
                        .First(x => x.gameObject.name == "OptionLineValue").text =
                    HumanizeKeyCode((KeyCode)PlayerPrefs.GetInt(option.InternalName, (int)option.DefaultValue));
                currentKeyOption.Item2.GetComponentInChildren<Image>().color = GameColors.GILGOR_PURPLE;
            }

            obj.GetComponentsInChildren<Text>().First(x => x.gameObject.name == "OptionLineValue").text =
                "Press Key...";
            obj.GetComponentInChildren<Image>().color = Color.green;
            currentKeyOption = Tuple.Create(option, obj);
        }

        private static void BoolOptionClicked(BaseOption option, GameObject obj)
        {
            var newOption = OptionsHandler.ToggleOption(option.InternalName);
            obj.GetComponentsInChildren<Text>().First(x => x.gameObject.name == "OptionLineValue").text =
                newOption ? "Enabled" : "Disabled";
            obj.GetComponentInChildren<Image>().color = newOption ? Color.green : Color.red;
        }

        private static void ShaderQualityOptionClicked(GenericOption<ShaderQuality> option, GameObject obj)
        {
            option.Value = (ShaderQuality)(((int)option.Value + 1) % Enum.GetValues(typeof(ShaderQuality)).Length);
            obj.GetComponentsInChildren<Text>().First(x => x.gameObject.name == "OptionLineValue").text = option.Value.ToString();
        }

        private static IEnumerator SwipeOption(GameObject option)
        {
            var startUpdateCount = updateCount;
            while (Mathf.RoundToInt(option.transform.localPosition.x) != -175 && startUpdateCount == updateCount)
            {
                option.AnimateMoveLocal(new Vector2(-175, option.transform.localPosition.y));
                yield return new WaitForEndOfFrame();
            }
        }

        private void SetView(int dir, bool instant = false)
        {
            currentKeyOption = null;
            SwipeView(dir, instant);
            scrollRect.verticalNormalizedPosition = 1f;

            var i = 0;
            foreach (var option in OptionsHandler.Options.Where(option => option.Tab == currentTab))
            {
                var optionObject = option is GenericOption<bool> or GenericOption<KeyCode>
                    ? InstantiateOptionLineObject(dir, option, i)
                    : option is SliderOption sliderOption
                        ? InstantiateOptionSliderObject(dir, sliderOption, i)
                        : option is EnumOption<ShaderQuality> shaderQualityOption
                                            ? InstantiateShaderQualityObject(dir, shaderQualityOption, i)
                                            : throw new Exception($"Option type is not supported: {option.GetType()}");
                if (instant)
                {
                    optionObject.transform.localPosition = new Vector3(-175f, optionObject.transform.localPosition.y);
                }
                else
                {
                    StartCoroutine(SwipeOption(optionObject));
                }

                CurrentView.Add(new OptionObject { Option = option, GameObject = optionObject });
                i++;
            }

            UpdateViewOptions();
        }

        private static GameObject InstantiateOptionSliderObject(int dir, SliderOption option, int i)
        {
            var optionObject = Instantiate(GamePrefabs.OptionSlider);
            SetupOptionObject(dir, new OptionObject { Option = option, GameObject = optionObject }, i);

            var slider = optionObject.GetComponentInChildren<Slider>();
            slider.minValue = option.MinValue;
            slider.maxValue = option.MaxValue;
            slider.onValueChanged.AddListener(delegate
            {
                option.Value = slider.value;
            });

            return optionObject;
        }

        private GameObject InstantiateShaderQualityObject(int dir, GenericOption<ShaderQuality> option, int i)
        {
            var optionObject = Instantiate(GamePrefabs.OptionLine);
            SetupOptionObject(dir, new OptionObject { Option = option, GameObject = optionObject }, i);

            optionObject.GetComponentInChildren<UnityEngine.UI.Button>().onClick.AddListener(delegate
            {
                OptionClicked(option, optionObject);
            });

            return optionObject;
        }

        private GameObject InstantiateOptionLineObject(int dir, BaseOption option, int i)
        {
            var optionObject = Instantiate(GamePrefabs.OptionLine);
            SetupOptionObject(dir, new OptionObject { Option = option, GameObject = optionObject }, i);

            optionObject.GetComponentInChildren<UnityEngine.UI.Button>().onClick.AddListener(delegate
            {
                OptionClicked(option, optionObject);
            });
            return optionObject;
        }

        private static void SetupOptionObject(int dir, OptionObject optionObject, int i)
        {
            optionObject.GameObject.GetComponent<Text>().text = optionObject.Option.ClientName;
            optionObject.GameObject.transform.SetParent(Instance.content.transform);
            optionObject.GameObject.transform.localPosition = new Vector3((dir < 0 ? 1 : -1) * 1500, 250 - (i * 40));
            optionObject.GameObject.transform.localScale = Vector3.one;
        }

        private static IEnumerator SwipeAndDestroyOption(GameObject option, int dir)
        {
            var startUpdateCount = updateCount;
            var polarity = dir < 0 ? -1 : 1;
            while ((int)Mathf.Round(option.transform.localPosition.x) != polarity * 1500 &&
                   startUpdateCount == updateCount)
            {
                option.AnimateMoveLocal(new Vector3(polarity * 1500, option.transform.localPosition.y));
                yield return new WaitForEndOfFrame();
            }

            DestroyOption(option);
        }

        private static void DestroyOption(GameObject option)
        {
            var button = option.GetComponentInChildren<UnityEngine.UI.Button>();
            if (button != null)
            {
                button.onClick.RemoveAllListeners();
            }

            var slider = option.GetComponentInChildren<Slider>();
            if (slider != null)
            {
                slider.onValueChanged.RemoveAllListeners();
            }

            Destroy(option);
        }

        private void SwipeView(int dir, bool instant = false)
        {
            foreach (var optionObject in CurrentView)
            {
                if (instant)
                {
                    DestroyOption(optionObject.GameObject);
                }
                else
                {
                    StartCoroutine(SwipeAndDestroyOption(optionObject.GameObject, dir));
                }
            }

            CurrentView.Clear();
        }

        public void TabButtonPressed(string tabToEnter)
        {
            if (!Enum.TryParse(tabToEnter, true, out Tab newTab))
            {
                throw new Exception($"Unable to parse Tab enum for value {tabToEnter}");
            }

            if (newTab == currentTab)
            {
                return;
            }

            var previousTab = currentTab;
            currentTab = newTab;

            // Stop all animations before click
            updateCount++;
            switch (newTab)
            {
                case Tab.General:
                    SetView(1);
                    break;
                case Tab.Controls:
                    SetView(previousTab == Tab.General ? -1 : 1);
                    break;
                case Tab.Video:
                    SetView(previousTab == Tab.Audio ? 1 : -1);
                    break;
                case Tab.Audio:
                    SetView(-1);
                    break;
            }
        }
    }
}
