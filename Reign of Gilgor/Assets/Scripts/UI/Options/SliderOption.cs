﻿using UnityEngine;

namespace Client.UI.Options
{
    internal sealed class SliderOption : GenericOption<float>
    {
        internal float MaxValue = 100f;
        internal float MinValue = 0f;

        internal SliderOption(string internalName)
        {
            InternalName = internalName;
            ClientName = string.Empty;
            DefaultValue = 0f;
            DependentOn = string.Empty;
        }

        protected override float GetValue()
        {
#if UNITY_EDITOR || DEVELOPMENT_BUILD
            ValidateOption();
#endif
            return !cacheSet ? SetCache(PlayerPrefs.GetFloat(InternalName, DefaultValue)) : cachedValue;
        }

        protected override void SetValue(float value)
        {
            PlayerPrefs.SetFloat(InternalName, value);
            base.SetValue(value);
        }
    }
}