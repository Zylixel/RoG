﻿using System;
using Client.Entity.Player;
using Client.UI.Options;
using UnityEngine;

namespace Client.UI
{
    public sealed class PauseHandler : Initializable<PauseHandler>
    {
        [SerializeField]
        private UnityEngine.UI.Button characterButton;

        [SerializeField]
        private UnityEngine.UI.Button inventoryButton;

        [SerializeField]
        private UnityEngine.UI.Button optionsButton;

        [SerializeField]
        private UnityEngine.UI.Button returnButton;

        public override void Initialize()
        {
            Instance = this;

            AddListeners();
            Close();
        }

        private void AddListeners()
        {
            returnButton.onClick.AddListener(() => Close());
            optionsButton.onClick.AddListener(() =>
            {
                Close();
                OptionsScreen.Instance.Open();
            });
            inventoryButton.onClick.AddListener(() =>
            {
                Close();
                InventoryDisplay.Instance.Open();
            });
            characterButton.onClick.AddListener(() =>
            {
                Close();
                InterSceneData.GoToTitleScreen();
            });

            InputMaster.SubscribeToKeyPress(KeyCode.Escape, EscapePressed);
        }

        private void EscapePressed(object sender, EventArgs e)
        {
            if (gameObject.activeSelf)
            {
                Close();
            }
            else if (InputMaster.InGameplay())
            {
                Open();
            }
        }

        internal void Open()
        {
            gameObject.SetActive(true);
            InputMaster.Focus = typeof(PauseHandler);
        }

        internal void Close()
        {
            gameObject.SetActive(false);
            InputMaster.Focus = typeof(Client.Game.Map.Map);
        }
    }
}