﻿using System.Collections;
using System.Linq;
using Client.Entity;
using Client.Entity.Player;
using Client.Networking;
using UnityEngine;

namespace Client.UI
{
    internal sealed class PlayerListHandler : MonoBehaviour
    {
        [SerializeField]
        private PlayerListHitbox[] hitboxes;

        private Player owner;

        private void Awake() => GameWorldManager.PlayerCreated.Begin += (player) => owner = player;

        private void OnEnable() => StartCoroutine(UpdateLoop());

        private void OnDisable() => StopAllCoroutines();

        private IEnumerator UpdateLoop()
        {
            while (true)
            {
                yield return new WaitForSeconds(0.75f);
                UpdateList();
            }
        }

        private void UpdateList()
        {
            if (owner == null)
            {
                return;
            }

            var i = 0;
            var lockedPlayers = owner.Manager.Map.GetAllEntitiesOrdered(p => p is not Player, false)
                .OfType<ServerPlayer>().Where(sp => AccountListHandler.AccountLocked(sp.AccountId));

            foreach (var player in lockedPlayers)
            {
                if (i >= hitboxes.Length)
                {
                    break;
                }

                hitboxes[i].SetPlayer(player);
                i++;
            }

            if (i < hitboxes.Length)
            {
                var nearbyPlayers = owner.Manager.Map
                    .GetNearbyEntitiesOrdered(30f, p => p is not Player && !lockedPlayers.Contains(p), false)
                    .OfType<ServerPlayer>();

                foreach (var player in nearbyPlayers)
                {
                    if (i >= hitboxes.Length)
                    {
                        break;
                    }

                    hitboxes[i].SetPlayer(player);
                    i++;
                }
            }

            // Disable slots that were not filled
            for (; i < hitboxes.Length; i++)
            {
                hitboxes[i].SetPlayer(null);
            }
        }
    }
}