﻿using Client.Entity;
using Client.Entity.Player;
using Client.UI.Tooltip;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Client.UI
{
    internal sealed class PlayerListHitbox : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
    {
        [SerializeField]
        private Image icon;

        internal Player MapPlayer;

        [SerializeField]
        private TextMeshProUGUI nameText;

        private ServerPlayer player;

        private void Awake()
        {
            MapPlayer = GameWorldManager.PlayerCreated.LastValue;
            GameWorldManager.PlayerCreated.Begin += (player) => MapPlayer = player;
        }

        public void OnPointerClick(PointerEventData data) => PlayerTooltip.Display(player);

        public void OnPointerEnter(PointerEventData data) => PlayerTooltipPreview.Display(player);

        public void OnPointerExit(PointerEventData data) => PlayerTooltipPreview.CloseTooltip();

        internal void SetPlayer(ServerPlayer player)
        {
            if (player == null)
            {
                icon.gameObject.SetActive(false);
            }
            else
            {
                this.player = player;
                icon.gameObject.SetActive(true);
                icon.sprite = player.ObjectDesc.Textures[4];
                nameText.text = player.Name;
                nameText.color = GameColors.GetOtherPlayerColor(MapPlayer, player);
            }
        }
    }
}