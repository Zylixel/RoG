using System.Linq;
using UnityEngine;

namespace Client.UI.Positioner
{
    internal class CirclePositioner : Positioner
    {
        [SerializeField]
        protected float radius = 0;
        [SerializeField]
        protected float offset = 0;

        internal override void PositionElements()
        {
            var children = GetElements().ToList();

            float currentRad = offset * Mathf.Deg2Rad;
            float deltaRad = (2 * Mathf.PI) / children.Count;

            foreach (Transform child in children)
            {
                child.localPosition = new Vector3(Mathf.Cos(currentRad), Mathf.Sin(currentRad), 0) * radius;
                currentRad += deltaRad;
            }
        }
    }
}