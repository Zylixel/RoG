using System.Linq;
using UnityEngine;

namespace Client.UI.Positioner
{
    internal sealed class ClassCirclePositioner : CirclePositioner
    {
        internal override void PositionElements()
        {
            var elements = GetElements().ToList();

            var currentRad = offset * Mathf.Deg2Rad;
            var deltaRad = (2 * Mathf.PI) / elements.Count;

            var filledPercent = elements.Count / (float)RoGCore.Models.Class.Count();
            var radius = this.radius * Mathf.Min(filledPercent * 2, 1);
            radius = elements.Count == 1 ? 0 : radius;

            foreach (Transform child in elements)
            {
                child.localPosition = new Vector3(Mathf.Cos(currentRad), Mathf.Sin(currentRad), 0) * radius;
                currentRad += deltaRad;
            }
        }
    }
}