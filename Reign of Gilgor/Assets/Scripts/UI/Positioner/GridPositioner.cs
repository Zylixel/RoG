using UnityEngine;

namespace Client.UI.Positioner
{
    internal sealed class GridPositioner : Positioner
    {
        [SerializeField]
        private int maxPerColumn = 2;
        [SerializeField]
        private float columnSpacing = 2;
        [SerializeField]
        private float rowSpacing = 2;

        internal override void PositionElements()
        {
            int columnPos = 0;
            int row = 0;

            foreach (Transform element in GetElements())
            {
                element.localPosition = new Vector3(columnPos * columnSpacing, row * rowSpacing, 0);

                if (++columnPos >= maxPerColumn)
                {
                    columnPos = 0;
                    row++;
                }
            }
        }
    }
}