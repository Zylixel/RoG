﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Client.UI.Positioner
{
    internal abstract class Positioner : MonoBehaviour
    {
        internal abstract void PositionElements();

        internal virtual IEnumerable<Transform> GetElements()
        {
            return GetComponentsInChildren<Transform>().Where(x => x.parent == transform);
        }

        private void OnValidate()
        {
            PositionElements();
        }
    }
}
