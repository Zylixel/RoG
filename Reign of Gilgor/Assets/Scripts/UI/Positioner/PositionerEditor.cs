#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;

namespace Client.UI.Positioner
{
    [CustomEditor(typeof(Positioner), true)]
    public class PositionerEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            Positioner myTarget = (Positioner)target;

            if (GUILayout.Button("Force Reposition"))
            {
                myTarget.PositionElements();
            }
        }
    }
}

#endif