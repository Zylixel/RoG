﻿using Client.Entity.Player;
using Client.Game;
using Client.Structures;
using Client.UI.Titlecard;
using DG.Tweening;
using Microsoft.Extensions.Logging;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

namespace Client
{
    internal sealed class PostEffects : Initializable<PostEffects>
    {
        private Vignette vignette;
        private Bloom bloom;
        private readonly bool inTitleCard;

        public override void Initialize()
        {
            if (Instance == null)
            {
                Instance = this;
                var volume = GetComponent<Volume>();
                volume.sharedProfile.TryGet(out vignette);
                volume.sharedProfile.TryGet(out bloom);
            }
            else
            {
                Logger.LogWarning("Only one PostEffects should be active at once");
            }

            TitlecardEvent.Begin += (_) => OnTitleCardStart();
            TitlecardEvent.End += OnTitleCardComplete;
            OptionsHandler.SubscribeToOptionsChange<ShaderQuality>("graphicsQuality", GraphicsQualityChanged, true);
        }

        private void GraphicsQualityChanged(object sender, ShaderQuality quality)
        {
            bloom.active = (int)quality >= (int)ShaderQuality.Medium;
            bloom.highQualityFiltering.Override(quality == ShaderQuality.High);
        }

        internal static void OnUpdate(Player stats)
        {
            if (Instance.inTitleCard)
            {
                return;
            }

            Instance.vignette.color.Override(Color.Lerp(Color.black, Color.red, 1 - (stats.HP / (float)stats.MaxHP)));
        }

        private static void OnTitleCardStart() => DOTween.To(() => Instance.vignette.intensity.value, x => Instance.vignette.intensity.Override(x), 0.4f, 1);

        private static void OnTitleCardComplete() => DOTween.To(() => Instance.vignette.intensity.value, x => Instance.vignette.intensity.Override(x), 0.1f, 1);
    }
}