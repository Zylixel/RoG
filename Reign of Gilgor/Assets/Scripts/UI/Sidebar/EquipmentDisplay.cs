﻿using System.Linq;
using Client.Entity;
using Client.Entity.Helpers;
using Client.Entity.Player;
using Client.Networking;
using Client.UI.Dialog;
using Client.UI.Item;
using Client.UI.Vault;
using Client.Utils;
using DG.Tweening;
using RoGCore.Models.ObjectSlot;
using RoGCore.Networking.Packets;
using RoGCore.Utils;
using UnityEngine;
using UnityEngine.UI;

#nullable enable

namespace Client.UI.Sidebar
{
    internal sealed class EquipmentDisplay
    {
        private const int DEFAULT_MOVE_DISTANCE = 125;
        private const int CONTAINER_MOVE_DISTANCE = 250;

        private static readonly Vector2 WEAPON_INV_POS = new Vector2(-405f, -160f + 300f);
        private static readonly Vector2 ABILITY_INV_POS = new Vector2(-405f, -240f + 300f);
        private static readonly Vector2 GARMENT_INV_POS = new Vector2(-405f, -320f + 300f);
        private static readonly Vector2 RING_INV_POS = new Vector2(-405f, -400f + 300f);
        private static readonly Vector2 WEAPON_EQ_POS = new Vector2(-120f, -50f - 182f);
        private static readonly Vector2 ABILITY_EQ_POS = new Vector2(-40f, -50f - 182f);
        private static readonly Vector2 GARMENT_EQ_POS = new Vector2(40f, -50f - 182f);
        private static readonly Vector2 RING_EQ_POS = new Vector2(120f, -50f - 182f);

        private readonly Image background;
        private readonly Transform sidebarHolder;
        private readonly ItemHolder garmentSlot;
        private readonly ItemHolder ringSlot;
        private readonly ItemHolder weaponSlot;
        private readonly ItemHolder abilitySlot;
        private readonly Player self;

        internal EquipmentDisplay(Player self)
        {
            this.self = self;
            background = GameObject.Find("EquipmentBackground").GetComponent<Image>();
            sidebarHolder = GameObject.Find("SidebarHolder").transform;
            weaponSlot = GameObject.Find("WeaponSlot").GetComponent<ItemHolder>();
            weaponSlot.Index = 0;
            abilitySlot = GameObject.Find("AbilitySlot").GetComponent<ItemHolder>();
            abilitySlot.Index = 1;
            garmentSlot = GameObject.Find("GarmentSlot").GetComponent<ItemHolder>();
            garmentSlot.Index = 2;
            ringSlot = GameObject.Find("RingSlot").GetComponent<ItemHolder>();
            ringSlot.Index = 3;
            InventoryDisplayClose();
        }

        internal void OnUpdate()
        {
            var slotTypes = self.ObjectDesc.SlotTypes;
            if (slotTypes is null)
            {
                return;
            }

            weaponSlot.Set(self.Inventory[weaponSlot.Index], self.Manager, self.Manager.GameData.GetSprite("BlankEquipment", EquipmentOutlines.GetIndex(slotTypes[weaponSlot.Index])));
            abilitySlot.Set(self.Inventory[abilitySlot.Index], self.Manager, self.Manager.GameData.GetSprite("BlankEquipment", EquipmentOutlines.GetIndex(slotTypes[abilitySlot.Index])));
            garmentSlot.Set(self.Inventory[garmentSlot.Index], self.Manager, self.Manager.GameData.GetSprite("BlankEquipment", EquipmentOutlines.GetIndex(slotTypes[garmentSlot.Index])));
            ringSlot.Set(self.Inventory[ringSlot.Index], self.Manager, self.Manager.GameData.GetSprite("BlankEquipment", EquipmentOutlines.GetIndex(slotTypes[ringSlot.Index])));
        }

        internal void InventoryDisplayOpen()
        {
            weaponSlot.transform.SetParent(InventoryDisplay.GetTransform());
            garmentSlot.transform.SetParent(InventoryDisplay.GetTransform());
            ringSlot.transform.SetParent(InventoryDisplay.GetTransform());
            abilitySlot.transform.SetParent(InventoryDisplay.GetTransform());
            weaponSlot.transform.localScale = Vector3.one;
            garmentSlot.transform.localScale = Vector3.one;
            abilitySlot.transform.localScale = Vector3.one;
            ringSlot.transform.localScale = Vector3.one;
            weaponSlot.transform.DOLocalMove(WEAPON_INV_POS, 0.25f);
            garmentSlot.transform.DOLocalMove(GARMENT_INV_POS, 0.25f);
            ringSlot.transform.DOLocalMove(RING_INV_POS, 0.25f);
            abilitySlot.transform.DOLocalMove(ABILITY_INV_POS, 0.25f);
        }

        internal void InventoryDisplayClose()
        {
            weaponSlot.transform.SetParent(sidebarHolder);
            garmentSlot.transform.SetParent(sidebarHolder);
            ringSlot.transform.SetParent(sidebarHolder);
            abilitySlot.transform.SetParent(sidebarHolder);
            weaponSlot.transform.localScale = Vector3.one;
            garmentSlot.transform.localScale = Vector3.one;
            abilitySlot.transform.localScale = Vector3.one;
            ringSlot.transform.localScale = Vector3.one;
            weaponSlot.transform.DOLocalMove(WEAPON_EQ_POS, 0.25f);
            garmentSlot.transform.DOLocalMove(GARMENT_EQ_POS, 0.25f);
            ringSlot.transform.DOLocalMove(RING_EQ_POS, 0.25f);
            abilitySlot.transform.DOLocalMove(ABILITY_EQ_POS, 0.25f);
        }

        internal void Tick()
        {
            if (InputMaster.Focus == typeof(InventoryDisplay))
            {
                foreach (var container in self.Manager.Map.GetInteractablesUnsorted().OfType<Container>())
                {
                    background.gameObject.AnimateSizeDelta(new Vector2(370, 100 + CONTAINER_MOVE_DISTANCE));
                    SideDisplayManager.TryContainer(container);
                    break;
                }
            }
            else
            {
                var highlightedEntity = ResolveSideDisplayObject(out var displayingSomething);

                if (!displayingSomething)
                {
                    background.gameObject.AnimateSizeDelta(new Vector2(370, 100));
                }

                if (highlightedEntity == null)
                {
                    EntityHighlighter.UnHighlightEntities();
                }
                else
                {
                    EntityHighlighter.HighlightEntity(self, highlightedEntity);
                }
            }

            if (UnityEngine.Input.GetMouseButtonDown(0))
            {
                foreach (var itemHolder in ItemHolder.GetItemsInRaycastResults(self.Manager.Map.CurrentRaycast, new ItemHolder[] { weaponSlot, abilitySlot, garmentSlot, ringSlot }))
                {
                    InventoryDisplay.Display();

                    if (HeldItemHandler.HasItem() && HeldItemHandler.Data is HeldItemHandler.HeldItemData data)
                    {
                        if (self.AuditItem(HeldItemHandler.GetItem(), itemHolder.Index)
                            && data.Container.AuditItem(itemHolder.Item, data.SlotId)
                            && HeldItemHandler.Dispatch() is IObjectSlot objectSlot)
                        {
                            GameWorldManager.CurrentOutgoingNetwork.OutgoingMessage(new InventoryMove(
                                objectSlot,
                                new ContainerSlot(self.Id, itemHolder.Index)));
                        }
                    }
                    else if (itemHolder.Item is not null)
                    {
                        HeldItemHandler.SetItem(self, itemHolder.Index, itemHolder.Sprite);
                    }

                    // TODO: Why is this here
                    break;
                }
            }
        }

        private SimpleEntity? ResolveSideDisplayObject(out bool displayed)
        {
            displayed = false;
            var guildInvite = InvitedToGuildHandler.Peek();
            if (guildInvite is not null)
            {
                background.gameObject.AnimateSizeDelta(new Vector2(370, 100 + DEFAULT_MOVE_DISTANCE));
                SideDisplayManager.TryDefault($"Guild Invite from {guildInvite.Value.GuildName}", "Accept", "Decline",
                    () => InvitedToGuildHandler.Accept(), () => InvitedToGuildHandler.Decline());
                displayed = true;
                return null;
            }

            foreach (var entity in self.Manager.Map.GetInteractablesSorted())
            {
                if (entity is Container && SideDisplayManager.TryContainer(entity))
                {
                    background.gameObject.AnimateSizeDelta(new Vector2(370, 100 + CONTAINER_MOVE_DISTANCE));
                    displayed = true;
                }
                else if (entity is Portal && SideDisplayManager.TryDefault(entity.ObjectDesc.DungeonName, "Enter", () => GameWorldManager.CurrentOutgoingNetwork.OutgoingMessage(new UsePortal(entity.Id))))
                {
                    background.gameObject.AnimateSizeDelta(new Vector2(370, 100 + DEFAULT_MOVE_DISTANCE));
                    displayed = true;
                }
                else if (entity.ObjectDesc.DisplayName is not null && entity.ObjectDesc.DisplayName.Equals("Old Blacksmith") &&
                    SideDisplayManager.TryDefault(entity.ObjectDesc.DisplayName, "Talk", () =>
                    {
                        QuickDialog.Display(
                            entity.ObjectDesc.Name,
                            "Greetings Traveler! I hear you've been collecting broken equipment from around the Realm... Although they may look dull, they hold immense power. For 10 <sprite=6> I will polish all of your equipment for you.",
                            "Polish 10 <sprite=6>",
                            () => GameWorldManager.CurrentOutgoingNetwork.OutgoingMessage(new PolishItems(entity.Id)),
                            DialogResultConversions.OldBlacksmithConversion, true);
                    }))
                {
                    background.gameObject.AnimateSizeDelta(new Vector2(370, 100 + DEFAULT_MOVE_DISTANCE));
                    displayed = true;
                }
                else if (entity.ObjectDesc.Name.Equals("Vault Terminal") && SideDisplayManager.TryDefault(entity.ObjectDesc.Name, "Open", () => VaultDisplay.Display()))
                {
                    background.gameObject.AnimateSizeDelta(new Vector2(370, 100 + DEFAULT_MOVE_DISTANCE));
                    displayed = true;
                }
                else if (entity.ObjectDesc.Name.Equals("Character Painter") && SideDisplayManager.TryDefault(entity.ObjectDesc.Name, "Open", () => CharacterPainterUI.Display(self)))
                {
                    background.gameObject.AnimateSizeDelta(new Vector2(370, 100 + DEFAULT_MOVE_DISTANCE));
                    displayed = true;
                }
                else if (entity.ObjectDesc.Name.Equals("Skill Manager") && SideDisplayManager.TryDefault(entity.ObjectDesc.Name, "Open", () => SkillTreeUI.Display(self)))
                {
                    background.gameObject.AnimateSizeDelta(new Vector2(370, 100 + DEFAULT_MOVE_DISTANCE));
                    displayed = true;
                }
                else if (entity.ObjectDesc.Name.Equals("Guild Registry"))
                {
                    background.gameObject.AnimateSizeDelta(new Vector2(370, 100 + DEFAULT_MOVE_DISTANCE));
                    if (self.GuildName.IsNullOrWhiteSpace())
                    {
                        SideDisplayManager.TryDefault(entity.ObjectDesc.Name, "Register", () => GuildRegisterDialog.Display());
                    }
                    else
                    {
                        SideDisplayManager.TryDefault(
                            self.GuildName,
                            "Leave",
                            () =>
                            {
                                QuickDialog.Display(
                                    "Leave Guild",
                                    $"Are you sure you want to leave <b>{self.GuildName}</b>?",
                                    "Leave",
                                    () => GameWorldManager.CurrentOutgoingNetwork.OutgoingMessage(new GuildRemove(self.Name)),
                                    true);
                            });
                    }

                    displayed = true;
                }

                if (displayed)
                {
                    return entity;
                }
            }

            return null;
        }
    }
}