﻿using System;
using Client.Audio;
using Client.Entity;
using Client.Game;
using Client.UI.Item;
using DG.Tweening;
using Structures;
using UnityEngine;
using UnityEngine.Events;

namespace Client.UI.Sidebar
{
    internal sealed class SideDisplayManager : Initializable<SideDisplayManager>
    {
        internal ActiveSideDisplay CurrentEnabled;

        private UnityAction callback;

        [SerializeField]
        private GenericInventory container;

        [SerializeField]
        private SidebarButtonGroup allUseGroup;

        [SerializeField]
        private SidebarButtonGroup allUseGroup2;

        private ActiveSideDisplay lastEnabled;

        public override void Initialize()
        {
            Instance = this;

            InputMaster.SubscribeToKeyPress(OptionsHandler.GetKeyCodeOption("interactKey"), InteractPressed);
        }

        // Should occur before entity tick
        private void Update()
        {
            lastEnabled = CurrentEnabled;
            CurrentEnabled = ActiveSideDisplay.None;
        }

        private void LateUpdate()
        {
            if (lastEnabled != CurrentEnabled)
            {
                switch (lastEnabled)
                {
                    case ActiveSideDisplay.Container:
                        DOTween.To(() => Instance.container.Alpha, x => Instance.container.Alpha = x, 0, 0.25f);
                        break;
                    case ActiveSideDisplay.AllUseButton:
                        DOTween.To(() => Instance.allUseGroup.Alpha, x => Instance.allUseGroup.Alpha = x, 0, 0.25f);
                        allUseGroup.RemoveSubscribers();
                        break;
                    case ActiveSideDisplay.AllUseButton2:
                        DOTween.To(() => Instance.allUseGroup2.Alpha, x => Instance.allUseGroup2.Alpha = x, 0, 0.25f);
                        allUseGroup2.RemoveSubscribers();
                        break;
                }

                switch (CurrentEnabled)
                {
                    case ActiveSideDisplay.Container:
                        DOTween.To(() => Instance.container.Alpha, x => Instance.container.Alpha = x, 1, 0.25f);
                        break;
                    case ActiveSideDisplay.AllUseButton:
                        DOTween.To(() => Instance.allUseGroup.Alpha, x => Instance.allUseGroup.Alpha = x, 1, 0.25f);
                        break;
                    case ActiveSideDisplay.AllUseButton2:
                        DOTween.To(() => Instance.allUseGroup2.Alpha, x => Instance.allUseGroup2.Alpha = x, 1, 0.25f);
                        break;
                    case ActiveSideDisplay.None:
                        callback = null;
                        break;
                }
            }
        }

        private void InteractPressed(object sender, EventArgs e)
        {
            if (InputMaster.Focus != typeof(Client.Game.Map.Map) || callback == null)
            {
                return;
            }

            AudioHandler.PlaySound("Fantasy/Special Click 03", 0.33f);
            callback.Invoke();
        }

        internal static bool TryDefault(string tooltipText, string buttonText, UnityAction callback)
        {
            Instance.CurrentEnabled = ActiveSideDisplay.AllUseButton;

            Instance.callback = callback;

            Instance.allUseGroup.Set(tooltipText, buttonText);
            Instance.allUseGroup.RemoveSubscribers();
            Instance.allUseGroup.AddSubscribers(() => callback?.Invoke());

            return true;
        }

        internal static bool TryDefault(string tooltipText, string buttonaText, string buttonbText, UnityAction callbacka,
            UnityAction callbackb)
        {
            Instance.CurrentEnabled = ActiveSideDisplay.AllUseButton2;

            Instance.callback = null;

            Instance.allUseGroup2.Set(tooltipText, buttonaText, buttonbText);
            Instance.allUseGroup2.RemoveSubscribers();
            Instance.allUseGroup2.AddSubscribers(callbacka, callbackb);

            return true;
        }

        internal static bool TryContainer(SimpleEntity container)
        {
            if (container is not IContainer)
            {
                return false;
            }

            Instance.callback = null;

            Instance.CurrentEnabled = ActiveSideDisplay.Container;
            Instance.container.Set(container);
            return true;
        }
    }
}