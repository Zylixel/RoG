using Client.UI.Button;
using Microsoft.Extensions.Logging;
using RoGCore.Logging;
using UnityEngine;
using UnityEngine.UI;

#nullable enable

namespace Client.UI.Sidebar
{
    [RequireComponent(typeof(CanvasGroup))]
    internal sealed class SidebarButtonGroup : MonoBehaviour
    {
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
        [SerializeField]
        private EnhancedButton[] buttons;
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

        [SerializeField]
        private Text? tooltip;

        [SerializeField]
        private bool autoDisable;

#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
        private CanvasGroup canvasGroup;
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

        private static readonly ILogger<SidebarButtonGroup> Logger = LogFactory.LoggingInstance<SidebarButtonGroup>();

        internal float Alpha
        {
            get => canvasGroup.alpha;
            set
            {
                canvasGroup.alpha = value;

                if (autoDisable)
                {
                    gameObject.SetActive(value > 0);
                }
            }
        }

        private void Awake()
        {
            canvasGroup = GetComponent<CanvasGroup>();

            if (autoDisable)
            {
                gameObject.SetActive(Alpha > 0);
            }
        }

        private void OnValidate()
        {
            if (buttons is null || buttons.Length == 0)
            {
                Logger.LogError($"GameObject: {gameObject.name}, Buttons value cannot be empty");
            }
        }

        internal void Set(string tooltip, params string[] buttonTexts)
        {
            if (buttonTexts.Length != buttons.Length)
            {
                Logger.LogError($"GameObject: {gameObject.name}, Parameter Mismatct between buttonTexts and buttons in Set");
            }

            for (var i = 0; i < buttons.Length; i++)
            {
                buttons[i].SetText(buttonTexts[i]);
            }

            if (this.tooltip != null)
            {
                this.tooltip.text = tooltip;
            }
        }

        internal void AddSubscribers(params UnityEngine.Events.UnityAction[] callbacks)
        {
            if (callbacks.Length != buttons.Length)
            {
                Logger.LogError($"GameObject: {gameObject.name}, Parameter Mismatct between callbacks and buttons in AddSubscribers");
            }

            for (var i = 0; i < buttons.Length; i++)
            {
                buttons[i].onClick.AddListener(callbacks[i]);
            }
        }

        internal void RemoveSubscribers()
        {
            for (var i = 0; i < buttons.Length; i++)
            {
                buttons[i].onClick.RemoveAllListeners();
            }
        }
    }
}
