﻿using System.Collections.Generic;
using DG.Tweening;
using RoGCore.Utils;
using TMPro;
using UnityEngine;

#nullable enable

namespace Client.UI
{
    internal class SilverDisplay
    {
        private readonly GameObject icon;
        private readonly TextMeshProUGUI textComponent;

        private int amount;
        private List<GameObject>? statusPointers;
        private TextMeshProUGUI? statusText;
        private Sequence? currentSequence;

        internal SilverDisplay(Transform parent, Vector2 position)
        {
            icon = Object.Instantiate(GamePrefabs.SilverCoin, parent);
            icon.transform.localPosition = position;

            textComponent = icon.GetComponentInChildren<TextMeshProUGUI>(true);

            amount = -1;
        }

        internal bool Enabled
        {
            get => icon.activeSelf;
            set
            {
                icon.SetActive(value);
                textComponent.gameObject.SetActive(value);
                if (!value)
                {
                    OnDisable();
                }
            }
        }

        internal void SetAmount(int amount, bool showChange)
        {
            if (amount == this.amount)
            {
                return;
            }

            var lastAmount = this.amount;
            this.amount = amount;
            textComponent.text = this.amount.ToString();
            if (showChange && lastAmount != -1 && textComponent.gameObject.activeInHierarchy)
            {
                if (statusText == null)
                {
                    StatusText(this.amount - lastAmount);
                }
                else
                {
                    var newPrice = statusText.text.TrimStart('+').IntParseFast() + (this.amount - lastAmount);
                    statusText.text = (newPrice > 0 ? "+" : string.Empty) + newPrice;
                }
            }
        }

        private void StatusText(int changedPrice)
        {
            if (statusPointers is null)
            {
                statusPointers = new List<GameObject>();
            }

            var go = Object.Instantiate(GamePrefabs.SilverStatusText, textComponent.gameObject.transform);
            statusText = go.GetComponent<TextMeshProUGUI>();
            statusText.text = (changedPrice > 0 ? "+" : string.Empty) + changedPrice;
            statusText.color = changedPrice > 0 ? Color.green : Color.red;

            statusText.transform.localPosition = Vector3.zero;
            statusText.transform.localScale = Vector2.one;

            statusPointers.Add(go);

            var status = statusText;

            var prefferedX = textComponent.GetPreferredValues().x + 20;
            currentSequence = DOTween.Sequence()
                .Append(status.transform.DOLocalMoveX(-prefferedX, 0.33f))
                .Join(status.DOFade(1f, 0.33f))
                .AppendInterval(1f)
                .AppendCallback(() => statusText = null)
                .Append(status.DOFade(0f, 0.33f))
                .AppendCallback(() =>
                {
                    statusPointers.Remove(go);
                    Object.Destroy(go);
                });
        }

        internal int GetAmount() => amount;

        internal void SetTextColor(Color color) => textComponent.color = color;

        internal void SetScale(float xScale, float yScale) => icon.transform.localScale = new Vector3(xScale, yScale, 1);

        private void OnDisable()
        {
            currentSequence?.Kill(false);

            if (statusPointers is not null)
            {
                foreach (var pointer in statusPointers)
                {
                    Object.Destroy(pointer);
                }
            }

            Object.Destroy(statusText);
        }
    }
}