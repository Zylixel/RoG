﻿using Client.UI;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class SkillConnector : MonoBehaviour
{
    private Image image;
    private SkillNode next;
    private SkillNode prev;

    private void SetPrevious(SkillNode value)
    {
        prev = value;
        value.Connections.Add(this);
    }

    private void SetNext(SkillNode value)
    {
        next = value;
        value.Connections.Add(this);
    }

    internal static SkillConnector Create(GameObject gameObject, SkillNode previous, SkillNode next)
    {
        var skillConnector = gameObject.AddComponent<SkillConnector>();
        skillConnector.SetPrevious(previous);
        skillConnector.SetNext(next);
        skillConnector.image = gameObject.GetComponent<Image>();

        SkillTreeUI.SkillTreeUpdated.Begin += (_) => skillConnector.OnNodesChange();

        return skillConnector;
    }

    internal void OnNodesChange() => image.color = prev.Active && next.GetEligibility()
            ? next.Active == true ? GameColors.SKILL_CONNECTOR_ENABLED : Color.white
            : GameColors.SKILL_DISABLED;
}