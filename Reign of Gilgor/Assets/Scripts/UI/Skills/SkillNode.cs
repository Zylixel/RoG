﻿using System;
using System.Collections.Generic;
using System.Linq;
using Client.UI;
using Client.UI.Skills;
using Microsoft.Extensions.Logging;
using RoGCore.Logging;
using RoGCore.Models;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using static SkillTree;

public sealed class SkillNode : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    internal readonly List<SkillConnector> Connections = new List<SkillConnector>();
    internal bool Active;
    internal Button Button;
    internal Image Icon;
    private Image image;
    private Dictionary<Skill, SkillNode> lookupTable;

    internal List<SkillNode> Next;
    private SkillNode[] requirements;
    private SkillNode[] incompatibilities;

    // Type should only be null in a testing environment
    internal Skill Skill;
    private SkillTreeUI tree;

    private static readonly ILogger<SkillNode> Logger = LogFactory.LoggingInstance<SkillNode>();

    public void OnPointerEnter(PointerEventData data)
    {
        if (tree.ProcessingData)
        {
            return;
        }

#if UNITY_EDITOR || DEVELOPMENT_BUILD
        if (Skill == null)
        {
            return;
        }
#endif
        SkillTooltipHandler.Open(this);
    }

    public void OnPointerExit(PointerEventData data) => SkillTooltipHandler.CloseTooltip();

    internal void Init(Dictionary<Skill, SkillNode> lookupTable)
    {
        this.lookupTable = lookupTable;
        tree = GetComponentsInParent<SkillTreeUI>(true)[0];
        Button = gameObject.GetComponent<Button>();
        image = gameObject.GetComponent<Image>();
        Icon = gameObject.GetComponentsInChildren<Image>().First(x => x.name == "SkillIcon");
        if (Enum.TryParse(gameObject.name, out SkillType st))
        {
            if (SkillLookup.TryGetValue(st, out Skill))
            {
                lookupTable[Skill] = this;
            }
            else
            {
                Logger.LogError($"Couldn't find skill {st} in the skill dictionary");
                return;
            }
        }
        else
        {
            Logger.LogError($"Couldn't find Skill with the name {gameObject.name}");
            Skill = new Skill();
        }

        Button.interactable = false;
        Button.onClick.AddListener(delegate
        {
            if (Active)
            {
                Active = false;

                // Test to see if any next skills would no longer meet requirements
                if (Next.Any(skill => skill.Active && !skill.GetEligibility()))
                {
                    Active = true;
                    return;
                }

                tree.RemoveSelectedSkill(Skill);
            }
            else if (tree.AvailablePoints >= Skill.SkillPointCost)
            {
                Active = true;
                tree.AddSelectedSkill(Skill);
            }
            else
            {
                return;
            }
        });
    }

    internal void ResolveSiblings()
    {
        Next = new List<SkillNode>();

        if (Skill == null)
        {
            return;
        }

        if (Skill.Requirements is not null)
        {
            requirements = ParseSkillList(Skill.Requirements);
        }

        if (Skill.Incompatibilities is not null)
        {
            incompatibilities = ParseSkillList(Skill.Incompatibilities);
        }

        foreach (var nextSkill in Skill.Next)
        {
            if (lookupTable.TryGetValue(nextSkill, out var node))
            {
                Next.Add(node);
            }
        }

        Update();
    }

    private SkillNode[] ParseSkillList(IList<SkillType> skills)
    {
        var ret = new SkillNode[skills.Count];
        for (var i = 0; i < skills.Count; i++)
        {
#if UNITY_EDITOR || DEVELOPMENT_BUILD
            try
#endif
            {
                ret[i] = lookupTable[SkillLookup[skills[i]]];
            }
#if UNITY_EDITOR || DEVELOPMENT_BUILD
            catch (Exception ex)
            {
                Logger.LogError(ex, null);
            }
#endif
        }

        return ret;
    }

    internal void OnServerData()
    {
#if UNITY_EDITOR || DEVELOPMENT_BUILD
        if (Skill == null)
        {
            return;
        }
#endif
        Active = tree.SelectedSkills.Contains(Skill);

        Update();
    }

    internal bool GetEligibility() => Skill is not null
&& tree.AvailablePoints >= Skill.SkillPointCost
&& (requirements is null || requirements.Any(req => req.Active))
&& (incompatibilities is null || !incompatibilities.Any(incompatibility => incompatibility.Active));

    private void Update()
    {
        var eligible = GetEligibility();

        Button.interactable = eligible;
        image.color = Active ? GameColors.SKILL_ENABLED : eligible ? Color.gray : GameColors.SKILL_DISABLED;
    }
}