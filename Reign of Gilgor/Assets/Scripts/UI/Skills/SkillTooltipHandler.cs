﻿using Client.UI.Blur;
using Client.UI.Tooltip;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Client.UI.Skills
{
    internal sealed class SkillTooltipHandler : AdvancedBaseTooltip<SkillTooltipHandler>
    {
        [SerializeField]
        private TextMeshProUGUI description;

        [SerializeField]
        private Image icon;

        [SerializeField]
        private TextMeshProUGUI label;

        [SerializeField]
        private TextMeshProUGUI typeCost;

        private SkillNode currentNode;

        internal static void Open(SkillNode node)
        {
            Instance.currentNode = node;
            Instance.Open();
        }

        private void OnEnable()
        {
            if (currentNode == null)
            {
                Close();
                return;
            }

            BlurScreen.Instance.TakeScreenshot();

            icon.sprite = currentNode.Icon.sprite;
            label.text = currentNode.Skill.Name;
            typeCost.text = currentNode.Skill.SkillPointCost + " Skill Point" + (currentNode.Skill.SkillPointCost == 1 ? string.Empty : "s");
            description.text = currentNode.Skill.Description + '\n';

            if (currentNode.Skill.StatsBoost is not null)
            {
                foreach (var statBoost in currentNode.Skill.StatsBoost)
                {
                    description.text += $"<color=#DBDE64>+{statBoost.Value}</color> <color=#DBDE64>{statBoost.Key}</color>\n";
                }
            }

            if (currentNode.Skill.WeaponProjectileLifetimeMultiplier != 1)
            {
                description.text += $"<color=#DBDE64>+{(currentNode.Skill.WeaponProjectileLifetimeMultiplier - 1) * 100f}%</color> <color=#DBDE64>Weapon Shot Lifetime</color>\n";
            }

            if (currentNode.Skill.WeaponProjectileSpeedMultiplier != 1)
            {
                description.text += $"<color=#DBDE64>+{(currentNode.Skill.WeaponProjectileSpeedMultiplier - 1) * 100f}%</color> <color=#DBDE64>Weapon Shot Speed</color>\n";
            }

            if (currentNode.Skill.WeaponDamageMultiplier != 1)
            {
                description.text += $"<color=#DBDE64>+{(currentNode.Skill.WeaponDamageMultiplier - 1) * 100f}%</color> <color=#DBDE64>Weapon Damage</color>\n";
            }

            if (currentNode.Skill.WeaponRateOfFireMultiplier != 1)
            {
                description.text += $"<color=#DBDE64>+{(currentNode.Skill.WeaponRateOfFireMultiplier - 1) * 100f}%</color> <color=#DBDE64>Weapon Rate of Fire</color>\n";
            }

            description.text.TrimEnd('\n');

            Instance.Update();
        }

        internal static void CloseTooltip() => Instance.Close();
    }
}