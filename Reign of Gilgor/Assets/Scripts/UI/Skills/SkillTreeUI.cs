﻿using System.Collections.Generic;
using System.Linq;
using Client.Entity.Player;
using Client.Game.Event;
using Client.UI.Dialog;
using RoGCore.Game;
using RoGCore.Models;
using RoGCore.Networking.Packets;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using static SkillTree;

namespace Client.UI
{
    internal sealed class SkillTreeUI : BaseDialog<SkillTreeUI>
    {
        public override bool NeedsGameData() => true;

        [SerializeField]
        private UnityEngine.UI.Button closeButton;
        [SerializeField]
        private GameObject DecieverHolder;
        [SerializeField]
        private GameObject HunterHolder;
        [SerializeField]
        internal Sprite LockedSprite;
        [SerializeField]
        private TextMeshProUGUI wipText;
        [SerializeField]
        private TextMeshProUGUI skillPointCounter;

        internal static GameEvent<SkillTreeUI> SkillTreeUpdated = new GameEvent<SkillTreeUI>();

        internal Dictionary<Skill, SkillNode> DeceiverNodeLookup = new Dictionary<Skill, SkillNode>();
        internal Dictionary<Skill, SkillNode> HunterNodeLookup = new Dictionary<Skill, SkillNode>();
        internal int AvailablePoints;
        internal HashSet<Skill> SelectedSkills = new HashSet<Skill>();

        private GameObject[] holders;
        private float treeScale;

        public override void Initialize()
        {
            base.Initialize();

            closeButton.onClick.AddListener(() => Close());
            var deceiverNodes = DecieverHolder.GetComponentsInChildren<SkillNode>(true);
            foreach (var node in deceiverNodes)
            {
                node.Init(DeceiverNodeLookup);
            }

            foreach (var node in deceiverNodes)
            {
                node.ResolveSiblings();
            }

            CreateConnections(DeceiverNodeLookup, DecieverHolder);

            var hunterNodes = HunterHolder.GetComponentsInChildren<SkillNode>(true);
            foreach (var node in hunterNodes)
            {
                node.Init(HunterNodeLookup);
            }

            foreach (var node in hunterNodes)
            {
                node.ResolveSiblings();
            }

            CreateConnections(HunterNodeLookup, HunterHolder);

            holders = new GameObject[2] { HunterHolder, DecieverHolder };

            SkillTreeUpdated.Begin += (_) => CalculateAvailablePoints();

            SkillTreeUpdated.Invoke(this);
        }

        internal static HashSet<Skill> ActiveSkills() => Instance.SelectedSkills;

        internal bool HasSkill(SkillType skill) => SelectedSkills.Contains(SkillLookup[skill]);

        internal void AddSelectedSkill(Skill skill)
        {
            SelectedSkills.Add(skill);
            SkillTreeUpdated.Invoke(this);
        }

        internal void RemoveSelectedSkill(Skill skill)
        {
            SelectedSkills.Remove(skill);
            SkillTreeUpdated.Invoke(this);
        }

        internal Dictionary<Skill, SkillNode> GetNodeLookup(Player player) => player.ObjectDesc.Name switch
        {
            "Hunter" => HunterNodeLookup,
            "Deceiver" => DeceiverNodeLookup,
            _ => null
        };

        private void SetHolders(Player player)
        {
            for (var i = 0; i < holders.Length; i++)
            {
                holders[i].SetActive(false);
            }

            wipText.gameObject.SetActive(false);

            switch (player.ObjectDesc.Name)
            {
                case "Hunter":
                    {
                        HunterHolder.SetActive(true);
                        break;
                    }

                case "Deceiver":
                    {
                        DecieverHolder.SetActive(true);
                        break;
                    }

                default:
                    {
                        wipText.gameObject.SetActive(true);
                        break;
                    }
            }
        }

        internal static void Set(byte[] data) => Instance.InternalSet(data);

        private void InternalSet(byte[] data) => SelectedSkills = Read(data);

        internal static void Display(Player player)
        {
            Instance.Open();

            Instance.SetHolders(player);
            if (Instance.GetNodeLookup(player) == null)
            {
                return;
            }

            foreach (var node in Instance.GetNodeLookup(player))
            {
                node.Value.OnServerData();
            }

            SkillTreeUpdated.Invoke(Instance);
        }

        internal void CalculateAvailablePoints()
        {
            var player = GameWorldManager.Instance.Map?.Player;

            if (player == null)
            {
                AvailablePoints = 0;
                skillPointCounter.text = "Skill Points: " + AvailablePoints;
                return;
            }

            var currentPrice = SelectedSkills.Sum(skill => skill.SkillPointCost);

            AvailablePoints = SkillTreePointCalculator.AvailablePoints(player.SkillLevels) - currentPrice;
            skillPointCounter.text = "Skill Points: " + AvailablePoints;
        }

        private void OnDisable()
        {
            GameWorldManager.CurrentOutgoingNetwork.OutgoingMessage(new SetSkillTree(Write(SelectedSkills)));

            // If the gameworld is disabled and reenabled, then we want the skill tree to still be gone
            gameObject.SetActive(false);
        }

        private void Update()
        {
            if (UnityEngine.Input.GetAxis("Mouse ScrollWheel") != 0)
            {
                treeScale = Mathf.Clamp(treeScale + (UnityEngine.Input.GetAxis("Mouse ScrollWheel") * 2), 0.5f, 1.5f);

                for (var i = 0; i < holders.Length; i++)
                {
                    holders[i].transform.localScale = new Vector2(treeScale, treeScale);
                }
            }
        }

        private static void CreateConnections(Dictionary<Skill, SkillNode> lookupTable, GameObject holder)
        {
            foreach (var skillPair in lookupTable)
            {
                Vector2 skillPos = skillPair.Value.transform.localPosition;
                foreach (var next in skillPair.Value.Next)
                {
                    var go = new GameObject("Connection");
                    var image = go.AddComponent<Image>();
                    image.rectTransform.SetParent(holder.transform);
                    image.rectTransform.SetAsFirstSibling();

                    Vector2 nextPos = next.transform.localPosition;
                    var dx = nextPos.x - skillPos.x;
                    var dy = nextPos.y - skillPos.y;
                    var dist = Mathf.Sqrt((dx * dx) + (dy * dy));
                    image.rectTransform.localScale = Vector3.one;
                    image.rectTransform.localPosition = new Vector3(skillPos.x + (dx / 2f), skillPos.y + (dy / 2f), 0f);
                    image.rectTransform.eulerAngles = new Vector3(0f, 0f, Mathf.Atan2(dy, dx) * Mathf.Rad2Deg);
                    image.rectTransform.sizeDelta = new Vector2(dist, 6f);

                    SkillConnector.Create(go, skillPair.Value, next);
                }
            }
        }
    }
}