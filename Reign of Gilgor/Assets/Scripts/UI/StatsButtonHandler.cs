﻿using Client;
using Client.Entity.Player;
using RoGCore.Models;
using TMPro;
using UnityEngine;

public class StatsButtonHandler : MonoBehaviour
{
    private Player owner;

    [SerializeField]
    private GameObject background;

    private void Awake() => GameWorldManager.PlayerCreated.Begin += (player) => owner = player;

    public void PointerDown()
    {
        if (!Input.GetKey(KeyCode.LeftControl))
        {
            return;
        }

        background.SetActive(true);
        foreach (var text in background.GetComponentsInChildren<TextMeshProUGUI>())
        {
            if (text.text.Contains("Dexterity"))
            {
                text.text = "Dexterity: " + GetStat(PlayerStats.Dexterity);
                text.color =
                    owner.PlayerStats[(int)PlayerStats.Dexterity] -
                    owner.Boosts[(int)PlayerStats.Dexterity] >= owner.ObjectDesc.PlayerDexterity[1]
                        ? Color.yellow
                        : Color.white;
            }
            else if (text.text.Contains("Speed"))
            {
                text.text = "Speed: " + GetStat(PlayerStats.Speed);
                text.color =
                    owner.PlayerStats[(int)PlayerStats.Speed] -
                    owner.Boosts[(int)PlayerStats.Speed] >= owner.ObjectDesc.PlayerSpeed[1]
                        ? Color.yellow
                        : Color.white;
            }
            else if (text.text.Contains("Vitality"))
            {
                text.text = "Vitality: " + GetStat(PlayerStats.Vitality);
                text.color =
                    owner.PlayerStats[(int)PlayerStats.Vitality] -
                    owner.Boosts[(int)PlayerStats.Vitality] >= owner.ObjectDesc.PlayerVitality[1]
                        ? Color.yellow
                        : Color.white;
            }
            else if (text.text.Contains("Wisdom"))
            {
                text.text = "Wisdom: " + GetStat(PlayerStats.Wisdom);
                text.color =
                    owner.PlayerStats[(int)PlayerStats.Wisdom] -
                    owner.Boosts[(int)PlayerStats.Wisdom] >= owner.ObjectDesc.PlayerWisdom[1]
                        ? Color.yellow
                        : Color.white;
            }
            else if (text.text.Contains("Defense"))
            {
                text.text = "Defense: " + GetStat(PlayerStats.Defense);
                text.color =
                    owner.PlayerStats[(int)PlayerStats.Defense] -
                    owner.Boosts[(int)PlayerStats.Defense] >= owner.ObjectDesc.PlayerDefense[1]
                        ? Color.yellow
                        : Color.white;
            }
            else if (text.text.Contains("Attack"))
            {
                text.text = "Attack: " + GetStat(PlayerStats.Attack);
                text.color =
                    owner.PlayerStats[(int)PlayerStats.Attack] -
                    owner.Boosts[(int)PlayerStats.Attack] >= owner.ObjectDesc.PlayerAttack[1]
                        ? Color.yellow
                        : Color.white;
            }
        }
    }

    private string GetStat(PlayerStats stat) => owner.Boosts[(int)stat] > 0
            ? owner.PlayerStats[(int)stat] +
                   $" ({(owner.Boosts[(int)stat] > 0 ? "+" : string.Empty)}{owner.Boosts[(int)stat]})"
            : owner.PlayerStats[(int)stat].ToString();

    public void PointerUp() => background.SetActive(false);
}