﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TabInputNavigator : MonoBehaviour
{
    private EventSystem system;

    private void Start() => system = EventSystem.current;

    private void Update()
    {
        if (system.currentSelectedGameObject == null || !Input.GetKeyDown(KeyCode.Tab))
        {
            return;
        }

        var current = system.currentSelectedGameObject.GetComponent<Selectable>();
        if (current == null)
        {
            return;
        }

        var up = Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);
        var next = up ? current.FindSelectableOnUp() : current.FindSelectableOnDown();

        // We are at the end or the beginning, go to either, depends on the direction we are tabbing in
        // The previous version would take the logical 0 selector, which would be the highest up in your editor hierarchy
        // But not certainly the first item on your GUI, or last for that matter
        // This code tabs in the correct visual order
        if (next == null)
        {
            next = current;
            Selectable pnext;
            if (up)
            {
                while ((pnext = next.FindSelectableOnDown()) is not null)
                {
                    next = pnext;
                }
            }
            else
            {
                while ((pnext = next.FindSelectableOnUp()) is not null)
                {
                    next = pnext;
                }
            }
        }

        // Simulate Inputfield MouseClick
        var inputfield = next.GetComponent<InputField>();
        inputfield?.OnPointerClick(new PointerEventData(system));

        // Select the next item in the taborder of our direction
        system.SetSelectedGameObject(next.gameObject);
    }
}