using System.Linq;
using Client.Game;
using Client.UI.Button;
using Client.UI.Item;
using RoGCore.Models;
using RoGCore.Networking.APIViewModels;
using UnityEngine;
using UnityEngine.UI;

namespace Client.UI.TitleScreen
{
    internal sealed class CharacterButton : EnhancedButton
    {
        protected override void Start()
        {
            base.Start();

            image.material = new Material(image.material);
        }

        internal void ParseData(CharacterViewModel characterData)
        {
            var gameData = TitleScreenController.Instance.GameData;
            var characterDesc = gameData.BetterObjects[characterData.Class];

            SetText(characterDesc.Name);

            var icon = GetComponentsInChildren<Image>().First(x => x.name == "Icon");
            icon.sprite = characterDesc.Textures[0];
            icon.material = new Material(EmbeddedData.PlayerMaterial);

            ApplyColorToIcon(characterData.Tex1, icon, ShaderProperties.BigColor, ShaderProperties.UseBigColor);
            ApplyColorToIcon(characterData.Tex2, icon, ShaderProperties.SmallColor, ShaderProperties.UseSmallColor);

            var equipmentHolder = GetComponentInChildren<EquipmentHolder>().AsArray();

            for (int i = 0; i < 4; i++)
            {
                RoGCore.Models.Item? setItem = null;
                Sprite emptySprite = null;

                if (characterData.Items[i] is not null)
                {
                    if (gameData.Items.TryGetValue(characterData.Items[i].ObjectType, out var item))
                    {
                        setItem = new RoGCore.Models.Item(item, characterData.Items[i]);
                    }
                }

                if (setItem == null)
                {
                    emptySprite = gameData.GetSprite("BlankEquipment", EquipmentOutlines.GetIndex(characterDesc.SlotTypes[i]));
                }

                equipmentHolder[i].Set(setItem, emptySprite);
            }
        }

        private static void ApplyColorToIcon(int colorValue, Graphic iconImage, int colorHash, int useColorHash)
        {
            // TODO
            // Keywords?
            // iconImage.material.EnableKeyword("useColor");

            if (colorValue < 0)
            {
                iconImage.material.SetInt(useColorHash, 0);
                return;
            }

            iconImage.material.SetInt(useColorHash, 1);
            iconImage.material.SetColor(colorHash, new RGB(colorValue));
        }
    }
}