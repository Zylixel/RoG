﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Client.Networking.API;
using Client.UI.Button;
using Client.UI.Dialog;
using Client.Utils;
using DG.Tweening;
using RoGCore.Game;
using RoGCore.Models;
using RoGCore.Networking.APIViewModels;
using RoGCore.Utils;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Client.UI.TitleScreen
{
    internal sealed class CharacterSelectionPage : TitleScreenPage
    {
        [SerializeField]
        private Positioner.Positioner positioner;
        [SerializeField]
        private Sprite newClassUnlockedIcon;

        internal static CharacterSelectionPage Instance;

        private readonly List<GameObject> dynamicButtons = new List<GameObject>();

        private GeneralHeader Header => GeneralHeader.Instance;

        public override void Initialize()
        {
            Instance = this;
        }

        internal override void Enable()
        {
            gameObject.SetActive(true);

            Header.Appear();
            Header.SetText($"Loading Character Data...");
            Header.SetCloseAction(() =>
            {
                TitleScreenController.Instance.SwipeLeftTo(TitleScreenPages.Start);
                Header.Disappear();
            });

            StartCoroutine(LoadAccountData());
        }

        internal IEnumerator LoadAccountData()
        {
            var apiResponse = new CoroutineResultWrapper<AccountViewModel>();
            yield return AccountStore.AccountData.GetValue(apiResponse);
            var account = apiResponse.Value;

            Header.SetText($"Hello, {account.Name}");

            dynamicButtons.ForEach(selection => selection.SetActive(false));
            dynamicButtons.ForEach(selection => Destroy(selection));
            dynamicButtons.Clear();

            EmbeddedData.PlayerMaterial.SetColor("OutlineColor", Color.black);

            CreateCharacterButtons(account.Characters);

            var skillLevels = new PlayerSkills(account.SkillLevels);
            CreateNewCharacterButton(
                NewCharacterRequirement.AverageSkillReached(account.Characters.Count, skillLevels.AverageLevel()),
                NewCharacterRequirement.AverageSkillLevelRequired(account.Characters.Count));

            positioner.PositionElements();
        }

        private void CreateCharacterButtons(IList<CharacterViewModel> characters)
        {
            foreach (var character in characters)
            {
                var characterButton = Instantiate(GamePrefabs.TitleCharacterButton).GetComponent<CharacterButton>();
                characterButton.name = character.CharacterId.ToString();
                characterButton.onClick.AddListener(() => OnSelection(character.CharacterId));
                characterButton.ParseData(character);

                characterButton.transform.SetParent(positioner.transform);
                dynamicButtons.Add(characterButton.gameObject);
            }
        }

        private void CreateNewCharacterButton(bool active, int skillRequirement)
        {
            var newCharacterButton = Instantiate(GamePrefabs.TitleNewCharacterButton).GetComponent<EnhancedButton>();

            newCharacterButton.interactable = active;
            if (active)
            {
                newCharacterButton.SetText($"<#{ColorUtility.ToHtmlStringRGB(GameColors.GILGOR_PURPLE)}>New Class Available!");
                newCharacterButton.GetComponentsInChildren<Image>().First(x => x.gameObject.name == "Icon").sprite = newClassUnlockedIcon;
                newCharacterButton.onClick.AddListener(() => TitleScreenController.Instance.SwipeRightTo(TitleScreenPages.ClassSelection));
            }
            else
            {
                var oldText = newCharacterButton.GetTextValue();
                var replacedText = oldText.Replace("$SkillRequirement", skillRequirement.ToString());
                newCharacterButton.SetText(replacedText);
            }

            newCharacterButton.transform.SetParent(positioner.transform);
            dynamicButtons.Add(newCharacterButton.gameObject);
        }

        private void OnSelection(ushort id)
        {
            InterSceneData.CharacterId = id;
            Header.Disappear();
            TitleScreenController.Instance.StartCoroutine(LoadGame(() => Header.Appear()));
        }

        internal IEnumerator LoadGame(UnityAction onError = null)
        {
            var endpointResult = new CoroutineResultWrapper<IPEndPoint>();
            yield return GameServerStore.GetPrimaryEndpoint(endpointResult);

            if (endpointResult.IsFailed)
            {
                ErrorDialog.Display(endpointResult.Result.ErrorString(), InterSceneData.GoToTitleScreen);
                onError?.Invoke();
                yield break;
            }

            TitleScreenController.Instance.SwipeRightTo(TitleScreenPages.Blank);

            yield return DOTween.ToAlpha(() => TitleScreenController.Instance.Stars.color, x => TitleScreenController.Instance.Stars.color = x, 0, 0.33f).WaitForCompletion();

            while (TitleScreenController.Instance.Moving)
            {
                yield return null;
            }

            InterSceneData.TitleScene.SetActive(false);
            InterSceneData.GameWorldScene.SetActive(true);
        }

#if UNITY_EDITOR
        public void OnEditor() => SceneManager.LoadScene("Editor");
#endif
    }
}