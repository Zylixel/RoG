﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Client.Networking.API;
using Client.UI.Item;
using Client.Utils;
using DG.Tweening;
using RoGCore.Networking.APIViewModels;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using static Client.UI.TitleScreen.TitleScreenController;

namespace Client.UI.TitleScreen
{
    internal sealed class ClassSelectionPage : TitleScreenPage
    {
        [SerializeField]
        private List<GameObject> classButtons = new List<GameObject>();
        [SerializeField]
        private Text classDescription;
        [SerializeField]
        private UnityEngine.UI.Button closeButton;
        [SerializeField]
        private UnityEngine.UI.Button selectClassButton;
        [SerializeField]
        private Image equipmentBackground;

        internal static ClassSelectionPage Instance;

        private RectTransform previewButton;
        private Vector3 previewOriginalPostion;

        private GeneralHeader Header => GeneralHeader.Instance;

        public override void Initialize()
        {
            Instance = this;

            selectClassButton.onClick.AddListener(OnCreateClicked);
            closeButton.onClick.AddListener(OnCloseClicked);
        }

        public void ButtonClicked(GameObject button)
        {
            UpdateDescription(button);

            ViewClassPreview(button, AnimationDuration);
        }

        internal override void Enable()
        {
            gameObject.SetActive(true);

            DisableAlreadyUsedClasses();

            GetComponentInChildren<Positioner.Positioner>(true).PositionElements();

            Header.Appear();
            Header.SetText($"Select a Class");
            Header.SetCloseAction(() => StartCoroutine(Close()));
        }

        private void Start()
        {
            foreach (var button in classButtons)
            {
                button.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(() => ButtonClicked(button));
            }
        }

        private IEnumerator Close()
        {
            var newAccount = new CoroutineOutputWrapper<bool>();
            yield return AccountStore.IsNewAccount(newAccount);

            if (newAccount)
            {
                TitleScreenController.Instance.SwipeLeftTo(TitleScreenPages.Start);
                Header.Disappear();
            }
            else
            {
                TitleScreenController.Instance.SwipeLeftTo(TitleScreenPages.CharacterSelect);
            }
        }

        private IEnumerator DisableAlreadyUsedClasses()
        {
            foreach (var button in classButtons)
            {
                button.SetActive(true);
            }

            var characters = new CoroutineResultWrapper<IList<CharacterViewModel>>();
            yield return CharacterStore.CharacterData.GetValue(characters);
            foreach (var character in characters.Value)
            {
                if (TitleScreenController.Instance.GameData.BetterObjects.TryGetValue(character.Class, out var characterDesc))
                {
                    var button = classButtons.FirstOrDefault(x => x.name == characterDesc.Name);

                    if (button != null)
                    {
                        button.SetActive(false);
                    }
                }
            }
        }

        private void ViewClassPreview(GameObject button, float animationDuration)
        {
            foreach (var butt in classButtons)
            {
                var canvasGroup = butt.GetComponent<CanvasGroup>();
                canvasGroup.interactable = false;
                canvasGroup.blocksRaycasts = false;

                if (butt != button)
                {
                    canvasGroup.DOFade(0f, animationDuration);
                }
            }

            previewButton = button.GetComponent<RectTransform>();
            previewOriginalPostion = button.transform.localPosition;

            classDescription.transform.localPosition = previewOriginalPostion;
            selectClassButton.transform.localPosition = previewOriginalPostion;
            equipmentBackground.transform.localPosition = previewOriginalPostion;
            closeButton.transform.localPosition = previewOriginalPostion;

            var selectCharacterGroup = selectClassButton.GetComponent<CanvasGroup>();
            selectCharacterGroup.interactable = true;
            selectCharacterGroup.blocksRaycasts = true;

            closeButton.image.raycastTarget = true;

            var classText = button.GetComponentsInChildren<TextMeshProUGUI>().First(child => child.name == "ClassText");
            var icon = button.GetComponentsInChildren<RectTransform>().First(child => child.name == "Icon");

            DOTween.Sequence()
                .Join(previewButton.DOSizeDelta(new Vector2(800, 550), animationDuration))
                .Join(previewButton.DOLocalMove(Vector3.zero, animationDuration))
                .Join(icon.DOLocalMove(new Vector2(0, 175), animationDuration))
                .Join(icon.DOSizeDelta(new Vector2(150, 150), animationDuration))
                .Join(classText.rectTransform.DOLocalMove(new Vector2(0, 75), animationDuration))
                .Join(DOTween.To(() => classText.fontSize, x => classText.fontSize = x, 24, animationDuration))
                .Join(classDescription.DOFade(1f, animationDuration))
                .Join(classDescription.transform.DOLocalMove(new Vector2(0, 25), animationDuration))
                .Join(equipmentBackground.GetComponent<CanvasGroup>().DOFade(1f, animationDuration))
                .Join(equipmentBackground.transform.DOLocalMove(new Vector2(0, -125), animationDuration))
                .Join(closeButton.transform.DOLocalMove(new Vector2(325, 175), animationDuration))
                .Join(closeButton.image.DOFade(1f, animationDuration))
                .Join(selectCharacterGroup.DOFade(1f, animationDuration))
                .Join(selectClassButton.transform.DOLocalMove(new Vector2(0, -230), animationDuration)).SetEase(Ease.OutQuad);

            Header.ShowCloseButton(false);
        }

        public void RemoveClassPreview(float animationDuration)
        {
            foreach (var butt in classButtons)
            {
                var canvasGroup = butt.GetComponent<CanvasGroup>();
                canvasGroup.blocksRaycasts = true;
                canvasGroup.interactable = true;
                canvasGroup.DOFade(1f, animationDuration);
            }

            var selectCharacterGroup = selectClassButton.GetComponent<CanvasGroup>();
            selectCharacterGroup.interactable = false;
            selectCharacterGroup.blocksRaycasts = false;

            closeButton.image.raycastTarget = false;

            var classText = previewButton.GetComponentsInChildren<TextMeshProUGUI>().First(child => child.name == "ClassText");
            var icon = previewButton.GetComponentsInChildren<RectTransform>().First(child => child.name == "Icon");

            DOTween.Sequence()
                .Join(previewButton.DOSizeDelta(new Vector2(200, 200), animationDuration))
                .Join(previewButton.DOLocalMove(previewOriginalPostion, animationDuration))
                .Join(icon.DOLocalMove(new Vector2(0, 25), animationDuration))
                .Join(icon.DOSizeDelta(new Vector2(100, 100), animationDuration))
                .Join(classText.rectTransform.DOLocalMove(new Vector2(0, -55), animationDuration))
                .Join(DOTween.To(() => classText.fontSize, x => classText.fontSize = x, 18, animationDuration))
                .Join(classDescription.DOFade(0f, animationDuration))
                .Join(classDescription.transform.DOLocalMove(previewOriginalPostion, animationDuration))
                .Join(equipmentBackground.GetComponent<CanvasGroup>().DOFade(0f, animationDuration))
                .Join(equipmentBackground.transform.DOLocalMove(previewOriginalPostion, animationDuration))
                .Join(closeButton.transform.DOLocalMove(previewOriginalPostion, animationDuration))
                .Join(closeButton.image.DOFade(0f, animationDuration))
                .Join(selectCharacterGroup.DOFade(0f, animationDuration))
                .Join(selectClassButton.transform.DOLocalMove(previewOriginalPostion, animationDuration)).SetEase(Ease.OutQuad);

            previewButton = null;
            Header.ShowCloseButton(true);
        }

        private void UpdateDescription(GameObject button)
        {
            var gameData = TitleScreenController.Instance.GameData;

            classDescription.text = ClassData.CharacterDescriptions[button.name].Description;

            var characterDesc = gameData.BetterObjects[button.name];

            var equipmentHolder = GetComponentInChildren<EquipmentHolder>().AsArray();

            for (int i = 0; i < 4; i++)
            {
                Sprite emptySprite = gameData.GetSprite("BlankEquipment", EquipmentOutlines.GetIndex(characterDesc.SlotTypes[i]));
                equipmentHolder[i].Set(null, emptySprite);
            }
        }

        private void OnCloseClicked()
        {
            RemoveClassPreview(AnimationDuration);
        }

        private void OnCreateClicked()
        {
            InterSceneData.CharacterId = TitleScreenController.Instance.GameData.Id2TypeObject[previewButton.gameObject.name];
            InterSceneData.NewCharacter = true;

            RemoveClassPreview(0);
            Header.Disappear();

            StartCoroutine(CharacterSelectionPage.Instance.LoadGame(ErrorLoading));
        }

        private void ErrorLoading()
        {
            Enable();
        }
    }
}