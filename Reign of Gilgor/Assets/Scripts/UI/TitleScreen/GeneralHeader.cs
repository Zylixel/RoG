using DG.Tweening;
using TMPro;
using UnityEngine.Events;

namespace Client.UI.TitleScreen
{
    public class GeneralHeader : Initializable<GeneralHeader>
    {
        private UnityEngine.UI.Button closeButton;
        private TextMeshProUGUI text;
        private UnityAction currentAction;

        private Sequence currentSequence;
        private TransitionState state = TransitionState.Disappered;

        private enum TransitionState
        {
            Disappered,
            Appearing,
            Appeared,
            Disappearing,
        }

        public override void Initialize()
        {
            base.Initialize();

            closeButton = GetComponentInChildren<UnityEngine.UI.Button>(true);
            text = GetComponentInChildren<TextMeshProUGUI>(true);
        }

        internal void SetText(string input)
        {
            DOTween.To(() => text.text, x => text.text = x, input, TitleScreenController.AnimationDuration);
        }

        internal void SetCloseAction(UnityAction action)
        {
            if (currentAction != null)
            {
                closeButton.onClick.RemoveListener(currentAction);
            }

            closeButton.onClick.AddListener(action);
            currentAction = action;
        }

        internal void Appear()
        {
            if (state == TransitionState.Appearing || state == TransitionState.Appeared)
            {
                return;
            }

            if (currentSequence != null)
            {
                currentSequence.Kill();
            }

            state = TransitionState.Appearing;

            gameObject.SetActive(true);
            ShowCloseButton(true);

            currentSequence = DOTween.Sequence()
                .Append(transform.DOLocalMoveY(450, TitleScreenController.AnimationDuration));

            currentSequence.onComplete += FinishAppear;
            currentSequence.onKill += () => currentSequence = null;
        }

        internal void ShowCloseButton(bool value)
        {
            closeButton.gameObject.SetActive(value);
        }

        private void FinishAppear()
        {
            state = TransitionState.Appeared;
        }

        internal void Disappear()
        {
            if (state == TransitionState.Disappearing || state == TransitionState.Disappered)
            {
                return;
            }

            currentSequence?.Kill();

            state = TransitionState.Disappearing;

            SetText(string.Empty);

            currentSequence = DOTween.Sequence()
                .Append(transform.DOLocalMoveY(600, TitleScreenController.AnimationDuration));

            currentSequence.onComplete += FinishDisappear;
            currentSequence.onKill += () => currentSequence = null;
        }

        private void FinishDisappear()
        {
            gameObject.SetActive(false);
            state = TransitionState.Disappered;
        }
    }
}