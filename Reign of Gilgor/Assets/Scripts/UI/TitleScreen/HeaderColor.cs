﻿using UnityEngine;
using UnityEngine.UI;

public class HeaderColor : MonoBehaviour
{
    private Text component;
    public float Speed;
    private float startTime;

    private void Start()
    {
        component = GetComponent<Text>();
        startTime = Time.time;
    }

    private void Update() => component.color = Color.Lerp(Color.white, GameColors.GILGOR_PURPLE, (Time.time - startTime) * Speed);
}