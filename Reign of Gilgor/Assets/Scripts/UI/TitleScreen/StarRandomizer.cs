﻿using Microsoft.Extensions.Logging;
using RoGCore.Logging;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Client.UI.TitleScreen
{
    public class StarRandomizer : MonoBehaviour
    {
        [SerializeField]
        private float moveSpeed = 1;
        [SerializeField]
        private int twinkleChance = 1;
        [SerializeField]
        private int generateRange = 25;
        [SerializeField]
        private int generateChance = 10;
        [SerializeField]
        private TileBase[] Tile;

        private Vector3 direction;
        private Tilemap tilemap;
        private Vector3Int lastGeneratePosition = new Vector3Int(-1, -1, -1);

        private static readonly ILogger<StarRandomizer> Logger = LogFactory.LoggingInstance<StarRandomizer>();

        private void Start()
        {
            var rad = Random.Range(0, 2 * Mathf.PI);
            direction = new Vector3(Mathf.Sin(rad), Mathf.Cos(rad), 0f);
            tilemap = GetComponentInChildren<Tilemap>(true);

            if (tilemap == null)
            {
                Logger.LogError($"GameObject: {gameObject.name} - There is no tilemap connected to this gameobject.");
            }
        }

        private void Update()
        {
            transform.localPosition += moveSpeed * Time.deltaTime * direction;

            var generatePosition = -new Vector3Int((int)transform.localPosition.x, (int)transform.localPosition.y, (int)transform.localPosition.z);

            if (lastGeneratePosition == generatePosition)
            {
                return;
            }

            lastGeneratePosition = generatePosition;

            for (var x = -generateRange + generatePosition.x; x <= generateRange + generatePosition.x; x++)
            {
                for (var y = -generateRange + generatePosition.y; y <= generateRange + generatePosition.y; y++)
                {
                    var position = new Vector3Int(x, y, 0);
                    var random = new System.Random(position.GetHashCode());

                    if (tilemap.GetTile(position) == null && random.Next(generateChance) == 0)
                    {
                        tilemap.SetTile(new Vector3Int(x, y, 0), Tile[Random.Range(0, Tile.Length)]);
                    }

                    if (tilemap.GetTile(position) != null && Random.Range(0, twinkleChance) == 0)
                    {
                        tilemap.SetTile(new Vector3Int(x, y, 0), Tile[Random.Range(0, Tile.Length)]);
                    }
                }
            }
        }
    }
}