using System.Collections;
using Client.Networking.API;
using Client.UI.Button;
using Client.UI.Dialog;
using Client.UI.TitleScreen;
using Client.Utils;
using FluentResults;
using RoGCore.Networking.APIViewModels;
using TMPro;
using UnityEngine;

internal sealed class StartScreenPage : TitleScreenPage
{
    [SerializeField]
    private TextMeshProUGUI loggedInText;
    [SerializeField]
    private SpriteEnhancedButton loginButton;
    [SerializeField]
    private SpriteEnhancedButton playButton;

    public override int InitializeOrder() => 0;

    public override void Initialize()
    {
        EnterLoggedOutState();
    }

    internal override void Enable()
    {
        gameObject.SetActive(true);
        StartCoroutine(Setup());
        AccountStore.AccountData.ResponseChanged += AccountDataChanged;
    }

    internal void OnDisable()
    {
        AccountStore.AccountData.ResponseChanged -= AccountDataChanged;
    }

    private void AccountDataChanged(Result<AccountViewModel> account)
    {
        StartCoroutine(Setup());
    }

    private void EnterLoggedInState()
    {
        loggedInText.text = $"Logged in as: {AccountStore.Username}";

        loginButton.SetText("Logout");
        loginButton.onClick.RemoveAllListeners();
        loginButton.onClick.AddListener(AccountStore.Logout);
    }

    private void EnterLoggedOutState()
    {
        loggedInText.text = "Logged out";

        loginButton.SetText("Login");
        loginButton.onClick.RemoveAllListeners();
        loginButton.onClick.AddListener(LoginDialog.Display);

        playButton.SetText("Login");
        playButton.onClick.RemoveAllListeners();
        playButton.onClick.AddListener(LoginDialog.Display);
    }

    private IEnumerator Setup()
    {
        var account = new CoroutineResultWrapper<AccountViewModel>();
        yield return AccountStore.AccountData.GetValue(account);

        if (account.IsFailed)
        {
            EnterLoggedOutState();
            yield break;
        }

        EnterLoggedInState();

        playButton.onClick.RemoveAllListeners();

        var newAccount = new CoroutineOutputWrapper<bool>();
        yield return AccountStore.IsNewAccount(newAccount);

        if (newAccount)
        {
            playButton.onClick.AddListener(() => TitleScreenController.Instance.SwipeRightTo(TitleScreenPages.ClassSelection));
            playButton.SetText("Begin");
        }
        else
        {
            playButton.onClick.AddListener(() => TitleScreenController.Instance.SwipeRightTo(TitleScreenPages.CharacterSelect));
            playButton.SetText("Play");
        }
    }
}
