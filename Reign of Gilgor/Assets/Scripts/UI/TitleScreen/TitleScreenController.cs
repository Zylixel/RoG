﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Client.Audio;
using Client.Game;
using Client.Networking.API;
using Client.UI.Dialog;
using Client.Utils;
using Microsoft.Extensions.Logging;
using RoGCore.Networking;
using RoGCore.Networking.APIViewModels;
using RoGCore.Utils;
using TMPro;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Client.UI.TitleScreen
{
    public partial class TitleScreenController : Initializable<TitleScreenController>
    {
        internal const float AnimationSpeed = 7.5f;
        internal const float AnimationDuration = 3f / AnimationSpeed;

        [SerializeField]
        internal Canvas LoadingCanvas;
        [SerializeField]
        internal Canvas BlankCanvas;
        [SerializeField]
        internal Canvas ClassSelectionCanvas;
        [SerializeField]
        internal Canvas CharacterSelectCanvas;
        [SerializeField]
        internal Canvas CreditCanvas;
        [SerializeField]
        internal Canvas StartCanvas;
        [SerializeField]
        internal Tilemap Stars;
        internal bool Initialized;
        internal bool Moving;
        internal TitleScreenPages MovingTo;
        internal EmbeddedData GameData;

        private readonly Dictionary<Canvas, TitleScreenPages> typeToEnum = new Dictionary<Canvas, TitleScreenPages>();
        private readonly Dictionary<TitleScreenPages, Vector3[]> componentOrigins = new Dictionary<TitleScreenPages, Vector3[]>();

        [SerializeField]
        private TextMeshProUGUI loadingText;
        [SerializeField]
        private TextMeshProUGUI loadingSubtext;
        private Dictionary<TitleScreenPages, Canvas> enumToCanvas;

        private static float SPAWN_DISTANCE => Screen.width + 100;

        public override void Initialize()
        {
            base.Initialize();

            enumToCanvas = new Dictionary<TitleScreenPages, Canvas>
            {
                { TitleScreenPages.Start, StartCanvas },
                { TitleScreenPages.CharacterSelect, CharacterSelectCanvas },
                { TitleScreenPages.ClassSelection, ClassSelectionCanvas },
                { TitleScreenPages.Blank, BlankCanvas },
                { TitleScreenPages.Credits, CreditCanvas },
                { TitleScreenPages.Loading, LoadingCanvas },
            };

            SetActive(true);

            foreach (var item in enumToCanvas)
            {
                typeToEnum.Add(item.Value, item.Key);
                componentOrigins.Add(item.Key, GetMovables(item.Value).Select(x => x.position).ToArray());
            }

            SetActive(false, LoadingCanvas);
        }

        public void SwipeLeftTo(Canvas canvas) => MoveTo(typeToEnum[canvas], -1);

        public void SwipeRightTo(Canvas canvas) => MoveTo(typeToEnum[canvas], 1);

        public void SwipeLeftTo(TitleScreenPages canvas) => MoveTo(canvas, -1);

        public void SwipeRightTo(TitleScreenPages canvas) => MoveTo(canvas, 1);

        public void InstantSwipe(TitleScreenPages canvas) => MoveTo(canvas, 0);

        internal IEnumerator LoadGame()
        {
            loadingText.color = Color.white;
            loadingText.text = "Loading Reign of Gilgor";

            var gameData = new CoroutineResultWrapper<GameDataViewModel>();
            yield return GameDataStore.LoadGameData(gameData);

            if (gameData.Result.IsFailed)
            {
                ErrorDialog.Display("Error Accessing Reign of Gilgor Servers", () => StartCoroutine(LoadGame()));
                Logger.LogError(gameData.Result.ErrorString());
                yield break;
            }

            EmbeddedData.Objectjson = gameData.Value.Objects;
            EmbeddedData.Groundjson = gameData.Value.Ground;
            EmbeddedData.Itemjson = gameData.Value.Items;
            EmbeddedData.Projectilejson = gameData.Value.Projectiles;

            GameData = new EmbeddedData();
            SkillTree.Initialize(gameData.Value.Skills);

            yield return TryLogin();

            Initializer.InitializeGameDataObjects(loadingText);

            Packet.InitPackets();
            PacketHandlers.InitalizeHandlers();

            GC.Collect();
            Initialized = true;
            InstantSwipe(TitleScreenPages.Start);
        }

        private static IEnumerable<Transform> GetMovables(Canvas canvas) => canvas.GetComponentsInChildren<Transform>(true).Where(child => child.CompareTag("Movable"));

        private void Start()
        {
            Logging.LoggingInitializer.Initialize();

            Initializer.InitializeStaticClasses(loadingText);
            Initializer.InitializeObjects(loadingText);

            MusicHandler.SetMusic("YouAndMe", 0);

            StartCoroutine(LoadGame());

            OptionsHandler.SubscribeToOptionsChange<bool>("vSync", (_, x) => QualitySettings.vSyncCount = x ? 1 : 0, true);
        }

        private void OnEnable()
        {
            if (!Initialized)
            {
                return;
            }

            if (InterSceneData.EnterSelectScreen)
            {
                InstantSwipe(TitleScreenPages.CharacterSelect);
            }
            else
            {
                SetActive(false, StartCanvas);
            }
        }

        private void Update()
        {
            if (MovingTo == TitleScreenPages.Awaiting)
            {
                return;
            }

            var diff = GetMovables(enumToCanvas[MovingTo]).First().position.x - GetAnchor(MovingTo).x;
            var toMove = -diff * AnimationSpeed * Time.deltaTime;
            Moving = Math.Abs(toMove) > 1f;
            foreach (var movable in GetAllMovables())
            {
                movable.Translate(toMove, 0f, 0f);
            }

            if (!(Math.Abs(GetMovables(enumToCanvas[MovingTo]).First().position.x - GetAnchor(MovingTo).x) < 1f))
            {
                return;
            }

            SetActive(false, enumToCanvas[MovingTo]);
            MovingTo = TitleScreenPages.Awaiting;
        }

        private IEnumerator TryLogin()
        {
            if (!AccountStore.LoadSavedCredentials())
            {
                yield break;
            }

            loadingText.text = "Logging in to Reign of Gilgor";
            loadingSubtext.text = string.Empty;

            var accountData = new CoroutineResultWrapper<AccountViewModel>();
            yield return AccountStore.Login(AccountStore.Username, AccountStore.Password, true, accountData);
        }

        private void MoveTo(TitleScreenPages enumVariant, int dir)
        {
            var canvas = enumToCanvas[enumVariant];

            var pageComponent = canvas.GetComponentInChildren<TitleScreenPage>(true);

            if (pageComponent != null)
            {
                pageComponent.Enable();
            }
            else
            {
                canvas.gameObject.SetActive(true);
            }

            MovingTo = enumVariant;
            var originalChildren = componentOrigins[enumVariant];
            var children = GetMovables(canvas).ToArray();
            for (var i = 0; i < originalChildren.Length; i++)
            {
                children[i].position = new Vector2(originalChildren[i].x + (dir * SPAWN_DISTANCE), originalChildren[i].y);
            }
        }

        private IEnumerable<Transform> GetAllMovables()
        {
            foreach (var canvas in enumToCanvas.Values)
            {
                foreach (var child in GetMovables(canvas))
                {
                    yield return child;
                }
            }
        }

        private Vector2 GetAnchor(TitleScreenPages canvas) => componentOrigins[canvas].First();

        private void SetActive(bool active, Canvas ignore = null)
        {
            foreach (var canvas in enumToCanvas.Values.Where(canvas => canvas != ignore))
            {
                canvas.gameObject.SetActive(active);
            }
        }
    }
}