﻿namespace Client.UI.TitleScreen
{
    internal abstract class TitleScreenPage : Initializable
    {
        internal abstract void Enable();
    }
}
