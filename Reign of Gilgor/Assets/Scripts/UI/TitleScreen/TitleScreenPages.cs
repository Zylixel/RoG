﻿namespace Client.UI.TitleScreen
{
    public enum TitleScreenPages
    {
        Awaiting,
        Start,
        CharacterSelect,
        ClassSelection,
        Blank,
        Credits,
        Loading,
    }
}