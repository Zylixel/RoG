using Client.Audio;
using DG.Tweening;
using TMPro;
using UnityEngine;

namespace Client.UI.Titlecard
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class Titlecard : Initializable<Titlecard>
    {
        private TextMeshProUGUI titleText;

        public override void Initialize()
        {
            titleText = gameObject.GetComponent<TextMeshProUGUI>();
            TitlecardEvent.Begin += OnDisplayTitleCardEvent;
        }

        [ContextMenu("Display")]
        public void Display() => TitlecardEvent.Invoke(titleText.text);

        private void OnDisplayTitleCardEvent(string title)
        {
            titleText.text = title;
            gameObject.SetActive(true);

            AudioHandler.PlaySound("Titlecard");
            DOTween.Sequence()
                .AppendInterval(0.4f)
                .Append(titleText.DOFade(1, 0.5f))
                .AppendInterval(1f)
                .Append(titleText.DOFade(0, 1))
                .OnComplete(() => TitlecardEvent.Revoke());
        }
    }
}