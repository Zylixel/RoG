﻿#nullable enable

namespace Client.UI.Titlecard
{
    internal static class TitlecardEvent
    {
        internal delegate void TitlecardEventHandler(string title);

        internal delegate void TitlecardEventEndHandler();

        internal static event TitlecardEventHandler? Begin;

        internal static event TitlecardEventEndHandler? End;

        internal static bool InProgess { get; private set; }

        internal static void Invoke(string title)
        {
            InProgess = true;
            Begin?.Invoke(title);
        }

        internal static void Revoke()
        {
            InProgess = false;
            End?.Invoke();
        }
    }
}
