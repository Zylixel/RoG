﻿using Client.Entity.Player;
using UnityEngine;
using UnityEngine.UI;

namespace Client.UI.Tooltip
{
    internal abstract class AdvancedBaseTooltip<T> : Initializable<T>
        where T : class
    {
        protected bool open;
        protected Player gamePlayer;

        [SerializeField]
        protected Image background;

        protected virtual bool FollowCursor() => true;

        public override void Initialize()
        {
            gameObject.SetActive(false);
            GameWorldManager.PlayerCreated.Begin += (player) => gamePlayer = player;
            base.Initialize();
        }

        internal virtual void Open()
        {
            if (open)
            {
                return;
            }

            open = true;

            gameObject.SetActive(true);

            foreach (RectTransform transform in gameObject.GetComponentInChildren<RectTransform>())
            {
                LayoutRebuilder.ForceRebuildLayoutImmediate(transform);
            }

            gameObject.GetComponentInChildren<HorizontalTooltipExpander>().Update();

            MoveToCursor();
        }

        internal virtual void Close()
        {
            if (!open)
            {
                return;
            }

            open = false;

            gameObject.SetActive(false);
        }

        protected virtual void Update()
        {
            if (FollowCursor())
            {
                MoveToCursor();
            }
        }

        protected void Toggle()
        {
            if (open)
            {
                Close();
            }
            else
            {
                Open();
            }
        }

        private void MoveToCursor()
        {
            var position = new Vector3(background.rectTransform.localPosition.x, background.rectTransform.localPosition.y, 0);

            var scaleMult = 0.95f;
            var scaleOffset = (1f / scaleMult) - 1;
            var scale = scaleMult * new Vector2(background.transform.lossyScale.x + scaleOffset, background.transform.lossyScale.y + scaleOffset);

            Vector3 offset = new Vector3(background.rectTransform.sizeDelta.x, background.rectTransform.sizeDelta.y, 0) * scale;
            offset *= new Vector2(UnityEngine.Input.mousePosition.x > Screen.width * 0.5f ? 1 : 0, UnityEngine.Input.mousePosition.y > Screen.height * 0.5f ? 0 : -1);

            transform.position = UnityEngine.Input.mousePosition - position - offset;
        }
    }
}