﻿using Client.Entity.Player;
using UnityEngine;

namespace Client.UI.Tooltip
{
    internal abstract class BaseTooltip<T> : Initializable<T>
        where T : class
    {
        protected bool open;
        protected Player gamePlayer;

        protected virtual bool FollowCursor() => true;

        public override void Initialize()
        {
            gameObject.SetActive(false);
            GameWorldManager.PlayerCreated.Begin += (player) => gamePlayer = player;
        }

        internal virtual void Open()
        {
            if (open)
            {
                return;
            }

            open = true;

            gameObject.SetActive(true);
            MoveToCursor();
            SetPivot();
        }

        internal virtual void Close()
        {
            if (!open)
            {
                return;
            }

            open = false;

            gameObject.SetActive(false);
        }

        protected virtual void Update()
        {
            if (FollowCursor())
            {
                MoveToCursor();
            }
        }

        protected void Toggle()
        {
            if (open)
            {
                Close();
            }
            else
            {
                Open();
            }
        }

        private void MoveToCursor() => transform.position = UnityEngine.Input.mousePosition;

        protected void SetPivot() => (gameObject.transform as RectTransform).pivot = new Vector2(
            UnityEngine.Input.mousePosition.x > Screen.width / 2f ? 1 : 0,
            UnityEngine.Input.mousePosition.y > Screen.height / 2f ? 1 : 0);
    }
}