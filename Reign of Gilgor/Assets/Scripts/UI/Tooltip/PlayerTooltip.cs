﻿using System.Collections.Generic;
using System.Linq;
using Client.Entity;
using Client.Game;
using Client.Networking;
using Client.UI.Chat;
using RoGCore.Models;
using RoGCore.Networking.Packets;
using UnityEngine;
using UnityEngine.UI;
using Text = UnityEngine.UI.Text;

namespace Client.UI.Tooltip
{
    internal sealed class PlayerTooltip : AdvancedBaseTooltip<PlayerTooltip>
    {
        [SerializeField]
        private UnityEngine.UI.Button blockButton;

        private List<UnityEngine.UI.Button> buttons;

        [SerializeField]
        private UnityEngine.UI.Button gambleButton;

        [SerializeField]
        private UnityEngine.UI.Button guildButton;

        [SerializeField]
        private UnityEngine.UI.Button lockButton;

        [SerializeField]
        private UnityEngine.UI.Button messageButton;

        [SerializeField]
        private UnityEngine.UI.Button teleportButton;

        [SerializeField]
        private UnityEngine.UI.Button tradeButton;

        [SerializeField]
        private Text nameText;

        [SerializeField]
        private Image playerSprite;

        private bool openedThisFrame;

        private ServerPlayer selectedPlayer;

        internal static bool Visible => Instance.gameObject.activeSelf;

        protected override bool FollowCursor() => false;

        public override void Initialize()
        {
            AddListeners();

            buttons = new List<UnityEngine.UI.Button> { teleportButton, messageButton, lockButton, blockButton, tradeButton, gambleButton, guildButton };

            tradeButton.gameObject.SetActive(false);
            gambleButton.gameObject.SetActive(false);
            blockButton.gameObject.SetActive(false);

            base.Initialize();
        }

        private void AddListeners()
        {
            teleportButton.onClick.AddListener(Teleport);
            messageButton.onClick.AddListener(Message);
            blockButton.onClick.AddListener(Block);
            tradeButton.onClick.AddListener(Trade);
            gambleButton.onClick.AddListener(Gamble);
            lockButton.onClick.AddListener(Lock);
            guildButton.onClick.AddListener(InviteToGuild);
        }

        private void Teleport()
        {
            if (selectedPlayer == null)
            {
                return;
            }

            GameWorldManager.CurrentOutgoingNetwork.OutgoingMessage(new Teleport(selectedPlayer.Id));
            Close();
        }

        private void Message()
        {
            if (selectedPlayer == null)
            {
                return;
            }

            ChatHandler.EnterChat($"/tell {selectedPlayer.Name} ");
            Close();
        }

        private void Block()
        {
            if (selectedPlayer == null)
            {
                return;
            }

            GameWorldManager.CurrentOutgoingNetwork.OutgoingMessage(new EditAccountList(EditAccountList.ListType.Ignored, selectedPlayer.AccountId));
            Close();
        }

        private void Trade()
        {
            if (selectedPlayer == null)
            {
                return;
            }

            TextHandler.AddText(new RoGCore.Networking.Packets.Text("Trading has not been implemented yet.", new RGB(0xFF0000)));
            Close();
        }

        private void Gamble()
        {
            if (selectedPlayer == null)
            {
                return;
            }

            TextHandler.AddText(new RoGCore.Networking.Packets.Text("Gambling has not been implemented yet.", new RGB(0xFF0000)));
            Close();
        }

        private void Lock()
        {
            if (selectedPlayer == null)
            {
                return;
            }

            GameWorldManager.CurrentOutgoingNetwork.OutgoingMessage(new EditAccountList(EditAccountList.ListType.Locked, selectedPlayer.AccountId));
            Close();
        }

        private void InviteToGuild()
        {
            if (selectedPlayer == null)
            {
                return;
            }

            GameWorldManager.CurrentOutgoingNetwork.OutgoingMessage(new GuildInvite(selectedPlayer.Name, gamePlayer.GuildName));
            Close();
        }

        protected override void Update()
        {
            playerSprite.sprite = selectedPlayer.SpriteRenderer.sprite;
            playerSprite.transform.eulerAngles = new Vector3(0, selectedPlayer.SpriteRenderer.flipX ? 180 : 0, 0);

            base.Update();
        }

        private void LateUpdate()
        {
            if (openedThisFrame)
            {
                openedThisFrame = false;
                return;
            }

            if (UnityEngine.Input.GetMouseButtonDown(0))
            {
                var currentRaycast = GameWorldManager.Instance.Map.CurrentRaycast;
                for (var i = 0; i < currentRaycast.Count; i++)
                {
                    if (currentRaycast[i].gameObject.name.Equals(gameObject.name))
                    {
                        return;
                    }
                }

                Close();
            }
        }

        internal static void Display(ServerPlayer player)
        {
            if (player == null)
            {
                return;
            }

            Instance.selectedPlayer = player;
            Instance.Open();
        }

        private void OnEnable()
        {
            if (selectedPlayer == null || gamePlayer == null)
            {
                return;
            }

            openedThisFrame = true;
            nameText.text = selectedPlayer.Name;
            playerSprite.sprite = selectedPlayer.SpriteRenderer.sprite;
            teleportButton.gameObject.SetActive(selectedPlayer.Manager.Map.Info.AllowTeleport);
            guildButton.gameObject.SetActive(gamePlayer.GuildRank >= 20 && selectedPlayer.GuildName?.Length == 0);

            // Reset pivot for button alignments
            background.rectTransform.pivot = Vector2.zero;
            var sizeDelta = new Vector2(300, 120 + (buttons.Count(x => x.gameObject.activeSelf) * 70));
            background.rectTransform.sizeDelta = sizeDelta;
            var activeButtons = 0;
            for (var i = 0; i < buttons.Count; i++)
            {
                if (buttons[i].gameObject.activeSelf)
                {
                    buttons[i].transform.localPosition =
                        new Vector2(sizeDelta.x * 0.5f, sizeDelta.y - 140 - (activeButtons * 70));
                    activeButtons++;
                }
            }

            background.material.SetColor(ShaderProperties.HighlightColor, GameColors.GetOtherPlayerColor(gamePlayer, selectedPlayer));
        }
    }
}