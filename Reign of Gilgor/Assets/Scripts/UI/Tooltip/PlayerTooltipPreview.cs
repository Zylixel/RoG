﻿using Client.Entity;
using Client.Entity.Player;
using Client.Game;
using Client.UI.Item;
using UnityEngine;
using UnityEngine.UI;

namespace Client.UI.Tooltip
{
    internal sealed class PlayerTooltipPreview : AdvancedBaseTooltip<PlayerTooltipPreview>
    {
        [SerializeField]
        private ItemHolder weaponHolder;

        [SerializeField]
        private ItemHolder abilityHolder;

        [SerializeField]
        private ItemHolder garmentHolder;

        [SerializeField]
        private ItemHolder ringHolder;

        [SerializeField]
        private GuildLabel guildLabel;

        [SerializeField]
        private Text levelText;

        [SerializeField]
        private Text nameText;

        [SerializeField]
        private Image playerSprite;

        private Player player;

        private ServerPlayer selectedPlayer;

        public override void Initialize()
        {
            GameWorldManager.PlayerCreated.Begin += (player) => this.player = player;

            base.Initialize();
        }

        protected override void Update()
        {
            if (PlayerTooltip.Visible)
            {
                Close();
                return;
            }

            base.Update();

            playerSprite.sprite = selectedPlayer.SpriteRenderer.sprite;
            playerSprite.transform.eulerAngles = new Vector3(0, selectedPlayer.SpriteRenderer.flipX ? 180 : 0, 0);
        }

        internal static void Display(ServerPlayer otherPlayer)
        {
            if (PlayerTooltip.Visible || otherPlayer == null)
            {
                return;
            }

            Instance.selectedPlayer = otherPlayer;
            Instance.Open();
        }

        internal static void CloseTooltip() => Instance.Close();

        private void OnEnable()
        {
            if (selectedPlayer == null)
            {
                return;
            }

            nameText.text = selectedPlayer.Name;
            playerSprite.sprite = selectedPlayer.SpriteRenderer.sprite;
            levelText.text = "Cumulative Skill Level " + selectedPlayer.SkillLevels.CumulativeLevels();
            weaponHolder.Set(selectedPlayer.Inventory[0], selectedPlayer.Manager);
            abilityHolder.Set(selectedPlayer.Inventory[1], selectedPlayer.Manager);
            garmentHolder.Set(selectedPlayer.Inventory[2], selectedPlayer.Manager);
            ringHolder.Set(selectedPlayer.Inventory[3], selectedPlayer.Manager);
            guildLabel.Set(selectedPlayer.GuildName, selectedPlayer.GuildRank);

            background.material.SetColor(ShaderProperties.HighlightColor, GameColors.GetOtherPlayerColor(player, selectedPlayer));
        }
    }
}