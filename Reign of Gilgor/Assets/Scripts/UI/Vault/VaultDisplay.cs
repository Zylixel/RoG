﻿using System;
using Client.Entity.Player;
using Client.UI.Dialog;
using Client.UI.Item;
using RoGCore.Assets;
using RoGCore.Models;
using RoGCore.Models.ObjectSlot;
using RoGCore.Networking.Packets;
using Structures;
using TMPro;
using UnityEngine;
using static Client.Entity.Helpers.EntityExtensions;
using static Client.Entity.Player.InventoryDisplay;

namespace Client.UI.Vault
{
    internal class VaultDisplay : BaseDialog<VaultDisplay>
    {
        // Shared with server
        private const int VaultSize = 40;

        [SerializeField]
        private UnityEngine.UI.Button closeButton;

        [SerializeField]
        private UnityEngine.UI.Button rightButton;

        [SerializeField]
        private TMP_InputField searchField;

        [SerializeField]
        private UnityEngine.UI.Button leftButton;

        private VaultData data;

        private readonly ItemHolder[] inventory = new ItemHolder[Inventory_Size];

        private readonly ItemHolder[] vault = new ItemHolder[VaultSize];

        private byte lockedAmount;

        private byte page;

        private byte pages;

        private Player player;

        public override void Initialize()
        {
            base.Initialize();

            closeButton.onClick.AddListener(Close);
            searchField.onSubmit.AddListener(delegate
            {
                page = 1;
                RequestData();
                searchField.gameObject.SetActive(false);
            });
            leftButton.onClick.AddListener(DecrementPage);
            rightButton.onClick.AddListener(IncrementPage);

            for (byte i = 0; i < Inventory_Size; i++)
            {
                var newBackground = Instantiate(GamePrefabs.ItemHolder, transform).GetComponent<ItemHolder>();
                newBackground.Index = (byte)(i + 4);
                newBackground.transform.localPosition = new Vector3(-600 + ((i % 7) * 80), 100 + ((int)Math.Floor(i / 7f) * -80));
                newBackground.SetSize(70);

                inventory[i] = newBackground;
            }

            for (byte i = 0; i < VaultSize; i++)
            {
                var newBackground = Instantiate(GamePrefabs.ItemHolder, transform).GetComponent<ItemHolder>();

                // Allows tooltip to overlay the item holder
                newBackground.transform.SetAsFirstSibling();

                newBackground.Index = i;
                newBackground.transform.localPosition = new Vector3(125 + ((i % 5f) * 80), 240 + ((int)Math.Floor(i / 5f) * -80));
                newBackground.SetSize(70);

                vault[i] = newBackground;
            }

            GameWorldManager.PlayerCreated.Begin += (player) => this.player = player;
        }

        internal static void OnUpdate(Player owner)
        {
            for (var i = 0; i < Inventory_Size; i++)
            {
                Instance.inventory[i].Set(owner.Inventory[i + 4], owner.Manager);
            }
        }

        internal static void OnResult(in VaultResult packet) => Instance.InternalOnResult(packet);

        private void InternalOnResult(in VaultResult packet)
        {
            pages = packet.Pages;
            lockedAmount = packet.LockedAmount;
            data.Inventory = RawItem.Cook(packet.NewInv, player.Manager.GameData.Items);
            for (var i = 0; i < VaultSize; i++)
            {
                if (i >= VaultSize - lockedAmount)
                {
                    vault[i].gameObject.SetActive(false);
                    continue;
                }

                vault[i].gameObject.SetActive(true);
                if (i >= data.Inventory.Length)
                {
                    vault[i].Set(null, player.Manager);
                    continue;
                }

                vault[i].Set(data.Inventory[i], player.Manager);
            }

            ProcessingData = false;
            CalculateButtons();
        }

        private void OnEnable() => RequestData();

        private void OnDisable() => HeldItemHandler.Dispatch();

        internal void RequestData()
        {
            ProcessingData = true;
            GameWorldManager.CurrentOutgoingNetwork.OutgoingMessage(new VaultRequest(page, searchField.text, VaultSortButtonHandler.SortingType));
        }

        internal static void Display()
        {
            Instance.page = 1;
            Instance.Open();
        }

        private void IncrementPage()
        {
            if (page >= pages)
            {
                return;
            }

            page++;
            RequestData();
        }

        private void DecrementPage()
        {
            if (page <= 1)
            {
                return;
            }

            page--;
            RequestData();
        }

        private void CalculateButtons()
        {
            rightButton.interactable = page != pages;
            leftButton.interactable = page != 1;
        }

        private void Update()
        {
            if (player == null)
            {
                return;
            }

            foreach (var bg in ItemHolder.GetItemsInRaycastResults(player.Manager.Map.CurrentRaycast, inventory))
            {
                if (!HeldItemHandler.HasItem() && bg.Item == null)
                {
                    continue;
                }

                if (!UnityEngine.Input.GetMouseButtonDown(0))
                {
                    continue;
                }

                if (HeldItemHandler.HasItem())
                {
                    if ((HeldItemHandler.CurrentHeld == HeldItemHandler.HeldType.Vault)
                        || (player.AuditItem(HeldItemHandler.GetItem(), bg.Index)
                            && ((HeldItemHandler.HeldItemData)HeldItemHandler.Data).Container.AuditItem(
                                 bg.Item, ((HeldItemHandler.HeldItemData)HeldItemHandler.Data).SlotId)))
                    {
                        GameWorldManager.CurrentOutgoingNetwork.OutgoingMessage(new InventoryMove(
                            HeldItemHandler.Dispatch(),
                            new ContainerSlot(player.Id, bg.Index)));
                    }
                }
                else if (bg.Item is not null)
                {
                    HeldItemHandler.SetItem(player, bg.Index, bg.Sprite);
                }
            }

            foreach (var bg in ItemHolder.GetItemsInRaycastResults(player.Manager.Map.CurrentRaycast, vault))
            {
                if (!HeldItemHandler.HasItem() && bg.Item is null)
                {
                    continue;
                }

                if (!UnityEngine.Input.GetMouseButtonDown(0))
                {
                    continue;
                }

                if (HeldItemHandler.HasItem())
                {
                    if (HeldItemHandler.CurrentHeld != HeldItemHandler.HeldType.Vault)
                    {
                        GameWorldManager.CurrentOutgoingNetwork.OutgoingMessage(new InventoryMove(HeldItemHandler.Dispatch(), new VaultSlot()));
                    }
                }
                else if (bg.Item is not null)
                {
                    HeldItemHandler.SetItem(bg.Item.Value, bg.Sprite);
                }
            }
        }

        private struct VaultData : IContainer
        {
            public RoGCore.Models.Item?[] Inventory { get; set; }

            // This will never be assigned, the vault is not an object
            public ObjectDescription ObjectDescProperty { get; set; }
        }
    }
}