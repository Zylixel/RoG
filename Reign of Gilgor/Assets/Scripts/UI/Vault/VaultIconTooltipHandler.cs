﻿using TMPro;
using UnityEngine;

namespace Client.UI.Vault
{
    internal sealed class VaultIconTooltipHandler : Initializable<VaultIconTooltipHandler>
    {
        [SerializeField]
        private TextMeshProUGUI label;

        public override void Initialize() => Instance = this;

        internal static void Open(string description)
        {
            Instance.Set(description);
            Instance.gameObject.SetActive(true);
        }

        internal static void SetLabel(string description) => Instance.label.text = description;

        internal static void Close() => Instance.gameObject.SetActive(false);

        private void Set(string desc)
        {
            label.text = desc;
            Update();
        }

        private void Update() => gameObject.transform.position = UnityEngine.Input.mousePosition;
    }
}