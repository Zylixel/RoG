﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Client.UI.Vault
{
    [RequireComponent(typeof(UnityEngine.UI.Button))]
    internal sealed class VaultSearchButtonHandler : Initializable<VaultSearchButtonHandler>, IPointerEnterHandler,
        IPointerExitHandler
    {
        private UnityEngine.UI.Button button;

        [SerializeField]
        private GameObject searchField;

        public void OnPointerEnter(PointerEventData data) => VaultIconTooltipHandler.Open("Set Search Term");

        public void OnPointerExit(PointerEventData data) => VaultIconTooltipHandler.Close();

        public override void Initialize()
        {
            button = gameObject.GetComponent<UnityEngine.UI.Button>();
            button.onClick.AddListener(OnButtonClicked);
        }

        private void OnButtonClicked() => searchField.SetActive(true);
    }
}