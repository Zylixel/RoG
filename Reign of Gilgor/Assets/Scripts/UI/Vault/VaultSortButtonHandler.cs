﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using static RoGCore.Networking.Packets.VaultRequest;

namespace Client.UI.Vault
{
    [RequireComponent(typeof(UnityEngine.UI.Button))]
    [RequireComponent(typeof(Image))]
    internal sealed class VaultSortButtonHandler : Initializable<VaultSortButtonHandler>, IPointerEnterHandler, IPointerExitHandler
    {
        private static readonly Dictionary<SortType, string> FullNames = new Dictionary<SortType, string>()
        {
            { SortType.Alphabetical, "Alphabetical Order" },
            { SortType.TierUp, "Tier Ascending" },
            { SortType.TierDown, "Tier Descending" },
            { SortType.LevelUp, "Level Ascending" },
            { SortType.LevelDown, "Level Descending" },
        };

        internal static SortType SortingType;

        private UnityEngine.UI.Button button;
        private Image icon;

        [SerializeField]
        private Sprite[] icons;

        public void OnPointerEnter(PointerEventData data) => VaultIconTooltipHandler.Open("Sort By:\n" + FullNames[SortingType]);

        public void OnPointerExit(PointerEventData data) => VaultIconTooltipHandler.Close();

        public override void Initialize()
        {
            button = gameObject.GetComponent<UnityEngine.UI.Button>();
            icon = gameObject.GetComponent<Image>();
            SortingType = SortType.Alphabetical;
            button.onClick.AddListener(OnButtonClicked);
            UpdateIcon();
#if UNITY_EDITOR || DEVELOPMENT_BUILD
            if (icons.Length < Enum.GetNames(typeof(SortType)).Length)
            {
                Logger.LogError("There are less icons than sorting types!");
            }
#endif
        }

        private void UpdateIcon()
        {
            icon.sprite = icons[(byte)SortingType];
            VaultIconTooltipHandler.SetLabel("Sort By:\n" + FullNames[SortingType]);
        }

        private void OnButtonClicked()
        {
            if (SortingType == SortType.LevelDown)
            {
                // if at end of sorting modes, move back to beginning
                SortingType = SortType.Alphabetical;
            }
            else
            {
                // Not the cleanest work, moves to next sorting type
                SortingType = (SortType)((byte)SortingType + 1);
            }

            UpdateIcon();
            VaultDisplay.Instance.RequestData();
        }
    }
}