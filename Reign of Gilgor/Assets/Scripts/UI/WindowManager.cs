using UnityEngine;

namespace Client.UI
{
    public class WindowManager : Initializable<WindowManager>
    {
        public override int InitializeOrder() => 0;

        public delegate void ScreenSizeChangeEventHandler();       // Define a delegate for the event

        public static event ScreenSizeChangeEventHandler ScreenSizeChangeEvent;                //  Define the Event

        protected virtual void OnScreenSizeChange(int width, int height) =>

            // Define Function trigger and protect the event for not null;
            ScreenSizeChangeEvent?.Invoke();

        private Vector2 lastScreenSize;

        public override void Initialize()
        {
            lastScreenSize = new Vector2(Screen.width, Screen.height);
            Instance = this;
        }

        private void Update()
        {
            var screenSize = new Vector2(Screen.width, Screen.height);

            if (lastScreenSize == screenSize)
            {
                return;
            }

            lastScreenSize = screenSize;
            OnScreenSizeChange(Screen.width, Screen.height);                        // Launch the event when the screen size change
        }
    }
}