﻿using Newtonsoft.Json.Utilities;
using RoGCore.Assets;
using RoGCore.Models;
using UnityEngine;

// This class ensures that JSONs are deserialized properly while IL2CPP is Enabled
public class AOTEnforcer : MonoBehaviour
{
    public void Awake()
    {
        AotHelper.EnsureList<SkillType>();
        AotHelper.EnsureList<Class.Type>();
        AotHelper.EnsureList<GroundDescription>();
        AotHelper.EnsureList<ConditionEffect>();

        Destroy(gameObject);
    }
}