﻿using UnityEngine;

namespace Client.Utils
{
    internal static class ColorExtensions
    {
        internal static Color32[] ExpandTo32(this Color[] colors)
        {
            Debug.Assert(Mathf.Sqrt(colors.Length) % 1 == 0);

            var oldSideLength = (int)Mathf.Sqrt(colors.Length);
            var newSideLength = oldSideLength + 2;

            var ret = new Color32[newSideLength * newSideLength];

            for (var x = 0; x < newSideLength; x++)
            {
                for (var y = 0; y < newSideLength; y++)
                {
                    var fixedX = Mathf.Clamp(x - 1, 0, oldSideLength - 1);
                    var fixedY = Mathf.Clamp(y - 1, 0, oldSideLength - 1);

                    ret[(newSideLength * x) + y] = colors[(oldSideLength * fixedX) + fixedY];
                }
            }

            return ret;
        }

        internal static Color[] Expand(this Color[] colors)
        {
            Debug.Assert(Mathf.Sqrt(colors.Length) % 1 == 0);

            var oldSideLength = (int)Mathf.Sqrt(colors.Length);
            var newSideLength = oldSideLength + 2;

            var ret = new Color[newSideLength * newSideLength];

            for (var x = 0; x < newSideLength; x++)
            {
                for (var y = 0; y < newSideLength; y++)
                {
                    var fixedX = Mathf.Clamp(x - 1, 0, oldSideLength - 1);
                    var fixedY = Mathf.Clamp(y - 1, 0, oldSideLength - 1);

                    ret[(newSideLength * x) + y] = colors[(oldSideLength * fixedX) + fixedY];
                }
            }

            return ret;
        }

        internal static Color32[] GetBlankColors(this Color baseColor, int length)
        {
            Color32 baseColorConverted = baseColor;
            var ret = new Color32[length];

            for (var i = 0; i < ret.Length; i++)
            {
                ret[i] = baseColorConverted;
            }

            return ret;
        }
    }
}
