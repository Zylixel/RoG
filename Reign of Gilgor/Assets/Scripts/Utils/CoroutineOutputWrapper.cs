﻿namespace Client.Utils
{
    internal class CoroutineOutputWrapper<T>
    {
        internal T Value { private get; set; }

        public static implicit operator T(CoroutineOutputWrapper<T> wrapper)
        {
            return wrapper.Value;
        }
    }
}