﻿using FluentResults;

namespace Client.Utils
{
    internal class CoroutineResultWrapper<T>
    {
        internal Result<T> Result;

        internal T Value => Result.Value;

        internal bool IsFailed => Result.IsFailed;

        public static implicit operator Result<T>(CoroutineResultWrapper<T> wrapper)
        {
            return wrapper.Result;
        }
    }
}