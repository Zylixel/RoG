using System.Diagnostics;

internal static class StopwatchExtensions
{
    internal static void StopAndSet(this Stopwatch stopWatch, ref double output)
    {
        stopWatch.Stop();
        output = stopWatch.Elapsed.TotalMilliseconds;
    }
}