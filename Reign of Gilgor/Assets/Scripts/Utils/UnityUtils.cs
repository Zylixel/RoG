﻿using System.Collections.Generic;
using System.Linq;
using Client.UI.TitleScreen;
using Microsoft.Extensions.Logging;
using RoGCore.Logging;
using RoGCore.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace Client.Utils
{
    internal static class UnityUtils
    {
        private static readonly Microsoft.Extensions.Logging.ILogger Logger = LogFactory.LoggingInstance(nameof(UnityUtils));

        private static void AnimateMoveLocalInternal(this Transform child, Vector3 desiredPosition, float animateSpeed)
        {
            var diff = desiredPosition - child.localPosition;

            // Saves a decent amount of MS because it reduces transform.set calls
            if (diff == Vector3.zero)
            {
                return;
            }

            var scaledDiff = diff * animateSpeed;

            child.localPosition += new Vector3(
                Mathf.Abs(scaledDiff.x) < 0.001f ? diff.x : scaledDiff.x,
                Mathf.Abs(scaledDiff.y) < 0.001f ? diff.y : scaledDiff.y,
                Mathf.Abs(scaledDiff.z) < 0.001f ? diff.z : scaledDiff.z);
        }

        internal static void AnimateMoveLocal(this Transform child, Vector3 desiredPosition, float multiplier = 1) =>
            child.AnimateMoveLocalInternal(
                desiredPosition,
                TitleScreenController.AnimationSpeed * Time.deltaTime * multiplier);

        internal static void AnimateMoveLocal(this GameObject go, Vector3 desiredPosition, float multiplier = 1) =>
            go.transform.AnimateMoveLocal(desiredPosition, multiplier);

        internal static void AnimateMoveLocal(this Component component, Vector3 desiredPosition, float multiplier = 1) =>
            component.transform.AnimateMoveLocal(desiredPosition, multiplier);

        internal static void AnimateMoveFixedLocal(this Transform child, Vector3 desiredPosition) =>
            child.AnimateMoveLocalInternal(desiredPosition, TitleScreenController.AnimationSpeed * Time.fixedDeltaTime);

        internal static void AnimateSizeDelta(this RectTransform child, Vector2 desiredPosition, float speed = TitleScreenController.AnimationSpeed)
        {
            var diff = desiredPosition - child.sizeDelta;

            // Saves a decent amount of MS because it reduces transform.set calls
            if (diff == Vector2.zero)
            {
                return;
            }

            var scaledDiff = speed * Time.deltaTime * diff;

            child.sizeDelta += new Vector2(
                Mathf.Abs(scaledDiff.x) < 0.001f ? diff.x : scaledDiff.x,
                Mathf.Abs(scaledDiff.y) < 0.001f ? diff.y : scaledDiff.y);
        }

        internal static void AnimateSizeDelta(this GameObject go, Vector2 desiredPosition) =>
            AnimateSizeDelta((RectTransform)go.transform, desiredPosition);

        internal static void AnimateSizeDelta(this Component component, Vector2 desiredPosition) =>
            AnimateSizeDelta((RectTransform)component.transform, desiredPosition);

        private static void SetGraphicsUI(IEnumerable<Graphic> graphics, float newAlpha)
        {
            foreach (var graphic in graphics.Where(x => !x.CompareTag("FixedAlpha")))
            {
                graphic.color = new Color(graphic.color.r, graphic.color.g, graphic.color.b, newAlpha);
            }
        }

        internal static void SetUIAlpha(this Graphic component, float newAlpha, bool affectChildren)
        {
            if (affectChildren)
            {
                SetGraphicsUI(component.GetComponentsInChildren<Graphic>(), newAlpha);
            }

            component.color = new Color(component.color.r, component.color.g, component.color.b, newAlpha);
        }

        internal static void SetUIAlpha(this GameObject gameObject, float newAlpha, bool affectChildren = true) => SetGraphicsUI(affectChildren ? gameObject.GetComponentsInChildren<Graphic>() : gameObject.GetComponents<Graphic>(), newAlpha);

        private static void ChangeUIAlphaInternal(this Graphic component, float alphaChange, bool affectChildren = true)
        {
            var newAlpha = Mathf.Clamp(component.color.a + alphaChange, 0, 1);

            if (component.color.a == newAlpha)
            {
                return;
            }

            if (affectChildren)
            {
                SetGraphicsUI(component.GetComponentsInChildren<Graphic>(), newAlpha);
            }

            component.color = new Color(component.color.r, component.color.g, component.color.b, newAlpha);
        }

        internal static void ChangeUIAlpha(this Graphic component, float alphaDiff, bool affectChildren = true)
        {
            var alphaChange = alphaDiff * Time.deltaTime;
            component.ChangeUIAlphaInternal(alphaChange, affectChildren);
        }

        internal static void ChangeUIAlphaFixed(this Graphic component, float alphaDiff, bool affectChildren)
        {
            var alphaChange = alphaDiff * Time.fixedDeltaTime;
            component.ChangeUIAlphaInternal(alphaChange, affectChildren);
        }

        private static void ChangeUIAlphaInternal(this GameObject gameObject, float alphaChange, bool affectChildren = true)
        {
            var preAlpha = gameObject.GetComponentsInChildren<Graphic>().First(x => !x.CompareTag("FixedAlpha")).color.a;
            var newAlpha = Mathf.Clamp(preAlpha + alphaChange, 0, 1);
            if (preAlpha == newAlpha)
            {
                return;
            }

            SetGraphicsUI(affectChildren ? gameObject.GetComponentsInChildren<Graphic>() : gameObject.GetComponents<Graphic>(), newAlpha);
        }

        internal static void ChangeUIAlpha(this GameObject gameObject, float alphaDiff, bool affectChildren = true)
        {
            var alphaChange = alphaDiff * Time.deltaTime;
            gameObject.ChangeUIAlphaInternal(alphaChange, affectChildren);
        }

        internal static Color GetUIColor(this GameObject gameObject)
        {
            var graphic = gameObject.GetComponent<Graphic>();
            if (graphic != null)
            {
                return graphic.color;
            }

            Logger.LogError(
                "An exception occurred in Utils.GetUIColor(), GameObject has no Graphic component to grab color");
            return Color.white;
        }

        internal static Color GetUIColor(this Component component) => GetUIColor(component.gameObject);

        internal static Sprite GetSinkingSprite(this Sprite sprite) => sprite;

        internal static Sprite ExpandSprite(Sprite sprite, int expandAmount)
        {
            var doubleExpandAmount = expandAmount * 2;
            var spriteWidth = (int)sprite.textureRect.width;
            var spriteHeight = (int)sprite.textureRect.height;

            var colors = sprite.texture.GetPixels((int)sprite.textureRect.x, (int)sprite.textureRect.y, spriteWidth, spriteHeight);

            var newTexture = new Texture2D(spriteWidth + doubleExpandAmount, spriteHeight + doubleExpandAmount, TextureFormat.ARGB32, false)
            {
                filterMode = FilterMode.Point,
            };

            for (var y = 0; y < newTexture.height; y++)
            {
                for (var x = 0; x < newTexture.width; x++)
                {
                    newTexture.SetPixel(x, y, Color.clear);
                }
            }

            newTexture.SetPixels(expandAmount, expandAmount, spriteWidth, spriteHeight, colors);
            newTexture.Apply();

            var ret = Sprite.Create(
                newTexture,
                new Rect(0, 0, newTexture.width, newTexture.height),
                sprite.pivot,
                sprite.pixelsPerUnit,
                0,
                SpriteMeshType.FullRect,
                sprite.border + (Vector4.one * expandAmount));

            ret.name = "generated_" + sprite.name;

            return ret;
        }

        internal static RenderTexture BlitTemporary(RenderTexture source, Material material)
        {
            var destination = RenderTexture.GetTemporary(source.width, source.height, 0, source.graphicsFormat);
            Graphics.Blit(source, destination, material);
            return destination;
        }

        /// <summary>
        ///     Doesn't allow dark colors
        /// </summary>
        internal static Color[] GetRandomColors(this Sprite sprite)
        {
            var colors = sprite.texture.GetPixels(
                (int)sprite.textureRect.x,
                (int)sprite.textureRect.y,
                (int)sprite.textureRect.width,
                (int)sprite.textureRect.height);

            var ret = new HashSet<Color>();

            for (var i = 0; i < colors.Length; i++)
            {
                var color = colors[i];

                if (color.r + color.g + color.b > 0.75f)
                {
                    ret.Add(colors[i]);
                }
            }

            if (ret.Count == 0)
            {
                ret.Add(colors[Random.Range(0, colors.Length)]);
            }

            return ret.ToArray();
        }

        internal static Vector2 Clamp(this Vector2 main, Vector2 min, Vector2 max) => new Vector2(main.x.Clamp(min.x, max.x), main.y.Clamp(min.y, max.y));
    }
}