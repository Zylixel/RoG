Shader "Blur/Horizontal"
{
    Properties
    {
        _MainTex("Texture", 2D) = "white" {}
        _SampleSize("Sample Size", Int) = 5
        _Spread("Spread", float) = 1
    }

    SubShader
    {
        // No culling or depth
        Cull Off 
        ZWrite Off 
        ZTest Never
        Lighting Off
        Blend One Zero

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert(appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            sampler2D _MainTex;
            float4      _MainTex_TexelSize;

            int _SampleSize;
            float _Spread;

            inline float GaussianWeight(int x)
            {
                const float oneDivA2 = 1.0f / 1.28f;
                const float oneDivASqrt2Pi = 1.0f / 2.0053026197f;

                float x2 = pow(x, 2);
                float e = exp(-x2 * oneDivA2);

                return e * oneDivASqrt2Pi;
            }

            float4 frag(v2f i) : SV_Target
            {
               float4 summation = float4(0, 0, 0, 0);

               float appliedWeight = 0;

               for (int x = -_SampleSize; x <= _SampleSize; x++)
               {
                    float2 xyTexel = float2(x, 0) * _MainTex_TexelSize;
                    float2 texelPosition = i.uv + xyTexel;

                    float4 tex = tex2D(_MainTex, texelPosition);
                    
                    float weight = GaussianWeight(x / _Spread);
                    appliedWeight += weight;
                    summation += tex * weight;
               }

               return summation / appliedWeight;
            }

            ENDCG
        }
    }
}