﻿//UNITY_SHADER_NO_UPGRADE
#ifndef MYHLSLINCLUDE_INCLUDED
#define MYHLSLINCLUDE_INCLUDED

void Player_Dye_half(half4 Main, half1 Dye, half3 Color, bool UseColor, out half4 Out)
{
    [branch] if(UseColor && Dye > 0)
    {
        Out.x = Dye * Color.x;
        Out.y = Dye * Color.y;
        Out.z = Dye * Color.z;
        Out.w = 1;
    }
    else
    {
        Out = Main;
    }
}

void GameEntity_Flash_half(half1 Time, half1 Rate, half3 Color, out half4 Out)
{
    if(Rate <= 0)
    {
        Out.xyzw = 0;
        return;
    }

    half1 sinTime = (sin(Time * Rate) + 1) / 2;
    sinTime *= 0.5;

    Out.x = Color.x * sinTime;
    Out.y = Color.y * sinTime;
    Out.z = Color.z * sinTime;
    Out.w = 0;
}
void Override_half4_half(half4 Main, half4 Override, out half4 Out)
{
    [branch] if (Override.a > 0)
    {
        Out = Override;
    }
    else
    {
        Out = Main;
    }
}

void unity_noise_randomValue_half (float2 uv, out half Out)
{
    Out = frac(sin(dot(uv, float2(12.9898, 78.233)))*43758.5453);
}

#endif //MYHLSLINCLUDE_INCLUDED