Shader "Custom/Mask"
{
    Properties
    {
        _MainTex("Texture", 2D) = "white" {}
        Mask("Mask", 2D) = "white" {}
    }

    SubShader
    {
        Pass
        {
            // This clears the mask before 
            Color(0, 0, 0, 0)
        }

        Pass
        {
            Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert(appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            sampler2D _MainTex;
            sampler2D Mask;

            float4 frag(v2f i) : SV_Target
            {
               if (tex2D(Mask, i.uv).r != 1)
               {
                   discard;
               }

                return tex2D(_MainTex, i.uv);
            }
            ENDCG
        }
    }
}