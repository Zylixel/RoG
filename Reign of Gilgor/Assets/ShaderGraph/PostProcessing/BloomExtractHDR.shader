Shader "Post Processing/ExtractHDR"
{
    Properties
    {
        _MainTex("Texture", 2D) = "white" {}
        _Threshold("Threshold", float) = 1
    }
        SubShader
        {
            // No culling or depth
            Cull Off
            ZWrite Off
            ZTest Always
            Lighting Off
            Blend One Zero

            Pass
            {
                CGPROGRAM
                #pragma vertex vert
                #pragma fragment frag

                #include "UnityCG.cginc"

                struct appdata
                {
                    float4 vertex : POSITION;
                    float2 uv : TEXCOORD0;
                };

                struct v2f
                {
                    float2 uv : TEXCOORD0;
                    float4 vertex : SV_POSITION;
                };

                v2f vert(appdata v)
                {
                    v2f o;
                    o.vertex = UnityObjectToClipPos(v.vertex);
                    o.uv = v.uv;
                    return o;
                }

                sampler2D _MainTex;
                float _Threshold;

                float4 frag(v2f i) : SV_Target
                {
                   float4 output =  tex2D(_MainTex, i.uv) - float4(_Threshold, _Threshold, _Threshold, 0);
                   output = max(output, float4(0, 0, 0, 0));
                   output.a = (output.x + output.y + output.z) / 3;
                   return output;
                }

                ENDCG
            }
        }
}