Shader "UI/OutlineAnimatedCompiled"
{
    Properties
    {
        [NoScaleOffset] _MainTex("MainTex", 2D) = "white" {}
        [HDR]_OutlineColor("Outline Color", Color) = (0, 0, 0, 1)
        _OutlineThickness("Outline Thickness", Range(0, 6)) = 1
        [HDR]_OutlineColor2("Outline Color 2", Color) = (0.5173668, 0, 1.498039, 1)
        [HideInInspector][NoScaleOffset]unity_Lightmaps("unity_Lightmaps", 2DArray) = "" {}
        [HideInInspector][NoScaleOffset]unity_LightmapsInd("unity_LightmapsInd", 2DArray) = "" {}
        [HideInInspector][NoScaleOffset]unity_ShadowMasks("unity_ShadowMasks", 2DArray) = "" {}
    }
        SubShader
    {
        Tags
        {
            "RenderPipeline" = "UniversalPipeline"
            "RenderType" = "Transparent"
            "UniversalMaterialType" = "Unlit"
            "Queue" = "Transparent"
            "ShaderGraphShader" = "true"
            "ShaderGraphTargetId" = ""
        }
        Pass
        {
            Name "Sprite Unlit"
            Tags
            {
                "LightMode" = "Universal2D"
            }

        // Render State
        Cull Off
    Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
    ZTest LEqual
    ZWrite Off

        // Debug
        // <None>

        // --------------------------------------------------
        // Pass

        HLSLPROGRAM

        // Pragmas
        #pragma target 2.0
    #pragma exclude_renderers d3d11_9x
    #pragma vertex vert
    #pragma fragment frag

        // DotsInstancingOptions: <None>
        // HybridV1InjectedBuiltinProperties: <None>

        // Keywords
        #pragma multi_compile_fragment _ DEBUG_DISPLAY
        // GraphKeywords: <None>

        // Defines
        #define _SURFACE_TYPE_TRANSPARENT 1
        #define ATTRIBUTES_NEED_NORMAL
        #define ATTRIBUTES_NEED_TANGENT
        #define ATTRIBUTES_NEED_TEXCOORD0
        #define ATTRIBUTES_NEED_COLOR
        #define VARYINGS_NEED_POSITION_WS
        #define VARYINGS_NEED_TEXCOORD0
        #define VARYINGS_NEED_COLOR
        #define FEATURES_GRAPH_VERTEX
        /* WARNING: $splice Could not find named fragment 'PassInstancing' */
        #define SHADERPASS SHADERPASS_SPRITEUNLIT
        /* WARNING: $splice Could not find named fragment 'DotsInstancingVars' */

        // Includes
        /* WARNING: $splice Could not find named fragment 'CustomInterpolatorPreInclude' */

        #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
    #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Texture.hlsl"
    #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
    #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
    #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/TextureStack.hlsl"
    #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
    #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShaderPass.hlsl"

        // --------------------------------------------------
        // Structs and Packing

        /* WARNING: $splice Could not find named fragment 'CustomInterpolatorPrePacking' */

        struct Attributes
    {
         float3 positionOS : POSITION;
         float3 normalOS : NORMAL;
         float4 tangentOS : TANGENT;
         float4 uv0 : TEXCOORD0;
         float4 color : COLOR;
        #if UNITY_ANY_INSTANCING_ENABLED
         uint instanceID : INSTANCEID_SEMANTIC;
        #endif
    };
    struct Varyings
    {
         float4 positionCS : SV_POSITION;
         float3 positionWS;
         float4 texCoord0;
         float4 color;
        #if UNITY_ANY_INSTANCING_ENABLED
         uint instanceID : CUSTOM_INSTANCE_ID;
        #endif
        #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
         uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
        #endif
        #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
         uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
        #endif
        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
         FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
        #endif
    };
    struct SurfaceDescriptionInputs
    {
         float4 uv0;
         float3 TimeParameters;
    };
    struct VertexDescriptionInputs
    {
         float3 ObjectSpaceNormal;
         float3 ObjectSpaceTangent;
         float3 ObjectSpacePosition;
    };
    struct PackedVaryings
    {
         float4 positionCS : SV_POSITION;
         float3 interp0 : INTERP0;
         float4 interp1 : INTERP1;
         float4 interp2 : INTERP2;
        #if UNITY_ANY_INSTANCING_ENABLED
         uint instanceID : CUSTOM_INSTANCE_ID;
        #endif
        #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
         uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
        #endif
        #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
         uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
        #endif
        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
         FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
        #endif
    };

        PackedVaryings PackVaryings(Varyings input)
    {
        PackedVaryings output;
        ZERO_INITIALIZE(PackedVaryings, output);
        output.positionCS = input.positionCS;
        output.interp0.xyz = input.positionWS;
        output.interp1.xyzw = input.texCoord0;
        output.interp2.xyzw = input.color;
        #if UNITY_ANY_INSTANCING_ENABLED
        output.instanceID = input.instanceID;
        #endif
        #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
        output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
        #endif
        #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
        output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
        #endif
        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
        output.cullFace = input.cullFace;
        #endif
        return output;
    }

    Varyings UnpackVaryings(PackedVaryings input)
    {
        Varyings output;
        output.positionCS = input.positionCS;
        output.positionWS = input.interp0.xyz;
        output.texCoord0 = input.interp1.xyzw;
        output.color = input.interp2.xyzw;
        #if UNITY_ANY_INSTANCING_ENABLED
        output.instanceID = input.instanceID;
        #endif
        #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
        output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
        #endif
        #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
        output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
        #endif
        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
        output.cullFace = input.cullFace;
        #endif
        return output;
    }


    // --------------------------------------------------
    // Graph

    // Graph Properties
    CBUFFER_START(UnityPerMaterial)
float4 _MainTex_TexelSize;
float4 _OutlineColor;
float _OutlineThickness;
float4 _OutlineColor2;
CBUFFER_END

// Object and Global properties
SAMPLER(SamplerState_Linear_Repeat);
TEXTURE2D(_MainTex);
SAMPLER(sampler_MainTex);

// Graph Includes
// GraphIncludes: <None>

// -- Property used by ScenePickingPass
#ifdef SCENEPICKINGPASS
float4 _SelectionID;
#endif

// -- Properties used by SceneSelectionPass
#ifdef SCENESELECTIONPASS
int _ObjectId;
int _PassValue;
#endif

// Graph Functions

void Unity_Multiply_float_float(float A, float B, out float Out)
{
    Out = A * B;
}

void Unity_Combine_float(float R, float G, float B, float A, out float4 RGBA, out float3 RGB, out float2 RG)
{
    RGBA = float4(R, G, B, A);
    RGB = float3(R, G, B);
    RG = float2(R, G);
}

void Unity_TilingAndOffset_float(float2 UV, float2 Tiling, float2 Offset, out float2 Out)
{
    Out = UV * Tiling + Offset;
}

void Unity_Add_float(float A, float B, out float Out)
{
    Out = A + B;
}

void Unity_Negate_float2(float2 In, out float2 Out)
{
    Out = -1 * In;
}

void Unity_Clamp_float(float In, float Min, float Max, out float Out)
{
    Out = clamp(In, Min, Max);
}

void Unity_Subtract_float(float A, float B, out float Out)
{
    Out = A - B;
}

void Unity_Negate_float(float In, out float Out)
{
    Out = -1 * In;
}


float2 Unity_GradientNoise_Dir_float(float2 p)
{
    // Permutation and hashing used in webgl-nosie goo.gl/pX7HtC
    p = p % 289;
    // need full precision, otherwise half overflows when p > 1
    float x = float(34 * p.x + 1) * p.x % 289 + p.y;
    x = (34 * x + 1) * x % 289;
    x = frac(x / 41) * 2 - 1;
    return normalize(float2(x - floor(x + 0.5), abs(x) - 0.5));
}

void Unity_GradientNoise_float(float2 UV, float Scale, out float Out)
{
    float2 p = UV * Scale;
    float2 ip = floor(p);
    float2 fp = frac(p);
    float d00 = dot(Unity_GradientNoise_Dir_float(ip), fp);
    float d01 = dot(Unity_GradientNoise_Dir_float(ip + float2(0, 1)), fp - float2(0, 1));
    float d10 = dot(Unity_GradientNoise_Dir_float(ip + float2(1, 0)), fp - float2(1, 0));
    float d11 = dot(Unity_GradientNoise_Dir_float(ip + float2(1, 1)), fp - float2(1, 1));
    fp = fp * fp * fp * (fp * (fp * 6 - 15) + 10);
    Out = lerp(lerp(d00, d01, fp.y), lerp(d10, d11, fp.y), fp.x) + 0.5;
}

void Unity_Multiply_float4_float4(float4 A, float4 B, out float4 Out)
{
    Out = A * B;
}

void Unity_InvertColors_float(float In, float InvertColors, out float Out)
{
    Out = abs(InvertColors - In);
}

void Unity_Add_float4(float4 A, float4 B, out float4 Out)
{
    Out = A + B;
}

/* WARNING: $splice Could not find named fragment 'CustomInterpolatorPreVertex' */

// Graph Vertex
struct VertexDescription
{
    float3 Position;
    float3 Normal;
    float3 Tangent;
};

VertexDescription VertexDescriptionFunction(VertexDescriptionInputs IN)
{
    VertexDescription description = (VertexDescription)0;
    description.Position = IN.ObjectSpacePosition;
    description.Normal = IN.ObjectSpaceNormal;
    description.Tangent = IN.ObjectSpaceTangent;
    return description;
}

    #ifdef FEATURES_GRAPH_VERTEX
Varyings CustomInterpolatorPassThroughFunc(inout Varyings output, VertexDescription input)
{
return output;
}
#define CUSTOMINTERPOLATOR_VARYPASSTHROUGH_FUNC
#endif

// Graph Pixel
struct SurfaceDescription
{
    float3 BaseColor;
    float Alpha;
};

SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
{
    SurfaceDescription surface = (SurfaceDescription)0;
    UnityTexture2D _Property_6f49cb1870776880b8a228146d62c592_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
    float4 _SampleTexture2D_ca00c4abd578558a9fe0f72f7813821b_RGBA_0 = SAMPLE_TEXTURE2D(_Property_6f49cb1870776880b8a228146d62c592_Out_0.tex, _Property_6f49cb1870776880b8a228146d62c592_Out_0.samplerstate, _Property_6f49cb1870776880b8a228146d62c592_Out_0.GetTransformedUV(IN.uv0.xy));
    float _SampleTexture2D_ca00c4abd578558a9fe0f72f7813821b_R_4 = _SampleTexture2D_ca00c4abd578558a9fe0f72f7813821b_RGBA_0.r;
    float _SampleTexture2D_ca00c4abd578558a9fe0f72f7813821b_G_5 = _SampleTexture2D_ca00c4abd578558a9fe0f72f7813821b_RGBA_0.g;
    float _SampleTexture2D_ca00c4abd578558a9fe0f72f7813821b_B_6 = _SampleTexture2D_ca00c4abd578558a9fe0f72f7813821b_RGBA_0.b;
    float _SampleTexture2D_ca00c4abd578558a9fe0f72f7813821b_A_7 = _SampleTexture2D_ca00c4abd578558a9fe0f72f7813821b_RGBA_0.a;
    UnityTexture2D _Property_36754ea1527c5880bcc326ad278d5a0f_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
    float _Property_01298d44535deb84b1f3baa29c59a862_Out_0 = _OutlineThickness;
    float _Float_0e36c7956e4c3c8c8355512261057a0f_Out_0 = 0.01;
    float _Multiply_91f08a7dd9fb9f80903915e7f1873bce_Out_2;
    Unity_Multiply_float_float(_Property_01298d44535deb84b1f3baa29c59a862_Out_0, _Float_0e36c7956e4c3c8c8355512261057a0f_Out_0, _Multiply_91f08a7dd9fb9f80903915e7f1873bce_Out_2);
    float4 _Combine_f739ea2d35f3d78b8edebfd98c15854f_RGBA_4;
    float3 _Combine_f739ea2d35f3d78b8edebfd98c15854f_RGB_5;
    float2 _Combine_f739ea2d35f3d78b8edebfd98c15854f_RG_6;
    Unity_Combine_float(_Multiply_91f08a7dd9fb9f80903915e7f1873bce_Out_2, 0, 0, 0, _Combine_f739ea2d35f3d78b8edebfd98c15854f_RGBA_4, _Combine_f739ea2d35f3d78b8edebfd98c15854f_RGB_5, _Combine_f739ea2d35f3d78b8edebfd98c15854f_RG_6);
    float2 _TilingAndOffset_ed9bbffa4b27bb83b8cf3d06b2da2bc1_Out_3;
    Unity_TilingAndOffset_float(IN.uv0.xy, float2 (1, 1), _Combine_f739ea2d35f3d78b8edebfd98c15854f_RG_6, _TilingAndOffset_ed9bbffa4b27bb83b8cf3d06b2da2bc1_Out_3);
    float4 _SampleTexture2D_bcc87b28fe8cd3838c8d93c901a573dc_RGBA_0 = SAMPLE_TEXTURE2D(_Property_36754ea1527c5880bcc326ad278d5a0f_Out_0.tex, _Property_36754ea1527c5880bcc326ad278d5a0f_Out_0.samplerstate, _Property_36754ea1527c5880bcc326ad278d5a0f_Out_0.GetTransformedUV(_TilingAndOffset_ed9bbffa4b27bb83b8cf3d06b2da2bc1_Out_3));
    float _SampleTexture2D_bcc87b28fe8cd3838c8d93c901a573dc_R_4 = _SampleTexture2D_bcc87b28fe8cd3838c8d93c901a573dc_RGBA_0.r;
    float _SampleTexture2D_bcc87b28fe8cd3838c8d93c901a573dc_G_5 = _SampleTexture2D_bcc87b28fe8cd3838c8d93c901a573dc_RGBA_0.g;
    float _SampleTexture2D_bcc87b28fe8cd3838c8d93c901a573dc_B_6 = _SampleTexture2D_bcc87b28fe8cd3838c8d93c901a573dc_RGBA_0.b;
    float _SampleTexture2D_bcc87b28fe8cd3838c8d93c901a573dc_A_7 = _SampleTexture2D_bcc87b28fe8cd3838c8d93c901a573dc_RGBA_0.a;
    UnityTexture2D _Property_79eeb244316bd687b10e5b2b1c3c09ba_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
    float4 _Combine_a9de65a8f59f018685cc09744f89c005_RGBA_4;
    float3 _Combine_a9de65a8f59f018685cc09744f89c005_RGB_5;
    float2 _Combine_a9de65a8f59f018685cc09744f89c005_RG_6;
    Unity_Combine_float(0, _Multiply_91f08a7dd9fb9f80903915e7f1873bce_Out_2, 0, 0, _Combine_a9de65a8f59f018685cc09744f89c005_RGBA_4, _Combine_a9de65a8f59f018685cc09744f89c005_RGB_5, _Combine_a9de65a8f59f018685cc09744f89c005_RG_6);
    float2 _TilingAndOffset_6117b33aa7fb348b94080b89276c7107_Out_3;
    Unity_TilingAndOffset_float(IN.uv0.xy, float2 (1, 1), _Combine_a9de65a8f59f018685cc09744f89c005_RG_6, _TilingAndOffset_6117b33aa7fb348b94080b89276c7107_Out_3);
    float4 _SampleTexture2D_4faeded9d075ff8bb2df9247bcca8085_RGBA_0 = SAMPLE_TEXTURE2D(_Property_79eeb244316bd687b10e5b2b1c3c09ba_Out_0.tex, _Property_79eeb244316bd687b10e5b2b1c3c09ba_Out_0.samplerstate, _Property_79eeb244316bd687b10e5b2b1c3c09ba_Out_0.GetTransformedUV(_TilingAndOffset_6117b33aa7fb348b94080b89276c7107_Out_3));
    float _SampleTexture2D_4faeded9d075ff8bb2df9247bcca8085_R_4 = _SampleTexture2D_4faeded9d075ff8bb2df9247bcca8085_RGBA_0.r;
    float _SampleTexture2D_4faeded9d075ff8bb2df9247bcca8085_G_5 = _SampleTexture2D_4faeded9d075ff8bb2df9247bcca8085_RGBA_0.g;
    float _SampleTexture2D_4faeded9d075ff8bb2df9247bcca8085_B_6 = _SampleTexture2D_4faeded9d075ff8bb2df9247bcca8085_RGBA_0.b;
    float _SampleTexture2D_4faeded9d075ff8bb2df9247bcca8085_A_7 = _SampleTexture2D_4faeded9d075ff8bb2df9247bcca8085_RGBA_0.a;
    float _Add_e735e7f79ffe7c8a87dfac1a44223974_Out_2;
    Unity_Add_float(_SampleTexture2D_bcc87b28fe8cd3838c8d93c901a573dc_A_7, _SampleTexture2D_4faeded9d075ff8bb2df9247bcca8085_A_7, _Add_e735e7f79ffe7c8a87dfac1a44223974_Out_2);
    UnityTexture2D _Property_540c5d5ced56ea85b16ea434a72ee1cb_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
    float2 _Negate_d95c3dae23b6f884ba51160f835bf10d_Out_1;
    Unity_Negate_float2(_Combine_f739ea2d35f3d78b8edebfd98c15854f_RG_6, _Negate_d95c3dae23b6f884ba51160f835bf10d_Out_1);
    float2 _TilingAndOffset_58ef25c93de9e480b7eb36ca1e6a614d_Out_3;
    Unity_TilingAndOffset_float(IN.uv0.xy, float2 (1, 1), _Negate_d95c3dae23b6f884ba51160f835bf10d_Out_1, _TilingAndOffset_58ef25c93de9e480b7eb36ca1e6a614d_Out_3);
    float4 _SampleTexture2D_944f2b90573a428097515561e4da887b_RGBA_0 = SAMPLE_TEXTURE2D(_Property_540c5d5ced56ea85b16ea434a72ee1cb_Out_0.tex, _Property_540c5d5ced56ea85b16ea434a72ee1cb_Out_0.samplerstate, _Property_540c5d5ced56ea85b16ea434a72ee1cb_Out_0.GetTransformedUV(_TilingAndOffset_58ef25c93de9e480b7eb36ca1e6a614d_Out_3));
    float _SampleTexture2D_944f2b90573a428097515561e4da887b_R_4 = _SampleTexture2D_944f2b90573a428097515561e4da887b_RGBA_0.r;
    float _SampleTexture2D_944f2b90573a428097515561e4da887b_G_5 = _SampleTexture2D_944f2b90573a428097515561e4da887b_RGBA_0.g;
    float _SampleTexture2D_944f2b90573a428097515561e4da887b_B_6 = _SampleTexture2D_944f2b90573a428097515561e4da887b_RGBA_0.b;
    float _SampleTexture2D_944f2b90573a428097515561e4da887b_A_7 = _SampleTexture2D_944f2b90573a428097515561e4da887b_RGBA_0.a;
    UnityTexture2D _Property_5087f3611590848db8b63c6a3051783a_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
    float2 _Negate_8b39b6acee2ff58b9eb2f43140724e8b_Out_1;
    Unity_Negate_float2(_Combine_a9de65a8f59f018685cc09744f89c005_RG_6, _Negate_8b39b6acee2ff58b9eb2f43140724e8b_Out_1);
    float2 _TilingAndOffset_e5eadde02fe8768796d9e2855306f82a_Out_3;
    Unity_TilingAndOffset_float(IN.uv0.xy, float2 (1, 1), _Negate_8b39b6acee2ff58b9eb2f43140724e8b_Out_1, _TilingAndOffset_e5eadde02fe8768796d9e2855306f82a_Out_3);
    float4 _SampleTexture2D_257173ffec7c0a8e8b49d459011105eb_RGBA_0 = SAMPLE_TEXTURE2D(_Property_5087f3611590848db8b63c6a3051783a_Out_0.tex, _Property_5087f3611590848db8b63c6a3051783a_Out_0.samplerstate, _Property_5087f3611590848db8b63c6a3051783a_Out_0.GetTransformedUV(_TilingAndOffset_e5eadde02fe8768796d9e2855306f82a_Out_3));
    float _SampleTexture2D_257173ffec7c0a8e8b49d459011105eb_R_4 = _SampleTexture2D_257173ffec7c0a8e8b49d459011105eb_RGBA_0.r;
    float _SampleTexture2D_257173ffec7c0a8e8b49d459011105eb_G_5 = _SampleTexture2D_257173ffec7c0a8e8b49d459011105eb_RGBA_0.g;
    float _SampleTexture2D_257173ffec7c0a8e8b49d459011105eb_B_6 = _SampleTexture2D_257173ffec7c0a8e8b49d459011105eb_RGBA_0.b;
    float _SampleTexture2D_257173ffec7c0a8e8b49d459011105eb_A_7 = _SampleTexture2D_257173ffec7c0a8e8b49d459011105eb_RGBA_0.a;
    float _Add_b70ffd13e2bfa787a16934bb38c56ad8_Out_2;
    Unity_Add_float(_SampleTexture2D_944f2b90573a428097515561e4da887b_A_7, _SampleTexture2D_257173ffec7c0a8e8b49d459011105eb_A_7, _Add_b70ffd13e2bfa787a16934bb38c56ad8_Out_2);
    float _Add_fb2eecb17215178a9578efda250b0622_Out_2;
    Unity_Add_float(_Add_e735e7f79ffe7c8a87dfac1a44223974_Out_2, _Add_b70ffd13e2bfa787a16934bb38c56ad8_Out_2, _Add_fb2eecb17215178a9578efda250b0622_Out_2);
    float _Clamp_70668fad17e62a8a8a90bb17bb4ba576_Out_3;
    Unity_Clamp_float(_Add_fb2eecb17215178a9578efda250b0622_Out_2, 0, 1, _Clamp_70668fad17e62a8a8a90bb17bb4ba576_Out_3);
    float _Subtract_0a9f98e4fccd35869a733289a56b426f_Out_2;
    Unity_Subtract_float(_Clamp_70668fad17e62a8a8a90bb17bb4ba576_Out_3, _SampleTexture2D_ca00c4abd578558a9fe0f72f7813821b_A_7, _Subtract_0a9f98e4fccd35869a733289a56b426f_Out_2);
    float4 _Property_8be4e98aa4cf7188944fee28d7b832d6_Out_0 = IsGammaSpace() ? LinearToSRGB(_OutlineColor) : _OutlineColor;
    float _Float_588ef754ce116c84b9bf451566006897_Out_0 = 0.5;
    float _Multiply_d4d03d59dc6a4387af08504407e565a5_Out_2;
    Unity_Multiply_float_float(_Float_588ef754ce116c84b9bf451566006897_Out_0, IN.TimeParameters.x, _Multiply_d4d03d59dc6a4387af08504407e565a5_Out_2);
    float _Negate_af5c8c9e3c53378a8eb25352afd9e1ad_Out_1;
    Unity_Negate_float(_Multiply_d4d03d59dc6a4387af08504407e565a5_Out_2, _Negate_af5c8c9e3c53378a8eb25352afd9e1ad_Out_1);
    float4 _Combine_0998a1384f325386886742422e5bfad5_RGBA_4;
    float3 _Combine_0998a1384f325386886742422e5bfad5_RGB_5;
    float2 _Combine_0998a1384f325386886742422e5bfad5_RG_6;
    Unity_Combine_float(0, _Negate_af5c8c9e3c53378a8eb25352afd9e1ad_Out_1, 0, 0, _Combine_0998a1384f325386886742422e5bfad5_RGBA_4, _Combine_0998a1384f325386886742422e5bfad5_RGB_5, _Combine_0998a1384f325386886742422e5bfad5_RG_6);
    float2 _TilingAndOffset_4085eee7c842e68f87fd2a37188b26c5_Out_3;
    Unity_TilingAndOffset_float(IN.uv0.xy, float2 (1, 1), _Combine_0998a1384f325386886742422e5bfad5_RG_6, _TilingAndOffset_4085eee7c842e68f87fd2a37188b26c5_Out_3);
    float _GradientNoise_ce59526b9e338c8b9453f55e8864b628_Out_2;
    Unity_GradientNoise_float(_TilingAndOffset_4085eee7c842e68f87fd2a37188b26c5_Out_3, 1, _GradientNoise_ce59526b9e338c8b9453f55e8864b628_Out_2);
    float4 _Multiply_9fdaee2e7b8c0c8f95c83217d2ddcdff_Out_2;
    Unity_Multiply_float4_float4(_Property_8be4e98aa4cf7188944fee28d7b832d6_Out_0, (_GradientNoise_ce59526b9e338c8b9453f55e8864b628_Out_2.xxxx), _Multiply_9fdaee2e7b8c0c8f95c83217d2ddcdff_Out_2);
    float4 _Property_e34cb4a40363568095dae2103e0fcf65_Out_0 = IsGammaSpace() ? LinearToSRGB(_OutlineColor2) : _OutlineColor2;
    float _InvertColors_ef4a2e538ecdef87a5d924c57dbf6e5c_Out_1;
    float _InvertColors_ef4a2e538ecdef87a5d924c57dbf6e5c_InvertColors = float(1);
    Unity_InvertColors_float(_GradientNoise_ce59526b9e338c8b9453f55e8864b628_Out_2, _InvertColors_ef4a2e538ecdef87a5d924c57dbf6e5c_InvertColors, _InvertColors_ef4a2e538ecdef87a5d924c57dbf6e5c_Out_1);
    float4 _Multiply_4c1b4b742d53fc83b276c72f487df4bf_Out_2;
    Unity_Multiply_float4_float4(_Property_e34cb4a40363568095dae2103e0fcf65_Out_0, (_InvertColors_ef4a2e538ecdef87a5d924c57dbf6e5c_Out_1.xxxx), _Multiply_4c1b4b742d53fc83b276c72f487df4bf_Out_2);
    float4 _Add_d077c838c455ec839e3843c5ed3e1a60_Out_2;
    Unity_Add_float4(_Multiply_9fdaee2e7b8c0c8f95c83217d2ddcdff_Out_2, _Multiply_4c1b4b742d53fc83b276c72f487df4bf_Out_2, _Add_d077c838c455ec839e3843c5ed3e1a60_Out_2);
    float4 _Multiply_114e0caaf1aeba8b9a80a522dee82222_Out_2;
    Unity_Multiply_float4_float4((_Subtract_0a9f98e4fccd35869a733289a56b426f_Out_2.xxxx), _Add_d077c838c455ec839e3843c5ed3e1a60_Out_2, _Multiply_114e0caaf1aeba8b9a80a522dee82222_Out_2);
    float4 _Add_67bf90165aa63a81ac9cacd8dac2859c_Out_2;
    Unity_Add_float4(_SampleTexture2D_ca00c4abd578558a9fe0f72f7813821b_RGBA_0, _Multiply_114e0caaf1aeba8b9a80a522dee82222_Out_2, _Add_67bf90165aa63a81ac9cacd8dac2859c_Out_2);
    float _Split_ec1861c818a145948f680fdb146e1b39_R_1 = _Add_67bf90165aa63a81ac9cacd8dac2859c_Out_2[0];
    float _Split_ec1861c818a145948f680fdb146e1b39_G_2 = _Add_67bf90165aa63a81ac9cacd8dac2859c_Out_2[1];
    float _Split_ec1861c818a145948f680fdb146e1b39_B_3 = _Add_67bf90165aa63a81ac9cacd8dac2859c_Out_2[2];
    float _Split_ec1861c818a145948f680fdb146e1b39_A_4 = _Add_67bf90165aa63a81ac9cacd8dac2859c_Out_2[3];
    surface.BaseColor = (_Add_67bf90165aa63a81ac9cacd8dac2859c_Out_2.xyz);
    surface.Alpha = _Split_ec1861c818a145948f680fdb146e1b39_A_4;
    return surface;
}

// --------------------------------------------------
// Build Graph Inputs

VertexDescriptionInputs BuildVertexDescriptionInputs(Attributes input)
{
    VertexDescriptionInputs output;
    ZERO_INITIALIZE(VertexDescriptionInputs, output);

    output.ObjectSpaceNormal = input.normalOS;
    output.ObjectSpaceTangent = input.tangentOS.xyz;
    output.ObjectSpacePosition = input.positionOS;

    return output;
}
    SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
{
    SurfaceDescriptionInputs output;
    ZERO_INITIALIZE(SurfaceDescriptionInputs, output);







    output.uv0 = input.texCoord0;
    output.TimeParameters = _TimeParameters.xyz; // This is mainly for LW as HD overwrite this value
#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN                output.FaceSign =                                   IS_FRONT_VFACE(input.cullFace, true, false);
#else
#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
#endif
#undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN

    return output;
}

    // --------------------------------------------------
    // Main

    #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
#include "Packages/com.unity.render-pipelines.universal/Editor/2D/ShaderGraph/Includes/SpriteUnlitPass.hlsl"

    ENDHLSL
}
Pass
{
    Name "Sprite Unlit"
    Tags
    {
        "LightMode" = "UniversalForward"
    }

        // Render State
        Cull Off
    Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
    ZTest LEqual
    ZWrite Off

        // Debug
        // <None>

        // --------------------------------------------------
        // Pass

        HLSLPROGRAM

        // Pragmas
        #pragma target 2.0
    #pragma exclude_renderers d3d11_9x
    #pragma vertex vert
    #pragma fragment frag

        // DotsInstancingOptions: <None>
        // HybridV1InjectedBuiltinProperties: <None>

        // Keywords
        #pragma multi_compile_fragment _ DEBUG_DISPLAY
        // GraphKeywords: <None>

        // Defines
        #define _SURFACE_TYPE_TRANSPARENT 1
        #define ATTRIBUTES_NEED_NORMAL
        #define ATTRIBUTES_NEED_TANGENT
        #define ATTRIBUTES_NEED_TEXCOORD0
        #define ATTRIBUTES_NEED_COLOR
        #define VARYINGS_NEED_POSITION_WS
        #define VARYINGS_NEED_TEXCOORD0
        #define VARYINGS_NEED_COLOR
        #define FEATURES_GRAPH_VERTEX
        /* WARNING: $splice Could not find named fragment 'PassInstancing' */
        #define SHADERPASS SHADERPASS_SPRITEFORWARD
        /* WARNING: $splice Could not find named fragment 'DotsInstancingVars' */

        // Includes
        /* WARNING: $splice Could not find named fragment 'CustomInterpolatorPreInclude' */

        #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
    #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Texture.hlsl"
    #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
    #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
    #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/TextureStack.hlsl"
    #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
    #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShaderPass.hlsl"

        // --------------------------------------------------
        // Structs and Packing

        /* WARNING: $splice Could not find named fragment 'CustomInterpolatorPrePacking' */

        struct Attributes
    {
         float3 positionOS : POSITION;
         float3 normalOS : NORMAL;
         float4 tangentOS : TANGENT;
         float4 uv0 : TEXCOORD0;
         float4 color : COLOR;
        #if UNITY_ANY_INSTANCING_ENABLED
         uint instanceID : INSTANCEID_SEMANTIC;
        #endif
    };
    struct Varyings
    {
         float4 positionCS : SV_POSITION;
         float3 positionWS;
         float4 texCoord0;
         float4 color;
        #if UNITY_ANY_INSTANCING_ENABLED
         uint instanceID : CUSTOM_INSTANCE_ID;
        #endif
        #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
         uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
        #endif
        #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
         uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
        #endif
        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
         FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
        #endif
    };
    struct SurfaceDescriptionInputs
    {
         float4 uv0;
         float3 TimeParameters;
    };
    struct VertexDescriptionInputs
    {
         float3 ObjectSpaceNormal;
         float3 ObjectSpaceTangent;
         float3 ObjectSpacePosition;
    };
    struct PackedVaryings
    {
         float4 positionCS : SV_POSITION;
         float3 interp0 : INTERP0;
         float4 interp1 : INTERP1;
         float4 interp2 : INTERP2;
        #if UNITY_ANY_INSTANCING_ENABLED
         uint instanceID : CUSTOM_INSTANCE_ID;
        #endif
        #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
         uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
        #endif
        #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
         uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
        #endif
        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
         FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
        #endif
    };

        PackedVaryings PackVaryings(Varyings input)
    {
        PackedVaryings output;
        ZERO_INITIALIZE(PackedVaryings, output);
        output.positionCS = input.positionCS;
        output.interp0.xyz = input.positionWS;
        output.interp1.xyzw = input.texCoord0;
        output.interp2.xyzw = input.color;
        #if UNITY_ANY_INSTANCING_ENABLED
        output.instanceID = input.instanceID;
        #endif
        #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
        output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
        #endif
        #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
        output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
        #endif
        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
        output.cullFace = input.cullFace;
        #endif
        return output;
    }

    Varyings UnpackVaryings(PackedVaryings input)
    {
        Varyings output;
        output.positionCS = input.positionCS;
        output.positionWS = input.interp0.xyz;
        output.texCoord0 = input.interp1.xyzw;
        output.color = input.interp2.xyzw;
        #if UNITY_ANY_INSTANCING_ENABLED
        output.instanceID = input.instanceID;
        #endif
        #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
        output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
        #endif
        #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
        output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
        #endif
        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
        output.cullFace = input.cullFace;
        #endif
        return output;
    }


    // --------------------------------------------------
    // Graph

    // Graph Properties
    CBUFFER_START(UnityPerMaterial)
float4 _MainTex_TexelSize;
float4 _OutlineColor;
float _OutlineThickness;
float4 _OutlineColor2;
CBUFFER_END

// Object and Global properties
SAMPLER(SamplerState_Linear_Repeat);
TEXTURE2D(_MainTex);
SAMPLER(sampler_MainTex);

// Graph Includes
// GraphIncludes: <None>

// -- Property used by ScenePickingPass
#ifdef SCENEPICKINGPASS
float4 _SelectionID;
#endif

// -- Properties used by SceneSelectionPass
#ifdef SCENESELECTIONPASS
int _ObjectId;
int _PassValue;
#endif

// Graph Functions

void Unity_Multiply_float_float(float A, float B, out float Out)
{
    Out = A * B;
}

void Unity_Combine_float(float R, float G, float B, float A, out float4 RGBA, out float3 RGB, out float2 RG)
{
    RGBA = float4(R, G, B, A);
    RGB = float3(R, G, B);
    RG = float2(R, G);
}

void Unity_TilingAndOffset_float(float2 UV, float2 Tiling, float2 Offset, out float2 Out)
{
    Out = UV * Tiling + Offset;
}

void Unity_Add_float(float A, float B, out float Out)
{
    Out = A + B;
}

void Unity_Negate_float2(float2 In, out float2 Out)
{
    Out = -1 * In;
}

void Unity_Clamp_float(float In, float Min, float Max, out float Out)
{
    Out = clamp(In, Min, Max);
}

void Unity_Subtract_float(float A, float B, out float Out)
{
    Out = A - B;
}

void Unity_Negate_float(float In, out float Out)
{
    Out = -1 * In;
}


float2 Unity_GradientNoise_Dir_float(float2 p)
{
    // Permutation and hashing used in webgl-nosie goo.gl/pX7HtC
    p = p % 289;
    // need full precision, otherwise half overflows when p > 1
    float x = float(34 * p.x + 1) * p.x % 289 + p.y;
    x = (34 * x + 1) * x % 289;
    x = frac(x / 41) * 2 - 1;
    return normalize(float2(x - floor(x + 0.5), abs(x) - 0.5));
}

void Unity_GradientNoise_float(float2 UV, float Scale, out float Out)
{
    float2 p = UV * Scale;
    float2 ip = floor(p);
    float2 fp = frac(p);
    float d00 = dot(Unity_GradientNoise_Dir_float(ip), fp);
    float d01 = dot(Unity_GradientNoise_Dir_float(ip + float2(0, 1)), fp - float2(0, 1));
    float d10 = dot(Unity_GradientNoise_Dir_float(ip + float2(1, 0)), fp - float2(1, 0));
    float d11 = dot(Unity_GradientNoise_Dir_float(ip + float2(1, 1)), fp - float2(1, 1));
    fp = fp * fp * fp * (fp * (fp * 6 - 15) + 10);
    Out = lerp(lerp(d00, d01, fp.y), lerp(d10, d11, fp.y), fp.x) + 0.5;
}

void Unity_Multiply_float4_float4(float4 A, float4 B, out float4 Out)
{
    Out = A * B;
}

void Unity_InvertColors_float(float In, float InvertColors, out float Out)
{
    Out = abs(InvertColors - In);
}

void Unity_Add_float4(float4 A, float4 B, out float4 Out)
{
    Out = A + B;
}

/* WARNING: $splice Could not find named fragment 'CustomInterpolatorPreVertex' */

// Graph Vertex
struct VertexDescription
{
    float3 Position;
    float3 Normal;
    float3 Tangent;
};

VertexDescription VertexDescriptionFunction(VertexDescriptionInputs IN)
{
    VertexDescription description = (VertexDescription)0;
    description.Position = IN.ObjectSpacePosition;
    description.Normal = IN.ObjectSpaceNormal;
    description.Tangent = IN.ObjectSpaceTangent;
    return description;
}

    #ifdef FEATURES_GRAPH_VERTEX
Varyings CustomInterpolatorPassThroughFunc(inout Varyings output, VertexDescription input)
{
return output;
}
#define CUSTOMINTERPOLATOR_VARYPASSTHROUGH_FUNC
#endif

// Graph Pixel
struct SurfaceDescription
{
    float3 BaseColor;
    float Alpha;
};

SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
{
    SurfaceDescription surface = (SurfaceDescription)0;
    UnityTexture2D _Property_6f49cb1870776880b8a228146d62c592_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
    float4 _SampleTexture2D_ca00c4abd578558a9fe0f72f7813821b_RGBA_0 = SAMPLE_TEXTURE2D(_Property_6f49cb1870776880b8a228146d62c592_Out_0.tex, _Property_6f49cb1870776880b8a228146d62c592_Out_0.samplerstate, _Property_6f49cb1870776880b8a228146d62c592_Out_0.GetTransformedUV(IN.uv0.xy));
    float _SampleTexture2D_ca00c4abd578558a9fe0f72f7813821b_R_4 = _SampleTexture2D_ca00c4abd578558a9fe0f72f7813821b_RGBA_0.r;
    float _SampleTexture2D_ca00c4abd578558a9fe0f72f7813821b_G_5 = _SampleTexture2D_ca00c4abd578558a9fe0f72f7813821b_RGBA_0.g;
    float _SampleTexture2D_ca00c4abd578558a9fe0f72f7813821b_B_6 = _SampleTexture2D_ca00c4abd578558a9fe0f72f7813821b_RGBA_0.b;
    float _SampleTexture2D_ca00c4abd578558a9fe0f72f7813821b_A_7 = _SampleTexture2D_ca00c4abd578558a9fe0f72f7813821b_RGBA_0.a;
    UnityTexture2D _Property_36754ea1527c5880bcc326ad278d5a0f_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
    float _Property_01298d44535deb84b1f3baa29c59a862_Out_0 = _OutlineThickness;
    float _Float_0e36c7956e4c3c8c8355512261057a0f_Out_0 = 0.01;
    float _Multiply_91f08a7dd9fb9f80903915e7f1873bce_Out_2;
    Unity_Multiply_float_float(_Property_01298d44535deb84b1f3baa29c59a862_Out_0, _Float_0e36c7956e4c3c8c8355512261057a0f_Out_0, _Multiply_91f08a7dd9fb9f80903915e7f1873bce_Out_2);
    float4 _Combine_f739ea2d35f3d78b8edebfd98c15854f_RGBA_4;
    float3 _Combine_f739ea2d35f3d78b8edebfd98c15854f_RGB_5;
    float2 _Combine_f739ea2d35f3d78b8edebfd98c15854f_RG_6;
    Unity_Combine_float(_Multiply_91f08a7dd9fb9f80903915e7f1873bce_Out_2, 0, 0, 0, _Combine_f739ea2d35f3d78b8edebfd98c15854f_RGBA_4, _Combine_f739ea2d35f3d78b8edebfd98c15854f_RGB_5, _Combine_f739ea2d35f3d78b8edebfd98c15854f_RG_6);
    float2 _TilingAndOffset_ed9bbffa4b27bb83b8cf3d06b2da2bc1_Out_3;
    Unity_TilingAndOffset_float(IN.uv0.xy, float2 (1, 1), _Combine_f739ea2d35f3d78b8edebfd98c15854f_RG_6, _TilingAndOffset_ed9bbffa4b27bb83b8cf3d06b2da2bc1_Out_3);
    float4 _SampleTexture2D_bcc87b28fe8cd3838c8d93c901a573dc_RGBA_0 = SAMPLE_TEXTURE2D(_Property_36754ea1527c5880bcc326ad278d5a0f_Out_0.tex, _Property_36754ea1527c5880bcc326ad278d5a0f_Out_0.samplerstate, _Property_36754ea1527c5880bcc326ad278d5a0f_Out_0.GetTransformedUV(_TilingAndOffset_ed9bbffa4b27bb83b8cf3d06b2da2bc1_Out_3));
    float _SampleTexture2D_bcc87b28fe8cd3838c8d93c901a573dc_R_4 = _SampleTexture2D_bcc87b28fe8cd3838c8d93c901a573dc_RGBA_0.r;
    float _SampleTexture2D_bcc87b28fe8cd3838c8d93c901a573dc_G_5 = _SampleTexture2D_bcc87b28fe8cd3838c8d93c901a573dc_RGBA_0.g;
    float _SampleTexture2D_bcc87b28fe8cd3838c8d93c901a573dc_B_6 = _SampleTexture2D_bcc87b28fe8cd3838c8d93c901a573dc_RGBA_0.b;
    float _SampleTexture2D_bcc87b28fe8cd3838c8d93c901a573dc_A_7 = _SampleTexture2D_bcc87b28fe8cd3838c8d93c901a573dc_RGBA_0.a;
    UnityTexture2D _Property_79eeb244316bd687b10e5b2b1c3c09ba_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
    float4 _Combine_a9de65a8f59f018685cc09744f89c005_RGBA_4;
    float3 _Combine_a9de65a8f59f018685cc09744f89c005_RGB_5;
    float2 _Combine_a9de65a8f59f018685cc09744f89c005_RG_6;
    Unity_Combine_float(0, _Multiply_91f08a7dd9fb9f80903915e7f1873bce_Out_2, 0, 0, _Combine_a9de65a8f59f018685cc09744f89c005_RGBA_4, _Combine_a9de65a8f59f018685cc09744f89c005_RGB_5, _Combine_a9de65a8f59f018685cc09744f89c005_RG_6);
    float2 _TilingAndOffset_6117b33aa7fb348b94080b89276c7107_Out_3;
    Unity_TilingAndOffset_float(IN.uv0.xy, float2 (1, 1), _Combine_a9de65a8f59f018685cc09744f89c005_RG_6, _TilingAndOffset_6117b33aa7fb348b94080b89276c7107_Out_3);
    float4 _SampleTexture2D_4faeded9d075ff8bb2df9247bcca8085_RGBA_0 = SAMPLE_TEXTURE2D(_Property_79eeb244316bd687b10e5b2b1c3c09ba_Out_0.tex, _Property_79eeb244316bd687b10e5b2b1c3c09ba_Out_0.samplerstate, _Property_79eeb244316bd687b10e5b2b1c3c09ba_Out_0.GetTransformedUV(_TilingAndOffset_6117b33aa7fb348b94080b89276c7107_Out_3));
    float _SampleTexture2D_4faeded9d075ff8bb2df9247bcca8085_R_4 = _SampleTexture2D_4faeded9d075ff8bb2df9247bcca8085_RGBA_0.r;
    float _SampleTexture2D_4faeded9d075ff8bb2df9247bcca8085_G_5 = _SampleTexture2D_4faeded9d075ff8bb2df9247bcca8085_RGBA_0.g;
    float _SampleTexture2D_4faeded9d075ff8bb2df9247bcca8085_B_6 = _SampleTexture2D_4faeded9d075ff8bb2df9247bcca8085_RGBA_0.b;
    float _SampleTexture2D_4faeded9d075ff8bb2df9247bcca8085_A_7 = _SampleTexture2D_4faeded9d075ff8bb2df9247bcca8085_RGBA_0.a;
    float _Add_e735e7f79ffe7c8a87dfac1a44223974_Out_2;
    Unity_Add_float(_SampleTexture2D_bcc87b28fe8cd3838c8d93c901a573dc_A_7, _SampleTexture2D_4faeded9d075ff8bb2df9247bcca8085_A_7, _Add_e735e7f79ffe7c8a87dfac1a44223974_Out_2);
    UnityTexture2D _Property_540c5d5ced56ea85b16ea434a72ee1cb_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
    float2 _Negate_d95c3dae23b6f884ba51160f835bf10d_Out_1;
    Unity_Negate_float2(_Combine_f739ea2d35f3d78b8edebfd98c15854f_RG_6, _Negate_d95c3dae23b6f884ba51160f835bf10d_Out_1);
    float2 _TilingAndOffset_58ef25c93de9e480b7eb36ca1e6a614d_Out_3;
    Unity_TilingAndOffset_float(IN.uv0.xy, float2 (1, 1), _Negate_d95c3dae23b6f884ba51160f835bf10d_Out_1, _TilingAndOffset_58ef25c93de9e480b7eb36ca1e6a614d_Out_3);
    float4 _SampleTexture2D_944f2b90573a428097515561e4da887b_RGBA_0 = SAMPLE_TEXTURE2D(_Property_540c5d5ced56ea85b16ea434a72ee1cb_Out_0.tex, _Property_540c5d5ced56ea85b16ea434a72ee1cb_Out_0.samplerstate, _Property_540c5d5ced56ea85b16ea434a72ee1cb_Out_0.GetTransformedUV(_TilingAndOffset_58ef25c93de9e480b7eb36ca1e6a614d_Out_3));
    float _SampleTexture2D_944f2b90573a428097515561e4da887b_R_4 = _SampleTexture2D_944f2b90573a428097515561e4da887b_RGBA_0.r;
    float _SampleTexture2D_944f2b90573a428097515561e4da887b_G_5 = _SampleTexture2D_944f2b90573a428097515561e4da887b_RGBA_0.g;
    float _SampleTexture2D_944f2b90573a428097515561e4da887b_B_6 = _SampleTexture2D_944f2b90573a428097515561e4da887b_RGBA_0.b;
    float _SampleTexture2D_944f2b90573a428097515561e4da887b_A_7 = _SampleTexture2D_944f2b90573a428097515561e4da887b_RGBA_0.a;
    UnityTexture2D _Property_5087f3611590848db8b63c6a3051783a_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
    float2 _Negate_8b39b6acee2ff58b9eb2f43140724e8b_Out_1;
    Unity_Negate_float2(_Combine_a9de65a8f59f018685cc09744f89c005_RG_6, _Negate_8b39b6acee2ff58b9eb2f43140724e8b_Out_1);
    float2 _TilingAndOffset_e5eadde02fe8768796d9e2855306f82a_Out_3;
    Unity_TilingAndOffset_float(IN.uv0.xy, float2 (1, 1), _Negate_8b39b6acee2ff58b9eb2f43140724e8b_Out_1, _TilingAndOffset_e5eadde02fe8768796d9e2855306f82a_Out_3);
    float4 _SampleTexture2D_257173ffec7c0a8e8b49d459011105eb_RGBA_0 = SAMPLE_TEXTURE2D(_Property_5087f3611590848db8b63c6a3051783a_Out_0.tex, _Property_5087f3611590848db8b63c6a3051783a_Out_0.samplerstate, _Property_5087f3611590848db8b63c6a3051783a_Out_0.GetTransformedUV(_TilingAndOffset_e5eadde02fe8768796d9e2855306f82a_Out_3));
    float _SampleTexture2D_257173ffec7c0a8e8b49d459011105eb_R_4 = _SampleTexture2D_257173ffec7c0a8e8b49d459011105eb_RGBA_0.r;
    float _SampleTexture2D_257173ffec7c0a8e8b49d459011105eb_G_5 = _SampleTexture2D_257173ffec7c0a8e8b49d459011105eb_RGBA_0.g;
    float _SampleTexture2D_257173ffec7c0a8e8b49d459011105eb_B_6 = _SampleTexture2D_257173ffec7c0a8e8b49d459011105eb_RGBA_0.b;
    float _SampleTexture2D_257173ffec7c0a8e8b49d459011105eb_A_7 = _SampleTexture2D_257173ffec7c0a8e8b49d459011105eb_RGBA_0.a;
    float _Add_b70ffd13e2bfa787a16934bb38c56ad8_Out_2;
    Unity_Add_float(_SampleTexture2D_944f2b90573a428097515561e4da887b_A_7, _SampleTexture2D_257173ffec7c0a8e8b49d459011105eb_A_7, _Add_b70ffd13e2bfa787a16934bb38c56ad8_Out_2);
    float _Add_fb2eecb17215178a9578efda250b0622_Out_2;
    Unity_Add_float(_Add_e735e7f79ffe7c8a87dfac1a44223974_Out_2, _Add_b70ffd13e2bfa787a16934bb38c56ad8_Out_2, _Add_fb2eecb17215178a9578efda250b0622_Out_2);
    float _Clamp_70668fad17e62a8a8a90bb17bb4ba576_Out_3;
    Unity_Clamp_float(_Add_fb2eecb17215178a9578efda250b0622_Out_2, 0, 1, _Clamp_70668fad17e62a8a8a90bb17bb4ba576_Out_3);
    float _Subtract_0a9f98e4fccd35869a733289a56b426f_Out_2;
    Unity_Subtract_float(_Clamp_70668fad17e62a8a8a90bb17bb4ba576_Out_3, _SampleTexture2D_ca00c4abd578558a9fe0f72f7813821b_A_7, _Subtract_0a9f98e4fccd35869a733289a56b426f_Out_2);
    float4 _Property_8be4e98aa4cf7188944fee28d7b832d6_Out_0 = IsGammaSpace() ? LinearToSRGB(_OutlineColor) : _OutlineColor;
    float _Float_588ef754ce116c84b9bf451566006897_Out_0 = 0.5;
    float _Multiply_d4d03d59dc6a4387af08504407e565a5_Out_2;
    Unity_Multiply_float_float(_Float_588ef754ce116c84b9bf451566006897_Out_0, IN.TimeParameters.x, _Multiply_d4d03d59dc6a4387af08504407e565a5_Out_2);
    float _Negate_af5c8c9e3c53378a8eb25352afd9e1ad_Out_1;
    Unity_Negate_float(_Multiply_d4d03d59dc6a4387af08504407e565a5_Out_2, _Negate_af5c8c9e3c53378a8eb25352afd9e1ad_Out_1);
    float4 _Combine_0998a1384f325386886742422e5bfad5_RGBA_4;
    float3 _Combine_0998a1384f325386886742422e5bfad5_RGB_5;
    float2 _Combine_0998a1384f325386886742422e5bfad5_RG_6;
    Unity_Combine_float(0, _Negate_af5c8c9e3c53378a8eb25352afd9e1ad_Out_1, 0, 0, _Combine_0998a1384f325386886742422e5bfad5_RGBA_4, _Combine_0998a1384f325386886742422e5bfad5_RGB_5, _Combine_0998a1384f325386886742422e5bfad5_RG_6);
    float2 _TilingAndOffset_4085eee7c842e68f87fd2a37188b26c5_Out_3;
    Unity_TilingAndOffset_float(IN.uv0.xy, float2 (1, 1), _Combine_0998a1384f325386886742422e5bfad5_RG_6, _TilingAndOffset_4085eee7c842e68f87fd2a37188b26c5_Out_3);
    float _GradientNoise_ce59526b9e338c8b9453f55e8864b628_Out_2;
    Unity_GradientNoise_float(_TilingAndOffset_4085eee7c842e68f87fd2a37188b26c5_Out_3, 1, _GradientNoise_ce59526b9e338c8b9453f55e8864b628_Out_2);
    float4 _Multiply_9fdaee2e7b8c0c8f95c83217d2ddcdff_Out_2;
    Unity_Multiply_float4_float4(_Property_8be4e98aa4cf7188944fee28d7b832d6_Out_0, (_GradientNoise_ce59526b9e338c8b9453f55e8864b628_Out_2.xxxx), _Multiply_9fdaee2e7b8c0c8f95c83217d2ddcdff_Out_2);
    float4 _Property_e34cb4a40363568095dae2103e0fcf65_Out_0 = IsGammaSpace() ? LinearToSRGB(_OutlineColor2) : _OutlineColor2;
    float _InvertColors_ef4a2e538ecdef87a5d924c57dbf6e5c_Out_1;
    float _InvertColors_ef4a2e538ecdef87a5d924c57dbf6e5c_InvertColors = float(1);
    Unity_InvertColors_float(_GradientNoise_ce59526b9e338c8b9453f55e8864b628_Out_2, _InvertColors_ef4a2e538ecdef87a5d924c57dbf6e5c_InvertColors, _InvertColors_ef4a2e538ecdef87a5d924c57dbf6e5c_Out_1);
    float4 _Multiply_4c1b4b742d53fc83b276c72f487df4bf_Out_2;
    Unity_Multiply_float4_float4(_Property_e34cb4a40363568095dae2103e0fcf65_Out_0, (_InvertColors_ef4a2e538ecdef87a5d924c57dbf6e5c_Out_1.xxxx), _Multiply_4c1b4b742d53fc83b276c72f487df4bf_Out_2);
    float4 _Add_d077c838c455ec839e3843c5ed3e1a60_Out_2;
    Unity_Add_float4(_Multiply_9fdaee2e7b8c0c8f95c83217d2ddcdff_Out_2, _Multiply_4c1b4b742d53fc83b276c72f487df4bf_Out_2, _Add_d077c838c455ec839e3843c5ed3e1a60_Out_2);
    float4 _Multiply_114e0caaf1aeba8b9a80a522dee82222_Out_2;
    Unity_Multiply_float4_float4((_Subtract_0a9f98e4fccd35869a733289a56b426f_Out_2.xxxx), _Add_d077c838c455ec839e3843c5ed3e1a60_Out_2, _Multiply_114e0caaf1aeba8b9a80a522dee82222_Out_2);
    float4 _Add_67bf90165aa63a81ac9cacd8dac2859c_Out_2;
    Unity_Add_float4(_SampleTexture2D_ca00c4abd578558a9fe0f72f7813821b_RGBA_0, _Multiply_114e0caaf1aeba8b9a80a522dee82222_Out_2, _Add_67bf90165aa63a81ac9cacd8dac2859c_Out_2);
    float _Split_ec1861c818a145948f680fdb146e1b39_R_1 = _Add_67bf90165aa63a81ac9cacd8dac2859c_Out_2[0];
    float _Split_ec1861c818a145948f680fdb146e1b39_G_2 = _Add_67bf90165aa63a81ac9cacd8dac2859c_Out_2[1];
    float _Split_ec1861c818a145948f680fdb146e1b39_B_3 = _Add_67bf90165aa63a81ac9cacd8dac2859c_Out_2[2];
    float _Split_ec1861c818a145948f680fdb146e1b39_A_4 = _Add_67bf90165aa63a81ac9cacd8dac2859c_Out_2[3];
    surface.BaseColor = (_Add_67bf90165aa63a81ac9cacd8dac2859c_Out_2.xyz);
    surface.Alpha = _Split_ec1861c818a145948f680fdb146e1b39_A_4;
    return surface;
}

// --------------------------------------------------
// Build Graph Inputs

VertexDescriptionInputs BuildVertexDescriptionInputs(Attributes input)
{
    VertexDescriptionInputs output;
    ZERO_INITIALIZE(VertexDescriptionInputs, output);

    output.ObjectSpaceNormal = input.normalOS;
    output.ObjectSpaceTangent = input.tangentOS.xyz;
    output.ObjectSpacePosition = input.positionOS;

    return output;
}
    SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
{
    SurfaceDescriptionInputs output;
    ZERO_INITIALIZE(SurfaceDescriptionInputs, output);







    output.uv0 = input.texCoord0;
    output.TimeParameters = _TimeParameters.xyz; // This is mainly for LW as HD overwrite this value
#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN                output.FaceSign =                                   IS_FRONT_VFACE(input.cullFace, true, false);
#else
#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
#endif
#undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN

    return output;
}

    // --------------------------------------------------
    // Main

    #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
#include "Packages/com.unity.render-pipelines.universal/Editor/2D/ShaderGraph/Includes/SpriteUnlitPass.hlsl"

    ENDHLSL
}
    }
        CustomEditor "UnityEditor.ShaderGraph.GenericShaderGraphMaterialGUI"
        FallBack "Hidden/Shader Graph/FallbackError"
}