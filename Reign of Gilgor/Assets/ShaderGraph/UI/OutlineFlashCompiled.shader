Shader "UI/OutlineFlashShaderCompiled"
{
    Properties
    {
        [NoScaleOffset] _MainTex("Main", 2D) = "white" {}
        [NoScaleOffset]_Mask("FlashMask", 2D) = "white" {}
        _FlashRate("FlashRate", Float) = 10
        _FlashColor("FlashColor", Color) = (1, 1, 1, 0)
        OutlineThickness("OutlineThickness", Float) = 0.125
        OutlineColor("OutlineColor", Color) = (0, 0, 0, 1)
        ObjectScale("Object Scale", Float) = 1
        _Alpha("Alpha", Float) = 0
        [NonModifiableTextureData][NoScaleOffset]_OutlineBackdrop_bb9928ba826749338c5c4e9b5aab4482_ShadowMask_1013696677("Texture2D", 2D) = "white" {}
        [HideInInspector][NoScaleOffset]unity_Lightmaps("unity_Lightmaps", 2DArray) = "" {}
        [HideInInspector][NoScaleOffset]unity_LightmapsInd("unity_LightmapsInd", 2DArray) = "" {}
        [HideInInspector][NoScaleOffset]unity_ShadowMasks("unity_ShadowMasks", 2DArray) = "" {}
    }
        SubShader
    {
        Tags
        {
            "RenderPipeline" = "UniversalPipeline"
            "RenderType" = "Transparent"
            "UniversalMaterialType" = "Unlit"
            "Queue" = "Transparent"
            "ShaderGraphShader" = "true"
            "ShaderGraphTargetId" = ""
        }
        Pass
        {
            Name "Sprite Unlit"
            Tags
            {
                "LightMode" = "Universal2D"
            }

        // Render State
        Cull Off
    Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
    ZTest LEqual
    ZWrite Off

        // Debug
        // <None>

        // --------------------------------------------------
        // Pass

        HLSLPROGRAM

        // Pragmas
        #pragma target 2.0
    #pragma exclude_renderers d3d11_9x
    #pragma vertex vert
    #pragma fragment frag

        // DotsInstancingOptions: <None>
        // HybridV1InjectedBuiltinProperties: <None>

        // Keywords
        #pragma multi_compile_fragment _ DEBUG_DISPLAY
        // GraphKeywords: <None>

        // Defines
        #define _SURFACE_TYPE_TRANSPARENT 1
        #define ATTRIBUTES_NEED_NORMAL
        #define ATTRIBUTES_NEED_TANGENT
        #define ATTRIBUTES_NEED_TEXCOORD0
        #define ATTRIBUTES_NEED_TEXCOORD1
        #define ATTRIBUTES_NEED_TEXCOORD2
        #define ATTRIBUTES_NEED_TEXCOORD3
        #define ATTRIBUTES_NEED_COLOR
        #define VARYINGS_NEED_POSITION_WS
        #define VARYINGS_NEED_TEXCOORD0
        #define VARYINGS_NEED_TEXCOORD1
        #define VARYINGS_NEED_TEXCOORD2
        #define VARYINGS_NEED_TEXCOORD3
        #define VARYINGS_NEED_COLOR
        #define FEATURES_GRAPH_VERTEX
        /* WARNING: $splice Could not find named fragment 'PassInstancing' */
        #define SHADERPASS SHADERPASS_SPRITEUNLIT
        /* WARNING: $splice Could not find named fragment 'DotsInstancingVars' */

        // Includes
        /* WARNING: $splice Could not find named fragment 'CustomInterpolatorPreInclude' */

        #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
    #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Texture.hlsl"
    #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
    #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
    #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/TextureStack.hlsl"
    #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
    #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShaderPass.hlsl"

        // --------------------------------------------------
        // Structs and Packing

        /* WARNING: $splice Could not find named fragment 'CustomInterpolatorPrePacking' */

        struct Attributes
    {
         float3 positionOS : POSITION;
         float3 normalOS : NORMAL;
         float4 tangentOS : TANGENT;
         float4 uv0 : TEXCOORD0;
         float4 uv1 : TEXCOORD1;
         float4 uv2 : TEXCOORD2;
         float4 uv3 : TEXCOORD3;
         float4 color : COLOR;
        #if UNITY_ANY_INSTANCING_ENABLED
         uint instanceID : INSTANCEID_SEMANTIC;
        #endif
    };
    struct Varyings
    {
         float4 positionCS : SV_POSITION;
         float3 positionWS;
         float4 texCoord0;
         float4 texCoord1;
         float4 texCoord2;
         float4 texCoord3;
         float4 color;
        #if UNITY_ANY_INSTANCING_ENABLED
         uint instanceID : CUSTOM_INSTANCE_ID;
        #endif
        #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
         uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
        #endif
        #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
         uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
        #endif
        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
         FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
        #endif
    };
    struct SurfaceDescriptionInputs
    {
         float4 uv0;
         float4 uv1;
         float4 uv2;
         float4 uv3;
         float3 TimeParameters;
    };
    struct VertexDescriptionInputs
    {
         float3 ObjectSpaceNormal;
         float3 ObjectSpaceTangent;
         float3 ObjectSpacePosition;
    };
    struct PackedVaryings
    {
         float4 positionCS : SV_POSITION;
         float3 interp0 : INTERP0;
         float4 interp1 : INTERP1;
         float4 interp2 : INTERP2;
         float4 interp3 : INTERP3;
         float4 interp4 : INTERP4;
         float4 interp5 : INTERP5;
        #if UNITY_ANY_INSTANCING_ENABLED
         uint instanceID : CUSTOM_INSTANCE_ID;
        #endif
        #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
         uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
        #endif
        #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
         uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
        #endif
        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
         FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
        #endif
    };

        PackedVaryings PackVaryings(Varyings input)
    {
        PackedVaryings output;
        ZERO_INITIALIZE(PackedVaryings, output);
        output.positionCS = input.positionCS;
        output.interp0.xyz = input.positionWS;
        output.interp1.xyzw = input.texCoord0;
        output.interp2.xyzw = input.texCoord1;
        output.interp3.xyzw = input.texCoord2;
        output.interp4.xyzw = input.texCoord3;
        output.interp5.xyzw = input.color;
        #if UNITY_ANY_INSTANCING_ENABLED
        output.instanceID = input.instanceID;
        #endif
        #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
        output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
        #endif
        #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
        output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
        #endif
        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
        output.cullFace = input.cullFace;
        #endif
        return output;
    }

    Varyings UnpackVaryings(PackedVaryings input)
    {
        Varyings output;
        output.positionCS = input.positionCS;
        output.positionWS = input.interp0.xyz;
        output.texCoord0 = input.interp1.xyzw;
        output.texCoord1 = input.interp2.xyzw;
        output.texCoord2 = input.interp3.xyzw;
        output.texCoord3 = input.interp4.xyzw;
        output.color = input.interp5.xyzw;
        #if UNITY_ANY_INSTANCING_ENABLED
        output.instanceID = input.instanceID;
        #endif
        #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
        output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
        #endif
        #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
        output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
        #endif
        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
        output.cullFace = input.cullFace;
        #endif
        return output;
    }


    // --------------------------------------------------
    // Graph

    // Graph Properties
    CBUFFER_START(UnityPerMaterial)
float4 _OutlineBackdrop_bb9928ba826749338c5c4e9b5aab4482_ShadowMask_1013696677_TexelSize;
float4 _MainTex_TexelSize;
float4 _Mask_TexelSize;
half _FlashRate;
half4 _FlashColor;
half OutlineThickness;
half4 OutlineColor;
half ObjectScale;
half _Alpha;
CBUFFER_END

// Object and Global properties
SAMPLER(SamplerState_Linear_Repeat);
TEXTURE2D(_OutlineBackdrop_bb9928ba826749338c5c4e9b5aab4482_ShadowMask_1013696677);
SAMPLER(sampler_OutlineBackdrop_bb9928ba826749338c5c4e9b5aab4482_ShadowMask_1013696677);
TEXTURE2D(_MainTex);
SAMPLER(sampler_MainTex);
TEXTURE2D(_Mask);
SAMPLER(sampler_Mask);

// Graph Includes
#include "Assets/ShaderGraph/CustomFunctionShaders.hlsl"

// -- Property used by ScenePickingPass
#ifdef SCENEPICKINGPASS
float4 _SelectionID;
#endif

// -- Properties used by SceneSelectionPass
#ifdef SCENESELECTIONPASS
int _ObjectId;
int _PassValue;
#endif

// Graph Functions

void Unity_ColorMask_half(half3 In, half3 MaskColor, half Range, out half Out, half Fuzziness)
{
    half Distance = distance(MaskColor, In);
    Out = saturate(1 - (Distance - Range) / max(Fuzziness, 1e-5));
}

void Unity_Multiply_half4_half4(half4 A, half4 B, out half4 Out)
{
    Out = A * B;
}

void Unity_Comparison_GreaterOrEqual_half(half A, half B, out half Out)
{
    Out = A >= B ? 1 : 0;
}

void Unity_Comparison_Equal_half(half A, half B, out half Out)
{
    Out = A == B ? 1 : 0;
}

void Unity_Or_half(half A, half B, out half Out)
{
    Out = A || B;
}

void Unity_Multiply_half_half(half A, half B, out half Out)
{
Out = A * B;
}

void Unity_Reciprocal_Fast_half2(half2 In, out half2 Out)
{
    Out = rcp(In);
}

void Unity_Reciprocal_half(half In, out half Out)
{
    Out = 1.0 / In;
}

void Unity_Multiply_half2_half2(half2 A, half2 B, out half2 Out)
{
Out = A * B;
}

void Unity_Add_half2(half2 A, half2 B, out half2 Out)
{
    Out = A + B;
}

struct Bindings_OutlineSample_5935d51de82a0eb439fe5be3af628c2c_half
{
half4 uv0;
half4 uv1;
half4 uv2;
half4 uv3;
};

void SG_OutlineSample_5935d51de82a0eb439fe5be3af628c2c_half(UnityTexture2D Texture2D_BADD5F9F, half Vector1_A2CEEF4B, half2 Vector2_950F9506, half Object_Scale, Bindings_OutlineSample_5935d51de82a0eb439fe5be3af628c2c_half IN, out half A_2)
{
UnityTexture2D _Property_a462cdbbc4e21f8299300428a67fb356_Out_0 = Texture2D_BADD5F9F;
half4 _UV_cf3003d855f53a80b12146a03d000837_Out_0 = IN.uv0;
UnityTexture2D _Property_40637bc593d246a380d158c5e41e8fb4_Out_0 = Texture2D_BADD5F9F;
half _TexelSize_361811e552ca4f1ab2e2b07e4ce98a24_Width_0 = _Property_40637bc593d246a380d158c5e41e8fb4_Out_0.texelSize.z;
half _TexelSize_361811e552ca4f1ab2e2b07e4ce98a24_Height_2 = _Property_40637bc593d246a380d158c5e41e8fb4_Out_0.texelSize.w;
half2 _Vector2_3d8fdf32f46a978c8b6a6275158b40aa_Out_0 = half2(_TexelSize_361811e552ca4f1ab2e2b07e4ce98a24_Width_0, _TexelSize_361811e552ca4f1ab2e2b07e4ce98a24_Height_2);
half2 _Reciprocal_c66fc76f3df6268ea7829e24c38439ac_Out_1;
Unity_Reciprocal_Fast_half2(_Vector2_3d8fdf32f46a978c8b6a6275158b40aa_Out_0, _Reciprocal_c66fc76f3df6268ea7829e24c38439ac_Out_1);
half _Property_e3976a49f31d8a85a0e8a0580baef75a_Out_0 = Vector1_A2CEEF4B;
half _Property_4e136003b70c4ab4add87ce76c8d759d_Out_0 = Object_Scale;
half _Reciprocal_7ddf14c974bc47ba9c8634f3ffd3b82a_Out_1;
Unity_Reciprocal_half(_Property_4e136003b70c4ab4add87ce76c8d759d_Out_0, _Reciprocal_7ddf14c974bc47ba9c8634f3ffd3b82a_Out_1);
half _Multiply_2ee2782e12ca4e818b0b048616704bde_Out_2;
Unity_Multiply_half_half(_Property_e3976a49f31d8a85a0e8a0580baef75a_Out_0, _Reciprocal_7ddf14c974bc47ba9c8634f3ffd3b82a_Out_1, _Multiply_2ee2782e12ca4e818b0b048616704bde_Out_2);
half2 _Multiply_b3e9757076a048849db0fa0d20333b85_Out_2;
Unity_Multiply_half2_half2(_Reciprocal_c66fc76f3df6268ea7829e24c38439ac_Out_1, (_Multiply_2ee2782e12ca4e818b0b048616704bde_Out_2.xx), _Multiply_b3e9757076a048849db0fa0d20333b85_Out_2);
half2 _Property_8d6f718782319580b294ce28423ec69a_Out_0 = Vector2_950F9506;
half2 _Multiply_cfab4d800a57e78781f71bbb890e5a7e_Out_2;
Unity_Multiply_half2_half2(_Multiply_b3e9757076a048849db0fa0d20333b85_Out_2, _Property_8d6f718782319580b294ce28423ec69a_Out_0, _Multiply_cfab4d800a57e78781f71bbb890e5a7e_Out_2);
half2 _Add_e01d035dd81b8d8da5d31a131e67ace9_Out_2;
Unity_Add_half2((_UV_cf3003d855f53a80b12146a03d000837_Out_0.xy), _Multiply_cfab4d800a57e78781f71bbb890e5a7e_Out_2, _Add_e01d035dd81b8d8da5d31a131e67ace9_Out_2);
half4 _SampleTexture2D_1c01dd08f4acb78e818fce32364589b4_RGBA_0 = SAMPLE_TEXTURE2D(_Property_a462cdbbc4e21f8299300428a67fb356_Out_0.tex, _Property_a462cdbbc4e21f8299300428a67fb356_Out_0.samplerstate, _Property_a462cdbbc4e21f8299300428a67fb356_Out_0.GetTransformedUV(_Add_e01d035dd81b8d8da5d31a131e67ace9_Out_2));
half _SampleTexture2D_1c01dd08f4acb78e818fce32364589b4_R_4 = _SampleTexture2D_1c01dd08f4acb78e818fce32364589b4_RGBA_0.r;
half _SampleTexture2D_1c01dd08f4acb78e818fce32364589b4_G_5 = _SampleTexture2D_1c01dd08f4acb78e818fce32364589b4_RGBA_0.g;
half _SampleTexture2D_1c01dd08f4acb78e818fce32364589b4_B_6 = _SampleTexture2D_1c01dd08f4acb78e818fce32364589b4_RGBA_0.b;
half _SampleTexture2D_1c01dd08f4acb78e818fce32364589b4_A_7 = _SampleTexture2D_1c01dd08f4acb78e818fce32364589b4_RGBA_0.a;
A_2 = _SampleTexture2D_1c01dd08f4acb78e818fce32364589b4_A_7;
}

void Unity_Add_half(half A, half B, out half Out)
{
    Out = A + B;
}

void Unity_Branch_half(half Predicate, half True, half False, out half Out)
{
    Out = Predicate ? True : False;
}

void Unity_Saturate_half(half In, out half Out)
{
    Out = saturate(In);
}

void Unity_Branch_half4(half Predicate, half4 True, half4 False, out half4 Out)
{
    Out = Predicate ? True : False;
}

struct Bindings_OutlineSubGraph_cca9a3cdf4ebbbf47b3960f57cef445d_half
{
half4 uv0;
half4 uv1;
half4 uv2;
half4 uv3;
};

void SG_OutlineSubGraph_cca9a3cdf4ebbbf47b3960f57cef445d_half(UnityTexture2D _Main_Texture, UnityTexture2D ShadowMask, half _OutlineThickness, half4 _OutlineColor, half Object_Scale, half _Sample_Corners, half _Enable_Outline, Bindings_OutlineSubGraph_cca9a3cdf4ebbbf47b3960f57cef445d_half IN, out half4 Out_1)
{
half _Property_c255289e5e904c44b6f9179ec57e8af2_Out_0 = _Enable_Outline;
UnityTexture2D _Property_cdc04c299f174f8fba8ac249e4a2b232_Out_0 = _Main_Texture;
half4 _SampleTexture2D_29d7a1f29de44f6cbd20e287d4579b4c_RGBA_0 = SAMPLE_TEXTURE2D(_Property_cdc04c299f174f8fba8ac249e4a2b232_Out_0.tex, _Property_cdc04c299f174f8fba8ac249e4a2b232_Out_0.samplerstate, _Property_cdc04c299f174f8fba8ac249e4a2b232_Out_0.GetTransformedUV(IN.uv0.xy));
half _SampleTexture2D_29d7a1f29de44f6cbd20e287d4579b4c_R_4 = _SampleTexture2D_29d7a1f29de44f6cbd20e287d4579b4c_RGBA_0.r;
half _SampleTexture2D_29d7a1f29de44f6cbd20e287d4579b4c_G_5 = _SampleTexture2D_29d7a1f29de44f6cbd20e287d4579b4c_RGBA_0.g;
half _SampleTexture2D_29d7a1f29de44f6cbd20e287d4579b4c_B_6 = _SampleTexture2D_29d7a1f29de44f6cbd20e287d4579b4c_RGBA_0.b;
half _SampleTexture2D_29d7a1f29de44f6cbd20e287d4579b4c_A_7 = _SampleTexture2D_29d7a1f29de44f6cbd20e287d4579b4c_RGBA_0.a;
half _Comparison_113df203248543e0b192348b1a8ca785_Out_2;
Unity_Comparison_GreaterOrEqual_half(_SampleTexture2D_29d7a1f29de44f6cbd20e287d4579b4c_A_7, 0.5, _Comparison_113df203248543e0b192348b1a8ca785_Out_2);
UnityTexture2D _Property_6bef216196d24d9f8ac29403d83e213f_Out_0 = ShadowMask;
half4 _SampleTexture2D_a44f919f050c4fed96551b3631e81f51_RGBA_0 = SAMPLE_TEXTURE2D(_Property_6bef216196d24d9f8ac29403d83e213f_Out_0.tex, _Property_6bef216196d24d9f8ac29403d83e213f_Out_0.samplerstate, _Property_6bef216196d24d9f8ac29403d83e213f_Out_0.GetTransformedUV(IN.uv0.xy));
half _SampleTexture2D_a44f919f050c4fed96551b3631e81f51_R_4 = _SampleTexture2D_a44f919f050c4fed96551b3631e81f51_RGBA_0.r;
half _SampleTexture2D_a44f919f050c4fed96551b3631e81f51_G_5 = _SampleTexture2D_a44f919f050c4fed96551b3631e81f51_RGBA_0.g;
half _SampleTexture2D_a44f919f050c4fed96551b3631e81f51_B_6 = _SampleTexture2D_a44f919f050c4fed96551b3631e81f51_RGBA_0.b;
half _SampleTexture2D_a44f919f050c4fed96551b3631e81f51_A_7 = _SampleTexture2D_a44f919f050c4fed96551b3631e81f51_RGBA_0.a;
half _Comparison_0d24209b0fb341058e16996fba48a926_Out_2;
Unity_Comparison_Equal_half(_SampleTexture2D_a44f919f050c4fed96551b3631e81f51_R_4, 0, _Comparison_0d24209b0fb341058e16996fba48a926_Out_2);
half _Or_cb0f721ed05048fdb6d013b3fb9660f1_Out_2;
Unity_Or_half(_Comparison_113df203248543e0b192348b1a8ca785_Out_2, _Comparison_0d24209b0fb341058e16996fba48a926_Out_2, _Or_cb0f721ed05048fdb6d013b3fb9660f1_Out_2);
half4 _Property_d616b029977b4e1db01d282968de91f6_Out_0 = _OutlineColor;
half _Split_deed0eb77c5c4217b8fe5c258ed6fd50_R_1 = _Property_d616b029977b4e1db01d282968de91f6_Out_0[0];
half _Split_deed0eb77c5c4217b8fe5c258ed6fd50_G_2 = _Property_d616b029977b4e1db01d282968de91f6_Out_0[1];
half _Split_deed0eb77c5c4217b8fe5c258ed6fd50_B_3 = _Property_d616b029977b4e1db01d282968de91f6_Out_0[2];
half _Split_deed0eb77c5c4217b8fe5c258ed6fd50_A_4 = _Property_d616b029977b4e1db01d282968de91f6_Out_0[3];
UnityTexture2D _Property_8393ce88430c4c6d9da5262cf925a5b2_Out_0 = _Main_Texture;
half _Property_13fd7e2022034b6581763eec9309f978_Out_0 = _OutlineThickness;
half _Multiply_d43ab8d3309a4b59a266c7010724136d_Out_2;
Unity_Multiply_half_half(_Property_13fd7e2022034b6581763eec9309f978_Out_0, 1, _Multiply_d43ab8d3309a4b59a266c7010724136d_Out_2);
half _Property_f4839464878d492abaf90c3c7031397e_Out_0 = Object_Scale;
Bindings_OutlineSample_5935d51de82a0eb439fe5be3af628c2c_half _OutlineSample_9c588921577241adb732f880358d4022;
_OutlineSample_9c588921577241adb732f880358d4022.uv0 = IN.uv0;
_OutlineSample_9c588921577241adb732f880358d4022.uv1 = IN.uv1;
_OutlineSample_9c588921577241adb732f880358d4022.uv2 = IN.uv2;
_OutlineSample_9c588921577241adb732f880358d4022.uv3 = IN.uv3;
half _OutlineSample_9c588921577241adb732f880358d4022_A_2;
SG_OutlineSample_5935d51de82a0eb439fe5be3af628c2c_half(_Property_8393ce88430c4c6d9da5262cf925a5b2_Out_0, _Multiply_d43ab8d3309a4b59a266c7010724136d_Out_2, half2 (1, 0), _Property_f4839464878d492abaf90c3c7031397e_Out_0, _OutlineSample_9c588921577241adb732f880358d4022, _OutlineSample_9c588921577241adb732f880358d4022_A_2);
UnityTexture2D _Property_6a32b7a37b914c6fb5453d6bd89b088d_Out_0 = _Main_Texture;
half _Property_5bc0de20fe6c4c3583e41d6d15baf983_Out_0 = Object_Scale;
Bindings_OutlineSample_5935d51de82a0eb439fe5be3af628c2c_half _OutlineSample_76af5941489b4ce89b6a8cd30bf3a22e;
_OutlineSample_76af5941489b4ce89b6a8cd30bf3a22e.uv0 = IN.uv0;
_OutlineSample_76af5941489b4ce89b6a8cd30bf3a22e.uv1 = IN.uv1;
_OutlineSample_76af5941489b4ce89b6a8cd30bf3a22e.uv2 = IN.uv2;
_OutlineSample_76af5941489b4ce89b6a8cd30bf3a22e.uv3 = IN.uv3;
half _OutlineSample_76af5941489b4ce89b6a8cd30bf3a22e_A_2;
SG_OutlineSample_5935d51de82a0eb439fe5be3af628c2c_half(_Property_6a32b7a37b914c6fb5453d6bd89b088d_Out_0, _Multiply_d43ab8d3309a4b59a266c7010724136d_Out_2, half2 (-1, 0), _Property_5bc0de20fe6c4c3583e41d6d15baf983_Out_0, _OutlineSample_76af5941489b4ce89b6a8cd30bf3a22e, _OutlineSample_76af5941489b4ce89b6a8cd30bf3a22e_A_2);
half _Add_8691b8f8d7824e3f869c5b95271ca865_Out_2;
Unity_Add_half(_OutlineSample_9c588921577241adb732f880358d4022_A_2, _OutlineSample_76af5941489b4ce89b6a8cd30bf3a22e_A_2, _Add_8691b8f8d7824e3f869c5b95271ca865_Out_2);
UnityTexture2D _Property_2765b01eca6849af9d2e933c1e4f8151_Out_0 = _Main_Texture;
half _Property_2af9f9fed8644e77b7bd094e1375d0d3_Out_0 = Object_Scale;
Bindings_OutlineSample_5935d51de82a0eb439fe5be3af628c2c_half _OutlineSample_0a0ba9aa9e334bfc93af2c944e6cbaef;
_OutlineSample_0a0ba9aa9e334bfc93af2c944e6cbaef.uv0 = IN.uv0;
_OutlineSample_0a0ba9aa9e334bfc93af2c944e6cbaef.uv1 = IN.uv1;
_OutlineSample_0a0ba9aa9e334bfc93af2c944e6cbaef.uv2 = IN.uv2;
_OutlineSample_0a0ba9aa9e334bfc93af2c944e6cbaef.uv3 = IN.uv3;
half _OutlineSample_0a0ba9aa9e334bfc93af2c944e6cbaef_A_2;
SG_OutlineSample_5935d51de82a0eb439fe5be3af628c2c_half(_Property_2765b01eca6849af9d2e933c1e4f8151_Out_0, _Multiply_d43ab8d3309a4b59a266c7010724136d_Out_2, half2 (0, 1), _Property_2af9f9fed8644e77b7bd094e1375d0d3_Out_0, _OutlineSample_0a0ba9aa9e334bfc93af2c944e6cbaef, _OutlineSample_0a0ba9aa9e334bfc93af2c944e6cbaef_A_2);
UnityTexture2D _Property_9e70ee21208a4d28bbb9e0ad8f4360e2_Out_0 = _Main_Texture;
half _Property_f2691b6a70914ee690d60a36a8ada4a9_Out_0 = Object_Scale;
Bindings_OutlineSample_5935d51de82a0eb439fe5be3af628c2c_half _OutlineSample_d3bf438e8e834837a54c90a4a730d33d;
_OutlineSample_d3bf438e8e834837a54c90a4a730d33d.uv0 = IN.uv0;
_OutlineSample_d3bf438e8e834837a54c90a4a730d33d.uv1 = IN.uv1;
_OutlineSample_d3bf438e8e834837a54c90a4a730d33d.uv2 = IN.uv2;
_OutlineSample_d3bf438e8e834837a54c90a4a730d33d.uv3 = IN.uv3;
half _OutlineSample_d3bf438e8e834837a54c90a4a730d33d_A_2;
SG_OutlineSample_5935d51de82a0eb439fe5be3af628c2c_half(_Property_9e70ee21208a4d28bbb9e0ad8f4360e2_Out_0, _Multiply_d43ab8d3309a4b59a266c7010724136d_Out_2, half2 (0, -1), _Property_f2691b6a70914ee690d60a36a8ada4a9_Out_0, _OutlineSample_d3bf438e8e834837a54c90a4a730d33d, _OutlineSample_d3bf438e8e834837a54c90a4a730d33d_A_2);
half _Add_7c337a8390dd4a57aa8416a9149a06c6_Out_2;
Unity_Add_half(_OutlineSample_0a0ba9aa9e334bfc93af2c944e6cbaef_A_2, _OutlineSample_d3bf438e8e834837a54c90a4a730d33d_A_2, _Add_7c337a8390dd4a57aa8416a9149a06c6_Out_2);
half _Add_0ea77ddb233548f58c8ebe27e4d6cd7a_Out_2;
Unity_Add_half(_Add_8691b8f8d7824e3f869c5b95271ca865_Out_2, _Add_7c337a8390dd4a57aa8416a9149a06c6_Out_2, _Add_0ea77ddb233548f58c8ebe27e4d6cd7a_Out_2);
half _Property_723278bd5fc94777bfbb58c043b8f91e_Out_0 = _Sample_Corners;
UnityTexture2D _Property_55f29b9151634ee4acc5f2bc87271dac_Out_0 = _Main_Texture;
half _Property_a366500d0cfa484cb5500429d406d4f6_Out_0 = Object_Scale;
Bindings_OutlineSample_5935d51de82a0eb439fe5be3af628c2c_half _OutlineSample_a8ceaa015742447489dbdeae207a9cbb;
_OutlineSample_a8ceaa015742447489dbdeae207a9cbb.uv0 = IN.uv0;
_OutlineSample_a8ceaa015742447489dbdeae207a9cbb.uv1 = IN.uv1;
_OutlineSample_a8ceaa015742447489dbdeae207a9cbb.uv2 = IN.uv2;
_OutlineSample_a8ceaa015742447489dbdeae207a9cbb.uv3 = IN.uv3;
half _OutlineSample_a8ceaa015742447489dbdeae207a9cbb_A_2;
SG_OutlineSample_5935d51de82a0eb439fe5be3af628c2c_half(_Property_55f29b9151634ee4acc5f2bc87271dac_Out_0, _Multiply_d43ab8d3309a4b59a266c7010724136d_Out_2, half2 (1, 1), _Property_a366500d0cfa484cb5500429d406d4f6_Out_0, _OutlineSample_a8ceaa015742447489dbdeae207a9cbb, _OutlineSample_a8ceaa015742447489dbdeae207a9cbb_A_2);
UnityTexture2D _Property_cce5aee286e440848c1d0703b55757b6_Out_0 = _Main_Texture;
half _Property_e02b523d1ec642fbbd514361296777c0_Out_0 = Object_Scale;
Bindings_OutlineSample_5935d51de82a0eb439fe5be3af628c2c_half _OutlineSample_8f60110e6d7641ac8a8974e4507f42d6;
_OutlineSample_8f60110e6d7641ac8a8974e4507f42d6.uv0 = IN.uv0;
_OutlineSample_8f60110e6d7641ac8a8974e4507f42d6.uv1 = IN.uv1;
_OutlineSample_8f60110e6d7641ac8a8974e4507f42d6.uv2 = IN.uv2;
_OutlineSample_8f60110e6d7641ac8a8974e4507f42d6.uv3 = IN.uv3;
half _OutlineSample_8f60110e6d7641ac8a8974e4507f42d6_A_2;
SG_OutlineSample_5935d51de82a0eb439fe5be3af628c2c_half(_Property_cce5aee286e440848c1d0703b55757b6_Out_0, _Multiply_d43ab8d3309a4b59a266c7010724136d_Out_2, half2 (-1, -1), _Property_e02b523d1ec642fbbd514361296777c0_Out_0, _OutlineSample_8f60110e6d7641ac8a8974e4507f42d6, _OutlineSample_8f60110e6d7641ac8a8974e4507f42d6_A_2);
half _Add_9646efb05e5c41f09db53b61810918ee_Out_2;
Unity_Add_half(_OutlineSample_a8ceaa015742447489dbdeae207a9cbb_A_2, _OutlineSample_8f60110e6d7641ac8a8974e4507f42d6_A_2, _Add_9646efb05e5c41f09db53b61810918ee_Out_2);
UnityTexture2D _Property_8a83e549c02342f981d0805651966866_Out_0 = _Main_Texture;
half _Property_6adeb1ac1a8b4aaf9f084e8cdba7af28_Out_0 = Object_Scale;
Bindings_OutlineSample_5935d51de82a0eb439fe5be3af628c2c_half _OutlineSample_e9a7eeb825284ae6b79837723e7f3184;
_OutlineSample_e9a7eeb825284ae6b79837723e7f3184.uv0 = IN.uv0;
_OutlineSample_e9a7eeb825284ae6b79837723e7f3184.uv1 = IN.uv1;
_OutlineSample_e9a7eeb825284ae6b79837723e7f3184.uv2 = IN.uv2;
_OutlineSample_e9a7eeb825284ae6b79837723e7f3184.uv3 = IN.uv3;
half _OutlineSample_e9a7eeb825284ae6b79837723e7f3184_A_2;
SG_OutlineSample_5935d51de82a0eb439fe5be3af628c2c_half(_Property_8a83e549c02342f981d0805651966866_Out_0, _Multiply_d43ab8d3309a4b59a266c7010724136d_Out_2, half2 (1, -1), _Property_6adeb1ac1a8b4aaf9f084e8cdba7af28_Out_0, _OutlineSample_e9a7eeb825284ae6b79837723e7f3184, _OutlineSample_e9a7eeb825284ae6b79837723e7f3184_A_2);
UnityTexture2D _Property_3b3f7ef7bd6141eca70486df83864227_Out_0 = _Main_Texture;
half _Property_b2d34ad5666a44b79ee7b7c17ff2f0e2_Out_0 = Object_Scale;
Bindings_OutlineSample_5935d51de82a0eb439fe5be3af628c2c_half _OutlineSample_193fb0916dc740eaafac5913899a3194;
_OutlineSample_193fb0916dc740eaafac5913899a3194.uv0 = IN.uv0;
_OutlineSample_193fb0916dc740eaafac5913899a3194.uv1 = IN.uv1;
_OutlineSample_193fb0916dc740eaafac5913899a3194.uv2 = IN.uv2;
_OutlineSample_193fb0916dc740eaafac5913899a3194.uv3 = IN.uv3;
half _OutlineSample_193fb0916dc740eaafac5913899a3194_A_2;
SG_OutlineSample_5935d51de82a0eb439fe5be3af628c2c_half(_Property_3b3f7ef7bd6141eca70486df83864227_Out_0, _Multiply_d43ab8d3309a4b59a266c7010724136d_Out_2, half2 (-1, 1), _Property_b2d34ad5666a44b79ee7b7c17ff2f0e2_Out_0, _OutlineSample_193fb0916dc740eaafac5913899a3194, _OutlineSample_193fb0916dc740eaafac5913899a3194_A_2);
half _Add_f088d895e95842b198543866f527ce74_Out_2;
Unity_Add_half(_OutlineSample_e9a7eeb825284ae6b79837723e7f3184_A_2, _OutlineSample_193fb0916dc740eaafac5913899a3194_A_2, _Add_f088d895e95842b198543866f527ce74_Out_2);
half _Add_4ae0565d1a074a7984696a4b321d7b73_Out_2;
Unity_Add_half(_Add_9646efb05e5c41f09db53b61810918ee_Out_2, _Add_f088d895e95842b198543866f527ce74_Out_2, _Add_4ae0565d1a074a7984696a4b321d7b73_Out_2);
half _Branch_ea26f381df754b65860e90abe6376d34_Out_3;
Unity_Branch_half(_Property_723278bd5fc94777bfbb58c043b8f91e_Out_0, _Add_4ae0565d1a074a7984696a4b321d7b73_Out_2, 0, _Branch_ea26f381df754b65860e90abe6376d34_Out_3);
half _Add_c40eb27d5fdc404981a0ef66ab7df7e4_Out_2;
Unity_Add_half(_Add_0ea77ddb233548f58c8ebe27e4d6cd7a_Out_2, _Branch_ea26f381df754b65860e90abe6376d34_Out_3, _Add_c40eb27d5fdc404981a0ef66ab7df7e4_Out_2);
half _Saturate_0deffd336d1d4937af7e630518f968d5_Out_1;
Unity_Saturate_half(_Add_c40eb27d5fdc404981a0ef66ab7df7e4_Out_2, _Saturate_0deffd336d1d4937af7e630518f968d5_Out_1);
half _Multiply_9e5b4bce50194c33b5279ac0d4ea77bf_Out_2;
Unity_Multiply_half_half(_Split_deed0eb77c5c4217b8fe5c258ed6fd50_A_4, _Saturate_0deffd336d1d4937af7e630518f968d5_Out_1, _Multiply_9e5b4bce50194c33b5279ac0d4ea77bf_Out_2);
half4 _Vector4_e1f5c9722253465a8f11fea95672911a_Out_0 = half4(_Split_deed0eb77c5c4217b8fe5c258ed6fd50_R_1, _Split_deed0eb77c5c4217b8fe5c258ed6fd50_G_2, _Split_deed0eb77c5c4217b8fe5c258ed6fd50_B_3, _Multiply_9e5b4bce50194c33b5279ac0d4ea77bf_Out_2);
half4 _Branch_a2e396118cd2475384e5a6b92b3cd166_Out_3;
Unity_Branch_half4(_Or_cb0f721ed05048fdb6d013b3fb9660f1_Out_2, _SampleTexture2D_29d7a1f29de44f6cbd20e287d4579b4c_RGBA_0, _Vector4_e1f5c9722253465a8f11fea95672911a_Out_0, _Branch_a2e396118cd2475384e5a6b92b3cd166_Out_3);
half4 _Branch_b7083fb1fc724f4d87c58f86f23ec327_Out_3;
Unity_Branch_half4(_Property_c255289e5e904c44b6f9179ec57e8af2_Out_0, _Branch_a2e396118cd2475384e5a6b92b3cd166_Out_3, _SampleTexture2D_29d7a1f29de44f6cbd20e287d4579b4c_RGBA_0, _Branch_b7083fb1fc724f4d87c58f86f23ec327_Out_3);
Out_1 = _Branch_b7083fb1fc724f4d87c58f86f23ec327_Out_3;
}

void Unity_Subtract_half4(half4 A, half4 B, out half4 Out)
{
    Out = A - B;
}

void Unity_Clamp_half4(half4 In, half4 Min, half4 Max, out half4 Out)
{
    Out = clamp(In, Min, Max);
}

void Unity_Add_half4(half4 A, half4 B, out half4 Out)
{
    Out = A + B;
}

void Unity_Saturate_half4(half4 In, out half4 Out)
{
    Out = saturate(In);
}

struct Bindings_OutlineBackdrop_744c969b165a0da49ae2d86b538d1386_half
{
half4 uv0;
half4 uv1;
half4 uv2;
half4 uv3;
};

void SG_OutlineBackdrop_744c969b165a0da49ae2d86b538d1386_half(UnityTexture2D Main, UnityTexture2D ShadowMask, half OutlineThickness, half4 OutlineColor, half Object_Scale, half _Sample_Corners, half _Enable_Outline, Bindings_OutlineBackdrop_744c969b165a0da49ae2d86b538d1386_half IN, out half4 Out_1)
{
UnityTexture2D _Property_90a419dc4f614512ab95bea1b5a778a1_Out_0 = Main;
UnityTexture2D _Property_b759b0dd428b4448824d5ed140a96188_Out_0 = ShadowMask;
half _Property_866d9bd9ba3b48c0bc4f8257ff41473d_Out_0 = OutlineThickness;
half4 _Property_57bdc3b2d2a843b0a313592ba18dc1f8_Out_0 = OutlineColor;
half _Property_ebda404cd5e543c79f33e48f1d033064_Out_0 = Object_Scale;
half _Property_3c8f21ce8f794a1d97b919433e0d3111_Out_0 = _Sample_Corners;
half _Property_83b487b19a2849e195531d7c941a6555_Out_0 = _Enable_Outline;
Bindings_OutlineSubGraph_cca9a3cdf4ebbbf47b3960f57cef445d_half _OutlineSubGraph_5b841b12f1504112ad34b55f03658271;
_OutlineSubGraph_5b841b12f1504112ad34b55f03658271.uv0 = IN.uv0;
_OutlineSubGraph_5b841b12f1504112ad34b55f03658271.uv1 = IN.uv1;
_OutlineSubGraph_5b841b12f1504112ad34b55f03658271.uv2 = IN.uv2;
_OutlineSubGraph_5b841b12f1504112ad34b55f03658271.uv3 = IN.uv3;
half4 _OutlineSubGraph_5b841b12f1504112ad34b55f03658271_Out_1;
SG_OutlineSubGraph_cca9a3cdf4ebbbf47b3960f57cef445d_half(_Property_90a419dc4f614512ab95bea1b5a778a1_Out_0, _Property_b759b0dd428b4448824d5ed140a96188_Out_0, _Property_866d9bd9ba3b48c0bc4f8257ff41473d_Out_0, _Property_57bdc3b2d2a843b0a313592ba18dc1f8_Out_0, _Property_ebda404cd5e543c79f33e48f1d033064_Out_0, _Property_3c8f21ce8f794a1d97b919433e0d3111_Out_0, _Property_83b487b19a2849e195531d7c941a6555_Out_0, _OutlineSubGraph_5b841b12f1504112ad34b55f03658271, _OutlineSubGraph_5b841b12f1504112ad34b55f03658271_Out_1);
UnityTexture2D _Property_ea3503ea967c4a42a1bf1078dd71da43_Out_0 = Main;
UnityTexture2D _Property_f2a8238803b44425876470beba40dd0d_Out_0 = ShadowMask;
half _Property_d2d0fb311c044fddaa652344afa16d54_Out_0 = OutlineThickness;
half _Multiply_204c0193bc4e49ecaa0f7a29ce53ec5f_Out_2;
Unity_Multiply_half_half(_Property_d2d0fb311c044fddaa652344afa16d54_Out_0, 2, _Multiply_204c0193bc4e49ecaa0f7a29ce53ec5f_Out_2);
half4 _Property_931f0829dc3a49b6bdb44c7227a71563_Out_0 = OutlineColor;
half4 _Multiply_9239990124df4c0b93122f9b7bd8e228_Out_2;
Unity_Multiply_half4_half4(_Property_931f0829dc3a49b6bdb44c7227a71563_Out_0, half4(1, 1, 1, 0.25), _Multiply_9239990124df4c0b93122f9b7bd8e228_Out_2);
half _Property_b8693891041d4b5c842acc907790d989_Out_0 = Object_Scale;
half _Property_4d9d03a33e7d44b4a14fe8b22fce75b3_Out_0 = _Sample_Corners;
half _Property_a3dd0e92844442c3a42b3b183b8748ac_Out_0 = _Enable_Outline;
Bindings_OutlineSubGraph_cca9a3cdf4ebbbf47b3960f57cef445d_half _OutlineSubGraph_fa6f19d5925e4f2589558d0537a65dfd;
_OutlineSubGraph_fa6f19d5925e4f2589558d0537a65dfd.uv0 = IN.uv0;
_OutlineSubGraph_fa6f19d5925e4f2589558d0537a65dfd.uv1 = IN.uv1;
_OutlineSubGraph_fa6f19d5925e4f2589558d0537a65dfd.uv2 = IN.uv2;
_OutlineSubGraph_fa6f19d5925e4f2589558d0537a65dfd.uv3 = IN.uv3;
half4 _OutlineSubGraph_fa6f19d5925e4f2589558d0537a65dfd_Out_1;
SG_OutlineSubGraph_cca9a3cdf4ebbbf47b3960f57cef445d_half(_Property_ea3503ea967c4a42a1bf1078dd71da43_Out_0, _Property_f2a8238803b44425876470beba40dd0d_Out_0, _Multiply_204c0193bc4e49ecaa0f7a29ce53ec5f_Out_2, _Multiply_9239990124df4c0b93122f9b7bd8e228_Out_2, _Property_b8693891041d4b5c842acc907790d989_Out_0, _Property_4d9d03a33e7d44b4a14fe8b22fce75b3_Out_0, _Property_a3dd0e92844442c3a42b3b183b8748ac_Out_0, _OutlineSubGraph_fa6f19d5925e4f2589558d0537a65dfd, _OutlineSubGraph_fa6f19d5925e4f2589558d0537a65dfd_Out_1);
half4 _Subtract_76793de1ebc440058c6e3a560b990497_Out_2;
Unity_Subtract_half4(_OutlineSubGraph_fa6f19d5925e4f2589558d0537a65dfd_Out_1, _OutlineSubGraph_5b841b12f1504112ad34b55f03658271_Out_1, _Subtract_76793de1ebc440058c6e3a560b990497_Out_2);
half4 _Clamp_9e0614f26838413697db05ad993d0e13_Out_3;
Unity_Clamp_half4(_Subtract_76793de1ebc440058c6e3a560b990497_Out_2, half4(0, 0, 0, 0), half4(1, 1, 1, 1), _Clamp_9e0614f26838413697db05ad993d0e13_Out_3);
half4 _Add_c0dc666fdaed45938e990d581939b301_Out_2;
Unity_Add_half4(_OutlineSubGraph_5b841b12f1504112ad34b55f03658271_Out_1, _Clamp_9e0614f26838413697db05ad993d0e13_Out_3, _Add_c0dc666fdaed45938e990d581939b301_Out_2);
half4 _Saturate_02e190d6960148d7b7f9e11c9393bc52_Out_1;
Unity_Saturate_half4(_Add_c0dc666fdaed45938e990d581939b301_Out_2, _Saturate_02e190d6960148d7b7f9e11c9393bc52_Out_1);
Out_1 = _Saturate_02e190d6960148d7b7f9e11c9393bc52_Out_1;
}

/* WARNING: $splice Could not find named fragment 'CustomInterpolatorPreVertex' */

// Graph Vertex
struct VertexDescription
{
    half3 Position;
    half3 Normal;
    half3 Tangent;
};

VertexDescription VertexDescriptionFunction(VertexDescriptionInputs IN)
{
    VertexDescription description = (VertexDescription)0;
    description.Position = IN.ObjectSpacePosition;
    description.Normal = IN.ObjectSpaceNormal;
    description.Tangent = IN.ObjectSpaceTangent;
    return description;
}

    #ifdef FEATURES_GRAPH_VERTEX
Varyings CustomInterpolatorPassThroughFunc(inout Varyings output, VertexDescription input)
{
return output;
}
#define CUSTOMINTERPOLATOR_VARYPASSTHROUGH_FUNC
#endif

// Graph Pixel
struct SurfaceDescription
{
    half3 BaseColor;
    half Alpha;
};

SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
{
    SurfaceDescription surface = (SurfaceDescription)0;
    UnityTexture2D _Property_31e6e86d16a54fdc93167323dd1bb61c_Out_0 = UnityBuildTexture2DStructNoScale(_Mask);
    half4 _SampleTexture2D_6ff5c694df00418195f0a3073c6659b3_RGBA_0 = SAMPLE_TEXTURE2D(_Property_31e6e86d16a54fdc93167323dd1bb61c_Out_0.tex, _Property_31e6e86d16a54fdc93167323dd1bb61c_Out_0.samplerstate, _Property_31e6e86d16a54fdc93167323dd1bb61c_Out_0.GetTransformedUV(IN.uv0.xy));
    half _SampleTexture2D_6ff5c694df00418195f0a3073c6659b3_R_4 = _SampleTexture2D_6ff5c694df00418195f0a3073c6659b3_RGBA_0.r;
    half _SampleTexture2D_6ff5c694df00418195f0a3073c6659b3_G_5 = _SampleTexture2D_6ff5c694df00418195f0a3073c6659b3_RGBA_0.g;
    half _SampleTexture2D_6ff5c694df00418195f0a3073c6659b3_B_6 = _SampleTexture2D_6ff5c694df00418195f0a3073c6659b3_RGBA_0.b;
    half _SampleTexture2D_6ff5c694df00418195f0a3073c6659b3_A_7 = _SampleTexture2D_6ff5c694df00418195f0a3073c6659b3_RGBA_0.a;
    half _ColorMask_df87682a03d54ad693d60342f0c457f5_Out_3;
    Unity_ColorMask_half((_SampleTexture2D_6ff5c694df00418195f0a3073c6659b3_RGBA_0.xyz), IsGammaSpace() ? half3(1, 0, 0) : SRGBToLinear(half3(1, 0, 0)), 0, _ColorMask_df87682a03d54ad693d60342f0c457f5_Out_3, 0);
    half _Property_ec17d3661ef1cd80850f94f5fc3cf8ab_Out_0 = _FlashRate;
    half4 _Property_d22763b69abd28838f68a1f3ddf746d6_Out_0 = _FlashColor;
    half4 _GameEntityFlashCustomFunction_d3e355381dc0ce86b779cb1ae730470e_Out_3;
    GameEntity_Flash_half(IN.TimeParameters.x, _Property_ec17d3661ef1cd80850f94f5fc3cf8ab_Out_0, (_Property_d22763b69abd28838f68a1f3ddf746d6_Out_0.xyz), _GameEntityFlashCustomFunction_d3e355381dc0ce86b779cb1ae730470e_Out_3);
    half4 _Multiply_bee050f977214478a63d598505cb0478_Out_2;
    Unity_Multiply_half4_half4((_ColorMask_df87682a03d54ad693d60342f0c457f5_Out_3.xxxx), _GameEntityFlashCustomFunction_d3e355381dc0ce86b779cb1ae730470e_Out_3, _Multiply_bee050f977214478a63d598505cb0478_Out_2);
    UnityTexture2D _Property_9148162509cc4b3cafc17cf5d22974ef_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
    half _Property_ea02751492574ed392eff2590e4bc651_Out_0 = OutlineThickness;
    half4 _Property_7064f4e4d6e04f8881e235bd36abb3c6_Out_0 = OutlineColor;
    half _Property_9a458f8f5d4e4fa781a096d41cdba96a_Out_0 = ObjectScale;
    Bindings_OutlineBackdrop_744c969b165a0da49ae2d86b538d1386_half _OutlineBackdrop_bb9928ba826749338c5c4e9b5aab4482;
    _OutlineBackdrop_bb9928ba826749338c5c4e9b5aab4482.uv0 = IN.uv0;
    _OutlineBackdrop_bb9928ba826749338c5c4e9b5aab4482.uv1 = IN.uv1;
    _OutlineBackdrop_bb9928ba826749338c5c4e9b5aab4482.uv2 = IN.uv2;
    _OutlineBackdrop_bb9928ba826749338c5c4e9b5aab4482.uv3 = IN.uv3;
    half4 _OutlineBackdrop_bb9928ba826749338c5c4e9b5aab4482_Out_1;
    SG_OutlineBackdrop_744c969b165a0da49ae2d86b538d1386_half(_Property_9148162509cc4b3cafc17cf5d22974ef_Out_0, UnityBuildTexture2DStructNoScale(_OutlineBackdrop_bb9928ba826749338c5c4e9b5aab4482_ShadowMask_1013696677), _Property_ea02751492574ed392eff2590e4bc651_Out_0, _Property_7064f4e4d6e04f8881e235bd36abb3c6_Out_0, _Property_9a458f8f5d4e4fa781a096d41cdba96a_Out_0, 1, 1, _OutlineBackdrop_bb9928ba826749338c5c4e9b5aab4482, _OutlineBackdrop_bb9928ba826749338c5c4e9b5aab4482_Out_1);
    half4 _Add_df9549590666928c9d76e49de42c3aba_Out_2;
    Unity_Add_half4(_Multiply_bee050f977214478a63d598505cb0478_Out_2, _OutlineBackdrop_bb9928ba826749338c5c4e9b5aab4482_Out_1, _Add_df9549590666928c9d76e49de42c3aba_Out_2);
    half _Split_85e876829c3848e3882b84add8bb43c1_R_1 = _Add_df9549590666928c9d76e49de42c3aba_Out_2[0];
    half _Split_85e876829c3848e3882b84add8bb43c1_G_2 = _Add_df9549590666928c9d76e49de42c3aba_Out_2[1];
    half _Split_85e876829c3848e3882b84add8bb43c1_B_3 = _Add_df9549590666928c9d76e49de42c3aba_Out_2[2];
    half _Split_85e876829c3848e3882b84add8bb43c1_A_4 = _Add_df9549590666928c9d76e49de42c3aba_Out_2[3];
    half _Property_f12dfe796cdd433b8893f05a405c359a_Out_0 = _Alpha;
    half _Multiply_9c23e6f8f2194ffaa4e75b49ea9e0fbb_Out_2;
    Unity_Multiply_half_half(_Split_85e876829c3848e3882b84add8bb43c1_A_4, _Property_f12dfe796cdd433b8893f05a405c359a_Out_0, _Multiply_9c23e6f8f2194ffaa4e75b49ea9e0fbb_Out_2);
    surface.BaseColor = (_Add_df9549590666928c9d76e49de42c3aba_Out_2.xyz);
    surface.Alpha = _Multiply_9c23e6f8f2194ffaa4e75b49ea9e0fbb_Out_2;
    return surface;
}

// --------------------------------------------------
// Build Graph Inputs

VertexDescriptionInputs BuildVertexDescriptionInputs(Attributes input)
{
    VertexDescriptionInputs output;
    ZERO_INITIALIZE(VertexDescriptionInputs, output);

    output.ObjectSpaceNormal = input.normalOS;
    output.ObjectSpaceTangent = input.tangentOS.xyz;
    output.ObjectSpacePosition = input.positionOS;

    return output;
}
    SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
{
    SurfaceDescriptionInputs output;
    ZERO_INITIALIZE(SurfaceDescriptionInputs, output);







    output.uv0 = input.texCoord0;
    output.uv1 = input.texCoord1;
    output.uv2 = input.texCoord2;
    output.uv3 = input.texCoord3;
    output.TimeParameters = _TimeParameters.xyz; // This is mainly for LW as HD overwrite this value
#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN                output.FaceSign =                                   IS_FRONT_VFACE(input.cullFace, true, false);
#else
#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
#endif
#undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN

    return output;
}

    // --------------------------------------------------
    // Main

    #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
#include "Packages/com.unity.render-pipelines.universal/Editor/2D/ShaderGraph/Includes/SpriteUnlitPass.hlsl"

    ENDHLSL
}
Pass
{
    Name "Sprite Unlit"
    Tags
    {
        "LightMode" = "UniversalForward"
    }

        // Render State
        Cull Off
    Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
    ZTest LEqual
    ZWrite Off

        // Debug
        // <None>

        // --------------------------------------------------
        // Pass

        HLSLPROGRAM

        // Pragmas
        #pragma target 2.0
    #pragma exclude_renderers d3d11_9x
    #pragma vertex vert
    #pragma fragment frag

        // DotsInstancingOptions: <None>
        // HybridV1InjectedBuiltinProperties: <None>

        // Keywords
        #pragma multi_compile_fragment _ DEBUG_DISPLAY
        // GraphKeywords: <None>

        // Defines
        #define _SURFACE_TYPE_TRANSPARENT 1
        #define ATTRIBUTES_NEED_NORMAL
        #define ATTRIBUTES_NEED_TANGENT
        #define ATTRIBUTES_NEED_TEXCOORD0
        #define ATTRIBUTES_NEED_TEXCOORD1
        #define ATTRIBUTES_NEED_TEXCOORD2
        #define ATTRIBUTES_NEED_TEXCOORD3
        #define ATTRIBUTES_NEED_COLOR
        #define VARYINGS_NEED_POSITION_WS
        #define VARYINGS_NEED_TEXCOORD0
        #define VARYINGS_NEED_TEXCOORD1
        #define VARYINGS_NEED_TEXCOORD2
        #define VARYINGS_NEED_TEXCOORD3
        #define VARYINGS_NEED_COLOR
        #define FEATURES_GRAPH_VERTEX
        /* WARNING: $splice Could not find named fragment 'PassInstancing' */
        #define SHADERPASS SHADERPASS_SPRITEFORWARD
        /* WARNING: $splice Could not find named fragment 'DotsInstancingVars' */

        // Includes
        /* WARNING: $splice Could not find named fragment 'CustomInterpolatorPreInclude' */

        #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
    #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Texture.hlsl"
    #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
    #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
    #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/TextureStack.hlsl"
    #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
    #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShaderPass.hlsl"

        // --------------------------------------------------
        // Structs and Packing

        /* WARNING: $splice Could not find named fragment 'CustomInterpolatorPrePacking' */

        struct Attributes
    {
         float3 positionOS : POSITION;
         float3 normalOS : NORMAL;
         float4 tangentOS : TANGENT;
         float4 uv0 : TEXCOORD0;
         float4 uv1 : TEXCOORD1;
         float4 uv2 : TEXCOORD2;
         float4 uv3 : TEXCOORD3;
         float4 color : COLOR;
        #if UNITY_ANY_INSTANCING_ENABLED
         uint instanceID : INSTANCEID_SEMANTIC;
        #endif
    };
    struct Varyings
    {
         float4 positionCS : SV_POSITION;
         float3 positionWS;
         float4 texCoord0;
         float4 texCoord1;
         float4 texCoord2;
         float4 texCoord3;
         float4 color;
        #if UNITY_ANY_INSTANCING_ENABLED
         uint instanceID : CUSTOM_INSTANCE_ID;
        #endif
        #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
         uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
        #endif
        #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
         uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
        #endif
        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
         FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
        #endif
    };
    struct SurfaceDescriptionInputs
    {
         float4 uv0;
         float4 uv1;
         float4 uv2;
         float4 uv3;
         float3 TimeParameters;
    };
    struct VertexDescriptionInputs
    {
         float3 ObjectSpaceNormal;
         float3 ObjectSpaceTangent;
         float3 ObjectSpacePosition;
    };
    struct PackedVaryings
    {
         float4 positionCS : SV_POSITION;
         float3 interp0 : INTERP0;
         float4 interp1 : INTERP1;
         float4 interp2 : INTERP2;
         float4 interp3 : INTERP3;
         float4 interp4 : INTERP4;
         float4 interp5 : INTERP5;
        #if UNITY_ANY_INSTANCING_ENABLED
         uint instanceID : CUSTOM_INSTANCE_ID;
        #endif
        #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
         uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
        #endif
        #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
         uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
        #endif
        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
         FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
        #endif
    };

        PackedVaryings PackVaryings(Varyings input)
    {
        PackedVaryings output;
        ZERO_INITIALIZE(PackedVaryings, output);
        output.positionCS = input.positionCS;
        output.interp0.xyz = input.positionWS;
        output.interp1.xyzw = input.texCoord0;
        output.interp2.xyzw = input.texCoord1;
        output.interp3.xyzw = input.texCoord2;
        output.interp4.xyzw = input.texCoord3;
        output.interp5.xyzw = input.color;
        #if UNITY_ANY_INSTANCING_ENABLED
        output.instanceID = input.instanceID;
        #endif
        #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
        output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
        #endif
        #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
        output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
        #endif
        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
        output.cullFace = input.cullFace;
        #endif
        return output;
    }

    Varyings UnpackVaryings(PackedVaryings input)
    {
        Varyings output;
        output.positionCS = input.positionCS;
        output.positionWS = input.interp0.xyz;
        output.texCoord0 = input.interp1.xyzw;
        output.texCoord1 = input.interp2.xyzw;
        output.texCoord2 = input.interp3.xyzw;
        output.texCoord3 = input.interp4.xyzw;
        output.color = input.interp5.xyzw;
        #if UNITY_ANY_INSTANCING_ENABLED
        output.instanceID = input.instanceID;
        #endif
        #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
        output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
        #endif
        #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
        output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
        #endif
        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
        output.cullFace = input.cullFace;
        #endif
        return output;
    }


    // --------------------------------------------------
    // Graph

    // Graph Properties
    CBUFFER_START(UnityPerMaterial)
float4 _OutlineBackdrop_bb9928ba826749338c5c4e9b5aab4482_ShadowMask_1013696677_TexelSize;
float4 _MainTex_TexelSize;
float4 _Mask_TexelSize;
half _FlashRate;
half4 _FlashColor;
half OutlineThickness;
half4 OutlineColor;
half ObjectScale;
half _Alpha;
CBUFFER_END

// Object and Global properties
SAMPLER(SamplerState_Linear_Repeat);
TEXTURE2D(_OutlineBackdrop_bb9928ba826749338c5c4e9b5aab4482_ShadowMask_1013696677);
SAMPLER(sampler_OutlineBackdrop_bb9928ba826749338c5c4e9b5aab4482_ShadowMask_1013696677);
TEXTURE2D(_MainTex);
SAMPLER(sampler_MainTex);
TEXTURE2D(_Mask);
SAMPLER(sampler_Mask);

// Graph Includes
#include "Assets/ShaderGraph/CustomFunctionShaders.hlsl"

// -- Property used by ScenePickingPass
#ifdef SCENEPICKINGPASS
float4 _SelectionID;
#endif

// -- Properties used by SceneSelectionPass
#ifdef SCENESELECTIONPASS
int _ObjectId;
int _PassValue;
#endif

// Graph Functions

void Unity_ColorMask_half(half3 In, half3 MaskColor, half Range, out half Out, half Fuzziness)
{
    half Distance = distance(MaskColor, In);
    Out = saturate(1 - (Distance - Range) / max(Fuzziness, 1e-5));
}

void Unity_Multiply_half4_half4(half4 A, half4 B, out half4 Out)
{
    Out = A * B;
}

void Unity_Comparison_GreaterOrEqual_half(half A, half B, out half Out)
{
    Out = A >= B ? 1 : 0;
}

void Unity_Comparison_Equal_half(half A, half B, out half Out)
{
    Out = A == B ? 1 : 0;
}

void Unity_Or_half(half A, half B, out half Out)
{
    Out = A || B;
}

void Unity_Multiply_half_half(half A, half B, out half Out)
{
Out = A * B;
}

void Unity_Reciprocal_Fast_half2(half2 In, out half2 Out)
{
    Out = rcp(In);
}

void Unity_Reciprocal_half(half In, out half Out)
{
    Out = 1.0 / In;
}

void Unity_Multiply_half2_half2(half2 A, half2 B, out half2 Out)
{
Out = A * B;
}

void Unity_Add_half2(half2 A, half2 B, out half2 Out)
{
    Out = A + B;
}

struct Bindings_OutlineSample_5935d51de82a0eb439fe5be3af628c2c_half
{
half4 uv0;
half4 uv1;
half4 uv2;
half4 uv3;
};

void SG_OutlineSample_5935d51de82a0eb439fe5be3af628c2c_half(UnityTexture2D Texture2D_BADD5F9F, half Vector1_A2CEEF4B, half2 Vector2_950F9506, half Object_Scale, Bindings_OutlineSample_5935d51de82a0eb439fe5be3af628c2c_half IN, out half A_2)
{
UnityTexture2D _Property_a462cdbbc4e21f8299300428a67fb356_Out_0 = Texture2D_BADD5F9F;
half4 _UV_cf3003d855f53a80b12146a03d000837_Out_0 = IN.uv0;
UnityTexture2D _Property_40637bc593d246a380d158c5e41e8fb4_Out_0 = Texture2D_BADD5F9F;
half _TexelSize_361811e552ca4f1ab2e2b07e4ce98a24_Width_0 = _Property_40637bc593d246a380d158c5e41e8fb4_Out_0.texelSize.z;
half _TexelSize_361811e552ca4f1ab2e2b07e4ce98a24_Height_2 = _Property_40637bc593d246a380d158c5e41e8fb4_Out_0.texelSize.w;
half2 _Vector2_3d8fdf32f46a978c8b6a6275158b40aa_Out_0 = half2(_TexelSize_361811e552ca4f1ab2e2b07e4ce98a24_Width_0, _TexelSize_361811e552ca4f1ab2e2b07e4ce98a24_Height_2);
half2 _Reciprocal_c66fc76f3df6268ea7829e24c38439ac_Out_1;
Unity_Reciprocal_Fast_half2(_Vector2_3d8fdf32f46a978c8b6a6275158b40aa_Out_0, _Reciprocal_c66fc76f3df6268ea7829e24c38439ac_Out_1);
half _Property_e3976a49f31d8a85a0e8a0580baef75a_Out_0 = Vector1_A2CEEF4B;
half _Property_4e136003b70c4ab4add87ce76c8d759d_Out_0 = Object_Scale;
half _Reciprocal_7ddf14c974bc47ba9c8634f3ffd3b82a_Out_1;
Unity_Reciprocal_half(_Property_4e136003b70c4ab4add87ce76c8d759d_Out_0, _Reciprocal_7ddf14c974bc47ba9c8634f3ffd3b82a_Out_1);
half _Multiply_2ee2782e12ca4e818b0b048616704bde_Out_2;
Unity_Multiply_half_half(_Property_e3976a49f31d8a85a0e8a0580baef75a_Out_0, _Reciprocal_7ddf14c974bc47ba9c8634f3ffd3b82a_Out_1, _Multiply_2ee2782e12ca4e818b0b048616704bde_Out_2);
half2 _Multiply_b3e9757076a048849db0fa0d20333b85_Out_2;
Unity_Multiply_half2_half2(_Reciprocal_c66fc76f3df6268ea7829e24c38439ac_Out_1, (_Multiply_2ee2782e12ca4e818b0b048616704bde_Out_2.xx), _Multiply_b3e9757076a048849db0fa0d20333b85_Out_2);
half2 _Property_8d6f718782319580b294ce28423ec69a_Out_0 = Vector2_950F9506;
half2 _Multiply_cfab4d800a57e78781f71bbb890e5a7e_Out_2;
Unity_Multiply_half2_half2(_Multiply_b3e9757076a048849db0fa0d20333b85_Out_2, _Property_8d6f718782319580b294ce28423ec69a_Out_0, _Multiply_cfab4d800a57e78781f71bbb890e5a7e_Out_2);
half2 _Add_e01d035dd81b8d8da5d31a131e67ace9_Out_2;
Unity_Add_half2((_UV_cf3003d855f53a80b12146a03d000837_Out_0.xy), _Multiply_cfab4d800a57e78781f71bbb890e5a7e_Out_2, _Add_e01d035dd81b8d8da5d31a131e67ace9_Out_2);
half4 _SampleTexture2D_1c01dd08f4acb78e818fce32364589b4_RGBA_0 = SAMPLE_TEXTURE2D(_Property_a462cdbbc4e21f8299300428a67fb356_Out_0.tex, _Property_a462cdbbc4e21f8299300428a67fb356_Out_0.samplerstate, _Property_a462cdbbc4e21f8299300428a67fb356_Out_0.GetTransformedUV(_Add_e01d035dd81b8d8da5d31a131e67ace9_Out_2));
half _SampleTexture2D_1c01dd08f4acb78e818fce32364589b4_R_4 = _SampleTexture2D_1c01dd08f4acb78e818fce32364589b4_RGBA_0.r;
half _SampleTexture2D_1c01dd08f4acb78e818fce32364589b4_G_5 = _SampleTexture2D_1c01dd08f4acb78e818fce32364589b4_RGBA_0.g;
half _SampleTexture2D_1c01dd08f4acb78e818fce32364589b4_B_6 = _SampleTexture2D_1c01dd08f4acb78e818fce32364589b4_RGBA_0.b;
half _SampleTexture2D_1c01dd08f4acb78e818fce32364589b4_A_7 = _SampleTexture2D_1c01dd08f4acb78e818fce32364589b4_RGBA_0.a;
A_2 = _SampleTexture2D_1c01dd08f4acb78e818fce32364589b4_A_7;
}

void Unity_Add_half(half A, half B, out half Out)
{
    Out = A + B;
}

void Unity_Branch_half(half Predicate, half True, half False, out half Out)
{
    Out = Predicate ? True : False;
}

void Unity_Saturate_half(half In, out half Out)
{
    Out = saturate(In);
}

void Unity_Branch_half4(half Predicate, half4 True, half4 False, out half4 Out)
{
    Out = Predicate ? True : False;
}

struct Bindings_OutlineSubGraph_cca9a3cdf4ebbbf47b3960f57cef445d_half
{
half4 uv0;
half4 uv1;
half4 uv2;
half4 uv3;
};

void SG_OutlineSubGraph_cca9a3cdf4ebbbf47b3960f57cef445d_half(UnityTexture2D _Main_Texture, UnityTexture2D ShadowMask, half _OutlineThickness, half4 _OutlineColor, half Object_Scale, half _Sample_Corners, half _Enable_Outline, Bindings_OutlineSubGraph_cca9a3cdf4ebbbf47b3960f57cef445d_half IN, out half4 Out_1)
{
half _Property_c255289e5e904c44b6f9179ec57e8af2_Out_0 = _Enable_Outline;
UnityTexture2D _Property_cdc04c299f174f8fba8ac249e4a2b232_Out_0 = _Main_Texture;
half4 _SampleTexture2D_29d7a1f29de44f6cbd20e287d4579b4c_RGBA_0 = SAMPLE_TEXTURE2D(_Property_cdc04c299f174f8fba8ac249e4a2b232_Out_0.tex, _Property_cdc04c299f174f8fba8ac249e4a2b232_Out_0.samplerstate, _Property_cdc04c299f174f8fba8ac249e4a2b232_Out_0.GetTransformedUV(IN.uv0.xy));
half _SampleTexture2D_29d7a1f29de44f6cbd20e287d4579b4c_R_4 = _SampleTexture2D_29d7a1f29de44f6cbd20e287d4579b4c_RGBA_0.r;
half _SampleTexture2D_29d7a1f29de44f6cbd20e287d4579b4c_G_5 = _SampleTexture2D_29d7a1f29de44f6cbd20e287d4579b4c_RGBA_0.g;
half _SampleTexture2D_29d7a1f29de44f6cbd20e287d4579b4c_B_6 = _SampleTexture2D_29d7a1f29de44f6cbd20e287d4579b4c_RGBA_0.b;
half _SampleTexture2D_29d7a1f29de44f6cbd20e287d4579b4c_A_7 = _SampleTexture2D_29d7a1f29de44f6cbd20e287d4579b4c_RGBA_0.a;
half _Comparison_113df203248543e0b192348b1a8ca785_Out_2;
Unity_Comparison_GreaterOrEqual_half(_SampleTexture2D_29d7a1f29de44f6cbd20e287d4579b4c_A_7, 0.5, _Comparison_113df203248543e0b192348b1a8ca785_Out_2);
UnityTexture2D _Property_6bef216196d24d9f8ac29403d83e213f_Out_0 = ShadowMask;
half4 _SampleTexture2D_a44f919f050c4fed96551b3631e81f51_RGBA_0 = SAMPLE_TEXTURE2D(_Property_6bef216196d24d9f8ac29403d83e213f_Out_0.tex, _Property_6bef216196d24d9f8ac29403d83e213f_Out_0.samplerstate, _Property_6bef216196d24d9f8ac29403d83e213f_Out_0.GetTransformedUV(IN.uv0.xy));
half _SampleTexture2D_a44f919f050c4fed96551b3631e81f51_R_4 = _SampleTexture2D_a44f919f050c4fed96551b3631e81f51_RGBA_0.r;
half _SampleTexture2D_a44f919f050c4fed96551b3631e81f51_G_5 = _SampleTexture2D_a44f919f050c4fed96551b3631e81f51_RGBA_0.g;
half _SampleTexture2D_a44f919f050c4fed96551b3631e81f51_B_6 = _SampleTexture2D_a44f919f050c4fed96551b3631e81f51_RGBA_0.b;
half _SampleTexture2D_a44f919f050c4fed96551b3631e81f51_A_7 = _SampleTexture2D_a44f919f050c4fed96551b3631e81f51_RGBA_0.a;
half _Comparison_0d24209b0fb341058e16996fba48a926_Out_2;
Unity_Comparison_Equal_half(_SampleTexture2D_a44f919f050c4fed96551b3631e81f51_R_4, 0, _Comparison_0d24209b0fb341058e16996fba48a926_Out_2);
half _Or_cb0f721ed05048fdb6d013b3fb9660f1_Out_2;
Unity_Or_half(_Comparison_113df203248543e0b192348b1a8ca785_Out_2, _Comparison_0d24209b0fb341058e16996fba48a926_Out_2, _Or_cb0f721ed05048fdb6d013b3fb9660f1_Out_2);
half4 _Property_d616b029977b4e1db01d282968de91f6_Out_0 = _OutlineColor;
half _Split_deed0eb77c5c4217b8fe5c258ed6fd50_R_1 = _Property_d616b029977b4e1db01d282968de91f6_Out_0[0];
half _Split_deed0eb77c5c4217b8fe5c258ed6fd50_G_2 = _Property_d616b029977b4e1db01d282968de91f6_Out_0[1];
half _Split_deed0eb77c5c4217b8fe5c258ed6fd50_B_3 = _Property_d616b029977b4e1db01d282968de91f6_Out_0[2];
half _Split_deed0eb77c5c4217b8fe5c258ed6fd50_A_4 = _Property_d616b029977b4e1db01d282968de91f6_Out_0[3];
UnityTexture2D _Property_8393ce88430c4c6d9da5262cf925a5b2_Out_0 = _Main_Texture;
half _Property_13fd7e2022034b6581763eec9309f978_Out_0 = _OutlineThickness;
half _Multiply_d43ab8d3309a4b59a266c7010724136d_Out_2;
Unity_Multiply_half_half(_Property_13fd7e2022034b6581763eec9309f978_Out_0, 1, _Multiply_d43ab8d3309a4b59a266c7010724136d_Out_2);
half _Property_f4839464878d492abaf90c3c7031397e_Out_0 = Object_Scale;
Bindings_OutlineSample_5935d51de82a0eb439fe5be3af628c2c_half _OutlineSample_9c588921577241adb732f880358d4022;
_OutlineSample_9c588921577241adb732f880358d4022.uv0 = IN.uv0;
_OutlineSample_9c588921577241adb732f880358d4022.uv1 = IN.uv1;
_OutlineSample_9c588921577241adb732f880358d4022.uv2 = IN.uv2;
_OutlineSample_9c588921577241adb732f880358d4022.uv3 = IN.uv3;
half _OutlineSample_9c588921577241adb732f880358d4022_A_2;
SG_OutlineSample_5935d51de82a0eb439fe5be3af628c2c_half(_Property_8393ce88430c4c6d9da5262cf925a5b2_Out_0, _Multiply_d43ab8d3309a4b59a266c7010724136d_Out_2, half2 (1, 0), _Property_f4839464878d492abaf90c3c7031397e_Out_0, _OutlineSample_9c588921577241adb732f880358d4022, _OutlineSample_9c588921577241adb732f880358d4022_A_2);
UnityTexture2D _Property_6a32b7a37b914c6fb5453d6bd89b088d_Out_0 = _Main_Texture;
half _Property_5bc0de20fe6c4c3583e41d6d15baf983_Out_0 = Object_Scale;
Bindings_OutlineSample_5935d51de82a0eb439fe5be3af628c2c_half _OutlineSample_76af5941489b4ce89b6a8cd30bf3a22e;
_OutlineSample_76af5941489b4ce89b6a8cd30bf3a22e.uv0 = IN.uv0;
_OutlineSample_76af5941489b4ce89b6a8cd30bf3a22e.uv1 = IN.uv1;
_OutlineSample_76af5941489b4ce89b6a8cd30bf3a22e.uv2 = IN.uv2;
_OutlineSample_76af5941489b4ce89b6a8cd30bf3a22e.uv3 = IN.uv3;
half _OutlineSample_76af5941489b4ce89b6a8cd30bf3a22e_A_2;
SG_OutlineSample_5935d51de82a0eb439fe5be3af628c2c_half(_Property_6a32b7a37b914c6fb5453d6bd89b088d_Out_0, _Multiply_d43ab8d3309a4b59a266c7010724136d_Out_2, half2 (-1, 0), _Property_5bc0de20fe6c4c3583e41d6d15baf983_Out_0, _OutlineSample_76af5941489b4ce89b6a8cd30bf3a22e, _OutlineSample_76af5941489b4ce89b6a8cd30bf3a22e_A_2);
half _Add_8691b8f8d7824e3f869c5b95271ca865_Out_2;
Unity_Add_half(_OutlineSample_9c588921577241adb732f880358d4022_A_2, _OutlineSample_76af5941489b4ce89b6a8cd30bf3a22e_A_2, _Add_8691b8f8d7824e3f869c5b95271ca865_Out_2);
UnityTexture2D _Property_2765b01eca6849af9d2e933c1e4f8151_Out_0 = _Main_Texture;
half _Property_2af9f9fed8644e77b7bd094e1375d0d3_Out_0 = Object_Scale;
Bindings_OutlineSample_5935d51de82a0eb439fe5be3af628c2c_half _OutlineSample_0a0ba9aa9e334bfc93af2c944e6cbaef;
_OutlineSample_0a0ba9aa9e334bfc93af2c944e6cbaef.uv0 = IN.uv0;
_OutlineSample_0a0ba9aa9e334bfc93af2c944e6cbaef.uv1 = IN.uv1;
_OutlineSample_0a0ba9aa9e334bfc93af2c944e6cbaef.uv2 = IN.uv2;
_OutlineSample_0a0ba9aa9e334bfc93af2c944e6cbaef.uv3 = IN.uv3;
half _OutlineSample_0a0ba9aa9e334bfc93af2c944e6cbaef_A_2;
SG_OutlineSample_5935d51de82a0eb439fe5be3af628c2c_half(_Property_2765b01eca6849af9d2e933c1e4f8151_Out_0, _Multiply_d43ab8d3309a4b59a266c7010724136d_Out_2, half2 (0, 1), _Property_2af9f9fed8644e77b7bd094e1375d0d3_Out_0, _OutlineSample_0a0ba9aa9e334bfc93af2c944e6cbaef, _OutlineSample_0a0ba9aa9e334bfc93af2c944e6cbaef_A_2);
UnityTexture2D _Property_9e70ee21208a4d28bbb9e0ad8f4360e2_Out_0 = _Main_Texture;
half _Property_f2691b6a70914ee690d60a36a8ada4a9_Out_0 = Object_Scale;
Bindings_OutlineSample_5935d51de82a0eb439fe5be3af628c2c_half _OutlineSample_d3bf438e8e834837a54c90a4a730d33d;
_OutlineSample_d3bf438e8e834837a54c90a4a730d33d.uv0 = IN.uv0;
_OutlineSample_d3bf438e8e834837a54c90a4a730d33d.uv1 = IN.uv1;
_OutlineSample_d3bf438e8e834837a54c90a4a730d33d.uv2 = IN.uv2;
_OutlineSample_d3bf438e8e834837a54c90a4a730d33d.uv3 = IN.uv3;
half _OutlineSample_d3bf438e8e834837a54c90a4a730d33d_A_2;
SG_OutlineSample_5935d51de82a0eb439fe5be3af628c2c_half(_Property_9e70ee21208a4d28bbb9e0ad8f4360e2_Out_0, _Multiply_d43ab8d3309a4b59a266c7010724136d_Out_2, half2 (0, -1), _Property_f2691b6a70914ee690d60a36a8ada4a9_Out_0, _OutlineSample_d3bf438e8e834837a54c90a4a730d33d, _OutlineSample_d3bf438e8e834837a54c90a4a730d33d_A_2);
half _Add_7c337a8390dd4a57aa8416a9149a06c6_Out_2;
Unity_Add_half(_OutlineSample_0a0ba9aa9e334bfc93af2c944e6cbaef_A_2, _OutlineSample_d3bf438e8e834837a54c90a4a730d33d_A_2, _Add_7c337a8390dd4a57aa8416a9149a06c6_Out_2);
half _Add_0ea77ddb233548f58c8ebe27e4d6cd7a_Out_2;
Unity_Add_half(_Add_8691b8f8d7824e3f869c5b95271ca865_Out_2, _Add_7c337a8390dd4a57aa8416a9149a06c6_Out_2, _Add_0ea77ddb233548f58c8ebe27e4d6cd7a_Out_2);
half _Property_723278bd5fc94777bfbb58c043b8f91e_Out_0 = _Sample_Corners;
UnityTexture2D _Property_55f29b9151634ee4acc5f2bc87271dac_Out_0 = _Main_Texture;
half _Property_a366500d0cfa484cb5500429d406d4f6_Out_0 = Object_Scale;
Bindings_OutlineSample_5935d51de82a0eb439fe5be3af628c2c_half _OutlineSample_a8ceaa015742447489dbdeae207a9cbb;
_OutlineSample_a8ceaa015742447489dbdeae207a9cbb.uv0 = IN.uv0;
_OutlineSample_a8ceaa015742447489dbdeae207a9cbb.uv1 = IN.uv1;
_OutlineSample_a8ceaa015742447489dbdeae207a9cbb.uv2 = IN.uv2;
_OutlineSample_a8ceaa015742447489dbdeae207a9cbb.uv3 = IN.uv3;
half _OutlineSample_a8ceaa015742447489dbdeae207a9cbb_A_2;
SG_OutlineSample_5935d51de82a0eb439fe5be3af628c2c_half(_Property_55f29b9151634ee4acc5f2bc87271dac_Out_0, _Multiply_d43ab8d3309a4b59a266c7010724136d_Out_2, half2 (1, 1), _Property_a366500d0cfa484cb5500429d406d4f6_Out_0, _OutlineSample_a8ceaa015742447489dbdeae207a9cbb, _OutlineSample_a8ceaa015742447489dbdeae207a9cbb_A_2);
UnityTexture2D _Property_cce5aee286e440848c1d0703b55757b6_Out_0 = _Main_Texture;
half _Property_e02b523d1ec642fbbd514361296777c0_Out_0 = Object_Scale;
Bindings_OutlineSample_5935d51de82a0eb439fe5be3af628c2c_half _OutlineSample_8f60110e6d7641ac8a8974e4507f42d6;
_OutlineSample_8f60110e6d7641ac8a8974e4507f42d6.uv0 = IN.uv0;
_OutlineSample_8f60110e6d7641ac8a8974e4507f42d6.uv1 = IN.uv1;
_OutlineSample_8f60110e6d7641ac8a8974e4507f42d6.uv2 = IN.uv2;
_OutlineSample_8f60110e6d7641ac8a8974e4507f42d6.uv3 = IN.uv3;
half _OutlineSample_8f60110e6d7641ac8a8974e4507f42d6_A_2;
SG_OutlineSample_5935d51de82a0eb439fe5be3af628c2c_half(_Property_cce5aee286e440848c1d0703b55757b6_Out_0, _Multiply_d43ab8d3309a4b59a266c7010724136d_Out_2, half2 (-1, -1), _Property_e02b523d1ec642fbbd514361296777c0_Out_0, _OutlineSample_8f60110e6d7641ac8a8974e4507f42d6, _OutlineSample_8f60110e6d7641ac8a8974e4507f42d6_A_2);
half _Add_9646efb05e5c41f09db53b61810918ee_Out_2;
Unity_Add_half(_OutlineSample_a8ceaa015742447489dbdeae207a9cbb_A_2, _OutlineSample_8f60110e6d7641ac8a8974e4507f42d6_A_2, _Add_9646efb05e5c41f09db53b61810918ee_Out_2);
UnityTexture2D _Property_8a83e549c02342f981d0805651966866_Out_0 = _Main_Texture;
half _Property_6adeb1ac1a8b4aaf9f084e8cdba7af28_Out_0 = Object_Scale;
Bindings_OutlineSample_5935d51de82a0eb439fe5be3af628c2c_half _OutlineSample_e9a7eeb825284ae6b79837723e7f3184;
_OutlineSample_e9a7eeb825284ae6b79837723e7f3184.uv0 = IN.uv0;
_OutlineSample_e9a7eeb825284ae6b79837723e7f3184.uv1 = IN.uv1;
_OutlineSample_e9a7eeb825284ae6b79837723e7f3184.uv2 = IN.uv2;
_OutlineSample_e9a7eeb825284ae6b79837723e7f3184.uv3 = IN.uv3;
half _OutlineSample_e9a7eeb825284ae6b79837723e7f3184_A_2;
SG_OutlineSample_5935d51de82a0eb439fe5be3af628c2c_half(_Property_8a83e549c02342f981d0805651966866_Out_0, _Multiply_d43ab8d3309a4b59a266c7010724136d_Out_2, half2 (1, -1), _Property_6adeb1ac1a8b4aaf9f084e8cdba7af28_Out_0, _OutlineSample_e9a7eeb825284ae6b79837723e7f3184, _OutlineSample_e9a7eeb825284ae6b79837723e7f3184_A_2);
UnityTexture2D _Property_3b3f7ef7bd6141eca70486df83864227_Out_0 = _Main_Texture;
half _Property_b2d34ad5666a44b79ee7b7c17ff2f0e2_Out_0 = Object_Scale;
Bindings_OutlineSample_5935d51de82a0eb439fe5be3af628c2c_half _OutlineSample_193fb0916dc740eaafac5913899a3194;
_OutlineSample_193fb0916dc740eaafac5913899a3194.uv0 = IN.uv0;
_OutlineSample_193fb0916dc740eaafac5913899a3194.uv1 = IN.uv1;
_OutlineSample_193fb0916dc740eaafac5913899a3194.uv2 = IN.uv2;
_OutlineSample_193fb0916dc740eaafac5913899a3194.uv3 = IN.uv3;
half _OutlineSample_193fb0916dc740eaafac5913899a3194_A_2;
SG_OutlineSample_5935d51de82a0eb439fe5be3af628c2c_half(_Property_3b3f7ef7bd6141eca70486df83864227_Out_0, _Multiply_d43ab8d3309a4b59a266c7010724136d_Out_2, half2 (-1, 1), _Property_b2d34ad5666a44b79ee7b7c17ff2f0e2_Out_0, _OutlineSample_193fb0916dc740eaafac5913899a3194, _OutlineSample_193fb0916dc740eaafac5913899a3194_A_2);
half _Add_f088d895e95842b198543866f527ce74_Out_2;
Unity_Add_half(_OutlineSample_e9a7eeb825284ae6b79837723e7f3184_A_2, _OutlineSample_193fb0916dc740eaafac5913899a3194_A_2, _Add_f088d895e95842b198543866f527ce74_Out_2);
half _Add_4ae0565d1a074a7984696a4b321d7b73_Out_2;
Unity_Add_half(_Add_9646efb05e5c41f09db53b61810918ee_Out_2, _Add_f088d895e95842b198543866f527ce74_Out_2, _Add_4ae0565d1a074a7984696a4b321d7b73_Out_2);
half _Branch_ea26f381df754b65860e90abe6376d34_Out_3;
Unity_Branch_half(_Property_723278bd5fc94777bfbb58c043b8f91e_Out_0, _Add_4ae0565d1a074a7984696a4b321d7b73_Out_2, 0, _Branch_ea26f381df754b65860e90abe6376d34_Out_3);
half _Add_c40eb27d5fdc404981a0ef66ab7df7e4_Out_2;
Unity_Add_half(_Add_0ea77ddb233548f58c8ebe27e4d6cd7a_Out_2, _Branch_ea26f381df754b65860e90abe6376d34_Out_3, _Add_c40eb27d5fdc404981a0ef66ab7df7e4_Out_2);
half _Saturate_0deffd336d1d4937af7e630518f968d5_Out_1;
Unity_Saturate_half(_Add_c40eb27d5fdc404981a0ef66ab7df7e4_Out_2, _Saturate_0deffd336d1d4937af7e630518f968d5_Out_1);
half _Multiply_9e5b4bce50194c33b5279ac0d4ea77bf_Out_2;
Unity_Multiply_half_half(_Split_deed0eb77c5c4217b8fe5c258ed6fd50_A_4, _Saturate_0deffd336d1d4937af7e630518f968d5_Out_1, _Multiply_9e5b4bce50194c33b5279ac0d4ea77bf_Out_2);
half4 _Vector4_e1f5c9722253465a8f11fea95672911a_Out_0 = half4(_Split_deed0eb77c5c4217b8fe5c258ed6fd50_R_1, _Split_deed0eb77c5c4217b8fe5c258ed6fd50_G_2, _Split_deed0eb77c5c4217b8fe5c258ed6fd50_B_3, _Multiply_9e5b4bce50194c33b5279ac0d4ea77bf_Out_2);
half4 _Branch_a2e396118cd2475384e5a6b92b3cd166_Out_3;
Unity_Branch_half4(_Or_cb0f721ed05048fdb6d013b3fb9660f1_Out_2, _SampleTexture2D_29d7a1f29de44f6cbd20e287d4579b4c_RGBA_0, _Vector4_e1f5c9722253465a8f11fea95672911a_Out_0, _Branch_a2e396118cd2475384e5a6b92b3cd166_Out_3);
half4 _Branch_b7083fb1fc724f4d87c58f86f23ec327_Out_3;
Unity_Branch_half4(_Property_c255289e5e904c44b6f9179ec57e8af2_Out_0, _Branch_a2e396118cd2475384e5a6b92b3cd166_Out_3, _SampleTexture2D_29d7a1f29de44f6cbd20e287d4579b4c_RGBA_0, _Branch_b7083fb1fc724f4d87c58f86f23ec327_Out_3);
Out_1 = _Branch_b7083fb1fc724f4d87c58f86f23ec327_Out_3;
}

void Unity_Subtract_half4(half4 A, half4 B, out half4 Out)
{
    Out = A - B;
}

void Unity_Clamp_half4(half4 In, half4 Min, half4 Max, out half4 Out)
{
    Out = clamp(In, Min, Max);
}

void Unity_Add_half4(half4 A, half4 B, out half4 Out)
{
    Out = A + B;
}

void Unity_Saturate_half4(half4 In, out half4 Out)
{
    Out = saturate(In);
}

struct Bindings_OutlineBackdrop_744c969b165a0da49ae2d86b538d1386_half
{
half4 uv0;
half4 uv1;
half4 uv2;
half4 uv3;
};

void SG_OutlineBackdrop_744c969b165a0da49ae2d86b538d1386_half(UnityTexture2D Main, UnityTexture2D ShadowMask, half OutlineThickness, half4 OutlineColor, half Object_Scale, half _Sample_Corners, half _Enable_Outline, Bindings_OutlineBackdrop_744c969b165a0da49ae2d86b538d1386_half IN, out half4 Out_1)
{
UnityTexture2D _Property_90a419dc4f614512ab95bea1b5a778a1_Out_0 = Main;
UnityTexture2D _Property_b759b0dd428b4448824d5ed140a96188_Out_0 = ShadowMask;
half _Property_866d9bd9ba3b48c0bc4f8257ff41473d_Out_0 = OutlineThickness;
half4 _Property_57bdc3b2d2a843b0a313592ba18dc1f8_Out_0 = OutlineColor;
half _Property_ebda404cd5e543c79f33e48f1d033064_Out_0 = Object_Scale;
half _Property_3c8f21ce8f794a1d97b919433e0d3111_Out_0 = _Sample_Corners;
half _Property_83b487b19a2849e195531d7c941a6555_Out_0 = _Enable_Outline;
Bindings_OutlineSubGraph_cca9a3cdf4ebbbf47b3960f57cef445d_half _OutlineSubGraph_5b841b12f1504112ad34b55f03658271;
_OutlineSubGraph_5b841b12f1504112ad34b55f03658271.uv0 = IN.uv0;
_OutlineSubGraph_5b841b12f1504112ad34b55f03658271.uv1 = IN.uv1;
_OutlineSubGraph_5b841b12f1504112ad34b55f03658271.uv2 = IN.uv2;
_OutlineSubGraph_5b841b12f1504112ad34b55f03658271.uv3 = IN.uv3;
half4 _OutlineSubGraph_5b841b12f1504112ad34b55f03658271_Out_1;
SG_OutlineSubGraph_cca9a3cdf4ebbbf47b3960f57cef445d_half(_Property_90a419dc4f614512ab95bea1b5a778a1_Out_0, _Property_b759b0dd428b4448824d5ed140a96188_Out_0, _Property_866d9bd9ba3b48c0bc4f8257ff41473d_Out_0, _Property_57bdc3b2d2a843b0a313592ba18dc1f8_Out_0, _Property_ebda404cd5e543c79f33e48f1d033064_Out_0, _Property_3c8f21ce8f794a1d97b919433e0d3111_Out_0, _Property_83b487b19a2849e195531d7c941a6555_Out_0, _OutlineSubGraph_5b841b12f1504112ad34b55f03658271, _OutlineSubGraph_5b841b12f1504112ad34b55f03658271_Out_1);
UnityTexture2D _Property_ea3503ea967c4a42a1bf1078dd71da43_Out_0 = Main;
UnityTexture2D _Property_f2a8238803b44425876470beba40dd0d_Out_0 = ShadowMask;
half _Property_d2d0fb311c044fddaa652344afa16d54_Out_0 = OutlineThickness;
half _Multiply_204c0193bc4e49ecaa0f7a29ce53ec5f_Out_2;
Unity_Multiply_half_half(_Property_d2d0fb311c044fddaa652344afa16d54_Out_0, 2, _Multiply_204c0193bc4e49ecaa0f7a29ce53ec5f_Out_2);
half4 _Property_931f0829dc3a49b6bdb44c7227a71563_Out_0 = OutlineColor;
half4 _Multiply_9239990124df4c0b93122f9b7bd8e228_Out_2;
Unity_Multiply_half4_half4(_Property_931f0829dc3a49b6bdb44c7227a71563_Out_0, half4(1, 1, 1, 0.25), _Multiply_9239990124df4c0b93122f9b7bd8e228_Out_2);
half _Property_b8693891041d4b5c842acc907790d989_Out_0 = Object_Scale;
half _Property_4d9d03a33e7d44b4a14fe8b22fce75b3_Out_0 = _Sample_Corners;
half _Property_a3dd0e92844442c3a42b3b183b8748ac_Out_0 = _Enable_Outline;
Bindings_OutlineSubGraph_cca9a3cdf4ebbbf47b3960f57cef445d_half _OutlineSubGraph_fa6f19d5925e4f2589558d0537a65dfd;
_OutlineSubGraph_fa6f19d5925e4f2589558d0537a65dfd.uv0 = IN.uv0;
_OutlineSubGraph_fa6f19d5925e4f2589558d0537a65dfd.uv1 = IN.uv1;
_OutlineSubGraph_fa6f19d5925e4f2589558d0537a65dfd.uv2 = IN.uv2;
_OutlineSubGraph_fa6f19d5925e4f2589558d0537a65dfd.uv3 = IN.uv3;
half4 _OutlineSubGraph_fa6f19d5925e4f2589558d0537a65dfd_Out_1;
SG_OutlineSubGraph_cca9a3cdf4ebbbf47b3960f57cef445d_half(_Property_ea3503ea967c4a42a1bf1078dd71da43_Out_0, _Property_f2a8238803b44425876470beba40dd0d_Out_0, _Multiply_204c0193bc4e49ecaa0f7a29ce53ec5f_Out_2, _Multiply_9239990124df4c0b93122f9b7bd8e228_Out_2, _Property_b8693891041d4b5c842acc907790d989_Out_0, _Property_4d9d03a33e7d44b4a14fe8b22fce75b3_Out_0, _Property_a3dd0e92844442c3a42b3b183b8748ac_Out_0, _OutlineSubGraph_fa6f19d5925e4f2589558d0537a65dfd, _OutlineSubGraph_fa6f19d5925e4f2589558d0537a65dfd_Out_1);
half4 _Subtract_76793de1ebc440058c6e3a560b990497_Out_2;
Unity_Subtract_half4(_OutlineSubGraph_fa6f19d5925e4f2589558d0537a65dfd_Out_1, _OutlineSubGraph_5b841b12f1504112ad34b55f03658271_Out_1, _Subtract_76793de1ebc440058c6e3a560b990497_Out_2);
half4 _Clamp_9e0614f26838413697db05ad993d0e13_Out_3;
Unity_Clamp_half4(_Subtract_76793de1ebc440058c6e3a560b990497_Out_2, half4(0, 0, 0, 0), half4(1, 1, 1, 1), _Clamp_9e0614f26838413697db05ad993d0e13_Out_3);
half4 _Add_c0dc666fdaed45938e990d581939b301_Out_2;
Unity_Add_half4(_OutlineSubGraph_5b841b12f1504112ad34b55f03658271_Out_1, _Clamp_9e0614f26838413697db05ad993d0e13_Out_3, _Add_c0dc666fdaed45938e990d581939b301_Out_2);
half4 _Saturate_02e190d6960148d7b7f9e11c9393bc52_Out_1;
Unity_Saturate_half4(_Add_c0dc666fdaed45938e990d581939b301_Out_2, _Saturate_02e190d6960148d7b7f9e11c9393bc52_Out_1);
Out_1 = _Saturate_02e190d6960148d7b7f9e11c9393bc52_Out_1;
}

/* WARNING: $splice Could not find named fragment 'CustomInterpolatorPreVertex' */

// Graph Vertex
struct VertexDescription
{
    half3 Position;
    half3 Normal;
    half3 Tangent;
};

VertexDescription VertexDescriptionFunction(VertexDescriptionInputs IN)
{
    VertexDescription description = (VertexDescription)0;
    description.Position = IN.ObjectSpacePosition;
    description.Normal = IN.ObjectSpaceNormal;
    description.Tangent = IN.ObjectSpaceTangent;
    return description;
}

    #ifdef FEATURES_GRAPH_VERTEX
Varyings CustomInterpolatorPassThroughFunc(inout Varyings output, VertexDescription input)
{
return output;
}
#define CUSTOMINTERPOLATOR_VARYPASSTHROUGH_FUNC
#endif

// Graph Pixel
struct SurfaceDescription
{
    half3 BaseColor;
    half Alpha;
};

SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
{
    SurfaceDescription surface = (SurfaceDescription)0;
    UnityTexture2D _Property_31e6e86d16a54fdc93167323dd1bb61c_Out_0 = UnityBuildTexture2DStructNoScale(_Mask);
    half4 _SampleTexture2D_6ff5c694df00418195f0a3073c6659b3_RGBA_0 = SAMPLE_TEXTURE2D(_Property_31e6e86d16a54fdc93167323dd1bb61c_Out_0.tex, _Property_31e6e86d16a54fdc93167323dd1bb61c_Out_0.samplerstate, _Property_31e6e86d16a54fdc93167323dd1bb61c_Out_0.GetTransformedUV(IN.uv0.xy));
    half _SampleTexture2D_6ff5c694df00418195f0a3073c6659b3_R_4 = _SampleTexture2D_6ff5c694df00418195f0a3073c6659b3_RGBA_0.r;
    half _SampleTexture2D_6ff5c694df00418195f0a3073c6659b3_G_5 = _SampleTexture2D_6ff5c694df00418195f0a3073c6659b3_RGBA_0.g;
    half _SampleTexture2D_6ff5c694df00418195f0a3073c6659b3_B_6 = _SampleTexture2D_6ff5c694df00418195f0a3073c6659b3_RGBA_0.b;
    half _SampleTexture2D_6ff5c694df00418195f0a3073c6659b3_A_7 = _SampleTexture2D_6ff5c694df00418195f0a3073c6659b3_RGBA_0.a;
    half _ColorMask_df87682a03d54ad693d60342f0c457f5_Out_3;
    Unity_ColorMask_half((_SampleTexture2D_6ff5c694df00418195f0a3073c6659b3_RGBA_0.xyz), IsGammaSpace() ? half3(1, 0, 0) : SRGBToLinear(half3(1, 0, 0)), 0, _ColorMask_df87682a03d54ad693d60342f0c457f5_Out_3, 0);
    half _Property_ec17d3661ef1cd80850f94f5fc3cf8ab_Out_0 = _FlashRate;
    half4 _Property_d22763b69abd28838f68a1f3ddf746d6_Out_0 = _FlashColor;
    half4 _GameEntityFlashCustomFunction_d3e355381dc0ce86b779cb1ae730470e_Out_3;
    GameEntity_Flash_half(IN.TimeParameters.x, _Property_ec17d3661ef1cd80850f94f5fc3cf8ab_Out_0, (_Property_d22763b69abd28838f68a1f3ddf746d6_Out_0.xyz), _GameEntityFlashCustomFunction_d3e355381dc0ce86b779cb1ae730470e_Out_3);
    half4 _Multiply_bee050f977214478a63d598505cb0478_Out_2;
    Unity_Multiply_half4_half4((_ColorMask_df87682a03d54ad693d60342f0c457f5_Out_3.xxxx), _GameEntityFlashCustomFunction_d3e355381dc0ce86b779cb1ae730470e_Out_3, _Multiply_bee050f977214478a63d598505cb0478_Out_2);
    UnityTexture2D _Property_9148162509cc4b3cafc17cf5d22974ef_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
    half _Property_ea02751492574ed392eff2590e4bc651_Out_0 = OutlineThickness;
    half4 _Property_7064f4e4d6e04f8881e235bd36abb3c6_Out_0 = OutlineColor;
    half _Property_9a458f8f5d4e4fa781a096d41cdba96a_Out_0 = ObjectScale;
    Bindings_OutlineBackdrop_744c969b165a0da49ae2d86b538d1386_half _OutlineBackdrop_bb9928ba826749338c5c4e9b5aab4482;
    _OutlineBackdrop_bb9928ba826749338c5c4e9b5aab4482.uv0 = IN.uv0;
    _OutlineBackdrop_bb9928ba826749338c5c4e9b5aab4482.uv1 = IN.uv1;
    _OutlineBackdrop_bb9928ba826749338c5c4e9b5aab4482.uv2 = IN.uv2;
    _OutlineBackdrop_bb9928ba826749338c5c4e9b5aab4482.uv3 = IN.uv3;
    half4 _OutlineBackdrop_bb9928ba826749338c5c4e9b5aab4482_Out_1;
    SG_OutlineBackdrop_744c969b165a0da49ae2d86b538d1386_half(_Property_9148162509cc4b3cafc17cf5d22974ef_Out_0, UnityBuildTexture2DStructNoScale(_OutlineBackdrop_bb9928ba826749338c5c4e9b5aab4482_ShadowMask_1013696677), _Property_ea02751492574ed392eff2590e4bc651_Out_0, _Property_7064f4e4d6e04f8881e235bd36abb3c6_Out_0, _Property_9a458f8f5d4e4fa781a096d41cdba96a_Out_0, 1, 1, _OutlineBackdrop_bb9928ba826749338c5c4e9b5aab4482, _OutlineBackdrop_bb9928ba826749338c5c4e9b5aab4482_Out_1);
    half4 _Add_df9549590666928c9d76e49de42c3aba_Out_2;
    Unity_Add_half4(_Multiply_bee050f977214478a63d598505cb0478_Out_2, _OutlineBackdrop_bb9928ba826749338c5c4e9b5aab4482_Out_1, _Add_df9549590666928c9d76e49de42c3aba_Out_2);
    half _Split_85e876829c3848e3882b84add8bb43c1_R_1 = _Add_df9549590666928c9d76e49de42c3aba_Out_2[0];
    half _Split_85e876829c3848e3882b84add8bb43c1_G_2 = _Add_df9549590666928c9d76e49de42c3aba_Out_2[1];
    half _Split_85e876829c3848e3882b84add8bb43c1_B_3 = _Add_df9549590666928c9d76e49de42c3aba_Out_2[2];
    half _Split_85e876829c3848e3882b84add8bb43c1_A_4 = _Add_df9549590666928c9d76e49de42c3aba_Out_2[3];
    half _Property_f12dfe796cdd433b8893f05a405c359a_Out_0 = _Alpha;
    half _Multiply_9c23e6f8f2194ffaa4e75b49ea9e0fbb_Out_2;
    Unity_Multiply_half_half(_Split_85e876829c3848e3882b84add8bb43c1_A_4, _Property_f12dfe796cdd433b8893f05a405c359a_Out_0, _Multiply_9c23e6f8f2194ffaa4e75b49ea9e0fbb_Out_2);
    surface.BaseColor = (_Add_df9549590666928c9d76e49de42c3aba_Out_2.xyz);
    surface.Alpha = _Multiply_9c23e6f8f2194ffaa4e75b49ea9e0fbb_Out_2;
    return surface;
}

// --------------------------------------------------
// Build Graph Inputs

VertexDescriptionInputs BuildVertexDescriptionInputs(Attributes input)
{
    VertexDescriptionInputs output;
    ZERO_INITIALIZE(VertexDescriptionInputs, output);

    output.ObjectSpaceNormal = input.normalOS;
    output.ObjectSpaceTangent = input.tangentOS.xyz;
    output.ObjectSpacePosition = input.positionOS;

    return output;
}
    SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
{
    SurfaceDescriptionInputs output;
    ZERO_INITIALIZE(SurfaceDescriptionInputs, output);







    output.uv0 = input.texCoord0;
    output.uv1 = input.texCoord1;
    output.uv2 = input.texCoord2;
    output.uv3 = input.texCoord3;
    output.TimeParameters = _TimeParameters.xyz; // This is mainly for LW as HD overwrite this value
#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN                output.FaceSign =                                   IS_FRONT_VFACE(input.cullFace, true, false);
#else
#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
#endif
#undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN

    return output;
}

    // --------------------------------------------------
    // Main

    #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
#include "Packages/com.unity.render-pipelines.universal/Editor/2D/ShaderGraph/Includes/SpriteUnlitPass.hlsl"

    ENDHLSL
}
    }
        CustomEditor "UnityEditor.ShaderGraph.GenericShaderGraphMaterialGUI"
        FallBack "Hidden/Shader Graph/FallbackError"
}