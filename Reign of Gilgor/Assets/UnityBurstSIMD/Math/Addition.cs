using System.Runtime.CompilerServices;
using AOT;
using Unity.Burst;

namespace UnityBurstSIMD.Math
{
    [BurstCompile(CompileSynchronously = true)]
    public static unsafe class Addition
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Add(int[] a, int[] b, ref int[] output)
        {
            fixed (int* aPointer = a, bPointer = b, outputPointer = output)
            {
                Add(aPointer, bPointer, outputPointer, a.Length);
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Add(int[] a, int b, ref int[] output)
        {
            fixed (int* aPointer = a, outputPointer = output)
            {
                Add(aPointer, b, outputPointer, a.Length);
            }
        }

        [MonoPInvokeCallback(typeof(Addition))]
        [BurstCompile(CompileSynchronously = true)]
        private static void Add(int* a, int* b, int* output, int count)
        {
            for (var i = 0; i < count; i++)
            {
                output[i] = a[i] + b[i];
            }
        }

        [MonoPInvokeCallback(typeof(Addition))]
        [BurstCompile(CompileSynchronously = true)]
        private static void Add(int* a, int b, int* output, int count)
        {
            for (var i = 0; i < count; i++)
            {
                output[i] = a[i] + b;
            }
        }
    }
}