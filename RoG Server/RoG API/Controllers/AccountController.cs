﻿namespace RoGAPI.Controllers;

using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.RateLimiting;
using RoGAPI.UseCases.Account;
using RoGAPI.UseCases.Character;
using RoGCore.Networking.APIViewModels;
using RoGCore.Utils;
using static RoGCore.Utils.ResultExtenstions;

[ApiController]
[Route("account")]
public sealed class AccountController(
    IAccountUseCases _accountUseCases,
    ICharacterUseCases _characterUseCases,
    IMapper _mapper) : ControllerBase
{
    [HttpGet("{username}")]
    public async Task<ActionResult<AccountViewModel>> GetAccountByUsername(string username, string password)
    {
        var accountResult = await _accountUseCases.ReadAccount(username);
        if (accountResult.IsFailed)
        {
            return BadRequest(accountResult.ErrorString());
        }

        var account = accountResult.Value;
        var accountValidated = _accountUseCases.ValidateAccount(account, password);
        if (accountValidated.IsFailed)
        {
            return BadRequest(accountValidated.ErrorString());
        }

        var characters = await _characterUseCases.GetAliveCharacters(account);

        var result = _mapper.Map<AccountViewModel>(account);
        result.Characters = _mapper.Map<IList<CharacterViewModel>>(characters);

        return Ok(result);
    }

    [HttpGet("register")]
    [EnableRateLimiting("five-minute")]
    public async Task<ActionResult<AccountViewModel>> Register(string username, string password)
    {
        var accountResult = await _accountUseCases.CreateAccount(username, password);
        if (accountResult.IsFailed)
        {
            return BadRequest(accountResult.ErrorString());
        }

        return await GetAccountByUsername(username, password);
    }
}
