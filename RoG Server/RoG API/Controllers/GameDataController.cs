using Microsoft.AspNetCore.Mvc;
using RoGAPI.DataAccess.GameData;
using RoGCore.Networking.APIViewModels;

namespace RoGAPI.Controllers;

[ApiController]
[Route("game-data")]
public sealed class GameDataController(IGameDataAccess _gameDataAccess) : ControllerBase
{
    [HttpGet]
    public async Task<ActionResult<GameDataViewModel>> GetGameData()
    {
        return Ok(new GameDataViewModel()
        {
            Ground = await _gameDataAccess.GetGroundData(),
            Objects = await _gameDataAccess.GetObjectData(),
            Items = await _gameDataAccess.GetItemData(),
            Projectiles = await _gameDataAccess.GetProjectileData(),
            Skills = await _gameDataAccess.GetSkillData(),
        });
    }
}
