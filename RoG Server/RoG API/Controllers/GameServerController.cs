﻿using Microsoft.AspNetCore.Mvc;
using RoGAPI.InterServer;
using RoGCore.Models;

namespace RoGAPI.Controllers;

[ApiController]
[Route("game-server")]
public sealed class GameServerController(IInterServerService _interServerService) : ControllerBase
{
    [HttpGet]
    public ActionResult<List<GameServer>> GetAllGameServers()
    {
        var gameServers = _interServerService.GameServers().Values.ToList();
        return Ok(gameServers);
    }
}
