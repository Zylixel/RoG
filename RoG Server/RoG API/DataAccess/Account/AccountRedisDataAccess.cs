﻿using Database;
using Database.Models;

namespace RoGAPI.DataAccess.Account;

public sealed class AccountRedisDataAccess : IAccountDataAccess
{
    public Task<DbAccount?> ReadAccount(string username)
    {
        return RedisDatabase.ReadAccount(username);
    }

    public Task<DbAccount> CreateAccount(string username, string password)
    {
        return RedisDatabase.CreateAccount(username, password);
    }
}
