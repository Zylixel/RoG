﻿using Database.Models;

namespace RoGAPI.DataAccess.Account;

public interface IAccountDataAccess
{
    public Task<DbAccount?> ReadAccount(string username);

    public Task<DbAccount> CreateAccount(string username, string password);
}
