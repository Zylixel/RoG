﻿using Database;
using Database.Models;

namespace RoGAPI.DataAccess.Character;

public sealed class CharacterRedisDataAccess : ICharacterDataAccess
{
    public Task<IEnumerable<DbChar>> GetAliveCharacters(DbAccount account)
    {
        return RedisDatabase.GetAliveCharacters(account);
    }
}
