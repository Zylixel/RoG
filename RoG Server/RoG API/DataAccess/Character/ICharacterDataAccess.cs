﻿using Database.Models;

namespace RoGAPI.DataAccess.Character;

public interface ICharacterDataAccess
{
    public Task<IEnumerable<DbChar>> GetAliveCharacters(DbAccount account);
}
