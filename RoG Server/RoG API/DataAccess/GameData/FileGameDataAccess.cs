﻿using RoGCore.Assets;

namespace RoGAPI.DataAccess.GameData;

public class FileGameDataAccess : IGameDataAccess
{
    public FileGameDataAccess()
    {
        EmbeddedData.BuildDataAsync().GetAwaiter().GetResult();
    }

    public Task<string> GetGroundData()
    {
        return Task.FromResult(EmbeddedData.GroundFile);
    }

    public Task<string> GetObjectData()
    {
        return Task.FromResult(EmbeddedData.ObjectFile);
    }

    public Task<string> GetItemData()
    {
        return Task.FromResult(EmbeddedData.ItemFile);
    }

    public Task<string> GetProjectileData()
    {
        return Task.FromResult(EmbeddedData.ProjectileFile);
    }

    public Task<string> GetSkillData()
    {
        return Task.FromResult(EmbeddedData.SkillFile);
    }
}
