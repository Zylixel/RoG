﻿namespace RoGAPI.DataAccess.GameData;

public interface IGameDataAccess
{
    public Task<string> GetGroundData();

    public Task<string> GetObjectData();

    public Task<string> GetItemData();

    public Task<string> GetProjectileData();

    public Task<string> GetSkillData();
}
