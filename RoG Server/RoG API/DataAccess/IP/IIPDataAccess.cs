﻿namespace RoGAPI.DataAccess.IP;

public interface IIPDataAccess
{
    public Task<bool> GetIPBanned(string ip);
}
