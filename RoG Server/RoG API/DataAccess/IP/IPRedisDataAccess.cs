﻿using Database;

namespace RoGAPI.DataAccess.IP;

public sealed class IPRedisDataAccess : IIPDataAccess
{
    public Task<bool> GetIPBanned(string ip)
    {
        return RedisDatabase.GetIPBanned(ip);
    }
}
