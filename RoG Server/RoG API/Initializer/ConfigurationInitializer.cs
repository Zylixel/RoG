﻿namespace RoGAPI.Initializer;

internal static class ConfigurationInitializer
{
    private const string SettingsFileName = "appsettings.json";

    internal static void Initialize(this ConfigurationManager configurationManager)
    {
        if (AppSettingsInDirectory())
        {
            return;
        }

        var relativePath = Path.Combine("..", "RoG Core", SettingsFileName);
        var absolutePath = Path.GetFullPath(relativePath);
        configurationManager.AddJsonFile(absolutePath);
    }

    internal static bool AppSettingsInDirectory()
    {
        return File.Exists(SettingsFileName);
    }
}
