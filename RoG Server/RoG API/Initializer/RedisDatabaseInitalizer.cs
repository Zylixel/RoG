using Database;
using Microsoft.Extensions.Options;
using RoGCore.SettingsModels;

namespace RoGAPI.Initializer;

internal sealed class RedisDatabaseInitalizer
{
    public RedisDatabaseInitalizer(IOptions<AppSettings> appSettings)
    {
        RedisDatabase.BuildAsync(appSettings.Value).GetAwaiter().GetResult();
    }
}