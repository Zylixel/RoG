using System.Collections.Concurrent;
using Database.InterServer;
using Database.Models;
using RoGCore.Models;
using static Database.InterServer.NetworkMsg;

namespace RoGAPI.InterServer;

internal class APIInterServerManager : InterServerManager, IInterServerService
{
    internal readonly ConcurrentDictionary<Guid, GameServer> GameServers = new();

    public APIInterServerManager(ILogger<InterServerManager> logger)
        : base(logger, NetworkType.API, "API")
    {
        Run();
    }

    IDictionary<Guid, GameServer> IInterServerService.GameServers()
    {
        return GameServers;
    }

    protected override void InstanceDisconnected(in Guid instanceId)
    {
        GameServers.TryRemove(instanceId, out _);
        base.InstanceDisconnected(instanceId);
    }

    protected override void HandleNetwork(object? sender, InterServerEventArgs<NetworkMsg> e)
    {
        switch (e.Content.Code)
        {
            case NetworkCode.Join:
            case NetworkCode.Ping:
                if (e.Content.Type == NetworkType.WorldServer && e.Content.ServerItem is not null)
                {
                    GameServers[e.InstanceId] = e.Content.ServerItem;
                }

                break;
        }

        base.HandleNetwork(sender, e);
    }
}
