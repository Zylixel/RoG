﻿using RoGCore.Models;

namespace RoGAPI.InterServer;

public interface IInterServerService
{
    IDictionary<Guid, GameServer> GameServers();

    void Dispose();
}