using Database.Models;
using RoGCore.Networking.APIViewModels;

namespace RoGAPI.Mapper;

public static class AccountMapper
{
    public static MappingProfile SetupAccountMapping(this MappingProfile mappingProfile)
    {
        mappingProfile.CreateMap<DbAccount, AccountViewModel>();

        return mappingProfile;
    }
}