using Database.Models;
using RoGCore.Networking.APIViewModels;

namespace RoGAPI.Mapper;

public static class CharacterMapper
{
    public static MappingProfile SetupCharacterMapping(this MappingProfile mappingProfile)
    {
        mappingProfile.CreateMap<DbChar, CharacterViewModel>();

        return mappingProfile;
    }
}