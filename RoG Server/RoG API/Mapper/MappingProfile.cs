using AutoMapper;

namespace RoGAPI.Mapper;

public sealed class MappingProfile : Profile
{
    public MappingProfile()
    {
        this.SetupAccountMapping();
        this.SetupCharacterMapping();
    }
}