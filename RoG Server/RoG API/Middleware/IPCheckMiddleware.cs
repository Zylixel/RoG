using RoGAPI.DataAccess.IP;

namespace RoGAPI.Middleware;

internal class IPCheckMiddleware(RequestDelegate _next, IIPDataAccess _ipDataAccess)
{
    public async Task InvokeAsync(HttpContext context)
    {
        var ip = context.Connection.RemoteIpAddress?.ToString();
        if (ip is null)
        {
            await Unauthorized(context, "Could not resolve IP Address");
            return;
        }

        var ipBanned = await _ipDataAccess.GetIPBanned(ip);
        if (ipBanned)
        {
            await Unauthorized(context, "IP Banned");
            return;
        }

        await _next(context);
    }

    private static async Task Unauthorized(HttpContext context, string message)
    {
        context.Response.StatusCode = StatusCodes.Status401Unauthorized;
        await context.Response.WriteAsync(message);
    }
}