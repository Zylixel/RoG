using RoGAPI.DataAccess.Account;
using RoGAPI.DataAccess.Character;
using RoGAPI.DataAccess.GameData;
using RoGAPI.DataAccess.IP;
using RoGAPI.Initializer;
using RoGAPI.InterServer;
using RoGAPI.Mapper;
using RoGAPI.Middleware;
using RoGAPI.UseCases.Account;
using RoGAPI.UseCases.Character;
using RoGCore.SettingsModels;
using Serilog;

namespace RoGAPI;

public static class Program
{
    public static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);

        builder.Configuration.Initialize();

        builder.Services.AddControllers();
        builder.Services.Configure<AppSettings>(builder.Configuration);
        builder.Services.AddSingleton<IInterServerService, APIInterServerManager>();
        builder.Services.AddSingleton<IAccountDataAccess, AccountRedisDataAccess>();
        builder.Services.AddSingleton<ICharacterDataAccess, CharacterRedisDataAccess>();
        builder.Services.AddSingleton<IGameDataAccess, FileGameDataAccess>();
        builder.Services.AddSingleton<IIPDataAccess, IPRedisDataAccess>();
        builder.Services.AddSingleton<IAccountUseCases, AccountUseCases>();
        builder.Services.AddSingleton<ICharacterUseCases, CharacterUseCases>();
        builder.Services.AddSingleton<RedisDatabaseInitalizer>();

        builder.Services.AddAutoMapper(typeof(MappingProfile));

        builder.Host.UseSerilog((context, _, configuration) =>
            configuration
                .ReadFrom.Configuration(context.Configuration)
                .Enrich.FromLogContext());

        var app = builder.Build();

        app.MapControllers();

        app.UseMiddleware<IPCheckMiddleware>();

        app.UseHttpsRedirection();
        app.UseSerilogRequestLogging();

        // Startup Services
        app.Services.GetRequiredService<RedisDatabaseInitalizer>();
        app.Services.GetRequiredService<IInterServerService>();

        app.Run();
    }
}
