using Database.Models;
using FluentResults;
using RoGAPI.DataAccess.Account;

namespace RoGAPI.UseCases.Account;

internal sealed class AccountUseCases(IAccountDataAccess _accountDataAccess) : IAccountUseCases
{
    public async Task<Result<DbAccount>> ReadAccount(string username)
    {
        var account = await _accountDataAccess.ReadAccount(username);
        if (account is null)
        {
            return Result.Fail("Account not found");
        }

        return Result.Ok(account);
    }

    public Result ValidateAccount(DbAccount account, string password)
    {
        return Result.FailIf(account.Password != password, "Invalid password");
    }

    public async Task<Result<DbAccount>> CreateAccount(string username, string password)
    {
        switch (username.Length)
        {
            case < 4:
                return Result.Fail("Username too short");
            case > 16:
                return Result.Fail("Username too large");
        }

        var account = await _accountDataAccess.ReadAccount(username);
        if (account is not null)
        {
            return Result.Fail("Username taken");
        }

        return await _accountDataAccess.CreateAccount(username, password);
    }
}