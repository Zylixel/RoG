using Database.Models;
using FluentResults;

namespace RoGAPI.UseCases.Account;

public interface IAccountUseCases
{
    public Task<Result<DbAccount>> ReadAccount(string username);

    public Result ValidateAccount(DbAccount account, string password);

    public Task<Result<DbAccount>> CreateAccount(string username, string password);
}