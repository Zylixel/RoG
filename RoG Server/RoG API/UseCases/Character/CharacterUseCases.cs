﻿using Database.Models;
using RoGAPI.DataAccess.Character;

namespace RoGAPI.UseCases.Character;

public class CharacterUseCases(ICharacterDataAccess _characterDataAccess) : ICharacterUseCases
{
    public async Task<IList<DbChar>> GetAliveCharacters(DbAccount account)
    {
        return (await _characterDataAccess.GetAliveCharacters(account)).OfType<DbChar>().ToList();
    }
}
