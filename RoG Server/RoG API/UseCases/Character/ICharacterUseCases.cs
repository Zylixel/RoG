using Database.Models;

namespace RoGAPI.UseCases.Character;

public interface ICharacterUseCases
{
    public Task<IList<DbChar>> GetAliveCharacters(DbAccount account);
}