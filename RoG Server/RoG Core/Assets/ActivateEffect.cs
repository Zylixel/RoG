﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using RoGCore.Models;

namespace RoGCore.Assets;

[Serializable]
public class ActivateEffect
{
    [JsonConverter(typeof(StringEnumConverter))]
    public ActivateEffects Effect;

    [JsonConverter(typeof(StringEnumConverter))]
    public ConditionEffectIndex? ConditionEffect;

    [JsonProperty(ItemConverterType = typeof(StringEnumConverter))]
    public SkillType[]? RequiredSkills;

    public int Stats;

    public ushort Amount;

    public float Range;

    public int Damage;

    [JsonProperty("DurationMs")]
    public int BaseDurationMs;

    public float EffectDuration;

    public float MaximumHitPoints;

    public float MaximumDistance;

    public float Radius;

    public int TotalDamage;

    public string ObjectId = string.Empty;

    public int AngleOffset;

    public int MaxTargets;

    public string Id = string.Empty;

    public int SkinType;

    public string? DungeonName;

    public string? LockedName;

    public string? Target;

    public string? Center;

    public bool UseWisMod;

    public float VisualEffect;

    public uint? Color;

    public bool NoStack;

    public int OffsetAngle;

    public float GapTiles;

    public int NumShots;

    public int GapAngle;

    public float Strength;

    public bool IgnoreDefense;

    public int? RevokeOnDamage;

    public int GetDurationMs(IEnumerable<Skill> currentSkills)
    {
        if (BaseDurationMs == 0)
        {
            return 0;
        }

        float duration = BaseDurationMs;

        foreach (var currentSkill in currentSkills)
        {
            duration += currentSkill.ActivateDurationModifier;
        }

        foreach (var currentSkill in currentSkills)
        {
            duration *= currentSkill.ActivateDurationMultiplier;
        }

        if (Effect == ActivateEffects.Summon && currentSkills.Select(skill => skill.Type).Contains(SkillType.Clone2))
        {
            duration += 1000;
        }

        return (int)Math.Max(duration, 0);
    }
}