﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace RoGCore.Assets;

[Serializable]
public class AudioDescription
{
    public string? ClipName;

    [JsonConverter(typeof(StringEnumConverter))]
    public AudioType Type;

    public float Pitch = 1;

    public float PitchVariation = 0;

    public float VolumeMulitplier = 1;
}