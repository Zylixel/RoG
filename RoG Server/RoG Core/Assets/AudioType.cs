﻿namespace RoGCore.Assets;

public enum AudioType : byte
{
    Sound,
    Music,
}