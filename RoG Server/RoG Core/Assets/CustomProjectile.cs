﻿using Newtonsoft.Json;
using RoGCore.Models;

namespace RoGCore.Assets;

[Serializable]
public class CustomProjectile
{
    public const string InvalidName = "Invalid Name";

    public string Name = InvalidName;

    [JsonProperty("Damage")]
    public int BaseDamage;

    [JsonProperty("Speed")]
    public int BaseSpeed;

    [JsonProperty("Lifetime")]
    public int BaseLifeTime;

    public int Size = 100;

    public bool Wavy;

    public bool Boomerang;

    public bool Parametric;

    public float Magnitude;

    public float Amplitude;

    public float Frequency;

    public float Orbit;

    public bool MultiHit;

    public bool ArmorPiercing;

    public float? InitialVelocity;

    public float Acceleration;

    public float InitialWaitTime;

    public ConditionEffect[]? Effects;

    public int EffectProbability;

    public int GetDamage(Item? item, IEnumerable<Skill> skills)
    {
        if (item is null)
        {
            return BaseDamage;
        }

        float damage = BaseDamage;

        foreach (var skill in skills)
        {
            if (item.Value.BaseItem.ItemType == ItemType.Weapon)
            {
                damage *= skill.WeaponDamageMultiplier;
            }
        }

        damage *= item.Value.Data.LevelStatMultiplier();

        return (int)damage;
    }

    public int GetSpeed(JsonItem? item, IEnumerable<Skill>? skills)
    {
        if (item is null || skills is null)
        {
            return BaseSpeed;
        }

        float speed = BaseSpeed;

        if (item.ItemType == ItemType.Weapon)
        {
            foreach (var skill in skills)
            {
                speed *= skill.WeaponProjectileSpeedMultiplier;
            }
        }

        return (int)speed;
    }

    public int GetLifetime(JsonItem? item, IEnumerable<Skill>? skills)
    {
        if (item is null || skills is null)
        {
            return BaseLifeTime;
        }

        float lifetime = BaseLifeTime;

        if (item.ItemType == ItemType.Weapon)
        {
            foreach (var skill in skills)
            {
                lifetime *= skill.WeaponProjectileLifetimeMultiplier;
            }
        }

        return (int)lifetime;
    }
}