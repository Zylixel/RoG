﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RoGCore.Logging;

#if NET8_0_OR_GREATER
using System.Collections.Frozen;
#endif

using System.Reflection;

namespace RoGCore.Assets;

public static class EmbeddedData
{
    private static readonly ILogger Logger = Logging.LogFactory.LoggingInstance(nameof(EmbeddedData));

    static EmbeddedData()
    {
        ObjectFile = string.Empty;
        GroundFile = string.Empty;
        ItemFile = string.Empty;
        ProjectileFile = string.Empty;
        SkillFile = string.Empty;

#if NET8_0_OR_GREATER
        Tiles = new Dictionary<string, GroundDescription>().ToFrozenDictionary();
        IdToObjectType = new Dictionary<string, ushort>().ToFrozenDictionary();
        Objects = new Dictionary<ushort, ObjectDescription>().ToFrozenDictionary();
        BetterObjects = new Dictionary<string, ObjectDescription>().ToFrozenDictionary();
        Projectiles = new Dictionary<string, ProjectileDescription>().ToFrozenDictionary();
        IdToItemType = new Dictionary<string, ushort>().ToFrozenDictionary();
        Items = new Dictionary<ushort, JsonItem>().ToFrozenDictionary();
        Skins = new List<int>().ToFrozenSet();
#else
        Tiles = new Dictionary<string, GroundDescription>();
        IdToObjectType = new Dictionary<string, ushort>();
        Objects = new Dictionary<ushort, ObjectDescription>();
        BetterObjects = new Dictionary<string, ObjectDescription>();
        Projectiles = new Dictionary<string, ProjectileDescription>();
        IdToItemType = new Dictionary<string, ushort>();
        Items = new Dictionary<ushort, JsonItem>();
        Skins = new HashSet<int>();
#endif
    }

#if NET8_0_OR_GREATER
    public static FrozenDictionary<string, GroundDescription> Tiles { get; private set; }

    public static FrozenDictionary<string, ushort> IdToObjectType { get; private set; }

    public static FrozenDictionary<ushort, ObjectDescription> Objects { get; private set; }

    public static FrozenDictionary<string, ObjectDescription> BetterObjects { get; private set; }

    public static FrozenDictionary<string, ushort> IdToItemType { get; private set; }

    public static FrozenDictionary<ushort, JsonItem> Items { get; private set; }

    public static FrozenDictionary<string, ProjectileDescription> Projectiles { get; private set; }

    public static FrozenSet<int> Skins { get; }
#else
    public static Dictionary<string, GroundDescription> Tiles { get; private set; }

    // TODO: Remove
    public static Dictionary<string, ushort> IdToObjectType { get; private set; }

    // TODO: Remove
    public static Dictionary<ushort, ObjectDescription> Objects { get; private set; }

    public static Dictionary<string, ObjectDescription> BetterObjects { get; private set; }

    public static Dictionary<string, ushort> IdToItemType { get; private set; }

    public static Dictionary<ushort, JsonItem> Items { get; private set; }

    public static Dictionary<string, ProjectileDescription> Projectiles { get; private set; }

    public static HashSet<int> Skins { get; }
#endif

    public static string ObjectFile { get; private set; }

    public static string GroundFile { get; private set; }

    public static string ItemFile { get; private set; }

    public static string ProjectileFile { get; private set; }

    public static string SkillFile { get; private set; }

    private static string? AssemblyDirectory => Path.GetDirectoryName(Assembly.GetEntryAssembly()?.Location);

    public static async Task BuildDataAsync()
    {
        if (AssemblyDirectory is null)
        {
            throw new ApplicationException("Could not Grab Assembly Directory");
        }

        var basePath = Path.Combine(AssemblyDirectory, "assets/json");
        using var groundStream = File.OpenRead(Path.Combine(basePath, "Ground.json"));
        using var objectStream = File.OpenRead(Path.Combine(basePath, "Objects.json"));
        using var projectileStream = File.OpenRead(Path.Combine(basePath, "Projectiles.json"));
        using var itemStream = File.OpenRead(Path.Combine(basePath, "Items.json"));
        using var skillStream = File.OpenRead(Path.Combine(basePath, "Skills.json"));

        var tasks = new Task[]
        {
            LoadGroundAsync(groundStream),
            LoadObjectsAsync(objectStream),
            LoadProjectilesAsync(projectileStream),
            LoadItemsAsync(itemStream),
            LoadSkillsAsync(skillStream),
        };

        await Task.WhenAll(tasks);

        Logger.LogResults(
            $"tile{(Tiles.Count > 1 ? "s" : string.Empty)}",
            Tiles.Count);
        Logger.LogResults(
            $"object{(Objects.Count > 1 ? "s" : string.Empty)}",
            Objects.Count);
        Logger.LogResults(
            $"item{(Items.Count > 1 ? "s" : string.Empty)}",
            Items.Count);
        Logger.LogResults(
            $"projectile{(Projectiles.Count > 1 ? "s" : string.Empty)}",
            Projectiles.Count);
    }

    public static void ReleaseFiles()
    {
        ObjectFile = string.Empty;
        GroundFile = string.Empty;
        ItemFile = string.Empty;
        ProjectileFile = string.Empty;
        SkillFile = string.Empty;
    }

    private static async Task LoadGroundAsync(Stream groundStream)
    {
        using StreamReader reader = new(groundStream);
        GroundFile = await reader.ReadToEndAsync();
        AddGrounds(JsonConvert.DeserializeObject<GroundDescription[]>(GroundFile) ?? Array.Empty<GroundDescription>());
    }

    private static async Task LoadObjectsAsync(Stream stream)
    {
        using StreamReader reader = new(stream);
        ObjectFile = await reader.ReadToEndAsync();
        AddObjects(JsonConvert.DeserializeObject<ObjectDescription[]>(ObjectFile) ?? Array.Empty<ObjectDescription>());
    }

    private static async Task LoadProjectilesAsync(Stream stream)
    {
        using StreamReader reader = new(stream);
        ProjectileFile = await reader.ReadToEndAsync();
        AddProjectiles(JsonConvert.DeserializeObject<ProjectileDescription[]>(ProjectileFile) ?? Array.Empty<ProjectileDescription>());
    }

    private static async Task LoadItemsAsync(Stream stream)
    {
        using StreamReader reader = new(stream);
        ItemFile = await reader.ReadToEndAsync();
        AddItems(JsonConvert.DeserializeObject<JsonItem[]>(ItemFile) ?? Array.Empty<JsonItem>());
    }

    private static async Task LoadSkillsAsync(Stream stream)
    {
        using StreamReader reader = new(stream);
        SkillFile = await reader.ReadToEndAsync();
    }

    private static void AddGrounds(IReadOnlyList<GroundDescription> data)
    {
        var tiles = new Dictionary<string, GroundDescription>();

        for (ushort i = 0; i < data.Count; i++)
        {
            var desc = data[i];

            if (desc.Name == GroundDescription.InvalidName)
            {
                Logger.LogError($"Could not load Ground with Invalid Name: {desc}");
                continue;
            }

            desc.ObjectType = i;
            tiles[desc.Name] = desc;
        }

#if NET8_0_OR_GREATER
        Tiles = tiles.ToFrozenDictionary();
#else
        Tiles = tiles;
#endif
    }

    private static void AddObjects(IReadOnlyList<ObjectDescription> data)
    {
        var idToObjectType = new Dictionary<string, ushort>(StringComparer.OrdinalIgnoreCase);
        var betterObjects = new Dictionary<string, ObjectDescription>(StringComparer.OrdinalIgnoreCase);
        var objects = new Dictionary<ushort, ObjectDescription>();

        for (ushort i = 0; i < data.Count; i++)
        {
            var desc = data[i];

            if (desc.Name == ObjectDescription.InvalidName)
            {
                Logger.LogError($"Could not load Object with Invalid Name: {desc}");
                continue;
            }

            desc.ObjectType = i;

            idToObjectType[desc.Name] = i;
            betterObjects[desc.Name] = desc;
            objects[i] = desc;
        }

#if NET8_0_OR_GREATER
        IdToObjectType = idToObjectType.ToFrozenDictionary(StringComparer.OrdinalIgnoreCase);
        BetterObjects = betterObjects.ToFrozenDictionary(StringComparer.OrdinalIgnoreCase);
        Objects = objects.ToFrozenDictionary();
#else
        IdToObjectType = idToObjectType;
        BetterObjects = betterObjects;
        Objects = objects;
#endif
    }

    private static void AddItems(IEnumerable<JsonItem> data)
    {
        var idToItemType = new Dictionary<string, ushort>(StringComparer.OrdinalIgnoreCase);
        var items = new Dictionary<ushort, JsonItem>();

        foreach (var desc in data)
        {
            if (desc.Name == JsonItem.InvalidName)
            {
                Logger.LogError($"Could not load Item with Invalid Name: {desc}");
                continue;
            }

            idToItemType[desc.Name] = desc.ObjectType;
            items[desc.ObjectType] = desc;
        }

#if NET8_0_OR_GREATER
        IdToItemType = idToItemType.ToFrozenDictionary(StringComparer.OrdinalIgnoreCase);
        Items = items.ToFrozenDictionary();
#else
        IdToItemType = idToItemType;
        Items = items;
#endif
    }

    private static void AddProjectiles(IEnumerable<ProjectileDescription> data)
    {
        var projectiles = new Dictionary<string, ProjectileDescription>();

        foreach (var desc in data)
        {
            if (desc.Name == ProjectileDescription.InvalidName)
            {
                Logger.LogError($"Could not load Projecilte with Invalid Name: {desc}");
                continue;
            }

            projectiles[desc.Name] = desc;
        }

#if NET8_0_OR_GREATER
        Projectiles = projectiles.ToFrozenDictionary();
#else
        Projectiles = projectiles;
#endif
    }
}