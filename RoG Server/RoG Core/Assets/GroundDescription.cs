﻿using UnityEngine;

namespace RoGCore.Assets;

[Serializable]
public class GroundDescription
{
    public const string InvalidName = "Invalid Name";

    public string Name = InvalidName;

    public int[]? TextureIndex;

    public bool NoWalk;

    public int Damage;

    public float Speed = 1f;

    public float PushX;

    public float PushY;

    public bool Liquid;

    public bool RandomOrientation;

    public int Layer;

    public Sprite[] Textures = Array.Empty<Sprite>();

    public Color32[] AverageColors = Array.Empty<Color32>();

    public Color[] RandomColors = Array.Empty<Color>();

    [NonSerialized]
    private ushort objectType;

    public ushort ObjectType { get => objectType; set => objectType = value; }
}