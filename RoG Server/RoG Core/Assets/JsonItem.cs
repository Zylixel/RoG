﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using RoGCore.Models;
using UnityEngine;

namespace RoGCore.Assets;

[Serializable]
public class JsonItem
{
    public const string InvalidName = "Invalid Name";

    public ushort ObjectType;
    public string Name = InvalidName;

    [JsonProperty("RateOfFire")]
    public float BaseRateOfFire = 1;

    public string TextureFile = string.Empty;
    public int[] TextureIndex = Array.Empty<int>();

    public string Type = string.Empty;
    public int SetType;

    public int Tier = -1;

    public string Description = string.Empty;
    public string[] ExtData = Array.Empty<string>();

    public int Shots = 1;

    public float ArcGap = 11.25f;

    public bool Usable;
    public int BagType;
    public int MpCost;
    public bool Consumable;
    public bool Potion;
    public int Cooldown;
    public int MpEndCost;
    public CustomProjectile[]? Projectiles;

    [JsonProperty(ItemConverterType = typeof(StringEnumConverter))]
    public Dictionary<PlayerStats, int>? StatsBoost;

    public ActivateEffect[]? ActivateEffects;

    public int Bag;

    [NonSerialized]
    public List<Sprite>? Textures;

    [NonSerialized]
    private ItemType? itemType;

    [JsonIgnore]
    public ItemType ItemType
    {
        get
        {
            if (itemType.HasValue)
            {
                return itemType.Value;
            }

            itemType = Type switch
            {
                "Bow" or "Wand" or "Staff" or "Sword" or "Dagger" or "Katana" => ItemType.Weapon,
                "Cloak" or "Scepter" => ItemType.Ability,
                "Leather" or "Armor" or "Robe" => ItemType.Garment,
                "Ring" => ItemType.Ring,
                _ => ItemType.Other,
            };

            return itemType.Value;
        }
    }

    public static string TierName(int tier)
    {
        return tier switch
        {
            -2 => "Common",
            -3 => "Uncommon",
            _ => "T" + tier.ToString(),
        };
    }

    public int GetRateOfFire(IEnumerable<Skill> currentSkills)
    {
        var rateOfFire = BaseRateOfFire;

        if (ItemType == ItemType.Weapon)
        {
            foreach (var currentSkill in currentSkills)
            {
                rateOfFire *= currentSkill.WeaponRateOfFireMultiplier;
            }
        }

        return (int)rateOfFire;
    }
}