﻿using Newtonsoft.Json;
using UnityEngine;

namespace RoGCore.Assets;

[Serializable]
public class ObjectDescription
{
    public const string InvalidName = "Invalid Name";

    public string? Class;

    public string Name = InvalidName;

    public TextureData? Texture;

    public bool Interactable;

    public string? Group;

    public bool Enemy;

    public int[] Size = new int[] { 100 };

    public bool OccupyFull;

    public bool OccupyHalf;

    public bool BlocksSight;

    public bool Static;

    public string? LightColor;

    public int LightProb;

    public float LightIntensity;

    public bool Shadow = true;

    public bool StaticRandomOffset = true;

    public CustomProjectile[]? Projectiles;

    public bool AffectedByWind;

    public int LifeTime;

    public ushort[]? Equipment;
    public string[]? SlotTypes;

    public int MaxHitPoints;

    public short Defense;

    // Format: Base, MaxLevel
    public int[]? PlayerHitPoint;
    public int[]? PlayerMagicPoint;
    public int[]? PlayerAttack;
    public int[]? PlayerDefense;
    public int[]? PlayerSpeed;
    public int[]? PlayerVitality;
    public int[]? PlayerWisdom;
    public int[]? PlayerDexterity;
    public int[]? PlayerEvasion;

    public int ExpMultiplier = 1;

    public bool StasisImmune;

    public bool StunImmune;

    public bool ParalyzedImmune;

    public bool DazedImmune;

    public bool ProtectFromGroundDamage;

    public AudioDescription? ShootSound;

    public AudioDescription? HitSound;

    public ushort MaxProjectiles = 16;

    // Portal
    public string? DungeonName;

    public List<ActivateEffect>? ActivateEffects;

    [NonSerialized]
    public Sprite[] Textures = Array.Empty<Sprite>();
    [NonSerialized]
    public Sprite[] TopTextures = Array.Empty<Sprite>();
    [NonSerialized]
    public Color LightColorProcessed = Color.clear;
    [NonSerialized]
    public Color32[] AverageColors = Array.Empty<Color32>();
    [NonSerialized]
    public Color32[] AverageTopColors = Array.Empty<Color32>();
    [NonSerialized]
    public Color[] RandomColors = new[] { Color.clear };

    [JsonProperty("DisplayName")]
#pragma warning disable CS0649
    private readonly string? jsonDisplayName;
#pragma warning restore CS0649

    [NonSerialized]
    private ushort objectType;

    [JsonIgnore]
    public ushort ObjectType { get => objectType; set => objectType = value; }

    [JsonIgnore]
    public string? DisplayName => jsonDisplayName ?? Name;

    public override string ToString()
    {
        return JsonConvert.SerializeObject(this);
    }
}