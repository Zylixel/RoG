﻿using UnityEngine;

namespace RoGCore.Assets;

[Serializable]
public class ProjectileDescription
{
    public const string InvalidName = "Invalid Name";

    public string Name = InvalidName;

    [NonSerialized]
    public Sprite[] Textures = Array.Empty<Sprite>();
    public TextureData? Texture;

    public int RotateSpeed;
    public int Size = 100;

    public string LightColor = string.Empty;
    [NonSerialized]
    public Color LightColorProcessed;
    public float LightIntensity;

    public string ParticleEffect = string.Empty;

    public string TintColor = string.Empty;
    [NonSerialized]
    public Color TintColorProcessed;
    public float TintIntensity = 1;

    [NonSerialized]
    public Color[]? RandomColors;
}