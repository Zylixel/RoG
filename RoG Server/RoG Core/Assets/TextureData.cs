﻿namespace RoGCore.Assets;

[Serializable]
public class TextureData
{
    public const int TEXTURETYPEFIRST = 0;
    public const int TEXTURETYPEANIMATED = 1;
    public const int TEXTURETYPERANDOM = 2;
    public const int TEXTURETYPECYCLE = 3;
    public const int TEXTURETYPERANDOMCYCLE = 4;
    public const int TEXTURETYPEPINGPONG = 5;

    public int Type;
    public int CycleSpeed;
    public int Rotation;
    public string? File;
    public int[]? Index;
    public string? TopFile;
    public int[]? TopIndex;
}