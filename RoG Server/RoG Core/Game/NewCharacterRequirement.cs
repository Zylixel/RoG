﻿using RoGCore.Models;

namespace RoGCore.Game;

public static class NewCharacterRequirement
{
    public static int AverageSkillLevelRequired(int currentCharacterCount)
    {
        return currentCharacterCount * 10;
    }

    public static bool AverageSkillReached(int currentCharacterCount, int averageSkillLevel)
    {
        return AverageSkillLevelRequired(currentCharacterCount) <= averageSkillLevel;
    }

    public static bool MaxCharactersReached(int currentCharacterCount)
    {
        return Class.Count() <= currentCharacterCount;
    }
}