﻿using System.Runtime.CompilerServices;
using RoGCore.Models;
using UnityEngine;

namespace RoGCore.Game;

public static class ProjectilePosition
{
    public static Vector2 GetPosition(this IProjectile projectile, float elapsedTime)
    {
        if (projectile.CustomDesc.InitialWaitTime > 0)
        {
            elapsedTime = Mathf.Max(elapsedTime - projectile.CustomDesc.InitialWaitTime, 0);
        }

        var dist = elapsedTime * 0.001f * (projectile.Speed * 0.1f);

        var normalPos = new Vector2(projectile.Angle.Cos, projectile.Angle.Sin) * dist;

        var newPos = projectile.BeginPosition + normalPos;

        projectile.RenderedTheta = projectile.Angle.Rad;

        if (projectile.CustomDesc.Wavy)
        {
            newPos += GetWavyOffset(projectile, elapsedTime, dist, normalPos);
        }

        if (projectile.CustomDesc.Parametric)
        {
            newPos += GetParametricOffset(projectile, elapsedTime);
        }

        if (projectile.CustomDesc.Orbit != 0)
        {
            newPos += GetOrbitOffset(projectile, dist, normalPos);
        }

        if (projectile.CustomDesc.Boomerang && elapsedTime >= projectile.Lifetime * 0.5f)
        {
            newPos += GetBoomerangOffset(projectile, dist);
        }

        if (projectile.CustomDesc.Amplitude != 0)
        {
            newPos += GetAmplitudeOffset(projectile, elapsedTime);
        }

        if (projectile.CustomDesc.Acceleration != 0)
        {
            newPos += GetAccelerationOffset(projectile, elapsedTime);
        }

        return newPos;
    }

    private static Vector2 GetWavyOffset(IProjectile projectile, float elapsedMspt, float dist, Vector2 normalPos)
    {
        var period = projectile.Id % 2 == 0 ? 0 : Mathf.PI;
        projectile.RenderedTheta = projectile.Angle.Rad + (Mathf.PI * 64 * Mathf.Sin(period + (6 * Mathf.PI * (elapsedMspt * 0.001f))));
        return (new Vector2(Mathf.Cos(projectile.RenderedTheta), Mathf.Sin(projectile.RenderedTheta)) * dist) - normalPos;
    }

    private static Vector2 GetParametricOffset(IProjectile projectile, float elapsedMspt)
    {
        projectile.RenderedTheta = elapsedMspt / projectile.Lifetime * 2f * Mathf.PI;
        var a = Mathf.Sin(projectile.RenderedTheta) * (projectile.Id % 2 != 0 ? 1 : -1);
        var b = Mathf.Sin(projectile.RenderedTheta * 2) * (projectile.Id % 4 < 2 ? 1 : -1);
        return new Vector2((a * projectile.Angle.Cos) - (b * projectile.Angle.Sin), (a * projectile.Angle.Sin) + (b * projectile.Angle.Cos)) * projectile.CustomDesc.Magnitude;
    }

    private static Vector2 GetOrbitOffset(IProjectile projectile, float dist, Vector2 normalPos)
    {
        var orbitDist = Math.Abs(projectile.CustomDesc.Orbit);
        projectile.RenderedTheta = projectile.Angle.Rad + (dist / orbitDist);
        projectile.OrbitThetaChange = dist / orbitDist;
        projectile.RenderedTheta *= projectile.CustomDesc.Orbit > 0 ? 1 : -1;

        return (new Vector2(Mathf.Cos(projectile.RenderedTheta), Mathf.Sin(projectile.RenderedTheta)) * orbitDist) - normalPos;
    }

    private static Vector2 GetBoomerangOffset(IProjectile projectile, float dist)
    {
        var halfDist = projectile.Lifetime * 0.5f * 0.001f * (projectile.Speed * 0.1f);
        return new Vector2(projectile.Angle.Cos, projectile.Angle.Sin) * 2 * (halfDist - dist);
    }

    private static Vector2 GetAmplitudeOffset(IProjectile projectile, float elapsedMspt)
    {
        var period = projectile.Id % 2 == 0 ? 0 : Mathf.PI;
        var d = projectile.CustomDesc.Amplitude *
                    Mathf.Sin(period + (elapsedMspt / projectile.Lifetime * projectile.CustomDesc.Frequency * 2f * Mathf.PI));
        return new Vector2(Mathf.Cos(projectile.Angle.Rad + (Mathf.PI * 0.5f)), Mathf.Sin(projectile.Angle.Rad + (Mathf.PI * 0.5f))) * d;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private static Vector2 GetAccelerationOffset(IProjectile projectile, float elapsedMspt)
    {
        // A standard acceleration equation is displacement = (1/2)(acceleration)(time^2) + (startingVelocity * time) + startingPosition.
        // However, (velocity * time) is calculated for every projectile, so is ommited from this method.
        // startingPosition is always 0 for projectiles.
        var displacement = 0.5f * projectile.CustomDesc.Acceleration * Mathf.Pow(elapsedMspt * 0.001f, 2);

        return new Vector2(projectile.Angle.Cos, projectile.Angle.Sin) * displacement;
    }
}