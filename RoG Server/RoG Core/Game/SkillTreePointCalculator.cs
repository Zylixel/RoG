﻿using RoGCore.Models;

namespace RoGCore.Game;

public static class SkillTreePointCalculator
{
    public static int AvailablePoints(PlayerSkills playerSkills)
    {
        return playerSkills.CumulativeLevels();
    }
}