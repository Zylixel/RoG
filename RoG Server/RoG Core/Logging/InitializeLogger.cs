﻿using Microsoft.Extensions.Configuration;
using RoGCore.SettingsModels;
using Serilog;

namespace RoGCore.Logging;

public static class InitializeLogger
{
    public static void Initialize(Stream jsonStream)
    {
        var logConfiguration = new ConfigurationBuilder()
            .AddJsonStream(jsonStream)
            .Build();

        Log.Logger = new LoggerConfiguration()
            .ReadFrom.Configuration(logConfiguration)
            .CreateLogger();

        var appSettings = new AppSettings();
        logConfiguration.Bind(appSettings);
        LoadingLogger.LoggingSettings = appSettings.CustomLogging;
    }
}
