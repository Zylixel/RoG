﻿using Microsoft.Extensions.Logging;
using RoGCore.SettingsModels;

namespace RoGCore.Logging;

public static class LoadingLogger
{
    public static CustomLoggingSettings LoggingSettings { get; set; } = new CustomLoggingSettings();

    public static void LogResults(this ILogger logger, string objects, int count, bool includeLoaded = true)
    {
        if (includeLoaded)
        {
            logger.LogInformation("Loaded {0} {1}", count, objects);
        }
        else
        {
            logger.LogInformation("{0} {1}", count, objects);
        }
    }

    public static void LogStatus(this ILogger logger, string currentFilePath, int current = -1, int total = -1)
    {
        if (!LoggingSettings.LogLoadingStatus)
        {
            return;
        }

        if (current == -1)
        {
            logger.LogInformation(currentFilePath);
        }
        else if (total == -1)
        {
            logger.LogInformation("({0}) {1}", current + 1, currentFilePath);
        }
        else
        {
            logger.LogInformation("({0}/{1}) {2}", current + 1, total, currentFilePath);
        }
    }

    public static void LogResults<T>(this ILogger<T> logger, string objects, int count, bool includeLoaded = true)
    {
        if (includeLoaded)
        {
            logger.LogInformation("Loaded {0} {1}", count, objects);
        }
        else
        {
            logger.LogInformation("{0} {1}", count, objects);
        }
    }

    public static void LogStatus<T>(this ILogger<T> logger, string type, string currentFilePath, int current = -1, int total = -1)
    {
        if (!LoggingSettings.LogLoadingStatus)
        {
            return;
        }

        if (current == -1)
        {
            logger.LogInformation(currentFilePath);
        }
        else if (total == -1)
        {
            logger.LogInformation("({0}) {1}", current + 1, currentFilePath);
        }
        else
        {
            logger.LogInformation("({0}/{1}) {2}", current + 1, total, currentFilePath);
        }
    }
}