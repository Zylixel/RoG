﻿using Microsoft.Extensions.Logging;
using Serilog;

namespace RoGCore.Logging;

public static class LogFactory
{
    private static ILoggerFactory factory = new LoggerFactory().AddSerilog(Log.Logger);

    public static ILogger<T> LoggingInstance<T>()
    {
        return factory.CreateLogger<T>();
    }

    public static Microsoft.Extensions.Logging.ILogger LoggingInstance(string categoryName)
    {
        return factory.CreateLogger(categoryName);
    }

    public static void Reload()
    {
        factory = new LoggerFactory().AddSerilog(Log.Logger);
    }
}
