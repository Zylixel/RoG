﻿using UnityEngine;

namespace RoGCore.Models;

public readonly struct ARGB
{
    public static readonly ARGB White = new(0xFFFFFFFF);

    public readonly byte A;
    public readonly byte R;
    public readonly byte G;
    public readonly byte B;

    public ARGB(uint argb)
    {
        A = (byte)((argb & 0xff000000) >> 24);
        R = (byte)((argb & 0x00ff0000) >> 16);
        G = (byte)((argb & 0x0000ff00) >> 8);
        B = (byte)((argb & 0x000000ff) >> 0);
    }

    public ARGB(byte a, byte r, byte g, byte b)
    {
        A = a;
        R = r;
        G = g;
        B = b;
    }

    public ARGB(NReader rdr)
        : this(rdr.ReadByte(), rdr.ReadByte(), rdr.ReadByte(), rdr.ReadByte())
    {
    }

    public static implicit operator Color(in ARGB obj)
    {
        return new(obj.R / 255f, obj.G / 255f, obj.B / 255f, obj.A / 255f);
    }

    public void Write(NWriter wtr)
    {
        wtr.Write(A);
        wtr.Write(R);
        wtr.Write(G);
        wtr.Write(B);
    }
}