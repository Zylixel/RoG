﻿namespace RoGCore.Models;

public static class AccountType
{
    public enum Rank
    {
        Regular = 0,
        Vip = 1,
        Moderator = 2,
        Administrator = 3,
        HeadStaff = 4,
    }

    public static Rank FromInt(int rank)
    {
        return rank >= 4 ? Rank.HeadStaff : (Rank)rank;
    }
}