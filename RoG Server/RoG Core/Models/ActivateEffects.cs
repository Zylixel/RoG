﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace RoGCore.Models;

[JsonConverter(typeof(StringEnumConverter))]
public enum ActivateEffects
{
    Nothing,
    Shoot,
    StatBoostSelf,
    StatBoostAura,
    BulletNova,
    ConditionEffectAura,
    ConditionEffectSelf,
    Heal,
    HealNova,
    Magic,
    MagicNova,
    Teleport,
    Lightning,
    RemoveNegativeConditions,
    RemoveNegativeConditionsSelf,
    IncrementStat,
    DazeBlast,
    ClearConditionEffectAura,
    ClearConditionEffectSelf,
    Dye,
    BulletCreate, // Waki
    UnlockVault,
    Scepter,
    Explosion,
    Portal,
    Summon,
}