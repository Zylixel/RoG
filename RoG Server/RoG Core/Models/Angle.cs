﻿using UnityEngine;

namespace RoGCore.Models;

public class Angle
{
    public static readonly Angle Zero = new(0);
    public static readonly Angle Pi = new(Mathf.PI);

    private float rad;
    private float cos = -1f;
    private float sin = -1f;

    public Angle(float rad)
    {
        Rad = rad;
    }

    public Angle(double rad)
    {
        Rad = (float)rad;
    }

    public float Deg { get; private set; }

    public float Rad
    {
        get => rad;

        set
        {
            rad = value;
            cos = -1f;
            sin = -1f;
            Deg = value * Mathf.Rad2Deg;
        }
    }

    public float Cos
    {
        get
        {
            if (cos == -1f)
            {
                cos = Mathf.Cos(Rad);
            }

            return cos;
        }
    }

    public float Sin
    {
        get
        {
            if (sin == -1f)
            {
                sin = Mathf.Sin(Rad);
            }

            return sin;
        }
    }

    public static Angle Read(NReader rdr)
    {
        return new(rdr.ReadSingle());
    }

    public static Angle RandomAngle(System.Random rand)
    {
        return new(rand.NextDouble() * 2 * Mathf.PI);
    }

    public Vector3 ToUnity3()
    {
        return new(0, 0, Deg);
    }

    public Angle GetCopy()
    {
        return new(Rad);
    }

    public void Write(NWriter wtr)
    {
        wtr.Write(Rad);
    }

    public override string ToString()
    {
        return $"{{Rad: {Rad}}}";
    }
}