﻿using RoGCore.Assets;

namespace RoGCore.Models;

public static class Class
{
    public enum Type : byte
    {
        Hunter,
        Wizard,
        Warrior,
        Deceiver,
        Warlock,
    }

    public static int Count()
    {
#if NET5_0_OR_GREATER
        return Enum.GetValues<Type>().Length;
#else
        return Enum.GetValues(typeof(Type)).Length;
#endif
    }

    public static Type Parse(ObjectDescription desc)
    {
        return Parse(desc.Name);
    }

    public static Type Parse(string descName)
    {
#if NET5_0_OR_GREATER
        return Enum.Parse<Type>(descName);
#else
        return (Type)Enum.Parse(typeof(Type), descName);
#endif
    }
}