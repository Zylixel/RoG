﻿using System.IO.Compression;
using System.Text;

namespace RoGCore.Models;

public static class Compressor
{
    public static byte[] Compress(byte[] buffer, CompressionLevel level = CompressionLevel.Optimal)
    {
        MemoryStream ms = new();
        GZipStream zip = new(ms, level, true);
        zip.Write(buffer, 0, buffer.Length);
        zip.Dispose();
        ms.Position = 0;

        var compressed = new byte[ms.Length];
        ms.Read(compressed, 0, compressed.Length);

        var gzBuffer = new byte[compressed.Length + 4];
        Buffer.BlockCopy(compressed, 0, gzBuffer, 4, compressed.Length);
        Buffer.BlockCopy(BitConverter.GetBytes(buffer.Length), 0, gzBuffer, 0, 4);
        return gzBuffer;
    }

    public static string Compress(string input, CompressionLevel level = CompressionLevel.Optimal)
    {
        var buffer = Encoding.ASCII.GetBytes(input);

        MemoryStream ms = new();
        GZipStream zip = new(ms, level, true);
        zip.Write(buffer, 0, buffer.Length);
        zip.Dispose();
        ms.Position = 0;

        var compressed = new byte[ms.Length];
        ms.Read(compressed, 0, compressed.Length);

        var gzBuffer = new byte[compressed.Length + 4];
        Buffer.BlockCopy(compressed, 0, gzBuffer, 4, compressed.Length);
        Buffer.BlockCopy(BitConverter.GetBytes(buffer.Length), 0, gzBuffer, 0, 4);
        return Convert.ToBase64String(gzBuffer);
    }

    public static byte[] Decompress(byte[] gzBuffer, int startIndex = 0)
    {
        MemoryStream ms = new();
        var msgLength = BitConverter.ToInt32(gzBuffer, startIndex);
        ms.Write(gzBuffer, startIndex + 4, gzBuffer.Length - (startIndex + 4));

        var buffer = new byte[msgLength];

        ms.Position = 0;
        GZipStream zip = new(ms, CompressionMode.Decompress, true);
        zip.Read(buffer, 0, buffer.Length);
        zip.Dispose();

        return buffer;
    }

    public static string Decompress(string input)
    {
        var gzBuffer = Convert.FromBase64String(input);

        MemoryStream ms = new();
        var msgLength = BitConverter.ToInt32(gzBuffer, 0);
        ms.Write(gzBuffer, 4, gzBuffer.Length - 4);

        var buffer = new byte[msgLength];

        ms.Position = 0;
        GZipStream zip = new(ms, CompressionMode.Decompress, true);
        zip.Read(buffer, 0, buffer.Length);
        zip.Dispose();

        return Encoding.ASCII.GetString(buffer);
    }
}