﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace RoGCore.Models;

[Serializable]
public struct ConditionEffect
{
    [JsonConverter(typeof(StringEnumConverter))]
    public ConditionEffectIndex Effect { get; set; }

    public int DurationMs { get; set; }
}