﻿using RoGCore.Networking;

namespace RoGCore.Models;

public sealed class GameServer : IStreamableObject<GameServer>
{
    public GameServer()
    {
    }

    public GameServer(NReader rdr)
    {
        Name = rdr.ReadString();
        Dns = rdr.ReadString();
        Usage = rdr.ReadDouble();
        AdminOnly = rdr.ReadBoolean();
        Port = rdr.ReadInt32();
    }

    public GameServer(string name, string dns, double usage, bool adminOnly, int port)
    {
        Name = name;
        Dns = dns;
        Usage = usage;
        AdminOnly = adminOnly;
        Port = port;
    }

    public string Name { get; set; } = string.Empty;

    public string Dns { get; set; } = string.Empty;

    public double Usage { get; set; }

    public bool AdminOnly { get; set; }

    public int Port { get; set; }

    public GameServer Read(NReader rdr)
    {
        return new GameServer(rdr);
    }

    public void Write(NWriter wtr)
    {
        wtr.Write(Name);
        wtr.Write(Dns);
        wtr.Write(Usage);
        wtr.Write(AdminOnly);
        wtr.Write(Port);
    }
}
