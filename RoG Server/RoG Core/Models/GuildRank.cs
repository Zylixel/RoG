﻿namespace RoGCore.Models;

public enum GuildRank : byte
{
    Initiate = 0,
    Member = 1,
    Officer = 2,
    Leader = 3,
    Founder = 4,
}