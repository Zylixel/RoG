﻿using RoGCore.Networking;

namespace RoGCore.Models;

public interface IClient
{
    public void Disconnect(DisconnectReason reason);

    public void ProcessPacket(in IPacket packet);
}