﻿using RoGCore.Assets;
using UnityEngine;

namespace RoGCore.Models;

public interface IProjectile
{
    public Angle Angle { get; }

    public ushort Id { get; }

    public CustomProjectile CustomDesc { get; }

    public Vector2 BeginPosition { get; }

    public ushort Damage { get; }

    public float OrbitThetaChange { get; set; }

    public float RenderedTheta { get; set; }

    public float Speed { get; }

    public float Lifetime { get; }
}