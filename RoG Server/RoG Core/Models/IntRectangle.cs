﻿using System.Numerics;

namespace RoGCore.Models;

public struct IntRectangle : IEquatable<IntRectangle>
{
    public static readonly IntRectangle Empty;

    public IntRectangle(int x, int y, int width, int height)
    {
        X = x;
        Y = y;
        Width = width;
        Height = height;
    }

    public IntRectangle(Vector2Int location, Vector2Int size)
    {
        X = location.X;
        Y = location.Y;
        Width = size.X;
        Height = size.Y;
    }

    public IntRectangle(Vector4 vector)
    {
        X = (int)vector.X;
        Y = (int)vector.Y;
        Width = (int)vector.Z;
        Height = (int)vector.W;
    }

    public Vector2Int Location
    {
        readonly get => new(X, Y);
        set
        {
            X = value.X;
            Y = value.Y;
        }
    }

    public Vector2Int Size
    {
        readonly get => new(Width, Height);
        set
        {
            Width = value.X;
            Height = value.Y;
        }
    }

    public int X { readonly get; set; }

    public int Y { readonly get; set; }

    public int Width { readonly get; set; }

    public int Height { readonly get; set; }

    public readonly int Left => X;

    public readonly int Top => Y;

    public readonly int Right => X + Width;

    public readonly int Bottom => Y + Height;

    public readonly bool IsEmpty => (Width <= 0) || (Height <= 0);

    public static explicit operator Vector4(IntRectangle rectangle)
    {
        return rectangle.ToVector4();
    }

    public static explicit operator IntRectangle(Vector4 vector)
    {
        return new IntRectangle(vector);
    }

    public static bool operator ==(IntRectangle left, IntRectangle right)
    {
        return left.X == right.X && left.Y == right.Y && left.Width == right.Width && left.Height == right.Height;
    }

    public static bool operator !=(IntRectangle left, IntRectangle right)
    {
        return !(left == right);
    }

    public static IntRectangle Intersect(IntRectangle a, IntRectangle b)
    {
        var x1 = Math.Max(a.X, b.X);
        var x2 = Math.Min(a.X + a.Width, b.X + b.Width);
        var y1 = Math.Max(a.Y, b.Y);
        var y2 = Math.Min(a.Y + a.Height, b.Y + b.Height);

        return x2 >= x1 && y2 >= y1 ? new IntRectangle(x1, y1, x2 - x1, y2 - y1) : Empty;
    }

    public static IntRectangle Union(IntRectangle a, IntRectangle b)
    {
        var x1 = Math.Min(a.X, b.X);
        var x2 = Math.Max(a.X + a.Width, b.X + b.Width);
        var y1 = Math.Min(a.Y, b.Y);
        var y2 = Math.Max(a.Y + a.Height, b.Y + b.Height);

        return new IntRectangle(x1, y1, x2 - x1, y2 - y1);
    }

    public override readonly bool Equals(object? obj)
    {
        return obj is IntRectangle rectangle && Equals(rectangle);
    }

    public readonly bool Equals(IntRectangle other)
    {
        return this == other;
    }

    public readonly bool Contains(int x, int y)
    {
        return X <= x && x < X + Width && Y <= y && y < Y + Height;
    }

    public readonly bool Contains(Vector2Int pt)
    {
        return Contains(pt.X, pt.Y);
    }

    public readonly bool Contains(IntRectangle rect)
    {
        return (X <= rect.X) && (rect.X + rect.Width <= X + Width) && (Y <= rect.Y) && (rect.Y + rect.Height <= Y + Height);
    }

    public override int GetHashCode()
    {
        return ToVector4().GetHashCode();
    }

    public void Intersect(IntRectangle rect)
    {
        var result = Intersect(rect, this);

        X = result.X;
        Y = result.Y;
        Width = result.Width;
        Height = result.Height;
    }

    public readonly bool IntersectsWith(IntRectangle rect)
    {
        return (rect.X < X + Width) && (X < rect.X + rect.Width) && (rect.Y < Y + Height) && (Y < rect.Y + rect.Height);
    }

    public void Offset(Vector2Int pos)
    {
        Offset(pos.X, pos.Y);
    }

    public void Offset(int x, int y)
    {
        X += x;
        Y += y;
    }

    public Vector4 ToVector4()
    {
        return new Vector4(X, Y, Width, Height);
    }

    public override readonly string ToString()
    {
        return $"{{X={X},Y={Y},Width={Width},Height={Height}}}";
    }
}