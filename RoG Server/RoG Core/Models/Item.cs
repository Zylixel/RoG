﻿using RoGCore.Assets;

namespace RoGCore.Models;

public readonly struct Item
{
    public readonly JsonItem BaseItem;
    public readonly RawItem Data;

    public Item(JsonItem item, bool sb = false)
    {
        Data = new RawItem { ObjectType = item.ObjectType, Soulbound = sb };
        BaseItem = item;
    }

    public Item(JsonItem item, RawItem raw)
    {
        BaseItem = item;
        Data = raw;
    }

    public static implicit operator RawItem(in Item item)
    {
        return item.Data;
    }

    public static RawItem?[] UnCookArray(in Item?[] items)
    {
        return Array.ConvertAll(items, item => item?.Data);
    }

    public Dictionary<PlayerStats, int> GetStatBoosts()
    {
        Dictionary<PlayerStats, int> ret = new();

        if (BaseItem.StatsBoost is null)
        {
            return ret;
        }

        var itemType = BaseItem.ItemType;

        if (itemType is not ItemType.Garment and not ItemType.Ring)
        {
            return BaseItem.StatsBoost;
        }

        foreach (var statBoost in BaseItem.StatsBoost)
        {
            var statMultiplier = Data.LevelStatMultiplier();
            if (statBoost.Value > 0)
            {
                ret.Add(statBoost.Key, (int)(statBoost.Value * statMultiplier));
            }
            else if (statMultiplier != 0)
            {
                ret.Add(statBoost.Key, (int)(statBoost.Value / statMultiplier));
            }
            else
            {
                ret.Add(statBoost.Key, statBoost.Value);
            }
        }

        return ret;
    }

    public int GetCooldown(IEnumerable<Skill> currentSkills)
    {
        if (BaseItem.Cooldown == 0)
        {
            return 0;
        }

        float cooldown = BaseItem.Cooldown;

        foreach (var currentSkill in currentSkills)
        {
            cooldown += currentSkill.CooldownModifier;
        }

        foreach (var currentSkill in currentSkills)
        {
            cooldown *= currentSkill.CooldownMultiplier;
        }

        cooldown /= Data.LevelStatMultiplier(0.5f);

        return (int)Math.Max(cooldown, 0);
    }

    public int GetMpCost(IEnumerable<Skill> currentSkills)
    {
        if (BaseItem.MpCost == 0)
        {
            return 0;
        }

        float mpCost = BaseItem.MpCost;

        foreach (var currentSkill in currentSkills)
        {
            mpCost += currentSkill.ManaModifier;
        }

        foreach (var currentSkill in currentSkills)
        {
            mpCost *= currentSkill.ManaMultiplier;
        }

        mpCost /= Data.LevelStatMultiplier(0.5f);

        return (int)Math.Max(mpCost, 0);
    }
}