﻿namespace RoGCore.Models;

public readonly struct ItemEffectDat
{
    public readonly byte Amount;
    public readonly byte Strength;
    public readonly float Cooldown;

    public ItemEffectDat(byte amount, byte strength, float cooldown)
    {
        Amount = amount;
        Strength = strength;
        Cooldown = cooldown;
    }
}