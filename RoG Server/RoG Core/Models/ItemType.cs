﻿namespace RoGCore.Models;

public enum ItemType : byte
{
    Other,
    Weapon,
    Ability,
    Garment,
    Ring,
}