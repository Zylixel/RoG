namespace RoGCore.Models;

public enum MessageId : byte
{
    Failure,
    CreateSuccess,
    Create,
    PlayerShoot,
    Move,
    Text,
    Damage,
    Update,
    Notification,
    NewTick,
    UseItem,
    ShowEffect,
    Hello,
    Reconnect,
    MapInfo,
    Load,
    SetCondition,
    Teleport,
    UsePortal,
    Result,
    CharacterHit,
    EnemyHit,
    OtherHit,
    SquareHit,
    EditAccountList,
    AccountList,
    GuildCreate,
    GuildRemove,
    GuildInvite,
    GuildJoin,
    GuildRankChange,
    AllyShoot,
    EnemyShoot,
    Escape,
    Audio,
    ContainerClaim,
    InventoryMove,
    PolishItems,
    VaultRequest,
    VaultResult,
    ObjectDamage,
    Paint,
    SetSkillTree,
    UseSkill,
    SkillShoot,
    News,
    EnemySpecificShoot,
    TitleCard,
    PlayerHit,
    SetSummonPosition,
}