﻿using System.Net;
using System.Text;

namespace RoGCore.Models;

public class NWriter : BinaryWriter
{
    public new MemoryStream BaseStream;

    public NWriter(MemoryStream s)
        : base(s, Encoding.UTF8)
    {
        BaseStream = s;
    }

    public override void Write(short value)
    {
        base.Write(IPAddress.HostToNetworkOrder(value));
    }

    public override void Write(int value)
    {
        base.Write(IPAddress.HostToNetworkOrder(value));
    }

    public override void Write(long value)
    {
        base.Write(IPAddress.HostToNetworkOrder(value));
    }

    public override void Write(ushort value)
    {
        base.Write((ushort)IPAddress.HostToNetworkOrder((short)value));
    }

    public override void Write(uint value)
    {
        base.Write((uint)IPAddress.HostToNetworkOrder((int)value));
    }

    public override void Write(ulong value)
    {
        base.Write((ulong)IPAddress.HostToNetworkOrder((long)value));
    }

    public override void Write(float value)
    {
        Write(BitConverter.GetBytes(value));
    }

    public override void Write(double value)
    {
        var b = BitConverter.GetBytes(value);
        Array.Reverse(b);
        Write(b);
    }

    public void WriteNullTerminatedString(string str)
    {
        Write(Encoding.UTF8.GetBytes(str));
        Write((byte)0);
    }

    public void WriteUtf(string str)
    {
        if (str is null)
        {
            Write((short)0);
        }
        else
        {
            Write((short)str.Length);
            Write(Encoding.UTF8.GetBytes(str));
        }
    }

    public void Write32Utf(string str)
    {
        Write(str.Length);
        Write(Encoding.UTF8.GetBytes(str));
    }
}