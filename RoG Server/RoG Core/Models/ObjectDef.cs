﻿namespace RoGCore.Models;

public readonly struct ObjectDef
{
    public readonly ushort ObjectType;
    public readonly ObjectStats Stats;

    public ObjectDef(ushort objectType, in ObjectStats stats)
    {
        ObjectType = objectType;
        Stats = stats;
    }

    public ObjectDef(NReader rdr)
    {
        ObjectType = rdr.ReadUInt16();
        Stats = new ObjectStats(rdr);
    }

    public void Write(NWriter wtr)
    {
        wtr.Write(ObjectType);
        Stats.Write(wtr);
    }
}