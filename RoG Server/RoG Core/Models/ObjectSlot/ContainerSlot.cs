﻿namespace RoGCore.Models.ObjectSlot;

public readonly struct ContainerSlot : IObjectSlot
{
    public ContainerSlot(long ownerId, byte slotId)
    {
        OwnerId = ownerId;
        SlotId = slotId;
    }

    public ContainerSlot(NReader reader)
    {
        OwnerId = reader.ReadInt64();
        SlotId = reader.ReadByte();
    }

    public ObjectSlotType Type => ObjectSlotType.Container;

    public long OwnerId { get; }

    public byte SlotId { get; }

    public Guid ItemGuid => throw new InvalidOperationException();

    void IObjectSlot.WriteData(NWriter writer)
    {
        writer.Write(OwnerId);
        writer.Write(SlotId);
    }
}