﻿namespace RoGCore.Models.ObjectSlot;

public readonly struct GroundSlot : IObjectSlot
{
    public ObjectSlotType Type => ObjectSlotType.Ground;

    public long OwnerId => throw new InvalidOperationException();

    public byte SlotId => throw new InvalidOperationException();

    public Guid ItemGuid => throw new InvalidOperationException();

    void IObjectSlot.WriteData(NWriter writer)
    {
    }
}