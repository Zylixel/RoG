﻿namespace RoGCore.Models.ObjectSlot;

public interface IObjectSlot
{
    ObjectSlotType Type { get; }

    long OwnerId { get; }

    byte SlotId { get; }

    Guid ItemGuid { get; }

    internal void WriteData(NWriter writer);
}