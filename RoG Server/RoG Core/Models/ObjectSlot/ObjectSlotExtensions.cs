﻿namespace RoGCore.Models.ObjectSlot;

public static class ObjectSlotExtensions
{
    public static IObjectSlot ReadObjectSlot(this NReader reader)
    {
        return (ObjectSlotType)reader.ReadByte() switch
        {
            ObjectSlotType.Ground => default(GroundSlot),
            ObjectSlotType.Container => new ContainerSlot(reader),
            ObjectSlotType.Vault => new VaultSlot(reader),
            _ => throw new InvalidDataException("IncomingIObjectSlot Type is invalid"),
        };
    }

    public static void Write(this IObjectSlot objectSlot, NWriter writer)
    {
        writer.Write((byte)objectSlot.Type);
        objectSlot.WriteData(writer);
    }
}