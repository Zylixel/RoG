﻿namespace RoGCore.Models.ObjectSlot;

public enum ObjectSlotType : byte
{
    Ground,
    Container,
    Vault,
}