﻿namespace RoGCore.Models.ObjectSlot;

public readonly struct VaultSlot : IObjectSlot
{
    public VaultSlot(Guid itemGuid)
    {
        ItemGuid = itemGuid;
    }

    public VaultSlot(NReader reader)
    {
        ItemGuid = new Guid(reader.ReadBytes(16));
    }

    public ObjectSlotType Type => ObjectSlotType.Vault;

    public long OwnerId => throw new InvalidOperationException();

    public byte SlotId => throw new InvalidOperationException();

    public Guid ItemGuid { get; }

    void IObjectSlot.WriteData(NWriter writer)
    {
        writer.Write(ItemGuid.ToByteArray());
    }
}