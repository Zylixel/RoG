﻿using System.Numerics;
using RoGCore.Utils;

namespace RoGCore.Models;

public readonly struct ObjectStats
{
    public readonly long Id;
    public readonly Vector2 Pos;
    public readonly KeyValuePair<byte, object>[] Stats;

    public ObjectStats(long id, Vector2 pos, KeyValuePair<byte, object>[] stats)
    {
        Id = id;
        Pos = pos;
        Stats = stats;
    }

    public ObjectStats(NReader rdr)
    {
        Id = rdr.ReadInt64();
        Pos = Vector2Extensions.Read(rdr);
        Stats = new KeyValuePair<byte, object>[rdr.ReadByte()];

        for (var i = 0; i < Stats.Length; i++)
        {
            var type = rdr.ReadByte();

            switch (StatsType.ExportType(type))
            {
                case StatsType.StatsExportType.Int:
                    Stats[i] = new KeyValuePair<byte, object>(type, rdr.ReadInt32());
                    break;
                case StatsType.StatsExportType.Utf:
                    Stats[i] = new KeyValuePair<byte, object>(type, rdr.ReadUtf());
                    break;
                case StatsType.StatsExportType.ByteArray:
                    Stats[i] = new KeyValuePair<byte, object>(type, rdr.ReadBytes(rdr.ReadUInt16()));
                    break;
            }
        }
    }

    public void Write(NWriter wtr)
    {
        wtr.Write(Id);
        Pos.Write(wtr);
        wtr.Write((byte)Stats.Length);

        for (var i = 0; i < Stats.Length; i++)
        {
            var stat = Stats[i];
            wtr.Write(stat.Key);

            switch (StatsType.ExportType(stat.Key))
            {
                case StatsType.StatsExportType.Int:
                    wtr.Write((int)stat.Value);
                    break;
                case StatsType.StatsExportType.Utf:
                    wtr.WriteUtf(stat.Value.ToString() ?? string.Empty);
                    break;
                case StatsType.StatsExportType.ByteArray:
                    var arr = (byte[])stat.Value;
                    wtr.Write((ushort)arr.Length);
                    wtr.Write(arr);
                    break;
            }
        }
    }
}