﻿namespace RoGCore.Models;

public class PlayerSkills
{
    private readonly uint[] skillXP;

    public PlayerSkills()
    {
        skillXP = new uint[Enum.GetValues(typeof(Skill)).Length];
    }

    public PlayerSkills(NReader reader)
        : this()
    {
        Read(reader);
    }

    public PlayerSkills(byte[] bytes)
        : this()
    {
        Read(bytes);
    }

    // Never delete a skill or change the skill value.
    // The database will be corrupted
    public enum Skill : byte
    {
        Combat = 0,
        Mining = 1,
        Farming = 2,
        Business = 3,
        Enchanting = 4,
        Fishing = 5,
    }

    public void Write(NWriter wtr)
    {
        wtr.Write((byte)skillXP.Length);

        for (byte i = 0; i < skillXP.Length; i++)
        {
            wtr.Write(skillXP[i]);
        }
    }

    public byte[] Write()
    {
        var capacity = 1 + (4 * skillXP.Length);

        MemoryStream ret = new(capacity);
        using (NWriter wtr = new(ret))
        {
            Write(wtr);
        }

        return ret.ToArray();
    }

    public void IncreaseSkill(Skill skill, uint xpAmount)
    {
        skillXP[(byte)skill] += xpAmount;
    }

    public uint GetXP(Skill skill)
    {
        return skillXP[(byte)skill];
    }

    public int GetLevel(Skill skill)
    {
        var xp = GetXP(skill);

        // Todo make this better
        return (int)(1 + (xp / 5000));
    }

    public int AverageLevel()
    {
        uint avgXP = 0;

        for (var i = 0; i < skillXP.Length; i++)
        {
            avgXP += skillXP[i];
        }

        avgXP /= (uint)skillXP.Length;

        // Todo make this better
        return (int)(1 + (avgXP / 5000));
    }

    public int CumulativeLevels()
    {
        uint avgXP = 0;

        for (var i = 0; i < skillXP.Length; i++)
        {
            avgXP += skillXP[i];
        }

        // Todo make this better
        return (int)(1 + (avgXP / 5000));
    }

    public void Read(byte[] bytes)
    {
        NReader reader = new(new MemoryStream(bytes));

        Read(reader);
    }

    private void Read(NReader reader)
    {
        try
        {
            var skillAmount = reader.ReadByte();

            for (byte i = 0; i < skillAmount; i++)
            {
                skillXP[i] = reader.ReadUInt32();
            }
        }
        catch
        {
            // Corrputed data or legacy player
        }
    }
}