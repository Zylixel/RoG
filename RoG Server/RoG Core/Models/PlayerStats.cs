﻿namespace RoGCore.Models;

public enum PlayerStats : byte
{
    Hp,
    Mp,
    Attack,
    Defense,
    Speed,
    Vitality,
    Wisdom,
    Dexterity,
}