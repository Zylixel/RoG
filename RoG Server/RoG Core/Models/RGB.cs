﻿using UnityEngine;

namespace RoGCore.Models;

public readonly struct RGB
{
    public static readonly RGB White = new(0xFFFFFF);
    public static readonly RGB Black = new(0x000000);
    public static readonly RGB Red = new(0xFF0000);
    public static readonly RGB Green = new(0x00FF00);
    public static readonly RGB Blue = new(0x0000FF);

    public readonly byte B;
    public readonly byte G;
    public readonly byte R;

    public RGB(int rgb)
    {
        R = (byte)((rgb & 0xff0000) >> 16);
        G = (byte)((rgb & 0x00ff00) >> 8);
        B = (byte)((rgb & 0x0000ff) >> 0);
    }

    public RGB(byte r, byte g, byte b)
    {
        R = r;
        G = g;
        B = b;
    }

    public RGB(NReader rdr)
        : this(rdr.ReadByte(), rdr.ReadByte(), rdr.ReadByte())
    {
    }

    public static implicit operator Color(in RGB obj)
    {
        return new(obj.R / 255f, obj.G / 255f, obj.B / 255f);
    }

    public static implicit operator RGB(Color obj)
    {
        return new((byte)(obj.r * 255f), (byte)(obj.g * 255f), (byte)(obj.b * 255f));
    }

    public static implicit operator int(in RGB obj)
    {
        int rgb = 0;

        rgb |= obj.R << 16;
        rgb |= obj.G << 8;
        rgb |= obj.B << 0;

        return rgb;
    }

    public void Write(NWriter wtr)
    {
        wtr.Write(R);
        wtr.Write(G);
        wtr.Write(B);
    }
}