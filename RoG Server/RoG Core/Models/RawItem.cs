﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RoGCore.Assets;
using RoGCore.Logging;

namespace RoGCore.Models;

public class RawItem
{
    public const byte MAXLEVEL = 50;

    private static readonly ILogger<RawItem> Logger = LogFactory.LoggingInstance<RawItem>();

    public RawItem()
    {
        ObjectType = 0;
        Soulbound = false;
        Xp = 0;
        Level = 1;
        Name = string.Empty;
        Uuid = Guid.NewGuid();
    }

    private RawItem(ushort objectType, bool soulbound, uint xp, byte level, string name, Guid? uuid)
    {
        ObjectType = objectType;
        Soulbound = soulbound;
        Xp = xp;
        Level = level;
        Name = name;
        Uuid = uuid ?? Guid.Empty;
    }

    public ushort ObjectType { get; set; }

    public bool Soulbound { get; set; }

    public uint Xp { get; set; }

    public byte Level { get; set; }

    public string Name { get; set; }

    [JsonIgnore]
    public Guid Uuid { get; set; }

    public static Item?[] Cook(RawItem?[] val, Dictionary<ushort, JsonItem> itemDictionary)
    {
        var ret = new Item?[val.Length];

        for (var i = 0; i < val.Length; i++)
        {
            var rawItem = val[i];
            ret[i] = rawItem is null || !itemDictionary.TryGetValue(rawItem.ObjectType, out var item) ? null : new Item(item, rawItem);
        }

        return ret;
    }

    public static byte[] Write(IEnumerable<RawItem?> val, bool includeUuid, bool compress)
    {
        var rawItems = val.ToList();

        MemoryStream ret = new();
        using (BinaryWriter wtr = new(ret))
        {
            wtr.Write((byte)rawItems.Count);

            foreach (var item in rawItems)
            {
                if (item is null)
                {
                    wtr.Write((ushort)0);
                    continue;
                }

                wtr.Write(item.ObjectType);
                wtr.Write(item.Soulbound);
                wtr.Write(item.Xp);
                wtr.Write(item.Level);
                wtr.Write(item.Name);

                if (includeUuid)
                {
                    wtr.Write(item.Uuid.ToByteArray());
                }
            }
        }

        byte[] header = { Utils.Utils.ConvertBoolArrayToByte(new[] { compress, includeUuid }) };
        var data = compress ? Compressor.Compress(ret.ToArray()) : ret.ToArray();

        var retArray = new byte[header.Length + data.Length];
        Buffer.BlockCopy(header, 0, retArray, 0, header.Length);
        Buffer.BlockCopy(data, 0, retArray, header.Length, data.Length);

        return retArray;
    }

    public static RawItem?[] Read(byte[]? data)
    {
        if (data is null)
        {
            return Array.Empty<RawItem?>();
        }

        BinaryReader? reader = null;
        try
        {
            var header = Utils.Utils.ConvertByteToBoolArrayReversed(data[0]);
            var includesUuid = header[0];
            var compress = header[1];

            if (compress)
            {
                data = Compressor.Decompress(data, 1);
                reader = new BinaryReader(new MemoryStream(data, 0, data.Length));
            }
            else
            {
                reader = new BinaryReader(new MemoryStream(data, 1, data.Length - 1));
            }

            var capacity = reader.ReadByte();
            var ret = new RawItem?[capacity];

            for (var i = 0; i < capacity; i++)
            {
                var objectType = reader.ReadUInt16();

                if (objectType == 0)
                {
                    ret[i] = null;
                    continue;
                }

                ret[i] = new RawItem(
                    objectType,
                    reader.ReadBoolean(),
                    reader.ReadUInt32(),
                    reader.ReadByte(),
                    reader.ReadString(),
                    includesUuid ? new Guid(reader.ReadBytes(16)) : null);
            }

            reader.Dispose();

            return ret;
        }
        catch (Exception ex)
        {
            Logger.LogError(ex, null);
        }
        finally
        {
            reader?.Dispose();
        }

        return Array.Empty<RawItem?>();
    }

    public static List<RawItem> ToNonNullList(RawItem?[] items)
    {
        List<RawItem> ret = new(items.Length);

        for (var i = 0; i < items.Length; i++)
        {
            var item = items[i];
            if (item != null)
            {
                ret.Add(item);
            }
        }

        return ret;
    }

    public bool Equals(RawItem dat2)
    {
        return ObjectType == dat2.ObjectType &&
          Soulbound == dat2.Soulbound &&
          Xp == dat2.Xp &&
          Level == dat2.Level &&
          Name == dat2.Name &&
          Uuid == dat2.Uuid;
    }

    public bool LenientEquals(RawItem dat2)
    {
        return ObjectType != 0 && ObjectType == dat2.ObjectType && Uuid.Equals(dat2.Uuid);
    }

    public Item? Cook()
    {
        return !EmbeddedData.Items.TryGetValue(ObjectType, out var item) ? null : new Item(item, this);
    }

    public bool IncreaseXp(uint amount = 1)
    {
        Xp += amount;

        var leveledUp = false;

        while (CheckLevelUp())
        {
            leveledUp = true;
        }

        return leveledUp;
    }

    public bool CheckLevelUp()
    {
        if (Level >= MAXLEVEL)
        {
            Level = MAXLEVEL;
            Xp = GetExpGoal();
            return false;
        }

        if (Xp >= GetExpGoal())
        {
            Xp -= GetExpGoal();
            Level++;
            return true;
        }

        return false;
    }

    public uint GetExpGoal()
    {
        return 500 + (((uint)Level - 1) * 500);
    }

    public float LevelStatMultiplier(float percentPerLevel = 1f)
    {
        return 1 + ((Level - 1) * percentPerLevel * 0.01f);
    }
}