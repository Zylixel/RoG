﻿using System.Numerics;
using System.Runtime.CompilerServices;

namespace RoGCore.Models;

public readonly struct Rectangle : IEquatable<Rectangle>
{
    public static readonly Rectangle Empty;

    public readonly float X;

    public readonly float Y;

    public readonly float Width;

    public readonly float Height;

    public Rectangle(float x, float y, float width, float height)
    {
        X = x;
        Y = y;
        Width = width;
        Height = height;
    }

    public Rectangle(Vector2 location, Vector2 size)
    {
        X = location.X;
        Y = location.Y;
        Width = size.X;
        Height = size.Y;
    }

    public Rectangle(Vector4 vector)
    {
        X = vector.X;
        Y = vector.Y;
        Width = vector.Z;
        Height = vector.W;
    }

    public Vector2 Location => new(X, Y);

    public Vector2 Size => new(Width, Height);

    public readonly float Left => X;

    public readonly float Top => Y;

    public readonly float Right => X + Width;

    public readonly float Bottom => Y + Height;

    public readonly bool IsEmpty => (Width <= 0) || (Height <= 0);

    public static explicit operator Vector4(Rectangle rectangle)
    {
        return rectangle.ToVector4();
    }

    public static explicit operator Rectangle(Vector4 vector)
    {
        return new Rectangle(vector);
    }

    public static bool operator ==(Rectangle left, Rectangle right)
    {
        return left.X == right.X && left.Y == right.Y && left.Width == right.Width && left.Height == right.Height;
    }

    public static bool operator !=(Rectangle left, Rectangle right)
    {
        return !(left == right);
    }

    public static Rectangle Intersect(Rectangle a, Rectangle b)
    {
        var x1 = Math.Max(a.X, b.X);
        var x2 = Math.Min(a.X + a.Width, b.X + b.Width);
        var y1 = Math.Max(a.Y, b.Y);
        var y2 = Math.Min(a.Y + a.Height, b.Y + b.Height);

        return x2 >= x1 && y2 >= y1 ? new Rectangle(x1, y1, x2 - x1, y2 - y1) : Empty;
    }

    public static Rectangle Union(Rectangle a, Rectangle b)
    {
        var x1 = Math.Min(a.X, b.X);
        var x2 = Math.Max(a.X + a.Width, b.X + b.Width);
        var y1 = Math.Min(a.Y, b.Y);
        var y2 = Math.Max(a.Y + a.Height, b.Y + b.Height);

        return new Rectangle(x1, y1, x2 - x1, y2 - y1);
    }

    public override readonly bool Equals(object? obj)
    {
        return obj is Rectangle rectangle && Equals(rectangle);
    }

    public readonly bool Equals(Rectangle other)
    {
        return this == other;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Contains(float x, float y)
    {
        return X <= x && x < Right && Y <= y && y < Bottom;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Contains(Vector2 pt)
    {
        return Contains(pt.X, pt.Y);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly bool Contains(Rectangle rect)
    {
        return (X <= rect.X) && (rect.Right <= Right) && (Y <= rect.Y) && (rect.Bottom <= Bottom);
    }

    public override int GetHashCode()
    {
        return ToVector4().GetHashCode();
    }

    public readonly bool IntersectsWith(Rectangle rect)
    {
        return (rect.X < Right) && (X < rect.Right) && (rect.Y < Bottom) && (Y < rect.Bottom);
    }

    public Vector4 ToVector4()
    {
        return new Vector4(X, Y, Width, Height);
    }

    public override readonly string ToString()
    {
        return $"{{X={X},Y={Y},Width={Width},Height={Height}}}";
    }
}