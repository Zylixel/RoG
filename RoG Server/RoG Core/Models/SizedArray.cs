﻿namespace RoGCore.Models;

public readonly struct SizedArray<T>
{
    public readonly T[] Data;
    public readonly int Length;

    public SizedArray(in T[] data, int length)
    {
        Data = data;
        Length = length;
    }

    public T this[int i]
    {
        get => Data[i];

        set => Data[i] = value;
    }
}