﻿using System.ComponentModel;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace RoGCore.Models;

[Serializable]
public class Skill : IEquatable<Skill>
{
    [JsonConverter(typeof(StringEnumConverter))]
    public SkillType Type;

    [JsonProperty(ItemConverterType = typeof(StringEnumConverter))]
    public SkillType[]? Requirements;

    [JsonProperty(ItemConverterType = typeof(StringEnumConverter))]
    public SkillType[]? Incompatibilities;

    public int SkillPointCost;
    public string Name;
    public string Description;

    [DefaultValue(1f)]
    public float ManaMultiplier = 1;
    public int ManaModifier;

    [DefaultValue(1f)]
    public float CooldownMultiplier = 1;
    public int CooldownModifier;

    [DefaultValue(1f)]
    public float ActivateDurationMultiplier = 1;
    public int ActivateDurationModifier;

    [DefaultValue(1f)]
    public float WeaponProjectileSpeedMultiplier = 1;
    [DefaultValue(1f)]
    public float WeaponProjectileLifetimeMultiplier = 1;
    [DefaultValue(1f)]
    public float WeaponDamageMultiplier = 1;
    [DefaultValue(1f)]
    public float WeaponRateOfFireMultiplier = 1;

    [JsonProperty(ItemConverterType = typeof(StringEnumConverter))]
    public Class.Type[] UsableClasses;

    [JsonProperty(ItemConverterType = typeof(StringEnumConverter))]
    public Dictionary<PlayerStats, int>? StatsBoost;

    public List<Skill> Next;

#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
    public Skill()
    {
        Next = new List<Skill>();
    }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

    public override bool Equals(object? obj)
    {
        return Equals((Skill?)obj);
    }

    public bool Equals(Skill? other)
    {
        return other is not null && Type == other.Type;
    }

    public override int GetHashCode()
    {
        return (int)Type;
    }

    public bool Usable(string className)
    {
        for (var i = 0; i < UsableClasses.Length; i++)
        {
            if (UsableClasses[i] == Class.Parse(className))
            {
                return true;
            }
        }

        return false;
    }
}