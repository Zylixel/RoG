﻿namespace RoGCore.Models;

public enum SkillType : ushort
{
    Sneak = 0,
    Project1 = 5,
    Project2 = 6,
    Project3 = 7,
    Vanish1 = 8,
    Vanish2 = 9,
    Vanish3 = 10,
    Clone1 = 11,
    Clone2 = 12,
    Clone3 = 13,
    Dex1 = 14,
    Spd1 = 15,
    Vit1 = 16,
    Wis1 = 17,
    Def1 = 18,
    Atk1 = 19,
    Mp1 = 20,
    Hp1 = 21,
    Vit2 = 22,
    Wis2 = 23,
    WeaponSpeed1 = 24,
    WeaponLifetime1 = 25,
    WeaponRoF1 = 26,
    WeaponDamage1 = 27,
}