﻿namespace RoGCore.Models;

public readonly ref struct StaticDef
{
    public readonly ushort ObjectType;
    public readonly StaticStats Stats;
    public readonly Vector2Int Position;

    public StaticDef(NReader rdr)
    {
        ObjectType = (ushort)rdr.ReadInt16();
        Stats = new StaticStats(rdr);
        Position = new Vector2Int(rdr);
    }

    public void Write(NWriter wtr)
    {
        wtr.Write(ObjectType);
        Stats.Write(wtr);
        Position.Write(wtr);
    }
}