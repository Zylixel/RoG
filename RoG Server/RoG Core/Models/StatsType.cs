﻿namespace RoGCore.Models;

public static class StatsType
{
    public const byte MaximumHp = 0;
    public const byte Hp = 1;
    public const byte Size = 2;
    public const byte MaximumMp = 3;
    public const byte Mp = 4;
    public const byte Inventory = 8;
    public const byte Attack = 20;
    public const byte Defense = 21;
    public const byte Speed = 22;
    public const byte Vitality = 26;
    public const byte Wisdom = 27;
    public const byte Dexterity = 28;
    public const byte Effects = 29;
    public const byte Stars = 30;
    public const byte Name = 31;
    public const byte Texture1 = 32;
    public const byte Texture2 = 33;
    public const byte Silver = 35;
    public const byte PortalUsable = 37;
    public const byte AccountId = 38;
    public const byte CurrentSilver = 39;
    public const byte ObjectConnection = 41;
    public const byte HpBoost = 46;
    public const byte MpBoost = 47;
    public const byte AttackBonus = 48;
    public const byte DefenseBonus = 49;
    public const byte SpeedBonus = 50;
    public const byte VitalityBonus = 51;
    public const byte WisdomBonus = 52;
    public const byte DexterityBonus = 53;
    public const byte Glowing = 59;
    public const byte AltTextureIndex = 61;
    public const byte Guild = 62;
    public const byte GuildRank = 63;
    public const byte Skin = 80;
    public const byte Effects2 = 96;
    public const byte Admin = 98;
    public const byte AccXp = 102;
    public const byte AccLevel = 103;
    public const byte SkillLevels = 107;

    internal enum StatsExportType
    {
        Int,
        Utf,
        ByteArray,
    }

    internal static StatsExportType ExportType(byte type)
    {
        return type switch
        {
            Name or Guild => StatsExportType.Utf,
            Inventory or SkillLevels => StatsExportType.ByteArray,
            _ => StatsExportType.Int,
        };
    }
}