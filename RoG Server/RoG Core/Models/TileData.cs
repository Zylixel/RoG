﻿namespace RoGCore.Models;

public readonly struct TileData
{
    public readonly ushort Tile;
    public readonly Vector2Int Position;

    public TileData(ushort tile, in Vector2Int position)
    {
        Tile = tile;
        Position = position;
    }

    public TileData(NReader rdr)
    {
        Tile = rdr.ReadUInt16();
        Position = new Vector2Int(rdr);
    }

    public void Write(NWriter wtr)
    {
        wtr.Write(Tile);
        Position.Write(wtr);
    }
}