﻿namespace RoGCore.Models;

[Flags]
public enum TileOccupency : byte
{
    None = 0,
    NoWalk = 1,
    OccupyFull = 2,
    OccupyHalf = 4,
}