﻿using System.Numerics;

namespace RoGCore.Models;

public readonly struct Vector2Int : IEquatable<Vector2Int>
{
    public static readonly Vector2Int Zero = new(0, 0);
    public static readonly Vector2Int One = new(1, 1);

    public readonly int X;
    public readonly int Y;
    public readonly Vector2 Obj;

    public Vector2Int(int x, int y)
    {
        X = x;
        Y = y;

        Obj = new Vector2(X, Y);
    }

    public Vector2Int(NReader rdr)
    {
        X = rdr.ReadInt32();
        Y = rdr.ReadInt32();

        Obj = new Vector2(X, Y);
    }

    public static implicit operator Vector2(in Vector2Int v)
    {
        return new(v.X, v.Y);
    }

    public static bool operator ==(in Vector2Int a, in Vector2Int b)
    {
        return a.X == b.X && a.Y == b.Y;
    }

    public static bool operator !=(in Vector2Int a, in Vector2Int b)
    {
        return a.X != b.X || a.Y != b.Y;
    }

    public static Vector2Int operator +(in Vector2Int a, in Vector2Int b)
    {
        return new(a.X + b.X, a.Y + b.Y);
    }

    public static Vector2Int operator -(in Vector2Int a, in Vector2Int b)
    {
        return new(a.X - b.X, a.Y - b.Y);
    }

    public static Vector2Int operator *(in Vector2Int a, in Vector2Int b)
    {
        return new(a.X * b.X, a.Y * b.Y);
    }

    public static Vector2Int operator *(in Vector2Int a, int b)
    {
        return new(a.X * b, a.Y * b);
    }

    public void Write(NWriter wtr)
    {
        wtr.Write(X);
        wtr.Write(Y);
    }

    public bool Equals(Vector2Int other)
    {
        return this == other;
    }

    public override bool Equals(object? obj)
    {
        return obj is not null && obj.GetType() == GetType() && Equals((Vector2Int)obj);
    }

    public override int GetHashCode()
    {
        return CombineHashCodes(X, Y);
    }

    public override string ToString()
    {
        return $"[{X}, {Y}]";
    }

    // Copyright (c) Microsoft. All rights reserved.
    // System.Numerics.HashCodeHelper
    internal static int CombineHashCodes(int h1, int h2)
    {
        return ((h1 << 5) + h1) ^ h2;
    }
}