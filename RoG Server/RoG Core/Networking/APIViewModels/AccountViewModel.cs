﻿namespace RoGCore.Networking.APIViewModels;

public sealed class AccountViewModel
{
    public int AccountId { get; set; }

    public string Name { get; set; } = string.Empty;

    public int Silver { get; set; }

    public byte[] SkillLevels { get; set; } = new byte[0];

    public GuildViewModel? Guild { get; set; }

    public int NextCharacterId { get; set; }

    public IList<CharacterViewModel> Characters { get; set; } = new CharacterViewModel[0];
}
