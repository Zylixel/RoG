﻿using RoGCore.Models;

namespace RoGCore.Networking.APIViewModels;

public sealed class CharacterViewModel
{
    public ushort CharacterId { get; set; }

    public string Class { get; set; } = string.Empty;

    public RawItem[] Items { get; set; } = new RawItem[0];

    public int MaxHitPoints { get; set; }

    public int MaxMagicPoints { get; set; }

    public int Attack { get; set; }

    public int Defense { get; set; }

    public int Speed { get; set; }

    public int Dexterity { get; set; }

    public int HpRegen { get; set; }

    public int MpRegen { get; set; }

    public int Tex1 { get; set; }

    public int Tex2 { get; set; }

    public bool Dead { get; set; }

    public int Texture { get; set; }
}
