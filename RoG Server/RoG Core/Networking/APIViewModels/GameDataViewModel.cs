﻿namespace RoGCore.Networking.APIViewModels;

public sealed class GameDataViewModel
{
    public string Objects { get; set; } = string.Empty;

    public string Ground { get; set; } = string.Empty;

    public string Items { get; set; } = string.Empty;

    public string Projectiles { get; set; } = string.Empty;

    public string Skills { get; set; } = string.Empty;
}
