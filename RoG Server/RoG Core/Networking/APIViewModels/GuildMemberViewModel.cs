﻿using RoGCore.Models;

namespace RoGCore.Networking.APIViewModels;

public sealed class GuildMemberViewModel
{
    public string Name { get; set; } = string.Empty;

    public GuildRank Rank { get; set; } = GuildRank.Member;
}
