﻿namespace RoGCore.Networking.APIViewModels;

public sealed class GuildViewModel
{
    public string HallType { get; set; } = string.Empty;

    public string Name { get; set; } = string.Empty;

    public int Id { get; set; }

    public IList<GuildMemberViewModel> Members { get; set; } = new GuildMemberViewModel[0];
}
