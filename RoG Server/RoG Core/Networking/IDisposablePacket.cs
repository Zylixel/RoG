﻿namespace RoGCore.Networking;

public interface IDisposablePacket
{
    public void Dispose();
}