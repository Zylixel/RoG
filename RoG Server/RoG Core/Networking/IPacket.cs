﻿using RoGCore.Models;

namespace RoGCore.Networking;

public interface IPacket : IStreamableObject<IPacket>
{
    public MessageId Id { get; }
}