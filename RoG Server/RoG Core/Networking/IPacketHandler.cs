﻿using RoGCore.Models;

namespace RoGCore.Networking;

public interface IPacketHandler
{
    MessageId Id { get; }

    bool RunOutsideOfLogicThread { get; }

    void Handle(IClient client, in IPacket packet);
}