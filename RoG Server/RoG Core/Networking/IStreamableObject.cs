﻿using RoGCore.Models;

namespace RoGCore.Networking;

public interface IStreamableObject<T>
{
    public T Read(NReader rdr);

    public void Write(NWriter wtr);
}