﻿using System.Collections.Concurrent;
using Microsoft.Extensions.Logging;
using RoGCore.Logging;
using RoGCore.Models;

namespace RoGCore.Networking;

public sealed class IncomingNetworkHandler
{
    public static readonly ConcurrentQueue<Action> PacketWorkQueue = new();

    private static readonly ConcurrentQueue<KeyValuePair<IClient, IPacket>> IncomingPackets = new();
    private static readonly ILogger<IncomingNetworkHandler> Logger = LogFactory.LoggingInstance<IncomingNetworkHandler>();

    private readonly IClient parent;

    public IncomingNetworkHandler(IClient parent)
    {
        this.parent = parent;
    }

    // Should be called in logic thread.
    public static void ProcessMessages()
    {
        while (IncomingPackets.TryDequeue(out var packet))
        {
            packet.Key.ProcessPacket(packet.Value);
        }

        while (PacketWorkQueue.TryDequeue(out var work))
        {
            work?.Invoke();
        }
    }

    public void IncomingMessage(byte[] bytes)
    {
        var packetId = bytes[Packet.HEADERSIZE - 1];

        var packet = Packet.Packets[packetId];
        if (packet is null)
        {
            Logger.LogError($"Message/Packet ID not found: {packetId}");
            return;
        }

        var stream = new MemoryStream(bytes, Packet.HEADERSIZE, bytes.Length - Packet.HEADERSIZE);
        var reader = new NReader(stream);

        packet = packet.Read(reader);
        IncomingPacketRecieved(packet);
    }

    private void IncomingPacketRecieved(in IPacket pkt)
    {
        if ((int)pkt.Id < PacketHandlers.Handlers.Length)
        {
            var handler = PacketHandlers.Handlers[(int)pkt.Id];
            if (handler?.RunOutsideOfLogicThread == true)
            {
                parent.ProcessPacket(pkt);
                return;
            }
        }

        IncomingPackets.Enqueue(new KeyValuePair<IClient, IPacket>(parent, pkt));
    }
}