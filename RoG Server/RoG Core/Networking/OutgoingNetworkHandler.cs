﻿using System.Net;
using System.Net.Sockets;
using RoGCore.Models;

namespace RoGCore.Networking;

public sealed class OutgoingNetworkHandler
{
    public const int BUFFERSIZE = 100000;

    private readonly UdpClient udpClient;
    private readonly IPEndPoint remoteEndpoint;
    private readonly byte[] buffer;
    private readonly MemoryStream stream;
    private readonly NWriter writer;

    public OutgoingNetworkHandler(UdpClient udpClient, IPEndPoint remoteEndpoint)
    {
        this.udpClient = udpClient;
        this.remoteEndpoint = remoteEndpoint;
        buffer = new byte[BUFFERSIZE];
        stream = new MemoryStream(buffer);
        writer = new NWriter(stream);
    }

    public void OutgoingMessage<T>(in T packet)
        where T : IPacket
    {
        stream.Position = 0;
        var len = packet.WriteWithHeader(writer);
        udpClient.Send(buffer, len, remoteEndpoint);
    }
}