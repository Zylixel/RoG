﻿using System.Reflection;
using Microsoft.Extensions.Logging;
using RoGCore.Logging;
using RoGCore.Models;

namespace RoGCore.Networking;

public static class Packet
{
    internal const int HEADERSIZE = 1;

    private static readonly ILogger Logger = LogFactory.LoggingInstance(nameof(Packet));

    public static IPacket?[] Packets { get; private set; } = Array.Empty<IPacket?>();

    public static void InitPackets()
    {
        var current = 0;
        var totalPackets = 0;
        var validPackets = Assembly.GetCallingAssembly().GetTypes().Concat(Assembly.GetExecutingAssembly().GetTypes()).Where(x => typeof(IPacket).IsAssignableFrom(x) && !x.IsAbstract).ToArray();

        Packets = new IPacket?[Enum.GetValues(typeof(MessageId)).Length];

        foreach (var i in validPackets)
        {
            Logger.LogStatus(i.Name, current++, validPackets.Length);

            try
            {
                var pkt = (IPacket?)Activator.CreateInstance(i);

                if (pkt is null)
                {
                    Logger.LogError($"ERROR loading {i.Name} : packet is null");
                    continue;
                }

                totalPackets++;
                Packets[(int)pkt.Id] = pkt;
            }
            catch (Exception e)
            {
                Logger.LogError($"ERROR loading {i.Name} : {e.Message}");
            }
        }

        Logger.LogResults("Packets", totalPackets);
    }

    public static int WriteWithHeader(this IPacket packet, NWriter fullWriter)
    {
        if (!fullWriter.BaseStream.CanWrite)
        {
            return 0;
        }

        fullWriter.BaseStream.SetLength(0);
        fullWriter.BaseStream.Position = HEADERSIZE;
        packet.Write(fullWriter);

        var messageLength = (int)fullWriter.BaseStream.Length;
        fullWriter.BaseStream.Position = 0;
        fullWriter.Write((byte)packet.Id);

        if (packet is IDisposablePacket disposable)
        {
            disposable.Dispose();
        }

        return messageLength;
    }
}