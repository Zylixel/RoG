﻿using System.Reflection;
using Microsoft.Extensions.Logging;
using RoGCore.Logging;
using RoGCore.Models;

namespace RoGCore.Networking;

public static class PacketHandlers
{
    private static readonly ILogger Logger = LogFactory.LoggingInstance(nameof(PacketHandlers));

    static PacketHandlers()
    {
        Handlers = Array.Empty<IPacketHandler>();
    }

    public static IPacketHandler?[] Handlers { get; private set; }

    public static void InitalizeHandlers()
    {
        var current = 0;
        var totalHandlers = 0;
        var validPackets = Assembly.GetCallingAssembly().GetTypes().Where(x => typeof(IPacketHandler).IsAssignableFrom(x) && !x.IsAbstract && !x.IsInterface).ToArray();
        Handlers = new IPacketHandler[Enum.GetValues(typeof(MessageId)).Length];

        foreach (var i in validPackets)
        {
            Logger.LogStatus(i.Name, current++, validPackets.Length);

            var pkt = (IPacketHandler?)Activator.CreateInstance(i);
            if (pkt is null)
            {
                Logger.LogError($"ERROR loading {i.Name} : packet is null");
                continue;
            }

            Handlers[(int)pkt.Id] = pkt;
            totalHandlers++;
        }

        Logger.LogResults("Handlers", totalHandlers);
    }
}