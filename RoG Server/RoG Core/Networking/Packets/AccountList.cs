﻿using RoGCore.Models;

namespace RoGCore.Networking.Packets;

public readonly struct AccountList : IPacket
{
    public readonly int[] LockedIds;
    public readonly int[] IgnoredIds;

    public AccountList(int[] lockedIds, int[] ignoredIds)
    {
        LockedIds = lockedIds;
        IgnoredIds = ignoredIds;
    }

    public MessageId Id => MessageId.AccountList;

    public IPacket Read(NReader rdr)
    {
        var lockedIds = new int[rdr.ReadUInt16()];
        for (var i = 0; i < lockedIds.Length; i++)
        {
            lockedIds[i] = rdr.ReadInt32();
        }

        var ignoredIds = new int[rdr.ReadUInt16()];
        for (var i = 0; i < ignoredIds.Length; i++)
        {
            ignoredIds[i] = rdr.ReadInt32();
        }

        return new AccountList(lockedIds, ignoredIds);
    }

    public void Write(NWriter wtr)
    {
        wtr.Write((ushort)LockedIds.Length);
        foreach (var i in LockedIds)
        {
            wtr.Write(i);
        }

        wtr.Write((ushort)IgnoredIds.Length);
        foreach (var i in IgnoredIds)
        {
            wtr.Write(i);
        }
    }
}