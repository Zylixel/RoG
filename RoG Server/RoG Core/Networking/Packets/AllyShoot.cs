﻿using System.Numerics;
using RoGCore.Models;
using RoGCore.Utils;

namespace RoGCore.Networking.Packets;

public readonly struct AllyShoot : IPacket
{
    public readonly ushort BulletId;
    public readonly long OwnerId;
    public readonly byte InventorySlot;
    public readonly Angle Angle;
    public readonly long Time;
    public readonly Vector2 StartingPos;

    public AllyShoot(ushort bulletId, long ownerId, byte inventorySlot, Angle angle, long time, Vector2 startingPos)
    {
        BulletId = bulletId;
        OwnerId = ownerId;
        InventorySlot = inventorySlot;
        Angle = angle;
        Time = time;
        StartingPos = startingPos;
    }

    public AllyShoot(long ownerId, byte inventorySlot, long time, IProjectile projectile)
    {
        BulletId = projectile.Id;
        OwnerId = ownerId;
        InventorySlot = inventorySlot;
        Angle = projectile.Angle;
        Time = time;
        StartingPos = projectile.BeginPosition.Convert();
    }

    public MessageId Id => MessageId.AllyShoot;

    public IPacket Read(NReader rdr)
    {
        return new AllyShoot(rdr.ReadUInt16(), rdr.ReadInt64(), rdr.ReadByte(), Angle.Read(rdr), rdr.ReadInt64(), Vector2Extensions.Read(rdr));
    }

    public void Write(NWriter wtr)
    {
        wtr.Write(BulletId);
        wtr.Write(OwnerId);
        wtr.Write(InventorySlot);
        Angle.Write(wtr);
        wtr.Write(Time);
        StartingPos.Write(wtr);
    }
}