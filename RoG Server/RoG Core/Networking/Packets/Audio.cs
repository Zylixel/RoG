﻿using RoGCore.Models;

namespace RoGCore.Networking.Packets;

public readonly struct Audio : IPacket
{
    public readonly long OriginId;
    public readonly Type AudioType;
    public readonly string Name;

    public Audio(long originId, Type audioType, string name)
    {
        OriginId = originId;
        AudioType = audioType;
        Name = name;
    }

    public Audio(Type audioType, string name)
    {
        OriginId = -1;
        AudioType = audioType;
        Name = name;
    }

    public enum Type : byte
    {
        Sound,
        Music,
    }

    public MessageId Id => MessageId.Audio;

    public IPacket Read(NReader rdr)
    {
        return new Audio(rdr.ReadInt64(), (Type)rdr.ReadByte(), rdr.ReadUtf());
    }

    public void Write(NWriter wtr)
    {
        wtr.Write(OriginId);
        wtr.Write((byte)AudioType);
        wtr.WriteUtf(Name);
    }
}