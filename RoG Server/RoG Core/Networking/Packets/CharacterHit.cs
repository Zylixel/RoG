﻿using RoGCore.Models;

namespace RoGCore.Networking.Packets;

public readonly struct CharacterHit : IPacket
{
    public readonly long CharacterId;

    public readonly ushort BulletId;

    public readonly long ObjectId;

    public CharacterHit(long characterId, ushort bulletId, long objectId)
    {
        CharacterId = characterId;
        BulletId = bulletId;
        ObjectId = objectId;
    }

    public MessageId Id => MessageId.CharacterHit;

    public IPacket Read(NReader rdr)
    {
        return new CharacterHit(rdr.ReadInt64(), rdr.ReadUInt16(), rdr.ReadInt64());
    }

    public void Write(NWriter wtr)
    {
        wtr.Write(CharacterId);
        wtr.Write(BulletId);
        wtr.Write(ObjectId);
    }
}