﻿using RoGCore.Models;

namespace RoGCore.Networking.Packets;

public readonly struct ContainerClaim : IPacket
{
    public readonly long ContainerId;
    public readonly short SlotId;

    public ContainerClaim(long containerId, short slotId)
    {
        ContainerId = containerId;
        SlotId = slotId;
    }

    public MessageId Id => MessageId.ContainerClaim;

    public IPacket Read(NReader rdr)
    {
        return new ContainerClaim(rdr.ReadInt64(), rdr.ReadInt16());
    }

    public void Write(NWriter wtr)
    {
        wtr.Write(ContainerId);
        wtr.Write(SlotId);
    }
}