﻿using RoGCore.Models;

namespace RoGCore.Networking.Packets;

public readonly struct Create : IPacket
{
    public readonly ushort ClassType;

    public Create(ushort classType)
    {
        ClassType = classType;
    }

    public MessageId Id => MessageId.Create;

    public IPacket Read(NReader rdr)
    {
        return new Create(rdr.ReadUInt16());
    }

    public void Write(NWriter wtr)
    {
        wtr.Write(ClassType);
    }
}