﻿using RoGCore.Models;

namespace RoGCore.Networking.Packets;

public readonly struct CreateSuccess : IPacket
{
    public readonly long ObjectId;
    public readonly ushort CharacterId;

    public CreateSuccess(long objectId, ushort characterId)
    {
        ObjectId = objectId;
        CharacterId = characterId;
    }

    public MessageId Id => MessageId.CreateSuccess;

    public IPacket Read(NReader rdr)
    {
        return new CreateSuccess(rdr.ReadInt64(), rdr.ReadUInt16());
    }

    public void Write(NWriter wtr)
    {
        wtr.Write(ObjectId);
        wtr.Write(CharacterId);
    }
}