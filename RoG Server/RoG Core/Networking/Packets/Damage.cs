﻿using RoGCore.Models;

namespace RoGCore.Networking.Packets;

public readonly struct Damage : IPacket
{
    public readonly long TargetId;
    public readonly ConditionEffects Effects;
    public readonly ushort DamageAmount;
    public readonly bool Killed;
    public readonly ushort BulletId;
    public readonly long ObjectId;
    public readonly bool Pierce;
    public readonly bool Critical;

    public Damage(long targetId, ConditionEffects effects, ushort damageAmount, bool killed, ushort bulletId, long objectId, bool pierce)
    {
        TargetId = targetId;
        Effects = effects;
        DamageAmount = damageAmount;
        Killed = killed;
        BulletId = bulletId;
        ObjectId = objectId;
        Pierce = pierce;

        // TODO: Remove
        Critical = false;
    }

    public Damage(NReader rdr)
    {
        TargetId = rdr.ReadInt64();
        var c = rdr.ReadByte();
        Effects = 0;
        for (var i = 0; i < c; i++)
        {
            Effects |= (ConditionEffects)(1 << rdr.ReadByte());
        }

        DamageAmount = rdr.ReadUInt16();
        Killed = rdr.ReadBoolean();
        BulletId = rdr.ReadUInt16();
        ObjectId = rdr.ReadInt64();
        Pierce = rdr.ReadBoolean();
        Critical = rdr.ReadBoolean();
    }

    public MessageId Id => MessageId.Damage;

    public IPacket Read(NReader rdr)
    {
        return new Damage(rdr);
    }

    public void Write(NWriter wtr)
    {
        wtr.Write(TargetId);
        List<byte> eff = new();
        for (byte i = 1; i < 255; i++)
        {
            if ((Effects & (ConditionEffects)(1 << i)) != 0)
            {
                eff.Add(i);
            }
        }

        wtr.Write((byte)eff.Count);
        foreach (var i in eff)
        {
            wtr.Write(i);
        }

        wtr.Write(DamageAmount);
        wtr.Write(Killed);
        wtr.Write(BulletId);
        wtr.Write(ObjectId);
        wtr.Write(Pierce);
        wtr.Write(Critical);
    }
}