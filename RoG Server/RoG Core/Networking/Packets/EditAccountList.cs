﻿using RoGCore.Models;

namespace RoGCore.Networking.Packets;

public readonly struct EditAccountList : IPacket
{
    public readonly ListType List;
    public readonly int AccountId;

    public EditAccountList(ListType list, int accountId)
    {
        List = list;
        AccountId = accountId;
    }

    public enum ListType : byte
    {
        Locked,
        Ignored,
    }

    public MessageId Id => MessageId.EditAccountList;

    public IPacket Read(NReader rdr)
    {
        return new EditAccountList((ListType)rdr.ReadByte(), rdr.ReadInt32());
    }

    public void Write(NWriter wtr)
    {
        wtr.Write((byte)List);
        wtr.Write(AccountId);
    }
}