﻿using RoGCore.Models;

namespace RoGCore.Networking.Packets;

public readonly struct EnemyShoot : IPacket
{
    public readonly long OwnerId;
    public readonly ushort BulletId;
    public readonly int BulletIndex;
    public readonly Angle Angle;
    public readonly ushort Damage;
    public readonly byte NumShots;
    public readonly float AngleInc;
    public readonly bool Offset;
    public readonly long Time;

    public EnemyShoot(long ownerId, ushort bulletId, int bulletIndex, Angle angle, ushort damage, byte numShots, float angleInc, bool offset, long time)
    {
        OwnerId = ownerId;
        BulletId = bulletId;
        BulletIndex = bulletIndex;
        Angle = angle;
        Damage = damage;
        NumShots = numShots;
        AngleInc = angleInc;
        Offset = offset;
        Time = time;
    }

    public EnemyShoot(long ownerId, int bulletIndex, IProjectile projectile, byte numShots, float angleInc, bool offset, long time)
    {
        OwnerId = ownerId;
        BulletId = projectile.Id;
        BulletIndex = bulletIndex;
        Angle = projectile.Angle;
        Damage = projectile.Damage;
        NumShots = numShots;
        AngleInc = angleInc;
        Offset = offset;
        Time = time;
    }

    public MessageId Id => MessageId.EnemyShoot;

    public IPacket Read(NReader rdr)
    {
        return new EnemyShoot(
rdr.ReadInt64(),
rdr.ReadUInt16(),
rdr.ReadInt32(),
Angle.Read(rdr),
rdr.ReadUInt16(),
rdr.ReadByte(),
rdr.ReadSingle(),
rdr.ReadBoolean(),
rdr.ReadInt64());
    }

    public void Write(NWriter wtr)
    {
        wtr.Write(OwnerId);
        wtr.Write(BulletId);
        wtr.Write(BulletIndex);
        Angle.Write(wtr);
        wtr.Write(Damage);
        wtr.Write(NumShots);
        wtr.Write(AngleInc);
        wtr.Write(Offset);
        wtr.Write(Time);
    }
}