﻿using System.Numerics;
using RoGCore.Models;
using RoGCore.Utils;

namespace RoGCore.Networking.Packets;

public readonly struct EnemySpecificShoot : IPacket
{
    public readonly long OwnerId;
    public readonly ushort BulletId;
    public readonly int BulletIndex;
    public readonly Angle Angle;
    public readonly ushort Damage;
    public readonly Vector2 StartPosition;
    public readonly long Time;

    public EnemySpecificShoot(
        long ownerId,
        ushort bulletId,
        int bulletIndex,
        Angle angle,
        ushort damage,
        Vector2 startPosition,
        long time)
    {
        OwnerId = ownerId;
        BulletId = bulletId;
        BulletIndex = bulletIndex;
        Angle = angle;
        Damage = damage;
        StartPosition = startPosition;
        Time = time;
    }

    public EnemySpecificShoot(long ownerId, int bulletIndex, IProjectile projectile, long time)
    {
        OwnerId = ownerId;
        BulletId = projectile.Id;
        BulletIndex = bulletIndex;
        Angle = projectile.Angle;
        Damage = projectile.Damage;
        StartPosition = projectile.BeginPosition.Convert();
        Time = time;
    }

    public MessageId Id => MessageId.EnemySpecificShoot;

    public IPacket Read(NReader rdr)
    {
        return new EnemySpecificShoot(
            rdr.ReadInt64(),
            rdr.ReadUInt16(),
            rdr.ReadInt32(),
            Angle.Read(rdr),
            rdr.ReadUInt16(),
            Vector2Extensions.Read(rdr),
            rdr.ReadInt64());
    }

    public void Write(NWriter wtr)
    {
        wtr.Write(OwnerId);
        wtr.Write(BulletId);
        wtr.Write(BulletIndex);
        Angle.Write(wtr);
        wtr.Write(Damage);
        StartPosition.Write(wtr);
        wtr.Write(Time);
    }
}