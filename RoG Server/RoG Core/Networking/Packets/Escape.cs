﻿using RoGCore.Models;

namespace RoGCore.Networking.Packets;

public readonly struct Escape : IPacket
{
    public MessageId Id => MessageId.Escape;

    public IPacket Read(NReader rdr)
    {
        rdr.ReadByte();

        return default(Escape);
    }

    public void Write(NWriter wtr)
    {
        wtr.Write((byte)1);
    }
}