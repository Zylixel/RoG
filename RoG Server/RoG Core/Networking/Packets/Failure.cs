﻿using RoGCore.Models;

namespace RoGCore.Networking.Packets;

public readonly struct Failure : IPacket
{
    public readonly string Text;
    public readonly string Title = string.Empty;

    public Failure(string errorText)
    {
        Text = errorText;
    }

    public Failure(string errorText, string title)
    {
        Text = errorText;
        Title = title;
    }

    public MessageId Id => MessageId.Failure;

    public IPacket Read(NReader rdr)
    {
        return new Failure(rdr.ReadUtf(), rdr.ReadUtf());
    }

    public void Write(NWriter wtr)
    {
        wtr.WriteUtf(Text);
        wtr.WriteUtf(Title);
    }
}