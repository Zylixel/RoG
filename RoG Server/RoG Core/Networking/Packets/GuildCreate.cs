﻿using RoGCore.Models;

namespace RoGCore.Networking.Packets;

public readonly struct GuildCreate : IPacket
{
    public readonly string Name;

    public GuildCreate(string name)
    {
        Name = name;
    }

    public MessageId Id => MessageId.GuildCreate;

    public IPacket Read(NReader rdr)
    {
        return new GuildCreate(rdr.ReadUtf());
    }

    public void Write(NWriter wtr)
    {
        wtr.WriteUtf(Name);
    }
}