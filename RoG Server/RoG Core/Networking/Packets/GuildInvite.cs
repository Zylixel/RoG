﻿using RoGCore.Models;

namespace RoGCore.Networking.Packets;

public readonly struct GuildInvite : IPacket
{
    public readonly string PlayerName;

    // GuildName isn't consumed on the server.
    public readonly string GuildName;

    public GuildInvite(string playerName, string guildName)
    {
        PlayerName = playerName;
        GuildName = guildName;
    }

    public MessageId Id => MessageId.GuildInvite;

    public IPacket Read(NReader rdr)
    {
        return new GuildInvite(rdr.ReadUtf(), rdr.ReadUtf());
    }

    public void Write(NWriter wtr)
    {
        wtr.WriteUtf(PlayerName);
        wtr.WriteUtf(GuildName);
    }
}