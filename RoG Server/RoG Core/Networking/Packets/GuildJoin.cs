﻿using RoGCore.Models;

namespace RoGCore.Networking.Packets;

public readonly struct GuildJoin : IPacket
{
    public readonly string Name;

    public GuildJoin(string name)
    {
        Name = name;
    }

    public MessageId Id => MessageId.GuildJoin;

    public IPacket Read(NReader rdr)
    {
        return new GuildJoin(rdr.ReadUtf());
    }

    public void Write(NWriter wtr)
    {
        wtr.WriteUtf(Name);
    }
}