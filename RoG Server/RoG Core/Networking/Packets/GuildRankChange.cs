﻿using RoGCore.Models;

namespace RoGCore.Networking.Packets;

public readonly struct GuildRankChange : IPacket
{
    public readonly string Name;
    public readonly GuildRank GuildRank;

    public GuildRankChange(string name, GuildRank guildRank)
    {
        Name = name;
        GuildRank = guildRank;
    }

    public MessageId Id => MessageId.GuildRankChange;

    public IPacket Read(NReader rdr)
    {
        return new GuildRankChange(rdr.ReadUtf(), (GuildRank)rdr.ReadByte());
    }

    public void Write(NWriter wtr)
    {
        wtr.WriteUtf(Name);
        wtr.Write((byte)GuildRank);
    }
}