﻿using RoGCore.Models;

namespace RoGCore.Networking.Packets;

public readonly struct GuildRemove : IPacket
{
    public readonly string Name;

    public GuildRemove(string name)
    {
        Name = name;
    }

    public MessageId Id => MessageId.GuildRemove;

    public IPacket Read(NReader rdr)
    {
        return new GuildRemove(rdr.ReadUtf());
    }

    public void Write(NWriter wtr)
    {
        wtr.WriteUtf(Name);
    }
}