﻿using RoGCore.Models;

namespace RoGCore.Networking.Packets;

public readonly struct Hello : IPacket
{
    public readonly string BuildVersion;
    public readonly short WorldId;
    public readonly string Username;
    public readonly string Password;

    public Hello(string buildVersion, short worldId, string username, string password)
    {
        BuildVersion = buildVersion;
        WorldId = worldId;
        Username = username;
        Password = password;
    }

    public MessageId Id => MessageId.Hello;

    public IPacket Read(NReader rdr)
    {
        string buildVersion = rdr.ReadUtf();
        short worldId = rdr.ReadInt16();
        string username = rdr.ReadUtf();
        string password = rdr.ReadUtf();

        return new Hello(buildVersion, worldId, username, password);
    }

    public void Write(NWriter wtr)
    {
        wtr.WriteUtf(BuildVersion);
        wtr.Write(WorldId);
        wtr.WriteUtf(Username);
        wtr.WriteUtf(Password);
    }
}