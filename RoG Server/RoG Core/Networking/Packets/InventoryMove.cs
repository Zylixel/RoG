﻿using RoGCore.Models;
using RoGCore.Models.ObjectSlot;

namespace RoGCore.Networking.Packets;

public readonly struct InventoryMove : IPacket
{
    public readonly IObjectSlot Origin;
    public readonly IObjectSlot Destination;

    public InventoryMove(in IObjectSlot origin, in IObjectSlot destination)
    {
        Origin = origin;
        Destination = destination;
    }

    public MessageId Id => MessageId.InventoryMove;

    public IPacket Read(NReader rdr)
    {
        return new InventoryMove(rdr.ReadObjectSlot(), rdr.ReadObjectSlot());
    }

    public void Write(NWriter wtr)
    {
        Origin.Write(wtr);
        Destination.Write(wtr);
    }
}