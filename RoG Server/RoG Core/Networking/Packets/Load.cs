﻿using RoGCore.Models;

namespace RoGCore.Networking.Packets;

public readonly struct Load : IPacket
{
    public readonly ushort CharacterId;

    public Load(ushort characterId)
    {
        CharacterId = characterId;
    }

    public MessageId Id => MessageId.Load;

    public IPacket Read(NReader rdr)
    {
        return new Load(rdr.ReadUInt16());
    }

    public void Write(NWriter wtr)
    {
        wtr.Write(CharacterId);
    }
}