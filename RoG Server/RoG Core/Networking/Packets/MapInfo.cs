﻿using RoGCore.Models;

namespace RoGCore.Networking.Packets;

public readonly struct MapInfo : IPacket
{
    public readonly ushort Width;
    public readonly ushort Height;
    public readonly string Name;
    public readonly byte Difficulty;
    public readonly byte Background;
    public readonly bool AllowTeleport;
    public readonly string Music;
    public readonly ushort EventBoost;
    public readonly ushort MaxPlayers;
    public readonly bool Dungeon;
    public readonly bool Lighting;

    public MapInfo(
        ushort width,
        ushort height,
        string name,
        byte difficulty,
        byte background,
        bool allowTeleport,
        string music,
        ushort eventBoost,
        ushort maxPlayers,
        bool dungeon,
        bool lighting)
    {
        Width = width;
        Height = height;
        Name = name;
        Difficulty = difficulty;
        Background = background;
        AllowTeleport = allowTeleport;
        Music = music;
        EventBoost = eventBoost;
        MaxPlayers = maxPlayers;
        Dungeon = dungeon;
        Lighting = lighting;
    }

    public MessageId Id => MessageId.MapInfo;

    public IPacket CreateInstance()
    {
        return default(MapInfo);
    }

    public IPacket Read(NReader rdr)
    {
        return new MapInfo(
            rdr.ReadUInt16(),
            rdr.ReadUInt16(),
            rdr.ReadUtf(),
            rdr.ReadByte(),
            rdr.ReadByte(),
            rdr.ReadBoolean(),
            rdr.ReadUtf(),
            rdr.ReadUInt16(),
            rdr.ReadUInt16(),
            rdr.ReadBoolean(),
            rdr.ReadBoolean());
    }

    public void Write(NWriter wtr)
    {
        wtr.Write(Width);
        wtr.Write(Height);
        wtr.WriteUtf(Name);
        wtr.Write(Difficulty);
        wtr.Write(Background);
        wtr.Write(AllowTeleport);
        wtr.WriteUtf(Music);
        wtr.Write(EventBoost);
        wtr.Write(MaxPlayers);
        wtr.Write(Dungeon);
        wtr.Write(Lighting);
    }
}