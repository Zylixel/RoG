﻿using System.Numerics;
using RoGCore.Models;
using RoGCore.Utils;

namespace RoGCore.Networking.Packets;

public readonly struct Move : IPacket
{
    public readonly Vector2 Position;

    public Move(Vector2 position)
    {
        Position = position;
    }

    public MessageId Id => MessageId.Move;

    public IPacket Read(NReader rdr)
    {
        return new Move(Vector2Extensions.Read(rdr));
    }

    public void Write(NWriter wtr)
    {
        Position.Write(wtr);
    }
}