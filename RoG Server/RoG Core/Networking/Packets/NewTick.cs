﻿using System.Buffers;
using RoGCore.Models;

namespace RoGCore.Networking.Packets;

public readonly struct NewTick : IPacket, IDisposablePacket
{
    public readonly int TickId;
    public readonly long ServerTime;
    public readonly float Tps;
    public readonly float Mspt;
    public readonly ushort PlayersInWorld;
    public readonly SizedArray<ObjectStats> UpdateStatuses;
    public readonly float LightLevel;

    public NewTick(int tickId, long serverTime, float tps, float mspt, ushort playersInWorld, in SizedArray<ObjectStats> updateStatuses, float lightLevel)
    {
        TickId = tickId;
        ServerTime = serverTime;
        Tps = tps;
        Mspt = mspt;
        PlayersInWorld = playersInWorld;
        UpdateStatuses = updateStatuses;
        LightLevel = lightLevel;
    }

    public NewTick(NReader rdr)
    {
        TickId = rdr.ReadInt32();
        ServerTime = rdr.ReadInt64();
        Tps = rdr.ReadSingle();
        Mspt = rdr.ReadSingle();
        PlayersInWorld = rdr.ReadUInt16();

        var updateStatusAmount = rdr.ReadUInt16();
        var updateStatuses = ArrayPool<ObjectStats>.Shared.Rent(updateStatusAmount);
        for (var i = 0; i < updateStatusAmount; i++)
        {
            updateStatuses[i] = new ObjectStats(rdr);
        }

        UpdateStatuses = new SizedArray<ObjectStats>(updateStatuses, updateStatusAmount);

        LightLevel = rdr.ReadSingle();
    }

    public MessageId Id => MessageId.NewTick;

    public IPacket Read(NReader rdr)
    {
        return new NewTick(rdr);
    }

    public void Write(NWriter wtr)
    {
        wtr.Write(TickId);
        wtr.Write(ServerTime);
        wtr.Write(Tps);
        wtr.Write(Mspt);
        wtr.Write(PlayersInWorld);

        wtr.Write((ushort)UpdateStatuses.Length);
        for (var i = 0; i < UpdateStatuses.Length; i++)
        {
            UpdateStatuses[i].Write(wtr);
        }

        wtr.Write(LightLevel);
    }

    public void Dispose()
    {
        ArrayPool<ObjectStats>.Shared.Return(UpdateStatuses.Data);
    }
}