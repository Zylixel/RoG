﻿using RoGCore.Models;

namespace RoGCore.Networking.Packets;

public readonly struct Notification : IPacket
{
    public readonly long ObjectId;
    public readonly string Text;
    public readonly ARGB Color;
    public readonly float Scale;

    public Notification(long objectId, string text, ARGB color, float scale = 1f)
    {
        ObjectId = objectId;
        Text = text;
        Color = color;
        Scale = scale;
    }

    public MessageId Id => MessageId.Notification;

    public IPacket Read(NReader rdr)
    {
        return new Notification(rdr.ReadInt64(), rdr.ReadUtf(), new ARGB(rdr), rdr.ReadSingle());
    }

    public void Write(NWriter wtr)
    {
        wtr.Write(ObjectId);
        wtr.WriteUtf(Text);
        Color.Write(wtr);
        wtr.Write(Scale);
    }
}