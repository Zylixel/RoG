﻿using RoGCore.Models;

namespace RoGCore.Networking.Packets;

public readonly struct ObjectDamage : IPacket
{
    public readonly Vector2Int TilePosition;
    public readonly ushort Damage;

    public ObjectDamage(Vector2Int tilePosition, ushort damage)
    {
        TilePosition = tilePosition;
        Damage = damage;
    }

    public MessageId Id => MessageId.ObjectDamage;

    public IPacket Read(NReader rdr)
    {
        return new ObjectDamage(new Vector2Int(rdr), rdr.ReadUInt16());
    }

    public void Write(NWriter wtr)
    {
        TilePosition.Write(wtr);
        wtr.Write(Damage);
    }
}