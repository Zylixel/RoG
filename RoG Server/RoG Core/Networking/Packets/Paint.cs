﻿using RoGCore.Models;

namespace RoGCore.Networking.Packets;

public readonly struct Paint : IPacket
{
    public readonly RequestType Request;
    public readonly RGB Color;

    public Paint(RequestType request, RGB color)
    {
        Request = request;
        Color = color;
    }

    public enum RequestType : byte
    {
        SetBig,
        SetSmall,
        RemoveBig,
        RemoveSmall,
    }

    public MessageId Id => MessageId.Paint;

    public IPacket Read(NReader rdr)
    {
        return new Paint((RequestType)rdr.ReadByte(), new RGB(rdr));
    }

    public void Write(NWriter wtr)
    {
        wtr.Write((byte)Request);
        Color.Write(wtr);
    }
}