﻿using RoGCore.Models;

namespace RoGCore.Networking.Packets;

public readonly struct PlayerHit : IPacket
{
    public readonly ushort BulletId;
    public readonly long ObjectId;

    public PlayerHit(ushort bulletId, long objectId)
    {
        BulletId = bulletId;
        ObjectId = objectId;
    }

    public MessageId Id => MessageId.PlayerHit;

    public IPacket Read(NReader rdr)
    {
        return new PlayerHit(
            rdr.ReadUInt16(),
            rdr.ReadInt64());
    }

    public void Write(NWriter wtr)
    {
        wtr.Write(BulletId);
        wtr.Write(ObjectId);
    }
}