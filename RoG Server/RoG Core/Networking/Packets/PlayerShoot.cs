﻿using System.Numerics;
using RoGCore.Models;
using RoGCore.Utils;

namespace RoGCore.Networking.Packets;

public readonly struct PlayerShoot : IPacket
{
    public readonly ushort BulletId;
    public readonly Vector2 Position;
    public readonly Angle Angle;
    public readonly long Time;

    public PlayerShoot(ushort bulletId, Vector2 position, Angle angle, long time)
    {
        BulletId = bulletId;
        Position = position;
        Angle = angle;
        Time = time;
    }

    public MessageId Id => MessageId.PlayerShoot;

    public IPacket CreateInstance()
    {
        return default(PlayerShoot);
    }

    public IPacket Read(NReader rdr)
    {
        return new PlayerShoot(rdr.ReadUInt16(), Vector2Extensions.Read(rdr), Angle.Read(rdr), rdr.ReadInt64());
    }

    public void Write(NWriter wtr)
    {
        wtr.Write(BulletId);
        Position.Write(wtr);
        Angle.Write(wtr);
        wtr.Write(Time);
    }
}