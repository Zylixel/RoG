﻿using RoGCore.Models;

namespace RoGCore.Networking.Packets;

public readonly struct PolishItems : IPacket
{
    public readonly long BlacksmithId;

    public PolishItems(long blacksmithId)
    {
        BlacksmithId = blacksmithId;
    }

    public MessageId Id => MessageId.PolishItems;

    public IPacket Read(NReader rdr)
    {
        return new PolishItems(rdr.ReadInt64());
    }

    public void Write(NWriter wtr)
    {
        wtr.Write(BlacksmithId);
    }
}