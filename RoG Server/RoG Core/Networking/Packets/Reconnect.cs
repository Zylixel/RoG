﻿using RoGCore.Models;

namespace RoGCore.Networking.Packets;

public readonly struct Reconnect : IPacket
{
    public readonly string Name;
    public readonly short GameId;
    public readonly byte Difficulty;

    public Reconnect(string name, short gameId, byte difficulty)
    {
        Name = name;
        GameId = gameId;
        Difficulty = difficulty;
    }

    public MessageId Id => MessageId.Reconnect;

    public IPacket Read(NReader rdr)
    {
        return new Reconnect(rdr.ReadUtf(), rdr.ReadInt16(), rdr.ReadByte());
    }

    public void Write(NWriter wtr)
    {
        wtr.WriteUtf(Name);
        wtr.Write(GameId);
        wtr.Write(Difficulty);
    }
}