﻿using RoGCore.Models;

namespace RoGCore.Networking.Packets;

public readonly struct Result : IPacket
{
    public readonly bool Success;
    public readonly string Message;

    public Result(bool success, string? message = null)
    {
        Success = success;
        Message = message ?? string.Empty;
    }

    public MessageId Id => MessageId.Result;

    public IPacket Read(NReader rdr)
    {
        return new Result(rdr.ReadBoolean(), rdr.ReadUtf());
    }

    public void Write(NWriter wtr)
    {
        wtr.Write(Success);
        wtr.WriteUtf(Message);
    }
}