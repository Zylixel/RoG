﻿using RoGCore.Models;

namespace RoGCore.Networking.Packets;

public readonly struct SetCondition : IPacket
{
    public readonly int Effect;
    public readonly float Duration;

    public SetCondition(int effect, float duration)
    {
        Effect = effect;
        Duration = duration;
    }

    public MessageId Id => MessageId.SetCondition;

    public IPacket Read(NReader rdr)
    {
        return new SetCondition(rdr.ReadInt32(), rdr.ReadSingle());
    }

    public void Write(NWriter wtr)
    {
        wtr.Write(Effect);
        wtr.Write(Duration);
    }
}