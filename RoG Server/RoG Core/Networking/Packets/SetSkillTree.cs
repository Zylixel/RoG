﻿using RoGCore.Models;

namespace RoGCore.Networking.Packets;

public readonly struct SetSkillTree : IPacket
{
    public readonly byte[] TreeData;

    public SetSkillTree(byte[] treeData)
    {
        TreeData = treeData;
    }

    public MessageId Id => MessageId.SetSkillTree;

    public IPacket CreateInstance()
    {
        return default(SetSkillTree);
    }

    public IPacket Read(NReader rdr)
    {
        var length = rdr.ReadUInt16();
        return new SetSkillTree(rdr.ReadBytes(length));
    }

    public void Write(NWriter wtr)
    {
        wtr.Write((ushort)TreeData.Length);
        wtr.Write(TreeData);
    }
}