﻿using System.Numerics;
using RoGCore.Models;
using RoGCore.Utils;

namespace RoGCore.Networking.Packets;

public readonly struct SetSummonPosition : IPacket
{
    public readonly Vector2 Position;

    public SetSummonPosition(Vector2 position)
    {
        Position = position;
    }

    public MessageId Id => MessageId.SetSummonPosition;

    public IPacket CreateInstance()
    {
        return default(SetSummonPosition);
    }

    public IPacket Read(NReader rdr)
    {
        return new SetSummonPosition(Vector2Extensions.Read(rdr));
    }

    public void Write(NWriter wtr)
    {
        Position.Write(wtr);
    }
}