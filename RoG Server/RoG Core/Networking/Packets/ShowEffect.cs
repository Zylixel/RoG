﻿using System.Numerics;
using RoGCore.Models;
using RoGCore.Utils;

namespace RoGCore.Networking.Packets;

public readonly struct ShowEffect : IPacket
{
    public readonly Type EffectType;
    public readonly long TargetId;
    public readonly ARGB Color;
    public readonly Vector2 PosA;
    public readonly Vector2 PosB;
    public readonly string Data;

    public ShowEffect(Type effectType, long targetId, in ARGB color, Vector2? posA = null, Vector2? posB = null, string? data = null)
    {
        EffectType = effectType;
        TargetId = targetId;
        Color = color;
        PosA = posA ?? Vector2.Zero;
        PosB = posB ?? Vector2.Zero;
        Data = data ?? string.Empty;
    }

    public ShowEffect(Type effectType, long targetId, Vector2? posA = null, Vector2? posB = null, string? data = null)
    {
        EffectType = effectType;
        TargetId = targetId;
        Color = ARGB.White;
        PosA = posA ?? Vector2.Zero;
        PosB = posB ?? Vector2.Zero;
        Data = data ?? string.Empty;
    }

    public enum Type : byte
    {
        Unknown = 0,
        Heal = 1,
        Teleport = 2, // lastPos = pos1
        Stream = 3,
        Toss = 4,
        Nova = 5, // radius=pos1.x
        Poison = 6,
        Line = 7,
        Burst = 8, // radius=dist(pos1,pos2)
        Flow = 9,
        Ring = 10, // radius=pos1.x
        Lightning = 11, // particleSize=pos2.x
        Collapse = 12, // radius=dist(pos1,pos2)
        Coneblast = 13, // origin=pos1, radius = pos2.x
        Jitter = 14,
        Flash = 15, // flashRate=pos1.x, time=pos1.y
        ThrowProjectile = 16,
        Scepter = 17, // Angle = Pos1.x
    }

    public MessageId Id => MessageId.ShowEffect;

    public IPacket Read(NReader rdr)
    {
        return new ShowEffect(
            (Type)rdr.ReadByte(),
            rdr.ReadInt64(),
            new ARGB(rdr),
            Vector2Extensions.Read(rdr),
            Vector2Extensions.Read(rdr),
            rdr.ReadUtf());
    }

    public void Write(NWriter wtr)
    {
        wtr.Write((byte)EffectType);
        wtr.Write(TargetId);
        PosA.Write(wtr);
        PosB.Write(wtr);
        Color.Write(wtr);
        wtr.WriteUtf(Data);
    }
}