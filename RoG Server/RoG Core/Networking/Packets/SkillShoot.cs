﻿using System.Numerics;
using RoGCore.Models;
using RoGCore.Utils;

namespace RoGCore.Networking.Packets;

public readonly struct SkillShoot : IPacket
{
    public readonly ushort BulletId;
    public readonly long OwnerId;
    public readonly SkillType Skill;
    public readonly Vector2 StartingPos;
    public readonly Angle Angle;
    public readonly short Damage;

    public SkillShoot(ushort bulletId, long ownerId, SkillType skill, Vector2 startingPos, Angle angle, short damage)
    {
        BulletId = bulletId;
        OwnerId = ownerId;
        Skill = skill;
        StartingPos = startingPos;
        Angle = angle;
        Damage = damage;
    }

    public MessageId Id => MessageId.SkillShoot;

    public IPacket Read(NReader rdr)
    {
        return new SkillShoot(
rdr.ReadUInt16(),
rdr.ReadInt64(),
(SkillType)rdr.ReadUInt16(),
Vector2Extensions.Read(rdr),
Angle.Read(rdr),
rdr.ReadInt16());
    }

    public void Write(NWriter wtr)
    {
        wtr.Write(BulletId);
        wtr.Write(OwnerId);
        wtr.Write((ushort)Skill);
        StartingPos.Write(wtr);
        Angle.Write(wtr);
        wtr.Write(Damage);
    }
}