﻿using RoGCore.Models;

namespace RoGCore.Networking.Packets;

public readonly struct SquareHit : IPacket
{
    public readonly byte BulletId;
    public readonly Vector2Int Square;

    public SquareHit(byte bulletId, Vector2Int square)
    {
        BulletId = bulletId;
        Square = square;
    }

    public MessageId Id => MessageId.SquareHit;

    public IPacket Read(NReader rdr)
    {
        return new SquareHit(rdr.ReadByte(), new Vector2Int(rdr));
    }

    public void Write(NWriter wtr)
    {
        wtr.Write(BulletId);
        Square.Write(wtr);
    }
}