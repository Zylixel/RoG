﻿using RoGCore.Models;

namespace RoGCore.Networking.Packets;

public readonly struct Teleport : IPacket
{
    public readonly long ObjectId;

    public Teleport(long objectId)
    {
        ObjectId = objectId;
    }

    public MessageId Id => MessageId.Teleport;

    public IPacket Read(NReader rdr)
    {
        return new Teleport(rdr.ReadInt64());
    }

    public void Write(NWriter wtr)
    {
        wtr.Write(ObjectId);
    }
}