﻿using RoGCore.Models;

namespace RoGCore.Networking.Packets;

public readonly struct Text : IPacket
{
    public readonly string Name;
    public readonly long ObjectId;
    public readonly string Message;
    public readonly RGB NameColor;
    public readonly RGB TextColor;
    public readonly string Recipient;

    public Text(string name, long objectId, string message, in RGB nameColor, in RGB textColor, string? recipient = null)
    {
        Name = name;
        ObjectId = objectId;
        Message = message;
        NameColor = nameColor;
        TextColor = textColor;
        Recipient = recipient ?? string.Empty;
    }

    public Text(string name, string message, in RGB nameColor, in RGB textColor, string? recipient = null)
    {
        Name = name;
        ObjectId = -1;
        Message = message;
        NameColor = nameColor;
        TextColor = textColor;
        Recipient = recipient ?? string.Empty;
    }

    public Text(string message, in RGB textColor, string? recipient = null)
    {
        Name = string.Empty;
        ObjectId = -1;
        Message = message;
        NameColor = RGB.White;
        TextColor = textColor;
        Recipient = recipient ?? string.Empty;
    }

    public Text(string message)
    {
        Name = string.Empty;
        ObjectId = -1;
        Message = message;
        NameColor = RGB.White;
        TextColor = RGB.White;
        Recipient = string.Empty;
    }

    public MessageId Id => MessageId.Text;

    public IPacket Read(NReader rdr)
    {
        return new Text(rdr.ReadUtf(), rdr.ReadInt64(), rdr.ReadUtf(), new RGB(rdr), new RGB(rdr), rdr.ReadUtf());
    }

    public void Write(NWriter wtr)
    {
        wtr.WriteUtf(Name);
        wtr.Write(ObjectId);
        wtr.WriteUtf(Message);
        NameColor.Write(wtr);
        TextColor.Write(wtr);
        wtr.WriteUtf(Recipient);
    }
}