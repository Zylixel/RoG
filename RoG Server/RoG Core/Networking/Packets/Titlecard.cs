﻿using RoGCore.Models;

namespace RoGCore.Networking.Packets;

public readonly struct TitleCard : IPacket
{
    public readonly string Title;

    public TitleCard(string title)
    {
        Title = title;
    }

    public MessageId Id => MessageId.TitleCard;

    public IPacket Read(NReader rdr)
    {
        return new TitleCard(rdr.ReadUtf());
    }

    public void Write(NWriter wtr)
    {
        wtr.WriteUtf(Title);
    }
}