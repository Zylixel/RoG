﻿using System.Buffers;
using RoGCore.Models;

namespace RoGCore.Networking.Packets;

public readonly struct Update : IPacket, IDisposablePacket, IDisposable
{
    public readonly SizedArray<TileData> Tiles;
    public readonly SizedArray<long> RemovedObjectIds;
    public readonly SizedArray<Vector2Int> RemovedStatics;
    public readonly List<ObjectDef> NewStatics;
    public readonly List<ObjectDef> NewObjects;
    public readonly Rectangle ViewRectangle;

    public Update(in SizedArray<TileData> tiles, List<ObjectDef> newObjects, in SizedArray<long> removedObjectIds, List<ObjectDef> newStatics, in SizedArray<Vector2Int> removedStatics, Rectangle viewRectangle)
    {
        Tiles = tiles;
        NewObjects = newObjects;
        RemovedObjectIds = removedObjectIds;
        NewStatics = newStatics;
        RemovedStatics = removedStatics;
        ViewRectangle = viewRectangle;
    }

    public Update(NReader rdr)
    {
        var tileAmount = rdr.ReadUInt16();
        var tiles = ArrayPool<TileData>.Shared.Rent(tileAmount);
        for (var i = 0; i < tileAmount; i++)
        {
            tiles[i] = new TileData(rdr);
        }

        Tiles = new SizedArray<TileData>(tiles, tileAmount);

        NewObjects = new List<ObjectDef>(rdr.ReadUInt16());
        for (var i = 0; i < NewObjects.Capacity; i++)
        {
            NewObjects.Add(new ObjectDef(rdr));
        }

        var removedObjectAmount = rdr.ReadUInt16();
        var removedObjects = ArrayPool<long>.Shared.Rent(removedObjectAmount);
        for (var i = 0; i < removedObjectAmount; i++)
        {
            removedObjects[i] = rdr.ReadInt64();
        }

        RemovedObjectIds = new SizedArray<long>(removedObjects, removedObjectAmount);

        NewStatics = new List<ObjectDef>(rdr.ReadUInt16());
        for (var i = 0; i < NewStatics.Capacity; i++)
        {
            NewStatics.Add(new ObjectDef(rdr));
        }

        var removedstaticAmount = rdr.ReadUInt16();
        var removedStatics = ArrayPool<Vector2Int>.Shared.Rent(removedObjectAmount);
        for (var i = 0; i < removedstaticAmount; i++)
        {
            removedStatics[i] = new Vector2Int(rdr);
        }

        RemovedStatics = new SizedArray<Vector2Int>(removedStatics, removedstaticAmount);

        ViewRectangle = new Rectangle(rdr.ReadSingle(), rdr.ReadSingle(), rdr.ReadSingle(), rdr.ReadSingle());
    }

    public MessageId Id => MessageId.Update;

    public IPacket Read(NReader rdr)
    {
        return new Update(rdr);
    }

    public void Write(NWriter wtr)
    {
        wtr.Write((ushort)Tiles.Length);
        for (var i = 0; i < Tiles.Length; i++)
        {
            Tiles[i].Write(wtr);
        }

        wtr.Write((ushort)NewObjects.Count);
        for (var i = 0; i < NewObjects.Count; i++)
        {
            NewObjects[i].Write(wtr);
        }

        wtr.Write((ushort)RemovedObjectIds.Length);
        for (var i = 0; i < RemovedObjectIds.Length; i++)
        {
            wtr.Write(RemovedObjectIds[i]);
        }

        wtr.Write((ushort)NewStatics.Count);
        for (var i = 0; i < NewStatics.Count; i++)
        {
            NewStatics[i].Write(wtr);
        }

        wtr.Write((ushort)RemovedStatics.Length);
        for (var i = 0; i < RemovedStatics.Length; i++)
        {
            RemovedStatics[i].Write(wtr);
        }

        wtr.Write(ViewRectangle.X);
        wtr.Write(ViewRectangle.Y);
        wtr.Write(ViewRectangle.Width);
        wtr.Write(ViewRectangle.Height);
    }

    public void Dispose()
    {
        ArrayPool<TileData>.Shared.Return(Tiles.Data);
        ArrayPool<long>.Shared.Return(RemovedObjectIds.Data);
        ArrayPool<Vector2Int>.Shared.Return(RemovedStatics.Data);
    }
}