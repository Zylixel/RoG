﻿using System.Numerics;
using RoGCore.Models;
using RoGCore.Models.ObjectSlot;
using RoGCore.Utils;

namespace RoGCore.Networking.Packets;

public readonly struct UseItem : IPacket
{
    public readonly IObjectSlot SlotObject;
    public readonly Vector2 ItemUsePos;

    public UseItem(IObjectSlot slotObject, Vector2 itemUsePos)
    {
        SlotObject = slotObject;
        ItemUsePos = itemUsePos;
    }

    public MessageId Id => MessageId.UseItem;

    public IPacket Read(NReader rdr)
    {
        return new UseItem(rdr.ReadObjectSlot(), Vector2Extensions.Read(rdr));
    }

    public void Write(NWriter wtr)
    {
        SlotObject.Write(wtr);
        ItemUsePos.Write(wtr);
    }
}