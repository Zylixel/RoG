﻿using RoGCore.Models;

namespace RoGCore.Networking.Packets;

public readonly struct UsePortal : IPacket
{
    public readonly long ObjectId;

    public UsePortal(long objectId)
    {
        ObjectId = objectId;
    }

    MessageId IPacket.Id => MessageId.UsePortal;

    public IPacket Read(NReader rdr)
    {
        return new UsePortal(rdr.ReadInt64());
    }

    public void Write(NWriter wtr)
    {
        wtr.Write(ObjectId);
    }
}