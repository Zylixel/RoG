﻿using System.Numerics;
using RoGCore.Models;
using RoGCore.Utils;

namespace RoGCore.Networking.Packets;

public readonly struct UseSkill : IPacket
{
    public readonly SkillType Skill;
    public readonly ushort BulletId;
    public readonly Vector2 Position;
    public readonly Angle Angle;

    public UseSkill(SkillType skill, ushort bulletId, Vector2 position, Angle angle)
    {
        Skill = skill;
        BulletId = bulletId;
        Position = position;
        Angle = angle;
    }

    public MessageId Id => MessageId.UseSkill;

    public IPacket Read(NReader rdr)
    {
        return new UseSkill(
(SkillType)rdr.ReadUInt16(),
rdr.ReadUInt16(),
Vector2Extensions.Read(rdr),
Angle.Read(rdr));
    }

    public void Write(NWriter wtr)
    {
        wtr.Write((ushort)Skill);
        wtr.Write(BulletId);
        Position.Write(wtr);
        Angle.Write(wtr);
    }
}