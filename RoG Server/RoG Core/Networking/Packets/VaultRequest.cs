﻿using RoGCore.Models;

namespace RoGCore.Networking.Packets;

public readonly struct VaultRequest : IPacket
{
    public readonly byte Page;
    public readonly string SearchTerm;
    public readonly SortType SortMode;

    public VaultRequest(byte page, string searchTerm, SortType sortMode)
    {
        Page = page;
        SearchTerm = searchTerm;
        SortMode = sortMode;
    }

    public enum SortType : byte
    {
        Alphabetical,
        TierUp,
        TierDown,
        LevelUp,
        LevelDown,
    }

    public MessageId Id => MessageId.VaultRequest;

    public IPacket Read(NReader rdr)
    {
        return new VaultRequest(rdr.ReadByte(), rdr.ReadUtf(), (SortType)rdr.ReadByte());
    }

    public void Write(NWriter wtr)
    {
        wtr.Write(Page);
        wtr.WriteUtf(SearchTerm);
        wtr.Write((byte)SortMode);
    }
}