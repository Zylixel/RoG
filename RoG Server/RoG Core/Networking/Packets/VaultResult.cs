﻿using RoGCore.Models;

namespace RoGCore.Networking.Packets;

public readonly struct VaultResult : IPacket
{
    public readonly RawItem?[] NewInv;
    public readonly byte Pages;
    public readonly byte LockedAmount;

    public VaultResult(RawItem?[] newInv, byte pages, byte lockedAmount)
    {
        NewInv = newInv;
        Pages = pages;
        LockedAmount = lockedAmount;
    }

    public MessageId Id => MessageId.VaultResult;

    public IPacket Read(NReader rdr)
    {
        var newInvSize = rdr.ReadUInt16();
        var newInv = RawItem.Read(rdr.ReadBytes(newInvSize));

        return new VaultResult(newInv, rdr.ReadByte(), rdr.ReadByte());
    }

    public void Write(NWriter wtr)
    {
        var newInvBytes = RawItem.Write(NewInv, true, true);

        wtr.Write(newInvBytes.Length);

        for (int i = 0; i < newInvBytes.Length; i++)
        {
            wtr.Write(newInvBytes[i]);
        }

        wtr.Write(Pages);
        wtr.Write(LockedAmount);
    }
}