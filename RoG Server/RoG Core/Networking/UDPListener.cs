﻿using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using Microsoft.Extensions.Logging;
using RoGCore.Logging;

namespace RoGCore.Networking;

public class UDPListener
{
    private static readonly ILogger<UDPListener> Logger = LogFactory.LoggingInstance<UDPListener>();

    private readonly UdpClient client;

    public UDPListener(UdpClient client, bool disableRemoteDisconnectException = true)
    {
        this.client = client;

        if (disableRemoteDisconnectException)
        {
            DisableRemoteDisconnectException();
        }
    }

    public event EventHandler<(IPEndPoint, byte[])>? PacketReceived;

    public void BeginListening()
    {
        client.BeginReceive(Listen, null);
    }

    private void Listen(IAsyncResult ar)
    {
        try
        {
            IPEndPoint? remoteEndpoint = null;
            var bytes = client.EndReceive(ar, ref remoteEndpoint);

            Debug.Assert(remoteEndpoint is not null, "remoteEndpoint should not be null");

            PacketReceived?.Invoke(this, (remoteEndpoint!, bytes));
        }
        catch (ObjectDisposedException)
        {
            Logger.LogInformation("Socket was disposed, this is expected behavior during a server shutdown");
        }
        catch (SocketException ex)
        {
            Logger.LogInformation($"SocketException {ex}, this is expected behavior during a server shutdown");
        }

        client.BeginReceive(Listen, null);
    }

    private void DisableRemoteDisconnectException()
    {
        // https://stackoverflow.com/a/39440399
        const int SIO_UDP_CONNRESET = -1744830452;
        client.Client.IOControl(
            (IOControlCode)SIO_UDP_CONNRESET,
            new byte[] { 0, 0, 0, 0 },
            null);
    }
}