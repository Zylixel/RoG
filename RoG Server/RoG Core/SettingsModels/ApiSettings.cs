﻿namespace RoGCore.SettingsModels;

public class ApiSettings
{
    public int Port { get; set; } = 3000;
}