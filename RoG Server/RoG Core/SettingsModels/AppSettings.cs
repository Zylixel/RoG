﻿namespace RoGCore.SettingsModels;

public class AppSettings
{
    public ApiSettings Api { get; set; } = new();

    public GameServerSettings GameServer { get; set; } = new();

    public DatabaseSettings Database { get; set; } = new();

    public CustomLoggingSettings CustomLogging { get; set; } = new();
}
