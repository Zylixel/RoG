﻿namespace RoGCore.SettingsModels;

public class CombatSettings
{
    public int OutOfCombatWisdomMultiplier { get; set; } = 4;

    public int OutOfCombatVitalityMultiplier { get; set; } = 10;
}
