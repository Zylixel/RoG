﻿namespace RoGCore.SettingsModels;

public class DatabaseSettings
{
    public string Host { get; set; } = "localhost";

    public int Port { get; set; } = 6379;

    public string Password { get; set; } = string.Empty;

    public int SyncTimeout { get; set; } = 120000;
}
