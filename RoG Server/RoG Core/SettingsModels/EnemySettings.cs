﻿namespace RoGCore.SettingsModels;

public class EnemySettings
{
    public int MaxPerPlayer { get; set; } = 20;

    public int MinSpawnDistance { get; set; } = 20;

    public int MaxSpawnDistance { get; set; } = 25;

    public int DespawnDistance => MaxSpawnDistance + 3;

    public int DespawnDistanceSqr => DespawnDistance * DespawnDistance;

    public int Spread { get; set; } = 3;

    public float HpScaling { get; set; } = 0.75f;

    public float HpScalingDistance { get; set; } = 15;
}
