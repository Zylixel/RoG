﻿namespace RoGCore.SettingsModels;

public class EventSettings
{
    public float LootDropMultiplier { get; set; } = 1.5f;
}
