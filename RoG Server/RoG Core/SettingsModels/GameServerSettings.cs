﻿namespace RoGCore.SettingsModels;

public class GameServerSettings
{
    public string Name { get; set; } = "Unnamed GameServer";

    public string Dns { get; set; } = "localhost";

    public int Port { get; set; } = 2060;

    public int Tps { get; set; } = 25;

    public bool Multithreading { get; set; } = true;

    public bool AdminOnly { get; set; }

    public string File { get; set; } = "RoG GameServer.exe";

    public EnemySettings Enemy { get; set; } = new();

    public CombatSettings Combat { get; set; } = new();

    public EventSettings Event { get; set; } = new();

    public SightSettings Sight { get; set; } = new();

    public MusicSettings Music { get; set; } = new();

    public int MaximumStat { get; set; } = 1000;

    public int StartingSilver { get; set; } = 10;

    public int StartingVaults { get; set; } = 15;

    public int StructureRadius { get; set; } = 3;

    public int StructureMapSize { get; set; } = 500;
}
