﻿namespace RoGCore.SettingsModels;

public class MusicSettings
{
    public string Default { get; set; } = "Default";

    public string RodentsBurrow { get; set; } = "RodentsBurrow";

    public string SafeArea { get; set; } = "YouAndMe";

    public string Realm { get; set; } = "OceanWaves";

    public string ReedosharksQuarters { get; set; } = "TheBuccaneersHaul";

    public string ReedosharkBattle { get; set; } = "Heroes Confrontation";
}
