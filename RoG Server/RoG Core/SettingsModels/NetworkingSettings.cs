﻿namespace RoGCore.SettingsModels;

public static class NetworkingSettings
{
    public const string BUILDVERSION = "v1";
    public const string MINORVERSION = "0.0";
    public const string FULLBUILD = BUILDVERSION + "." + MINORVERSION;

    public const int MAXCONNECTIONS = 1000;
}