﻿namespace RoGCore.SettingsModels;

public class SightSettings
{
    public int Radius { get; set; } = 20;

    public int RectangleSize { get; set; } = 48;
}
