﻿using System.ComponentModel;

namespace RoGCore.Utils;

// Used to allow IL2CPP Compiler to compile properly
public class ArrayDefaultValueAttribute : DefaultValueAttribute
{
    public ArrayDefaultValueAttribute(int[] value)
        : base(value)
    {
    }
}