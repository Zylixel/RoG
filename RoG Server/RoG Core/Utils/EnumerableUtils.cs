﻿using System.Security.Cryptography;

namespace RoGCore.Utils;

public static class EnumerableUtils
{
    public static T RandomElement<T>(this IEnumerable<T> source, Random rng)
        where T : struct
    {
        T current = default;
        var count = 0;
        foreach (var element in source)
        {
            count++;
            if (rng.Next(count) == 0)
            {
                current = element;
            }
        }

        return count == 0 ? throw new InvalidOperationException("Sequence was empty") : current;
    }

    public static void Shuffle<T>(this IList<T> list)
    {
        if (list is null)
        {
            return;
        }

        var randomNumberProvider = RandomNumberGenerator.Create();

        var n = list.Count;
        while (n > 1)
        {
            var box = new byte[1];
            do
            {
                randomNumberProvider.GetBytes(box);
            }
            while (box[0] >= n * (uint.MaxValue / n));
            var k = box[0] % n;
            n--;
            (list[n], list[k]) = (list[k], list[n]);
        }

        randomNumberProvider.Dispose();
    }
}