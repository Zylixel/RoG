﻿using UnityEngine;
using Random = System.Random;

namespace RoGCore.Utils;

public static class MathsUtils
{
    public static double Dist(double x1, double y1, double x2, double y2)
    {
        return Math.Sqrt(((x1 - x2) * (x1 - x2)) + ((y1 - y2) * (y1 - y2)));
    }

    public static double DistSqr(double x1, double y1, double x2, double y2)
    {
        return ((x1 - x2) * (x1 - x2)) + ((y1 - y2) * (y1 - y2));
    }

    public static double NextDouble(this Random rand, double minValue, double maxValue)
    {
        return (rand.NextDouble() * (maxValue - minValue)) + minValue;
    }

    public static T Clamp<T>(this T val, T min, T max)
        where T : IComparable<T>
    {
        return val.CompareTo(min) < 0 ? min : val.CompareTo(max) > 0 ? max : val;
    }

    public static float Remap(this float value, float from1, float to1, float from2, float to2)
    {
        return ((value - from1) / (to1 - from1) * (to2 - from2)) + from2;
    }

    public static float ClampAngle(float angle)
    {
        while (angle < 0)
        {
            angle += Mathf.PI * 2;
        }

        while (angle > Mathf.PI * 2)
        {
            angle -= Mathf.PI * 2;
        }

        return angle;
    }
}