﻿using System.Text;
using FluentResults;

namespace RoGCore.Utils;

public static class ResultExtenstions
{
    public static string ErrorString<T>(this Result<T> result) => ErrorString(result.Errors);

    public static string ErrorString(this Result result) => ErrorString(result.Errors);

    private static string ErrorString(IList<IError> errors)
    {
        if (errors.Count == 0)
        {
            return errors[0].Message;
        }

        var stringBuilder = new StringBuilder();

        for (var i = 0; i < errors.Count; i++)
        {
            var error = errors[i];
            stringBuilder.Append(error.Message);

            if (i < errors.Count - 1)
            {
                stringBuilder.Append(", ");
            }
        }

        return stringBuilder.ToString();
    }
}
