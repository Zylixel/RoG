﻿using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace RoGCore.Utils;

public static class StringUtils
{
    public const string BADWORDS = "(arse|arsehole|ass|asshat|assjabber|asspirate|assbag|assbandit|assbanger|assbite|assclown|asscock|asscracker|asses|assface|assfuck|assfucker|assgoblin|asshat|asshead|asshole|asshopper|assjacker|asslick|asslicker|assmonkey|assmunch|assmuncher|assnigger|asspirate|assshit|assshole)";

    public static int FromString(string x)
    {
        x = x.Trim();
        return x.StartsWith("0x") ? int.Parse(x.Remove(0, 2), NumberStyles.HexNumber) : x.IntParseFast();
    }

    /// <summary>
    ///     Indicates whether a specified string is null, empty, or consists only of white-space characters.
    /// </summary>
    /// <param name="value">value: The string to test.</param>
    /// <returns>
    ///     true if the value parameter is null or System.String.Empty, or if value consists exclusively of white-space
    ///     characters.
    /// </returns>
    public static bool IsNullOrWhiteSpace(this string? value)
    {
        if (value is null)
        {
            return true;
        }

        var index = 0;
        while (index < value.Length)
        {
            if (char.IsWhiteSpace(value[index]))
            {
                index++;
            }
            else
            {
                return false;
            }
        }

        return true;
    }

    public static bool ContainsIgnoreCase(this string self, string val)
    {
        return self.IndexOf(val, StringComparison.InvariantCultureIgnoreCase) != -1;
    }

    public static bool EqualsIgnoreCase(this string self, string val)
    {
        return self.Equals(val, StringComparison.InvariantCultureIgnoreCase);
    }

    public static string ToCommaSepString<T>(this T[] arr)
    {
        StringBuilder ret = new();
        for (var i = 0; i < arr.Length; i++)
        {
            var value = arr[i];
            if (value is null)
            {
                continue;
            }

            if (i != 0)
            {
                ret.Append(", ");
            }

            ret.Append(value);
        }

        return ret.ToString();
    }

    public static string ToSafeText(this string str)
    {
        Regex wordFilter = new(BADWORDS);
        return wordFilter.Replace(str, "@%#$&!");
    }

    public static int IntParseFast(this string value)
    {
        var result = value[0] == 45 ? 0 : value[0] - 48;
        for (var i = 1; i < value.Length; i++)
        {
            result = (10 * result) + (value[i] - 48);
        }

        return value[0] == 45 ? -result : result;
    }

    public static byte ByteParseFast(this string value)
    {
        byte result = 0;
        for (var i = 0; i < value.Length; i++)
        {
            result = (byte)((10 * result) + (value[i] - 48));
        }

        return result;
    }

    public static ushort UShortParseFast(this string value)
    {
        ushort result = 0;
        for (var i = 0; i < value.Length; i++)
        {
            result = (ushort)((10 * result) + (value[i] - 48));
        }

        return result;
    }

    public static ulong ULongParseFast(this string value)
    {
        ulong result = 0;
        for (var i = 0; i < value.Length; i++)
        {
            result = (10 * result) + (ulong)(value[i] - 48);
        }

        return result;
    }

    public static string ByteArrayToString(this byte[] arr)
    {
        StringBuilder ret = new();
        for (var i = 0; i < arr.Length; i++)
        {
            var value = arr[i];
            ret.Append(value < 10 ? $"00{value}" : value < 100 ? $"0{value}" : value.ToString());
        }

        return ret.ToString();
    }

    public static byte[] StringToByteArray(this string str)
    {
        var ret = new byte[str.Length / 3];

        for (var i = 0; i < str.Length; i += 3)
        {
            ret[i / 3] = Convert.ToByte(str[i].ToString() + str[i + 1].ToString() + str[i + 2].ToString());
        }

        return ret;
    }

    public static Stream GenerateStream(this string s)
    {
        var stream = new MemoryStream();
        var writer = new StreamWriter(stream);
        writer.Write(s);
        writer.Flush();
        stream.Position = 0;
        return stream;
    }
}