﻿namespace RoGCore.Utils;

public static class Utils
{
    public static void Swap<T>(ref T a, ref T b)
    {
        (b, a) = (a, b);
    }

    public static int ToUnixTimestamp(this DateTime dt)
    {
        return (int)(dt.ToUniversalTime() - new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;
    }

    public static T Exec<T>(this Task<T> task)
    {
        task.Wait();
        return task.Result;
    }

    public static byte[] RandomBytes(int len)
    {
        var arr = new byte[len];
        Random r = new();
        r.NextBytes(arr);
        return arr;
    }

    public static T RandomEnumValue<T>(int seed = 0)
    {
        var rand = seed == 0 ? new Random() : new Random(seed);
        var v = Enum.GetValues(typeof(T));
        return (T)v.GetValue(rand.Next(v.Length))!;
    }

    public static byte ConvertBoolArrayToByte(bool[] source)
    {
        byte result = 0;

        // This assumes the array never contains more than 8 elements!
        var index = 8 - source.Length;

        // Loop through the array
        foreach (var b in source)
        {
            // if the element is 'true' set the bit at that position
            if (b)
            {
                result |= (byte)(1 << (7 - index));
            }

            index++;
        }

        return result;
    }

    public static bool[] ConvertByteToBoolArrayReversed(byte b)
    {
        // prepare the return result
        var result = new bool[8];

        // check each bit in the byte. if 1 set to true, if 0 set to false
        for (var i = 0; i < 8; i++)
        {
            result[i] = (b & (1 << i)) != 0;
        }

        return result;
    }

    public class ConstrainedAverage
    {
        private readonly List<double> entries = new();

        private readonly int maxEntries;

        private readonly bool calculateNull;

        public ConstrainedAverage(int maxEntries, bool calculateNull = false)
        {
            this.maxEntries = maxEntries;
            this.calculateNull = calculateNull;
        }

        public int EntryCount => entries.Count;

        public void AddEntry(double entry)
        {
            if (entries.Count >= maxEntries)
            {
                entries.RemoveAt(0);
            }

            entries.Add(entry);
        }

        public double Get()
        {
            double all = 0;
            for (var i = 0; i < entries.Count; i++)
            {
                all += entries[i];
            }

            return all / (calculateNull ? maxEntries : entries.Count);
        }
    }
}