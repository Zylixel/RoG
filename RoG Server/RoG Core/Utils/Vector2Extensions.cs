﻿using System.Runtime.CompilerServices;
using RoGCore.Models;
using UnityEngine;
using Vector2 = System.Numerics.Vector2;
using Vector3 = UnityEngine.Vector3;

namespace RoGCore.Utils;

public static class Vector2Extensions
{
    public static readonly Vector2 NegativeOne = new(-1, -1);
    public static readonly Vector2 Half = new(0.5f, 0.5f);

    public static Vector2 Read(this NReader rdr)
    {
        return new()
        {
            X = rdr.ReadSingle(),
            Y = rdr.ReadSingle(),
        };
    }

    public static void Write(this Vector2 v, NWriter wtr)
    {
        wtr.Write(v.X);
        wtr.Write(v.Y);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector2 Convert(this UnityEngine.Vector2 vector)
    {
        return new(vector.x, vector.y);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector2 Convert(this Vector3 vector)
    {
        return new(vector.x, vector.y);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector3 ToUnity3(this Vector2 v)
    {
        return new(v.X, v.Y, 0);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static UnityEngine.Vector2 ToUnity2(this Vector2 v)
    {
        return new(v.X, v.Y);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Models.Vector2Int ToIntFloor(this Vector2 v)
    {
        return new(Mathf.FloorToInt(v.X), Mathf.FloorToInt(v.Y));
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Models.Vector2Int ToIntCeil(this Vector2 v)
    {
        return new(Mathf.CeilToInt(v.X), Mathf.CeilToInt(v.Y));
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Models.Vector2Int ToInt(this Vector2 v)
    {
        return new((int)v.X, (int)v.Y);
    }

    public static IEnumerable<Models.Vector2Int> Collision(this Vector2 v)
    {
        var x = (int)v.X;
        var y = (int)v.Y;

        yield return new Models.Vector2Int(x, y);

        var xFrac = x - v.X;
        var yFrac = y - v.Y;

        if (xFrac < 0.5)
        {
            yield return new Models.Vector2Int(x - 1, y);

            if (yFrac < 0.5)
            {
                yield return new Models.Vector2Int(x, y - 1);
                yield return new Models.Vector2Int(x - 1, y - 1);
            }
            else if (yFrac > 0.5)
            {
                yield return new Models.Vector2Int(x, y + 1);
                yield return new Models.Vector2Int(x - 1, y + 1);
            }
        }
        else if (xFrac > 0.5)
        {
            yield return new Models.Vector2Int(x + 1, y);

            if (yFrac < 0.5)
            {
                yield return new Models.Vector2Int(x, y - 1);
                yield return new Models.Vector2Int(x + 1, y - 1);
            }
            else if (yFrac > 0.5)
            {
                yield return new Models.Vector2Int(x, y + 1);
                yield return new Models.Vector2Int(x + 1, y + 1);
            }
        }
        else if (yFrac < 0.5)
        {
            yield return new Models.Vector2Int(x, y - 1);
        }
        else if (yFrac > 0.5)
        {
            yield return new Models.Vector2Int(x, y + 1);
        }
    }
}