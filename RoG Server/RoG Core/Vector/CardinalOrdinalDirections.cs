﻿using RoGCore.Models;

namespace RoGCore.Vector;

public static class CardinalOrdinalDirections
{
    [ThreadStatic]
    private static IntVector2S? cardinalDirections;

    [ThreadStatic]
    private static IntVector2S? nineByNineArea;

    [ThreadStatic]
    private static IntVector2S? surroundingArea;

    public enum Direction : byte
    {
        Center,
        Left,
        Right,
        BottomLeft,
        Bottom,
        BottomRight,
        TopLeft,
        Top,
        TopRight,
    }

    public static IntVector2S CardinalDirections
    {
        get
        {
            if (cardinalDirections is null)
            {
                IntVector2S localCardinalDirections = new(4)
                {
                    [0] = new Vector2Int(1, 0),
                    [1] = new Vector2Int(-1, 0),
                    [2] = new Vector2Int(0, 1),
                    [3] = new Vector2Int(0, -1),
                };
                cardinalDirections = localCardinalDirections;
            }

            return cardinalDirections.Value;
        }
    }

    public static IntVector2S NineByNineArea
    {
        get
        {
            if (nineByNineArea is null)
            {
                IntVector2S localNineByNineArea = new(9)
                {
                    [0] = new Vector2Int(0, 0),
                    [1] = new Vector2Int(-1, 0),
                    [2] = new Vector2Int(1, 0),
                    [3] = new Vector2Int(-1, -1),
                    [4] = new Vector2Int(0, -1),
                    [5] = new Vector2Int(1, -1),
                    [6] = new Vector2Int(-1, 1),
                    [7] = new Vector2Int(0, 1),
                    [8] = new Vector2Int(1, 1),
                };
                nineByNineArea = localNineByNineArea;
            }

            return nineByNineArea.Value;
        }
    }

    public static IntVector2S SurroundingArea
    {
        get
        {
            if (surroundingArea is null)
            {
                IntVector2S localSurroundingArea = new(8)
                {
                    [0] = new Vector2Int(-1, 0),
                    [1] = new Vector2Int(1, 0),
                    [2] = new Vector2Int(-1, -1),
                    [3] = new Vector2Int(0, -1),
                    [4] = new Vector2Int(1, -1),
                    [5] = new Vector2Int(-1, 1),
                    [6] = new Vector2Int(0, 1),
                    [7] = new Vector2Int(1, 1),
                };
                surroundingArea = localSurroundingArea;
            }

            return surroundingArea.Value;
        }
    }
}