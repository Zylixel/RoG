﻿using System.Collections;
using System.Runtime.CompilerServices;
using RoGCore.Models;

namespace RoGCore.Vector;

public struct IntVector2S : IEnumerable<Vector2Int>
{
    public int[] X;
    public int[] Y;

    public IntVector2S(int count)
    {
        Count = count;
        X = new int[count];
        Y = new int[count];
    }

    public int Count { get; private set; }

    public Vector2Int this[int i]
    {
        get => new(X[i], Y[i]);
        set
        {
            X[i] = value.X;
            Y[i] = value.Y;
        }
    }

    public void Resize(int newSize)
    {
        Count = newSize;
        Array.Resize(ref X, newSize);
        Array.Resize(ref Y, newSize);
    }

    public IntVector2S GetCopy()
    {
        IntVector2S ret = new(Count);

        X.CopyTo(ret.X, 0);
        Y.CopyTo(ret.Y, 0);

        return ret;
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public IEnumerator<Vector2Int> GetEnumerator()
    {
        for (var i = 0; i < Count; i++)
        {
            yield return this[i];
        }
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void Add(Vector2Int b, ref IntVector2S output)
    {
        X.Add(b.X, ref output.X);
        Y.Add(b.Y, ref output.Y);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void Add(IntVector2S b, ref IntVector2S output)
    {
        X.Add(b.X, ref output.X);
        Y.Add(b.Y, ref output.Y);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void Multiply(Vector2Int b, ref IntVector2S output)
    {
        X.Multiply(b.X, ref output.X);
        Y.Multiply(b.Y, ref output.Y);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void Multiply(float b, ref IntVector2S output)
    {
        X.Multiply(b, ref output.X);
        Y.Multiply(b, ref output.Y);
    }
}