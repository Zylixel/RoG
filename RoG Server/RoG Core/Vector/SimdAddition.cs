﻿namespace RoGCore.Vector;

public static class SimdAddition
{
    public static void Add(this int[] a, int[] b, ref int[] output)
    {
        var simdLength = System.Numerics.Vector<int>.Count;
        System.Numerics.Vector<int> vectorA;
        System.Numerics.Vector<int> vectorB;

        var i = 0;
        for (; i < a.Length - simdLength; i += simdLength)
        {
            vectorA = new System.Numerics.Vector<int>(a, i);
            vectorB = new System.Numerics.Vector<int>(b, i);
            (vectorA + vectorB).CopyTo(output, i);
        }

        for (; i < a.Length; i++)
        {
            output[i] = a[i] + b[i];
        }
    }

    public static void Add(this float[] a, float[] b, ref float[] output)
    {
        var simdLength = System.Numerics.Vector<float>.Count;
        System.Numerics.Vector<float> vectorA;
        System.Numerics.Vector<float> vectorB;

        var i = 0;
        for (; i < a.Length - simdLength; i += simdLength)
        {
            vectorA = new System.Numerics.Vector<float>(a, i);
            vectorB = new System.Numerics.Vector<float>(b, i);
            (vectorA + vectorB).CopyTo(output, i);
        }

        for (; i < a.Length; i++)
        {
            output[i] = a[i] + b[i];
        }
    }

    public static void Add(this int[] a, int b, ref int[] output)
    {
        var simdLength = System.Numerics.Vector<int>.Count;
        System.Numerics.Vector<int> vectorA;
        System.Numerics.Vector<int> vectorB = new(b);

        var i = 0;
        for (; i < a.Length - simdLength; i += simdLength)
        {
            vectorA = new System.Numerics.Vector<int>(a, i);
            (vectorA + vectorB).CopyTo(output, i);
        }

        for (; i < a.Length; i++)
        {
            output[i] = a[i] + b;
        }
    }

    public static void Add(this float[] a, float b, ref float[] output)
    {
        var simdLength = System.Numerics.Vector<float>.Count;
        System.Numerics.Vector<float> vectorA;
        System.Numerics.Vector<float> vectorB = new(b);

        var i = 0;
        for (; i < a.Length - simdLength; i += simdLength)
        {
            vectorA = new System.Numerics.Vector<float>(a, i);
            (vectorA + vectorB).CopyTo(output, i);
        }

        for (; i < a.Length; i++)
        {
            output[i] = a[i] + b;
        }
    }
}