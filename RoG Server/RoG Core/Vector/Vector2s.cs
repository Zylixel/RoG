﻿using System.Collections;
using System.Numerics;
using System.Runtime.CompilerServices;

namespace RoGCore.Vector;

public struct Vector2S : IEnumerable<Vector2>
{
    public float[] X;
    public float[] Y;

    public Vector2S(int count)
    {
        Count = count;
        X = new float[count];
        Y = new float[count];
    }

    public int Count { get; private set; }

    public Vector2 this[int i]
    {
        get => new(X[i], Y[i]);
        set
        {
            X[i] = value.X;
            Y[i] = value.Y;
        }
    }

    public void Resize(int newSize)
    {
        Count = newSize;
        Array.Resize(ref X, newSize);
        Array.Resize(ref Y, newSize);
    }

    public Vector2S GetCopy()
    {
        Vector2S ret = new(Count);

        X.CopyTo(ret.X, 0);
        Y.CopyTo(ret.Y, 0);

        return ret;
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public IEnumerator<Vector2> GetEnumerator()
    {
        for (var i = 0; i < Count; i++)
        {
            yield return this[i];
        }
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void Add(Vector2 b, ref Vector2S output)
    {
        X.Add(b.X, ref output.X);
        Y.Add(b.Y, ref output.Y);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void Add(Vector2S b, ref Vector2S output)
    {
        X.Add(b.X, ref output.X);
        Y.Add(b.Y, ref output.Y);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void Multiply(Vector2 b, ref Vector2S output)
    {
        X.Multiply(b.X, ref output.X);
        Y.Multiply(b.Y, ref output.Y);
    }
}