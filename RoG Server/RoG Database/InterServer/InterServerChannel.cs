﻿using System.Buffers;
using System.Buffers.Binary;
using System.Diagnostics;
using Database.InterServer.Messages;
using Database.Models;
using Microsoft.Extensions.Logging;
using RoGCore.Models;
using RoGCore.Networking;
using StackExchange.Redis;

namespace Database.InterServer;

public abstract class InterServerChannel
{
    public static readonly RedisChannel COMMANDSTRING = RedisChannel.Literal("command");
    public static readonly RedisChannel NETWORK = RedisChannel.Literal("network");
    public static readonly RedisChannel CHAT = RedisChannel.Literal("chat");

    protected readonly Guid InstanceId;

    private const int MaxMessageLength = 1000;

    private readonly ISubscriber subscriber;

    private readonly MemoryStream publishStream = new(new byte[MaxMessageLength]);
    private readonly MemoryStream subscriptionStream = new(new byte[MaxMessageLength]);

    protected InterServerChannel(ILogger<InterServerChannel> _logger)
    {
        subscriber = RedisDatabase.Db.Multiplexer.GetSubscriber();
        InstanceId = Guid.NewGuid();

        _logger.LogInformation("InterServerChannel ID is {0}", InstanceId);
    }

    public enum NetworkType : byte
    {
        API,
        WorldServer,
    }

    public void Publish<T>(RedisChannel channel, in IStreamableObject<T> val)
        where T : struct
    {
        Publish(channel, val, Guid.Empty);
    }

    public void Publish<T>(RedisChannel channel, in IStreamableObject<T> val, in Guid target)
        where T : struct
    {
        const int headerSize = sizeof(ushort);

        Message<T> message = new(InstanceId, target, val);

        publishStream.Position = 0;
        publishStream.SetLength(0);
        var writer = new NWriter(publishStream);
        message.Write(writer);

        var arrayPool = ArrayPool<byte>.Shared;
        var dataLength = (ushort)publishStream.Length;
        var arr = arrayPool.Rent(dataLength + headerSize);

        WriteHeader(arr, dataLength);
        publishStream.Position = 0;
        publishStream.Read(arr, headerSize, dataLength + headerSize);

        RedisDatabase.Db.Publish(channel, arr, CommandFlags.FireAndForget);

        arrayPool.Return(arr);
    }

    public void AddHandler<T>(RedisChannel channel, EventHandler<InterServerEventArgs<T>> handler)
        where T : struct
    {
        // Multithreaded
        subscriber.Subscribe(
            channel,
            (_, buff) =>
            {
                lock (subscriptionStream)
                {
                    var buffer = (byte[]?)buff;

                    if (buffer is null)
                    {
                        return;
                    }

                    var size = ReadHeader(buffer);

                    // Write the buffer into a memorystream to be deserialized
                    subscriptionStream.Position = 0;
                    subscriptionStream.Write(buffer, 2, size);
                    subscriptionStream.Position = 0;
                    subscriptionStream.SetLength(size);

                    // Deserialize the incoming message
                    var reader = new NReader(subscriptionStream);
                    var message = new Message<T>(reader);

                    if (message.Sender == InstanceId
                        || (message.Target != Guid.Empty && message.Target != InstanceId))
                    {
                        return;
                    }

                    handler(this, new InterServerEventArgs<T>(message.Sender, (T)message.Content));
                }
            },
            CommandFlags.FireAndForget);
    }

    private static void WriteHeader(byte[] input, in ushort value)
    {
        Debug.Assert(input.Length > 1, "header is not greater than 1 byte");

        Span<byte> buffer = stackalloc byte[sizeof(ushort)];
        BinaryPrimitives.WriteUInt16LittleEndian(buffer, value);
        input[0] = buffer[0];
        input[1] = buffer[1];
    }

    private static ushort ReadHeader(byte[] input)
    {
        Debug.Assert(input.Length > 1, "header is not greater than 1 byte");

        return BinaryPrimitives.ReadUInt16LittleEndian(input);
    }
}