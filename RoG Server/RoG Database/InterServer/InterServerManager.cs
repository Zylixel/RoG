﻿using Database.InterServer.Messages;
using Database.Models;
using Microsoft.Extensions.Logging;
using static Database.InterServer.NetworkMsg;

namespace Database.InterServer;

public abstract class InterServerManager : InterServerChannel, IDisposable
{
    protected readonly Dictionary<Guid, ServerInstance> availableInstance = [];

    protected readonly string name;
    protected readonly NetworkType type;
    protected readonly ILogger logger;

    private readonly System.Timers.Timer tickTimer = new(1000);

    protected InterServerManager(
        ILogger<InterServerManager> logger,
        in NetworkType type,
        string name)
        : base(logger)
    {
        this.logger = logger;
        this.name = name;
        this.type = type;

        AddHandler<NetworkMsg>(NETWORK, HandleNetwork);
        AddHandler<Chat>(CHAT, HandleChat);

        Publish(NETWORK, new NetworkMsg(NetworkCode.Join, type, name));
    }

    public void Dispose()
    {
        tickTimer.Stop();
        tickTimer.Dispose();

        Publish(NETWORK, GenerateNetworkMessage(NetworkCode.Quit));
        GC.SuppressFinalize(this);
    }

    public void Run()
    {
        tickTimer.Elapsed += (_, _) => Tick();
        tickTimer.Start();
    }

    protected virtual void InstanceConnected(ServerInstance serverInstance)
    {
        availableInstance[serverInstance.Id] = serverInstance;
        logger.LogInformation("Server {0} connected to the network.", serverInstance);
    }

    protected virtual void InstanceDisconnected(in Guid instanceId)
    {
        availableInstance.Remove(instanceId, out var serverInstance);
        logger.LogInformation("Server {0} disconnected from the network.", serverInstance);
    }

    protected virtual void HandleNetwork(object? sender, InterServerEventArgs<NetworkMsg> e)
    {
        switch (e.Content.Code)
        {
            case NetworkCode.Join:
                InstanceConnected(new ServerInstance(e.InstanceId, e.Content.Type));
                break;
            case NetworkCode.Ping:
                if (!availableInstance.TryGetValue(e.InstanceId, out var instance))
                {
                    InstanceConnected(new ServerInstance(e.InstanceId, e.Content.Type));
                }
                else
                {
                    instance.OnPing();
                }

                break;
            case NetworkCode.Quit:
                InstanceDisconnected(e.InstanceId);
                break;
        }
    }

    protected virtual void HandleChat(object? sender, InterServerEventArgs<Chat> e)
    {
        switch (e.Content.Type)
        {
            case Chat.ChatType.Tell:
                {
                    var from = RedisDatabase.ResolveIgn(e.Content.From);
                    var to = RedisDatabase.ResolveIgn(e.Content.To);
                    logger.LogInformation("<{0} -> {1}> {2}", from, to, e.Content.Text);
                }

                break;
            case Chat.ChatType.Guild:
                {
                    var from = RedisDatabase.ResolveIgn(e.Content.From);
                    logger.LogInformation("<{0} -> Guild> {2}", from, e.Content.Text);
                }

                break;
            case Chat.ChatType.Announce:
                logger.LogInformation("<Announcement> {0}", e.Content.Text);

                break;
        }
    }

    protected virtual NetworkMsg GenerateNetworkMessage(in NetworkCode code)
    {
        return new NetworkMsg(code, type, name);
    }

    private void Tick()
    {
        Publish(NETWORK, GenerateNetworkMessage(NetworkCode.Ping));

        var enumerator = availableInstance.GetEnumerator();

        while (enumerator.MoveNext())
        {
            var instance = enumerator.Current;

            if (instance.Value.IsTimedOut())
            {
                InstanceDisconnected(instance.Key);
            }
        }
    }
}
