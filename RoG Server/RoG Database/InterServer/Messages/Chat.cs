﻿using RoGCore.Models;
using RoGCore.Networking;

namespace Database.InterServer.Messages;

public readonly struct Chat : IStreamableObject<Chat>
{
    public readonly ChatType Type;

    public readonly string ServerName;

    public readonly int ObjId;

    public readonly int Stars;

    public readonly string From;

    public readonly string To;

    public readonly string Text;

    public Chat(
        ChatType type,
        string serverName,
        int objId,
        int stars,
        string from,
        string to,
        string text)
    {
        Type = type;
        ServerName = serverName;
        ObjId = objId;
        Stars = stars;
        From = from;
        To = to;
        Text = text;
    }

    public Chat(NReader rdr)
        : this(
            (ChatType)rdr.ReadByte(),
            rdr.ReadString(),
            rdr.ReadInt32(),
            rdr.ReadInt32(),
            rdr.ReadString(),
            rdr.ReadString(),
            rdr.ReadString())
    {
    }

    public enum ChatType : byte
    {
        Tell,
        Guild,
        Announce,
    }

    public Chat Read(NReader rdr)
    {
        return new Chat(rdr);
    }

    public void Write(NWriter wtr)
    {
        wtr.Write((byte)Type);
        wtr.Write(ServerName);
        wtr.Write(ObjId);
        wtr.Write(Stars);
        wtr.Write(From);
        wtr.Write(To);
        wtr.Write(Text);
    }
}
