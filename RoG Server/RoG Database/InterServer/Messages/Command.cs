﻿using RoGCore.Models;
using RoGCore.Networking;

namespace Database.InterServer.Messages;

public readonly struct Command(in Command.NetworkCommand networkMessage) : IStreamableObject<Command>
{
    public readonly NetworkCommand NetworkMessage = networkMessage;

    public enum NetworkCommand : byte
    {
        Stop,
    }

    public Command Read(NReader rdr)
    {
        return new Command((NetworkCommand)rdr.ReadByte());
    }

    public void Write(NWriter wtr)
    {
        wtr.Write((byte)NetworkMessage);
    }
}
