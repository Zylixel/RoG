﻿using RoGCore.Models;
using RoGCore.Networking;

namespace Database.InterServer.Messages;

public readonly struct Message<T> : IStreamableObject<Message<T>>
{
    public readonly Guid Sender;

    public readonly Guid Target;

    public readonly IStreamableObject<T> Content;

    public Message(in Guid instId, in Guid targetInst, in IStreamableObject<T> content)
    {
        Sender = instId;
        Target = targetInst;
        Content = content;
    }

    public Message(NReader rdr)
    {
        Sender = new Guid(rdr.ReadBytes(16));
        Target = new Guid(rdr.ReadBytes(16));

        var contentObj = ((IStreamableObject<T>?)default(T))
            ?? throw new ArgumentException($"{nameof(T)} is not an IInterServerMessage");

        Content = (IStreamableObject<T>)contentObj.Read(rdr)!;
    }

    public Message<T> Read(NReader rdr)
    {
        return new Message<T>(rdr);
    }

    public void Write(NWriter wtr)
    {
        wtr.Write(Sender.ToByteArray());
        wtr.Write(Target.ToByteArray());
        Content.Write(wtr);
    }
}