﻿using RoGCore.Models;
using RoGCore.Networking;
using static Database.InterServer.InterServerChannel;

namespace Database.InterServer;

public readonly struct NetworkMsg : IStreamableObject<NetworkMsg>
{
    public readonly NetworkCode Code;

    public readonly NetworkType Type;

    public readonly string Name;

    public readonly GameServer? ServerItem;

    public NetworkMsg(in NetworkCode code, in NetworkType type, string name, GameServer? serverItem = null)
    {
        Code = code;
        Type = type;
        Name = name;
        ServerItem = serverItem;
    }

    public enum NetworkCode : byte
    {
        Join,
        Ping,
        Quit,
    }

    public override string ToString()
    {
        return $"[{Code}, {Type}, {Name}, {ServerItem}]";
    }

    public NetworkMsg Read(NReader rdr)
    {
        var code = (NetworkCode)rdr.ReadByte();
        var type = (NetworkType)rdr.ReadByte();
        var name = rdr.ReadString();
        GameServer? serverItem = rdr.ReadBoolean() ? new GameServer(rdr) : null;
        return new NetworkMsg(code, type, name, serverItem);
    }

    public void Write(NWriter wtr)
    {
        wtr.Write((byte)Code);
        wtr.Write((byte)Type);
        wtr.Write(Name);

        wtr.Write(ServerItem is not null);
        ServerItem?.Write(wtr);
    }
}
