﻿using static Database.InterServer.InterServerChannel;

namespace Database.InterServer;

public sealed class ServerInstance(in Guid id, NetworkType networkType)
{
    public readonly Guid Id = id;

    private DateTime lastPing = DateTime.UtcNow;

    public void OnPing()
    {
        lastPing = DateTime.UtcNow;
    }

    public bool IsTimedOut()
    {
        var currentTime = DateTime.UtcNow;
        var difference = currentTime - lastPing;
        return difference > TimeSpan.FromSeconds(5);
    }

    public override string ToString()
    {
        return $"\"{Id}\" ({networkType})";
    }
}
