﻿namespace Database.Models;

public enum AddGuildMemberStatus
{
    Ok,
    AlreadyInGuild,
    InAnotherGuild,
    IsAMember,
    GuildFull,
    Error,
}