﻿using RoGCore.Models;

namespace Database.Models;

public sealed class DbAccount(int accId) : RedisObject($"account.{accId}")
{
    public int AccountId { get; } = accId;

    public string Name
    {
        get => GetValue<string>("name");
        set => SetValue("name", value);
    }

    public int Rank
    {
        get => GetValue("rank", 0);
        set => SetValue("rank", value);
    }

    public string GuildName
    {
        get => GetValue<string>("guildName");
        set => SetValue("guildName", value);
    }

    public int GuildId
    {
        get => GetValue<int>("guildId");
        set => SetValue("guildId", value);
    }

    public GuildRank GuildRank
    {
        get => (GuildRank)GetValue<int>("guildRank");
        set => SetValue("guildRank", (int)value);
    }

    public DateTime RegTime
    {
        get => GetValue<DateTime>("regTime");
        set => SetValue("regTime", value);
    }

    public int Silver
    {
        get => GetValue("silver", RedisDatabase.Settings.GameServer.StartingSilver);
        set => SetValue("silver", value);
    }

    public int NextCharId
    {
        get => GetValue<int>("nextCharId");
        set => SetValue("nextCharId", value);
    }

    public int VaultSlots
    {
        get => GetValue("vaultSlots", RedisDatabase.Settings.GameServer.StartingVaults);
        set => SetValue("vaultSlots", value);
    }

    public List<RawItem> Vault
    {
        get => RawItem.ToNonNullList(RawItem.Read(GetValue<byte[]>("vault")));
        internal set => SetValue("vault", RawItem.Write(value, true, true));
    }

    public bool Muted
    {
        get => GetValue("muted", false);
        set => SetValue("muted", value);
    }

    public bool Banned
    {
        get => GetValue("banned", false);
        set => SetValue("banned", value);
    }

    public int[] Locked
    {
        get => GetValue<int[]>("locked");
        set => SetValue("locked", value);
    }

    public int[] Ignored
    {
        get => GetValue<int[]>("ignored");
        set => SetValue("ignored", value);
    }

    public RGB Color
    {
        get => new(GetValue<int>("color"));
        set => SetValue("color", value);
    }

    public PlayerSkills Skills
    {
        get => new(GetValue("skills", Array.Empty<byte>()));
        set => SetValue("skills", value.Write());
    }

    public string Password
    {
        get => GetValue<string>("password");
        set => SetValue("password", value);
    }

    internal string? LockToken { get; set; }
}