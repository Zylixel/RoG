﻿using Newtonsoft.Json;
using RoGCore.Models;

namespace Database.Models;

public class DbChar : RedisObject
{
    internal DbChar(DbAccount acc, ushort characterId)
        : base($"char.{acc.AccountId}.{characterId}", false)
    {
        Account = acc;
        CharacterId = characterId;
    }

    [JsonIgnore]
    public DbAccount Account { get; }

    public ushort CharacterId { get; }

    public string Class
    {
        get => GetValue<string>("charType");
        set => SetValue("charType", value);
    }

    public RawItem?[] Items
    {
        get => RawItem.Read(GetValue<byte[]>("items"));
        set => SetValue("items", RawItem.Write(value, true, true));
    }

    public int[] Stats
    {
        get => Array.ConvertAll(GetValue<int[]>("stats"), x => x);
        set => SetValue("stats", Array.ConvertAll(value, x => x));
    }

    public int Tex1
    {
        get => GetValue("tex1", -1);
        set => SetValue("tex1", value);
    }

    public int Tex2
    {
        get => GetValue("tex2", -1);
        set => SetValue("tex2", value);
    }

    public int Skin
    {
        get => GetValue("skin", -1);
        set => SetValue("skin", value);
    }

    public DateTime CreateTime
    {
        get => GetValue("createTime", DateTime.Now);
        set => SetValue("createTime", value);
    }

    public DateTime LastSeen
    {
        get => GetValue("lastSeen", DateTime.Now);
        set => SetValue("lastSeen", value);
    }

    public SkillTree SkillTree
    {
        get => new(GetValue<byte[]>("skillTree"), this);
        set => SetValue("skillTree", value.Data);
    }

    internal static async Task<DbChar?> CreateAsync(DbAccount acc, ushort charId)
    {
        DbChar ret = new(acc, charId);
        await ret.ReloadFieldsAsync();
        return ret.IsNull ? null : ret;
    }
}