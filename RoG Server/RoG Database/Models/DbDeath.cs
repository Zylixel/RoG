﻿namespace Database.Models;

public class DbDeath(DbAccount acc, int charId) : RedisObject($"death.{acc.AccountId}.{charId}")
{
    public DbAccount Account { get; } = acc;

    public int CharId { get; } = charId;

    public ushort ObjectType
    {
        get => GetValue<ushort>("objType");
        set => SetValue("objType", value);
    }

    public int Level
    {
        get => GetValue<int>("level");
        set => SetValue("level", value);
    }

    public int TotalFame
    {
        get => GetValue<int>("totalFame");
        set => SetValue("totalFame", value);
    }

    public string Killer
    {
        get => GetValue<string>("killer");
        set => SetValue("killer", value);
    }

    public DateTime DeathTime
    {
        get => GetValue<DateTime>("deathTime");
        set => SetValue("deathTime", value);
    }
}