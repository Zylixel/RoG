﻿namespace Database.Models;

public class DbGuild : RedisObject
{
    public DbGuild(DbAccount acc)
        : base($"guild.{acc.GuildId}")
    {
        Id = acc.GuildId;
    }

    internal DbGuild(int id)
        : base($"guild.{id}")
    {
        Id = id;
    }

    public int Id { get; }

    public string Name
    {
        get => GetValue<string>("name");
        set => SetValue("name", value);
    }

    public int[] Members
    {
        get => GetValue<int[]>("members");
        set => SetValue("members", value);
    }

    public int WarId
    {
        get => GetValue<int>("warId");
        set => SetValue("warId", value);
    }
}