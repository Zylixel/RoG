﻿namespace Database.Models;

public class DbGuildWar : RedisObject
{
    internal DbGuildWar(int id)
        : base($"guildWar.{id}")
    {
        Id = id;
    }

    public int Id { get; }

    public int ChallengingGuild
    {
        get => GetValue<int>("challengingGuild");
        set => SetValue("challengingGuild", value);
    }

    public int ChallengedGuild
    {
        get => GetValue<int>("challengedGuild");
        set => SetValue("challengedGuild", value);
    }

    public int ChallengingGuildPoints
    {
        get => GetValue<int>("challengingGuildPoints");
        set => SetValue("challengingGuildPoints", value);
    }

    public int ChallengedGuildPoints
    {
        get => GetValue<int>("challengedGuildPoints");
        set => SetValue("challengedGuildPoints", value);
    }

    public DateTime Expiration
    {
        get => GetValue<DateTime>("expireTime");
        set => SetValue("expireTime", value);
    }
}