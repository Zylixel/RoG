﻿namespace Database.Models;

public enum GuildCreateStatus
{
    Ok,
    UsedName,
    InvalidName,
}