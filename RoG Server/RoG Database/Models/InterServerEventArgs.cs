﻿namespace Database.Models;

public class InterServerEventArgs<T>(in Guid instId, T val) : EventArgs
{
    public Guid InstanceId { get; } = instId;

    public T Content { get; } = val;
}