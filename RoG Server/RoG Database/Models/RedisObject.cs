﻿using System.Text;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RoGCore.Logging;
using RoGCore.Utils;
using StackExchange.Redis;

namespace Database.Models;

public abstract class RedisObject
{
    private static readonly ILogger<RedisObject> Logger = LogFactory.LoggingInstance<RedisObject>();

    // Note do not modify returning buffer
    private Dictionary<string, byte[]?> fields = [];

    protected RedisObject(string key, bool reloadImmediate = true)
    {
        Key = key;

        if (reloadImmediate)
        {
            ReloadFields();
        }
    }

    [JsonIgnore]
    public string Key { get; }

    [JsonIgnore]
    public bool IsNull => fields.Count == 0;

    public void Flush()
    {
        var converted = new HashEntry[fields.Count];

        for (var i = 0; i < fields.Count; i++)
        {
            var field = fields.ElementAt(i);
            converted[i] = new HashEntry(field.Key, field.Value);
        }

        RedisDatabase.Db.HashSet(Key, converted, CommandFlags.FireAndForget);
    }

    public void Flush(IDatabaseAsync? conn = null)
    {
        var converted = new HashEntry[fields.Count];

        for (var i = 0; i < fields.Count; i++)
        {
            var field = fields.ElementAt(i);
            converted[i] = new HashEntry(field.Key, field.Value);
        }

        (conn ?? RedisDatabase.Db).HashSetAsync(Key, converted, CommandFlags.FireAndForget);
    }

    public async Task FlushAsync(IDatabaseAsync? conn = null)
    {
        var converted = new HashEntry[fields.Count];

        for (var i = 0; i < fields.Count; i++)
        {
            var field = fields.ElementAt(i);
            converted[i] = new HashEntry(field.Key, field.Value);
        }

        await (conn ?? RedisDatabase.Db).HashSetAsync(Key, converted, CommandFlags.FireAndForget);
    }

    public void ReloadFields()
    {
        fields = RedisDatabase.Db.HashGetAll(Key).ToDictionary(x => x.Name.ToString(), x => (byte[]?)x.Value);
    }

    public async Task ReloadFieldsAsync()
    {
        fields = (await RedisDatabase.Db.HashGetAllAsync(Key)).ToDictionary(x => x.Name.ToString(), x => (byte[]?)x.Value);
    }

#pragma warning disable CS8601 // Possible null reference assignment.
    protected T GetValue<T>(string key, T def = default)
#pragma warning restore CS8601 // Possible null reference assignment.
        where T : notnull
    {
        if (!fields.TryGetValue(key, out var val) || val is null)
        {
            return def;
        }

        var type = typeof(T);
        if (type == typeof(int))
        {
            return (T)(object)Encoding.UTF8.GetString(val).IntParseFast();
        }

        if (type == typeof(ushort))
        {
            return (T)(object)Encoding.UTF8.GetString(val).UShortParseFast();
        }

        if (type == typeof(ulong))
        {
            return (T)(object)Encoding.UTF8.GetString(val).ULongParseFast();
        }

        if (type == typeof(bool))
        {
            return (T)(object)(val[0] != 0);
        }

        if (type == typeof(DateTime))
        {
            return (T)(object)DateTime.FromBinary(BitConverter.ToInt64(val, 0));
        }

        if (type == typeof(byte[]))
        {
            return (T)(object)val;
        }

        if (type == typeof(short[]))
        {
            return (T)(object)BitConverter.ToInt16(val, 0);
        }

        if (type == typeof(ushort[]))
        {
            var ret = new ushort[val.Length / 2];
            Buffer.BlockCopy(val, 0, ret, 0, val.Length);
            return (T)(object)ret;
        }

        if (type == typeof(int[]))
        {
            var ret = new int[val.Length / 4];
            Buffer.BlockCopy(val, 0, ret, 0, val.Length);
            return (T)(object)ret;
        }

        return type == typeof(string) ? (T)(object)Encoding.UTF8.GetString(val) : throw new NotSupportedException();
    }

    protected void SetValue<T>(string key, T val)
    {
        if (val is null)
        {
            Logger.LogError($"Error setting database key '{key}'. Value was null");
            return;
        }

        byte[] buff;
        if (typeof(T) == typeof(int) || typeof(T) == typeof(ushort) ||
            typeof(T) == typeof(string) || typeof(T) == typeof(ulong))
        {
            var stringVal = val.ToString();

            if (stringVal is null)
            {
                Logger.LogError($"Error setting database key '{key}'. Value was null");
                return;
            }

            buff = Encoding.UTF8.GetBytes(stringVal);
        }
        else if (typeof(T) == typeof(bool))
        {
            buff = [(bool)(object)val ? (byte)1 : (byte)0];
        }
        else if (typeof(T) == typeof(DateTime))
        {
            buff = BitConverter.GetBytes(((DateTime)(object)val).ToBinary());
        }
        else if (typeof(T) == typeof(byte[]))
        {
            buff = (byte[])(object)val;
        }
        else if (typeof(T) == typeof(ushort[]))
        {
            var v = (ushort[])(object)val;
            buff = new byte[v.Length * 2];
            Buffer.BlockCopy(v, 0, buff, 0, buff.Length);
        }
        else if (typeof(T) == typeof(int[]))
        {
            var v = (int[])(object)val;
            buff = new byte[v.Length * 4];
            Buffer.BlockCopy(v, 0, buff, 0, buff.Length);
        }
        else
        {
            throw new NotSupportedException();
        }

        fields[key] = buff;
    }
}