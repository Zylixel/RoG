﻿using System.Collections.Frozen;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RoGCore.Assets;
using RoGCore.Game;
using RoGCore.Logging;
using RoGCore.Models;

namespace Database.Models;

public sealed class SkillTree
{
    private static readonly ILogger<SkillTree> Logger = LogFactory.LoggingInstance<SkillTree>();

    private readonly DbChar character;

    private byte[]? data;

    public SkillTree(byte[]? bytes, DbChar @char)
    {
        character = @char;

        if (ValidateAndSet(bytes))
        {
            return;
        }
    }

    public static FrozenDictionary<SkillType, Skill> SkillLookup { get; private set; } = new Dictionary<SkillType, Skill>(new SkillTypeComparer()).ToFrozenDictionary();

    public FrozenSet<Skill> CurrentSkills { get; private set; } = new HashSet<Skill>().ToFrozenSet();

    public byte[] Data
    {
        get
        {
            if (data is not null)
            {
                return data;
            }

            MemoryStream ret = new();
            using (NWriter wtr = new(ret))
            {
                foreach (var skill in CurrentSkills)
                {
                    wtr.Write((ushort)skill.Type);
                }
            }

            data = ret.ToArray();
            return data;
        }
    }

    public static void Initialize()
    {
        // create skills
        var skills = JsonConvert.DeserializeObject<List<Skill>>(EmbeddedData.SkillFile);
        if (skills is null)
        {
            Logger.LogError("Error loading skills from skill file");
            return;
        }

        // populate lookup dict
        var lookupDictionary = new Dictionary<SkillType, Skill>(new SkillTypeComparer());
        foreach (var skill in skills)
        {
            lookupDictionary.TryAdd(skill.Type, skill);
        }

        SkillLookup = lookupDictionary.ToFrozenDictionary();

        // resolve skill network
        foreach (var skill in skills)
        {
            if (skill.Requirements is null)
            {
                continue;
            }

            for (var i = 0; i < skill.Requirements.Length; i++)
            {
                SkillLookup[skill.Requirements[i]].Next.Add(skill);
            }
        }

        Logger.LogResults(
            $"Skill{(SkillLookup.Count > 1 ? "s" : string.Empty)}",
            SkillLookup.Count);
    }

    public bool ValidateAndSet(byte[]? bytes)
    {
        var skills = Read(bytes);
        var totalCost = 0;
        foreach (var skill in skills)
        {
            if (!skill.Usable(character.Class))
            {
                return false;
            }

            if (skill.Requirements is not null && skill.Requirements.Length > 0)
            {
                var contains = false;
                for (var i = 0; i < skill.Requirements.Length; i++)
                {
                    if (skills.Contains(SkillLookup[skill.Requirements[i]]))
                    {
                        contains = true;
                    }
                }

                if (!contains)
                {
                    return false;
                }
            }

            if (skill.Incompatibilities is not null && skill.Incompatibilities.Length > 0)
            {
                var contains = false;
                for (var i = 0; i < skill.Incompatibilities.Length; i++)
                {
                    if (skills.Contains(SkillLookup[skill.Incompatibilities[i]]))
                    {
                        contains = true;
                    }
                }

                if (contains)
                {
                    return false;
                }
            }

            totalCost += skill.SkillPointCost;
        }

        if (totalCost > SkillTreePointCalculator.AvailablePoints(character.Account.Skills))
        {
            return false;
        }

        CurrentSkills = skills.ToFrozenSet();
        data = bytes;
        return true;
    }

    private static HashSet<Skill> Read(byte[]? bytes)
    {
        HashSet<Skill> ret = [];
        if (bytes is null)
        {
            return ret;
        }

        try
        {
            NReader reader = new(new MemoryStream(bytes));
            while (reader.BaseStream.Position != reader.BaseStream.Length)
            {
                var type = (SkillType)reader.ReadUInt16();
                if (SkillLookup.TryGetValue(type, out var skill))
                {
                    ret.Add(skill);
                }
                else
                {
                    Logger.LogWarning($"Failed to load skill, skillType: {type}");
                }
            }
        }
        catch
        {
            Logger.LogError("Failed to load skills into character, emptying skill tree");
            return [];
        }

        return ret;
    }
}