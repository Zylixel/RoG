﻿using RoGCore.Models;

namespace Database.Models;

internal sealed class SkillTypeComparer : IEqualityComparer<SkillType>
{
    public bool Equals(SkillType x, SkillType y)
    {
        return x == y;
    }

    public int GetHashCode(SkillType x)
    {
        return (int)x;
    }
}