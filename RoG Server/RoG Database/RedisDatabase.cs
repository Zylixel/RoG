﻿using Database.Models;
using RoGCore.Assets;
using RoGCore.Models;
using RoGCore.SettingsModels;
using StackExchange.Redis;

namespace Database;

public static class RedisDatabase
{
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
    public static ConnectionMultiplexer Multiplexer { get; private set; }

    internal static IDatabase Db { get; private set; }

    internal static AppSettings Settings { get; private set; }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

    public static async Task BuildAsync(AppSettings settings)
    {
        Settings = settings;

        ConfigurationOptions options = new()
        {
            SyncTimeout = Settings.Database.SyncTimeout,
            Password = Settings.Database.Password,
            EndPoints = { { Settings.Database.Host, Settings.Database.Port } },
        };

        Multiplexer = await ConnectionMultiplexer.ConnectAsync(options);
        Db = Multiplexer.GetDatabase();
    }

    public static void UpdateSilver(DbAccount acc, int amount)
    {
        acc.Silver += amount;
        acc.Flush();
        acc.ReloadFields();
    }

    public static async Task<DbChar?> LoadCharacterAsync(DbAccount acc, ushort charId)
    {
        return await DbChar.CreateAsync(acc, charId);
    }

    public static void LockAccount(DbAccount acc, int lockId)
    {
        var x = acc.Locked.ToList();
        if (!x.Remove(lockId))
        {
            x.Add(lockId);
            acc.Locked = [.. x];
        }

        acc.Locked = [.. x];
        acc.Flush();
        acc.ReloadFields();
    }

    public static void IgnoreAccount(DbAccount acc, int ignoreId)
    {
        var x = acc.Ignored.ToList();
        if (!x.Remove(ignoreId))
        {
            x.Add(ignoreId);
            acc.Ignored = [.. x];
        }

        acc.Ignored = [.. x];
        acc.Flush();
        acc.ReloadFields();
    }

    public static AddGuildMemberStatus AddGuildMember(DbGuild guild, DbAccount acc, bool founder = false)
    {
        if (acc.GuildId == guild.Id)
        {
            return AddGuildMemberStatus.AlreadyInGuild;
        }

        if (acc.GuildId > 0)
        {
            return AddGuildMemberStatus.InAnotherGuild;
        }

        const int guildSize = 100;
        if (guild.Members.Length >= guildSize)
        {
            return AddGuildMemberStatus.GuildFull;
        }

        var members = guild.Members.ToList();
        if (members.Contains(acc.AccountId))
        {
            return AddGuildMemberStatus.IsAMember;
        }

        members.Add(acc.AccountId);
        guild.Members = [.. members];
        guild.Flush();
        acc.GuildName = guild.Name;
        acc.GuildId = guild.Id;
        acc.GuildRank = founder ? GuildRank.Founder : GuildRank.Initiate;
        acc.Flush();
        return AddGuildMemberStatus.Ok;
    }

    public static bool AddVault(DbAccount acc, RawItem item)
    {
        if (acc.Vault.Count >= acc.VaultSlots)
        {
            return false;
        }

        var g = acc.Vault;
        g.Add(item);
        acc.Vault = g;
        acc.Flush();
        acc.ReloadFields();
        return true;
    }

    public static bool RemoveVault(DbAccount acc, RawItem item)
    {
        var newVault = acc.Vault.Where(x => !x.LenientEquals(item)).ToList();

        // No item would be removed
        if (newVault.Count == acc.Vault.Count)
        {
            return false;
        }

        acc.Vault = newVault;
        acc.Flush();
        acc.ReloadFields();
        return true;
    }

    public static bool ChangeGuildRank(DbAccount acc, GuildRank rank)
    {
        if (acc.GuildId <= 0)
        {
            return false;
        }

        acc.GuildRank = rank;
        acc.Flush();
        return true;
    }

    public static void BanIp(string ip)
    {
        Db.HashSetAsync("IPs", ip, true, flags: CommandFlags.FireAndForget);
    }

    public static async Task<bool> GetIPBanned(string ip)
    {
        return (bool)await Db.HashGetAsync("IPs", ip);
    }

    public static int GetAccountLength()
    {
        return Convert.ToInt32(Db.StringGet("nextAccId"));
    }

    public static async Task<DbAccount> CreateAccount(string username, string password)
    {
        var newAccId = (int)Db.StringIncrement("nextAccId");
        DbAccount acc = new(newAccId)
        {
            Name = username,
            Rank = 1,
            GuildName = string.Empty,
            GuildId = -1,
            GuildRank = 0,
            VaultSlots = Settings.GameServer.StartingVaults,
            RegTime = DateTime.Now,
            Silver = Settings.GameServer.StartingSilver,
            Muted = false,
            Banned = false,
            Locked = [0],
            Ignored = [0],
            Password = password,
        };
        await acc.FlushAsync();

        await Db.HashSetAsync(
            "names",
            acc.Name.ToUpperInvariant(),
            acc.AccountId.ToString(),
            flags: CommandFlags.FireAndForget);

        return acc;
    }

    public static async Task<DbAccount?> ReadAccount(string username)
    {
        if ((await Db.HashGetAsync("names", username.ToUpperInvariant())).TryParse(out int id))
        {
            return GetAccount(id);
        }

        return null;
    }

    public static DbAccount? GetAccount(int id)
    {
        DbAccount ret = new(id);
        if (ret.IsNull)
        {
            return null;
        }

        return ret;
    }

    public static async Task<DbGuild?> GetGuild(string name)
    {
        if ((await Db.HashGetAsync("guilds", name.ToUpperInvariant())).TryParse(out int id))
        {
            return GetGuild(id);
        }

        return null;
    }

    public static async Task<string?> ResolveIgn(string accId)
    {
        return await Db.HashGetAsync($"account.{accId}", "name");
    }

    public static async Task<IEnumerable<DbChar>> GetAliveCharacters(DbAccount acc)
    {
        var characterIds = (await Db.SetMembersAsync($"alive.{acc.AccountId}"))
            .Select(i => BitConverter.ToUInt16(i!, 0));

        return (await Task.WhenAll(characterIds.Select(async characterId =>
            await LoadCharacterAsync(acc, characterId)))).OfType<DbChar>();
    }

    public static async Task<DbChar> CreateCharacter(
        DbAccount acc,
        ObjectDescription desc,
        int skin)
    {
        await Db.SetLengthAsync($"alive.{acc.AccountId}");

        acc.NextCharId++;
        await acc.FlushAsync();
        await acc.ReloadFieldsAsync();

        var startingEquipLoaded = desc.Equipment is null
            ? (new RawItem?[4 + 28])
            : desc.Equipment.Select(item => item == 0 ? null : new RawItem { ObjectType = item }).ToArray();

        Array.Resize(ref startingEquipLoaded, 4 + 28);

        DbChar character = new(acc, (ushort)acc.NextCharId)
        {
            Class = desc.Name,
            Items = startingEquipLoaded,
            Stats =
            [
                (short)(desc.PlayerHitPoint?[0] ?? 0),
                (short)(desc.PlayerMagicPoint?[0] ?? 0),
                (short)(desc.PlayerAttack?[0] ?? 0),
                (short)(desc.PlayerDefense?[0] ?? 0),
                (short)(desc.PlayerSpeed?[0] ?? 0),
                (short)(desc.PlayerDexterity?[0] ?? 0),
                (short)(desc.PlayerVitality?[0] ?? 0),
                (short)(desc.PlayerWisdom?[0] ?? 0),
            ],
            Tex1 = -1,
            Tex2 = -1,
            Skin = skin,
            CreateTime = DateTime.Now,
            LastSeen = DateTime.Now,
        };
        await character.FlushAsync();

        await Db.SetAddAsync(
            $"alive.{acc.AccountId}",
            BitConverter.GetBytes((ushort)acc.NextCharId),
            CommandFlags.FireAndForget);

        return character;
    }

    public static async Task<DbChar?> LoadCharacter(int accId, ushort charId)
    {
        DbAccount acc = new(accId);
        return await LoadCharacterAsync(acc, charId);
    }

    public static async Task<bool> SaveCharacter(DbAccount acc, DbChar character, bool lockAcc)
    {
        var trans = Db.CreateTransaction();
        if (lockAcc)
        {
            trans.AddCondition(Condition.StringEqual($"lock.{acc.AccountId}", acc.LockToken));
        }

        await character.FlushAsync(trans);
        return trans.Execute();
    }

    public static void MuteAccount(DbAccount acc)
    {
        Db.HashSet(acc.Key, "muted", "1", flags: CommandFlags.FireAndForget);
        acc.Flush();
        acc.ReloadFields();
    }

    public static void UnmuteAccount(DbAccount acc)
    {
        Db.HashSet(acc.Key, "muted", "0", flags: CommandFlags.FireAndForget);
        acc.Flush();
        acc.ReloadFields();
    }

    public static void BanAccount(DbAccount acc)
    {
        Db.HashSet(acc.Key, "banned", "1", flags: CommandFlags.FireAndForget);
        acc.Flush();
        acc.ReloadFields();
    }

    public static GuildCreateStatus CreateGuild(string guildName, out DbGuild guild)
    {
        guild = new DbGuild(-1);
        if (string.IsNullOrWhiteSpace(guildName))
        {
            return GuildCreateStatus.InvalidName;
        }

        guildName = guildName.Trim();
        var newGuildId = (int)Db.StringIncrement("newGuildId");
        if (!Db.HashSet("guilds", guildName.ToUpperInvariant(), Convert.ToString(newGuildId), When.NotExists))
        {
            return GuildCreateStatus.UsedName;
        }

        guild = new DbGuild(newGuildId)
        {
            Name = guildName,
            Members = [],
        };
        guild.Flush();
        return GuildCreateStatus.Ok;
    }

    public static DbGuild? GetGuild(int id)
    {
        DbGuild ret = new(id);
        return ret.IsNull ? null : ret;
    }

    public static void RemoveFromGuild(DbAccount acc)
    {
        var guild = GetGuild(acc.GuildId);
        if (guild is null)
        {
            return;
        }

        var members = guild.Members.ToList();
        if (members.Contains(acc.AccountId))
        {
            members.Remove(acc.AccountId);
            guild.Members = [.. members];
            guild.Flush();
        }

        if (members.Count == 0)
        {
            Db.HashDelete("guilds", guild.Name.ToUpperInvariant(), CommandFlags.FireAndForget);
        }

        acc.GuildName = string.Empty;
        acc.GuildId = -1;
        acc.GuildRank = GuildRank.Initiate;
        acc.Flush();
    }

    public static DbGuildWar? GetGuildWar(int id)
    {
        DbGuildWar ret = new(id);
        return ret.IsNull ? null : ret;
    }

    public static bool AddGuildWarPoints(DbAccount acc, int changeBy)
    {
        var guildId = acc.GuildId;
        if (guildId == 0)
        {
            return false;
        }

        var guild = GetGuild(guildId);
        if (guild is null)
        {
            return false;
        }

        var war = GetGuildWar(guild.WarId);
        if (war is null)
        {
            return false;
        }

        if (war.ChallengedGuild == guild.Id)
        {
            war.ChallengedGuildPoints += changeBy;
        }
        else
        {
            war.ChallengingGuildPoints += changeBy;
        }

        war.Flush();
        return true;
    }

    public static void AddGuildWarReq(string from, string to)
    {
        Db.HashSet(
"guildWarRequests",
to.ToUpperInvariant(),
from.ToUpperInvariant(),
flags: CommandFlags.FireAndForget);
    }

    public static void RemGuildWarReq(string to)
    {
        Db.HashDelete("guildWarRequests", to.ToUpperInvariant(), CommandFlags.FireAndForget);
    }

    public static string FindWarRequest(string guildName)
    {
        return (string?)Db.HashGet("guildWarRequests", guildName.ToUpperInvariant()) ?? "-1";
    }

    public static void AcceptGuildWarReq(DbGuild challengingGuild, DbGuild challengedGuild)
    {
        var id = (int)Db.StringIncrement("nextGuildWarId");
        challengingGuild.WarId = id;
        challengedGuild.WarId = id;
        challengingGuild.Flush();
        challengedGuild.Flush();
        RemGuildWarReq(challengedGuild.Name);
        DbGuildWar guildWar = new(id)
        {
            ChallengingGuild = challengingGuild.Id,
            ChallengingGuildPoints = 0,
            ChallengedGuild = challengedGuild.Id,
            ChallengedGuildPoints = 0,
            Expiration = DateTime.Now.AddMinutes(5),
        };
        guildWar.Flush();
    }

    public static void EndGuildWar(DbGuildWar war)
    {
        var guild1 = GetGuild(war.ChallengedGuild);
        if (guild1 != null)
        {
            guild1.WarId = 0;
            guild1.Flush();
        }

        var guild2 = GetGuild(war.ChallengingGuild);
        if (guild2 != null)
        {
            guild2.WarId = 0;
            guild2.Flush();
        }

        Db.KeyDelete($"guildWar.{war.Id}", CommandFlags.FireAndForget);
    }
}
