﻿using System.Numerics;
using System.Runtime.CompilerServices;
using GameServer.Logic.Behaviors.Storage;
using GameServer.Logic.Engine;
using GameServer.Realm;
using GameServer.Realm.Entities;
using GameServer.Realm.Entities.Enemy;
using GameServer.Realm.World;
using GameServer.Realm.World.Worlds;
using RoGCore.Models;

namespace GameServer.Logic.Behaviors;

internal class AdvancedWander : StatefulCycleBehavior<AdvancedWanderStorage>
{
    private readonly float speed;
    private readonly RandomRange radiusFromSpawn;
    private readonly float minDistance;
    private readonly Cooldown cooldown;

    internal AdvancedWander(float speed, in RandomRange radiusFromSpawn, float minDistance = 0, Cooldown? cooldown = null)
    {
        this.speed = speed;
        this.radiusFromSpawn = radiusFromSpawn;
        this.minDistance = minDistance;
        this.cooldown = cooldown ?? new Cooldown(2000, 500);
    }

    internal override void OnStateEntry(Entity host, ref AdvancedWanderStorage? state)
    {
        state = null;
    }

    internal override void TickCore(Entity host, ref AdvancedWanderStorage? state)
    {
        if (host is not Enemy enemy)
        {
            return;
        }

        if (state is null)
        {
            state = new AdvancedWanderStorage();
            FindNewSpot(ref state, enemy);
        }

        Status = CycleStatus.NotStarted;
        if (host.HasConditionEffect(ConditionEffectIndex.Paralyzed))
        {
            return;
        }

        if (state.RemainingCooldown > 0)
        {
            state.RemainingCooldown -= host.Owner.CurrentTime.StaticMspt;
            return;
        }

        Status = CycleStatus.InProgress;

        if (state.Destination == host.Position)
        {
            ResetState(enemy, ref state);
            return;
        }

        var dist = host.GetSpeed(speed);

        var diff = state.Destination - host.Position;
        dist = MathF.Min(dist, diff.Length());

        if (!host.ValidateAndMove(host.Position + (Vector2.Normalize(diff) * dist)) || !AssertPosition(enemy, host.Position))
        {
            ResetState(enemy, ref state);
            return;
        }

        void ResetState(Enemy enemy, ref AdvancedWanderStorage state)
        {
            state.RemainingCooldown = cooldown.Next();
            FindNewSpot(ref state, enemy);
            Status = CycleStatus.Completed;
        }
    }

    // Expensive
    [MethodImpl(MethodImplOptions.AggressiveOptimization)]
    protected virtual bool ValidateDestination(Enemy enemy, Vector2 destination)
    {
        var diff = destination - enemy.Position;

        return diff.Length() >= minDistance && TestRayOccupency(enemy.Owner, enemy.Position, diff);

        static bool TestRayOccupency(World world, Vector2 startPoint, Vector2 diff)
        {
            var normalDiff = Vector2.Normalize(diff);

            for (float i = 1; i <= diff.Length(); i += 0.5f)
            {
                if (!world.RegionUnblocked(startPoint + (normalDiff * i), TileOccupency.NoWalk | TileOccupency.OccupyFull))
                {
                    return false;
                }
            }

            return true;
        }
    }

    protected virtual bool AssertPosition(Enemy enemy, Vector2 newPosition)
    {
        return true;
    }

    // Expensive
    [MethodImpl(MethodImplOptions.AggressiveOptimization)]
    private void FindNewSpot(ref AdvancedWanderStorage state, Enemy enemy)
    {
        var attemps = 0;
        const int maxAttempts = 100;

        while (attemps < maxAttempts)
        {
            attemps++;

            var radius = Random.Shared.Next(radiusFromSpawn);
            var angle = Angle.RandomAngle(Random.Shared);

            state.Destination = enemy.SpawnPoint + (new Vector2(angle.Sin, angle.Cos) * radius);

            if (ValidateDestination(enemy, state.Destination))
            {
                return;
            }
        }
    }
}