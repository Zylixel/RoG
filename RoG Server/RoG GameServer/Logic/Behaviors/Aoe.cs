﻿using System.Numerics;
using GameServer.Logic.Engine;
using GameServer.Realm.Entities;
using GameServer.Realm.Entities.Enemy;
using GameServer.Realm.Entities.Player;
using RoGCore.Models;
using RoGCore.Networking.Packets;

namespace GameServer.Logic.Behaviors;

internal class Aoe(float radius, bool players, int minDamage, int maxDamage, bool noDef, uint color) : Behavior
{
    private readonly ARGB color = new ARGB(color);

    internal override void OnStateEntry(Entity host)
    {
        Vector2 pos = new()
        {
            X = host.X,
            Y = host.Y,
        };
        var damage = Random.Shared.Next(minDamage, maxDamage);
        if (players)
        {
            host.Owner.Aoe<Player>(pos, radius, player =>
            {
                player.Damage(null, damage, noDef, out _);
                host.Owner.BroadcastPacket(new ShowEffect(ShowEffect.Type.Nova, host.Id, color, new Vector2(radius, 0)));
            });
        }
        else
        {
            host.Owner.Aoe<Enemy>(pos, radius, enemy =>
            {
                enemy.Damage(null, damage, noDef, out _);
                host.Owner.BroadcastPacket(new ShowEffect(ShowEffect.Type.Nova, host.Id, color, new Vector2(radius, 0)));
            });
        }
    }
}