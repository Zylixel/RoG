﻿using System.Numerics;
using System.Runtime.CompilerServices;
using GameServer.Logic.Engine;
using GameServer.Realm.Entities.Enemy;
using GameServer.Realm.Entities.Player;
using GameServer.Realm.World.Worlds;
using RoGCore.Models;

namespace GameServer.Logic.Behaviors;

internal class AvoidWander : AdvancedWander
{
    private readonly float raypointWidth;
    private readonly float halfRaypointWidth;

    internal AvoidWander(float speed, float raypointWidth, in RandomRange radiusFromSpawn, float minDistance = 0, Cooldown? cooldown = null)
        : base(speed, radiusFromSpawn, minDistance, cooldown)
    {
        this.raypointWidth = raypointWidth;
        halfRaypointWidth = raypointWidth * 0.5f;
    }

    // Expensive
    [MethodImpl(MethodImplOptions.AggressiveOptimization)]
    protected override bool ValidateDestination(Enemy enemy, Vector2 destination)
    {
        var diff = destination - enemy.Position;

        var normalDiff = Vector2.Normalize(diff);

        for (float i = 1; i <= diff.Length(); i += 0.5f)
        {
            var raypoint = enemy.Position + (normalDiff * i);

            if (enemy.Owner.EntitiesInRectangle(new Rectangle(raypoint.X - halfRaypointWidth, raypoint.Y - halfRaypointWidth, raypointWidth, raypointWidth)).OfType<Player>().Any())
            {
                return false;
            }
        }

        return base.ValidateDestination(enemy, destination);
    }
}