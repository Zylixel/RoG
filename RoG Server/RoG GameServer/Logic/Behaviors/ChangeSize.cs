﻿using GameServer.Logic.Engine;
using GameServer.Realm.Entities;

namespace GameServer.Logic.Behaviors;

internal class ChangeSize(int rate, int target) : StatefulBehavior<float>
{
    internal override void TickCore(Entity host, ref float cool)
    {
        if (cool <= 0)
        {
            if (host.Size != target)
            {
                host.Size += rate;
                if ((rate > 0 && host.Size > target) ||
                    (rate < 0 && host.Size < target))
                {
                    host.Size = target;
                }
            }

            cool = 100f;
        }
        else
        {
            cool -= host.Owner.CurrentTime.StaticMspt;
        }
    }
}