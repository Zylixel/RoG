﻿using GameServer.Realm.Entities;

namespace GameServer.Logic.Behaviors.Condition;

internal abstract class Condition
{
    internal abstract bool Invoke(Entity host);
}