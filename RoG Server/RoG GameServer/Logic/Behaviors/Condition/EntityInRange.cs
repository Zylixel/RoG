﻿using GameServer.Realm;
using GameServer.Realm.Entities;

namespace GameServer.Logic.Behaviors.Condition;

internal class EntityInRange(string entityName, float distance) : Condition
{
    internal override bool Invoke(Entity host)
    {
        return host.InRange<Entity>(distance, EntityUtils.NamePredicate(entityName)).Any();
    }
}