﻿using GameServer.Realm;
using GameServer.Realm.Entities;

namespace GameServer.Logic.Behaviors.Condition;

internal class GroupInRange(float distance, params string[] targetGroups) : Condition
{
    protected readonly string[] TargetGroups = targetGroups;

    internal override bool Invoke(Entity host)
    {
        return host.InRange<Entity>(distance, EntityUtils.GroupsPredicate(TargetGroups)).Any();
    }
}