﻿using GameServer.Logic.Engine;
using GameServer.Realm.Entities;

namespace GameServer.Logic.Behaviors.Condition;

internal sealed class If(Condition predicate, params IChildren[] trueChildren) : StatefulBehavior<bool?>
{
    private IChildren[]? falseChildren;
    private IEnumerable<IChildren> allChildren = trueChildren;

    internal override void Resolve(State parent, Dictionary<string, State> states)
    {
        foreach (var child in allChildren)
        {
            child.Resolve(parent, states);
        }
    }

    internal override void OnStateEntry(Entity host, ref bool? state)
    {
        foreach (var child in allChildren)
        {
            child.OnStateEntry(host);
        }

        state = null;
    }

    internal override void TickCore(Entity host, ref bool? state)
    {
        var lastValue = state;

        if (predicate.Invoke(host))
        {
            if (lastValue == false)
            {
                for (var i = 0; i < trueChildren.Length; i++)
                {
                    trueChildren[i].OnStateEntry(host);
                }
            }

            for (var i = 0; i < trueChildren.Length; i++)
            {
                trueChildren[i].Tick(host);
            }

            state = true;
        }
        else if (falseChildren is not null)
        {
            if (lastValue == true)
            {
                for (var i = 0; i < falseChildren.Length; i++)
                {
                    falseChildren[i].OnStateEntry(host);
                }
            }

            for (var i = 0; i < falseChildren.Length; i++)
            {
                falseChildren[i].Tick(host);
            }

            state = false;
        }
    }

    internal If Else(params IChildren[] falseChildren)
    {
        this.falseChildren = falseChildren;

        allChildren = allChildren.Concat(falseChildren);

        return this;
    }
}