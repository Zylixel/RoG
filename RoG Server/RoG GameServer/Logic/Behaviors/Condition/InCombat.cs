﻿using GameServer.Realm.Entities;

namespace GameServer.Logic.Behaviors.Condition;

internal class InCombat : Condition
{
    // State storage: none
    internal override bool Invoke(Entity host)
    {
        return host.InCombat();
    }
}