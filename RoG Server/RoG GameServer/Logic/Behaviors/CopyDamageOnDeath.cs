﻿using GameServer.Logic.Engine;
using GameServer.Realm;
using GameServer.Realm.Entities.Enemy;

namespace GameServer.Logic.Behaviors;

internal class CopyDamageOnDeath(string child, float dist = 50) : Behavior
{
    internal override void Resolve(State parent, Dictionary<string, State> states)
    {
        parent.Death +=
            (sender, e) =>
            {
                if (e.Host is Enemy host)
                {
                    var en = e.Host.GetNearest<Enemy>(dist, EntityUtils.NamePredicate(child));
                    en?.SetDamageCounter(host.DamageCounter, en);
                }
            };
    }
}