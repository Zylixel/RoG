﻿using GameServer.Logic.Engine;
using GameServer.Realm.Entities;
using RoGCore.Models;

namespace GameServer.Logic.Behaviors;

internal class DepthsRemoveLight : Behavior
{
    internal override void OnStateEntry(Entity host)
    {
        foreach (var tile in host.Owner.GetTiles(new Rectangle(0, 0, host.Owner.Map.Width, host.Owner.Map.Height)))
        {
            if (tile is null)
            {
                continue;
            }

            if (!(tile.ObjDesc?.LightIntensity > 0))
            {
                continue;
            }

            tile.ObjDesc = null;
        }
    }
}