﻿using GameServer.Logic.Engine;
using GameServer.Realm.Entities;
using GameServer.Realm.Entities.GameObject;
using GameServer.Realm.World;
using Microsoft.Extensions.Logging;
using RoGCore.Logging;

namespace GameServer.Logic.Behaviors;

internal class DropPortal : Behavior
{
    private static readonly ILogger<DropPortal> Logger = LogFactory.LoggingInstance<DropPortal>();

    private readonly int despawnTime;
    private readonly int dropDelay;
    private readonly int percent;
    private readonly string stringObjType;
    private readonly float xAdjustment;
    private readonly float yAdjustment;

    public DropPortal(
        ushort objType,
        int percent,
        int dropDelaySec = 0,
        float xAdjustment = 0,
        float yAdjustment = 0,
        int portalDespawnTimeSec = 30)
    {
        stringObjType = objType.ToString();
        this.percent = percent;
        this.xAdjustment = xAdjustment;
        this.yAdjustment = yAdjustment;
        dropDelay = dropDelaySec;
        despawnTime = portalDespawnTimeSec;
    }

    public DropPortal(
        string objType,
        int percent,
        int dropDelaySec = 0,
        float xAdjustment = 0,
        float yAdjustment = 0,
        int portalDespawnTimeSec = 30)
    {
        stringObjType = objType;
        this.percent = percent;
        this.xAdjustment = xAdjustment;
        this.yAdjustment = yAdjustment;
        dropDelay = dropDelaySec;
        despawnTime = portalDespawnTimeSec;
    }

    internal override void OnStateEntry(Entity host)
    {
        if (host.Owner.Name == "Arena")
        {
            return;
        }

        if (Random.Shared.Next(1, 100) <= percent)
        {
            // TODO: Reimplement dropDelay
            if (Entity.Resolve(stringObjType, host.Owner) is not Portal portal)
            {
                return;
            }

            var world = host.Owner;
            portal.Move(host.X + xAdjustment, host.Y + yAdjustment);

            // world.Timers.Add(new WorldTimer(dropDelay * 1000, (_, _) => world.EnterWorld(portal)));
            world.Timers.Add(new WorldTimer(despawnTime * 1000, (_, _) =>
            {
                try
                {
                    world.LeaveWorld(portal);
                }
                catch (Exception ex)
                {
                    Logger.LogError($"Couldn't despawn portal.\n{ex}");
                }
            }));
        }
    }
}