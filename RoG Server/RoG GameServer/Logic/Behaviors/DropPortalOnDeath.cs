﻿using GameServer.Logic.Engine;
using GameServer.Realm.Entities;
using GameServer.Realm.Entities.GameObject;
using GameServer.Realm.World;

namespace GameServer.Logic.Behaviors;

internal class DropPortalOnDeath(
    string objId,
    int percent,
    int dropDelaySec = 0,
    float xAdjustment = 0,
    float yAdjustment = 0,
    int portalDespawnTimeSec = 30) : Behavior
{
    internal override void Resolve(State parent, Dictionary<string, State> states)
    {
        parent.Death += (_, e) =>
        {
            if (Random.Shared.Next(1, 101) > percent)
            {
                return;
            }

            if (Entity.Resolve(objId, e.Host.Owner) is not Portal portal)
            {
                return;
            }

            // TODO: Reimplement dropDelay
            portal.Move(e.Host.X + xAdjustment, e.Host.Y + yAdjustment);

            // w.Timers.Add(new WorldTimer(dropDelay * 1000, (_, _) => { w.EnterWorld(portal); }));
            e.Host.Owner.Timers.Add(new WorldTimer(portalDespawnTimeSec * 1000, (_, _) => e.Host.Owner.LeaveWorld(portal)));
        };
    }
}