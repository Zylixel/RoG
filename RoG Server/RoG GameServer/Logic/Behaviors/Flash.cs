﻿using System.Numerics;
using GameServer.Logic.Engine;
using GameServer.Realm.Entities;
using RoGCore.Models;
using RoGCore.Networking.Packets;

namespace GameServer.Logic.Behaviors;

internal class Flash(ARGB color, float flashRate, int flashLength) : Behavior
{
    internal override void OnStateEntry(Entity host)
    {
        host.Owner.BroadcastPacket(new ShowEffect(ShowEffect.Type.Flash, host.Id, color, new Vector2(flashRate, flashLength)), host.Position);
    }
}