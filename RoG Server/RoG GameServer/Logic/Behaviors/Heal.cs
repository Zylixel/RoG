using GameServer.Logic.Engine;
using GameServer.Realm;
using GameServer.Realm.Entities;
using GameServer.Realm.Entities.Enemy;
using RoGCore.Models;
using RoGCore.Networking.Packets;
using RoGCore.Utils;

namespace GameServer.Logic.Behaviors;

internal class Heal : StatefulBehavior<float?>
{
    // TODO: Frozen hashset
    private readonly HashSet<string> targetGroups;
    private readonly int amount;
    private readonly float range;
    private readonly Cooldown coolDown;

    internal Heal(float range, int amount, string[] targetGroups, Cooldown coolDown = default)
    {
        this.range = range;
        this.amount = amount;
        this.targetGroups = new HashSet<string>(targetGroups.Length);
        for (var i = 0; i < targetGroups.Length; i++)
        {
            this.targetGroups.Add(targetGroups[i]);
        }

        this.coolDown = coolDown;
    }

    internal override void TickCore(Entity host, ref float? cool)
    {
        cool ??= 0;
        if (cool <= 0)
        {
            if (targetGroups.Contains("Self"))
            {
                HealSelf(host);
            }
            else if (targetGroups.Contains("Other"))
            {
                HealGroup(host, host.InRange<Enemy>(range));
            }
            else
            {
                HealGroup(host, host.InRange<Enemy>(range, EntityUtils.GroupsPredicate(targetGroups)));
            }

            cool = coolDown.Next();
        }
        else
        {
            cool -= host.Owner.CurrentTime.StaticMspt;
        }
    }

    private void HealSelf(Entity host)
    {
        if (host is not Enemy enemy)
        {
            return;
        }

        var newHp = (amount + enemy.Hp).Clamp(0, enemy.MaximumHp);
        if (newHp == enemy.Hp)
        {
            return;
        }

        enemy.Owner.BroadcastPacket(new ShowEffect(ShowEffect.Type.Heal, enemy.Id));
        enemy.Owner.BroadcastPacket(new Notification(host.Id, $"+{newHp - enemy.Hp}", new ARGB(0xff00ff00)));

        enemy.Hp = newHp;
    }

    private void HealGroup(Entity host, IEnumerable<Enemy> group)
    {
        foreach (var enemy in group)
        {
            var newHp = (amount + enemy.Hp).Clamp(0, enemy.MaximumHp);
            if (newHp == enemy.Hp)
            {
                continue;
            }

            enemy.Owner.BroadcastPacket(new ShowEffect(ShowEffect.Type.Heal, enemy.Id));
            enemy.Owner.BroadcastPacket(new ShowEffect(ShowEffect.Type.Line, enemy.Id, enemy.Position));
            enemy.Owner.BroadcastPacket(new Notification(host.Id, $"+{newHp - enemy.Hp}", new ARGB(0xff00ff00)));

            enemy.Hp = newHp;
        }
    }
}