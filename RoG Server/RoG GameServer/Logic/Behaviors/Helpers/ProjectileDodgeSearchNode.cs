﻿using System.Numerics;
using GameServer.Realm;
using GameServer.Realm.Entities;

namespace GameServer.Logic.Behaviors.Helpers;

internal class ProjectileDodgeSearchNode
{
    internal ProjectileDodgeSearchNode(ProjectileDodgeSearchNode? parent, Vector2 position)
    {
        Parent = parent;
        Children = [];
        Position = position;
        Depth = parent?.Depth + 1 ?? 0;
        Parent?.Children.Add(this);
    }

    internal Vector2 Position { get; init; }

    internal int Depth { get; init; }

    internal float? SelfValue { get; set; }

    internal IEnumerable<ProjectileDodgeSearchNode>? Siblings => Parent?.Children.Where(x => x != this);

    private ProjectileDodgeSearchNode? Parent { get; init; }

    private List<ProjectileDodgeSearchNode> Children { get; init; }

    internal float CalculateSelfValue(Vector2[] projectiles, Entity host)
    {
        // Check to see how many projeciles would hit the entity.
        // Projectile hit area is increased to compensate for lag or time mismatches.
        float hitCount = projectiles.Count(projectilePosition =>
            Projectile.HittingEntity(Position, host, projectilePosition, 1.25f));
        SelfValue = hitCount;
        return hitCount;
    }
}