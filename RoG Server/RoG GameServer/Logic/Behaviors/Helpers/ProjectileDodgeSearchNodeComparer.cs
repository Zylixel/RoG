﻿namespace GameServer.Logic.Behaviors.Helpers;

internal class ProjectileDodgeSearchNodeComparer : IEqualityComparer<ProjectileDodgeSearchNode>
{
    public bool Equals(ProjectileDodgeSearchNode? x, ProjectileDodgeSearchNode? y)
    {
        return ReferenceEquals(x, y) || (x?.Depth == y?.Depth && x?.Position == y?.Position);
    }

    public int GetHashCode(ProjectileDodgeSearchNode obj)
    {
        return HashCode.Combine(obj.Position, obj.Depth);
    }
}