﻿using GameServer.Logic.Engine;
using GameServer.Realm;
using GameServer.Realm.Entities;
using GameServer.Realm.Entities.Enemy;
using Microsoft.Extensions.Logging;
using RoGCore.Logging;

namespace GameServer.Logic.Behaviors;

internal class HpLessOrder(float dist, float threshold, string children, string targetStateName) : Behavior
{
    private static readonly ILogger<HpLessOrder> Logger = LogFactory.LoggingInstance<HpLessOrder>();

    private State? targetState;

    internal override void TickCore(Entity host)
    {
        if (!CheckHp(host, threshold))
        {
            return;
        }

        targetState ??= FindState(BehaviorDb.Definitions[children].Key, targetStateName);
        if (targetState == null)
        {
            return;
        }

        foreach (var i in host.InRange<Entity>(dist, EntityUtils.NamePredicate(children)))
        {
            if (i.CurrentState?.Is(targetState) != true)
            {
                i.SwitchTo(targetState);
            }
        }
    }

    private static bool CheckHp(Entity host, double threshold)
    {
        if (host is not Enemy enemy)
        {
            Logger.LogWarning($"[HpLessOrder] Host '{host.Name}' is not enemy");
            return false;
        }

        return threshold > 1.0
                ? enemy.Hp < threshold
                : (float)enemy.Hp / enemy.MaximumHp < threshold;
    }
}