﻿using GameServer.Logic.Engine;
using GameServer.Realm.Entities;

namespace GameServer.Logic.Behaviors;

internal class LeaveWorld(int time = 10000) : StatefulBehavior<float>
{
    internal override void OnStateEntry(Entity host, ref float state)
    {
        state = time;
    }

    internal override void TickCore(Entity host, ref float cool)
    {
        if (cool <= 0)
        {
            host.Owner.LeaveWorld(host);
        }
        else
        {
            cool -= host.Owner.CurrentTime.StaticMspt;
        }
    }
}