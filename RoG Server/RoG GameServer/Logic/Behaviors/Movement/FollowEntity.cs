﻿using System.Numerics;
using GameServer.Logic.Behaviors.Storage;
using GameServer.Logic.Behaviors.TargetProvider;
using GameServer.Logic.Engine;
using GameServer.Realm.Entities;
using RoGCore.Models;

namespace GameServer.Logic.Behaviors.Movement;

internal class FollowEntity(
        float speed,
        ITargetProvider targetProvider,
        float distance = 6,
        Cooldown? attentionSpan = null,
        bool variance = true) : StatefulCycleBehavior<FollowStorage>
{
    private readonly Cooldown attentionSpan = attentionSpan ?? new Cooldown(1000, 0);

    internal override void TickCore(Entity host, ref FollowStorage followState)
    {
        Status = CycleStatus.NotStarted;
        if (host.HasConditionEffect(ConditionEffectIndex.Paralyzed))
        {
            return;
        }

        var completed = false;
        switch (followState.State)
        {
            case FollowEnum.DontKnowWhere:
                var entity = targetProvider.GetTarget(host);
                if (entity is not null)
                {
                    followState.State = FollowEnum.Acquired;
                    followState.Following = entity;
                    followState.AttentionLeft = attentionSpan.Next();
                    goto case FollowEnum.Acquired;
                }

                break;
            case FollowEnum.Acquired:
                Status = CycleStatus.InProgress;
                if (followState.AttentionLeft <= 0)
                {
                    completed = true;
                    followState.State = FollowEnum.DontKnowWhere;
                    goto case FollowEnum.DontKnowWhere;
                }

                followState.AttentionLeft -= host.Owner.CurrentTime.StaticMspt;
                var vect = followState.Following!.Position - host.Position;
                var length = vect.Length();
                if (length > distance)
                {
                    if (variance)
                    {
                        vect += new Vector2(Random.Shared.Next(-2, 3), Random.Shared.Next(-2, 3)) * 0.5f;
                    }

                    vect = Vector2.Normalize(vect);
                    var dist = host.HasConditionEffect(ConditionEffectIndex.Confused) ? Random.Shared.Next(0, 4) == 0 ? 1f : -1f : 1f;
                    dist *= host.GetSpeed(speed);
                    dist = Math.Min(dist, length);
                    host.ValidateAndMove(host.Position + (vect * dist));
                }

                break;
        }

        if (completed)
        {
            Status = CycleStatus.Completed;
        }
    }
}