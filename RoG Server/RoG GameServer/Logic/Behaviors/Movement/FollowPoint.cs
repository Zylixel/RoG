﻿using System.Numerics;
using GameServer.Logic.Behaviors.Storage;
using GameServer.Logic.Behaviors.TargetProvider;
using GameServer.Logic.Engine;
using GameServer.Realm.Entities;
using RoGCore.Models;

namespace GameServer.Logic.Behaviors.Movement;

internal class FollowPoint : StatefulCycleBehavior<FollowPointStorage>
{
    private protected readonly ITargetProvider TargetProvider;
    private readonly float range;
    private readonly float speed;
    private readonly Cooldown attentionSpan;
    private readonly bool variance;

    internal FollowPoint(
        float speed,
        ITargetProvider targetProvider,
        float distance = 6,
        Cooldown? attentionSpan = null,
        bool variance = true)
    {
        this.speed = speed;
        TargetProvider = targetProvider;
        range = distance;
        this.attentionSpan = attentionSpan ?? new Cooldown(1000, 0);
        this.variance = variance;
    }

    internal override void TickCore(Entity host, ref FollowPointStorage? state)
    {
        state ??= new FollowPointStorage();
        Status = CycleStatus.NotStarted;
        if (host.HasConditionEffect(ConditionEffectIndex.Paralyzed))
        {
            state = new FollowPointStorage();
            return;
        }

        var completed = false;
        switch (state.State)
        {
            case FollowEnum.DontKnowWhere:
                var point = TargetProvider.GetPosition(host);
                if (point is not null)
                {
                    state.State = FollowEnum.Acquired;
                    state.Following = point;
                    state.AttentionLeft = attentionSpan.Next();
                    goto case FollowEnum.Acquired;
                }

                break;
            case FollowEnum.Acquired:
                Status = CycleStatus.InProgress;
                if (state.Following is null || state.AttentionLeft <= 0)
                {
                    completed = true;
                    state.State = FollowEnum.DontKnowWhere;
                    goto case FollowEnum.DontKnowWhere;
                }

                state.AttentionLeft -= host.Owner.CurrentTime.StaticMspt;
                var vect = state.Following.Value - host.Position;
                var length = vect.Length();
                if (length > range)
                {
                    if (variance)
                    {
                        vect += new Vector2(Random.Shared.Next(-2, 3), Random.Shared.Next(-2, 3)) * 0.5f;
                    }

                    vect = Vector2.Normalize(vect);
                    var dist = host.HasConditionEffect(ConditionEffectIndex.Confused) ? Random.Shared.Next(0, 4) == 0 ? 1f : -1f : 1f;
                    dist *= host.GetSpeed(speed);
                    dist = Math.Min(dist, length);
                    host.ValidateAndMove(host.Position + (vect * dist));
                }

                break;
        }

        if (completed)
        {
            Status = CycleStatus.Completed;
        }
    }
}