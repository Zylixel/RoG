﻿using System.Numerics;
using GameServer.Logic.Behaviors.TargetProvider;
using GameServer.Logic.Engine;
using GameServer.Realm.Entities;
using RoGCore.Models;

namespace GameServer.Logic.Behaviors.Movement;

internal class Goto(float speed, ITargetProvider targetProvider, bool once = true, bool signal = false, float maxDist = 0) : StatefulCycleBehavior<bool>
{
    internal override void TickCore(Entity host, ref bool state)
    {
        var returned = state;
        if (returned && once)
        {
            return;
        }

        var position = targetProvider.GetPosition(host);
        if (position is null || host.HasConditionEffect(ConditionEffectIndex.Paralyzed))
        {
            return;
        }

        var vect = position.Value - host.Position;
        var length = vect.Length();

        if (length <= maxDist)
        {
            if (!returned)
            {
                if (signal)
                {
                    host.InvokeBehaviorEvent(BehaviorSignal.GotoComplete);
                }

                state = true;
            }

            return;
        }

        var dist = host.HasConditionEffect(ConditionEffectIndex.Confused) ? Random.Shared.Next(0, 4) == 0 ? 1 : -1 : 1f;
        dist *= host.GetSpeed(speed);
        dist = Math.Min(dist, length);
        host.ValidateAndMove(host.Position + (Vector2.Normalize(vect) * dist));
    }
}