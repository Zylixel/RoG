﻿using System.Numerics;
using GameServer.Logic.Behaviors.Storage;
using GameServer.Logic.Behaviors.TargetProvider;
using GameServer.Logic.Engine;
using GameServer.Realm.Entities;
using RoGCore.Models;

namespace GameServer.Logic.Behaviors.Movement;

internal class Orbit(
    float speed,
    float? radius = null,
    ITargetProvider? targetProvider = null,
    float? speedVariance = null,
    float? radiusVariance = null,
    int? dir = null) : StatefulCycleBehavior<OrbitStorage>
{
    private readonly ITargetProvider targetProvider = targetProvider ?? PlayerProvider.Default;
    private readonly float radiusVariance = radiusVariance ?? speed * 0.1f;
    private readonly float speedVariance = speedVariance ?? speed * 0.1f;
    private readonly int direction = dir ?? Random.Shared.Next(0, 2);

    internal override void OnStateEntry(Entity host, ref OrbitStorage? state)
    {
        float stateRadius;
        if (radius is not null)
        {
            stateRadius = radius.Value;
        }
        else
        {
            var targetPosition = targetProvider.GetPosition(host) ?? Vector2.Zero;
            stateRadius = Vector2.Distance(targetPosition, host.Position);
        }

        state = new OrbitStorage
        {
            Speed = (float)(speed + (speedVariance * ((Random.Shared.NextDouble() * 2) - 1))),
            Radius = (float)(stateRadius + (radiusVariance * ((Random.Shared.NextDouble() * 2) - 1))),
        };
    }

    internal override void TickCore(Entity host, ref OrbitStorage? state)
    {
        if (state is null)
        {
            return;
        }

        Status = CycleStatus.NotStarted;
        if (host.HasConditionEffect(ConditionEffectIndex.Paralyzed))
        {
            return;
        }

        var position = targetProvider.GetPosition(host);
        if (position != null)
        {
            var angle = host.Position == position.Value
                ? Math.Atan2(
                    host.Y - position.Value.Y + ((Random.Shared.NextDouble() * 2) - 1),
                    host.X - position.Value.X + ((Random.Shared.NextDouble() * 2) - 1))
                : Math.Atan2(host.Y - position.Value.Y, host.X - position.Value.X);
            var angularSpd = host.GetSpeed(state.Speed) / state.Radius;
            angle += angularSpd * (direction == 0 ? 1 : -1);
            var vect = position.Value + (new Vector2((float)Math.Cos(angle), (float)Math.Sin(angle)) * state.Radius);
            vect -= host.Position;
            vect = Vector2.Normalize(vect);
            vect *= host.GetSpeed(state.Speed);
            host.ValidateAndMove(host.X + vect.X, host.Y + vect.Y);
            Status = CycleStatus.InProgress;
        }
    }
}