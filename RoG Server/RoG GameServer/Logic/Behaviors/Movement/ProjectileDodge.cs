﻿using System.Numerics;
using GameServer.Logic.Behaviors.Helpers;
using GameServer.Logic.Engine;
using GameServer.Realm;
using GameServer.Realm.Entities;
using RoGCore.Game;
using RoGCore.Models;
using RoGCore.Utils;
using RoGCore.Vector;

namespace GameServer.Logic.Behaviors.Movement;

internal class ProjectileDodge(float maximumSpeed) : CycleBehavior
{
    // Determines how many ticks (-1) the algorithm looks into the future.
    internal const int SEARCHDEPTH = 6;

    // Distance from the host entity to check for projectiles to dodge.
    private const int PROJECTILESEARCHRANGE = 5;

    // Allows the host entity to move in half speeds (Performance Impact).
    private const bool HALFSPEED = true;
    private readonly Dictionary<ProjectileDodgeSearchNode, float> nodeCache = new(new ProjectileDodgeSearchNodeComparer());
    private readonly Vector2[][] projectileStorage = new Vector2[SEARCHDEPTH][];

    private Vector2S searchBranch = new Vector2S(HALFSPEED ? 9 : 5);

    internal override void TickCore(Entity host)
    {
        Status = CycleStatus.NotStarted;
        if (host.HasConditionEffect(ConditionEffectIndex.Paralyzed))
        {
            return;
        }

        var position = GetPosition(host);
        if (position == host.Position)
        {
            Status = CycleStatus.Completed;
            return;
        }

        Status = CycleStatus.InProgress;
        var vect = position - host.Position;
        if (host.HasConditionEffect(ConditionEffectIndex.Confused))
        {
            vect *= Random.Shared.Next(0, 4) == 0 ? 1 : -1;
        }

        host.ValidateAndMove(host.Position + vect);
    }

    internal Vector2 GetPosition(Entity host)
    {
        const int PROJECTILESEARCHRANGESQR = PROJECTILESEARCHRANGE * PROJECTILESEARCHRANGE;
        var projectilesToDodge = host.Owner.GatherProjectiles()
            .Where(x => x.CanHitEntity(host) && host.DistSqr(x.Position) < PROJECTILESEARCHRANGESQR).ToArray();
        if (projectilesToDodge.Length == 0)
        {
            return host.Position;
        }

        nodeCache.Clear();
        PopulateProjectileStorage(projectilesToDodge, host.Owner.CurrentTime.StaticMspt);
        PopulateSearchBranch(host.GetSpeed(maximumSpeed));
        var bestNode = GetBestNode(null, host.Position, host);
        if (bestNode is null)
        {
            return host.Position;
        }

        return bestNode.Value.Key.Position;
    }

    private static int[] CalculateSearchBranchOrder(int amount)
    {
        var order = new int[amount];
        for (var i = 0; i < order.Length; i++)
        {
            while (true)
            {
                var value = Random.Shared.Next(1, order.Length + 1);
                if (!order.Contains(value))
                {
                    order[i] = value;
                    break;
                }
            }
        }

        return order;
    }

    private void PopulateProjectileStorage(Projectile[] projectilesToDodge, float staticMspt)
    {
        for (var i = 0; i < SEARCHDEPTH; i++)
        {
            projectileStorage[i] = new Vector2[projectilesToDodge.Length];
            var time = HighResolutionDateTime.UtcNow.AddMilliseconds(i * staticMspt);
            for (var j = 0; j < projectilesToDodge.Length; j++)
            {
                var projectile = projectilesToDodge[j];
                projectileStorage[i][j] =
                    projectile.GetPosition((float)(time - projectile.BeginTime).TotalMilliseconds).Convert();
            }
        }
    }

    private void PopulateSearchBranch(float speed)
    {
        searchBranch[0] = Vector2.Zero;
        var order = CalculateSearchBranchOrder(HALFSPEED ? 8 : 4);
        searchBranch[order[0]] = new Vector2(speed, speed);
        searchBranch[order[1]] = new Vector2(speed, -speed);
        searchBranch[order[2]] = new Vector2(-speed, speed);
        searchBranch[order[3]] = new Vector2(-speed, -speed);
        if (HALFSPEED)
        {
            speed *= 0.5f;
            searchBranch[order[4]] = new Vector2(speed, speed);
            searchBranch[order[5]] = new Vector2(speed, -speed);
            searchBranch[order[6]] = new Vector2(-speed, speed);
            searchBranch[order[7]] = new Vector2(-speed, -speed);
        }
    }

    private float? AnalyzeNode(ProjectileDodgeSearchNode node, Entity host)
    {
        var fullNodeValue = node.CalculateSelfValue(projectileStorage[node.Depth], host);
        var siblings = node.Siblings;
        if (siblings is not null)
        {
            foreach (var sibling in siblings)
            {
                // if a sibling's self evalutation is 33% better than mine, then give up because the odds of being better at the end of
                // the branch is very low.
                if (sibling.SelfValue is not null && sibling.SelfValue < fullNodeValue + (fullNodeValue * 0.33f))
                {
                    return null;
                }
            }
        }

        if (node.Depth != SEARCHDEPTH - 1)
        {
            var bestNode =
                GetBestNode(node, node.Position, host);
            if (bestNode != null)
            {
                fullNodeValue += bestNode.Value.Value;
            }
        }

        nodeCache.Add(node, fullNodeValue);
        return fullNodeValue;
    }

    private KeyValuePair<ProjectileDodgeSearchNode, float>? GetBestNode(
        ProjectileDodgeSearchNode? parent,
        Vector2 startPosition,
        Entity host)
    {
        Vector2S searchPositions = new(searchBranch.Count);
        searchBranch.Add(startPosition, ref searchPositions);
        KeyValuePair<ProjectileDodgeSearchNode, float>? bestNode = null;
        for (var i = 0; i < searchPositions.Count; i++)
        {
            ProjectileDodgeSearchNode newNode = new(parent, searchPositions[i]);
            float? nodeValue;
            if (nodeCache.TryGetValue(newNode, out var cachedNodeValue))
            {
                nodeValue = cachedNodeValue;
            }
            else
            {
                nodeValue = AnalyzeNode(newNode, host);
            }

            if (nodeValue is null)
            {
                continue;
            }

            if (bestNode is null || bestNode.Value.Value > nodeValue.Value)
            {
                bestNode = KeyValuePair.Create(newNode, nodeValue.Value);

                // Cannot dodge less than 0 projectiles. Stop searching for a better node.
                if (nodeValue.Value == 0)
                {
                    break;
                }
            }
        }

        return bestNode;
    }
}