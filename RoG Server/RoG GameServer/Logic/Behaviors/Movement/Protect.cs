﻿using System.Numerics;
using GameServer.Logic.Behaviors.TargetProvider;
using GameServer.Logic.Engine;
using GameServer.Realm.Entities;
using RoGCore.Models;

namespace GameServer.Logic.Behaviors.Movement;

internal enum ProtectState
{
    DontKnowWhere,
    Protecting,
    Protected,
}

internal class Protect : StatefulCycleBehavior<ProtectState>
{
    private readonly ITargetProvider targetProvider;
    private readonly float protectionRange;
    private readonly float reprotectRange;
    private readonly float speed;

    internal Protect(
        double speed,
        ITargetProvider targetProvider,
        double protectionRange = 4,
        double reprotectRange = 1)
    {
        this.speed = (float)speed;
        this.targetProvider = targetProvider;
        this.protectionRange = (float)protectionRange;
        this.reprotectRange = (float)reprotectRange;
    }

    internal override void TickCore(Entity host, ref ProtectState state)
    {
        Status = CycleStatus.NotStarted;
        if (host.HasConditionEffect(ConditionEffectIndex.Paralyzed))
        {
            return;
        }

        var entity = targetProvider.GetTarget(host);
        Vector2 vector;
        switch (state)
        {
            case ProtectState.DontKnowWhere:
                if (entity != null)
                {
                    state = ProtectState.Protecting;
                    goto case ProtectState.Protecting;
                }

                break;
            case ProtectState.Protecting:
                if (entity is null)
                {
                    state = ProtectState.DontKnowWhere;
                    break;
                }

                vector = new Vector2(entity.X - host.X, entity.Y - host.Y);
                if (vector.Length() > reprotectRange)
                {
                    Status = CycleStatus.InProgress;
                    Vector2.Normalize(vector);
                    var dist = host.GetSpeed(speed);
                    host.ValidateAndMove(host.X + (vector.X * dist), host.Y + (vector.Y * dist));
                }
                else
                {
                    Status = CycleStatus.Completed;
                    state = ProtectState.Protected;
                }

                break;
            case ProtectState.Protected:
                if (entity is null)
                {
                    state = ProtectState.DontKnowWhere;
                    break;
                }

                Status = CycleStatus.Completed;
                vector = new Vector2(entity.X - host.X, entity.Y - host.Y);
                if (vector.Length() > protectionRange)
                {
                    state = ProtectState.Protecting;
                    goto case ProtectState.Protecting;
                }

                break;
        }
    }
}