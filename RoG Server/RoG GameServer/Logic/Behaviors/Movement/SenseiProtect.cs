﻿using System.Numerics;
using GameServer.Logic.Engine;
using GameServer.Realm;
using GameServer.Realm.Entities;
using GameServer.Realm.Entities.Enemy;
using RoGCore.Models;

namespace GameServer.Logic.Behaviors.Movement;

internal struct SenseiProtectStorage
{
    internal float SideOffset;
    internal float ForwardOffset;
    internal Enemy? Protectee;
}

internal class SenseiProtect : StatefulCycleBehavior<SenseiProtectStorage>
{
    private readonly float speed;
    private readonly float distFromPlayer;

    internal SenseiProtect(float speed, float distFromPlayer = 4)
    {
        this.speed = speed;
        this.distFromPlayer = distFromPlayer;
    }

    internal override void OnStateEntry(Entity host, ref SenseiProtectStorage state)
    {
        state = GenerateStorage(host);
    }

    internal override void TickCore(Entity host, ref SenseiProtectStorage state)
    {
        if (host.Owner.CurrentTime.TickCount % Program.AppSettings.GameServer.Tps == 0)
        {
            state.SideOffset = (Random.Shared.NextSingle() * 10f) - 5f;
            state.ForwardOffset = (Random.Shared.NextSingle() * 2f) - 1f;
        }

        Status = CycleStatus.NotStarted;
        if (host.HasConditionEffect(ConditionEffectIndex.Paralyzed) || state.Protectee is null)
        {
            return;
        }

        Status = CycleStatus.InProgress;

        var closestPlayer = state.Protectee.GetNearest<Entity>(float.PositiveInfinity, EntityUtils.PlayerOrSummonPredicate());

        if (closestPlayer is null)
        {
            return;
        }

        Angle angle = new(MathF.Atan2(state.Protectee.Position.Y - closestPlayer.Position.Y, state.Protectee.Position.X - closestPlayer.Position.X));
        Vector2 angleVect = new(angle.Cos, angle.Sin);
        Angle sideAngle = new(angle.Rad + (0.5f * MathF.PI));
        Vector2 sideAngleVect = new(sideAngle.Cos, sideAngle.Sin);

        var destination = closestPlayer.Position + (angleVect * (distFromPlayer + state.ForwardOffset)) + (sideAngleVect * state.SideOffset);

        if (destination == host.Position)
        {
            return;
        }

        var dist = host.GetSpeed(speed);

        var diff = destination - host.Position;
        dist = MathF.Min(dist, diff.Length());

        host.ValidateAndMove(host.Position + (Vector2.Normalize(diff) * dist));
    }

    private static SenseiProtectStorage GenerateStorage(Entity host)
    {
        return new SenseiProtectStorage
        {
            Protectee = host.InRange<Enemy>(float.PositiveInfinity, EntityUtils.NamePredicate("Arimanius The Rat King")).First(),
            SideOffset = (Random.Shared.NextSingle() * 10f) - 5f,
            ForwardOffset = (Random.Shared.NextSingle() * 2f) - 1f,
        };
    }
}