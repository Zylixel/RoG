﻿using System.Numerics;
using GameServer.Logic.Behaviors.TargetProvider;
using GameServer.Logic.Engine;
using GameServer.Realm.Entities;
using RoGCore.Models;

namespace GameServer.Logic.Behaviors.Movement;

internal class StayBack(float speed, float distance = 8, ITargetProvider? targetProvider = null) : StatefulCycleBehavior<float?>
{
    private ITargetProvider TargetProvider { get; } = targetProvider ?? PlayerProvider.Default;

    internal override void TickCore(Entity host, ref float? cooldown)
    {
        cooldown ??= 1000f;
        Status = CycleStatus.NotStarted;
        if (host.HasConditionEffect(ConditionEffectIndex.Paralyzed))
        {
            return;
        }

        var avoidPosition = TargetProvider.GetPosition(host);
        if (avoidPosition is not null)
        {
            var vect = avoidPosition.Value - host.Position;

            if (vect == Vector2.Zero)
            {
                vect = new Vector2(Random.Shared.NextSingle(), Random.Shared.NextSingle());
            }

            if (vect.Length() < distance)
            {
                vect = Vector2.Normalize(vect);
                var dist = host.GetSpeed(speed);
                vect *= dist;
                host.ValidateAndMove(host.Position - vect);
            }

            if (cooldown <= 0)
            {
                Status = CycleStatus.Completed;
                cooldown = 1000f;
            }
            else
            {
                Status = CycleStatus.InProgress;
                cooldown -= host.Owner.CurrentTime.StaticMspt;
            }
        }
    }
}