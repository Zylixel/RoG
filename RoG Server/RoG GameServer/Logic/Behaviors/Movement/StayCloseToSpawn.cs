﻿using System.Numerics;
using GameServer.Logic.Engine;
using GameServer.Realm.Entities;

namespace GameServer.Logic.Behaviors.Movement;

internal class StayCloseToSpawn(float speed, int range = 5) : StatefulCycleBehavior<Vector2>
{
    internal override void OnStateEntry(Entity host, ref Vector2 state)
    {
        // TODO: Use spawnpoint if available
        state = host.Position;
    }

    internal override void TickCore(Entity host, ref Vector2 spawnPoint)
    {
        var vector = spawnPoint;
        Status = CycleStatus.NotStarted;

        var l = (vector - new Vector2(host.X, host.Y)).Length();
        if (l <= range)
        {
            Status = CycleStatus.Completed;
            return;
        }

        vector -= new Vector2(host.X, host.Y);
        Vector2.Normalize(vector);
        var dist = host.GetSpeed(speed);
        host.ValidateAndMove(host.X + (vector.X * dist), host.Y + (vector.Y * dist));
        Status = CycleStatus.InProgress;
    }
}