﻿using System.Numerics;
using GameServer.Logic.Behaviors.Storage;
using GameServer.Logic.Engine;
using GameServer.Realm.Entities;
using RoGCore.Models;

namespace GameServer.Logic.Behaviors.Movement;

internal class Wander : StatefulCycleBehavior<WanderStorage>
{
    private readonly Cooldown period;
    private readonly Cooldown cooldown;
    private readonly float speed;
    private readonly string? avoidGround;

    internal Wander(float speed, string? avoidGround = null, Cooldown? period = null, Cooldown? cooldown = null)
    {
        this.speed = speed;
        this.avoidGround = avoidGround;
        this.period = period ?? new Cooldown(1250, 500);
        this.cooldown = cooldown ?? new Cooldown(2000, 500);
    }

    internal override void TickCore(Entity host, ref WanderStorage? storage)
    {
        if (storage is null)
        {
            storage = new WanderStorage();
            FindNewSpot(storage);
        }

        Status = CycleStatus.NotStarted;
        if (host.HasConditionEffect(ConditionEffectIndex.Paralyzed))
        {
            return;
        }

        if (storage.RemainingCooldown > 0)
        {
            storage.RemainingCooldown -= host.Owner.CurrentTime.StaticMspt;
            return;
        }

        Status = CycleStatus.InProgress;
        if (storage.RemainingDistance <= 0)
        {
            storage.RemainingCooldown = cooldown.Next();
            FindNewSpot(storage);
            Status = CycleStatus.Completed;
        }

        var dist = host.GetSpeed(speed);
        if (avoidGround != null)
        {
            var x = host.X + (storage.Direction.X * dist);
            var y = host.Y + (storage.Direction.Y * dist);
            var tile = host.Owner.GetTile((int)x, (int)y);
            if (tile?.TileDesc != null && tile.TileDesc.Name == avoidGround)
            {
                storage.RemainingDistance -= dist;
                return;
            }
        }

        var vect = storage.Direction * dist;
        host.ValidateAndMove(host.Position + vect);
        storage.RemainingDistance -= dist;

        void FindNewSpot(WanderStorage storage)
        {
            var angle = Angle.RandomAngle(Random.Shared);
            storage.Direction = new Vector2(angle.Cos, angle.Sin);
            storage.Direction = Vector2.Normalize(storage.Direction);
            storage.RemainingDistance = period.Next() / 1000f;
        }
    }
}