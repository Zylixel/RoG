﻿using GameServer.Logic.Engine;

namespace GameServer.Logic.Behaviors;

/// <summary>
/// Initializes a new instance of the <see cref="OnDeathBehavior"/> class.
/// Please note that you can only use a behavior here who is overloading the OnStateEntry method.
/// </summary>
/// <param name="behavior">The Behavior to execute.</param>
internal class OnDeathBehavior(params Behavior[] behavior) : Behavior
{
    internal override void Resolve(State parent, Dictionary<string, State> states)
    {
        foreach (var behavior in behavior)
        {
            parent.Death += (s, e) => behavior.OnStateEntry(e.Host);
        }
    }
}