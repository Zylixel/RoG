﻿using GameServer.Logic.Engine;
using GameServer.Realm;
using GameServer.Realm.Entities;
using Microsoft.Extensions.Logging;
using RoGCore.Logging;

namespace GameServer.Logic.Behaviors;

internal class Order(float range, string children, string targetStateName, bool once = true) : Behavior
{
    private static readonly ILogger<Order> Logger = LogFactory.LoggingInstance<Order>();

    private State? targetState;

    internal override void Resolve(State parent, Dictionary<string, State> states)
    {
        if (!BehaviorDb.Definitions.TryGetValue(children, out var childState))
        {
            Logger.LogError($"Key {children} is not found in Order.cs");
            return;
        }

        targetState = FindState(childState.Key, targetStateName);
    }

    internal override void OnStateEntry(Entity host)
    {
        Command(host);
    }

    internal override void TickCore(Entity host)
    {
        if (once)
        {
            return;
        }

        Command(host);
    }

    private void Command(Entity host)
    {
        foreach (var i in host.InRange<Entity>(range, EntityUtils.NamePredicate(children)))
        {
            if (i.CurrentState?.Is(targetState) == false)
            {
                i.SwitchTo(targetState);
            }
        }
    }
}