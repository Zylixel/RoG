﻿using GameServer.Logic.Engine;
using GameServer.Realm.Entities;

namespace GameServer.Logic.Behaviors;

internal class Prioritize(params CycleBehavior[] children) : StatefulBehavior<int>
{
    internal override void OnStateEntry(Entity host, ref int index)
    {
        index = -1;
        foreach (var i in children)
        {
            i.OnStateEntry(host);
        }
    }

    internal override void TickCore(Entity host, ref int index)
    {
        // select
        if (index < 0)
        {
            index = 0;
            for (var i = 0; i < children.Length; i++)
            {
                (children[i] as IChildren)?.Tick(host);
                if (children[i].Status == CycleStatus.InProgress)
                {
                    index = i;
                    break;
                }
            }
        }
        else
        {
            // run a cycle
            (children[index] as IChildren)?.Tick(host);
            if (children[index].Status != CycleStatus.InProgress)
            {
                index = -1;
            }
        }
    }
}