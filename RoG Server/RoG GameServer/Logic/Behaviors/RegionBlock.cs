﻿using GameServer.Logic.Engine;
using GameServer.Realm.Entities;
using GameServer.Realm.Terrain.Tile;
using RoGCore.Assets;

namespace GameServer.Logic.Behaviors;

internal sealed class RegionBlock : Behavior
{
    private readonly TileRegion region;
    private readonly string objId;

    internal RegionBlock(TileRegion region, string objId)
    {
        this.region = region;
        this.objId = objId;
    }

    internal override void OnStateEntry(Entity host)
    {
        // If returns null, it will clear the tile
        EmbeddedData.BetterObjects.TryGetValue(objId, out var objDesc);
        for (var y = 0; y < host.Owner.Map.Height; y++)
        {
            for (var x = 0; x < host.Owner.Map.Width; x++)
            {
                var currentTile = host.Owner.GetTile(x, y);

                if (currentTile is null || currentTile.Region != region)
                {
                    continue;
                }

                currentTile.ObjDesc = objDesc;
            }
        }
    }
}