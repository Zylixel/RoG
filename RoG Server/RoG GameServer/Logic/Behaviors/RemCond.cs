﻿using GameServer.Logic.Engine;
using GameServer.Realm.Entities;
using RoGCore.Models;

namespace GameServer.Logic.Behaviors;

internal class RemCond(ConditionEffectIndex effect, bool perm = false) : Behavior
{
    internal override void OnStateEntry(Entity host)
    {
        host.ApplyConditionEffect(new ConditionEffect
        {
            Effect = effect,
            DurationMs = 0,
        });
    }

    internal override void OnStateExit(Entity host)
    {
        if (!perm)
        {
            host.ApplyConditionEffect(new ConditionEffect
            {
                Effect = effect,
                DurationMs = 0,
            });
        }
    }
}