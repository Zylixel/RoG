﻿using GameServer.Logic.Engine;
using GameServer.Realm;
using GameServer.Realm.Entities;

namespace GameServer.Logic.Behaviors;

internal class RemoveEntity(float dist, string children) : Behavior
{
    internal override void OnStateEntry(Entity host)
    {
        foreach (var e in host.InRange<Entity>(dist, EntityUtils.NamePredicate(children)))
        {
            host.Owner.LeaveWorld(e);
        }
    }
}