﻿using GameServer.Logic.Engine;
using GameServer.Realm.Entities;

namespace GameServer.Logic.Behaviors;

// replacement for simple sequential state transition
internal class Sequence(params CycleBehavior[] children) : StatefulBehavior<int>
{
    internal override void OnStateEntry(Entity host, ref int state)
    {
        foreach (var i in children)
        {
            i.OnStateEntry(host);
        }
    }

    internal override void TickCore(Entity host, ref int index)
    {
        children[index].TickCore(host);
        if (children[index].Status is CycleStatus.Completed or
            CycleStatus.NotStarted)
        {
            index++;
            if (index == children.Length)
            {
                index = 0;
            }
        }
    }
}