﻿using GameServer.Logic.Engine;
using GameServer.Realm.Entities;

namespace GameServer.Logic.Behaviors;

internal class SetLight(float intensity) : Behavior
{
    internal override void OnStateEntry(Entity host)
    {
        host.Owner.LightLevel = intensity;
    }
}