﻿using GameServer.Logic.Behaviors.Storage;
using GameServer.Logic.Behaviors.TargetProvider;
using GameServer.Logic.Engine;
using GameServer.Realm.Entities;
using GameServer.Realm.Entities.GameObject;
using RoGCore.Models;
using RoGCore.Networking.Packets;

namespace GameServer.Logic.Behaviors.Shoot;

internal class CannonShoot(
        Cooldown coolDown,
        ITargetProvider? targetProvider = null,
        int projectileIndex = 0,
        Cooldown cooldownOffset = default,
        GroupProvider? targetGroups = null)
    : ShootBase(
        coolDown,
        targetProvider,
        1,
        projectileIndex,
        0,
        cooldownOffset,
        targetGroups)
{
    internal override void OnStateEntry(Entity host, ref ShootStorage? state)
    {
        if (host is not Cannon cannon)
        {
            throw new InvalidCastException("Entity must be a Cannon");
        }

        state = new ShootStorage() { Cooldown = (cannon.ShootOrder * 250f) + CoolDownOffset.Next() };
    }

    protected override void ShootProjectile(Entity host, in Target target, ref ShootStorage shootState)
    {
        if (target.Angle is null)
        {
            return;
        }

        var count = Count;
        if (host.HasConditionEffect(ConditionEffectIndex.Dazed))
        {
            count = (byte)Math.Max(1, count / 2);
        }

        var angle = target.Angle.Rad;
        var desc = host.ObjectDescription.Projectiles?[ProjectileIndex];

        if (desc is null)
        {
            return;
        }

        var dmg = desc.BaseDamage;

        var globalTime = HighResolutionDateTime.UtcNow.Ticks;
        var prj = host.CreateProjectile(
            desc,
            dmg,
            globalTime,
            host.Position,
            angle,
            host.Owner,
            targetGroups: TargetGroups);

        host.Owner.BroadcastPacket(new EnemyShoot(host.Id, ProjectileIndex, prj, count, 0f, false, globalTime), host, false);
    }
}