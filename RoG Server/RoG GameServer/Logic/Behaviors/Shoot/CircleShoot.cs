﻿using GameServer.Logic.Behaviors.Storage;
using GameServer.Logic.Behaviors.TargetProvider;
using GameServer.Logic.Engine;
using GameServer.Realm;
using GameServer.Realm.Entities;
using RoGCore.Models;
using RoGCore.Networking.Packets;

namespace GameServer.Logic.Behaviors.Shoot;

internal class CircleShoot : ShootBase
{
    private readonly float angleIncrement;
    private readonly float shootAngle;
    private readonly float startAngle;
    private readonly bool offsetProjectiles;
    private readonly float missInterval;
    private readonly int missCount;

    internal CircleShoot(
        byte count = 1,
        int projectileIndex = 0,
        float angleIncrement = (float)Math.PI / 8,
        float startAngle = 0,
        bool offsetProjectiles = false,
        float coolDownOffset = 0,
        float missInterval = 0,
        int missCount = 1,
        Cooldown coolDown = default,
        GroupProvider? targetGroups = null)
        : base(coolDown, SelfProvider.Default, count, projectileIndex, 0, coolDownOffset, targetGroups)
    {
        this.angleIncrement = angleIncrement;
        this.startAngle = startAngle;
        this.offsetProjectiles = offsetProjectiles;
        this.missInterval = missInterval;
        this.missCount = missCount;
        shootAngle = (float)(count == 1 ? 0 : 360.0 / count * Math.PI / 180);
    }

    protected override void ShootProjectile(Entity host, in Target target, ref ShootStorage shootState)
    {
        if (host.HasConditionEffect(ConditionEffectIndex.Stunned))
        {
            return;
        }

        var count = Count;
        if (host.HasConditionEffect(ConditionEffectIndex.Dazed))
        {
            count = (byte)Math.Max(1, count / 2);
        }

        var desc = host.ObjectDescription.Projectiles?[ProjectileIndex]
            ?? throw new ArgumentException($"{host} does not have projectiles to shoot.");
        var dmg = desc.BaseDamage;

        if (shootState.Data is not CircleShootState circleShootState)
        {
            circleShootState.CurrentAngle = startAngle;
            circleShootState.MissesLeft = 0;
            circleShootState.TotalShots = 0;
        }

        circleShootState.TotalShots++;
        circleShootState.CurrentAngle += angleIncrement;

        if (circleShootState.MissesLeft > 0)
        {
            circleShootState.MissesLeft--;
            shootState.Data = circleShootState;
            return;
        }

        if (missInterval > 0 && circleShootState.TotalShots % missInterval == 0)
        {
            circleShootState.MissesLeft = missCount - 1;
            shootState.Data = circleShootState;
            return;
        }

        shootState.Data = circleShootState;

        var globalTime = HighResolutionDateTime.UtcNow.Ticks;
        Projectile? mainProjectile = null;
        for (var i = 0; i < count; i++)
        {
            var angle = circleShootState.CurrentAngle + (shootAngle * i);
            var prj = offsetProjectiles
                ? host.CreateProjectile(
                    desc,
                    dmg,
                    globalTime,
                    host.Position,
                    angle,
                    host.Owner,
                    targetGroups: TargetGroups)
                : host.CreateProjectile(desc, dmg, globalTime, host.Position, angle, host.Owner, targetGroups: TargetGroups);

            if (i == 0)
            {
                mainProjectile = prj;
            }
        }

        if (mainProjectile is null)
        {
            return;
        }

        host.Owner.BroadcastPacket(new EnemyShoot(host.Id, ProjectileIndex, mainProjectile, count, shootAngle, offsetProjectiles, globalTime), host, false);
    }

    private struct CircleShootState
    {
        internal float CurrentAngle;
        internal int TotalShots;
        internal int MissesLeft;
    }
}