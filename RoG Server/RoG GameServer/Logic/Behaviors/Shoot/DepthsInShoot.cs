﻿using System.Numerics;
using GameServer.Logic.Behaviors.Storage;
using GameServer.Logic.Behaviors.TargetProvider;
using GameServer.Realm.Entities;
using RoGCore.Models;
using RoGCore.Networking;
using RoGCore.Networking.Packets;

namespace GameServer.Logic.Behaviors.Shoot;

internal class DepthsInShoot : ShootBase
{
    private readonly float shootAngle;

    internal DepthsInShoot()
        : base(100000, SelfProvider.Default, 10, 6, 0, 250, null)
    {
        shootAngle = (float)(Count == 1 ? 0 : 360.0 / Count * Math.PI / 180);
    }

    protected override void ShootProjectile(Entity host, in Target target, ref ShootStorage shootState)
    {
        if (target.Position is null)
        {
            throw new ArgumentException("TargetProvider does not provide an angle for DepthsInShoot");
        }

        if (host.HasConditionEffect(ConditionEffectIndex.Stunned))
        {
            return;
        }

        var desc = host.ObjectDescription.Projectiles?[ProjectileIndex]
            ?? throw new ArgumentException($"{host} does not have projectiles to shoot.");
        var dmg = desc.BaseDamage;

        var globalTime = HighResolutionDateTime.UtcNow.Ticks;

        var packets = new IPacket[Count];

        for (var i = 0; i < Count; i++)
        {
            var posAngle = shootAngle * i;
            var pos = host.Position + new Vector2
            {
                X = 3f * MathF.Cos(posAngle),
                Y = 3f * MathF.Sin(posAngle),
            };
            var tagetAngle = MathF.Atan2(target.Position.Value.Y - pos.Y, target.Position.Value.X - pos.X);

            var prj = host.CreateProjectile(
                    desc,
                    dmg,
                    globalTime,
                    pos,
                    tagetAngle,
                    host.Owner,
                    targetGroups: TargetGroups);

            packets[i] = new EnemySpecificShoot(host.Id, ProjectileIndex, prj, globalTime);
        }

        host.Owner.BroadcastPackets(packets, host);
    }
}