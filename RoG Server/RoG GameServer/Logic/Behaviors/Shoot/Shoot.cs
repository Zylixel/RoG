﻿using GameServer.Logic.Behaviors.Storage;
using GameServer.Logic.Behaviors.TargetProvider;
using GameServer.Logic.Engine;
using GameServer.Realm;
using GameServer.Realm.Entities;
using RoGCore.Assets;
using RoGCore.Models;
using RoGCore.Networking.Packets;

namespace GameServer.Logic.Behaviors.Shoot;

internal class Shoot : ShootBase
{
    private readonly float angleOffset;
    private readonly float shootAngle;

    internal Shoot(
        Cooldown coolDown,
        ITargetProvider? targetProvider = null,
        byte count = 1,
        float? shootAngle = null,
        int projectileIndex = 0,
        float angleOffset = 0,
        float predictive = 0,
        Cooldown coolDownOffset = default,
        GroupProvider? targetGroups = null)
        : base(coolDown, targetProvider, count, projectileIndex, predictive, coolDownOffset, targetGroups)
    {
        this.shootAngle = count == 1 ? 0 : (shootAngle ?? 360f / count) * MathF.PI / 180f;
        this.angleOffset = angleOffset * MathF.PI / 180f;
    }

    protected override void ShootProjectile(Entity host, in Target target, ref ShootStorage shootState)
    {
        if (target.Angle is null)
        {
            return;
        }

        var globalTime = HighResolutionDateTime.UtcNow.Ticks;
        var count = Count;
        if (host.HasConditionEffect(ConditionEffectIndex.Dazed))
        {
            count = (byte)Math.Max(1, count / 2);
        }

        var angle = target.Angle.Rad;
        var desc = host.ObjectDescription.Projectiles?[ProjectileIndex];

        if (desc is null)
        {
            return;
        }

        angle += angleOffset;
        if (Predictive != 0 && target.Entity != null)
        {
            angle += Predict(host, target.Entity, desc) * Predictive;
        }

        var dmg = desc.BaseDamage;

        var startAngle = angle - (shootAngle * (count - 1) / 2);
        Projectile? mainProjectile = null;

        for (var i = 0; i < count; i++)
        {
            var prj = host.CreateProjectile(
                desc,
                dmg,
                globalTime,
                host.Position,
                (float)(startAngle + (shootAngle * i)),
                host.Owner,
                targetGroups: TargetGroups);

            if (i == 0)
            {
                mainProjectile = prj;
            }
        }

        if (mainProjectile is null)
        {
            return;
        }

        host.Owner.BroadcastPacket(new EnemyShoot(host.Id, ProjectileIndex, mainProjectile, count, shootAngle, false, globalTime), host, false);
    }

    private static float Predict(Entity host, Entity target, CustomProjectile desc)
    {
        var history = target.TryGetHistory(1);
        if (history is null)
        {
            return 0;
        }

        var originalAngle = MathF.Atan2(history.Value.Y - host.Y, history.Value.X - host.X);
        var newAngle = MathF.Atan2(target.Y - host.Y, target.X - host.X);

        var bulletSpeed = desc.GetSpeed(null, null) * 0.01f;

        var angularVelo = (newAngle - originalAngle) * 10f;
        return angularVelo * bulletSpeed;
    }
}