﻿using GameServer.Logic.Behaviors.Storage;
using GameServer.Logic.Behaviors.TargetProvider;
using GameServer.Logic.Engine;
using GameServer.Realm.Entities;
using GameServer.Realm.World.Worlds;
using RoGCore.Models;

namespace GameServer.Logic.Behaviors.Shoot;

internal abstract class ShootBase(
    Cooldown coolDown,
    ITargetProvider? targetProvider,
    byte count,
    int projectileIndex,
    float predictive,
    Cooldown coolDownOffset,
    GroupProvider? targetGroups) : StatefulCycleBehavior<ShootStorage>
{
    protected readonly ITargetProvider TargetProvider = targetProvider ?? PlayerProvider.Default;
    protected readonly Cooldown CoolDownOffset = coolDownOffset;
    protected readonly byte Count = count;
    protected readonly float Predictive = predictive;
    protected readonly int ProjectileIndex = projectileIndex;
    protected readonly Cooldown CoolDown = coolDown;
    protected readonly string[] TargetGroups = targetGroups is not null
            ? targetGroups.Value.Groups
            : targetProvider is GroupProvider groupProvider ? groupProvider.Groups : PlayerTargetGroup;

    private static readonly string[] PlayerTargetGroup = ["Player"];

    internal override void TickCore(Entity host, ref ShootStorage? shootState)
    {
        if (host.Owner is Nexus)
        {
            return;
        }

        shootState ??= new ShootStorage()
        {
            Cooldown = CoolDownOffset.Next(),
        };

        Status = CycleStatus.NotStarted;
        shootState.Cooldown -= host.Owner.CurrentTime.StaticMspt;
        if (shootState.Cooldown > 0)
        {
            Status = CycleStatus.InProgress;
            return;
        }

        if (host.HasConditionEffect(ConditionEffectIndex.Stunned))
        {
            return;
        }

        var target = TargetProvider.GetAll(host);
        if (target.Angle is null)
        {
            shootState.Cooldown = CoolDown.Next();
            Status = CycleStatus.Completed;
            return;
        }

        ShootProjectile(host, target, ref shootState);
        shootState.Cooldown = CoolDown.Next();
        Status = CycleStatus.Completed;
    }

    protected abstract void ShootProjectile(Entity host, in Target target, ref ShootStorage shootState);
}