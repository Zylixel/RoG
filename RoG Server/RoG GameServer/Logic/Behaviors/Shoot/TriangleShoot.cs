﻿using System.Numerics;
using GameServer.Logic.Behaviors.Storage;
using GameServer.Logic.Behaviors.TargetProvider;
using GameServer.Logic.Engine;
using GameServer.Realm.Entities;
using RoGCore.Models;
using RoGCore.Networking;
using RoGCore.Networking.Packets;

namespace GameServer.Logic.Behaviors.Shoot;

internal class TriangleShoot : ShootBase
{
    private readonly int lines;
    private readonly int increasePerLine;
    private readonly float spacing;
    private readonly Vector2Int[]? skipProjectiles;

    internal TriangleShoot(
        ITargetProvider? targetProvider = null,
        byte count = 1,
        float predictive = 0,
        int projectileIndex = 0,
        float coolDownOffset = 0,
        int lines = 2,
        int increasePerLine = 1,
        float spacing = 1,
        Vector2Int[]? skipProjectiles = null,
        Cooldown coolDown = default,
        GroupProvider? targetGroups = null)
        : base(coolDown, targetProvider, count, projectileIndex, predictive, coolDownOffset, targetGroups)
    {
        this.lines = lines;
        this.increasePerLine = increasePerLine;
        this.spacing = spacing;
        this.skipProjectiles = skipProjectiles;
    }

    protected override void ShootProjectile(Entity host, in Target target, ref ShootStorage shootState)
    {
        if (target.Position is null || target.Angle is null)
        {
            return;
        }

        var globalTime = HighResolutionDateTime.UtcNow.Ticks;

        var desc = host.ObjectDescription.Projectiles?[ProjectileIndex]
            ?? throw new ArgumentException($"{host} does not have projectiles to shoot.");

        var dmg = desc.BaseDamage;

        var batch = new IPacket[GetTotalProjectileCount()];
        var batchCount = 0;

        const float halfPi = MathF.PI * 0.5f;
        Vector2 projectileSideVector = new(MathF.Cos(target.Angle.Rad + halfPi), MathF.Sin(target.Angle.Rad + halfPi));
        var lineStartOffset = spacing * (lines - 1) * 0.5f;

        for (var line = 0; line < lines; line++)
        {
            var projectilesInLine = (line * increasePerLine) + 1;
            var projectileStartOffset = -(spacing * (projectilesInLine - 1) * 0.5f);

            var linePosition = new Vector2(target.Angle.Cos, target.Angle.Sin) * (lineStartOffset - line) * spacing;

            for (var i = 0; i < projectilesInLine; i++)
            {
                if (skipProjectiles?.Contains(new Vector2Int(line, i)) == true)
                {
                    continue;
                }

                var sideOffset = projectileStartOffset + (i * spacing);
                var posi = host.Position + linePosition + (projectileSideVector * sideOffset);

                var proj = host.CreateProjectile(
                    desc,
                    dmg,
                    globalTime,
                    posi,
                    target.Angle.Rad,
                    host.Owner,
                    targetGroups: TargetGroups);

                batch[batchCount] = new EnemySpecificShoot(host.Id, ProjectileIndex, proj, globalTime);
                batchCount++;
            }
        }

        host.Owner.BroadcastPackets(batch, host);
    }

    private int GetTotalProjectileCount()
    {
        var total = 0;
        for (var i = 0; i < lines; i++)
        {
            if (skipProjectiles is null)
            {
                total += (i * increasePerLine) + 1;
                continue;
            }

            for (var j = 0; j < (i * increasePerLine) + 1; j++)
            {
                if (!skipProjectiles.Contains(new Vector2Int(i, j)))
                {
                    total++;
                }
            }
        }

        return total;
    }
}