﻿using System.Numerics;
using GameServer.Logic.Behaviors.Storage;
using GameServer.Logic.Behaviors.TargetProvider;
using GameServer.Logic.Engine;
using GameServer.Realm;
using GameServer.Realm.Entities;
using RoGCore.Models;
using RoGCore.Networking;
using RoGCore.Networking.Packets;

namespace GameServer.Logic.Behaviors.Shoot;

internal class WakiShoot : ShootBase
{
    // State storage: cooldown timer
    protected readonly float GapAngle;
    protected readonly float GapTiles;
    protected readonly float OffsetAngle;
    protected readonly float Range;

    internal WakiShoot(
        ITargetProvider? targetProvider = null,
        byte count = 1,
        float predictive = 0,
        int projectileIndex = 0,
        float coolDownOffset = 0,
        Cooldown coolDown = default,
        float gapAngle = 45f,
        float gapTiles = 0.4f,
        float offsetAngle = 90,
        float range = 4,
        GroupProvider? targetGroups = null)
        : base(coolDown, targetProvider, count, projectileIndex, predictive, coolDownOffset, targetGroups)
    {
        GapAngle = gapAngle;
        GapTiles = gapTiles;
        OffsetAngle = offsetAngle;
        Range = range;
    }

    protected override void ShootProjectile(Entity host, in Target target, ref ShootStorage shootState)
    {
        if (target.Entity is null || target.Position is null)
        {
            return;
        }

        var globalTime = HighResolutionDateTime.UtcNow.Ticks;
        int count = Count;
        if (host.HasConditionEffect(ConditionEffectIndex.Dazed))
        {
            count = Math.Max(1, count / 2);
        }

        var desc = host.ObjectDescription.Projectiles?[ProjectileIndex]
            ?? throw new ArgumentException($"{host} does not have projectiles to shoot.");
        var dmg = desc.BaseDamage;

        var targetPoint = Predictive > 0 ? Predict(host, target.Entity) : target.Position.Value;
        var targetAngle = MathF.Atan2(targetPoint.Y - host.Y, targetPoint.X - host.X);
        if (host.DistSqr(targetPoint) > Range * Range)
        {
            targetPoint = host.Position + (new Vector2(MathF.Cos(targetAngle), MathF.Sin(targetAngle)) * Range);
        }

        const float rangeMultiplier = 0.01f * 0.01f * 0.5f;
        const float degToRad = MathF.PI / 180f;
        var range = desc.BaseSpeed * desc.BaseLifeTime * rangeMultiplier;
        var startGap = (count - 1) * 0.5f * GapTiles;
        var gapAngle = GapAngle * degToRad;
        Vector2 angleMinusGap = new(MathF.Cos(targetAngle - gapAngle), MathF.Sin(targetAngle - gapAngle));
        var startPosition = targetPoint + (angleMinusGap * startGap);
        var batch = new IPacket[count];
        for (var i = 0; i < count; i++)
        {
            var anglei = targetAngle + (OffsetAngle * degToRad);
            var posi = startPosition - (angleMinusGap * (i * GapTiles));
            posi -= new Vector2(MathF.Cos(anglei), MathF.Sin(anglei)) * range;

            var proj = host.CreateProjectile(
                desc,
                dmg,
                globalTime,
                posi,
                anglei,
                host.Owner,
                targetGroups: TargetGroups);

            batch[i] = new EnemySpecificShoot(host.Id, ProjectileIndex, proj, globalTime);
        }

        host.Owner.BroadcastPackets(batch, host);
    }

    private static Vector2 Predict(Entity host, Entity target)
    {
        var history = target.TryGetHistory(1);
        if (history is null)
        {
            return target.Position;
        }

        var originalAngle = MathF.Atan2(history.Value.Y - host.Y, history.Value.X - host.X);
        var newAngle = MathF.Atan2(target.Y - host.Y, target.X - host.X);
        var anglePrediction = (newAngle - originalAngle) * 10f;
        var distPrediction = MathF.Sqrt(target.DistSqr(history.Value));
        return target.Position +
               (new Vector2(MathF.Cos(anglePrediction), MathF.Sin(anglePrediction)) * distPrediction);
    }
}