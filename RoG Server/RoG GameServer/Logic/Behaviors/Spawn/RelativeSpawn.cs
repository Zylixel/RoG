﻿using System.Numerics;
using GameServer.Logic.Engine;
using GameServer.Realm.Entities;
using GameServer.Realm.Entities.Enemy;

namespace GameServer.Logic.Behaviors.Spawn;

internal class RelativeSpawn(string children, Vector2 spawnPos) : Behavior
{
    internal override void OnStateEntry(Entity host)
    {
        var entity = Entity.Resolve(children, host.Owner);
        if (entity is Enemy enemy)
        {
            enemy.GivesItemXp = false;
        }

        entity.Move(
            host.X + spawnPos.X,
            host.Y + spawnPos.Y);
    }
}