﻿using GameServer.Logic.Engine;
using GameServer.Realm;
using GameServer.Realm.Entities;
using GameServer.Realm.Entities.Enemy;

namespace GameServer.Logic.Behaviors.Spawn;

internal class Reproduce : StatefulBehavior<float?>
{
    // State storage: cooldown timer
    private readonly string? children;
    private readonly int densityMax;
    private readonly float densityRadius;
    private readonly double spawnRadius;
    private readonly Cooldown coolDown;

    internal Reproduce(
        string? children = null,
        float densityRadius = 10,
        int densityMax = 5,
        double spawnRadius = -1,
        Cooldown coolDown = default)
    {
        this.children = children;
        this.densityRadius = densityRadius;
        this.densityMax = densityMax;
        this.coolDown = coolDown;
        this.spawnRadius = spawnRadius == -1 ? densityRadius : spawnRadius;
    }

    internal override void TickCore(Entity host, ref float? cool)
    {
        cool ??= coolDown.Next();
        if (cool <= 0)
        {
            var count = host.InRange<Entity>(
                densityRadius,
                EntityUtils.NamePredicate(children ?? host.ObjectDescription.Name)).Count();
            if (count < densityMax)
            {
                var entity = Entity.Resolve(children ?? host.ObjectDescription.Name, host.Owner);
                if (entity is Enemy enemy && host is Enemy)
                {
                    enemy.GivesItemXp = false;
                }

                var i = 0;
                double targetX;
                double targetY;
                do
                {
                    var angle = Random.Shared.NextDouble() * 2 * Math.PI;
                    targetX = host.X + (spawnRadius * 0.5 * Math.Cos(angle));
                    targetY = host.Y + (spawnRadius * 0.5 * Math.Sin(angle));
                    i++;
                }
                while (targetX < host.Owner.Map.Width &&
                         targetY < host.Owner.Map.Height &&
                         targetX > 0 && targetY > 0 &&
                         i < 10);

                entity.Move((float)targetX, (float)targetY);
            }

            cool = coolDown.Next();
        }
        else
        {
            cool -= host.Owner.CurrentTime.StaticMspt;
        }
    }
}