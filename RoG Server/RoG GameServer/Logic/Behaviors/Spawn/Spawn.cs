﻿using GameServer.Logic.Engine;
using GameServer.Realm.Entities;
using GameServer.Realm.Entities.Enemy;
using RoGCore.Models;

namespace GameServer.Logic.Behaviors.Spawn;

internal struct SpawnState
{
    internal int CurrentNumber;
    internal float RemainingTime;
}

internal class Spawn : StatefulBehavior<SpawnState>
{
    private static readonly int[] Directions = [-1, 0, 1];

    // State storage: Spawn state
    private readonly string children;
    private readonly int initialSpawn;
    private readonly int maxChildren;
    private readonly bool mustSpawnOutTile;
    private readonly Cooldown coolDown;

    internal Spawn(
        string children,
        int maxChildren = 5,
        double initialSpawn = 0.5,
        Cooldown coolDown = default,
        bool mustSpawnOutTile = false)
    {
        this.children = children;
        this.maxChildren = maxChildren;
        this.initialSpawn = (int)(maxChildren * initialSpawn);
        this.mustSpawnOutTile = mustSpawnOutTile;
        this.coolDown = coolDown;
    }

    internal override void OnStateEntry(Entity host, ref SpawnState spawnState)
    {
        var world = host.Owner;

        spawnState = new SpawnState
        {
            CurrentNumber = initialSpawn,
            RemainingTime = coolDown.Next(),
        };

        for (var i = 0; i < initialSpawn; i++)
        {
            var entity = Entity.Resolve(children, world);
            if (entity is Enemy enemy && host is Enemy)
            {
                enemy.GivesItemXp = false;
            }

            if (mustSpawnOutTile)
            {
                entity.Move(
                    host.X + Directions[Random.Shared.Next(3)],
                    host.Y + Directions[Random.Shared.Next(3)]);
                var iteration = 0;
                while (world.GetTile(entity.IntPosition)?.Occupied(TileOccupency.NoWalk | TileOccupency.OccupyHalf) != false)
                {
                    if (iteration > 20)
                    {
                        break;
                    }

                    iteration++;
                    entity.Move(
                        host.X + Directions[Random.Shared.Next(3)],
                        host.Y + Directions[Random.Shared.Next(3)]);
                }
            }
            else
            {
                entity.Move(
                    host.X + (float)(Random.Shared.NextDouble() * 0.5),
                    host.Y + (float)(Random.Shared.NextDouble() * 0.5));
            }
        }
    }

    internal override void TickCore(Entity host, ref SpawnState spawnState)
    {
        if (spawnState.RemainingTime <= 0 && spawnState.CurrentNumber < maxChildren)
        {
            var entity = Entity.Resolve(children, host.Owner);
            if (entity is Enemy enemy && host is Enemy)
            {
                enemy.GivesItemXp = false;
            }

            if (mustSpawnOutTile)
            {
                entity.Move(
                    host.X + Directions[Random.Shared.Next(3)],
                    host.Y + Directions[Random.Shared.Next(3)]);
                var iteration = 0;
                while (host.Owner.Map[entity.IntPosition]?.ObjDesc?.OccupyFull != false)
                {
                    if (iteration > 20)
                    {
                        break;
                    }

                    iteration++;
                    entity.Move(
                        host.X + Directions[Random.Shared.Next(3)],
                        host.Y + Directions[Random.Shared.Next(3)]);
                }
            }
            else
            {
                entity.Move(
                    host.X + (float)(Random.Shared.NextDouble() * 0.5),
                    host.Y + (float)(Random.Shared.NextDouble() * 0.5));
            }

            spawnState.RemainingTime = coolDown.Next();
            spawnState.CurrentNumber++;
        }
        else
        {
            spawnState.RemainingTime -= host.Owner.CurrentTime.StaticMspt;
        }
    }
}