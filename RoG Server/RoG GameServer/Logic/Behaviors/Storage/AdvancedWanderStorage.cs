﻿using System.Numerics;

namespace GameServer.Logic.Behaviors.Storage;

internal class AdvancedWanderStorage
{
    internal Vector2 Destination;
    internal float RemainingCooldown;
}
