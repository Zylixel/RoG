﻿using System.Numerics;
using GameServer.Logic.Behaviors.Movement;

namespace GameServer.Logic.Behaviors.Storage;

internal class FollowPointStorage
{
    public FollowEnum State;
    public Vector2? Following;
    public float AttentionLeft;
}
