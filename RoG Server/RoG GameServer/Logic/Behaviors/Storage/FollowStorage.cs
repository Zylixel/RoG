﻿using GameServer.Logic.Behaviors.Movement;
using GameServer.Realm.Entities;

namespace GameServer.Logic.Behaviors.Storage;

internal struct FollowStorage
{
    public FollowEnum State;
    public Entity? Following;
    public float AttentionLeft;
}
