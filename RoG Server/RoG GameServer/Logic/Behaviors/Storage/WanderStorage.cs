﻿using System.Numerics;

namespace GameServer.Logic.Behaviors.Storage;

internal class WanderStorage
{
    internal Vector2 Direction;
    internal float RemainingDistance;
    internal float RemainingCooldown;
}
