﻿using GameServer.Logic.Engine;
using GameServer.Realm.Entities;
using GameServer.Realm.Entities.Enemy;

namespace GameServer.Logic.Behaviors;

internal class Suicide : Behavior
{
    internal override void TickCore(Entity host)
    {
        if (host is not Enemy enemy)
        {
            throw new NotSupportedException("Use Decay instead");
        }

        enemy.Death();
    }
}