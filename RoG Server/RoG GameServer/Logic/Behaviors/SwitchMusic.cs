﻿using GameServer.Logic.Engine;
using GameServer.Realm.Entities;

namespace GameServer.Logic.Behaviors;

internal class SwitchMusic(string musicName) : Behavior
{
    internal override void OnStateEntry(Entity host)
    {
        host.Owner.SwitchMusic(musicName);
    }
}