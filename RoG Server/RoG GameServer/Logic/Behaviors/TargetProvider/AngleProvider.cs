﻿using System.Numerics;
using GameServer.Realm.Entities;
using RoGCore.Models;

namespace GameServer.Logic.Behaviors.TargetProvider;

internal readonly struct AngleProvider(Angle angle) : ITargetProvider
{
    internal static readonly AngleProvider Zero = new(Angle.Zero);
    internal static readonly AngleProvider Pi = new(Angle.Pi);

    [Obsolete]
    public Entity? GetTarget(Entity host)
    {
        throw new NotSupportedException();
    }

    [Obsolete]
    public Vector2? GetPosition(Entity host)
    {
        throw new NotSupportedException();
    }

    public Target GetAll(Entity host)
    {
        return new(null, null, angle);
    }
}