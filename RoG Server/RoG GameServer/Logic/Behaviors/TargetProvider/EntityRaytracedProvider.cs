﻿using System.Numerics;
using GameServer.Realm;
using GameServer.Realm.Entities;
using RoGCore.Models;

namespace GameServer.Logic.Behaviors.TargetProvider;

internal readonly struct EntityRaytracedProvider(string entityName, float acquireRange = ITargetProvider.DEFAULTACQUIRERANGE) : ITargetProvider
{
    public Entity? GetTarget(Entity host)
    {
        return host.GetRaytraced(host.InRange<Entity>(acquireRange, EntityUtils.NamePredicate(entityName)));
    }

    public Vector2? GetPosition(Entity host)
    {
        return GetTarget(host)?.Position;
    }

    public Target GetAll(Entity host)
    {
        var target = GetTarget(host);
        var position = target?.Position;
        var angle = target is null ? null : new Angle(Math.Atan2(target.Y - host.Y, target.X - host.X));
        return new Target(target, position, angle);
    }
}