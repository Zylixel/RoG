﻿using System.Numerics;
using GameServer.Realm;
using GameServer.Realm.Entities;
using RoGCore.Models;

namespace GameServer.Logic.Behaviors.TargetProvider;

public readonly struct GroupProvider : ITargetProvider
{
    internal readonly string[] Groups;
    private readonly float acquireRange;

    public GroupProvider(string[] group, float acquireRange)
    {
        Groups = group;
        this.acquireRange = acquireRange;
    }

    public GroupProvider(GroupProvider provider, float acquireRange)
    {
        Groups = provider.Groups;
        this.acquireRange = acquireRange;
    }

    public GroupProvider(string[] group)
    {
        Groups = group;
        acquireRange = ITargetProvider.DEFAULTACQUIRERANGE;
    }

    public Entity? GetTarget(Entity host)
    {
        return host.GetNearest<Entity>(acquireRange, EntityUtils.GroupsPredicate(Groups));
    }

    public Vector2? GetPosition(Entity host)
    {
        return GetTarget(host)?.Position;
    }

    public Target GetAll(Entity host)
    {
        var target = GetTarget(host);
        var position = target?.Position;
        var angle = target is null ? null : new Angle(Math.Atan2(target.Y - host.Y, target.X - host.X));
        return new Target(target, position, angle);
    }
}