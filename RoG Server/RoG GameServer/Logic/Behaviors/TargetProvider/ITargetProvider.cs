﻿using System.Numerics;
using GameServer.Realm.Entities;

namespace GameServer.Logic.Behaviors.TargetProvider;

public interface ITargetProvider
{
    public const float DEFAULTACQUIRERANGE = 11f;

    Entity? GetTarget(Entity host);

    Vector2? GetPosition(Entity host);

    Target GetAll(Entity host);
}