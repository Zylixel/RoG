﻿using System.Numerics;
using GameServer.Realm.Entities;
using RoGCore.Models;

namespace GameServer.Logic.Behaviors.TargetProvider;

internal readonly struct OffsetProvider(ITargetProvider targetProvider, Vector2 offset) : ITargetProvider
{
    public Entity? GetTarget(Entity host)
    {
        throw new NotSupportedException();
    }

    public Vector2? GetPosition(Entity host)
    {
        return targetProvider.GetPosition(host) + offset;
    }

    public Target GetAll(Entity host)
    {
        var target = targetProvider.GetAll(host);

        var position = target.Position + offset;

        var angle = position is null
            ? null
            : new Angle(Math.Atan2(position.Value.Y - host.Y, position.Value.X - host.X));

        return new Target(target.Entity, position, angle);
    }
}