﻿using System.Numerics;
using GameServer.Realm;
using GameServer.Realm.Entities;
using RoGCore.Models;

namespace GameServer.Logic.Behaviors.TargetProvider;

internal readonly struct PlayerPositionRandomProvider(float acquireRange, float randomRadius) : ITargetProvider
{
    public Entity? GetTarget(Entity host)
    {
        throw new NotSupportedException();
    }

    public Vector2? GetPosition(Entity host)
    {
        var playerPosition = host.GetNearest<Entity>(acquireRange, EntityUtils.PlayerOrSummonPredicate())?.Position;

        return playerPosition is null ? null : RandomPosition(playerPosition.Value, randomRadius);

        static Vector2 RandomPosition(Vector2 origin, float maximumDistance)
        {
            var angle = Random.Shared.Next(0, 360) * MathF.PI / 180f;
            var dist = Random.Shared.NextSingle() * maximumDistance;
            return origin + (new Vector2(MathF.Cos(angle), MathF.Sin(angle)) * dist);
        }
    }

    public Target GetAll(Entity host)
    {
        var position = GetPosition(host);
        var angle = position is null ? null : new Angle(Math.Atan2(position.Value.Y - host.Y, position.Value.X - host.X));
        return new Target(null, position, angle);
    }
}