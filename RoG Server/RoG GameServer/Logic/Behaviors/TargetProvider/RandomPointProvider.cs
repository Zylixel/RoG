﻿using System.Numerics;
using GameServer.Realm.Entities;
using RoGCore.Models;
using RoGCore.Utils;

namespace GameServer.Logic.Behaviors.TargetProvider;

internal readonly struct RandomPointProvider(
    float maximumDistance = ITargetProvider.DEFAULTACQUIRERANGE,
    bool ensureNotOccupied = false) : ITargetProvider
{
    internal static readonly RandomPointProvider Default = new(ITargetProvider.DEFAULTACQUIRERANGE, false);

    public Entity? GetTarget(Entity host)
    {
        throw new NotSupportedException();
    }

    public Vector2? GetPosition(Entity host)
    {
        if (ensureNotOccupied)
        {
            Vector2Int position;
            while (true)
            {
                position = RandomPosition(host.Position, maximumDistance);

                if (!host.Owner.TryGetTile(position, out var tile))
                {
                    continue;
                }

                if (tile!.Occupied(TileOccupency.NoWalk | TileOccupency.OccupyHalf))
                {
                    continue;
                }

                break;
            }

            return position;
        }
        else
        {
            return RandomPosition(host.Position, maximumDistance);
        }

        static Vector2Int RandomPosition(Vector2 origin, float maximumDistance)
        {
            var angle = Random.Shared.Next(0, 360) * MathF.PI / 180f;
            var dist = Random.Shared.NextSingle() * maximumDistance;
            return (origin + (new Vector2(MathF.Cos(angle), MathF.Sin(angle)) * dist)).ToInt();
        }
    }

    public Target GetAll(Entity host)
    {
        Entity? target = null;
        var position = GetPosition(host);
        var angle = position is null
            ? null
            : new Angle(Math.Atan2(position.Value.Y - host.Y, position.Value.X - host.X));
        return new Target(target, position, angle);
    }
}