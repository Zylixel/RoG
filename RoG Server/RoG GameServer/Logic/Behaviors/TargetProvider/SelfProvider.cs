﻿using System.Numerics;
using GameServer.Realm.Entities;
using RoGCore.Models;

namespace GameServer.Logic.Behaviors.TargetProvider;

internal readonly struct SelfProvider : ITargetProvider
{
    internal static readonly SelfProvider Default = default;

    public Entity? GetTarget(Entity host)
    {
        return host;
    }

    public Vector2? GetPosition(Entity host)
    {
        return host.Position;
    }

    public Target GetAll(Entity host)
    {
        var target = host;
        Vector2? position = host.Position;
        var angle = Angle.Zero;
        return new Target(target, position, angle);
    }
}