﻿using System.Numerics;
using GameServer.Realm.Entities;
using GameServer.Realm.Entities.Enemy;
using RoGCore.Models;

namespace GameServer.Logic.Behaviors.TargetProvider;

internal readonly struct SpawnPointProvider : ITargetProvider
{
    internal static readonly SpawnPointProvider Default = default;

    public Entity? GetTarget(Entity host)
    {
        throw new NotSupportedException();
    }

    public Vector2? GetPosition(Entity host)
    {
        return (host as Enemy)?.SpawnPoint;
    }

    public Target GetAll(Entity host)
    {
        var position = (host as Enemy)?.SpawnPoint;
        var angle = position is null
            ? null
            : new Angle(Math.Atan2(position.Value.Y - host.Y, position.Value.X - host.X));
        return new Target(null, position, angle);
    }
}