﻿using System.Numerics;
using GameServer.Realm.Entities;
using GameServer.Realm.Entities.GameObject;
using RoGCore.Models;

namespace GameServer.Logic.Behaviors.TargetProvider;

internal readonly struct SummonPositionProvider : ITargetProvider
{
    internal static readonly SummonPositionProvider Default = default;

    public Entity? GetTarget(Entity host)
    {
        throw new NotSupportedException();
    }

    public Vector2? GetPosition(Entity host)
    {
        return host is not Summon summon ? throw new ArgumentException("parameter /'host/' is not of type Summon") : (Vector2?)summon.DesiredPosition;
    }

    public Target GetAll(Entity host)
    {
        Entity? target = null;
        var position = GetPosition(host);
        var angle = position is null
            ? null
            : new Angle(Math.Atan2(position.Value.Y - host.Y, position.Value.X - host.X));
        return new Target(target, position, angle);
    }
}