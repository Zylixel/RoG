﻿using System.Numerics;

namespace GameServer.Logic.Behaviors.TargetProvider;

internal static class TargerProviderExtensions
{
    internal static OffsetProvider Offset(this ITargetProvider targetProvider, Vector2 offset)
    {
        return new OffsetProvider(targetProvider, offset);
    }
}