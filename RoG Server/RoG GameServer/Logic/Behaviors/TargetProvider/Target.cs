﻿using System.Numerics;
using GameServer.Realm.Entities;
using RoGCore.Models;

namespace GameServer.Logic.Behaviors.TargetProvider;

public readonly struct Target(Entity? entity, Vector2? position, Angle? angle)
{
    public Entity? Entity { get; } = entity;

    public Vector2? Position { get; } = position;

    public Angle? Angle { get; } = angle;
}