﻿using GameServer.Logic.Behaviors.TargetProvider;
using GameServer.Logic.Engine;
using GameServer.Realm.Entities;
using GameServer.Realm.Entities.Enemy;
using RoGCore.Models;
using RoGCore.Networking.Packets;

namespace GameServer.Logic.Behaviors;

internal class Taunt : StatefulBehavior<float?>
{
    private readonly bool broadcast;
    private readonly float probability = 1;
    private readonly string[] text;
    private readonly bool once;
    private Cooldown cooldown = new(0, 0);

    internal Taunt(params string[] text)
    {
        this.text = text;
    }

    internal Taunt(double probability, params string[] text)
    {
        this.text = text;
        this.probability = (float)probability;
    }

    internal Taunt(bool broadcast, params string[] text)
    {
        this.text = text;
        this.broadcast = broadcast;
    }

    internal Taunt(Cooldown cooldown, params string[] text)
    {
        this.text = text;
        this.cooldown = cooldown;
    }

    internal Taunt(Cooldown cooldown, bool once, params string[] text)
    {
        this.text = text;
        this.cooldown = cooldown;
        this.once = once;
    }

    internal Taunt(double probability, bool broadcast, params string[] text)
    {
        this.text = text;
        this.probability = (float)probability;
        this.broadcast = broadcast;
    }

    internal Taunt(double probability, Cooldown cooldown, params string[] text)
    {
        this.text = text;
        this.probability = (float)probability;
        this.cooldown = cooldown;
    }

    internal Taunt(bool broadcast, Cooldown cooldown, params string[] text)
    {
        this.text = text;
        this.broadcast = broadcast;
        this.cooldown = cooldown;
    }

    internal Taunt(double probability, bool broadcast, Cooldown cooldown, params string[] text)
    {
        this.text = text;
        this.probability = (float)probability;
        this.broadcast = broadcast;
        this.cooldown = cooldown;
    }

    internal override void TickCore(Entity host, ref float? cool)
    {
        if (cool != null && cooldown.IsNonZero())
        {
            return; // cooldown = 0 -> once per state entry
        }

        // get cooldown
        cool ??= cooldown.Next();
        cool -= host.Owner.CurrentTime.StaticMspt;

        // if cooldown is active
        if (cool > 0f)
        {
            return;
        }

        // if cooldown is finished and taunt is one-time
        else if (once)
        {
            cooldown = new Cooldown(0, 0);
        }

        // if cooldown is finished and taunt is multi-time
        else
        {
            cool = cooldown.Next();
        }

        if (probability > 0 && Random.Shared.NextDouble() >= probability)
        {
            return;
        }

        var taunt = text.Length == 1 ? text[0] : text[Random.Shared.Next(text.Length)];
        if (taunt.Contains("{PLAYER}"))
        {
            var player = PlayerProvider.Default.GetTarget(host);
            if (player is null)
            {
                return;
            }

            taunt = taunt.Replace("{PLAYER}", player.Name);
        }

        if (host is Enemy enemy)
        {
            taunt = taunt.Replace("{HP}", enemy.Hp.ToString());
        }

        Text packet = new Text(host.ObjectDescription.Name, host.Id, taunt, new RGB(0xFF0000), new RGB(0xFF0000));
        if (broadcast)
        {
            host.Owner.BroadcastPacket(packet);
        }
        else
        {
            host.Owner.BroadcastPacket(packet, host.Position);
        }
    }
}