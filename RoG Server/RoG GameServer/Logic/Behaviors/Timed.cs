﻿using GameServer.Logic.Engine;
using GameServer.Realm.Entities;

namespace GameServer.Logic.Behaviors;

internal class Timed(int period, IChildren behavior) : StatefulCycleBehavior<float>
{
    internal override void OnStateEntry(Entity host, ref float state)
    {
        behavior.OnStateEntry(host);
        state = period;
    }

    internal override void TickCore(Entity host, ref float state)
    {
        behavior.Tick(host);
        Status = CycleStatus.InProgress;
        state -= host.Owner.CurrentTime.StaticMspt;
        if (period <= 0)
        {
            state = period;
            Status = CycleStatus.Completed;

            if (behavior is Prioritize)
            {
                host.SetState(behavior, -1);
            }
        }
    }
}