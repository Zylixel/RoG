﻿using GameServer.Logic.Engine;
using GameServer.Realm.Entities;

namespace GameServer.Logic.Behaviors;

internal class TitleCard(string title) : Behavior
{
    internal override void OnStateEntry(Entity host)
    {
        host.Owner.BroadcastPacket(new RoGCore.Networking.Packets.TitleCard(title));
    }
}