﻿using System.Numerics;
using GameServer.Logic.Engine;
using GameServer.Realm.Entities;
using GameServer.Realm.World;
using RoGCore.Assets;
using RoGCore.Models;
using RoGCore.Networking.Packets;

namespace GameServer.Logic.Behaviors.Toss;

internal abstract class BaseTossObject : StatefulBehavior<float?>
{
    protected readonly string child;
    private readonly int tossTime;
    private readonly float coolDownOffset;
    private readonly Cooldown coolDown;
    private readonly bool ignoreStun;
    private readonly bool signal;

    private ObjectDescription childDescription;

    internal BaseTossObject(
        string child,
        int tossTime,
        Cooldown coolDown = default,
        float coolDownOffset = 0,
        bool ignoreStun = false,
        bool signal = false)
    {
        this.child = child;
        this.tossTime = tossTime;
        this.coolDown = coolDown;
        this.coolDownOffset = coolDownOffset;
        this.ignoreStun = ignoreStun;
        this.signal = signal;
        childDescription = new ObjectDescription();
    }

    internal override void OnStateEntry(Entity host, ref float? state)
    {
        childDescription = EmbeddedData.BetterObjects[child];
    }

    internal override void TickCore(Entity host, ref float? cool)
    {
        cool ??= coolDownOffset;
        if (cool <= 0)
        {
            if (!ignoreStun && host.HasConditionEffect(ConditionEffectIndex.Stunned))
            {
                return;
            }

            Toss(host);

            cool = coolDown.Next();
        }
        else
        {
            cool -= host.Owner.CurrentTime.StaticMspt;
        }
    }

    protected abstract void Toss(Entity host);

    protected void TossAt(Entity host, Vector2 position)
    {
        host.Owner.BroadcastPacket(new ShowEffect(ShowEffect.Type.Toss, host.Id, position, new Vector2(tossTime, 0), child));

        if (childDescription.ActivateEffects != null)
        {
            host.Owner.Timers.Add(new WorldTimer(
                tossTime + childDescription.LifeTime,
                (world, _) => TossFinished(host, world, position)));
        }
        else
        {
            host.Owner.Timers.Add(new WorldTimer(
                tossTime,
                (world, _) => TossFinished(host, world, position)));
        }
    }

    protected virtual void TossFinished(Entity host, World world, Vector2 target)
    {
        if (signal)
        {
            host.InvokeBehaviorEvent(BehaviorSignal.TossComplete);
        }

        if (childDescription.ActivateEffects is null)
        {
            return;
        }

        foreach (var activateEffect in childDescription.ActivateEffects)
        {
            switch (activateEffect.Effect)
            {
                case ActivateEffects.Explosion:
                    world.Explosion(
                        target,
                        activateEffect.Radius,
                        host,
                        activateEffect.Damage,
                        activateEffect.IgnoreDefense);

                    break;
            }
        }
    }
}