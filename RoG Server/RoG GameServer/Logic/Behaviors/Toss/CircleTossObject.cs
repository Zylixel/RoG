﻿using System.Numerics;
using GameServer.Logic.Engine;
using GameServer.Realm.Entities;
using RoGCore.Utils;

namespace GameServer.Logic.Behaviors.Toss;

internal class CircleTossObject : BaseTossObject
{
    private readonly int count;
    private readonly float radius;

    internal CircleTossObject(
        string child,
        int tossTime,
        int count,
        float radius,
        Cooldown coolDown = default,
        float coolDownOffset = 0,
        bool ignoreStun = false)
        : base(child, tossTime, coolDown, coolDownOffset, ignoreStun)
    {
        this.count = count;
        this.radius = radius;
    }

    protected override void Toss(Entity host)
    {
        var angleInc = MathF.Tau / count;
        float angle = 0;

        for (var i = 0; i < count; i++)
        {
            var target = host.Position + (new Vector2(MathF.Cos(angle), MathF.Sin(angle)) * radius);

            if (host.Owner.TryGetTile(target.ToInt(), out var tile) && !tile!.Occupied(RoGCore.Models.TileOccupency.OccupyHalf))
            {
                TossAt(host, target);
            }

            angle += angleInc;
        }
    }
}