﻿using System.Numerics;
using GameServer.Logic.Behaviors.TargetProvider;
using GameServer.Logic.Engine;
using GameServer.Realm.Entities;
using GameServer.Realm.Entities.Enemy;
using GameServer.Realm.World;

namespace GameServer.Logic.Behaviors.Toss;

internal class TossEntity : BaseTossObject
{
    private readonly ITargetProvider targetProvider;

    internal TossEntity(
        string child,
        int tossTime,
        ITargetProvider? targetProvider = null,
        Cooldown coolDown = default,
        float coolDownOffset = 0,
        bool ignoreStun = false,
        bool signal = false)
        : base(child, tossTime, coolDown, coolDownOffset, ignoreStun, signal)
    {
        this.targetProvider = targetProvider ?? PlayerProvider.Default;
    }

    protected override void Toss(Entity host)
    {
        var position = targetProvider.GetPosition(host);

        if (position is null)
        {
            return;
        }

        TossAt(host, position.Value);
    }

    // TODO: Why are we passing world in this method?
    protected override void TossFinished(Entity host, World world, Vector2 target)
    {
        var entity = Entity.Resolve(child, world);
        entity.Move(target);

        if (entity is Enemy enemy)
        {
            enemy.GivesItemXp = false;
        }

        base.TossFinished(host, world, target);
    }
}