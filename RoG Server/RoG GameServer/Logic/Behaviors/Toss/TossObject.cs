﻿using GameServer.Logic.Behaviors.TargetProvider;
using GameServer.Logic.Engine;
using GameServer.Realm.Entities;

namespace GameServer.Logic.Behaviors.Toss;

internal class TossObject : BaseTossObject
{
    private readonly ITargetProvider targetProvider;

    internal TossObject(
        string child,
        int tossTime,
        ITargetProvider? targetProvider = null,
        Cooldown coolDown = default,
        float coolDownOffset = 0,
        bool ignoreStun = false,
        bool signal = false)
        : base(child, tossTime, coolDown, coolDownOffset, ignoreStun, signal)
    {
        this.targetProvider = targetProvider ?? PlayerProvider.Default;
    }

    protected override void Toss(Entity host)
    {
        var position = targetProvider.GetPosition(host);

        if (position is null)
        {
            return;
        }

        TossAt(host, position.Value);
    }
}