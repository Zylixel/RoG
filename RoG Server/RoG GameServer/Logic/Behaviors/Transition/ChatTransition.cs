﻿using GameServer.Realm.Entities;

namespace GameServer.Logic.Behaviors.Transition;

internal class ChatTransition(string targetState, params string[] texts)
    : Engine.Transition(targetState)
{
    // TODO: not sure if this works with states
    private bool transition;

    internal void OnChatReceived(string text)
    {
        if (texts.Contains(text))
        {
            transition = true;
        }
    }

    internal override bool TickCore(Entity host)
    {
        return transition;
    }
}