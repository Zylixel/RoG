﻿using GameServer.Realm.Entities;
using GameServer.Realm.Entities.Enemy;

namespace GameServer.Logic.Behaviors.Transition;

internal class DamageTakenTransition(int damage, string targetState)
    : Engine.Transition(targetState)
{
    internal override bool TickCore(Entity host)
    {
        if (host is not Enemy enemy)
        {
            return false;
        }

        var damageSoFar = 0;
        foreach (var i in enemy.DamageCounter.GetPlayerData())
        {
            damageSoFar += i.Value;

            if (damageSoFar >= damage)
            {
                return true;
            }
        }

        return false;
    }
}