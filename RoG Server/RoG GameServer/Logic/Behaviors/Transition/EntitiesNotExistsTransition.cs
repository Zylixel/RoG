﻿using GameServer.Realm;
using GameServer.Realm.Entities;

namespace GameServer.Logic.Behaviors.Transition;

internal class EntitiesNotExistsTransition : Engine.Transition
{
    private readonly float distance;
    private readonly Predicate<Entity>[] predicates;

    public EntitiesNotExistsTransition(
        float dist,
        string targetState,
        params string[] childrens)
        : base(targetState)
    {
        distance = dist;
        predicates = new Predicate<Entity>[childrens.Length];
        for (var i = 0; i < childrens.Length; i++)
        {
            predicates[i] = EntityUtils.NamePredicate(childrens[i]);
        }
    }

    internal override bool TickCore(Entity host)
    {
        return !host.InRange<Entity>(distance, Or(predicates)).Any();
    }

    private static Predicate<T> Or<T>(Predicate<T>[] predicates)
    {
        return (T item) =>
        {
            foreach (var predicate in predicates)
            {
                if (predicate(item))
                {
                    return true;
                }
            }

            return false;
        };
    }
}