﻿using GameServer.Realm;
using GameServer.Realm.Entities;

namespace GameServer.Logic.Behaviors.Transition;

internal class EntityExistsTransition(string target, float dist, string targetState)
    : Engine.Transition(targetState)
{
    internal override bool TickCore(Entity host)
    {
        return host.InRange<Entity>(dist, EntityUtils.NamePredicate(target)).Any();
    }
}