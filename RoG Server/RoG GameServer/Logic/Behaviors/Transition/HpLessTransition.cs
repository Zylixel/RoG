﻿using GameServer.Realm.Entities;
using GameServer.Realm.Entities.Enemy;

namespace GameServer.Logic.Behaviors.Transition;

internal class HpLessTransition(float threshold, string targetState, bool once = false)
    : Engine.StatefulTransition<bool>(targetState)
{
    internal override bool TickCore(Entity host, ref bool alreadyTriggered)
    {
        if (host is not Enemy enemy || (once && alreadyTriggered))
        {
            return false;
        }

        var ret = threshold > 1f
                ? enemy.Hp < threshold
                : (float)enemy.Hp / enemy.MaximumHp < threshold;

        if (ret)
        {
            alreadyTriggered = true;
        }

        return ret;
    }
}