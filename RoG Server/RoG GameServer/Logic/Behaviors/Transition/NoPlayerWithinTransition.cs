﻿using GameServer.Realm;
using GameServer.Realm.Entities;
using GameServer.Realm.Entities.Player;

namespace GameServer.Logic.Behaviors.Transition;

internal class NoPlayerWithinTransition(float dist, string targetState)
    : Engine.Transition(targetState)
{
    internal override bool TickCore(Entity host)
    {
        return !host.InRange<Player>(dist).Any();
    }
}