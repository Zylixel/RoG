﻿using GameServer.Logic.Engine;
using GameServer.Realm.Entities;

namespace GameServer.Logic.Behaviors.Transition;

internal sealed class SignaledTransition(BehaviorSignal signal, string targetState)
    : Engine.Transition(targetState)
{
    private readonly HashSet<Entity> signaled = [];

    internal override void OnStateEntry(Entity host)
    {
        host.BehaviorSignalEvent += OnBehaviorSignal;
    }

    internal override bool TickCore(Entity host)
    {
        if (signaled.Contains(host))
        {
            signaled.Remove(host);
            return true;
        }

        return false;
    }

    internal override void OnStateExit(Entity host)
    {
        host.BehaviorSignalEvent -= OnBehaviorSignal;
    }

    private void OnBehaviorSignal(Entity sender, BehaviorSignal signalType)
    {
        if (signalType == signal && sender is Entity entity)
        {
            signaled.Add(entity);
        }
    }
}