﻿using GameServer.Logic.Engine;
using GameServer.Realm.Entities;

namespace GameServer.Logic.Behaviors.Transition;

internal class TimedTransition(Cooldown cooldown, params string[] states)
    : StatefulTransition<float>(states)
{
    internal override bool TickCore(Entity host, ref float cool)
    {
        if (cool <= 0)
        {
            cool = cooldown.Next();
            selectedState = Random.Shared.Next(TargetStates.Length);
            return true;
        }

        cool -= host.Owner.CurrentTime.StaticMspt;
        return false;
    }
}