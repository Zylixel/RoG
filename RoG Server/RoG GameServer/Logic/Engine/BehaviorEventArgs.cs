﻿using GameServer.Realm.Entities;

namespace GameServer.Logic.Engine;

internal class BehaviorEventArgs(Entity host) : EventArgs
{
    internal Entity Host { get; init; } = host;
}