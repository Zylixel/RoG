﻿namespace GameServer.Logic.Engine;

internal enum BehaviorSignal : byte
{
    GotoComplete,
    TossComplete,
}
