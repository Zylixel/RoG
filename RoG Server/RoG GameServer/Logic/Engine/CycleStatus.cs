﻿namespace GameServer.Logic.Engine;

public enum CycleStatus
{
    NotStarted,
    InProgress,
    Completed,
}
