﻿using GameServer.Logic.Behaviors;
using GameServer.Logic.Behaviors.Condition;
using GameServer.Logic.Behaviors.Movement;
using GameServer.Logic.Behaviors.Shoot;
using GameServer.Logic.Behaviors.TargetProvider;
using GameServer.Logic.Behaviors.Transition;
using GameServer.Logic.Loot;
using RoGCore.Models;

namespace GameServer.Logic.Engine.Monsters;

public static partial class BehaviorDb
{
    public static readonly Engine.BehaviorDb.BehaviorList Beach = () => Engine.BehaviorDb.Behav()
        .Init(
            "Beach Snake",
            new State(
                new DropPortalOnDeath("Snake Den Portal", 50),
                new State(
                    "VenomShoot",
                    new If(
                        new InCombat(),
                        new Prioritize(
                                new StayBack(0.75f, 4.5f, BeachGroupProvider),
                                new Wander(0.4f, period: 250, cooldown: 0)),
                        new Shoot(
                            coolDown: 1500,
                            targetProvider: new GroupProvider(["Player", "Swamp", "Jungle"], 7f),
                            count: 1,
                            coolDownOffset: 500),
                        new TimedTransition(8000, "Bite"))
                    .Else(
                        new Prioritize(
                                new Wander(0.3f)))),
                new State(
                    "Bite",
                    new FollowEntity(5f, BeachGroupProvider, 1.5f),
                    new Shoot(
                        0,
                        targetProvider: new GroupProvider(["Player", "Swamp", "Jungle"], 4f),
                        projectileIndex: 1,
                        coolDownOffset: 100),
                    new TimedTransition(300, "VenomShoot"))),
            new TierLoot(1, ItemType.Weapon, 0.05f),
            new TierLoot(1, ItemType.Garment, 0.05f),
            new TierLoot(1, ItemType.Ring, 0.05f))
        .Init(
            "Sand Man",
            new State(
                new State(
                    "do",
                    new Wander(0.3f),
                    new FollowEntity(0.3f, BeachGroupProvider, 2f),
                    new Shoot(coolDown: 1000, targetProvider: new GroupProvider(["Player", "Swamp", "Jungle"], 9f), count: 1))),
            new TierLoot(1, ItemType.Weapon, 0.05f),
            new TierLoot(1, ItemType.Garment, 0.05f),
            new TierLoot(1, ItemType.Ring, 0.05f));

    private static readonly GroupProvider BeachGroupProvider = new(["Player", "Jungle", "Swamp"]);
}