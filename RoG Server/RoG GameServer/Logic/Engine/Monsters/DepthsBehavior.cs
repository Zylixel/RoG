﻿using GameServer.Logic.Behaviors;
using GameServer.Logic.Behaviors.Movement;
using GameServer.Logic.Behaviors.Shoot;
using GameServer.Logic.Behaviors.TargetProvider;
using GameServer.Logic.Behaviors.Transition;
using GameServer.Logic.Loot;
using RoGCore.Models;

namespace GameServer.Logic.Engine.Monsters;

public static partial class BehaviorDb
{
    public static readonly Engine.BehaviorDb.BehaviorList Depths = () => Engine.BehaviorDb.Behav()
        .Init(
            "Depths Scorpion",
            new State(
                new State(
                    "Shoot",
                    new Wander(0.25f),
                    new FollowEntity(1f, new PlayerProvider(6f), 1f),
                    new Shoot(coolDown: 500, count: 3, shootAngle: 8f, predictive: 0.5f))),
            new TierLoot(1, ItemType.Weapon, 0.15),
            new TierLoot(1, ItemType.Garment, 0.15),
            new TierLoot(1, ItemType.Ring, 0.15))
        .Init(
            "Depths Lion",
            new State(
                new State(
                    "Shoot",
                    new Wander(0.25f),
                    new FollowEntity(0.75f, new PlayerProvider(3f), 3f),
                    new Shoot(coolDown: 1000, count: 1, predictive: 0.5f))),
            new TierLoot(1, ItemType.Weapon, 0.15),
            new TierLoot(1, ItemType.Garment, 0.15),
            new TierLoot(1, ItemType.Ring, 0.15))
        .Init(
            "Depths Knight",
            new State(
                new State(
                    "Shoot",
                    new AddCond(ConditionEffectIndex.Armored),
                    new Wander(0.25f),
                    new FollowEntity(0.5f, new PlayerProvider(6f), 3f),
                    new Shoot(coolDown: 900, count: 1, predictive: 0.5f))),
            new TierLoot(1, ItemType.Weapon, 0.15),
            new TierLoot(1, ItemType.Garment, 0.15),
            new TierLoot(1, ItemType.Ring, 0.15))
        .Init(
            "Depths Dragon",
            new State(
                new State(
                    "Shoot",
                    new Wander(0.25f),
                    new FollowEntity(0.75f, new PlayerProvider(6f), 3f),
                    new Shoot(coolDown: 750, count: 3, shootAngle: 4, predictive: 0.5f))),
            new TierLoot(1, ItemType.Weapon, 0.15),
            new TierLoot(1, ItemType.Garment, 0.15),
            new TierLoot(1, ItemType.Ring, 0.15))
        .Init(
            "The Beast",
            new State(
                new HpLessTransition(0.4f, "PreOrbit", true),
                new OnDeathBehavior(new SetLight(0.1f)),
                new DropPortalOnDeath("Exit Portal", 100, portalDespawnTimeSec: 1000),
                new State(
                    "Await",
                    new AddCond(ConditionEffectIndex.Invincible, true),
                    new PlayerWithinTransition(8, "Awake")),
                new State(
                    "Awake",
                    new TitleCard(@"The
Beast"),
                    new Flash(new ARGB(0xFFFFFF), 2, 5000),
                    new TimedTransition(4000, "StageOne")),
                new State(
                    "StageOne",
                    new RemCond(ConditionEffectIndex.Invincible),
                    new TimedTransition(0, "StageOneCircler", "StageOneSprayAttack", "StageOneSitAttack")),
                new State(
                    "StageOneCircler",
                    new State(
                        new Goto(4f, SpawnPointProvider.Default, signal: true),
                        new SignaledTransition(BehaviorSignal.GotoComplete, "StageOneCirclerMain")),
                    new State(
                        "StageOneCirclerMain",
                        new CircleShoot(
                            6,
                            angleIncrement: (float)Math.PI / 40,
                            offsetProjectiles: true,
                            coolDown: 50),
                        new TimedTransition(2000, "StageOneSitAttack", "StageOneSprayAttack"))),
                new State(
                    "StageOneSprayAttack",
                    new Wander(1.5f, period: new Cooldown(3000, 1500), cooldown: 0),
                    new Shoot(100, PlayerProvider.Infinite, count: 2, shootAngle: 0, projectileIndex: 4),
                    new Shoot(200, PlayerProvider.Infinite, projectileIndex: 5, coolDownOffset: 50),
                    new TimedTransition(2000, "StageOneSitAttack", "StageOneCircler")),
                new State(
                    "StageOneSitAttack",
                    new State(
                        new Flash(new ARGB(0xFF0000), 5, 3000),
                        new Goto(6f, PlayerProvider.Infinite, signal: true, maxDist: 0.25f),
                        new TimedTransition(500, "StageOneSitAttackMain"),
                        new SignaledTransition(BehaviorSignal.GotoComplete, "StageOneSitAttackMain")),
                    new State(
                        "StageOneSitAttackMain",
                        new DepthsInShoot(),
                        new TimedTransition(1500, "StageOneSprayAttack", "StageOneCircler"))),
                new State(
                    "PreOrbit",
                    new Goto(1.5f, SpawnPointProvider.Default, signal: true),
                    new SetLight(0f),
                    new SignaledTransition(BehaviorSignal.GotoComplete, "Orbit")),
                new State(
                    "Orbit",
                    FlowerPattern![0],
                    FlowerPattern[1],
                    new CircleShoot(
                        8,
                        1,
                        (float)Math.PI / 15,
                        offsetProjectiles: true,
                        coolDown: 1000),
                    new HpLessTransition(0.2f, "Rage")),
                new State(
                    "Rage",
                    new AddCond(ConditionEffectIndex.Invincible, true),
                    new Flash(new ARGB(0xFF0000), 3, 5000),
                    new Timed(3000, new DepthsRemoveLight()),
                    new TimedTransition(5000, "Final")),
                new State(
                    "Final",
                    new RemCond(ConditionEffectIndex.Invincible),
                    FlowerPattern[0],
                    FlowerPattern[1],
                    new CircleShoot(
                        8,
                        1,
                        (float)Math.PI / 15,
                        offsetProjectiles: true,
                        coolDown: 400))),
            new TierLoot(1, ItemType.Weapon, 0.2),
            new TierLoot(1, ItemType.Garment, 0.2),
            new TierLoot(1, ItemType.Ring, 0.2),
            new ItemLoot("Nightmare Bow", 0.25),
            new ItemLoot("Demon Hide", 0.25));

    private static readonly IChildren[] FlowerPattern =
    [
        new CircleShoot(
        4,
        2,
        (float)Math.PI / 30f,
        offsetProjectiles: true,
        coolDown: 100),
        new CircleShoot(
        4,
        3,
        (float)Math.PI / 30f,
        offsetProjectiles: true,
        missInterval: 20,
        missCount: 5,
        coolDown: 150),
    ];
}