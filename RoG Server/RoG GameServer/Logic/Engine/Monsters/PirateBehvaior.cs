﻿using GameServer.Logic.Behaviors;
using GameServer.Logic.Behaviors.Movement;
using GameServer.Logic.Behaviors.Shoot;
using GameServer.Logic.Behaviors.TargetProvider;
using GameServer.Logic.Behaviors.Toss;
using GameServer.Logic.Behaviors.Transition;
using GameServer.Logic.Loot;
using GameServer.Realm.Terrain.Tile;
using RoGCore.Models;

namespace GameServer.Logic.Engine.Monsters;

public static partial class BehaviorDb
{
    public static readonly Engine.BehaviorDb.BehaviorList Pirate = () => Engine.BehaviorDb.Behav()
        .Init(
            "Pirate",
            new State(
                new DropPortalOnDeath("Reedoshark's Quarters Portal", 50),
                new State(
                    new Wander(0.3f),
                    new StayCloseToSpawn(0.3f),
                    new Shoot(coolDown: 2000, targetProvider: new PlayerProvider(9f), count: 1))),
            new TierLoot(1, ItemType.Weapon, 0.1f),
            new TierLoot(1, ItemType.Garment, 0.1f),
            new TierLoot(1, ItemType.Ring, 0.1f))
        .Init(
            "Pirate Sailor",
            new State(
                new State(
                    new Wander(0.4f, period: 250, cooldown: 0),
                    new Prioritize(
                        new FollowEntity(1.2f, PlayerRaytracedProvider.Default, 3f),
                        new FollowEntity(1.2f, new EntityRaytracedProvider("Pirate Captain", 5f), 3)),
                    new Shoot(coolDown: 1500, targetProvider: new PlayerRaytracedProvider(9f), count: 1))),
            new TierLoot(1, ItemType.Weapon, 0.1f),
            new TierLoot(1, ItemType.Garment, 0.1f),
            new TierLoot(1, ItemType.Ring, 0.1f))
        .Init(
            "Pirate Brawler",
            new State(
                new State(
                    new Wander(0.4f, period: 250, cooldown: 0),
                    new Prioritize(
                        new FollowEntity(1.6f, PlayerRaytracedProvider.Default, 1f),
                        new FollowEntity(1.6f, new EntityRaytracedProvider("Pirate Captain", 5f), 1f)),
                    new Shoot(coolDown: 1000, targetProvider: new PlayerRaytracedProvider(9f), count: 1))),
            new TierLoot(1, ItemType.Weapon, 0.1f),
            new TierLoot(1, ItemType.Garment, 0.1f),
            new TierLoot(1, ItemType.Ring, 0.1f))
        .Init(
            "Pirate Captain",
            new State(
                new State(
                    new Wander(0.4f, period: 250, cooldown: 0),
                    new FollowEntity(1f, PlayerRaytracedProvider.Default, 3.5f),
                    new Shoot(750, targetProvider: new PlayerRaytracedProvider(9f), count: 1))),
            new TierLoot(1, ItemType.Weapon, 0.15f),
            new TierLoot(1, ItemType.Garment, 0.15f),
            new TierLoot(1, ItemType.Ring, 0.15f))
        .Init(
            "Small Cannon",
            new State(
                new State("Wait"),
                new State(
                    "Shoot",
                    new CannonShoot(new Cooldown(4000, 0), new EntityProvider("Anchor", 1.5f)),
                    new CannonShoot(new Cooldown(4000, 0), new EntityProvider("Anchor", 1.5f), cooldownOffset: 150),
                    new CannonShoot(new Cooldown(4000, 0), new EntityProvider("Anchor", 1.5f), cooldownOffset: 300)),
                new State(
                    "Last Stand",
                    new Shoot(new Cooldown(6000, 1000), new EntityProvider("Anchor", 1.5f), projectileIndex: 1, coolDownOffset: new Cooldown(3000, 3000)),
                    new Shoot(7000, new EntityProvider("Anchor", 1.5f), coolDownOffset: new Cooldown(3500, 3500))),
                new State(
                    "FullShoot",
                    new Shoot(new Cooldown(1500, 500), new EntityProvider("Anchor", 1.5f), projectileIndex: 1, coolDownOffset: new Cooldown(750, 750)))))
        .Init(
            "Reedoshark the Pirate King",
            new State(
                new DropPortalOnDeath("Exit Portal", 100, portalDespawnTimeSec: 1000),
                new OnDeathBehavior(
                    new SwitchMusic(Program.AppSettings.GameServer.Music.ReedosharksQuarters),
                    new Order(float.MaxValue, "Small Cannon", "Wait")),
                new State(
                    new Wander(0.3f),
                    new StayCloseToSpawn(0.1f),
                    new AddCond(ConditionEffectIndex.Invincible, true),
                    new PlayerWithinTransition(12, "Greet")),
                new State(
                    "Greet",
                    new TitleCard(
                        @"Reedoshark
The
Pirate King"),
                    new SwitchMusic(Program.AppSettings.GameServer.Music.ReedosharkBattle),
                    new TimedTransition(4000, "Close")),
                new State(
                    "Close",
                    new RegionBlock(TileRegion.Hallway1, "Wooden Wall"),
                    new RemCond(ConditionEffectIndex.Invincible),
                    new TimedTransition(100, "BombTosses", "RunAndCannons", "Cannons", "StayBackAndToss")),
                new State(
                    "Normal Attacks",
                    new HpLessTransition(0.15f, "Last Stand"),
                    new State(
                    "BombTosses",
                    new Goto(2f, default(SpawnPointProvider), true, true),
                    new Order(float.MaxValue, "Small Cannon", "Wait"),
                    new State(
                        new SignaledTransition(BehaviorSignal.GotoComplete, "BombTossesStartToss")),
                    new State(
                        "BombTossesStartToss",
                        new WakiShoot(new PlayerProvider(5), count: 3, predictive: 1f, coolDown: 1500, range: 3),
                        new CircleTossObject("Small Bomb", 1500, 30, 10, 10000, 300),
                        new CircleTossObject("Small Bomb", 1500, 20, 8, 10000, 500),
                        new CircleTossObject("Small Bomb", 1500, 16, 6, 10000, 700),
                        new CircleTossObject("Small Bomb", 1500, 14, 4, 10000, 900),
                        new TimedTransition(new Cooldown(4500, 0), "RunAndCannons", "Cannons", "StayBackAndToss"))),
                    new State(
                    "RunAndCannons",
                    new Wander(1.25f, period: new Cooldown(2000, 3000), cooldown: 0),
                    new WakiShoot(new PlayerProvider(5), count: 3, predictive: 1f, coolDown: 1500, range: 3),
                    new TossObject("Small Bomb", 2000, PlayerProvider.Infinite, 1500),
                    new TossObject("Small Bomb", 2000, new RandomPointProvider(ensureNotOccupied: true), 750),
                    new Order(float.MaxValue, "Small Cannon", "Shoot"),
                    new TimedTransition(new Cooldown(12500, 2500), "BombTosses", "Cannons", "StayBackAndToss")),
                    new State(
                    "StayBackAndToss",
                    new Order(float.MaxValue, "Small Cannon", "Wait"),
                    new FollowEntity(2f, PlayerProvider.Infinite, distance: 8),
                    new Wander(0.5f, cooldown: 100),
                    new WakiShoot(PlayerProvider.Infinite, count: 3, predictive: 1f, coolDown: 250, range: 1.5f),
                    new TossObject("Small Bomb Fast", 750, new PlayerPositionRandomProvider(acquireRange: 12, randomRadius: 2), 1500),
                    new TossObject("Small Bomb Fast", 750, new PlayerPositionRandomProvider(acquireRange: 12, randomRadius: 2), 1500),
                    new TossObject("Small Bomb Fast", 750, new PlayerPositionRandomProvider(acquireRange: 12, randomRadius: 2), 1500),
                    new TossObject("Small Bomb Fast", 750, new PlayerPositionRandomProvider(acquireRange: 12, randomRadius: 2), 1500),
                    new TossObject("Small Bomb Fast", 750, new PlayerPositionRandomProvider(acquireRange: 12, randomRadius: 2), 1500),
                    new TossObject("Small Bomb Fast", 750, new PlayerPositionRandomProvider(acquireRange: 12, randomRadius: 2), 1500),
                    new TimedTransition(new Cooldown(12500, 2500), "BombTosses", "RunAndCannons", "Cannons")),
                    new State(
                    "Cannons",
                    new Goto(2f, default(SpawnPointProvider), true, true),
                    new Order(float.MaxValue, "Small Cannon", "Wait"),
                    new State(
                        new SignaledTransition(BehaviorSignal.GotoComplete, "Cannons Start")),
                    new State(
                        "Cannons Start",
                        new Order(float.MaxValue, "Small Cannon", "FullShoot"),
                        new TimedTransition(new Cooldown(8000, 2500), "Cannons Cooldown")),
                    new State(
                            "Cannons Cooldown",
                            new Order(float.MaxValue, "Small Cannon", "Wait"),
                            new TimedTransition(2000, "BombTosses", "RunAndCannons", "StayBackAndToss")))),
                new State(
                    "Last Stand",
                    new Order(float.MaxValue, "Small Cannon", "Wait"),
                    new Goto(2f, default(SpawnPointProvider), true, true),
                    new State(
                        new SignaledTransition(BehaviorSignal.GotoComplete, "Last Stand Attack")),
                    new State(
                        "Last Stand Attack",
                        new Wander(1.25f, period: new Cooldown(2000, 3000), cooldown: 0),
                        new Order(float.MaxValue, "Small Cannon", "Last Stand"),
                        new TossObject("Small Bomb Fast", 1250, new PlayerPositionRandomProvider(acquireRange: 12, randomRadius: 2), 2500),
                        new TossObject("Small Bomb Fast", 1250, new PlayerPositionRandomProvider(acquireRange: 12, randomRadius: 2), 2500),
                        new TossObject("Small Bomb Fast", 1250, new PlayerPositionRandomProvider(acquireRange: 12, randomRadius: 2), 2500),
                        new TossObject("Small Bomb Fast", 1250, new PlayerPositionRandomProvider(acquireRange: 12, randomRadius: 2), 2500),
                        new WakiShoot(new PlayerProvider(5), count: 3, predictive: 1f, coolDown: 2000, range: 3),
                        new CircleTossObject("Small Bomb", 1500, 20, 8, 5000, 500)))),
            new TierLoot(1, ItemType.Weapon, 0.2f),
            new TierLoot(1, ItemType.Garment, 0.2f),
            new TierLoot(1, ItemType.Ring, 0.2f),
            new ItemLoot("Pirate Cutlass", 0.2f),
            new ItemLoot("Dread Stump", 0.2f));
}