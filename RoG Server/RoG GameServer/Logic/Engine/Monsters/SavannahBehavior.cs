﻿using GameServer.Logic.Behaviors;
using GameServer.Logic.Behaviors.Movement;
using GameServer.Logic.Behaviors.Shoot;
using GameServer.Logic.Behaviors.TargetProvider;
using GameServer.Logic.Loot;
using RoGCore.Models;

namespace GameServer.Logic.Engine.Monsters;

public static partial class BehaviorDb
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "HAA0101:Array allocation for params parameter", Justification = "Allocation only occurs at startup")]
    public static readonly Engine.BehaviorDb.BehaviorList Savannah = () => Engine.BehaviorDb.Behav()
        .Init(
            "Savannah Wizard",
            new State(
                new State(
                    "Wander",
                    new Wander(0.35f),
                    new StayBack(0.5f, 4.5f, DefaultSavannahGroup),
                    new Shoot(coolDown: 1750, targetProvider: DefaultSavannahGroup, count: 1))),
            new TierLoot(1, ItemType.Weapon, 0.15f),
            new TierLoot(1, ItemType.Garment, 0.15f),
            new TierLoot(1, ItemType.Ring, 0.15f))
        .Init(
            "Savannah Bandit",
            new State(
                new State(
                    "Wander",
                    new Wander(0.4f),
                    new FollowEntity(0.9f, DefaultSavannahGroup, 1.25f),
                    new Shoot(coolDown: 90, targetProvider: DefaultSavannahGroup, count: 1))),
            new TierLoot(1, ItemType.Weapon, 0.15f),
            new TierLoot(1, ItemType.Garment, 0.15f),
            new TierLoot(1, ItemType.Ring, 0.15f))
        .Init(
            "Small Reaper",
            new State(
                new State(
                    "Wander",
                    new Wander(0.35f),
                    new FollowEntity(0.5f, DefaultSavannahGroup, 2.5f),
                    new Shoot(
                        coolDown: 200,
                        targetProvider: RandomPointProvider.Default,
                        count: 2,
                        projectileIndex: 1,
                        targetGroups: DefaultSavannahGroup),
                    new Shoot(
                        coolDown: 400,
                        targetProvider: new AngleProvider(new Angle(15 * Math.PI / 180)),
                        count: 7,
                        coolDownOffset: 0,
                        targetGroups: DefaultSavannahGroup),
                    new Shoot(
                        coolDown: 400,
                        targetProvider: new AngleProvider(new Angle(30 * Math.PI / 180)),
                        count: 7,
                        coolDownOffset: 100,
                        targetGroups: DefaultSavannahGroup),
                    new Shoot(
                        coolDown: 400,
                        targetProvider: new AngleProvider(new Angle(45 * Math.PI / 180)),
                        count: 7,
                        coolDownOffset: 200,
                        targetGroups: DefaultSavannahGroup),
                    new Shoot(
                        coolDown: 400,
                        targetProvider: new AngleProvider(new Angle(60 * Math.PI / 180)),
                        count: 7,
                        coolDownOffset: 300,
                        targetGroups: DefaultSavannahGroup))),
            new TierLoot(1, ItemType.Weapon, 0.2f),
            new TierLoot(1, ItemType.Garment, 0.2f),
            new TierLoot(1, ItemType.Ring, 0.2f))
        .Init(
            "Savannah Priest",
            new State(
                new State(
                    "Wander",
                    new Wander(0.35f),
                    new StayBack(0.5f, 5f, DefaultSavannahGroup),
                    new Heal(10, 200, ["Savannah"], 500),
                    new Heal(10, 100, ["Self"], 500))),
            new TierLoot(1, ItemType.Weapon, 0.15f),
            new TierLoot(1, ItemType.Garment, 0.15f),
            new TierLoot(1, ItemType.Ring, 0.15f))
        .Init(
            "Savannah Archer",
            new State(
                new State(
                    "Wander",
                    new Wander(0.35f),
                    new StayBack(0.5f, 4.5f, DefaultSavannahGroup),
                    new Shoot(coolDown: 2000, targetProvider: DefaultSavannahGroup, count: 3, shootAngle: 8f))),
            new TierLoot(1, ItemType.Weapon, 0.15f),
            new TierLoot(1, ItemType.Garment, 0.15f),
            new TierLoot(1, ItemType.Ring, 0.15f))
        .Init(
            "Savannah Rat",
            new State(
                new DropPortalOnDeath("Rodent's Burrow Portal", 50),
                new State(
                    "Work",
                    new Wander(0.5f),
                    new Shoot(coolDown: 1000, targetProvider: DefaultSavannahGroup, count: 2, shootAngle: 8f))),
            new TierLoot(1, ItemType.Weapon, 0.15f),
            new TierLoot(1, ItemType.Garment, 0.15f),
            new TierLoot(1, ItemType.Ring, 0.15f));

    private static readonly GroupProvider DefaultSavannahGroup = new(["Player", "Jungle", "Swamp", "Tundra"]);
}