﻿using GameServer.Logic.Behaviors;
using GameServer.Logic.Behaviors.Condition;
using GameServer.Logic.Behaviors.Movement;
using GameServer.Logic.Behaviors.Shoot;
using GameServer.Logic.Behaviors.TargetProvider;
using GameServer.Logic.Behaviors.Toss;
using GameServer.Logic.Behaviors.Transition;
using GameServer.Logic.Loot;
using RoGCore.Models;

namespace GameServer.Logic.Engine.Monsters;

public static partial class BehaviorDb
{
    // ReSharper disable once UnusedMember.Local
    public static readonly Engine.BehaviorDb.BehaviorList Snake = () => Engine.BehaviorDb.Behav()
        .Init(
            "Purple Snake",
            new State(
                new State(
                    "VenomShoot",
                    new If(
                        new EntityInRange("Green Snake", 4),
                        new Orbit(0.75f, 1.25f, new EntityProvider("Green Snake")))
                    .Else(
                        new If(
                        new EntityInRange("Yellow Snake", 4),
                        new Orbit(0.75f, 1.25f, new EntityProvider("Yellow Snake")))
                        .Else(
                            new If(
                            new EntityInRange("Blue Snake", 4),
                            new Orbit(0.75f, 1.25f, new EntityProvider("Blue Snake"))))
                        .Else(
                            new StayBack(1.5f, 5f))),
                    new Shoot(2000)),
                new State(
                    "Protect",
                    new Orbit(0.5f, targetProvider: new EntityProvider("Akurra the Snake God")),
                    new Shoot(coolDown: 2000, count: 1))),
            new TierLoot(1, ItemType.Weapon, 0.1),
            new TierLoot(1, ItemType.Garment, 0.1),
            new TierLoot(1, ItemType.Ring, 0.1),
            new ItemLoot("Snake Skin Breastplate", 0.01))
        .Init(
            "Green Snake",
            new State(
                new State(
                    "VenomShoot",
                    new FollowEntity(1f, new PlayerProvider(4), 1.5f),
                    new Shoot(coolDown: 1000, count: 1)),
                new State(
                    "Protect",
                    new Orbit(0.5f, targetProvider: new EntityProvider("Akurra the Snake God")),
                    new Shoot(coolDown: 1000, count: 1))),
            new TierLoot(1, ItemType.Weapon, 0.05),
            new TierLoot(1, ItemType.Garment, 0.05),
            new TierLoot(1, ItemType.Ring, 0.05),
            new ItemLoot("Snake Venom Dagger", 0.01))
        .Init(
            "Yellow Snake",
            new State(
                new State(
                    "VenomShoot",
                    new StayBack(1f, 5f),
                    new Wander(0.75f, cooldown: new Cooldown(0, 100)),
                    new Shoot(coolDown: 1250, count: 2, shootAngle: 8f)),
                new State(
                    "Protect",
                    new Orbit(0.5f, targetProvider: new EntityProvider("Akurra the Snake God")),
                    new Shoot(coolDown: 1250, count: 2, shootAngle: 8f))),
            new TierLoot(1, ItemType.Weapon, 0.05),
            new TierLoot(1, ItemType.Garment, 0.05),
            new TierLoot(1, ItemType.Ring, 0.05))
        .Init(
            "Blue Snake",
            new State(
                new State(
                    "VenomShoot",
                    new Wander(0.75f, cooldown: new Cooldown(0, 100)),
                    new Shoot(coolDown: 1500, count: 1),
                    new Shoot(coolDown: 1500, count: 1, coolDownOffset: 100),
                    new Shoot(coolDown: 1500, count: 1, coolDownOffset: 200)),
                new State(
                    "Protect",
                    new Orbit(0.5f, targetProvider: new EntityProvider("Akurra the Snake God")),
                    new Shoot(coolDown: 1500, count: 1),
                    new Shoot(coolDown: 1500, count: 1, coolDownOffset: 100),
                    new Shoot(coolDown: 1500, count: 1, coolDownOffset: 200))),
            new TierLoot(1, ItemType.Weapon, 0.05),
            new TierLoot(1, ItemType.Garment, 0.05),
            new TierLoot(1, ItemType.Ring, 0.05))
        .Init(
            "Akurra the Snake God",
            new State(
                new DropPortalOnDeath("Exit Portal", 100, portalDespawnTimeSec: 1000),
                new State(
                    "Wait",
                    new AddCond(ConditionEffectIndex.Invincible),
                    new Wander(0.3f),
                    new PlayerWithinTransition(6f, "Circle")),
                new State(
                    "Circle",
                    new TitleCard(
                        @"Akurra
The
Snake God"),
                    new Orbit(3, 7, SpawnPointProvider.Default),
                    new HpLessTransition(0.4f, "gotospawn"),
                    new Shoot(coolDown: 500)),
                new State(
                    "gotospawn",
                    new AddCond(ConditionEffectIndex.Invincible, true),
                    new Goto(2, SpawnPointProvider.Default, signal: true),
                    new SignaledTransition(BehaviorSignal.GotoComplete, "SpawnFriends")),
                new State(
                    "SpawnFriends",
                    new TossEntity("Purple Snake", 1000, SelfProvider.Default.Offset(new System.Numerics.Vector2(-1, -1)), coolDown: 7500),
                    new TossEntity("Green Snake", 1000, SelfProvider.Default.Offset(new System.Numerics.Vector2(1, -1)), coolDown: 7500, coolDownOffset: 100),
                    new TossEntity("Yellow Snake", 1000, SelfProvider.Default.Offset(new System.Numerics.Vector2(-1, 1)), coolDown: 7500, coolDownOffset: 200),
                    new TossEntity("Blue Snake", 1000, SelfProvider.Default.Offset(new System.Numerics.Vector2(1, 1)), coolDown: 7500, coolDownOffset: 300),
                    new TossEntity("Purple Snake", 1000, SelfProvider.Default.Offset(new System.Numerics.Vector2(0, -8)), coolDown: 7500, coolDownOffset: 400),
                    new TossEntity("Green Snake", 1000, SelfProvider.Default.Offset(new System.Numerics.Vector2(8, 0)), coolDown: 7500, coolDownOffset: 500),
                    new TossEntity("Yellow Snake", 1000, SelfProvider.Default.Offset(new System.Numerics.Vector2(0, 8)), coolDown: 7500, coolDownOffset: 600),
                    new TossEntity("Blue Snake", 1000, SelfProvider.Default.Offset(new System.Numerics.Vector2(-8, 0)), coolDown: 7500, coolDownOffset: 700, signal: true),
                    new Order(10f, "Purple Snake", "Protect", false),
                    new Order(10f, "Green Snake", "Protect", false),
                    new Order(10f, "Yellow Snake", "Protect", false),
                    new Order(10f, "Blue Snake", "Protect", false),
                    new SignaledTransition(BehaviorSignal.TossComplete, "Wait For Snakes Death")),
                new State(
                    "Wait For Snakes Death",
                    new EntitiesNotExistsTransition(10, "LastStand Cooldown", "Green Snake", "Purple Snake", "Yellow Snake", "Blue Snake")),
                new State(
                    "LastStand Cooldown",
                    new TimedTransition(1000, "LastStand")),
                new State(
                    "LastStand",
                    new RemCond(ConditionEffectIndex.Invincible),
                    new ProjectileDodge(1f),
                    new Shoot(new Cooldown(50, 100), RandomPointProvider.Default, 3, projectileIndex: 1))),
            new TierLoot(1, ItemType.Weapon, 0.15),
            new TierLoot(1, ItemType.Garment, 0.15),
            new TierLoot(1, ItemType.Ring, 0.15),
            new ItemLoot("Snake Venom Dagger", 0.25),
            new ItemLoot("Snake Skin Breastplate", 0.25));
}