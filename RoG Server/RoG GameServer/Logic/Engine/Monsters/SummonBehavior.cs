﻿using GameServer.Logic.Behaviors.Movement;
using GameServer.Logic.Behaviors.TargetProvider;

namespace GameServer.Logic.Engine.Monsters;

public static partial class BehaviorDb
{
    public static readonly Engine.BehaviorDb.BehaviorList Summons = () => Engine.BehaviorDb.Behav()
        .Init(
            "Fake Player",
            new State(
                new FollowPoint(2, SummonPositionProvider.Default, 0, 50, false)));
}