﻿using GameServer.Logic.Behaviors;
using GameServer.Logic.Behaviors.Movement;
using GameServer.Logic.Behaviors.Shoot;
using GameServer.Logic.Behaviors.TargetProvider;
using GameServer.Logic.Loot;
using RoGCore.Models;

namespace GameServer.Logic.Engine.Monsters;

public static partial class BehaviorDb
{
    public static readonly Engine.BehaviorDb.BehaviorList Beachzone = () => Engine.BehaviorDb.Behav()
        .Init(
            "Old Blacksmith",
            new State(
                new State(
                    "BeOld",
                    new Wander(0.2f),
                    new StayCloseToSpawn(0.3f))))
        .Init(
            "Old Blacksmith With Coat",
            new State(
                new State(
                    "BeOld",
                    new Wander(0.2f),
                    new StayCloseToSpawn(0.3f))))
        .Init(
            "Red Bandit",
            new State(
                new DropPortalOnDeath("The Depths Portal", 25),
                new State(
                    "Wander",
                    new Wander(0.4f),
                    new FollowEntity(0.8f, DefaultRedGroup, 1.5f),
                    new Shoot(coolDown: 100, targetProvider: DefaultRedGroup9, count: 1))),
            new TierLoot(1, ItemType.Weapon, 0.1f),
            new TierLoot(1, ItemType.Garment, 0.1f),
            new TierLoot(1, ItemType.Ring, 0.1f))
        .Init(
            "Red Butcher",
            new State(
                new DropPortalOnDeath("The Depths Portal", 50),
                new State(
                    "Wander",
                    new Wander(0.3f),
                    new FollowEntity(0.5f, DefaultRedGroup, 2f),
                    new Shoot(coolDown: 2500, targetProvider: DefaultRedGroup9, count: 1))),
            new TierLoot(1, ItemType.Weapon, 0.1f),
            new TierLoot(1, ItemType.Garment, 0.1f),
            new TierLoot(1, ItemType.Ring, 0.1f))
        .Init(
            "Red Archer",
            new State(
                new DropPortalOnDeath("The Depths Portal", 50),
                new State(
                    "Wander",
                    new Wander(0.35f),
                    new StayBack(0.5f, 4.5f, DefaultRedGroup),
                    new Shoot(coolDown: 1500, targetProvider: DefaultRedGroup9, count: 2, shootAngle: 8f))),
            new TierLoot(1, ItemType.Weapon, 0.1f),
            new TierLoot(1, ItemType.Garment, 0.1f),
            new TierLoot(1, ItemType.Ring, 0.1f))
        .Init(
            "Green Archer",
            new State(
                new DropPortalOnDeath("The Depths Portal", 50),
                new State(
                    "Wander",
                    new Wander(0.35f),
                    new StayBack(0.5f, 4.5f, DefaultGreenGroup),
                    new Shoot(coolDown: 1500, targetProvider: DefaultGreenGroup9, count: 1))),
            new TierLoot(1, ItemType.Weapon, 0.1f),
            new TierLoot(1, ItemType.Garment, 0.1f),
            new TierLoot(1, ItemType.Ring, 0.1f))
        .Init(
            "Green Spearman",
            new State(
                new DropPortalOnDeath("The Depths Portal", 50),
                new State(
                    "Wander",
                    new Wander(0.3f),
                    new FollowEntity(0.4f, DefaultGreenGroup, 1f),
                    new Shoot(coolDown: 2500, targetProvider: new GroupProvider(DefaultGreenGroup, 2f), count: 1))),
            new TierLoot(1, ItemType.Weapon, 0.1f),
            new TierLoot(1, ItemType.Garment, 0.1f),
            new TierLoot(1, ItemType.Ring, 0.1f))
        .Init(
            "Green Wizard",
            new State(
                new DropPortalOnDeath("The Depths Portal", 50),
                new State(
                    "Wander",
                    new Wander(0.35f),
                    new StayBack(0.5f, 4.5f, DefaultGreenGroup),
                    new Shoot(coolDown: 2000, targetProvider: DefaultGreenGroup, count: 1))),
            new TierLoot(1, ItemType.Weapon, 0.1f),
            new TierLoot(1, ItemType.Garment, 0.1f),
            new TierLoot(1, ItemType.Ring, 0.1f));

    private static readonly GroupProvider DefaultRedGroup =
        new(["Player", "Beach", "Jungle", "Savannah", "Tundra"]);

    private static readonly GroupProvider DefaultRedGroup9 = new(DefaultRedGroup, 9f);

    private static readonly GroupProvider DefaultGreenGroup =
        new(["Player", "Beach", "Swamp", "Savannah", "Tundra"]);

    private static readonly GroupProvider DefaultGreenGroup9 = new(DefaultGreenGroup, 9f);
}