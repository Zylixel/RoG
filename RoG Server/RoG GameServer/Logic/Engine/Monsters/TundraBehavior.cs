﻿using GameServer.Logic.Behaviors.Movement;
using GameServer.Logic.Behaviors.Shoot;
using GameServer.Logic.Behaviors.TargetProvider;
using GameServer.Logic.Behaviors.Transition;
using GameServer.Logic.Loot;
using RoGCore.Models;

namespace GameServer.Logic.Engine.Monsters;

public static partial class BehaviorDb
{
    public static readonly Engine.BehaviorDb.BehaviorList Tundra = () => Engine.BehaviorDb.Behav()
        .Init(
            "Snowman",
            new State(
                new State(
                    "Wait",
                    new PlayerWithinTransition(1, "Attack"),
                    new HpLessTransition(0.999f, "Attack")),
                new State(
                    "Attack",
                    new Wander(0.3f),
                    new FollowEntity(0.7f, DefaultTundraGroup, 4f),
                    new Shoot(coolDown: 800, targetProvider: DefaultTundraGroup, count: 1))),
            new TierLoot(1, ItemType.Weapon, 0.15f),
            new TierLoot(1, ItemType.Garment, 0.15f),
            new TierLoot(1, ItemType.Ring, 0.15f))
        .Init(
            "Tundra Penguin",
            new State(
                new State(
                    "Attack",
                    new Wander(0.4f),
                    new FollowEntity(0.8f, DefaultTundraGroup, 4f),
                    new Shoot(coolDown: 1000, targetProvider: DefaultTundraGroup, count: 3, shootAngle: 8f))),
            new TierLoot(1, ItemType.Weapon, 0.15f),
            new TierLoot(1, ItemType.Garment, 0.15f),
            new TierLoot(1, ItemType.Ring, 0.15f))
        .Init(
            "Tundra Fox",
            new State(
                new State(
                    "Attack",
                    new Wander(0.5f),
                    new FollowEntity(1f, DefaultTundraGroup, 0.5f),
                    new Shoot(coolDown: 100, targetProvider: DefaultTundraGroup, count: 1))),
            new TierLoot(1, ItemType.Weapon, 0.15f),
            new TierLoot(1, ItemType.Garment, 0.15f),
            new TierLoot(1, ItemType.Ring, 0.15f))
        .Init(
            "Tundra Owl",
            new State(
                new State(
                    "Attack",
                    new Wander(0.45f),
                    new StayBack(0.9f, 5f, DefaultTundraGroup),
                    new Shoot(coolDown: 1200, targetProvider: DefaultTundraGroup, count: 2, shootAngle: 4f))),
            new TierLoot(1, ItemType.Weapon, 0.15f),
            new TierLoot(1, ItemType.Garment, 0.15f),
            new TierLoot(1, ItemType.Ring, 0.15f));

    private static readonly GroupProvider DefaultTundraGroup = new(["Player", "Jungle", "Swamp", "Savannah"]);
}