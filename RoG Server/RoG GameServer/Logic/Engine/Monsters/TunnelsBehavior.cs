﻿using System.Numerics;
using GameServer.Logic.Behaviors;
using GameServer.Logic.Behaviors.Condition;
using GameServer.Logic.Behaviors.Movement;
using GameServer.Logic.Behaviors.Shoot;
using GameServer.Logic.Behaviors.TargetProvider;
using GameServer.Logic.Behaviors.Toss;
using GameServer.Logic.Behaviors.Transition;
using GameServer.Logic.Loot;
using RoGCore.Models;

namespace GameServer.Logic.Engine.Monsters;

public static partial class BehaviorDb
{
    public static readonly Engine.BehaviorDb.BehaviorList Tunnels = () => Engine.BehaviorDb.Behav()
        .Init(
            "Sensei Rat",
            new State(
                new AddCond(ConditionEffectIndex.Invincible),
                new State("AwaitOrders"),
                new State(
                    "Fight",
                    new RemCond(ConditionEffectIndex.Invincible),
                    new State(
                        "FightWander",
                        new SenseiProtect(2.5f),
                        new TriangleShoot(targetProvider: PlayerProvider.Infinite, projectileIndex: 2, coolDownOffset: 250, lines: 2, spacing: 0.75f, coolDown: 750),
                        new TimedTransition(3000, "FightShoot")),
                    new State(
                        "FightShoot",
                        new Flash(new ARGB(127, 0, 200, 200), 3, 1000),
                        new TriangleShoot(targetProvider: PlayerProvider.Infinite, projectileIndex: 3, coolDownOffset: 500, lines: 4, spacing: 0.75f, skipProjectiles: [new Vector2Int(2, 1)], coolDown: 10000),
                        new TimedTransition(1000, "FightWander"))),
                new State(
                    "Orbit",
                    new Orbit(2, 14, new EntityProvider("Arimanius The Rat King", float.MaxValue), 0, 0, 0)),
                new State(
                    "OrbitShoot1",
                    new Orbit(2, 14, new EntityProvider("Arimanius The Rat King", float.MaxValue), 0, 0, 0),
                    new Shoot(coolDown: 200, targetProvider: new EntityProvider("Arimanius The Rat King", 15), count: 1),
                    new Shoot(coolDown: 200, targetProvider: new PlayerProvider(7f), count: 2, shootAngle: 50f, projectileIndex: 1)),
                new State(
                    "OrbitShoot2",
                    new Orbit(2, 14, new EntityProvider("Arimanius The Rat King", float.MaxValue), 0, 0, 1),
                    new Shoot(coolDown: 200, targetProvider: new EntityProvider("Arimanius The Rat King", 15), count: 1),
                    new Shoot(coolDown: 200, targetProvider: new PlayerProvider(7f), count: 2, shootAngle: 50f, projectileIndex: 1)),
                new State(
                    "Die",
                    new Suicide())),
            new TierLoot(1, ItemType.Weapon, 0.25),
            new TierLoot(1, ItemType.Garment, 0.25),
            new TierLoot(1, ItemType.Ring, 0.25))
        .Init(
            "Burrow Green Slime",
            new State(
                new State(
                    "Work",
                    new Wander(2f, period: 2000),
                    new StayCloseToSpawn(1.5f),
                    new Shoot(coolDown: 150, targetProvider: AngleProvider.Zero))),
            new TierLoot(1, ItemType.Weapon, 0.2),
            new TierLoot(1, ItemType.Garment, 0.2),
            new TierLoot(1, ItemType.Ring, 0.2))
        .Init(
            "Burrow Purple Slime",
            new State(
                new State(
                    "Work",
                    new Wander(2f, period: 2000),
                    new StayCloseToSpawn(1.5f),
                    new Shoot(coolDown: 150, targetProvider: AngleProvider.Zero))),
            new TierLoot(1, ItemType.Weapon, 0.2),
            new TierLoot(1, ItemType.Garment, 0.2),
            new TierLoot(1, ItemType.Ring, 0.2))
        .Init(
            "Burrow Rat",
            new State(
                new State(
                    "Work",
                    new Wander(1f),
                    new StayCloseToSpawn(1f),
                    new Shoot(coolDown: 1000, count: 2, shootAngle: 8f))),
            new TierLoot(1, ItemType.Weapon, 0.2),
            new TierLoot(1, ItemType.Garment, 0.2),
            new TierLoot(1, ItemType.Ring, 0.2))
        .Init(
            "Burrow Bat",
            new State(
                new State(
                    "Work",
                    new Wander(1f),
                    new StayCloseToSpawn(1f),
                    new Shoot(coolDown: 2000, predictive: 0.5f),
                    new Shoot(coolDown: 2000, projectileIndex: 1, predictive: 0.5f),
                    new Shoot(coolDown: 2000, projectileIndex: 2, predictive: 0.5f))),
            new TierLoot(1, ItemType.Weapon, 0.2),
            new TierLoot(1, ItemType.Garment, 0.2),
            new TierLoot(1, ItemType.Ring, 0.2))
        .Init(
            "Burrow Purple Slime Orbiter 1",
            new State(
                new AddCond(ConditionEffectIndex.Invincible, true),
                new State(
                    "Orbit",
                    new Orbit(3, 4.5f, new EntityProvider("Arimanius The Rat King", float.MaxValue), 0, 0, 0)),
                new State(
                    "andShoot",
                    new Orbit(3, 4.5f, new EntityProvider("Arimanius The Rat King", float.MaxValue), 0, 0, 0),
                    new Shoot(coolDown: 50, targetProvider: AngleProvider.Zero)),
                new State(
                    "Die",
                    new Suicide())))
        .Init(
            "Burrow Purple Slime Orbiter 2",
            new State(
                new AddCond(ConditionEffectIndex.Invincible, true),
                new State(
                    "Orbit",
                    new Orbit(5, 9, new EntityProvider("Arimanius The Rat King", float.MaxValue), 0, 0, 1)),
                new State(
                    "andShoot",
                    new Orbit(5, 9, new EntityProvider("Arimanius The Rat King", float.MaxValue), 0, 0, 1),
                    new Shoot(coolDown: 0, targetProvider: AngleProvider.Zero)),
                new State(
                    "Die",
                    new Suicide())))
        .Init(
            "Arimanius The Rat King",
            new State(
                new OnDeathBehavior(new Order(30, "Sensei Rat", "Die")),
                new OnDeathBehavior(new Order(30, "Burrow Purple Slime Orbiter 1", "Die")),
                new OnDeathBehavior(new Order(30, "Burrow Purple Slime Orbiter 2", "Die")),
                new DropPortalOnDeath("Exit Portal", 100, portalDespawnTimeSec: 1000),
                new AddCond(ConditionEffectIndex.Invincible, true),
                new HpLessTransition(0.7f, "Trans1", true),
                new State(
                    "Wait",
                    new PlayerWithinTransition(8, "Greet")),
                new State(
                    "Greet",
                    new TitleCard(
@"Arimanius
the
Rat King"),
                    new Flash(new ARGB(0xFF0000), 3, 10000),
                    new TimedTransition(3000, "Fight1")),
                new State(
                    "Fight1",
                    new RemCond(ConditionEffectIndex.Invincible),
                    new FollowEntity(5f, PlayerProvider.Infinite, 1f),
                    new StayCloseToSpawn(2, 14),
                    new Shoot(coolDown: 50, count: 5, shootAngle: 8f, projectileIndex: 1, predictive: 0.25f),
                    new TimedTransition(250, "Fight2")),
                new State(
                    "Fight2",
                    new RemCond(ConditionEffectIndex.Invincible),
                    new Wander(0.5f),
                    new StayCloseToSpawn(2, 14),
                    new Shoot(coolDown: 500, count: 3, shootAngle: 8f, projectileIndex: 1, predictive: 0.25f),
                    new Shoot(coolDown: 400, count: 5, shootAngle: 10f, predictive: 0.75f, coolDownOffset: 200),
                    new TimedTransition(5000, "Fight1")),
                new State(
                    "Trans1",
                    new AddCond(ConditionEffectIndex.Invincible, true),
                    new Flash(new ARGB(0xFF0000), 3, 5000),
                    new Goto(3, SpawnPointProvider.Default, signal: true),
                    new SignaledTransition(BehaviorSignal.GotoComplete, "Trans2")),
                new State(
                    "Trans2",
                    new TossEntity("Sensei Rat", 1500, SelfProvider.Default.Offset(new Vector2(0, 4)), 100000),
                    new TossEntity("Sensei Rat", 1500, SelfProvider.Default.Offset(new Vector2(0, -4)), 100000),
                    new TimedTransition(2500, "Sensei Fight")),
                new State(
                    "Sensei Fight",
                    new Heal(0, 125, ["Self"], 1500),
                    new If(
                        new PlayerInRange(7),
                        new StayBack(2f, distance: 9f))
                    .Else(
                        new AvoidWander(1.5f, 7f, new Realm.World.Worlds.RandomRange(0, 15), 3, new Cooldown(0, 100))),
                    new Order(float.PositiveInfinity, "Sensei Rat", "Fight"),
                    new EntityNotExistsTransition("Sensei Rat", 100, "beginRage")),
                new State(
                    "beginRage",
                    new Flash(new ARGB(0xFF0000), 3, 5000),
                    new RemCond(ConditionEffectIndex.Invincible),
                    new Goto(1, SpawnPointProvider.Default, signal: true),
                    new SignaledTransition(BehaviorSignal.GotoComplete, "rage0")),
                new State(
                    "rage0",
                    new TossEntity("Sensei Rat", 2000, SelfProvider.Default.Offset(new Vector2(0, 8)), 100000),
                    new TossEntity("Sensei Rat", 2000, SelfProvider.Default.Offset(new Vector2(0, -8)), 100000),
                    new TossEntity("Sensei Rat", 2000, SelfProvider.Default.Offset(new Vector2(-8, 0)), 100000),
                    new TossEntity("Sensei Rat", 2000, SelfProvider.Default.Offset(new Vector2(8, 0)), 100000),
                    new Order(float.PositiveInfinity, "Sensei Rat", "Orbit", false),
                    new TossEntity("Burrow Purple Slime Orbiter 1", 1500, SelfProvider.Default.Offset(new Vector2(2, 2)), 100000),
                    new TossEntity("Burrow Purple Slime Orbiter 2", 1750, SelfProvider.Default.Offset(new Vector2(-4, -4)), 100000),
                    new TimedTransition(3000, "rage1")),
                new State(
                    "rage1",
                    new Order(float.PositiveInfinity, "Burrow Purple Slime Orbiter 1", "andShoot"),
                    new Order(float.PositiveInfinity, "Burrow Purple Slime Orbiter 2", "andShoot"),
                    new CircleShoot(5, angleIncrement: (float)Math.PI / -10f, offsetProjectiles: true, coolDown: 500),
                    new Order(30, "Sensei Rat", "OrbitShoot1"),
                    new TimedTransition(new Cooldown(3500, 1500), "rage2")),
                new State(
                    "rage2",
                    new CircleShoot(5, angleIncrement: (float)Math.PI / 10f, offsetProjectiles: true, coolDown: 500),
                    new Order(30, "Sensei Rat", "OrbitShoot2"),
                    new TimedTransition(new Cooldown(3500, 1500), "rage1"))),
            new TierLoot(1, ItemType.Weapon, 0.5),
            new TierLoot(1, ItemType.Garment, 0.5),
            new TierLoot(1, ItemType.Ring, 0.5),
            new ItemLoot("Vermin Wand", 0.15),
            new ItemLoot("Robe of Arimanius", 0.15),
            new ItemLoot("Gooey Ring", 0.15));
}