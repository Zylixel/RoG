﻿using GameServer.Realm.Entities;

namespace GameServer.Logic.Engine;

internal abstract class StatefulCycleBehavior<T> : CycleBehavior
{
    internal override void TickCore(Entity host)
    {
        var state = host.GetState<T>(this);
        TickCore(host, ref state);
        host.SetState(this, state);
    }

    internal virtual void TickCore(Entity host, ref T? state)
    {
    }

    internal override void OnStateEntry(Entity host)
    {
        var state = host.GetState<T>(this);
        OnStateEntry(host, ref state);
        host.SetState(this, state);
    }

    internal virtual void OnStateEntry(Entity host, ref T? state)
    {
    }

    internal override void OnStateExit(Entity host)
    {
        var state = host.GetState<T>(this);
        OnStateExit(host, ref state);
        host.SetState(this, state);
    }

    internal virtual void OnStateExit(Entity host, ref T? state)
    {
    }
}