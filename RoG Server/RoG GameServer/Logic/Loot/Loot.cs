﻿using System.Collections.Frozen;
using GameServer.Realm.Entities.Enemy;
using GameServer.Realm.Entities.GameObject;
using GameServer.Realm.Entities.Player;
using RoGCore.Assets;
using RoGCore.Models;

namespace GameServer.Logic.Loot;

internal class Loot : List<ILootDef>
{
    private static readonly FrozenDictionary<int, string> BagDict = new Dictionary<int, string>()
    {
        { 0, "Brown Bag" },
        { 1, "Green Bag" },
        { 2, "Blue Bag" },
    }.ToFrozenDictionary();

    internal Loot(params ILootDef[] lootDefs) // For independent loots(e.g. chests)
    {
        AddRange(lootDefs);
        Add(new ItemLoot("Small Vault Storage Chip", 1 / 100D));
        Add(new ItemLoot("Large Vault Storage Chip", 1 / 1000D));
    }

    internal IEnumerable<JsonItem> GetLoots(int min, int max) // For independent loots(e.g. chests)
    {
        List<LootDef> consideration = [];
        foreach (var i in this)
        {
            i.Populate(null, Random.Shared, consideration, default);
        }

        var retCount = Random.Shared.Next(min, max);
        foreach (var i in consideration)
        {
            if (Random.Shared.NextDouble() < i.Probabilty)
            {
                yield return i.Item;
                retCount--;
            }

            if (retCount == 0)
            {
                yield break;
            }
        }
    }

    internal void Handle(Enemy enemy)
    {
        if (enemy.Owner.Name == "Arena")
        {
            return;
        }

        List<LootDef> consideration = [];
        var dats = enemy.DamageCounter.GetPlayerData().ToArray();

        var loots = dats.ToDictionary(
            d => d.Key, _ => (IList<JsonItem>)[]);

        foreach (var dat in dats)
        {
            var prob = Program.AppSettings.GameServer.Event.LootDropMultiplier - 1;
            prob += dat.Value / enemy.ObjectDescription.MaxHitPoints;

            foreach (var i in this)
            {
                i.Populate(enemy, Random.Shared, consideration, dat);
            }

            var playerLoot = loots[dat.Key];
            foreach (var i in consideration)
            {
                if (Random.Shared.NextDouble() <= i.Probabilty * prob)
                {
                    playerLoot.Add(i.Item);
                }
            }
        }

        AddBagsToWorld(enemy, loots);
    }

    private static void AddBagsToWorld(Enemy enemy, IDictionary<Player, IList<JsonItem>> soulbound)
    {
        foreach (var i in soulbound)
        {
            if (i.Value.Count > 0)
            {
                ShowBags(enemy, i.Value, i.Key);
            }
        }
    }

    private static void ShowBags(Enemy enemy, IEnumerable<JsonItem> loots, Player owner)
    {
        var ownerId = owner.Client?.Account.AccountId ?? -1;
        var bagType = 0;
        var items = new JsonItem[8];
        var idx = 0;
        foreach (var i in loots)
        {
            if (i.Bag > bagType)
            {
                bagType = i.Bag;
            }

            items[idx] = i;
            idx++;

            if (idx == 8)
            {
                ShowBag(enemy, ownerId, BagDict[bagType], items);
                bagType = 0;
                items = new JsonItem[8];
                idx = 0;
            }
        }

        if (idx > 0)
        {
            ShowBag(enemy, ownerId, BagDict[bagType], items);
        }
    }

    private static void ShowBag(Enemy enemy, int owner, string bagType, JsonItem[] items)
    {
        var inv = Array.ConvertAll<JsonItem, Item?>(items, stat => stat != null ? new Item(stat) : null);
        Container container = new(bagType, 1000 * 60, true, enemy.Owner)
        {
            Inventory = inv,
            BagOwner = owner,
        };
        container.Move(
            enemy.X + (float)Random.Shared.NextDouble() - 0.5f,
            enemy.Y + (float)Random.Shared.NextDouble() - 0.5f);
        container.Size = 80;
    }
}