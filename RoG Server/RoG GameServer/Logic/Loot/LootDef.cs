﻿using RoGCore.Assets;

namespace GameServer.Logic.Loot;

internal readonly struct LootDef
{
    internal readonly JsonItem Item;
    internal readonly double Probabilty;

    internal LootDef(JsonItem item, double probabilty)
    {
        Item = item;
        Probabilty = probabilty;
    }
}