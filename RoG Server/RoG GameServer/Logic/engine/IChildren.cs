﻿using GameServer.Realm.Entities;

namespace GameServer.Logic.Engine;

internal interface IChildren
{
    internal void Resolve(State parent, Dictionary<string, State> states);

    internal bool Tick(Entity host);

    internal void OnStateEntry(Entity host);

    internal void OnStateExit(Entity host);
}