﻿using GameServer.Realm.Entities.Enemy;
using GameServer.Realm.Entities.Player;

namespace GameServer.Logic.Loot;

internal interface ILootDef
{
    void Populate(
        Enemy? enemy,
        Random rand,
        IList<LootDef> lootDefs,
        in KeyValuePair<Player, int> dat);
}