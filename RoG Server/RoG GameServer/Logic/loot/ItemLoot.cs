﻿using GameServer.Realm.Entities.Enemy;
using GameServer.Realm.Entities.Player;
using RoGCore.Assets;

namespace GameServer.Logic.Loot;

internal class ItemLoot : ILootDef
{
    internal readonly string Item;
    private readonly double probability;

    internal ItemLoot(string item, double probability)
    {
        Item = item;
        this.probability = probability;
    }

    public void Populate(
        Enemy? enemy,
        Random rand,
        IList<LootDef> lootDefs,
        in KeyValuePair<Player, int> dat)
    {
        lootDefs.Add(new LootDef(EmbeddedData.Items[EmbeddedData.IdToItemType[Item]], probability));
    }
}