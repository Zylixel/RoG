﻿namespace GameServer.Logic.Loot;

internal static class LootTemplates
{
    internal static ILootDef[] StatIncreasePotionsLoot()
    {
        return
[
        new OnlyOne(
            new ItemLoot("Potion of Defense", 1),
            new ItemLoot("Potion of Attack", 1),
            new ItemLoot("Potion of Speed", 1),
            new ItemLoot("Potion of Vitality", 1),
            new ItemLoot("Potion of Wisdom", 1),
            new ItemLoot("Potion of Dexterity", 1)),
];
    }
}