﻿using GameServer.Realm.Entities.Enemy;
using GameServer.Realm.Entities.Player;

namespace GameServer.Logic.Loot;

internal class MostDamagers : ILootDef
{
    private readonly ILootDef[] loots;
    private readonly int amount;

    internal MostDamagers(int amount, params ILootDef[] loots)
    {
        this.amount = amount;
        this.loots = loots;
    }

    public void Populate(
        Enemy? enemy,
        Random rand,
        IList<LootDef> lootDefs,
        in KeyValuePair<Player, int> dat)
    {
        if (enemy is null)
        {
            return;
        }

        var mostDamage = GetMostDamage(enemy.DamageCounter.GetPlayerData());

        foreach (var damager in mostDamage)
        {
            if (!damager.Equals(dat))
            {
                continue;
            }

            foreach (var loot in loots)
            {
                loot.Populate(enemy, rand, lootDefs, dat);
            }
        }
    }

    private IEnumerable<KeyValuePair<Player, int>> GetMostDamage(IEnumerable<KeyValuePair<Player, int>> data)
    {
        var damages = data.Select(_ => _.Value).ToList();
        var len = damages.Count < amount ? damages.Count : amount;
        for (var i = 0; i < len; i++)
        {
            var val = damages.Max();
            yield return data.First(_ => _.Value == val);
            damages.Remove(val);
        }
    }
}