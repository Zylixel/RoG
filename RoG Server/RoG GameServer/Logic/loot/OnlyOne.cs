﻿using GameServer.Realm.Entities.Enemy;
using GameServer.Realm.Entities.Player;

namespace GameServer.Logic.Loot;

internal class OnlyOne : ILootDef
{
    private readonly ILootDef[] loots;

    internal OnlyOne(params ILootDef[] loots)
    {
        this.loots = loots;
    }

    public void Populate(
        Enemy? enemy,
        Random rand,
        IList<LootDef> lootDefs,
        in KeyValuePair<Player, int> dat)
    {
        loots[rand.Next(0, loots.Length)].Populate(enemy, rand, lootDefs, dat);
    }
}