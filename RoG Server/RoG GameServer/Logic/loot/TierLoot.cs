﻿using GameServer.Realm.Entities.Enemy;
using GameServer.Realm.Entities.Player;
using RoGCore.Assets;
using RoGCore.Models;

namespace GameServer.Logic.Loot;

internal class TierLoot : ILootDef
{
    private static readonly HashSet<string> WeaponT = ["Wand", "Sword", "Dagger", "Bow"];

    // private static readonly int[] AbilityT = { 4, 5, 11, 12, 13, 15, 16, 18, 19, 20, 21, 22, 23, 25 };
    private static readonly HashSet<string> ArmorT = ["Leather", "Robe", "Armor"];
    private static readonly HashSet<string> RingT = ["Ring"];

    // private static readonly int[] PotionT = { 10 };
    private readonly double probability;

    private readonly byte tier;
    private readonly HashSet<string> types;

    internal TierLoot(byte tier, ItemType type, double probability)
    {
        this.tier = tier;
        types = type switch
        {
            ItemType.Weapon => WeaponT,

            // TODO
            // ItemType.Ability => AbilityT,
            ItemType.Garment => ArmorT,
            ItemType.Ring => RingT,
            _ => throw new NotSupportedException(type.ToString()),
        };
        this.probability = probability;
    }

    public void Populate(
        Enemy? enemy,
        Random rand,
        IList<LootDef> lootDefs,
        in KeyValuePair<Player, int> dat)
    {
        var candidates = EmbeddedData.Items
            .Where(item => item.Value.Tier == tier && types.Contains(item.Value.Type))
            .Select(item => item.Value)
            .ToArray();

        foreach (var i in candidates)
        {
            lootDefs.Add(new LootDef(i, probability / candidates.Length));
        }
    }
}