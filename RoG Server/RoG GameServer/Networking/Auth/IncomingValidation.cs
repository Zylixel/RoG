﻿using Database;
using Database.Models;
using FluentResults;
using GameServer.Networking.Socket;
using GameServer.Realm;
using RoGCore.Models;
using RoGCore.SettingsModels;

namespace GameServer.Networking.Auth;

internal static class IncomingValidation
{
    public static async Task<Result<DbAccount>> ValidateIncomingClient(Client client, string username, string password, string clientBuild)
    {
        if (clientBuild != NetworkingSettings.FULLBUILD)
        {
            return Result.Fail($"You are trying to connect using build <b>{clientBuild}</b> instead of <b>{NetworkingSettings.FULLBUILD}</b>. Please update your client.");
        }

        var dns = GetDns(client);
        if (dns is null)
        {
            return Result.Fail("Could not resolve IP");
        }

        var account = await RedisDatabase.ReadAccount(username);
        if (account is null)
        {
            return Result.Fail("Account not found");
        }

        if (account.Password != password)
        {
            return Result.Fail("Invalid password");
        }

        if (Program.AppSettings.GameServer.AdminOnly && account.Rank < (int)AccountType.Rank.Administrator)
        {
            return Result.Fail("Server is Admin Only");
        }

        var ipBannedTask = RedisDatabase.GetIPBanned(dns);
        var dupeDetectedTask = RealmManager.AntiDupe.AccountHasDupedItems(account);

        await Task.WhenAll(ipBannedTask, dupeDetectedTask);
        var ipBanned = await ipBannedTask;
        var dupeDetected = await dupeDetectedTask;

        if (ipBanned || dupeDetected || account.Banned)
        {
            return Result.Fail("Banned");
        }

        return Result.Ok(account);
    }

    private static string? GetDns(Client client)
    {
        var endPoint = client.Endpoint.ToString();
        return endPoint?.Split(':')[0];
    }
}
