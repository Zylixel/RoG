﻿using Database;
using GameServer.Networking.Socket;
using GameServer.Realm;
using GameServer.Realm.Entities.GameObject;
using GameServer.Realm.Entities.Models;
using GameServer.Realm.Entities.Player;
using RoGCore.Models;
using RoGCore.Networking.Packets;

namespace GameServer.Networking.Messages.Handlers;

internal class ContainerClaimHandler : PacketHandler<ContainerClaim>
{
    public override MessageId Id => MessageId.ContainerClaim;

    public override bool RunOutsideOfLogicThread => false;

    protected override void HandlePacket(Client client, in ContainerClaim packet)
    {
        if (client.Player is null)
        {
            return;
        }

        var entity = client.Player.Owner.GetEntity(packet.ContainerId);
        if (entity is not IContainer container || entity is Player ||
            EntityUtils.DistSqr(client.Player, entity) > 4)
        {
            return;
        }

        if (packet.SlotId != -1)
        {
            TakeItem(container, client, packet.SlotId);
        }
        else
        {
            SendToInventory(container, client);

            if (container.Inventory.Any(item => item != null))
            {
                SendToVault(container, client);
            }
        }

        if (container is Container con)
        {
            con.DespawnIfEmpty();
        }
    }

    private static void SendToInventory(IContainer container, Client client)
    {
        if (client.Player is null)
        {
            return;
        }

        for (var i = 0; i < container.Inventory.Length; i++)
        {
            var item = container.Inventory[i];
            if (item is null)
            {
                continue;
            }

            for (var j = 4; j < client.Player.Inventory.Length; j++)
            {
                if (client.Player.Inventory[j]?.BaseItem != null)
                {
                    continue;
                }

                container.Inventory[i] = null;
                client.Player.Inventory[j] = item;
                break;
            }
        }
    }

    private static void SendToVault(IContainer container, Client client)
    {
        if (client.Player is null)
        {
            return;
        }

        var success = true;

        for (var i = 0; i < container.Inventory.Length; i++)
        {
            var item = container.Inventory[i];
            if (item is null)
            {
                continue;
            }

            if (RedisDatabase.AddVault(client.Account, item.Value))
            {
                container.Inventory[i] = null;
            }
            else
            {
                success = false;
                break;
            }
        }

        client.Player.SendInfo(success ?
            "Inventory full! Sending items to vault." :
            "Inventory full! Couldn't send items to vault because it is full");
    }

    private static void TakeItem(IContainer container, Client client, int slotId)
    {
        if (client.Player is null)
        {
            return;
        }

        var item = container.Inventory[slotId];
        if (item is null)
        {
            return;
        }

        for (var j = 4; j < client.Player.Inventory.Length; j++)
        {
            if (client.Player.Inventory[j]?.BaseItem is null)
            {
                container.Inventory[slotId] = null;
                client.Player.Inventory[j] = item;
                break;
            }
        }

        // Check to see if the item is still there after trying to send to inventory
        item = container.Inventory[slotId];
        if (item != null)
        {
            // Send item to player vault
            if (RedisDatabase.AddVault(client.Account, item.Value))
            {
                client.Player.SendInfo("Inventory full! Sending item to vault.");
                container.Inventory[slotId] = null;
            }
            else
            {
                client.Player.SendInfo("Inventory full! Couldn't send item to vault because it is full");
            }
        }
    }
}