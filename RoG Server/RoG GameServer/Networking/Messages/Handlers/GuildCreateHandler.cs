﻿using Database;
using Database.Models;
using GameServer.Networking.Socket;
using GameServer.Realm;
using RoGCore.Models;
using RoGCore.Networking.Packets;

namespace GameServer.Networking.Messages.Handlers;

internal class GuildCreateHandler : PacketHandler<GuildCreate>
{
    public override MessageId Id => MessageId.GuildCreate;

    public override bool RunOutsideOfLogicThread => false;

    protected static Result ProcessPacket(Client client, in GuildCreate packet)
    {
        var player = client.Player;

        if (player is null)
        {
            return new Result(false, "Internal Server Error.");
        }

        var guildName = packet.Name;

        if (client.Account.Silver < 1000)
        {
            return new Result(false, "Not enough silver to create a guild.");
        }

        if (client.Account.GuildId > 0)
        {
            return new Result(false, "You cannot create a guild as a guild member.");
        }

        switch (RedisDatabase.CreateGuild(guildName, out var guild))
        {
            case GuildCreateStatus.InvalidName:
                return new Result(false, "Guild name cannot be blank");
            case GuildCreateStatus.UsedName:
                return new Result(false, "Guild already exists.");
            case GuildCreateStatus.Ok:
                RedisDatabase.UpdateSilver(client.Account, -1000);
                RedisDatabase.AddGuildMember(guild, client.Account, true);
                player.GuildManager = GuildManager.Add(player, guild);
                return new Result(true);
        }

        return new Result(false, "Internal Server Error.");
    }

    protected override void HandlePacket(Client client, in GuildCreate packet)
    {
        client.SendPacket(ProcessPacket(client, packet));
    }
}