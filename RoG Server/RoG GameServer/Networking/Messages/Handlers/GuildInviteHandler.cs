﻿using GameServer.Networking.Socket;
using RoGCore.Models;
using RoGCore.Networking.Packets;

namespace GameServer.Networking.Messages.Handlers;

internal class GuildInviteHandler : PacketHandler<GuildInvite>
{
    public override MessageId Id => MessageId.GuildInvite;

    public override bool RunOutsideOfLogicThread => false;

    protected override void HandlePacket(Client client, in GuildInvite packet)
    {
        var player = client.Player;

        if (player is null)
        {
            return;
        }

        if (player.GuildManager is null)
        {
            player.SendInfo("You are not in a guild");
            return;
        }

        if (client.Account.GuildRank < GuildRank.Officer)
        {
            player.SendInfo("Members and initiates cannot invite!");
            return;
        }

        var target = player.Owner.GetPlayerByName(packet.PlayerName);

        if (target?.Client is null)
        {
            player.SendInfo($"Cannot find {packet.PlayerName}, player must be nearby to invite");
            return;
        }

        if (target.GuildManager is not null)
        {
            player.SendError("Player is already in a guild!");
            return;
        }

        if (target.Client.Account.Ignored.Contains(client.Account.AccountId))
        {
            return;
        }

        target.Client.SendPacket(new GuildInvite(player.Name, player.GuildManager.Guild.Name));
        target.Invited = player.GuildManager.Guild.Name;
        player.SendInfo($"{packet.PlayerName} has been invited to join {player.GuildManager.Guild.Name}");
    }
}