﻿using Database;
using GameServer.Networking.Socket;
using GameServer.Realm;
using RoGCore.Models;
using RoGCore.Networking.Packets;
using RoGCore.Utils;

namespace GameServer.Networking.Messages.Handlers;

internal class JoinGuildPacketHandler : PacketHandler<GuildJoin>
{
    public override MessageId Id => MessageId.GuildJoin;

    public override bool RunOutsideOfLogicThread => false;

    protected override void HandlePacket(Client client, in GuildJoin packet)
    {
        var player = client.Player;

        if (player is null)
        {
            return;
        }

        if (player.Invited != packet.Name)
        {
            player.SendInfo($"You were not invited to join {packet.Name}");
            return;
        }

        if (player.GuildManager != null)
        {
            player.SendInfo("Leave your current guild before joining a new one");
            return;
        }

        var guild = RedisDatabase.GetGuild(packet.Name).Exec();
        if (guild == null)
        {
            player.SendInfo("Unable to join: Guild does not exist");
            return;
        }

        RedisDatabase.AddGuildMember(guild, client.Account);
        player.GuildManager = GuildManager.Add(player, guild);
        player.SendGuild($"{player.Name} joined {guild.Name}!");
    }
}