﻿using Database;
using GameServer.Networking.Socket;
using RoGCore.Models;
using RoGCore.Networking.Packets;
using RoGCore.Utils;

namespace GameServer.Networking.Messages.Handlers;

internal class GuildRankChangeHandler : PacketHandler<GuildRankChange>
{
    public override MessageId Id => MessageId.GuildRankChange;

    public override bool RunOutsideOfLogicThread => false;

    protected override void HandlePacket(Client client, in GuildRankChange packet)
    {
        var other = RedisDatabase.ReadAccount(packet.Name).Exec();
        var player = client.Player;

        if (player is null)
        {
            return;
        }

        if (other is null)
        {
            player.SendError("Error: Account not found");
            return;
        }

        if (client.Account.GuildRank < GuildRank.Officer)
        {
            player.SendInfo("Members and initiates cannot promote!");
            return;
        }

        if (other.GuildName != client.Account.GuildName)
        {
            player.SendInfo("You can only change a player in your guild.");
            return;
        }

        RedisDatabase.ChangeGuildRank(other, packet.GuildRank);
        player.SendGuild($"{other.Name} has become a {packet.GuildRank}");
    }
}