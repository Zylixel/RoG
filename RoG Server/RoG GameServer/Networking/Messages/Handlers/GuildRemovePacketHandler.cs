﻿using Database;
using GameServer.Networking.Socket;
using GameServer.Realm.Entities.Player;
using RoGCore.Models;
using RoGCore.Networking.Packets;
using RoGCore.Utils;

namespace GameServer.Networking.Messages.Handlers;

internal class GuildRemovePacketHandler : PacketHandler<GuildRemove>
{
    public override MessageId Id => MessageId.GuildRemove;

    public override bool RunOutsideOfLogicThread => false;

    protected override void HandlePacket(Client client, in GuildRemove packet)
    {
        var player = client.Player;

        if (player is null)
        {
            return;
        }

        client.SendPacket(GetResult(client, player, packet));
    }

    private static Result GetResult(Client client, Player player, in GuildRemove packet)
    {
        var acc = RedisDatabase.ReadAccount(packet.Name).Exec();
        var clientGuild = player.GuildManager;

        if (acc is null || clientGuild is null)
        {
            return new Result(false, "Account not found");
        }

        if (acc.GuildName == clientGuild.Guild.Name && player.Name != packet.Name)
        {
            if (client.Account.GuildRank <= acc.GuildRank)
            {
                return new Result(false, "Insufficient Rank");
            }

            clientGuild.Leave(acc);
            player.SendGuild($"{player.Name} removed {acc.Name} from {clientGuild.Guild.Name}");

            return new Result(true);
        }
        else if (player.Name == packet.Name)
        {
            clientGuild.Leave(acc);
            client.SendPacket(new Result(true));
            player.SendGuild($"{player.Name} left {clientGuild.Guild.Name}");

            return new Result(true);
        }

        return new Result(false, "An Unknown Error Occured");
    }
}