﻿using Database;
using GameServer.Networking.Socket;
using GameServer.Realm;
using GameServer.Realm.Entities.GameObject;
using GameServer.Realm.Entities.Models;
using GameServer.Realm.Entities.Player;
using GameServer.Realm.World.Worlds;
using Microsoft.Extensions.Logging;
using RoGCore.Logging;
using RoGCore.Models;
using RoGCore.Models.ObjectSlot;
using RoGCore.Networking.Packets;

namespace GameServer.Networking.Messages.Handlers;

internal class InventoryMoveHandler : PacketHandler<InventoryMove>
{
    private static readonly ILogger<InventoryMoveHandler> Logger = LogFactory.LoggingInstance<InventoryMoveHandler>();

    public override MessageId Id => MessageId.InventoryMove;

    public override bool RunOutsideOfLogicThread => false;

    protected override void HandlePacket(Client client, in InventoryMove packet)
    {
        var player = client.Player;
        if (player is null)
        {
            return;
        }

        switch (packet.Origin.Type)
        {
            case ObjectSlotType.Container:
                switch (packet.Destination.Type)
                {
                    case ObjectSlotType.Ground:
                        DropItem(player, packet.Origin.SlotId);
                        return;
                    case ObjectSlotType.Vault:
                        MoveToVault(player, packet.Origin.SlotId);
                        return;
                    case ObjectSlotType.Container:
                        SwapItems(player, packet);
                        return;
                }

                break;
            case ObjectSlotType.Vault:
                switch (packet.Destination.Type)
                {
                    case ObjectSlotType.Container:
                        TakeFromVault(player, packet.Destination.SlotId, packet.Origin.ItemGuid);
                        return;
                }

                break;
        }

        player.Client.Disconnect(DisconnectReason.CheatEngineDetected);
    }

    private static void SwapItems(Player player, in InventoryMove packet)
    {
        var originEntity = player.Owner.GetEntity(packet.Origin.OwnerId);
        var destinationEntity = player.Owner.GetEntity(packet.Destination.OwnerId);

        if (originEntity is null || destinationEntity is null || originEntity.DistSqr(destinationEntity) > 1.5f * 1.5f)
        {
            return;
        }

        if (!(originEntity is IContainer originContainer && destinationEntity is IContainer destinationContainer))
        {
            return;
        }

        var item1 = originContainer.Inventory[packet.Origin.SlotId];
        var item2 = destinationContainer.Inventory[packet.Destination.SlotId];

        if (!IsValid(item1, item2, originContainer, destinationContainer, packet, player.Client))
        {
            player.Client.Disconnect(DisconnectReason.CheatEngineDetected);
            return;
        }

        if (originEntity is Player && destinationEntity is Player && originEntity.Id != destinationEntity.Id)
        {
            Logger.LogError($"{originEntity.Name} just tried to steal items from {destinationEntity.Name}'s inventory");
            return;
        }

        originContainer.Inventory[packet.Origin.SlotId] = item2;
        destinationContainer.Inventory[packet.Destination.SlotId] = item1;
        if (item2?.Data.Soulbound == true)
        {
            player.DropBag(item2);
            originContainer.Inventory[packet.Origin.SlotId] = null;
        }

        if (item1?.Data.Soulbound == true)
        {
            player.DropBag(item1);
            destinationContainer.Inventory[packet.Destination.SlotId] = null;
        }

        if (originEntity is Container containerOne)
        {
            containerOne.DespawnIfEmpty();
        }

        if (destinationEntity is Container containerTwo)
        {
            containerTwo.DespawnIfEmpty();
        }

        player.CalcBoost();
    }

    private static void TakeFromVault(Player player, int slotId, Guid guid)
    {
        if (player.Owner is not HomeBase)
        {
            return;
        }

        var items = player.Client.Account.Vault.Where(x => x.Uuid == guid);
        if (!items.Any() || slotId < 4)
        {
            return;
        }

        var item = items.First();

        if (!RedisDatabase.RemoveVault(player.Client.Account, item))
        {
            return;
        }

        var invItem = player.Inventory[slotId];
        if (invItem != null)
        {
            // No need to check if full because an item was just taken out of the vault
            RedisDatabase.AddVault(player.Client.Account, invItem.Value);
        }

        player.Inventory[slotId] = item.Cook();
        player.CalcBoost();
        VaultRequestHandler.RefreshClient(player.Client);
    }

    private static void MoveToVault(Player player, int slotId)
    {
        if (player.Owner is not HomeBase)
        {
            return;
        }

        var item = player.Inventory[slotId];

        if (item is null)
        {
            return;
        }

        if (!RedisDatabase.AddVault(player.Client.Account, item))
        {
            player.SendError("Vault is full!");
            return;
        }

        player.Inventory[slotId] = null;
        player.CalcBoost();
        VaultRequestHandler.RefreshClient(player.Client);
    }

    private static void DropItem(Player player, int slotId)
    {
        var item = player.Inventory[slotId];

        if (item is null)
        {
            return;
        }

        player.Inventory[slotId] = null;
        player.CalcBoost();

        player.DropBag(item);
    }

    private static bool IsValid(
        Item? item1,
        Item? item2,
        IContainer con1,
        IContainer con2,
        in InventoryMove packet,
        Client client)
    {
        if (con2 is Container)
        {
            return true;
        }

        var ret = true;
        switch (con1)
        {
            case Container:
                if (!con2.AuditItem(item1, packet.Destination.SlotId))
                {
                    Logger.LogInformation($"Cheat engine detected for player {client.Player?.Name},\nInvalid InvSwap.");
                }

                break;

            case Player when con2 is Player:
                ret = con1.AuditItem(item1, packet.Destination.SlotId) &&
                          con2.AuditItem(item2, packet.Origin.SlotId);
                if (!ret)
                {
                    Logger.LogInformation($"Cheat engine detected for player {client.Player?.Name},\nInvalid InvSwap.");
                }

                break;
        }

        return ret;
    }
}