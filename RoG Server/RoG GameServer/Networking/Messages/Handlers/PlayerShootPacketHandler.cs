﻿using GameServer.Networking.Socket;
using GameServer.Realm.Entities.Player;
using Microsoft.Extensions.Logging;
using RoGCore.Logging;
using RoGCore.Models;
using RoGCore.Networking.Packets;

namespace GameServer.Networking.Messages.Handlers;

internal class PlayerShootPacketHandler : PacketHandler<PlayerShoot>
{
    private static readonly ILogger<PlayerShootPacketHandler> Logger = LogFactory.LoggingInstance<PlayerShootPacketHandler>();

    public override MessageId Id => MessageId.PlayerShoot;

    public override bool RunOutsideOfLogicThread => false;

    protected override void HandlePacket(Client client, in PlayerShoot packet)
    {
        if (client.Player is null)
        {
            return;
        }

        Handle(client.Player, packet);
    }

    private static void Handle(Player player, PlayerShoot packet)
    {
        var item = player.Inventory[0];
        if (item is null)
        {
            return;
        }

        /*player.ValidatePlayerShoot(item.BaseItem, packet.Time);
        if (result == PlayerShootStatus.COOLDOWN_STILL_ACTIVE || result == PlayerShootStatus.NUM_PROJECTILE_MISMATCH)
        {
            if (player.Client != null)
            {
                Logger.LogInformation($"PlayerShoot validation failure ({player.Name}:{player.Client.Account.AccountId}): {result}");
            }
            else
            {
                Logger.LogInformation($"PlayerShoot validation failure ({player.Name}:null client): {result}");
            }

            if (player.ShotLimit.Log())
            {
                player.Client.SendPacket(new outgoing.FAILURE
                {
                    ErrorId = 8,
                    ErrorDescription =
                        JSONErrorIDHandler.
                            FormatedJSONError(
                                errorID: ErrorIDs.EXPLOIT,
                                labels: null,
                                arguments: null
                            )
                });

                player.Client.Disconnect(DisconnectReason.BADSHOOT);
            }

            // return;
        }*/

        var itemProjectiles = item.Value.BaseItem.Projectiles;

        if (itemProjectiles?.Length > 0)
        {
            player.PlayerShootProjectile(
                 packet.BulletId,
                 itemProjectiles[0],
                 packet.Time,
                 packet.Position,
                 packet.Angle.Rad,
                 item.Value);

            player.Owner.BroadcastPacket(
                new AllyShoot(
                    packet.BulletId,
                    player.Id,
                    0,
                    packet.Angle,
                    packet.Time,
                    packet.Position),
                player,
                true);
        }
        else
        {
            if (player.Client != null)
            {
                Logger.LogInformation($"PlayerShoot validation failure ({player.Name}:{player.Client.Account.AccountId}): Cannot resolve '{item.Value.BaseItem.ObjectType}' projectiles");
            }
            else
            {
                Logger.LogInformation($"PlayerShoot validation failure ({player.Name}:null client): Cannot resolve '{item.Value.BaseItem.ObjectType}' projectiles");
            }
        }
    }
}