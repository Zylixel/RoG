﻿using GameServer.Networking.Socket;
using RoGCore.Models;
using RoGCore.Networking.Packets;

namespace GameServer.Networking.Messages.Handlers;

internal class SetSummonPositionHandler : PacketHandler<SetSummonPosition>
{
    public override MessageId Id => MessageId.SetSummonPosition;

    public override bool RunOutsideOfLogicThread => false;

    protected override void HandlePacket(Client client, in SetSummonPosition packet)
    {
        if (client.Player is null)
        {
            return;
        }

        foreach (var summon in client.Player.Summons.Where(summon => summon.Controllable))
        {
            summon.DesiredPosition = packet.Position;
        }
    }
}