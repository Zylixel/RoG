﻿using GameServer.Networking.Socket;
using GameServer.Realm;
using GameServer.Realm.Commands;
using RoGCore.Models;
using RoGCore.Networking.Packets;

namespace GameServer.Networking.Messages.Handlers;

internal class TextHandler : PacketHandler<Text>
{
    public override MessageId Id => MessageId.Text;

    public override bool RunOutsideOfLogicThread => false;

    protected override void HandlePacket(Client client, in Text packet)
    {
        if (client?.Player is null)
        {
            return;
        }

        if (packet.Message[0] == '/')
        {
            CommandManager.Execute(client.Player, client.Player.Owner.CurrentTime, packet.Message);
        }
        else
        {
            if (client.Account.Muted)
            {
                client.Player.SendInfo("{\"key\":\"server.muted\"}");
                return;
            }

            if (!string.IsNullOrWhiteSpace(packet.Message))
            {
                ChatManager.Say(client.Player, packet.Message);
            }
            else
            {
                client.Player.SendInfo("{\"key\":\"server.invalid_chars\"}");
            }
        }
    }
}