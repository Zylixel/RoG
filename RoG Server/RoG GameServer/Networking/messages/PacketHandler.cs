﻿using GameServer.Networking.Socket;
using RoGCore.Models;
using RoGCore.Networking;

namespace GameServer.Networking.Messages;

internal abstract class PacketHandler<T> : IPacketHandler
    where T : IPacket
{
    public abstract MessageId Id { get; }

    public abstract bool RunOutsideOfLogicThread { get; }

    public void Handle(IClient client, in IPacket packet)
    {
        if (client is not Client cli)
        {
            throw new ArgumentException(client.ToString());
        }

        HandlePacket(cli, (T)packet);

        if (packet is IDisposablePacket disposable)
        {
            disposable.Dispose();
        }
    }

    protected abstract void HandlePacket(Client client, in T packet);
}