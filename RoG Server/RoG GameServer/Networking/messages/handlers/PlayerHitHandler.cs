﻿using System.Diagnostics;
using GameServer.Networking.Socket;
using GameServer.Realm.Entities.Models;
using Microsoft.Extensions.Logging;
using RoGCore.Logging;
using RoGCore.Models;
using RoGCore.Networking.Packets;

namespace GameServer.Networking.Messages.Handlers;

internal class PlayerHitHandler : PacketHandler<PlayerHit>
{
    private static readonly ILogger<PlayerHitHandler> Logger = LogFactory.LoggingInstance<PlayerHitHandler>();

    public override MessageId Id => MessageId.PlayerHit;

    public override bool RunOutsideOfLogicThread => false;

    protected override void HandlePacket(Client client, in PlayerHit packet)
    {
        if (client.Player is null)
        {
            return;
        }

        var hitter = client.Player.Owner.GetEntity(packet.ObjectId);

        if (hitter is null || hitter is not IProjectileOwner projOwner)
        {
            Logger.LogWarning("hitter is null or hitter is not IProjectileOwner");
            return;
        }

        Debug.Assert(projOwner.Projectiles?.Length >= packet.BulletId, "BulletId was below the projectile Owners Projectiles.Length");

        var prj = projOwner.Projectiles?[packet.BulletId];
        if (prj == null)
        {
            Logger.LogWarning("prj is null");
            return;
        }

        prj.Hit(client.Player);
    }
}