﻿namespace GameServer.Networking.Socket;

internal partial class Client
{
    internal enum SaveReason : byte
    {
        Reconnect,
        Disconnect,
        Death,
        CheckDupe,
    }
}