﻿using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Text.Json;
using GameServer.Networking;
using GameServer.Realm;
using Microsoft.Extensions.Logging;
using RoGCore.Assets;
using RoGCore.Logging;
using RoGCore.SettingsModels;

namespace GameServer;

public static class Program
{
    internal static AppSettings AppSettings = new();

    public static void Main()
    {
        var stopWatch = Stopwatch.StartNew();
        Console.Title = "Loading...";

        Thread.CurrentThread.Name = "Entry";

        InitializeLogger.Initialize(File.Open("appsettings.json", FileMode.Open));

        Task.Run(BuildDependencies).Wait();

        RuntimeHelpers.RunClassConstructor(typeof(RealmManager).TypeHandle);
        ClientServer clientServer = new();

        Console.Title = AppSettings.GameServer.Name;

        stopWatch.Stop();

        var logger = LogFactory.LoggingInstance(nameof(Program));
        logger.LogInformation("GameServer initialized in {0}ms. Use /stop or Ctrl+C to close.", stopWatch.ElapsedMilliseconds);

        Console.CancelKeyPress += (_, _) => Shutdown.Stop();

        Shutdown.ShutdownEvent += (_, _) =>
        {
            logger.LogInformation("Terminating GameServer...");
            clientServer.Stop();
            RealmManager.Stop();
            Environment.Exit(0);
        };

        string? currentLine;
        while (true)
        {
            currentLine = Console.ReadLine();

            if (currentLine is null)
            {
                continue;
            }

            switch (currentLine[1..].ToUpperInvariant())
            {
                case "SHUTDOWN":
                case "STOP":
                    Shutdown.Stop();
                    break;
                case "RESTART":
                    Shutdown.Restart();
                    break;
                case "STATS":
                    Console.Title =
        @$"{AppSettings.GameServer.Name} | 
TPS: {AppSettings.GameServer.Tps * LogicTicker.CurrentTime.StaticMspt / Math.Max(LogicTicker.CurrentTime.StaticMspt, LogicTicker.CurrentTime.LastMspt)} 
MPST: {LogicTicker.CurrentTime.LastMspt} 
Worlds: {RealmManager.Worlds.Distinct().Count()} 
Threads: {Process.GetCurrentProcess().Threads.Count}";
                    break;
                default:
                    Console.WriteLine($"Unknown command: {currentLine}");
                    break;
            }
        }
    }

    private static async Task BuildDependencies()
    {
        var jsonString = File.ReadAllText("appsettings.json");
        AppSettings = JsonSerializer.Deserialize<AppSettings>(jsonString)!;

        var startTasks = new Task[]
        {
            EmbeddedData.BuildDataAsync(),
            Database.RedisDatabase.BuildAsync(AppSettings),
        };

        await Task.WhenAll(startTasks);
    }
}