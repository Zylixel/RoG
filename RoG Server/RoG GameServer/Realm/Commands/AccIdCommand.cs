﻿using GameServer.Realm.Entities.Player;
using RoGCore.Models;

namespace GameServer.Realm.Commands;

internal class AccIdCommand() : Command("accid", AccountType.Rank.HeadStaff)
{
    internal override CommandResult Process(Player player, RealmTime time, string[] args)
    {
        if (string.IsNullOrEmpty(args[0]))
        {
            return CommandResult.Failure("Usage: /accid <player>");
        }

        var plr = RealmManager.FindPlayer(args[0]);
        return plr is null
            ? CommandResult.Failure("Account ID of " + args[0] + " not found")
            : CommandResult.Success($"Account ID of {plr.Name} : {plr.Client.Account.AccountId}");
    }
}