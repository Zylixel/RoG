﻿using GameServer.Realm.Entities.Player;
using RoGCore.Models;

namespace GameServer.Realm.Commands;

internal class AddEffCommand() : Command("addeff", AccountType.Rank.Administrator)
{
    internal override CommandResult Process(Player player, RealmTime time, string[] args)
    {
        if (args.Length == 0)
        {
            return CommandResult.Failure("Usage: /addeff <Effectname or Effectnumber>");
        }

        player.ApplyConditionEffect(new ConditionEffect
        {
            Effect = (ConditionEffectIndex)Enum.Parse(typeof(ConditionEffectIndex), args[0].Trim(), true),
            DurationMs = -1,
        });

        return CommandResult.Success("Success");
    }
}