﻿using GameServer.Realm.Entities.Player;
using RoGCore.Models;

namespace GameServer.Realm.Commands;

internal class AddItemXp() : Command("AddItemXp", AccountType.Rank.Administrator)
{
    internal override CommandResult Process(Player player, RealmTime time, string[] args)
    {
        var item = player.Inventory[int.Parse(args[0])];
        if (item != null)
        {
            item.Value.Data.IncreaseXp(uint.Parse(args[1]));
        }
        else
        {
            return CommandResult.Failure("Item not found");
        }

        return CommandResult.Success();
    }
}