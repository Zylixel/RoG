﻿using GameServer.Realm.Entities.Player;
using RoGCore.Models;

namespace GameServer.Realm.Commands;

internal class Announcement() : Command("announce", AccountType.Rank.Administrator)
{
    internal override CommandResult Process(Player player, RealmTime time, string[] args)
    {
        if (args.Length == 0)
        {
            return CommandResult.Failure("Usage: /announce <saytext>");
        }

        var saytext = string.Join(" ", args);
        RealmManager.Chat.PlayerAnnounce(saytext);
        return CommandResult.Success("Success");
    }
}