﻿using Database;
using GameServer.Realm.Entities.Player;
using RoGCore.Models;

namespace GameServer.Realm.Commands;

internal class BanCommand() : Command("ban", AccountType.Rank.Administrator)
{
    internal override CommandResult Process(Player player, RealmTime time, string[] args)
    {
        var p = RealmManager.FindPlayer(args[0]);
        if (p?.Client is null)
        {
            return CommandResult.Failure("Player not found");
        }

        RedisDatabase.BanAccount(p.Client.Account);
        p.Client.Disconnect(DisconnectReason.PlayerBanned);
        return CommandResult.Success("Success");
    }
}