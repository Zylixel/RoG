﻿using Database;
using GameServer.Realm.Entities.Player;
using RoGCore.Models;

namespace GameServer.Realm.Commands;

internal class BanIpCommand() : Command("BanIp", AccountType.Rank.HeadStaff)
{
    internal override CommandResult Process(Player player, RealmTime time, string[] args)
    {
        RedisDatabase.BanIp(args[0]);
        return CommandResult.Success("Success");
    }
}