﻿using GameServer.Realm.Entities.Player;
using RoGCore.Models;

namespace GameServer.Realm.Commands;

internal class ColorCommand() : Command("color", AccountType.Rank.Vip)
{
    internal override CommandResult Process(Player player, RealmTime time, string[] args)
    {
        var decValue = Convert.ToInt32(args[0], 16);
        player.Client.Account.Color = player.GlowColor = new RGB(decValue);
        player.Client.Account.Flush();
        return CommandResult.Success();
    }
}