﻿using System.Collections.Frozen;
using GameServer.Realm.Entities.Player;
using Microsoft.Extensions.Logging;
using RoGCore.Logging;

namespace GameServer.Realm.Commands;

internal static class CommandManager
{
    internal static readonly FrozenDictionary<string, Command> Commands;

    private static readonly ILogger Logger = LogFactory.LoggingInstance(nameof(CommandManager));

    static CommandManager()
    {
        var commands = new Dictionary<string, Command>();
        var validCommands = typeof(Command).Assembly.GetTypes().Where(x => typeof(Command).IsAssignableFrom(x) && x != typeof(Command));
        var current = 0;
        foreach (var i in validCommands)
        {
            var instance = (Command?)Activator.CreateInstance(i);
            if (instance is null)
            {
                Logger.LogError($"ERROR loading {i.Name} : instance is null");
                continue;
            }

            foreach (var commandName in instance.CommandNames)
            {
                commands.Add(commandName, instance);
                Logger.LogStatus($"/{commandName}", current++, commands.Count);
            }
        }

        Commands = commands.ToFrozenDictionary(StringComparer.InvariantCultureIgnoreCase);

        Logger.LogResults(
            $"Command{(Commands.Count > 1 ? "s" : string.Empty)}",
            Commands.Count);
    }

    // Forcibly load the commands
    internal static void Load()
    {
    }

    internal static bool Execute(Player player, RealmTime time, string text)
    {
        var index = text.IndexOf(' ');
        var cmd = text.AsSpan().Slice(1, index == -1 ? text.Length - 1 : index - 1);
        var args = index == -1 ? string.Empty : text.AsSpan()[(index + 1)..];

        if (!Commands.TryGetValue(cmd.ToString(), out var command))
        {
            player.SendInfo($"Unknown command: {text}");
            return false;
        }

        Logger.LogInformation($"[CommandString] <{player.Name}> {text}");
        return command.Execute(player, time, args.ToString());
    }
}