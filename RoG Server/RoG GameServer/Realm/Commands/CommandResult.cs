﻿namespace GameServer.Realm.Commands;

internal readonly struct CommandResult
{
    internal readonly bool IsSuccess;

    internal readonly string? Message;

    private CommandResult(bool isSuccess, string? message)
    {
        IsSuccess = isSuccess;
        Message = message;
    }

    internal static CommandResult Success(string? message = null) => new CommandResult(true, message);

    internal static CommandResult Failure(string? message = null) => new CommandResult(false, message);
}
