﻿using GameServer.Realm.Entities.Player;
using RoGCore.Utils;

namespace GameServer.Realm.Commands;

internal class GChatCommand() : Command("g", "guild")
{
    internal override CommandResult Process(Player player, RealmTime time, string[] args)
    {
        if (player.GuildManager == null)
        {
            return CommandResult.Failure("You need to be in a guild to use guild chat!");
        }

        var saytext = string.Join(" ", args);
        if (string.IsNullOrWhiteSpace(saytext))
        {
            return CommandResult.Failure("Usage: /guild <text>");
        }

        player.GuildManager.Chat(player, saytext.ToSafeText());
        return CommandResult.Success();
    }
}