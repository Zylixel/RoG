﻿using System.Diagnostics;
using GameServer.Realm.Entities.Player;
using RoGCore.Assets;
using RoGCore.Models;

namespace GameServer.Realm.Commands;

internal class GiveCommand() : Command("give", AccountType.Rank.Administrator)
{
    internal override CommandResult Process(Player player, RealmTime time, string[] args)
    {
        Debug.Assert(player.Client is not null, "Unexpected Player.Client is null");

        if (args.Length == 0)
        {
            return CommandResult.Failure("Usage: /give <Itemname>");
        }

        var name = string.Join(" ", args).Trim();
        if (!EmbeddedData.IdToItemType.TryGetValue(name, out var objType))
        {
            return CommandResult.Failure("Unknown item!");
        }

        for (var i = 4; i < player.Inventory.Length; i++)
        {
            if (player.Inventory[i]?.BaseItem is null)
            {
                player.Inventory[i] = new Item(
                    EmbeddedData.Items[objType],
                    player.Client.Account.Rank < 4);
                return CommandResult.Success("Success");
            }
        }

        return CommandResult.Failure("Item cannot be given!");
    }
}