﻿using GameServer.Realm.Entities.Player;
using RoGCore.Models;

namespace GameServer.Realm.Commands;

internal class GodCommand() : Command("god", AccountType.Rank.Administrator)
{
    internal override CommandResult Process(Player player, RealmTime time, string[] args)
    {
        if (player.HasConditionEffect(ConditionEffectIndex.Invincible))
        {
            player.ApplyConditionEffect(new ConditionEffect
            {
                Effect = ConditionEffectIndex.Invincible,
                DurationMs = 0,
            });
            return CommandResult.Success("Godmode Off");
        }

        player.ApplyConditionEffect(new ConditionEffect
        {
            Effect = ConditionEffectIndex.Invincible,
            DurationMs = -1,
        });
        return CommandResult.Success("Godmode On");
    }
}