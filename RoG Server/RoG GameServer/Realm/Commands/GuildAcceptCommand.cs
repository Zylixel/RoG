﻿using Database;
using GameServer.Realm.Entities.Player;
using RoGCore.Utils;

namespace GameServer.Realm.Commands;

internal class GuildAcceptCommand() : Command("GAccept")
{
    internal override CommandResult Process(Player player, RealmTime time, string[] args)
    {
        var playerGuild = player.GuildManager?.Guild;
        if (playerGuild is null)
        {
            return CommandResult.Failure("You need to be in a guild to use this command.");
        }

        if (player.Client?.Account.GuildRank < RoGCore.Models.GuildRank.Founder)
        {
            return CommandResult.Failure("You must be the guild founder to use this command");
        }

        // if (GuildInfo.Members.Length < 5 &&)
        //  return CommandResult.Failure("Your Guild must have more than 5 members to participate!");
        var challengingGuildName = RedisDatabase.FindWarRequest(playerGuild.Name);
        if (challengingGuildName == "-1")
        {
            return CommandResult.Failure("Nobody has challenged your Guild!");
        }

        var challengingGuild = RedisDatabase.GetGuild(challengingGuildName).Exec();
        if (challengingGuild is null)
        {
            return CommandResult.Failure("Nobody has challenged your Guild!");
        }

        RedisDatabase.AcceptGuildWarReq(challengingGuild, playerGuild);
        player.SendGuild($"{player.Name} has started a War with {challengingGuild.Name}");
        player.SendGuild(challengingGuild, $"You have gone to War with {playerGuild.Name}");
        return CommandResult.Success("Success");
    }
}