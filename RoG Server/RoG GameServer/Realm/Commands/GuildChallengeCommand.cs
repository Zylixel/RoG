﻿using Database;
using GameServer.Realm.Entities.Player;
using RoGCore.Models;
using RoGCore.Utils;

namespace GameServer.Realm.Commands;

internal class GuildChallengeCommand() : Command("GChallenge")
{
    internal override CommandResult Process(Player player, RealmTime time, string[] args)
    {
        var playerGuild = player.GuildManager?.Guild;
        if (string.IsNullOrWhiteSpace(args[0]))
        {
            return CommandResult.Failure("Usage: /gchallenge <Guild>");
        }

        if (playerGuild is null)
        {
            return CommandResult.Failure("You need to be in a guild to use this command.");
        }

        if (player.Client?.Account.GuildRank < GuildRank.Founder)
        {
            return CommandResult.Failure("You must be the guild founder to use this command");
        }

        if (playerGuild.Members.Length < 5)
        {
            return CommandResult.Failure("Your Guild must have more than 5 members to participate!");
        }

        var challengedGuild = RedisDatabase.GetGuild(args[0]).Exec();
        if (challengedGuild is null)
        {
            return CommandResult.Failure("Guild does not exist.");
        }

        if (RedisDatabase.FindWarRequest(challengedGuild.Name) != "-1")
        {
            return CommandResult.Failure("Guild already has a GuildWar Request");
        }

        if (challengedGuild.WarId != 0)
        {
            return CommandResult.Failure("Guild is already in a GuildWar!");
        }

        RedisDatabase.AddGuildWarReq(playerGuild.Name, challengedGuild.Name);
        player.SendGuild($"{player.Name} has challenged {challengedGuild.Name} to a War!");
        player.SendGuild(challengedGuild, $"{playerGuild.Name} has challenged your Guild to a War!");
        return CommandResult.Success("Success");
    }
}