﻿using Database;
using GameServer.Realm.Entities.Player;
using RoGCore.Utils;

namespace GameServer.Realm.Commands;

internal class GuildDenyCommand() : Command("GDeny")
{
    internal override CommandResult Process(Player player, RealmTime time, string[] args)
    {
        var playerGuild = player.GuildManager?.Guild;
        if (playerGuild is null)
        {
            return CommandResult.Failure("You need to be in a guild to use this command.");
        }

        if (player.Client?.Account.GuildRank < RoGCore.Models.GuildRank.Founder)
        {
            return CommandResult.Failure("You must be the guild founder to use this command");
        }

        var challengingGuildName = RedisDatabase.FindWarRequest(playerGuild.Name);
        if (challengingGuildName == "-1")
        {
            return CommandResult.Failure("Nobody has challenged your Guild!");
        }

        var challengingGuild = RedisDatabase.GetGuild(challengingGuildName).Exec();
        if (challengingGuild is null)
        {
            return CommandResult.Success("Success");
        }

        RedisDatabase.RemGuildWarReq(challengingGuild.Name);
        player.SendGuild(challengingGuild, $"The Guild War request to {playerGuild.Name} was denied");
        return CommandResult.Success("Success");
    }
}