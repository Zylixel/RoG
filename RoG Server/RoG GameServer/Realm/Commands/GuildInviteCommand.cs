﻿namespace GameServer.Realm.Commands;

// TODO: Implement
/*internal class GuildInviteCommand : CommandString
{
    public GuildInviteCommand() : base("invite") { }

    internal override CommandResult Process(Player player, RealmTime time, string[] args)
    {
        if (string.IsNullOrWhiteSpace(args[0]))
        {
            return CommandResult.Failure("Usage: /invite <player name>");
        }

        if (player.GuildManager is null)
        {
            return CommandResult.Failure("You are not in a guild");
        }

        if (player.Client is null || player.Owner is null)
        {
            return CommandResult.Failure("An error occured");
        }

        if (player.Client.Account.GuildRank >= 20)
        {
            Player target = player.Owner.GetPlayerByName(args[0]);

            if (target is null)
            {
                player.SendInfo($"Cannot find {args[0]}, player must have be nearby to invite");
                return CommandResult.Failure();
            }

            if (target.Client != null && target.GuildManager is null)
            {
                if (target.Client.Account.Ignored.Contains(player.Client.Account.AccountId))
                {
                    return CommandResult.Failure("Player has blocked you");
                }

                target.Client.SendPacket(new INVITEDTOGUILD
                {
                    Name = player.Name,
                    GuildName = player.GuildManager.Guild.Name
                });
                target.Invited = player.GuildManager.Guild.Name;

                player.SendInfo($"{target.Name} has been invited to join {player.GuildManager.Guild.Name}");
                return CommandResult.Success();
            }

            return CommandResult.Failure("Player is already in a guild!");
        }
        else
        {
            return CommandResult.Failure("Members and initiates cannot invite!");
        }
    }
}*/