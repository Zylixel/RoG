﻿using Database;
using GameServer.Realm.Entities.Player;
using RoGCore.Utils;

namespace GameServer.Realm.Commands;

internal class GuildJoinCommand() : Command("join")
{
    internal override CommandResult Process(Player player, RealmTime time, string[] args)
    {
        if (string.IsNullOrWhiteSpace(args[0]))
        {
            return CommandResult.Failure("Usage: /join <guild name>");
        }

        if (player.Invited != args[0])
        {
            return CommandResult.Failure($"You were not invited to join {args[0]}");
        }

        if (player.GuildManager != null)
        {
            return CommandResult.Failure("Leave your current guild before joining a new one");
        }

        var guild = RedisDatabase.GetGuild(args[0]).Exec();
        if (guild != null && player.Client != null)
        {
            RedisDatabase.AddGuildMember(guild, player.Client.Account);
            player.GuildManager = GuildManager.Add(player, guild);
            player.SendGuild($"{player.Name} joined {guild.Name}!");
            return CommandResult.Success();
        }

        return CommandResult.Failure("Unable to join: Guild does not exist");
    }
}