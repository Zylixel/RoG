﻿using GameServer.Realm.Entities.Player;
using RoGCore.Models;

namespace GameServer.Realm.Commands;

internal class ItemName() : Command("ItemName", AccountType.Rank.Vip)
{
    internal override CommandResult Process(Player player, RealmTime time, string[] args)
    {
        List<string> newName = new(args);
        newName.RemoveAt(0);
        var name = string.Join(" ", newName).Trim();
        if (!name.All(char.IsLetter))
        {
            return CommandResult.Failure("Invalid Name, Must only contain letters");
        }

        switch (name.Length)
        {
            case < 3:
                return CommandResult.Failure("Invalid Name, Name must be larger than three characters");
            case > 20:
                return CommandResult.Failure("Invalid Name, Name must be smaller than twenty characters");
        }

        var item = player.Inventory[int.Parse(args[0])];
        if (item != null)
        {
            item.Value.Data.Name = name;
            player.Inventory[int.Parse(args[0])] = item;
        }
        else
        {
            return CommandResult.Failure("Item not found");
        }

        return CommandResult.Success();
    }
}