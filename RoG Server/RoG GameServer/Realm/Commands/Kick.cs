﻿using GameServer.Realm.Entities.Player;
using RoGCore.Models;

namespace GameServer.Realm.Commands;

internal class Kick() : Command("kick", AccountType.Rank.Moderator)
{
    internal override CommandResult Process(Player player, RealmTime time, string[] args)
    {
        if (args.Length == 0)
        {
            return CommandResult.Failure("Usage: /kick <playername>");
        }

        foreach (var i in player.Owner.Players)
        {
            if (string.Equals(i.Name, args[0].Trim(), StringComparison.OrdinalIgnoreCase))
            {
                i.Client.Disconnect(DisconnectReason.PlayerKick);
            }
        }

        return CommandResult.Success("Player Disconnected");
    }
}