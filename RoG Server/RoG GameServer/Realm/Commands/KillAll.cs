﻿using GameServer.Realm.Entities.Player;
using RoGCore.Models;
using RoGCore.Utils;

namespace GameServer.Realm.Commands;

internal class KillAll() : Command("killAll", AccountType.Rank.Administrator)
{
    internal override CommandResult Process(Player player, RealmTime time, string[] args)
    {
        var iterations = 0;
        var lastKilled = -1;
        var killed = 0;

        var mobName = args.Aggregate((s, a) => $"{s} {a}");
        while (killed != lastKilled)
        {
            lastKilled = killed;
            foreach (var i in player.Owner.Enemies.Where(e =>
                e.ObjectDescription.Name.ContainsIgnoreCase(mobName)))
            {
                i.Death();
                killed++;
            }

            foreach (var i in player.Owner.Objects.Where(e =>
                e.ObjectDescription.Name.ContainsIgnoreCase(mobName)))
            {
                player.Owner.LeaveWorld(i);
                killed++;
            }

            if (++iterations >= 5)
            {
                break;
            }
        }

        return CommandResult.Success($"{killed} enemy killed!");
    }
}