﻿using GameServer.Realm.Entities.Player;
using RoGCore.Models;

namespace GameServer.Realm.Commands;

internal class LinkCommand() : Command("link", AccountType.Rank.Administrator)
{
    internal override CommandResult Process(Player player, RealmTime time, string[] args)
    {
        var world = player.Owner;
        if (world is null || world.Id < 0)
        {
            return CommandResult.Failure("You cannot link this world.");
        }

        RealmPortalMonitor.WorldAdded(world);
        RealmManager.Chat.Announce($"A portal to {world.Name} has opened up in the nexus!");
        return CommandResult.Success("World Linked");
    }
}