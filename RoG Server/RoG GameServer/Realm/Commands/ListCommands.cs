﻿using GameServer.Realm.Entities.Player;

namespace GameServer.Realm.Commands;

internal class ListCommands() : Command("commands")
{
    internal override CommandResult Process(Player player, RealmTime time, string[] args)
    {
        var commands = CommandManager.Commands
            .Where(command => (int)command.Value.PermissionLevel <= player.Client.Account.Rank)
            .Select(command => command.Key);

        return CommandResult.Success(string.Join(", ", commands));
    }
}