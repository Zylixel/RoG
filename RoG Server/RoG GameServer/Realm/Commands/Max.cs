﻿using GameServer.Realm.Entities.Player;
using RoGCore.Models;

namespace GameServer.Realm.Commands;

internal class Max() : Command("max", AccountType.Rank.Administrator)
{
    internal override CommandResult Process(Player player, RealmTime time, string[] args)
    {
        for (var i = 0; i < Enum.GetValues(typeof(PlayerStats)).Length - 1; i++)
        {
            if (player.PlayerStatData.Length <= i)
            {
                break;
            }

            var statData = player.PlayerStatData[i];
            if (statData.Length <= 1)
            {
                break;
            }

            player.Stats[i] = (short)statData[1];
        }

        return CommandResult.Success("Success");
    }
}