﻿using Database;
using GameServer.Realm.Entities.Player;
using RoGCore.Models;

namespace GameServer.Realm.Commands;

internal class Mute() : Command("mute", AccountType.Rank.Moderator)
{
    internal override CommandResult Process(Player player, RealmTime time, string[] args)
    {
        if (args.Length == 0)
        {
            return CommandResult.Failure("Usage: /mute <playername>");
        }

        foreach (var i in player.Owner.Players)
        {
            var client = i.Client;
            if (string.Equals(i.Name, args[0].Trim(), StringComparison.OrdinalIgnoreCase))
            {
                continue;
            }

            client.Account.Muted = true;
            RedisDatabase.MuteAccount(client.Account);
        }

        return CommandResult.Success("Player Muted.");
    }
}