﻿using GameServer.Realm.Entities.Player;
using RoGCore.SettingsModels;

namespace GameServer.Realm.Commands;

internal class OnlineCommand() : Command("online", "playersOnline")
{
    internal override CommandResult Process(Player player, RealmTime time, string[] args)
    {
        const int serverMaxUsage = NetworkingSettings.MAXCONNECTIONS;
        var serverCurrentUsage = RealmManager.Clients.Keys.Count;
        var worldCurrentUsage = player.Owner.Players.Count;
        return CommandResult.Success(
            $"Server: {serverCurrentUsage}/{serverMaxUsage} player{(serverCurrentUsage > 1 ? "s" : string.Empty)} | {player.Owner.Name}: {worldCurrentUsage} player{(worldCurrentUsage > 1 ? "s" : string.Empty)}. Use /who to see players in world");
    }
}