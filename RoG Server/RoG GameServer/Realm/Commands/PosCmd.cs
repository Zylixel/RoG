﻿using GameServer.Realm.Entities.Player;
using RoGCore.Models;

namespace GameServer.Realm.Commands;

internal class PosCmd() : Command("p", AccountType.Rank.Administrator)
{
    internal override CommandResult Process(Player player, RealmTime time, string[] args)
    {
        return CommandResult.Success($"X: {(int)player.X} - Y: {(int)player.Y}");
    }
}