﻿using GameServer.Realm.Entities.Player;
using RoGCore.Networking.Packets;

namespace GameServer.Realm.Commands;

internal class RealmCommand() : Command("Realm")
{
    internal override CommandResult Process(Player player, RealmTime time, string[] args)
    {
        player.Client.Reconnect(new Reconnect("Gilgor's Realm", RealmPortalMonitor.GetRealm().Id, 0), true);
        return CommandResult.Success("Success");
    }
}