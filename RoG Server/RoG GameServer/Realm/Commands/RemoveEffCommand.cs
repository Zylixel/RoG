﻿using GameServer.Realm.Entities.Player;
using RoGCore.Models;

namespace GameServer.Realm.Commands;

internal class RemoveEffCommand() : Command("remeff", AccountType.Rank.Moderator)
{
    internal override CommandResult Process(Player player, RealmTime time, string[] args)
    {
        if (args.Length == 0)
        {
            return CommandResult.Failure("Usage: /remeff <Effectname or Effectnumber>");
        }

        player.ApplyConditionEffect(new ConditionEffect
        {
            Effect = (ConditionEffectIndex)Enum.Parse(typeof(ConditionEffectIndex), args[0].Trim(), true),
            DurationMs = 0,
        });

        return CommandResult.Success("Success");
    }
}