﻿using GameServer.Realm.Entities.Player;
using GameServer.Realm.World;
using RoGCore.Models;

namespace GameServer.Realm.Commands;

internal class RestartCommand() : Command("restart", AccountType.Rank.HeadStaff)
{
    internal override CommandResult Process(Player player, RealmTime time, string[] args)
    {
        player.Owner.Timers.Add(new WorldTimer(10 * 1000, (_, _) => Shutdown.Restart()));
        RealmManager.Chat.Announce("Server restarting soon. You will be disconnected in 10 seconds");
        return CommandResult.Success("Success");
    }
}