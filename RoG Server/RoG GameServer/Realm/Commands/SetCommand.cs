﻿using GameServer.Realm.Entities.Player;
using RoGCore.Models;
using RoGCore.Utils;

namespace GameServer.Realm.Commands;

internal class SetCommand() : Command("setStat", AccountType.Rank.Administrator)
{
    internal override CommandResult Process(Player player, RealmTime time, string[] args)
    {
        switch (args.Length)
        {
            case 2:
                try
                {
                    var stat = args[0].ToLower();
                    var amount = short.Parse(args[1]);
                    if (amount > Program.AppSettings.GameServer.MaximumStat && stat != "health" && stat != "mana" && stat != "hp" && stat != "mp")
                    {
                        return CommandResult.Failure($"That's a bit excessive... Keep it under {Program.AppSettings.GameServer.MaximumStat}");
                    }

                    switch (stat)
                    {
                        case "health":
                        case "hp":
                            player.Stats[0] = amount;
                            break;
                        case "mana":
                        case "mp":
                            player.Stats[1] = amount;
                            break;
                        case "atk":
                        case "attack":
                            player.Stats[2] = amount;
                            break;
                        case "def":
                        case "defense":
                            player.Stats[3] = amount;
                            break;
                        case "spd":
                        case "speed":
                            player.Stats[4] = amount;
                            break;
                        case "vit":
                        case "vitality":
                            player.Stats[5] = amount;
                            break;
                        case "wis":
                        case "wisdom":
                            player.Stats[6] = amount;
                            break;
                        case "dex":
                        case "dexterity":
                            player.Stats[7] = amount;
                            break;
                        default:
                            return CommandResult.Failure("Invalid Stat");
                    }
                }
                catch
                {
                    return CommandResult.Failure("Error while setting stat");
                }

                return CommandResult.Success("Success");
            case 3 when player.Client.Account.Rank < 3:
                return CommandResult.Failure("Only higher ranked admins can set other players stats");
            case 3:
                {
                    foreach (var i in RealmManager.Clients.Values)
                    {
                        if (i.Player != null && i.Account.Name.EqualsIgnoreCase(args[0]))
                        {
                            try
                            {
                                var stat = args[1].ToLower();
                                var amount = short.Parse(args[2]);
                                switch (stat)
                                {
                                    case "health":
                                    case "hp":
                                        i.Player.Stats[0] = amount;
                                        break;
                                    case "mana":
                                    case "mp":
                                        i.Player.Stats[1] = amount;
                                        break;
                                    case "atk":
                                    case "attack":
                                        i.Player.Stats[2] = amount;
                                        break;
                                    case "def":
                                    case "defense":
                                        i.Player.Stats[3] = amount;
                                        break;
                                    case "spd":
                                    case "speed":
                                        i.Player.Stats[4] = amount;
                                        break;
                                    case "vit":
                                    case "vitality":
                                        i.Player.Stats[5] = amount;
                                        break;
                                    case "wis":
                                    case "wisdom":
                                        i.Player.Stats[6] = amount;
                                        break;
                                    case "dex":
                                    case "dexterity":
                                        i.Player.Stats[7] = amount;
                                        break;
                                    default:
                                        return CommandResult.Failure("Invalid Stat");
                                }

                                player.SendInfo("Success");
                            }
                            catch
                            {
                                return CommandResult.Failure("Error while setting stat");
                            }

                            return CommandResult.Success("Success");
                        }
                    }

                    return CommandResult.Failure("Player could not be found!");
                }
        }

        return CommandResult.Failure("Usage: /setStat <Stat> <Amount> or /setStat <Player> <Stat> <Amount>");
    }
}