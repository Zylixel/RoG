﻿using GameServer.Realm.Entities.Player;
using RoGCore.Models;
using RoGCore.Utils;

namespace GameServer.Realm.Commands;

internal class SizeCommand() : Command("size", AccountType.Rank.Administrator)
{
    internal override CommandResult Process(Player player, RealmTime time, string[] args)
    {
        if (string.IsNullOrEmpty(args[0]))
        {
            return CommandResult.Failure(
                "Usage: / size < positive integer >. Using 0 will restore the default size for the sprite.");
        }

        var size = StringUtils.FromString(args[0]);
        if (size is (< 5 and not 0) or > 500)
        {
            return CommandResult.Failure(
                $"Invalid size. Size needs to be within the range: 5-500. Use 0 to reset size to default.");
        }

        player.Size = size;
        return CommandResult.Success("Success");
    }
}