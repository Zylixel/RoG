﻿using GameServer.Realm.Entities;
using GameServer.Realm.Entities.Enemy;
using GameServer.Realm.Entities.Player;
using RoGCore.Assets;
using RoGCore.Models;

namespace GameServer.Realm.Commands;

internal class SpawnCommand() : Command("spawn", AccountType.Rank.Administrator)
{
    internal override CommandResult Process(Player player, RealmTime time, string[] args)
    {
        if (args.Length > 0 && int.TryParse(args[0], out var num))
        {
            var name = string.Join(" ", args.Skip(1).ToArray());
            if (!ParseName(ref name))
            {
                return CommandResult.Failure("Unknown Entity.");
            }

            var c = int.Parse(args[0]);
            if (c > 25)
            {
                return CommandResult.Failure("Maximum spawn count is set to 25.");
            }

            for (var i = 0; i < num; i++)
            {
                var entity = Entity.Resolve(name, player.Owner);
                if (entity is Enemy enemy)
                {
                    enemy.GivesItemXp = false;
                }

                // TODO: Reimplement timer
                name = entity.Name;
                entity.Move(player.X, player.Y);

                // player.Owner.Timers.Add(new WorldTimer(5 * 1000, (_, _) => player.Owner.EnterWorld(entity)));
            }

            ChatManager.Say(player, $"Spawning {c} {name} in 5 seconds...");
        }
        else
        {
            var name = string.Join(" ", args);
            if (!ParseName(ref name))
            {
                return CommandResult.Failure("Unknown Entity.");
            }

            var entity = Entity.Resolve(name, player.Owner);
            if (entity is Enemy enemy)
            {
                enemy.GivesItemXp = false;
            }

            // TODO: Reimplement timer
            entity.Move(player.X, player.Y);

            // player.Owner.Timers.Add(new WorldTimer(5 * 1000, (_, _) => player.Owner.EnterWorld(entity)));
            ChatManager.Say(player, "Spawning " + entity.Name + " in 5 seconds...");
        }

        return CommandResult.Success("Success");
    }

    private static bool ParseName(ref string name)
    {
        if (EmbeddedData.BetterObjects.ContainsKey(name))
        {
            return true;
        }

        foreach (var objectName in EmbeddedData.BetterObjects.Keys)
        {
            if (objectName.Contains(name, StringComparison.InvariantCultureIgnoreCase))
            {
                name = objectName;
                return true;
            }
        }

        return false;
    }
}