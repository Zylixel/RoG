﻿using GameServer.Realm.Entities.Player;
using RoGCore.Models;
using RoGCore.Networking.Packets;

namespace GameServer.Realm.Commands;

internal class SummonAll() : Command("summonall", AccountType.Rank.Administrator)
{
    internal override CommandResult Process(Player player, RealmTime time, string[] args)
    {
        foreach (var i in RealmManager.Clients)
        {
            if (i.Value.Player is null)
            {
                continue;
            }

            if (i.Value.Player.Owner == player.Owner)
            {
                i.Value.Player.Move(player.X, player.Y);
                player.Client.SendPacket(new Move(player.Position));
            }
            else
            {
                player.Client.SendPacket(new Reconnect(player.Owner.Name, player.Owner.Id, player.Owner.Difficulty));
            }
        }

        return CommandResult.Success();
    }
}