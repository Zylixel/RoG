﻿using GameServer.Realm.Entities.Player;
using RoGCore.Networking.Packets;

namespace GameServer.Realm.Commands;

internal class TeleportCommand() : Command("teleport", "tp")
{
    internal override CommandResult Process(Player player, RealmTime time, string[] args)
    {
        if (string.Equals(player.Name, args[0], StringComparison.OrdinalIgnoreCase))
        {
            return CommandResult.Failure("You are already at yourself, and always will be!");
        }

        foreach (var i in player.Owner.Players)
        {
            if (!string.Equals(i.Name, args[0].Trim(), StringComparison.OrdinalIgnoreCase))
            {
                continue;
            }

            return CommandResult.Success(player.Teleport(new Teleport(i.Id)));
        }

        return CommandResult.Failure($"Cannot teleport, {args[0].Trim()} not found!");
    }
}