﻿using GameServer.Realm.Entities.Player;
using RoGCore.Networking.Packets;
using RoGCore.Utils;

namespace GameServer.Realm.Commands;

internal class TellCommand() : Command("tell")
{
    internal override CommandResult Process(Player player, RealmTime time, string[] args)
    {
        if (args.Length < 2)
        {
            return CommandResult.Failure("Usage: /tell <player name> <text>");
        }

        var playername = args[0].Trim();
        var msg = string.Join(" ", args, 1, args.Length - 1);
        if (string.Equals(player.Name, playername, StringComparison.OrdinalIgnoreCase))
        {
            return CommandResult.Failure("Quit telling yourself!");
        }

        foreach (var i in RealmManager.Clients.Values)
        {
            if (i.Player is null)
            {
                continue;
            }

            if (i.Account.Name.EqualsIgnoreCase(playername))
            {
                player.Client.SendPacket(new Text(
                    player.Name,
                    player.Id,
                    msg.ToSafeText(),
                    new RoGCore.Models.RGB(0x0080ff),
                    new RoGCore.Models.RGB(0x0080ff),
                    i.Account.Name));

                i.SendPacket(new Text(
                    player.Name,
                    i.Player.Owner.Id,
                    msg.ToSafeText(),
                    new RoGCore.Models.RGB(0x0080ff),
                    new RoGCore.Models.RGB(0x0080ff),
                    i.Account.Name));

                return CommandResult.Success();
            }
        }

        return CommandResult.Failure($"{playername} not found.");
    }
}