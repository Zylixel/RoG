﻿using GameServer.Realm.Entities.Player;
using RoGCore.Models;
using RoGCore.Networking.Packets;

namespace GameServer.Realm.Commands;

internal class TpPosCommand() : Command("tpPos", AccountType.Rank.Administrator)
{
    internal override CommandResult Process(Player player, RealmTime time, string[] args)
    {
        if (args.Length < 2)
        {
            return CommandResult.Failure("Usage: /tp <X coordinate> <Y coordinate>");
        }

        int x, y;
        try
        {
            x = int.Parse(args[0]);
            y = int.Parse(args[1]);
        }
        catch
        {
            return CommandResult.Failure("Invalid coordinates!");
        }

        player.Move(x + 0.5f, y + 0.5f);
        player.Client.SendPacket(new Move(player.Position));

        return CommandResult.Success("Success");
    }
}