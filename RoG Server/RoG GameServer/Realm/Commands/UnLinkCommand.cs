﻿using GameServer.Realm.Entities.Player;
using RoGCore.Models;

namespace GameServer.Realm.Commands;

internal class UnLinkCommand() : Command("unlink", AccountType.Rank.Administrator)
{
    internal override CommandResult Process(Player player, RealmTime time, string[] args)
    {
        var world = player.Owner;
        if (world is null || world.Id < 0)
        {
            return CommandResult.Failure("You cannot unlink this world.");
        }

        RealmPortalMonitor.WorldRemoved(world);
        return CommandResult.Success("Link removed.");
    }
}