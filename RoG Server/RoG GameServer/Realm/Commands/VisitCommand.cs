﻿using GameServer.Realm.Entities.Player;
using RoGCore.Models;
using RoGCore.Networking.Packets;
using RoGCore.Utils;

namespace GameServer.Realm.Commands;

internal class VisitCommand() : Command("visit", AccountType.Rank.Vip)
{
    internal override CommandResult Process(Player player, RealmTime time, string[] args)
    {
        foreach (var client in RealmManager.Clients.Values)
        {
            if (client.Player is null)
            {
                continue;
            }

            var otherWorld = client.Player.Owner;
            if (!client.Player.Name.EqualsIgnoreCase(args[0]))
            {
                continue;
            }

            if (client.Player.Owner == player.Owner)
            {
                player.Move(client.Player.Position);
                player.Client.SendPacket(new Move(client.Player.Position));
            }
            else if (otherWorld != null)
            {
                player.Client.SendPacket(new Reconnect(otherWorld.Name, otherWorld.Id, otherWorld.Difficulty));
                return CommandResult.Success($"You are visiting {otherWorld.Id}");
            }
        }

        return CommandResult.Failure($"Player '{args[0]}' could not be found!");
    }
}