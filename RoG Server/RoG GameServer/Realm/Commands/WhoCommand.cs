﻿using GameServer.Realm.Entities.Player;

namespace GameServer.Realm.Commands;

internal class WhoCommand() : Command("who")
{
    internal override CommandResult Process(Player player, RealmTime time, string[] args)
    {
        var playerNames = player.Owner.Players.Select(player => player.Name);
        return CommandResult.Success("Players online: " + string.Join(", ", playerNames));
    }
}