﻿using GameServer.Realm.Entities.Player;
using RoGCore.Models;
using RoGCore.Utils;

namespace GameServer.Realm.Commands;

internal class WhosAlive() : Command("WhosAlive", AccountType.Rank.HeadStaff)
{
    internal override CommandResult Process(Player player, RealmTime time, string[] args)
    {
        var mobName = args.Aggregate((s, a) => $"{s} {a}");
        foreach (var i in player.Owner.Enemies.Where(e =>
            e.ObjectDescription.Name.ContainsIgnoreCase(mobName)))
        {
            player.SendInfo($"{i.ObjectDescription.Name} is Alive");
        }

        return CommandResult.Success("Success");
    }
}