﻿namespace GameServer.Realm.Entities;

public class ActivateBoost
{
    private readonly List<int> amounts;

    public ActivateBoost()
    {
        amounts = [];
    }

    public int GetBoost()
    {
        amounts.Sort();
        var boost = 0;
        for (var i = 0; i < amounts.Count; i++)
        {
            boost += (int)(amounts[amounts.Count - 1 - i] * Math.Pow(.5, i));
        }

        return boost;
    }

    public void Push(int amount)
    {
        amounts.Add(amount);
    }

    public void Pop(int amount)
    {
        if (amounts.Count > 0)
        {
            amounts.Remove(amount);
        }
    }
}