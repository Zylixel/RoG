﻿using System.Diagnostics.CodeAnalysis;
using System.Numerics;
using GameServer.Logic.Behaviors.Transition;
using GameServer.Logic.Engine;
using GameServer.Realm.Entities.GameObject;
using GameServer.Realm.Entities.Models;
using Microsoft.Extensions.Logging;
using RoGCore.Assets;
using RoGCore.Logging;
using RoGCore.Models;

namespace GameServer.Realm.Entities;

public partial class Entity : IProjectileOwner, QuadTree.IHasVector2, IEquatable<Entity>, IEqualityComparer<Entity>
{
    internal readonly ObjectDescription ObjectDescription;
    internal readonly World.World Owner;

    internal bool BagDropped;
    internal RGB GlowColor;
    internal int AltTextureIndex = -1;
    internal bool Disposed;

    internal long Id;
    internal string Name;
    internal float HitRadius;
    internal float LastCombat;

    // TODO: this is a Player only feature
    protected Vector2[]? posHistory;
    protected ushort nextProjectileId;

    private const int Effectcount = 53;

    private static readonly ILogger<Entity> Logger = LogFactory.LoggingInstance<Entity>();

    private readonly Guid guid;
    private readonly float[]? effects;
    private Dictionary<IChildren, object?> stateStorage = [];
    private Dictionary<byte, object>? stats;
    private byte posIdx;
    private bool stateEntry;
    private State? stateEntryCommonRoot;
    private bool tickingEffects;
    private ConditionEffects conditionEffects;
    private int conditionEffects1;
    private int conditionEffects2;
    private int size;

    internal Entity(string objId, bool interactive, World.World owner)
    {
        guid = Guid.NewGuid();
        Owner = owner;
        BagDropped = false;
        if (!EmbeddedData.BetterObjects.TryGetValue(objId, out var desc))
        {
            ObjectDescription = new ObjectDescription();
            Logger.LogError("Cannot get description for entity {0}", objId);
        }
        else
        {
            ObjectDescription = desc;
        }

        Name = ObjectDescription.Name;
        BehaviorDb.ResolveBehavior(this);
        Size = ObjectDescription.Size.Length == 1
            ? ObjectDescription.Size[0]
            : Random.Shared.Next(ObjectDescription.Size[0], ObjectDescription.Size[^1] + 1);

        LastCombat = 4f;

        if (interactive)
        {
            effects = new float[Effectcount];
        }

        Owner.EnterWorld(this);
    }

    internal delegate void BehaviorSignalEventHandler(Entity sender, BehaviorSignal signal);

    internal event BehaviorSignalEventHandler? BehaviorSignalEvent;

    Entity IProjectileOwner.Self => this;

    public Projectile?[]? Projectiles { get; private set; }

    internal ConditionEffects ConditionEffects
    {
        get => conditionEffects;
        set
        {
            conditionEffects = value;
            conditionEffects1 = (int)value;
            conditionEffects2 = (int)((ulong)value >> 32);
        }
    }

    internal State? CurrentState { get; private set; }

    internal int Size
    {
        get => size;
        set
        {
            size = value;
            HitRadius = Projectile.GetHitBox(value);
            Owner.EntityChangedHitbox(this);
        }
    }

    public virtual void Dispose()
    {
        Disposed = true;
    }

    public override bool Equals(object? obj)
    {
        return Equals(obj as Entity);
    }

    public bool Equals(Entity? other)
    {
        return other is not null && other.guid == guid;
    }

    public bool Equals(Entity? x, Entity? y)
    {
        return ReferenceEquals(x, y) || x?.guid == y?.guid;
    }

    public override string ToString()
    {
        return $"[{Name}: {guid}]";
    }

    public override int GetHashCode()
    {
        return guid.GetHashCode();
    }

    public int GetHashCode([DisallowNull] Entity obj)
    {
        return obj.guid.GetHashCode();
    }

    internal static Entity Resolve(string objId, World.World owner)
    {
        var desc = EmbeddedData.BetterObjects[objId];
        switch (desc.Class)
        {
            case "Sign":
                return new Sign(objId, owner);
            case "Wall":
            case "HalfWall":
                return new Wall(objId, desc.MaxHitPoints, owner);
            case "GameObject":
                return new GameObject.GameObject(objId, desc.MaxHitPoints, desc.Static, false, owner);
            case "GuildRegister":
            case "GuildChronicle":
            case "GuildBoard":
                return new GameObject.GameObject(objId, 0, false, false, owner);
            case "Container":
                return new Container(desc, owner);
            case "Enemy":
                return new Enemy.Enemy(objId, owner);
            case "Portal":
            case "GuildHallPortal":
                return new Portal(objId, 0, owner);
            case "ArenaPortal":
            case "ItemNameForge":
            case "AccountLevel":
            case "ColorChanger":
            case "ItemEvolver":
                return new GameObject.GameObject(objId, 0, true, false, owner);
            case "NPC":
                return new GameObject.GameObject(objId, 0, false, false, owner);
            case "Cannon":
                return new Cannon(owner, desc);
            case "Projectile":
            case "Player":
            case "Pet":
            case "Summon":
                throw new ArgumentException($"{desc.Class} should not instantiated using Entity.Resolve");
            default:
                Logger.LogWarning($"Not supported type: {desc.Class} for {objId}");
                return new Entity(objId, true, owner);
        }
    }

    internal bool InCombat()
    {
        return LastCombat <= 4f;
    }

    internal void InvokeBehaviorEvent(BehaviorSignal signal)
    {
        BehaviorSignalEvent?.Invoke(this, signal);
    }

    internal void SwitchTo(State? state)
    {
        var origState = CurrentState;
        CurrentState = state;
        EnterDeepestSubState();
        stateEntryCommonRoot = State.CommonParent(origState, CurrentState);
        stateEntry = true;
    }

    internal void OnChatTextReceived(string text)
    {
        var state = CurrentState;
        if (state != null)
        {
            foreach (var t in state.Transitions.OfType<ChatTransition>())
            {
                t.OnChatReceived(text);
            }
        }
    }

    internal void TickState()
    {
        State? s;
        if (stateEntry)
        {
            // State entry
            s = CurrentState;
            while (s != null && s != stateEntryCommonRoot)
            {
                foreach (var j in s.Transitions.Concat(s.Behaviors))
                {
                    j.OnStateEntry(this);
                }

                s = s.Parent;
            }

            stateEntryCommonRoot = null;
            stateEntry = false;
        }

        var origState = CurrentState;
        var state = CurrentState;
        var transisted = false;
        while (state != null)
        {
            if (!transisted)
            {
                for (var i = 0; i < state.Transitions.Count; i++)
                {
                    if (state.Transitions[i].Tick(this))
                    {
                        transisted = true;
                        break;
                    }
                }
            }

            for (var i = 0; i < state.Behaviors.Count; i++)
            {
                state.Behaviors[i].Tick(this);
            }

            state = state.Parent;
        }

        if (!transisted)
        {
            return;
        }

        // State exit
        s = origState;
        while (s != null && s != stateEntryCommonRoot)
        {
            foreach (var j in s.Transitions.Concat(s.Behaviors))
            {
                j.OnStateExit(this);
            }

            s = s.Parent;
        }
    }

    internal virtual ObjectStats ExportStats()
    {
        stats ??= [];

        ExportStats(stats);

        var statsAsArray = new KeyValuePair<byte, object>[stats.Count];
        var i = 0;
        var enumerator = stats.GetEnumerator();

        while (enumerator.MoveNext())
        {
            statsAsArray[i] = enumerator.Current;
            i++;
        }

        return new(Id, new Vector2(X, Y), statsAsArray);
    }

    internal virtual ObjectDef ToDefinition()
    {
        return new(ObjectDescription.ObjectType, ExportStats());
    }

    internal virtual void Tick()
    {
        if (Disposed)
        {
            return;
        }

        if (CurrentState != null && !HasConditionEffect(ConditionEffectIndex.Stasis))
        {
            TickState();
        }

        if (posHistory != null)
        {
            posHistory[posIdx++] = new Vector2 { X = X, Y = Y };
        }

        if (effects != null)
        {
            ProcessConditionEffects();
        }

        LastCombat += Owner.CurrentTime.StaticMspt / 1000f;
    }

    internal Vector2? TryGetHistory(long timeAgo)
    {
        if (posHistory is null || Disposed)
        {
            return null;
        }

        var tickPast = timeAgo * Program.AppSettings.GameServer.Tps / 1000;
        return tickPast > 255 ? null : posHistory[(byte)(posIdx - 2)];
    }

    internal Projectile CreateProjectile(
        CustomProjectile desc,
        int dmg,
        long time,
        Vector2 pos,
        float angle,
        World.World world,
        Item? from = null,
        IReadOnlyList<string>? targetGroups = null)
    {
        Projectile newProjectile = new(desc, this, targetGroups, (ushort)dmg, new Angle(angle), world, nextProjectileId, from)
        {
            BeginTime = new DateTime(time),
            BeginPosition = pos,
            Position = pos,
        };

        nextProjectileId = (ushort)((nextProjectileId + 1) % ObjectDescription.MaxProjectiles);

        Projectiles ??= new Projectile?[ObjectDescription.MaxProjectiles];

        var oldProjectile = Projectiles[newProjectile.Id];
        if (oldProjectile is not null)
        {
            Logger.LogWarning("{0} is overriding a projectile. Consider increasing the max projectile limit on the entity.", this);
            oldProjectile.Destroy();
        }

        Projectiles[newProjectile.Id] = newProjectile;
        return newProjectile;
    }

    internal virtual bool HitByProjectile(Projectile projectile)
    {
        return ObjectDescription.Enemy || ObjectDescription.Class == "Player";
    }

    internal bool HasConditionEffect(ConditionEffects eff)
    {
        return (ConditionEffects & eff) != 0;
    }

    internal bool HasConditionEffect(ConditionEffectIndex eff)
    {
        return (ConditionEffects & (ConditionEffects)(1UL << (int)eff)) != 0;
    }

    internal void RemoveConditionEffect(ConditionEffectIndex effect)
    {
        ApplyConditionEffect(effect, 0);
    }

    internal void ApplyConditionEffect(ConditionEffectIndex effect, int durationMs = -1)
    {
        if (effects is null)
        {
            Logger.LogWarning("Failed to apply condition effect to Entity {0}", this);
            return;
        }

        if (!CanApplyCondition(effect))
        {
            return;
        }

        var eff = (int)effect;
        effects[eff] = durationMs;
        if (durationMs != 0)
        {
            ConditionEffects |= (ConditionEffects)(1UL << eff);
        }

        if (durationMs >= 0)
        {
            tickingEffects = true;
        }
    }

    internal void ApplyConditionEffect(IEnumerable<ConditionEffect> effs)
    {
        foreach (var eff in effs)
        {
            ApplyConditionEffect(eff.Effect, eff.DurationMs);
        }
    }

    internal void ApplyConditionEffect(ConditionEffect eff)
    {
        ApplyConditionEffect(eff.Effect, eff.DurationMs);
    }

    internal void ApplyConditionEffect(params ConditionEffect[] effs)
    {
        foreach (var eff in effs)
        {
            ApplyConditionEffect(eff.Effect, eff.DurationMs);
        }
    }

    internal virtual int GetDefense()
    {
        return ObjectDescription.Defense;
    }

    internal T? GetState<T>(IChildren caller)
    {
        stateStorage.TryGetValue(caller, out var state);

        if (state is null)
        {
            return default;
        }
        else
        {
            return (T)state;
        }
    }

    internal void SetState<T>(IChildren caller, T? state)
    {
        if (state is null || state.Equals(default))
        {
            stateStorage.Remove(caller);
        }
        else
        {
            stateStorage[caller] = state;
        }
    }

    protected virtual void ExportStats(IDictionary<byte, object> stats)
    {
        stats[StatsType.Name] = Name;
        stats[StatsType.Size] = Size;
        stats[StatsType.Glowing] = (int)GlowColor;
        stats[StatsType.Effects] = conditionEffects1;
        stats[StatsType.Effects2] = conditionEffects2;
        if (this is not Player.Player)
        {
            stats[StatsType.AltTextureIndex] = AltTextureIndex != -1 ? AltTextureIndex : 0;
        }
    }

    private void ProcessConditionEffects()
    {
        if (effects is null || !tickingEffects)
        {
            return;
        }

        ConditionEffects newEffects = 0;
        tickingEffects = false;
        for (var i = 0; i < effects.Length; i++)
        {
            if (effects[i] > 0)
            {
                effects[i] -= Owner.CurrentTime.StaticMspt;
                if (effects[i] > 0)
                {
                    newEffects |= (ConditionEffects)(1UL << i);
                }
                else
                {
                    effects[i] = 0;
                }

                tickingEffects = true;
            }
            else if (effects[i] != 0)
            {
                newEffects |= (ConditionEffects)(1UL << i);
            }
        }

        if (newEffects != ConditionEffects)
        {
            ConditionEffects = newEffects;
        }
    }

    private void EnterDeepestSubState()
    {
        // always the first deepest sub-state
        if (CurrentState is null)
        {
            return;
        }

        while (CurrentState.States.Count > 0)
        {
            CurrentState = CurrentState.States[0];
        }
    }

    private bool CanApplyCondition(ConditionEffectIndex effect)
    {
        switch (effect)
        {
            case ConditionEffectIndex.Stunned when HasConditionEffect(ConditionEffects.StunImmume):
            case ConditionEffectIndex.Stasis when HasConditionEffect(ConditionEffects.StasisImmune):
            case ConditionEffectIndex.Paralyzed when HasConditionEffect(ConditionEffects.ParalyzeImmune):
            case ConditionEffectIndex.ArmorBroken when HasConditionEffect(ConditionEffects.ArmorBreakImmune):
            case ConditionEffectIndex.Curse when HasConditionEffect(ConditionEffects.CurseImmune):
            case ConditionEffectIndex.Petrify when HasConditionEffect(ConditionEffects.PetrifyImmune):
                return false;
            default:
                return (effect != ConditionEffectIndex.Dazed ||
                        !HasConditionEffect(ConditionEffects.DazedImmune))
                       && (effect != ConditionEffectIndex.Slowed ||
                           !HasConditionEffect(ConditionEffects.SlowedImmune));
        }
    }
}