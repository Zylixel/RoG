﻿using System.Numerics;
using GameServer.Realm.World;
using RoGCore.Models;

namespace GameServer.Realm.Entities;

public partial class Entity
{
    private const float MoveThreshold = .4f;

    public float X { get; private set; }

    public float Y { get; private set; }

    public Vector2 Vector => Position;

    public Vector2 Position { get; set; }

    internal Vector2Int IntPosition { get; private set; }

    internal virtual void Move(Vector2 pos)
    {
        Move(pos.X, pos.Y);
    }

    internal virtual void Move(in Vector2Int pos)
    {
        Move(pos.X, pos.Y);
    }

    internal virtual void Move(float x, float y)
    {
        if (X == x && Y == y)
        {
            return;
        }

        var oldVector = Position;
        X = x;
        Y = y;
        Position = new Vector2(x, y);
        IntPosition = new Vector2Int((int)x, (int)y);
        Owner.UpdateEntityCollision(this, oldVector);

        // TODO: this is bad
        (this as Player.Player)?.Sight?.Update();
    }

    internal void ValidateAndMove(float x, float y)
    {
        ValidateAndMove(new Vector2(x, y));
    }

    internal bool ValidateAndMove(Vector2 pos)
    {
        if (Position == pos)
        {
            return false;
        }

        var newPos = ResolveNewLocation(pos);

        if (Position == newPos)
        {
            return false;
        }

        Move(newPos);

        return true;
    }

    internal float GetSpeed(float speed)
    {
        return (float)(speed *
                   (HasConditionEffect(ConditionEffects.Slowed)
                       ? 0.5f
                       : 1) * 5 * (Owner.CurrentTime.StaticMspt / 1000.0));
    }

    private Vector2 ResolveNewLocation(Vector2 location)
    {
        var newPosition = Position;
        if (HasConditionEffect(ConditionEffectIndex.Paralyzed) || HasConditionEffect(ConditionEffectIndex.Petrify))
        {
            return newPosition;
        }

        var d = location - newPosition;
        if (d.X is < MoveThreshold and > -MoveThreshold && d.Y is < MoveThreshold and > -MoveThreshold)
        {
            return CalcNewLocation(location);
        }

        var absD = Vector2.Abs(d);
        var ds = MoveThreshold / Math.Max(absD.X, absD.Y);
        var tds = 0f;
        var done = false;
        while (!done)
        {
            if (tds + ds >= 1)
            {
                ds = 1 - tds;
                done = true;
            }

            newPosition = CalcNewLocation(newPosition + (d * ds));
            tds += ds;
        }

        return newPosition;
    }

    private Vector2 CalcNewLocation(Vector2 location)
    {
        float fx = 0;
        float fy = 0;
        var isFarX = (X % .5f == 0 && location.X != X) || (int)(X * 2f) != (int)(location.X * 2f);
        var isFarY = (Y % .5f == 0 && location.Y != Y) || (int)(Y * 2f) != (int)(location.Y * 2f);
        if ((!isFarX && !isFarY) || Owner.RegionUnblocked(location, TileOccupency.NoWalk | TileOccupency.OccupyHalf))
        {
            return location;
        }

        if (isFarX)
        {
            fx = location.X > X ? (int)(location.X * 2) * 0.5f : (int)(X * 2) * 0.5f;
            if ((int)fx > (int)X)
            {
                fx -= 0.01f;
            }
        }

        if (isFarY)
        {
            fy = location.Y > Y ? (int)(location.Y * 2) * 0.5f : (int)(Y * 2) * 0.5f;
            if ((int)fy > (int)Y)
            {
                fy -= 0.01f;
            }
        }

        if (!isFarX)
        {
            return new Vector2(location.X, fy);
        }

        if (!isFarY)
        {
            return new Vector2(fx, location.Y);
        }

        var ax = location.X > X ? location.X - fx : fx - location.X;
        var ay = location.Y > Y ? location.Y - fy : fy - location.Y;
        if (ax > ay)
        {
            if (Owner.RegionUnblocked(location.X, fy, TileOccupency.NoWalk | TileOccupency.OccupyHalf))
            {
                return new Vector2(location.X, fy);
            }

            if (Owner.RegionUnblocked(fx, location.Y, TileOccupency.NoWalk | TileOccupency.OccupyHalf))
            {
                return new Vector2(fx, location.Y);
            }
        }
        else
        {
            if (Owner.RegionUnblocked(fx, location.Y, TileOccupency.NoWalk | TileOccupency.OccupyHalf))
            {
                return new Vector2(fx, location.Y);
            }

            if (Owner.RegionUnblocked(location.X, fy, TileOccupency.NoWalk | TileOccupency.OccupyHalf))
            {
                return new Vector2(location.X, fy);
            }
        }

        return new Vector2(fx, fy);
    }
}