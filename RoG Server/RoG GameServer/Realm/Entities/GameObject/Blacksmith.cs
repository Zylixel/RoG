﻿namespace GameServer.Realm.Entities.GameObject;

internal class Blacksmith(World.World owner, string objectName, int tier) : GameObject(objectName, 0, false, false, owner)
{
    internal int Tier => tier;
}
