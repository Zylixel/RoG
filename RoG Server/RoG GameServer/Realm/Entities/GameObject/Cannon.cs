﻿using RoGCore.Assets;

namespace GameServer.Realm.Entities.GameObject;

internal class Cannon(World.World owner, ObjectDescription objectDescription)
    : GameObject(
        objectDescription.Name,
        objectDescription.MaxHitPoints,
        objectDescription.Static,
        false,
        owner)
{
    internal int ShootOrder;
}
