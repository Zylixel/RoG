﻿using Microsoft.Extensions.Logging;
using RoGCore.Logging;
using RoGCore.Models;
using RoGCore.Utils;

namespace GameServer.Realm.Entities.GameObject;

internal class Portal : GameObject
{
    internal string PortalName = string.Empty;
    internal bool RealmPortal;
    internal bool Usable = true;
    internal World.World? WorldInstance;

    private static readonly ILogger<Portal> Logger = LogFactory.LoggingInstance<Portal>();

    internal Portal(string objId, int life, World.World owner)
        : base(objId, life, false, true, owner)
    {
        CheckDungeon();
    }

    internal override void Tick()
    {
        if (WorldInstance != null && RealmPortal)
        {
            Usable = WorldInstance.Players.Count < RealmManager.MAXREALMPLAYERS;
        }

        base.Tick();
    }

    internal override bool HitByProjectile(Projectile projectile)
    {
        return false;
    }

    internal override bool DoesFlash()
    {
        return true;
    }

    protected override void ExportStats(IDictionary<byte, object> stats)
    {
        stats[StatsType.PortalUsable] = Usable ? 1 : 0;
        base.ExportStats(stats);
        stats[StatsType.Name] = Name.IsNullOrWhiteSpace() ? ObjectDescription.DungeonName ?? string.Empty : Name;
    }

    private void CheckDungeon()
    {
        if (ObjectDescription.DungeonName is null)
        {
            return;
        }

        var worldName = "GameServer.Realm.World.Worlds." +
                            ObjectDescription.DungeonName.Replace(" ", string.Empty).Replace("'", string.Empty);
        var worldType = Type.GetType(worldName, false, true);
        if (worldType is null)
        {
            Logger.LogError($"Portal is trying to access undeclared dungeon instance \"{worldName}\"");
            return;
        }

        var w = (World.World?)Activator.CreateInstance(worldType);
        if (w is null)
        {
            Logger.LogError($"Portal is trying to access undeclared dungeon instance \"{worldName}\"");
        }
    }
}