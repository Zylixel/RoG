﻿namespace GameServer.Realm.Entities.GameObject;

internal class Sign : GameObject
{
    internal Sign(string objId, World.World owner)
        : base(objId, 0, true, false, owner)
    {
    }

    internal override bool HitByProjectile(Projectile projectile)
    {
        return false;
    }
}