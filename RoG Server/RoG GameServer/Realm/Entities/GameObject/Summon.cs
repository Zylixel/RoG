﻿using System.Numerics;
using RoGCore.Models;

namespace GameServer.Realm.Entities.GameObject;

internal class Summon : GameObject
{
    private readonly int removeOnSummonerDamage;

    internal Summon(string objId, int lifetime, Vector2 desiredPosition, Player.Player summoner, int? removeOnPlayerDamage)
        : base(objId, lifetime, false, true, summoner.Owner)
    {
        DesiredPosition = desiredPosition;
        Summoner = summoner;
        Summoner.Summons.Add(this);

        if (removeOnPlayerDamage is not null)
        {
            removeOnSummonerDamage = removeOnPlayerDamage.Value;
            Summoner.OnHit += SummonerHit;
        }
    }

    internal Player.Player Summoner { get; init; }

    internal Vector2 DesiredPosition { get; set; }

    internal bool Controllable { get; set; }

    public override void Dispose()
    {
        Summoner.Summons.Remove(this);
        Summoner.OnHit -= SummonerHit;
        base.Dispose();
    }

    internal override bool DoesFlash()
    {
        return true;
    }

    internal override bool HitByProjectile(Projectile projectile)
    {
        return false;
    }

    protected override void ExportStats(IDictionary<byte, object> stats)
    {
        base.ExportStats(stats);
        if (Summoner.Client.Character is not null)
        {
            stats[StatsType.Texture1] = Summoner.Client.Character.Tex1;
            stats[StatsType.Texture2] = Summoner.Client.Character.Tex2;
        }

        stats[StatsType.AltTextureIndex] = (int)Summoner.ObjectDescription.ObjectType;
    }

    private void SummonerHit(object? sender, int damage)
    {
        if (damage >= removeOnSummonerDamage)
        {
            Owner.LeaveWorld(this);
        }
    }
}