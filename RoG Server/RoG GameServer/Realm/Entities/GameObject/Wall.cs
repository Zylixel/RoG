﻿using RoGCore.Networking.Packets;

namespace GameServer.Realm.Entities.GameObject;

internal class Wall(string objId, int hp, World.World owner) : GameObject(objId, hp, true, false, owner)
{
    internal override bool HitByProjectile(Projectile projectile)
    {
        if (!Vulnerable || projectile.Owner is not Player.Player)
        {
            return true;
        }

        var dmg = (int)this.GetDefenseDamage(projectile.Damage, projectile.CustomDesc.ArmorPiercing);
        hp -= dmg;
        Owner.BroadcastPacket(new Damage(Id, 0, (ushort)dmg, !CheckHp(), projectile.Id, projectile.Owner.Self.Id, projectile.CustomDesc.ArmorPiercing));
        return true;
    }
}