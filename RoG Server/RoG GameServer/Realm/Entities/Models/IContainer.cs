﻿using RoGCore.Models;

namespace GameServer.Realm.Entities.Models;

internal interface IContainer
{
    string[]? SlotTypes { get; }

    Item?[] Inventory { get; }
}