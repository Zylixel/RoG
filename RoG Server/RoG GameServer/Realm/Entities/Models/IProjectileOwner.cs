﻿namespace GameServer.Realm.Entities.Models;

internal interface IProjectileOwner
{
    Projectile?[]? Projectiles { get; }

    Entity Self { get; }
}