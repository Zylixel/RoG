﻿using GameServer.Realm.World.Worlds;
using RoGCore.Models;
using RoGCore.Networking.Packets;

namespace GameServer.Realm.Entities.Character;

internal abstract class Character : Entity
{
    internal Character(string objId, World.World owner)
        : base(objId, true, owner)
    {
        Hp = ObjectDescription.MaxHitPoints;
    }

    internal int Hp { get; set; }

    internal bool Damage(Projectile projectile)
    {
        return projectile.CustomDesc.Effects != null && (projectile.CustomDesc.EffectProbability == 0
                                                      || Random.Shared.Next(1, 101) <=
                                                      projectile.CustomDesc.EffectProbability)
            ? Damage(
                projectile.Owner.Self,
                projectile.Damage,
                projectile.CustomDesc.ArmorPiercing,
                out _,
                projectile.CustomDesc.Effects)
            : Damage(
                projectile.Owner.Self,
                projectile.Damage,
                projectile.CustomDesc.ArmorPiercing,
                out _);
    }

    internal virtual bool Damage(
        Entity? from,
        int dmg,
        bool noDef,
        out int appliedDamage,
        IEnumerable<ConditionEffect>? effs = null)
    {
        appliedDamage = 0;
        if (Owner is Nexus || HasConditionEffect(ConditionEffectIndex.Invincible) ||
            HasConditionEffect(ConditionEffectIndex.Paused) || HasConditionEffect(ConditionEffectIndex.Stasis))
        {
            return false;
        }

        if (from != null)
        {
            from.LastCombat = 0f;
        }

        LastCombat = 0f;
        CalculateDamage(ref dmg, noDef);
        appliedDamage = dmg;
        Hp -= appliedDamage;
        if (effs != null)
        {
            ApplyConditionEffect(effs);
        }

        Damage packet = new(Id, 0, (ushort)dmg, Hp <= 0, 0, from?.Id ?? -1, noDef);
        Owner.BroadcastPacket(packet, Position);
        return true;
    }

    private void CalculateDamage(ref int dmg, bool noDef)
    {
        dmg = (int)this.GetDefenseDamage(dmg, noDef);
        if (dmg > Hp)
        {
            dmg = Hp;
        }
    }
}