﻿using System.Numerics;
using GameServer.Logic.Engine;
using RoGCore.Models;
using RoGCore.Networking.Packets;

namespace GameServer.Realm.Entities.Enemy;

internal class Enemy : Character.Character
{
    internal bool GivesItemXp = true;
    internal Player.Player? RealmOwner;

    private readonly bool stat;
    private float bleeding;

    internal Enemy(string objId, World.World owner)
        : base(objId, owner)
    {
        MaximumHp = ObjectDescription.MaxHitPoints;
        stat = MaximumHp == 0;
        DamageCounter = new DamageCounter(this);

        if (ObjectDescription.StasisImmune)
        {
            ApplyConditionEffect(new ConditionEffect
            {
                Effect = ConditionEffectIndex.StasisImmune,
                DurationMs = -1,
            });
        }

        SpawnPoint = new Vector2 { X = X, Y = Y };
        ScaleMaximumHp();
    }

    internal int MaximumHp { get; private set; }

    internal DamageCounter DamageCounter { get; private set; }

    // TODO: would be nice if this was { get; init; }
    internal Vector2 SpawnPoint { get; set; }

    public override void Dispose()
    {
        RealmOwner?.RealmEnemyList.Remove(this);
        base.Dispose();
    }

    internal void Death()
    {
        DamageCounter.Death();
        CurrentState?.OnDeath(new BehaviorEventArgs(this));
        Owner.LeaveWorld(this);
    }

    internal void SetDamageCounter(DamageCounter counter, Enemy enemy)
    {
        DamageCounter = counter;
        DamageCounter.UpdateEnemy(enemy);
    }

    internal override bool Damage(
        Entity? from,
        int dmg,
        bool noDef,
        out int appliedDamage,
        IEnumerable<ConditionEffect>? effs = null)
    {
        appliedDamage = 0;
        if (stat)
        {
            return false;
        }

        if (!base.Damage(from, dmg, noDef, out appliedDamage, effs))
        {
            return false;
        }

        if (from is Player.Player player)
        {
            DamageCounter.HitBy(player, null, dmg);
        }

        if (Hp <= 0)
        {
            Death();
        }

        return true;
    }

    internal override bool HitByProjectile(Projectile projectile)
    {
        if (!Damage(projectile))
        {
            return false;
        }

        Owner.BroadcastPacket(new CharacterHit(Id, projectile.Id, projectile.Owner.Self.Id), Position);
        return true;
    }

    internal override void Tick()
    {
        if (!stat && HasConditionEffect(ConditionEffectIndex.Bleeding))
        {
            if (bleeding > 1)
            {
                Hp -= (int)bleeding;
                bleeding -= (int)bleeding;
            }

            bleeding += 28 * (Owner.CurrentTime.StaticMspt * 0.001f);
        }

        // Every 1 second
        if (Owner.CurrentTime.TickCount % Program.AppSettings.GameServer.Tps == 0)
        {
            ScaleMaximumHp();

            if (Owner is World.Worlds.Realm && (RealmOwner?.Disposed != false || this.DistSqr(RealmOwner) > Program.AppSettings.GameServer.Enemy.DespawnDistanceSqr))
            {
                TryFindNewRealmOwner();
            }
        }

        base.Tick();
    }

    protected override void ExportStats(IDictionary<byte, object> stats)
    {
        stats[StatsType.Hp] = Hp;
        stats[StatsType.MaximumHp] = MaximumHp;

        base.ExportStats(stats);
    }

    private void ScaleMaximumHp()
    {
        if (MaximumHp == 0)
        {
            return;
        }

        var playersNearby = Math.Max(this.InRange<Player.Player>(Program.AppSettings.GameServer.Enemy.HpScalingDistance).Count(), 1);
        var newMaxHp = ObjectDescription.MaxHitPoints + (int)(ObjectDescription.MaxHitPoints * (playersNearby - 1) * Program.AppSettings.GameServer.Enemy.HpScaling);
        Hp = (int)(Hp * ((double)newMaxHp / MaximumHp));
        MaximumHp = newMaxHp;
    }

    private void TryFindNewRealmOwner()
    {
        RealmOwner?.RealmEnemyList.Remove(this);
        var nearestPlayer = this.GetNearest<Player.Player>(Program.AppSettings.GameServer.Enemy.DespawnDistance);
        if (nearestPlayer is not null)
        {
            RealmOwner = nearestPlayer;
            RealmOwner.RealmEnemyList.Add(this);
        }
        else
        {
            Owner.LeaveWorld(this);
        }
    }
}