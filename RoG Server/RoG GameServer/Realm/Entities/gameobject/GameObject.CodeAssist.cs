﻿using RoGCore.Assets;
using RoGCore.Models;
using RoGCore.Utils;

namespace GameServer.Realm.Entities.GameObject;

internal partial class GameObject
{
    protected bool CheckHp()
    {
        if (!Vulnerable || hp >= 0)
        {
            return true;
        }

        Vector2Int vector = new((int)(X - 0.5), (int)(Y - 0.5));
        var currentTile = Owner.GetTile(vector);
        if (currentTile?.ObjDesc is not null && currentTile.ObjDesc.Name == ObjectDescription.Name)
        {
            currentTile.ObjDesc = null;
        }

        Owner.LeaveWorld(this);
        return false;
    }

    private static bool IsInteractive(string objId)
    {
        return EmbeddedData.BetterObjects.TryGetValue(objId, out var desc)
                && (desc.Class == null
                            ? !desc.Static || desc.Enemy || desc.OccupyFull
                            : desc.Class != "Container" && !desc.Class.ContainsIgnoreCase("wall") &&
                            desc.Class != "Merchant" && desc.Class != "Portal"
                && (!desc.Static || desc.Enemy || desc.OccupyFull));
    }
}