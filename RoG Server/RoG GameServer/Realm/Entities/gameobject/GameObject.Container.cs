﻿using GameServer.Realm.Entities.Models;
using RoGCore.Assets;
using RoGCore.Models;

namespace GameServer.Realm.Entities.GameObject;

internal class Container : GameObject, IContainer
{
    internal int BagOwner = -1;

    public Container(string objId, int life, bool dying, World.World owner)
        : base(objId, life, false, dying, owner)
    {
        Inventory = new Item?[8];
        SlotTypes = new string[8];
        for (var i = 0; i < SlotTypes.Length; i++)
        {
            SlotTypes[i] = "All";
        }
    }

    public Container(ObjectDescription desc, World.World owner)
        : base(desc.Name, desc.LifeTime, false, false, owner)
    {
        SlotTypes = desc.SlotTypes?.ToArray();
        if (desc.Equipment != null)
        {
            var inv =
                desc.Equipment
                    .Select(_ => _ == 0 ? null : EmbeddedData.Items[_])
                    .ToArray();
            Array.Resize(ref inv, 8);
            Inventory = inv.CreateItems(false);
        }
        else
        {
            Inventory = new Item?[8];
        }
    }

    public string[]? SlotTypes { get; }

    public Item?[] Inventory { get; set; }

    internal void DespawnIfEmpty()
    {
        if (!Inventory.Any(x => x != null))
        {
            Owner.LeaveWorld(this);
        }
    }

    internal override bool HitByProjectile(Projectile projectile)
    {
        return false;
    }

    internal override bool DoesFlash()
    {
        return true;
    }

    protected override void ExportStats(IDictionary<byte, object> stats)
    {
        stats[StatsType.Inventory] = RawItem.Write(Item.UnCookArray(Inventory), false, false);
        base.ExportStats(stats);
    }
}