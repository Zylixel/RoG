﻿using RoGCore.Models;

namespace GameServer.Realm.Entities.GameObject;

internal partial class GameObject
{
    protected override void ExportStats(IDictionary<byte, object> stats)
    {
        stats[StatsType.Hp] = hp;
        base.ExportStats(stats);
    }
}