﻿using System.Numerics;
using RoGCore.Assets;
using RoGCore.Models;
using RoGCore.Networking.Packets;

namespace GameServer.Realm.Entities.GameObject;

internal partial class GameObject : Entity
{
    internal GameObject(string objId, int lifeTime, bool stat, bool dying, World.World owner)
        : base(objId, IsInteractive(objId), owner)
    {
        if (lifeTime != 0)
        {
            Vulnerable = true;
            this.lifeTime = lifeTime;
        }

        this.dying = dying;
        Static = stat;
        createTime = DateTime.Now;
    }

    internal virtual bool DoesFlash()
    {
        return false;
    }

    internal override bool HitByProjectile(Projectile projectile)
    {
        if (projectile.Owner is not Player.Player player || HasConditionEffect(ConditionEffectIndex.Invincible)
                                                  || HasConditionEffect(ConditionEffectIndex.Paused)
                                                  || HasConditionEffect(ConditionEffectIndex.Stasis)
                                                  || !Vulnerable
                                                  || (Static && !ObjectDescription.Enemy))
        {
            return false;
        }

        var dmg = (int)this.GetDefenseDamage(projectile.Damage, projectile.CustomDesc.ArmorPiercing);
        if (!HasConditionEffect(ConditionEffectIndex.Invulnerable))
        {
            hp -= dmg;
        }

        if (projectile.CustomDesc.Effects is not null)
        {
            foreach (var effect in projectile.CustomDesc.Effects.Where(effect =>
            (effect.Effect != ConditionEffectIndex.Stunned || !ObjectDescription.StunImmune) &&
            (effect.Effect != ConditionEffectIndex.Paralyzed || !ObjectDescription.ParalyzedImmune) &&
            (effect.Effect != ConditionEffectIndex.Dazed || !ObjectDescription.DazedImmune)))
            {
                ApplyConditionEffect(effect);
            }
        }

        hp -= dmg;
        Owner.BroadcastPacket(new Damage(Id, 0, (ushort)dmg, !CheckHp(), projectile.Id, projectile.Owner.Self.Id, projectile.CustomDesc.ArmorPiercing));

        return true;
    }

    internal override void Tick()
    {
        base.Tick();
        if (!Vulnerable)
        {
            return;
        }

        if (dying)
        {
            hp = (int)(createTime.AddMilliseconds(lifeTime) - DateTime.Now).TotalMilliseconds;
            if (hp <= lifeTime * (1 / 4f) && blinkingState < 2 && DoesFlash())
            {
                if (blinkingState == 0)
                {
                    blinkingState = 1;
                    Owner.BroadcastPacket(new ShowEffect(ShowEffect.Type.Flash, Id, new Vector2(2f, 0f)));
                }
                else if (hp <= lifeTime * (1 / 8f))
                {
                    blinkingState = 2;
                    Owner.BroadcastPacket(new ShowEffect(ShowEffect.Type.Flash, Id, new Vector2(10f, 0f)));
                }
            }
        }

        CheckHp();
    }
}