﻿namespace GameServer.Realm.Entities.GameObject;

internal partial class GameObject
{
    internal readonly bool Static;

    protected readonly bool Vulnerable;
    protected int hp;

    private readonly DateTime createTime;
    private readonly int lifeTime;
    private readonly bool dying;
    private int blinkingState;
}