﻿namespace GameServer.Realm.Entities.Player;

internal interface IPlayer
{
    bool VisibleToEnemy();
}