﻿using Database.Models;
using GameServer.Networking.Socket;
using RoGCore.Models;
using RoGCore.Networking.Packets;

namespace GameServer.Realm.Entities.Player;

internal partial class Player
{
    public bool SendInfo(string? text)
    {
        if (string.IsNullOrWhiteSpace(text))
        {
            return false;
        }

        Client.SendPacket(new Text(text, new RGB(0xFFFF00)));
        return true;
    }

    public bool SendError(string? text)
    {
        if (string.IsNullOrWhiteSpace(text))
        {
            return false;
        }

        Client.SendPacket(new Text(text, new RGB(0xFF0000)));
        return true;
    }

    public bool SendGuild(string text)
    {
        if (string.IsNullOrWhiteSpace(text))
        {
            return false;
        }

        foreach (var client in RealmManager.Clients.Values.Where(x =>
            x.Player != null && x.Player.GuildManager == GuildManager))
        {
            Client.SendPacket(new Text(text, new RGB(0x0a9900), "*Guild*"));
        }

        return true;
    }

    public void SendGuild(DbGuild to, string text)
    {
        foreach (var client in RealmManager.Clients.Values.Where(x =>
            x.Player?.GuildManager != null && x.Player.GuildManager.Guild.Name == to.Name))
        {
            Client.SendPacket(new Text(text, new RGB(0x0a9900), "*Guild*"));
        }
    }
}