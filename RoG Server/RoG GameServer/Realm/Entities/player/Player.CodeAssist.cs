﻿using System.Numerics;
using RoGCore.Models;

namespace GameServer.Realm.Entities.Player;

internal partial class Player
{
    public bool VisibleToEnemy()
    {
        return !(HasConditionEffect(ConditionEffectIndex.Paused) ||
            HasConditionEffect(ConditionEffectIndex.Invisible) || newbieTime > 0);
    }

    internal bool IsInvincible()
    {
        return HasConditionEffect(ConditionEffectIndex.Paused) ||
          HasConditionEffect(ConditionEffectIndex.Stasis) ||
          HasConditionEffect(ConditionEffectIndex.Invincible) ||
          HasConditionEffect(ConditionEffectIndex.Invulnerable);
    }

    internal void CalcBoost()
    {
        if (ActivateBoost is null)
        {
            ActivateBoost = new ActivateBoost[8];
            for (var i = 0; i < 8; i++)
            {
                ActivateBoost[i] = new ActivateBoost();
            }
        }

        for (var i = 0; i < Boosts.Length; i++)
        {
            Boosts[i] = 0;
        }

        for (var i = 0; i < 4; i++)
        {
            var item = Inventory[i];
            if (item is null)
            {
                continue;
            }

            foreach (var pair in item.Value.GetStatBoosts())
            {
                Boosts[(int)pair.Key] += pair.Value;
            }
        }

        // apply activate boosts
        for (var i = 0; i < 8; i++)
        {
            Boosts[i] += ActivateBoost[i].GetBoost();
        }
    }

    internal void CalcSkillTreeBoost()
    {
        SkillTreeStats = new int[9];
        foreach (var skill in SkillTree.CurrentSkills)
        {
            if (skill.StatsBoost is null)
            {
                continue;
            }

            foreach (var statBoost in skill.StatsBoost)
            {
                SkillTreeStats[statBoost.Value] += statBoost.Value;
            }
        }
    }

    internal void SaveToCharacter()
    {
        var chr = Client.Character;
        if (chr is null)
        {
            return;
        }

        chr.Items = Item.UnCookArray(Inventory);
        chr.Stats = Stats;
        chr.Skin = PlayerSkin;
        chr.LastSeen = DateTime.Now;
        chr.SkillTree = SkillTree;
    }

    internal void SaveToAccount()
    {
        var acc = Client.Account;
        if (acc is null)
        {
            return;
        }

        acc.Skills = Skills;
    }

    internal int GetMaximumHp()
    {
        return Stats[(int)PlayerStats.Hp] + Boosts[(int)PlayerStats.Hp] +
          SkillTreeStats[(int)PlayerStats.Hp];
    }

    internal int GetMaximumMp()
    {
        return Stats[(int)PlayerStats.Mp] + Boosts[(int)PlayerStats.Mp] +
          SkillTreeStats[(int)PlayerStats.Mp];
    }

    internal void SetNewbiePeriod()
    {
        newbieTime = 3000;
    }

    private static float Dist(Entity a, Vector2 b)
    {
        var dx = a.X - b.X;
        var dy = a.Y - b.Y;
        return (float)Math.Sqrt((dx * dx) + (dy * dy));
    }

    private void HandleRegen()
    {
        if (Hp == GetMaximumHp() || !CanHpRegen())
        {
            hpRegenCounter = 0;
        }
        else
        {
            hpRegenCounter += this.GetHpRegen() * Owner.CurrentTime.StaticMspt / 1000f;
            var regen = (int)hpRegenCounter;
            if (regen > 0)
            {
                Hp = Math.Min(GetMaximumHp(), Hp + regen);
                hpRegenCounter -= regen;
            }
        }

        if (Client?.Character != null)
        {
            if (mp == GetMaximumMp() || !CanMpRegen())
            {
                mpRegenCounter = 0;
            }
            else
            {
                mpRegenCounter += this.GetMpRegen() * Owner.CurrentTime.StaticMspt / 1000f;
                var regen = (int)mpRegenCounter;
                if (regen <= 0)
                {
                    return;
                }

                mp = Math.Min(GetMaximumMp(), mp + regen);
                mpRegenCounter -= regen;
            }
        }
    }

    private bool CanHpRegen()
    {
        return !(HasConditionEffect(ConditionEffectIndex.Sick) ||
            HasConditionEffect(ConditionEffectIndex.Bleeding));
    }

    private bool CanMpRegen()
    {
        return !HasConditionEffect(ConditionEffectIndex.Quiet);
    }

    private void SetTpDisabledPeriod()
    {
        canTpCooldownTime = 10 * 1000;
    }

    private bool TpCooledDown()
    {
        return canTpCooldownTime <= 0;
    }

    private float UseWisMod(float value, int offset = 1)
    {
        double totalWisdom = Stats[(int)PlayerStats.Wisdom] + (2 * Boosts[(int)PlayerStats.Wisdom]);
        if (totalWisdom < 30)
        {
            return value;
        }

        double m = value < 0 ? -1 : 1;
        var n = (value * totalWisdom / 150) + (value * m);
        n = Math.Floor(n * Math.Pow(10, offset)) / Math.Pow(10, offset);
        return n - ((int)n * m) >= 1 / Math.Pow(10, offset) * m ? (int)(n * 10) / 10.0f : (int)n;
    }
}