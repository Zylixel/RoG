﻿using RoGCore.Models;
using RoGCore.Networking.Packets;

namespace GameServer.Realm.Entities.Player;

internal partial class Player
{
    internal event EventHandler<int>? OnHit;

    internal override bool Damage(
        Entity? from,
        int dmg,
        bool noDef,
        out int appliedDamage,
        IEnumerable<ConditionEffect>? effs = null)
    {
        if (!base.Damage(from, dmg, noDef, out appliedDamage, effs))
        {
            return false;
        }

        OnHit?.Invoke(this, appliedDamage);
        IncrementArmorXp();
        if (Hp <= 0)
        {
            Hp = 0;
            Death(from != null ? from.Name : "An Unknown Force");
        }

        return true;
    }

    internal override bool HitByProjectile(Projectile projectile)
    {
        if (projectile.Owner is Player)
        {
            return false;
        }

        if (!Damage(projectile))
        {
            return false;
        }

        Owner.BroadcastPacket(new CharacterHit(Id, projectile.Id, projectile.Owner.Self.Id));

        return base.HitByProjectile(projectile);
    }

    private void IncrementArmorXp()
    {
        var armor = Inventory[1];
        if (armor is null)
        {
            return;
        }

        armor.Value.Data.IncreaseXp();
    }
}