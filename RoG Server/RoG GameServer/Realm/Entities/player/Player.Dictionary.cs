﻿namespace GameServer.Realm.Entities.Player;

internal partial class Player
{
    private static readonly Dictionary<string, byte> QuestPriorities = new()
    {
        { "Reedoshark the Pirate King", 0 },
        { "The Beast", 0 },
        { "Akurra the Snake God", 0 },
        { "Arimanius The Rat King", 0 },
    };
}