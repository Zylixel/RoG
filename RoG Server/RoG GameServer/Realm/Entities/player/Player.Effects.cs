﻿using RoGCore.Models;

namespace GameServer.Realm.Entities.Player;

internal partial class Player
{
    private static readonly ConditionEffect[] NegativeEffs =
    [
        new() { Effect = ConditionEffectIndex.Slowed, DurationMs = 0 },
        new() { Effect = ConditionEffectIndex.Paralyzed, DurationMs = 0 },
        new() { Effect = ConditionEffectIndex.Weak, DurationMs = 0 },
        new() { Effect = ConditionEffectIndex.Stunned, DurationMs = 0 },
        new() { Effect = ConditionEffectIndex.Confused, DurationMs = 0 },
        new() { Effect = ConditionEffectIndex.Blind, DurationMs = 0 },
        new() { Effect = ConditionEffectIndex.Quiet, DurationMs = 0 },
        new() { Effect = ConditionEffectIndex.ArmorBroken, DurationMs = 0 },
        new() { Effect = ConditionEffectIndex.Bleeding, DurationMs = 0 },
        new() { Effect = ConditionEffectIndex.Dazed, DurationMs = 0 },
        new() { Effect = ConditionEffectIndex.Sick, DurationMs = 0 },
        new() { Effect = ConditionEffectIndex.Drunk, DurationMs = 0 },
        new() { Effect = ConditionEffectIndex.Hallucinating, DurationMs = 0 },
        new() { Effect = ConditionEffectIndex.Hexed, DurationMs = 0 },
        new() { Effect = ConditionEffectIndex.Unstable, DurationMs = 0 },
    ];

    private void HandleEffects()
    {
        if (Client.Character is null)
        {
            return;
        }

        var staticMspt = Owner.CurrentTime.StaticMspt;

        if (HasConditionEffect(ConditionEffectIndex.Healing))
        {
            if (healing > 1)
            {
                Hp = Math.Min(Stats[(int)PlayerStats.Hp] + Boosts[(int)PlayerStats.Hp], Hp + (int)healing);
                healing -= (int)healing;
            }

            healing += 6 * (staticMspt * 0.001f);
        }

        if (HasConditionEffect(ConditionEffectIndex.Quiet) && mp > 0)
        {
            mp = 0;
        }

        switch (Hp)
        {
            case > 1 when HasConditionEffect(ConditionEffectIndex.Bleeding) &&
                          !HasConditionEffect(ConditionEffectIndex.Recovering):
                if (bleeding > 1)
                {
                    Hp -= (int)bleeding;
                    bleeding -= (int)bleeding;
                }

                bleeding += 6 * (staticMspt * 0.001f);
                break;

            case > 1 when HasConditionEffect(ConditionEffectIndex.BloodLust):
                if (bleeding > 1)
                {
                    Hp -= (int)bleeding;
                    bleeding -= (int)bleeding;
                }

                bleeding += 3 * (staticMspt * 0.001f);
                break;
        }

        if (newbieTime > 0)
        {
            newbieTime -= staticMspt;
            if (newbieTime < 0)
            {
                newbieTime = 0;
            }
        }

        if (canTpCooldownTime > 0)
        {
            canTpCooldownTime -= staticMspt;
            if (canTpCooldownTime < 0)
            {
                canTpCooldownTime = 0;
            }
        }
    }
}