﻿using RoGCore.Models;

namespace GameServer.Realm.Entities.Player;

internal partial class Player
{
    protected override void ExportStats(IDictionary<byte, object> stats)
    {
        base.ExportStats(stats);
        if (Client?.Character is null)
        {
            return;
        }

        stats[StatsType.SkillLevels] = Skills.Write();
        stats[StatsType.AccountId] = Client.Account.AccountId;
        stats[StatsType.Silver] = Client.Account.Silver;
        stats[StatsType.CurrentSilver] = Client.Account.Silver;

        if (GuildManager != null)
        {
            stats[StatsType.Guild] = GuildManager.Guild.Name;
            stats[StatsType.GuildRank] = Client.Account.GuildRank;
        }
        else
        {
            stats[StatsType.Guild] = string.Empty;
            stats[StatsType.GuildRank] = 0;
        }

        stats[StatsType.Texture1] = Client.Character.Tex1;
        stats[StatsType.Texture2] = Client.Character.Tex2;
        stats[StatsType.Hp] = Hp;
        stats[StatsType.Mp] = mp;
        stats[StatsType.Inventory] = RawItem.Write(Item.UnCookArray(Inventory), false, false);
        stats[StatsType.MaximumHp] = GetMaximumHp();
        stats[StatsType.MaximumMp] = GetMaximumMp();
        stats[StatsType.Attack] = Stats[(int)PlayerStats.Attack] + Boosts[(int)PlayerStats.Attack] +
                                  SkillTreeStats[(int)PlayerStats.Attack];
        stats[StatsType.Defense] = Stats[(int)PlayerStats.Defense] + Boosts[(int)PlayerStats.Defense] +
                                   SkillTreeStats[(int)PlayerStats.Defense];
        stats[StatsType.Speed] = Stats[(int)PlayerStats.Speed] + Boosts[(int)PlayerStats.Speed] +
                                 SkillTreeStats[(int)PlayerStats.Speed];
        stats[StatsType.Vitality] = Stats[(int)PlayerStats.Vitality] + Boosts[(int)PlayerStats.Vitality] +
                                    SkillTreeStats[(int)PlayerStats.Vitality];
        stats[StatsType.Wisdom] = Stats[(int)PlayerStats.Wisdom] + Boosts[(int)PlayerStats.Wisdom] +
                                  SkillTreeStats[(int)PlayerStats.Wisdom];
        stats[StatsType.Dexterity] = Stats[(int)PlayerStats.Dexterity] + Boosts[(int)PlayerStats.Dexterity] +
                                     SkillTreeStats[(int)PlayerStats.Dexterity];
        stats[StatsType.HpBoost] = Boosts[(int)PlayerStats.Hp];
        stats[StatsType.MpBoost] = Boosts[(int)PlayerStats.Mp];
        stats[StatsType.AttackBonus] = Boosts[(int)PlayerStats.Attack];
        stats[StatsType.DefenseBonus] = Boosts[(int)PlayerStats.Defense];
        stats[StatsType.SpeedBonus] = Boosts[(int)PlayerStats.Speed];
        stats[StatsType.VitalityBonus] = Boosts[(int)PlayerStats.Vitality];
        stats[StatsType.WisdomBonus] = Boosts[(int)PlayerStats.Wisdom];
        stats[StatsType.DexterityBonus] = Boosts[(int)PlayerStats.Dexterity];
        stats[StatsType.Size] = Size;
        stats[StatsType.Skin] = PlayerSkin;
    }
}