﻿using RoGCore.Networking.Packets;

namespace GameServer.Realm.Entities.Player;

internal partial class Player
{
    internal void HandleGround()
    {
        if (Owner.TryGetTile(IntPosition, out var tile) && tile!.TileDesc?.Damage > 0 && tile.ObjDesc?.ProtectFromGroundDamage == false)
        {
            var dmg = this.GetDefenseDamage(tile.TileDesc.Damage, true);
            Hp -= dmg;
            Owner.BroadcastPacket(new Damage(Id, 0, dmg, Hp <= 0, 0, Id, true));
            if (Hp <= 0)
            {
                Death(tile.TileDesc.Name);
            }
        }
    }
}