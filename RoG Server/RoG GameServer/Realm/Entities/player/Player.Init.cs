﻿using System.Numerics;
using System.Runtime.CompilerServices;
using Database;
using GameServer.Networking.Socket;
using GameServer.Realm.Entities.Models;
using RoGCore.Assets;
using RoGCore.Models;
using RoGCore.Networking.Packets;
using RoGCore.Utils;

namespace GameServer.Realm.Entities.Player;

internal partial class Player : Character.Character, IContainer, IPlayer
{
    internal Player(Client client, World.World owner)
        : base(client.Character?.Class ?? throw new ArgumentException("Character was null"), owner)
    {
        var character = client.Character;
        AccountRank = AccountType.FromInt(client.Account.Rank);
        Client = client;
        Name = client.Account.Name;
        GuildManager = GuildManager.Add(this, RedisDatabase.GetGuild(client.Account.GuildId));
        ConditionEffects = 0;
        PlayerSkin = EmbeddedData.Skins.Contains(character.Skin) ? character.Skin : 0;
        GlowColor = client.Account.Color;
        Boosts = new int[8];
        SkillTreeStats = new int[8];
        SkillTree = character.SkillTree;
        Skills = client.Account.Skills;
        ClientEntities = [];
        clientStatic = [];
        tiles = [];
        PlayerStatData =
        [
            ObjectDescription.PlayerHitPoint ?? [], ObjectDescription.PlayerMagicPoint ?? [],
            ObjectDescription.PlayerAttack ?? [], ObjectDescription.PlayerDefense ?? [],
            ObjectDescription.PlayerSpeed ?? [], ObjectDescription.PlayerVitality ?? [],
            ObjectDescription.PlayerWisdom ?? [], ObjectDescription.PlayerDexterity ?? [],
            ObjectDescription.PlayerEvasion ?? [],
        ];
        var inv = Array.ConvertAll(character.Items, rawDataItem => rawDataItem?.Cook());
        Array.Resize(ref inv, 4 + 28);
        Inventory = inv;
        var slotTypes = ObjectDescription.SlotTypes?.ToArray() ?? [];
        Array.Resize(ref slotTypes, 4 + 28);
        var stats = (int[])character.Stats.Clone();
        Array.Resize(ref stats, 9);
        Stats = stats;
        CalcSkillTreeBoost();
        CalcBoost();
        Sight = new Sight(this);
        for (var i = 0; i < slotTypes.Length; i++)
        {
            if (slotTypes[i].IsNullOrWhiteSpace())
            {
                slotTypes[i] = "All";
            }
        }

        SlotTypes = slotTypes;
        posHistory = new Vector2[256];

        Hp = GetMaximumHp();
        mp = GetMaximumMp();

        Move(owner.GetPlayerSpawn(Name));
        SetNewbiePeriod();
        SendUpdate(owner);
        Client.SendPacket(new SetSkillTree(SkillTree.Data));
        Client.SendPacket(new AccountList(Client.Account.Locked, Client.Account.Ignored));
    }

    public void Death(string killer)
    {
        if (Client.Character is null)
        {
            return;
        }

        switch (killer)
        {
            case "":
            case "Unknown":
                break;
            default:
                Owner.BroadcastPacket(new Text($"{Name} died to {killer}.", new RGB(0x123456)));
                break;
        }

        Client.Reconnect(
            new Reconnect("Nexus", World.World.NEXUSID, 0),
            true);
    }

    internal string? Teleport(Teleport packet)
    {
        var obj = Owner.GetEntity(packet.ObjectId);
        if (obj is null)
        {
            return "You are trying to teleport to something that isn't there anymore...";
        }

        if (!TpCooledDown())
        {
            return "You must wait for the teleport cooldown to finish";
        }

        if (obj.HasConditionEffect(ConditionEffectIndex.Invisible))
        {
            return "You cannot teleport to invisible players";
        }

        if (obj.HasConditionEffect(ConditionEffectIndex.Paused))
        {
            return "You cannot teleport to paused players";
        }

        if (obj.Id == Id)
        {
            return "You cannot teleport to yourself!";
        }

        if (!Owner.AllowTeleport)
        {
            return $"You cannot teleport while in {Owner.Name}";
        }

        SetTpDisabledPeriod();
        Vector2 lastPos = new(X, Y);
        Move(obj.X, obj.Y);
        SetNewbiePeriod();
        Client.SendPacket(new Move(Position));
        Owner.BroadcastPacket(new ShowEffect(ShowEffect.Type.Teleport, Id, lastPos));
        return null;
    }

    internal void EnemyKilled(Enemy.Enemy enemy, uint exp)
    {
        var silverIncrease = (int)Math.Ceiling(exp / 100f);
        Client.Account.Silver += silverIncrease;

        if (exp == 0 || Client.Character == null)
        {
            return;
        }

        IncreaseSkill(PlayerSkills.Skill.Combat, exp);
    }

    internal Projectile? PlayerShootProjectile(
        ushort id,
        CustomProjectile desc,
        long time,
        Vector2 position,
        float angle,
        Item item)
    {
        nextProjectileId = id;
        return CreateProjectile(
            desc,
            this.GetAttackDamage(desc.GetDamage(item, SkillTree.CurrentSkills)),
            time,
            position,
            angle,
            Owner,
            item);
    }

    internal override int GetDefense()
    {
        return StatsManager.GetStats(this, (int)PlayerStats.Defense);
    }

    [MethodImpl(MethodImplOptions.AggressiveOptimization)]
    internal override void Tick()
    {
        if (Client.Disposed)
        {
            Owner.LeaveWorld(this);
            return;
        }

        if (HasConditionEffect(ConditionEffectIndex.BloodLust))
        {
            RemoveConditionEffect(ConditionEffectIndex.Recovering);
        }
        else
        {
            if (HasConditionEffect(ConditionEffectIndex.Recovering))
            {
                if (InCombat())
                {
                    RemoveConditionEffect(ConditionEffectIndex.Recovering);
                }
            }
            else if (!InCombat())
            {
                ApplyConditionEffect(ConditionEffectIndex.Recovering);

                Owner.BroadcastPacket(
                     new ShowEffect(ShowEffect.Type.Flash, Id, new ARGB(0xFF00FF00), new Vector2(10, 500)),
                     Position);
            }
        }

        if (!HasConditionEffect(ConditionEffects.Paused))
        {
            HandleRegen();
        }

        HandleEffects();

        SendUpdate(Owner);
        SendNewTick(Owner);

        if (Owner.Dungeon && !IsInvincible())
        {
            HandleGround();
        }

        base.Tick();
    }
}