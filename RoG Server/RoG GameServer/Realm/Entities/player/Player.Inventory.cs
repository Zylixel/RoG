﻿using GameServer.Realm.Entities.GameObject;
using RoGCore.Models;

namespace GameServer.Realm.Entities.Player;

internal partial class Player
{
    public void DropBag(Item? i)
    {
        if (i is null)
        {
            return;
        }

        Container container = new("Brown Bag", 1000 * 60, true, Owner);
        if (i.Value.Data.Soulbound && Client != null)
        {
            container.BagOwner = Client.Account.AccountId;
        }

        container.Inventory[0] = i;

        container.Move(Position
            + new System.Numerics.Vector2(Random.Shared.NextSingle(), Random.Shared.NextSingle())
            - new System.Numerics.Vector2(-0.5f, -0.5f));

        container.Size = 75;
    }
}