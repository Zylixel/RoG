﻿using GameServer.Networking.Socket;
using RoGCore.Models;
using RoGCore.Networking.Packets;

namespace GameServer.Realm.Entities.Player;

internal partial class Player
{
    internal void IncreaseSkill(PlayerSkills.Skill skill, uint xpAmount)
    {
        Skills.IncreaseSkill(skill, xpAmount);

        var skillName = Enum.GetName(skill);

        Client.SendPacket(new Notification(Id, $"{skillName} +{xpAmount}", new ARGB(0x8900FF)));
    }
}