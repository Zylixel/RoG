﻿using System.Buffers;
using GameServer.Realm.Entities.GameObject;
using GameServer.Realm.Terrain.Tile;
using RoGCore.Models;

namespace GameServer.Realm.Entities.Player;

internal partial class Player
{
    internal void SendUpdate(World.World owner)
    {
        var viewRect = Sight.GetRect();

        IEnumerable<WmapTile> currentTiles;
        Rectangle despawnRectangle;
        int maxTileCount;

        if (viewRect is not null)
        {
            currentTiles = owner.GetTiles(viewRect.Value);
            despawnRectangle = viewRect.Value;
            maxTileCount = (int)(viewRect.Value.Width * viewRect.Value.Height);
        }
        else
        {
            var sight = Sight.GetSight();
            currentTiles = owner.GetTiles(sight);
            despawnRectangle = new(
                IntPosition.X - (Program.AppSettings.GameServer.Sight.RectangleSize * 0.5f),
                IntPosition.Y - (Program.AppSettings.GameServer.Sight.RectangleSize * 0.5f),
                Program.AppSettings.GameServer.Sight.RectangleSize,
                Program.AppSettings.GameServer.Sight.RectangleSize);

            maxTileCount = sight.Count;
        }

        RemoveOutsideRect(despawnRectangle);

        var tileList = CurrentTilePass(
            currentTiles,
            maxTileCount,
            out var newStatics,
            out var removedStatics);

        var newEntities = GetNewEntities(owner, despawnRectangle);

        var dropEntities = GetRemovedEntities(ref despawnRectangle);

        if (newEntities.Count == 0 && tileList.Length == 0 && dropEntities.Length == 0 && newStatics.Count == 0 &&
            removedStatics.Length == 0)
        {
            return;
        }

        var dropEntitiesArray = ArrayPool<long>.Shared.Rent(dropEntities.Length);
        for (var i = 0; i < dropEntities.Length; i++)
        {
            var entity = dropEntities.Data[i];
            ClientEntities.Remove(entity);
            dropEntitiesArray[i] = entity.Id;
        }

        var wrappedDroppedEntities = new SizedArray<long>(dropEntitiesArray, dropEntities.Length);
        ArrayPool<Entity>.Shared.Return(dropEntities.Data);

        Client.SendPacket(new RoGCore.Networking.Packets.Update(tileList, newEntities, wrappedDroppedEntities, newStatics, removedStatics, viewRect ?? despawnRectangle));
    }

    private SizedArray<TileData> CurrentTilePass(in IEnumerable<WmapTile> currentTiles, int maxTileCount, out List<ObjectDef> newStatics, out SizedArray<Vector2Int> removedStatics)
    {
        var tileList = ArrayPool<TileData>.Shared.Rent(maxTileCount);
        var tileListIndex = 0;

        var removedStaticsArr = ArrayPool<Vector2Int>.Shared.Rent(clientStatic.Count);
        var removedStaticsIndex = 0;

        newStatics = [];

        foreach (var tile in currentTiles)
        {
            var newTile = !tiles.TryGetValue(tile.Position, out var tileCount);
            if (!newTile && tileCount >= tile.UpdateCount)
            {
                continue;
            }

            if (GetNewStatic(tile, out var def))
            {
                newStatics.Add(def);
            }
            else if (!newTile && tile.ObjDesc is null && clientStatic.Remove(tile.Position))
            {
                removedStaticsArr[removedStaticsIndex] = tile.Position;
                removedStaticsIndex++;
            }

            tiles[tile.Position] = tile.UpdateCount;
            if (tile.TileDesc is null)
            {
                continue;
            }

            tileList[tileListIndex] = new TileData(tile.TileDesc.ObjectType, tile.Position);
            tileListIndex++;
        }

        removedStatics = new SizedArray<Vector2Int>(removedStaticsArr, removedStaticsIndex);
        return new SizedArray<TileData>(tileList, tileListIndex);
    }

    private void RemoveOutsideRect(Rectangle rect)
    {
        // Remove static entities outside the rect
        clientStatic.RemoveWhere(x => !rect.Contains(x.X, x.Y));

        // Remove tiles outside of the rect
        var removeTiles = ArrayPool<System.Numerics.Vector2>.Shared.Rent(tiles.Count);
        var removeTilesSize = 0;

        var enumerator = tiles.GetEnumerator();

        while (enumerator.MoveNext())
        {
            var tile = enumerator.Current;

            if (!rect.Contains(tile.Key))
            {
                removeTiles[removeTilesSize] = tile.Key;
                removeTilesSize++;
            }
        }

        for (var i = 0; i < removeTilesSize; i++)
        {
            tiles.Remove(removeTiles[i]);
        }

        ArrayPool<System.Numerics.Vector2>.Shared.Return(removeTiles);
    }

    private List<ObjectDef> GetNewEntities(World.World owner, in Rectangle viewRect)
    {
        List<ObjectDef> ret = [];
        for (var i = 0; i < owner.Players.Count; i++)
        {
            var player = owner.Players[i];
            if (ClientEntities.Add(player))
            {
                ret.Add(player.ToDefinition());
            }
        }

        for (var i = 0; i < owner.Enemies.Count; i++)
        {
            var enemy = owner.Enemies[i];
            if (viewRect.Contains(enemy.Position) && tiles.ContainsKey(enemy.IntPosition) && ClientEntities.Add(enemy))
            {
                ret.Add(enemy.ToDefinition());
            }
        }

        for (var i = 0; i < owner.Objects.Count; i++)
        {
            var @object = owner.Objects[i];
            if (!viewRect.Contains(@object.Position))
            {
                continue;
            }

            if (@object is Container con && con.BagOwner != -1 && con.BagOwner != Client.Account.AccountId)
            {
                continue;
            }

            if (!tiles.ContainsKey(@object.IntPosition) || !ClientEntities.Add(@object))
            {
                continue;
            }

            ret.Add(@object.ToDefinition());
        }

        return ret;
    }

    private SizedArray<Entity> GetRemovedEntities(ref Rectangle viewRect)
    {
        var entities = ArrayPool<Entity>.Shared.Rent(ClientEntities.Count);
        var retCount = 0;
        var enumerator = ClientEntities.GetEnumerator();

        while (enumerator.MoveNext())
        {
            var entity = enumerator.Current;

            if (entity.Disposed)
            {
                entities[retCount] = entity;
                retCount++;
                continue;
            }

            if (entity is Player)
            {
                continue;
            }

            if ((!viewRect.Contains(entity.Position) && !(entity is GameObject.GameObject { Static: true })) || !tiles.ContainsKey(entity.IntPosition))
            {
                entities[retCount] = entity;
                retCount++;
            }
        }

        return new SizedArray<Entity>(entities, retCount);
    }

    private bool GetNewStatic(in WmapTile tile, out ObjectDef def)
    {
        def = default;
        if (tile.ObjDesc is null || !clientStatic.Add(tile.Position))
        {
            return false;
        }

        def = tile.ToDef(tile.Position);
        return true;
    }

    private void SendNewTick(World.World owner)
    {
        var sendEntities = ArrayPool<ObjectStats>.Shared.Rent(ClientEntities.Count);
        ushort count = 0;

        var clientEntityEnumerator = ClientEntities.GetEnumerator();

        while (clientEntityEnumerator.MoveNext())
        {
            var entity = clientEntityEnumerator.Current;
            sendEntities[count] = entity.ExportStats();
            count++;
        }

        var time = owner.CurrentTime;

        Client.SendPacket(new RoGCore.Networking.Packets.NewTick(
            tickId++,
            DateTime.UtcNow.ToBinary(),
            (float)(Program.AppSettings.GameServer.Tps * time.StaticMspt / Math.Max(time.StaticMspt, time.LastMspt)),
            (float)time.LastMspt,
            (ushort)owner.Players.Count,
            new SizedArray<ObjectStats>(sendEntities, count),
            owner.LightLevel));
    }
}