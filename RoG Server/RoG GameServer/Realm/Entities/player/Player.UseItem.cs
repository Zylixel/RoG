﻿using System.Numerics;
using Database.Models;
using GameServer.Realm.Entities.GameObject;
using GameServer.Realm.Entities.Models;
using GameServer.Realm.Terrain.Tile;
using GameServer.Realm.World;
using RoGCore.Assets;
using RoGCore.Models;
using RoGCore.Networking;
using RoGCore.Networking.Packets;

namespace GameServer.Realm.Entities.Player;

internal partial class Player
{
    private static readonly Skill Clone3Skill = SkillTree.SkillLookup[SkillType.Clone3];

    internal bool Activate(RealmTime time, in Item item, in UseItem pkt)
    {
        var endMethod = false;
        var target = pkt.ItemUsePos;
        if (Client?.Character != null)
        {
            mp -= item.GetMpCost(SkillTree.CurrentSkills);
        }

        if (Owner.GetEntity(pkt.SlotObject.OwnerId) is not IContainer con)
        {
            return true;
        }

        if (item.BaseItem.ActivateEffects is null)
        {
            return true;
        }

        foreach (var eff in item.BaseItem.ActivateEffects)
        {
            if (eff.RequiredSkills?.Any(requiredSkill => SkillTree.CurrentSkills.Select(x => x.Type).Contains(requiredSkill)) == false)
            {
                continue;
            }

            if (endMethod)
            {
                return true;
            }

            switch (eff.Effect)
            {
                case ActivateEffects.BulletNova:
                    BulletNova(time, item, target);
                    break;
                case ActivateEffects.Shoot:
                    Shoot(time, item, target);
                    break;
                case ActivateEffects.StatBoostSelf:
                    StatBoostSelf(eff);
                    break;
                case ActivateEffects.StatBoostAura:
                    StatBoostAura(eff);
                    break;
                case ActivateEffects.ConditionEffectSelf:
                    ConditionEffectSelf(eff);
                    break;
                case ActivateEffects.ConditionEffectAura:
                    ConditionEffectAura(eff);
                    break;
                case ActivateEffects.Heal:
                    Heal(eff);
                    break;
                case ActivateEffects.HealNova:
                    HealNova(eff);
                    break;
                case ActivateEffects.Magic:
                    Magic(eff);
                    break;
                case ActivateEffects.MagicNova:
                    MagicNova(eff);
                    break;
                case ActivateEffects.Teleport:
                    Teleport(target);
                    break;
                case ActivateEffects.Lightning:
                    Lightning(eff, target);
                    break;
                case ActivateEffects.RemoveNegativeConditions:
                    RemoveNegativeConditions(eff);
                    break;
                case ActivateEffects.RemoveNegativeConditionsSelf:
                    RemoveNegativeConditionsSelf();
                    break;
                case ActivateEffects.IncrementStat:
                    IncrementStat(eff);
                    break;
                case ActivateEffects.BulletCreate:
                    Waki(time, item, target, eff);
                    break;
                case ActivateEffects.UnlockVault:
                    endMethod = Vault(eff);
                    break;
                case ActivateEffects.Scepter:
                    Scepter(target, eff);
                    break;
                case ActivateEffects.Portal:
                    endMethod = OpenPortal(eff);
                    break;
                case ActivateEffects.Summon:
                    endMethod = Summon(eff, pkt);
                    break;
            }
        }

        return endMethod;
    }

    private void ActivateHealHp(Player player, int amount, List<IPacket> pkts)
    {
        var newHp = Math.Min(GetMaximumHp(), player.Hp + amount);
        if (newHp != player.Hp)
        {
            pkts.Add(new ShowEffect(ShowEffect.Type.Heal, player.Id, ARGB.White));
            pkts.Add(new Notification(
                player.Id,
                $"{{\"key\":\"blank\",\"tokens\":{{\"data\":\"+{newHp - player.Hp}\"}}}}",
                new ARGB(0xff00ff00)));
            player.Hp = newHp;
        }
    }

    private void ActivateHealMp(Player player, int amount, List<IPacket> pkts)
    {
        if (player.Client.Character is null)
        {
            return;
        }

        var newMp = Math.Min(GetMaximumMp(), player.mp + amount);
        if (newMp == player.mp)
        {
            return;
        }

        pkts.Add(new ShowEffect(ShowEffect.Type.Heal, player.Id, new ARGB(0x6084e0)));
        pkts.Add(new Notification(
            player.Id,
            $"{{\"key\":\"blank\",\"tokens\":{{\"data\":\"+{newMp - player.mp}\"}}}}",
            new ARGB(0x6084e0)));
        player.mp = newMp;
    }

    private void Shoot(RealmTime time, in Item item, Vector2 target)
    {
        if (item.BaseItem.Projectiles is null || item.BaseItem.Projectiles.Length == 0)
        {
            return;
        }

        var arcGap = item.BaseItem.ArcGap * Math.PI / 180;
        var startAngle = Math.Atan2(target.Y - Y, target.X - X) - ((item.BaseItem.Projectiles.Length - 1) / 2f * arcGap);
        var prjDesc = item.BaseItem.Projectiles[0]; // Assume only one
        var batch = new IPacket[item.BaseItem.Projectiles.Length];
        var globalTime = DateTime.UtcNow.Ticks;

        for (var i = 0; i < batch.Length; i++)
        {
            var projectile = CreateProjectile(
                prjDesc,
                this.GetAttackDamage(prjDesc.GetDamage(item, SkillTree.CurrentSkills)),
                time.TotalElapsedMs,
                Position,
                (float)(startAngle + (arcGap * i)),
                Owner);

            batch[i] = new AllyShoot(Id, 0, globalTime, projectile);
        }

        Owner.BroadcastPackets(batch, this);
    }

    private void Waki(RealmTime time, in Item item, Vector2 target, ActivateEffect eff)
    {
        if (item.BaseItem.Projectiles is null || item.BaseItem.Projectiles.Length == 0)
        {
            return;
        }

        var prjDesc = item.BaseItem.Projectiles[0];
        var startPoint = target;
        var startAngle = Math.Atan2(startPoint.Y - Y, startPoint.X - X);
        var distance = Dist(this, startPoint);
        if (distance > eff.MaximumDistance)
        {
            var diff = distance - eff.MaximumDistance;
            startPoint.X -= (float)(diff * Math.Cos(startAngle));
            startPoint.Y -= (float)(diff * Math.Sin(startAngle));
        }

        var range = prjDesc.GetSpeed(item.BaseItem, SkillTree.CurrentSkills) * 0.01f * prjDesc.GetLifetime(item.BaseItem, SkillTree.CurrentSkills) * 0.01f;
        var startGap = (eff.NumShots - 1) * 0.5f * eff.GapTiles;
        var gapAngle = eff.GapAngle * Math.PI / 180;
        var startX = startPoint.X + (startGap * Math.Cos(startAngle - gapAngle));
        var startY = startPoint.Y + (startGap * Math.Sin(startAngle - gapAngle));
        var batch = new IPacket[eff.NumShots];
        var globalTime = DateTime.UtcNow.Ticks;

        for (var i = 0; i < eff.NumShots; i++)
        {
            var startXi = startX - (eff.GapTiles * i * Math.Cos(startAngle - gapAngle));
            var startYi = startY - (eff.GapTiles * i * Math.Sin(startAngle - gapAngle));
            var startAnglei = startAngle + (eff.OffsetAngle * Math.PI / 180);
            startXi -= range * 0.5f * Math.Cos(startAnglei);
            startYi -= range * 0.5f * Math.Sin(startAnglei);
            var projectile = CreateProjectile(
                prjDesc,
                this.GetAttackDamage(prjDesc.GetDamage(item, SkillTree.CurrentSkills)),
                time.TotalElapsedMs,
                new Vector2((float)startXi, (float)startYi),
                (float)startAnglei,
                Owner);

            batch[i] = new AllyShoot(Id, 0, globalTime, projectile);
        }

        Owner.BroadcastPackets(batch, Position);
    }

    private void Scepter(Vector2 target, ActivateEffect eff)
    {
        // Get angle from mouse pos to player
        var theta = MathF.Atan2(target.Y - Y, target.X - X);
        Owner.BroadcastPacket(new ShowEffect(ShowEffect.Type.Scepter, Id, new Vector2(theta, 0)));

        // Get nearest enemy to mouse point
        var enemy = Owner.GetNearest<Enemy.Enemy>(
            target,
            3,
            x => x.ObjectDescription.MaxHitPoints < eff.MaximumHitPoints);

        // Ensure enemy actually exists
        if (enemy is null)
        {
            // Test for enemy with any max hp
            enemy = Owner.GetNearest<Enemy.Enemy>(target, 3);

            // Still nothing?
            if (enemy is null)
            {
                return;
            }

            // Damage Enemy
            enemy.Damage(this, eff.Damage, true, out _);
            return;
        }

        // Damage Enemy
        enemy.Damage(this, eff.Damage, true, out _);

        // Get angle from enemy to player
        theta = MathF.Atan2(enemy.Y - Y, enemy.X - X);

        // get distance to move enemy
        var dist = Math.Min(Math.Sqrt(this.DistSqr(enemy)), eff.Strength);

        // Move enemy towards player instantly
        // Momentum could be added in the future for a better effect, but this is okay for now
        enemy.ValidateAndMove(X + (float)(dist * Math.Cos(theta)), Y + (float)(dist * Math.Sin(theta)));
    }

    private void BulletNova(RealmTime time, in Item item, Vector2 target)
    {
        if (item.BaseItem.Projectiles is null || item.BaseItem.Projectiles.Length == 0)
        {
            return;
        }

        var prjDesc = item.BaseItem.Projectiles[0];
        var batch = new IPacket[20];
        var globalTime = DateTime.UtcNow.Ticks;

        for (var i = 0; i < 20; i++)
        {
            var projectile = CreateProjectile(
                prjDesc,
                this.GetAttackDamage(prjDesc.GetDamage(item, SkillTree.CurrentSkills)),
                time.TotalElapsedMs,
                target,
                (float)(i * (Math.PI * 2) / 20),
                Owner);

            batch[i] = new AllyShoot(Id, 0, globalTime, projectile);
        }

        Owner.BroadcastPackets(batch, Position);
    }

    private void StatBoostSelf(ActivateEffect eff)
    {
        int idx;
        PlayerStats boost;
        if (eff.Stats == StatsType.MaximumHp)
        {
            boost = PlayerStats.Hp;
            idx = 0;
        }
        else if (eff.Stats == StatsType.MaximumMp)
        {
            boost = PlayerStats.Mp;
            idx = 1;
        }
        else if (eff.Stats == StatsType.Attack)
        {
            boost = PlayerStats.Attack;
            idx = 2;
        }
        else if (eff.Stats == StatsType.Defense)
        {
            boost = PlayerStats.Defense;
            idx = 3;
        }
        else if (eff.Stats == StatsType.Speed)
        {
            boost = PlayerStats.Speed;
            idx = 4;
        }
        else if (eff.Stats == StatsType.Dexterity)
        {
            boost = PlayerStats.Dexterity;
            idx = 5;
        }
        else if (eff.Stats == StatsType.Vitality)
        {
            boost = PlayerStats.Vitality;
            idx = 6;
        }
        else
        {
            boost = PlayerStats.Wisdom;
            idx = 7;
        }

        Boosts[(int)boost] += eff.Amount;
        ApplyConditionEffect(new ConditionEffect
        {
            DurationMs = eff.GetDurationMs(SkillTree.CurrentSkills),
            Effect = (ConditionEffectIndex)(idx + 39),
        });
        Owner.Timers.Add(new WorldTimer(eff.GetDurationMs(SkillTree.CurrentSkills), (_, _) =>
        {
            Boosts[(int)boost] -= eff.Amount;
        }));
        Owner.BroadcastPacket(new ShowEffect(ShowEffect.Type.Heal, Id));
    }

    private void StatBoostAura(ActivateEffect eff)
    {
        int idx;
        PlayerStats boost;
        if (eff.Stats == StatsType.MaximumHp)
        {
            boost = PlayerStats.Hp;
            idx = 0;
        }
        else if (eff.Stats == StatsType.MaximumMp)
        {
            boost = PlayerStats.Mp;
            idx = 1;
        }
        else if (eff.Stats == StatsType.Attack)
        {
            boost = PlayerStats.Attack;
            idx = 2;
        }
        else if (eff.Stats == StatsType.Defense)
        {
            boost = PlayerStats.Defense;
            idx = 3;
        }
        else if (eff.Stats == StatsType.Speed)
        {
            boost = PlayerStats.Speed;
            idx = 4;
        }
        else if (eff.Stats == StatsType.Dexterity)
        {
            boost = PlayerStats.Dexterity;
            idx = 5;
        }
        else if (eff.Stats == StatsType.Vitality)
        {
            boost = PlayerStats.Vitality;
            idx = 6;
        }
        else
        {
            boost = PlayerStats.Wisdom;
            idx = 7;
        }

        var bit = idx + 39;
        var amountSba = eff.Amount;
        var durationSba = eff.GetDurationMs(SkillTree.CurrentSkills);
        var rangeSba = eff.Range;
        var noStack = eff.NoStack;
        if (eff.UseWisMod)
        {
            amountSba = (ushort)UseWisMod(eff.Amount, 0);
            durationSba = (int)(UseWisMod(eff.GetDurationMs(SkillTree.CurrentSkills) / 1000f) * 1000);
            rangeSba = UseWisMod(eff.Range);
        }

        this.Aoe<Player>(rangeSba, player =>
        {
            if (ActivateBoost is null)
            {
                return;
            }

            // TODO support for noStack StatBoostAura attribute (paladin total hp increase / insta heal)
            if (!noStack)
            {
                ApplyConditionEffect(new ConditionEffect
                {
                    DurationMs = durationSba,
                    Effect = (ConditionEffectIndex)bit,
                });
                ActivateBoost[(int)boost].Push(amountSba);
                Hp += amountSba;
                CalcBoost();
                Owner.Timers.Add(new WorldTimer(durationSba, (_, _) =>
                {
                    if (ActivateBoost is null)
                    {
                        return;
                    }

                    ActivateBoost[(int)boost].Pop(amountSba);
                    CalcBoost();
                }));
            }
            else
            {
                if (player.HasConditionEffect(ConditionEffectIndex.HpBoost))
                {
                    return;
                }

                ActivateBoost[(int)boost].Push(amountSba);
                Hp += amountSba;
                CalcBoost();
                ApplyConditionEffect(new ConditionEffect
                {
                    DurationMs = durationSba,
                    Effect = (ConditionEffectIndex)bit,
                });
                Owner.Timers.Add(new WorldTimer(durationSba, (_, _) =>
                {
                    if (ActivateBoost is null)
                    {
                        return;
                    }

                    ActivateBoost[(int)boost].Pop(amountSba);
                    CalcBoost();
                }));
            }
        });
        Owner.BroadcastPacket(new ShowEffect(ShowEffect.Type.Nova, Id, new Vector2(rangeSba, 0)), Position);
    }

    private void ConditionEffectSelf(ActivateEffect eff)
    {
        if (eff.ConditionEffect is null)
        {
            return;
        }

        var durationCes = eff.GetDurationMs(SkillTree.CurrentSkills);
        if (eff.UseWisMod)
        {
            durationCes = (int)(UseWisMod(eff.GetDurationMs(SkillTree.CurrentSkills) / 1000f) * 1000);
        }

        RevokableConditionalEffect conditionalEffect = new(this, eff);
        conditionalEffect.Invoke(durationCes);
    }

    private void ConditionEffectAura(ActivateEffect eff)
    {
        if (eff.ConditionEffect is null)
        {
            return;
        }

        var durationCea = eff.GetDurationMs(SkillTree.CurrentSkills);
        var rangeCea = eff.Range;
        if (eff.UseWisMod)
        {
            durationCea = (int)(UseWisMod(eff.GetDurationMs(SkillTree.CurrentSkills) / 1000f) * 1000);
            rangeCea = UseWisMod(eff.Range);
        }

        this.Aoe<Player>(rangeCea, player =>
        {
            if (eff.ConditionEffect is null)
            {
                return;
            }

            player.ApplyConditionEffect(new ConditionEffect
            {
                Effect = eff.ConditionEffect.Value,
                DurationMs = durationCea,
            });
        });
        ARGB color = eff.ConditionEffect.Value switch
        {
            ConditionEffectIndex.Damaging => new ARGB(0xffff0000),
            ConditionEffectIndex.Berserk => new ARGB(0x808080),
            _ => ARGB.White,
        };
        Owner.BroadcastPacket(new ShowEffect(ShowEffect.Type.Nova, Id, color, new Vector2(rangeCea, 0)), Position);
    }

    private void Heal(ActivateEffect eff)
    {
        List<IPacket> pkts = [];
        ActivateHealHp(this, eff.Amount, pkts);
        Owner.BroadcastPackets(pkts);
    }

    private void HealNova(ActivateEffect eff)
    {
        var amountHn = eff.Amount;
        var rangeHn = eff.Range;
        if (eff.UseWisMod)
        {
            amountHn = (ushort)UseWisMod(eff.Amount, 0);
            rangeHn = UseWisMod(eff.Range);
        }

        List<IPacket> packets = [];
        this.Aoe<Player>(rangeHn, player => ActivateHealHp(player, amountHn, packets));
        packets.Add(new ShowEffect(ShowEffect.Type.Nova, Id, new Vector2(rangeHn, 0)));

        Owner.BroadcastPackets(packets, Position);
    }

    private void Magic(ActivateEffect eff)
    {
        List<IPacket> packets = [];
        ActivateHealMp(this, eff.Amount, packets);
        Owner.BroadcastPackets(packets, Position);
    }

    private void MagicNova(ActivateEffect eff)
    {
        List<IPacket> packets = [];
        this.Aoe<Player>(eff.Range / 2, player => ActivateHealMp(player, eff.Amount, packets));
        packets.Add(new ShowEffect(ShowEffect.Type.Nova, Id, new Vector2(eff.Range, 0)));
        Owner.BroadcastPackets(packets, Position);
    }

    private void Teleport(Vector2 target)
    {
        if (Client.Account.Rank < (int)AccountType.Rank.Vip &&
            Owner.Map[(int)target.X, (int)target.Y]?.Region == TileRegion.SupporterRegion)
        {
            SendError("This area is for supporters only!");
            return;
        }

        Vector2 lastPos = new(X, Y);
        Move(target.X, target.Y);
        Owner.BroadcastPackets(
            [
                new Move(Position),
                new ShowEffect(ShowEffect.Type.Teleport, Id, lastPos),
            ],
            Position);
    }

    private void Lightning(ActivateEffect eff, Vector2 target)
    {
        Enemy.Enemy? start = null;
        var angle = Math.Atan2(target.Y - Y, target.X - X);
        var diff = Math.PI / 3;
        Owner.Aoe<Enemy.Enemy>(target, 6, enemy =>
        {
            var x = Math.Atan2(enemy.Y - Y, enemy.X - X);
            if (Math.Abs(angle - x) < diff)
            {
                start = enemy;
                diff = Math.Abs(angle - x);
            }
        });
        if (start is null)
        {
            return;
        }

        var current = start;
        var targets = new Enemy.Enemy[eff.MaxTargets];
        for (var i = 0; i < targets.Length; i++)
        {
            targets[i] = current;
            var next = current.GetNearest<Enemy.Enemy>(
                8,
                enemy => Array.IndexOf(targets, enemy) == -1 && this.DistSqr(enemy) <= 6 * 6);
            if (next is null)
            {
                break;
            }

            current = next;
        }

        List<IPacket> pkts = [];
        for (var i = 0; i < targets.Length; i++)
        {
            if (targets[i] is null)
            {
                break;
            }

            if (targets[i].HasConditionEffect(ConditionEffectIndex.Invincible))
            {
                continue;
            }

            Entity prev = i == 0 ? this : targets[i - 1];
            targets[i]?.Damage(this, eff.TotalDamage, false, out _);
            if (eff.ConditionEffect != null)
            {
                targets[i].ApplyConditionEffect(new ConditionEffect
                {
                    Effect = eff.ConditionEffect.Value,
                    DurationMs = (int)(eff.EffectDuration * 1000),
                });
            }

            pkts.Add(new ShowEffect(
                ShowEffect.Type.Lightning,
                prev.Id,
                new ARGB(0xffff0088),
                targets[i].Position,
                new Vector2(350, 0)));
        }

        Owner.BroadcastPackets(pkts, Position);
    }

    private void RemoveNegativeConditions(ActivateEffect eff)
    {
        this.Aoe<Player>(eff.Range / 2, player => player.ApplyConditionEffect(NegativeEffs));
        Owner.BroadcastPacket(new ShowEffect(ShowEffect.Type.Nova, Id, new Vector2(eff.Range / 2, 0)), Position);
    }

    private void RemoveNegativeConditionsSelf()
    {
        ApplyConditionEffect(NegativeEffs);
        Owner.BroadcastPacket(new ShowEffect(ShowEffect.Type.Nova, Id, new Vector2(1, 0)), Position);
    }

    private void IncrementStat(ActivateEffect eff)
    {
        var idx = -1;
        if (eff.Stats == StatsType.MaximumHp)
        {
            idx = 0;
        }
        else if (eff.Stats == StatsType.MaximumMp)
        {
            idx = 1;
        }
        else if (eff.Stats == StatsType.Attack)
        {
            idx = 2;
        }
        else if (eff.Stats == StatsType.Defense)
        {
            idx = 3;
        }
        else if (eff.Stats == StatsType.Speed)
        {
            idx = 4;
        }
        else if (eff.Stats == StatsType.Vitality)
        {
            idx = 5;
        }
        else if (eff.Stats == StatsType.Wisdom)
        {
            idx = 6;
        }
        else if (eff.Stats == StatsType.Dexterity)
        {
            idx = 7;
        }

        Stats[idx] += eff.Amount;
        var limit = (short)PlayerStatData[idx][1];
        if (Stats[idx] > limit)
        {
            Stats[idx] = limit;
        }
    }

    private bool Vault(ActivateEffect eff)
    {
        if (Client is null)
        {
            SendError($"Failed to unlock {eff.Amount} vault slots");
            return true;
        }

        Client.Account.VaultSlots += eff.Amount;
        Client.Account.Flush();
        SendInfo($"Unlocked {eff.Amount} vault slot{(eff.Amount == 1 ? string.Empty : "s")}!");
        return false;
    }

    private bool OpenPortal(ActivateEffect eff)
    {
        if (eff.Id is null || !EmbeddedData.BetterObjects.TryGetValue(eff.Id, out var desc))
        {
            SendError("Dungeon not implemented yet.");
            return true;
        }

        var timeoutTime = desc.LifeTime == 0 ? 30 * 1000 : desc.LifeTime;
        var dungName = desc.DungeonName;
        if (dungName is null || desc.Name is null)
        {
            SendError("Dungeon disabled or under maintenance until further notice");
            return true;
        }

        Portal portal = new(desc.Name, timeoutTime, Owner);
        portal.Move(X, Y);
        var packets = new IPacket[2]
        {
            new Notification(Id, dungName + " opened by " + Name, new ARGB(0x00FF00)),
            new Text(dungName + " opened by " + Name, new RGB(0xFFFF00)),
        };
        Owner.BroadcastPackets(packets);
        return false;
    }

    private bool Summon(ActivateEffect eff, in UseItem pkt)
    {
        if (Summons.Count == Summons.Capacity)
        {
            return true;
        }

        if (eff.Id is null)
        {
            SendError("Summon not implemented correctly.");
            return true;
        }

        Summon summon = new(eff.Id, eff.GetDurationMs(SkillTree.CurrentSkills), pkt.ItemUsePos, this, eff.RevokeOnDamage);

        if (SkillTree.CurrentSkills.Contains(Clone3Skill))
        {
            summon.Controllable = true;
        }

        summon.Move(X, Y);

        return false;
    }
}