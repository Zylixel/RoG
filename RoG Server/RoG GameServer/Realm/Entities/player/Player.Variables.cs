﻿using System.Numerics;
using Database.Models;
using GameServer.Networking.Socket;
using GameServer.Realm.Entities.GameObject;
using RoGCore.Models;

namespace GameServer.Realm.Entities.Player;

internal partial class Player
{
    internal List<Entity> RealmEnemyList = new(Program.AppSettings.GameServer.Enemy.MaxPerPlayer * 2);
    private readonly HashSet<Vector2> clientStatic;
    private readonly Dictionary<Vector2, int> tiles;
    private float hpRegenCounter;
    private float mpRegenCounter;
    private float canTpCooldownTime;
    private float bleeding;
    private float healing;
    private float newbieTime;
    private int tickId;
    private int mp;

    public Item?[] Inventory { get; set; }

    public string[]? SlotTypes { get; set; }

    internal AccountType.Rank AccountRank { get; set; }

    internal int[][] PlayerStatData { get; }

    internal int[] Stats { get; }

    internal int[] Boosts { get; }

    internal int[] SkillTreeStats { get; private set; }

    internal ActivateBoost[]? ActivateBoost { get; private set; }

    internal Client Client { get; }

    internal string? Invited { get; set; }

    internal GuildManager? GuildManager { get; set; }

    internal int PlayerSkin { get; set; }

    internal HashSet<Entity> ClientEntities { get; }

    internal Sight Sight { get; }

    internal SkillTree SkillTree { get; }

    internal PlayerSkills Skills { get; }

    internal List<Summon> Summons { get; } = new List<Summon>(3);
}