﻿using System.Numerics;
using RoGCore.Assets;
using RoGCore.Models;
using RoGCore.Networking.Packets;

namespace GameServer.Realm.Entities.Player;

internal class RevokableConditionalEffect
{
    private readonly Player player;
    private readonly ActivateEffect activateEffect;

    private Timer? destroyTimer;

    public RevokableConditionalEffect(Player player, ActivateEffect activateEffect)
    {
        if (activateEffect.ConditionEffect is null)
        {
            throw new ArgumentException("activateEffect must have a conditionEffect");
        }

        this.player = player;
        this.activateEffect = activateEffect;

        if (activateEffect.RevokeOnDamage is not null)
        {
            player.OnHit += PlayerHit;
        }
    }

    internal void Invoke(int durationMs)
    {
        player.ApplyConditionEffect(new ConditionEffect
        {
            Effect = activateEffect.ConditionEffect!.Value,
            DurationMs = durationMs,
        });

        var color = activateEffect.ConditionEffect.Value switch
        {
            ConditionEffectIndex.Damaging => new ARGB(0xffff0000),
            ConditionEffectIndex.Berserk => new ARGB(0x808080),
            _ => new ARGB(0xffffffff),
        };

        player.Owner.BroadcastPacket(new ShowEffect(ShowEffect.Type.Ring, player.Id, color, new Vector2(2f, 0)), player, true);

        destroyTimer = new Timer(Destroy, this, durationMs, int.MaxValue);
    }

    private void PlayerHit(object? sender, int damage)
    {
        if (damage < activateEffect.RevokeOnDamage!.Value)
        {
            return;
        }

        player.ApplyConditionEffect(new ConditionEffect
        {
            Effect = activateEffect.ConditionEffect!.Value,
            DurationMs = 0,
        });

        Destroy(sender);
    }

    private void Destroy(object? sender)
    {
        player.OnHit -= PlayerHit;
        destroyTimer?.Dispose();
    }
}