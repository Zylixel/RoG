﻿using System.Numerics;
using System.Runtime.CompilerServices;
using GameServer.Realm.Entities;
using GameServer.Realm.Entities.GameObject;
using GameServer.Realm.Entities.Player;
using RoGCore.Models;
using RoGCore.Utils;

namespace GameServer.Realm;

internal static class EntityUtils
{
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static double DistSqr(this Entity a, Entity b)
    {
        return Vector2.DistanceSquared(a.Position, b.Position);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float DistSqr(this Entity a, Vector2 b)
    {
        return Vector2.DistanceSquared(a.Position, b);
    }

    public static double DistSqr(this Entity a, float x, float y)
    {
        var dx = a.X - x;
        var dy = a.Y - y;
        return (dx * dx) + (dy * dy);
    }

    public static T? GetNearest<T>(this Entity entity, float dist)
        where T : Entity
    {
        return entity.Owner.GetNearest<T>(entity.Position, dist);
    }

    public static T? GetNearest<T>(this Entity entity, float dist, Predicate<Entity> predicate)
        where T : Entity
    {
        return entity.Owner.GetNearest<T>(entity.Position, dist, predicate);
    }

    public static Predicate<Entity> NamePredicate(string name)
    {
        return en => en.ObjectDescription.Name == name;
    }

    public static Predicate<Entity> GroupsPredicate(IReadOnlyCollection<string> groups)
    {
        return en => en.ObjectDescription.Group is not null && groups.Contains(en.ObjectDescription.Group);
    }

    public static Predicate<Entity> PlayerOrSummonPredicate()
    {
        return en => en is Player or Summon;
    }

    public static IEnumerable<T> InRange<T>(this Entity entity, float dist)
        where T : Entity
    {
        return entity.Owner.InRange<T>(entity.Position, dist);
    }

    public static IEnumerable<T> InRange<T>(this Entity entity, float dist, Predicate<Entity> predicate)
        where T : Entity
    {
        return entity.Owner.InRange<T>(entity.Position, dist, predicate);
    }

    public static T? GetRaytraced<T>(this Entity entity, IEnumerable<T> entities)
        where T : Entity
    {
        const float precision = 0.5f;

        foreach (var otherEntity in entities)
        {
            var angle = (float)Math.Atan2(otherEntity.Y - entity.Y, otherEntity.X - entity.X);
            var dist = (float)Math.Sqrt(entity.DistSqr(otherEntity));
            Vector2 point = new(MathF.Cos(angle), MathF.Sin(angle));
            var blocked = false;
            for (float i = 0; i <= dist; i += precision)
            {
                if (entity.Owner.TryGetTile((entity.Position + (point * i)).ToInt(), out var tile) &&
                    !tile!.Occupied(TileOccupency.OccupyHalf))
                {
                    continue;
                }

                blocked = true;
                break;
            }

            if (!blocked)
            {
                return otherEntity;
            }
        }

        return null;
    }

    public static void Aoe<T>(this Entity entity, float radius, Action<T> callback)
        where T : Entity
    {
        entity.Owner.Aoe(entity.Position, radius, callback);
    }

    internal static T PickWeightedEntry<T>(in IEnumerable<WeightedEntry<T>> foliage)
    {
        var weightedEntries = foliage as WeightedEntry<T>[] ?? foliage.ToArray();
        var randomNumber = Random.Shared.Next(0, weightedEntries.Sum(c => c.Weight));
        var selected = weightedEntries[0];
        foreach (var broker in weightedEntries)
        {
            if (randomNumber < broker.Weight)
            {
                selected = broker;
                break;
            }

            randomNumber -= broker.Weight;
        }

        return selected.Entry;
    }

    internal readonly struct WeightedEntry<T>(in T entry, in int weight)
    {
        internal readonly T Entry = entry;
        internal readonly int Weight = weight;
    }
}