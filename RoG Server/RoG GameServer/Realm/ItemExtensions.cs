﻿using GameServer.Realm.Entities.GameObject;
using GameServer.Realm.Entities.Models;
using Microsoft.Extensions.Logging;
using RoGCore.Assets;
using RoGCore.Logging;
using RoGCore.Models;

namespace GameServer.Realm;

internal static class ItemExtensions
{
    private static readonly ILogger Logger = LogFactory.LoggingInstance(nameof(ItemExtensions));

    public static bool AuditItem(this IContainer container, Item? item, int slot)
    {
        if (container is Container)
        {
            Logger.LogDebug("Success: Trying to insert item into GiftChest");
            return true;
        }

        if (item is null)
        {
            Logger.LogDebug("Success: Item is null");
            return true;
        }

        if (container.SlotTypes is null)
        {
            Logger.LogDebug("Success: Container has no slottypes");
            return true;
        }

        if (container.SlotTypes[slot] == "All")
        {
            Logger.LogDebug("Success: slottype is all");
            return true;
        }

        if (item.Value.BaseItem.Type == container.SlotTypes[slot])
        {
            Logger.LogDebug("Success: Slottypes are same");
            return true;
        }

        Logger.LogDebug($"Test Failed: Item is not null: {item.Value.BaseItem.Name}");
        Logger.LogDebug("Test Failed: Container has slottypes");
        Logger.LogDebug($"Test Failed: Slottype is not All: {container.SlotTypes[slot]}");
        Logger.LogDebug($"Test Failed: slottype doesnt match item: {item.Value.BaseItem.Type}");
        return false;
    }

    internal static Item? CreateItem(this JsonItem? item, bool soulbound)
    {
        return item is null ? null : new Item(item, soulbound);
    }

    internal static Item?[] CreateItems(this JsonItem?[] items, bool soulbound)
    {
        var ret = Array.ConvertAll(items, staticItem => CreateItem(staticItem, soulbound));
        Array.Resize(ref ret, 8);
        return ret;
    }
}