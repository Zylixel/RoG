﻿using System.Collections.Concurrent;
using System.Diagnostics;
using Microsoft.Extensions.Logging;
using RoGCore.Logging;
using RoGCore.Networking;

namespace GameServer.Realm;

internal static class LogicTicker
{
    private static readonly ILogger Logger = LogFactory.LoggingInstance(nameof(LogicTicker));
    private static readonly ConcurrentQueue<int> WorldTickWorkIndexQueue = new();
    private static readonly Stopwatch MsptWatch = new();
    private static readonly object TimeLock = new();
    private static RealmTime currentTime;
    private static World.World?[][] batches = [];
    private static bool batchesNeedUpdating = true;
    private static CountdownEvent worldTickCountdown = new(1);

    static LogicTicker()
    {
        currentTime.StaticMspt = StaticMspt;
    }

    internal static RealmTime CurrentTime
    {
        get
        {
            lock (TimeLock)
            {
                return currentTime;
            }
        }
    }

    private static float StaticMspt => 1000f / Program.AppSettings.GameServer.Tps;

    internal static void TickLoop()
    {
        Thread.CurrentThread.Name = "LogicLoop";
        Logger.LogDebug("Logic loop started.");
        while (!RealmManager.Terminating)
        {
            try
            {
                MsptWatch.Reset();
                MsptWatch.Start();
                Update();

                IncomingNetworkHandler.ProcessMessages();

                var worlds = RealmManager.Worlds;
                if (Program.AppSettings.GameServer.Multithreading)
                {
                    TickWorldsMultithreaded(worlds);
                }
                else
                {
                    TickWorlds(worlds);
                }

                currentTime.LastMspt = MsptWatch.Elapsed.TotalMilliseconds;
                currentTime.StaticMspt = StaticMspt;
                TimeSpan timeSpan = new(0, 0, 0, 0, (int)StaticMspt);
                var msptSpan = timeSpan - MsptWatch.Elapsed;
                if (msptSpan.TotalMilliseconds <= 0)
                {
                    // Server is falling behind!
                    Logger.LogWarning("Server is lagging. Tick took {0} ms to process!", MsptWatch.ElapsedMilliseconds);
                }
                else
                {
                    Thread.Sleep((int)msptSpan.TotalMilliseconds);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, null);
            }
        }

        Logger.LogInformation("Logic loop stopped.");
    }

    internal static void WorldListChanged()
    {
        batchesNeedUpdating = true;
    }

    private static void Update()
    {
        currentTime.TotalElapsedMs += (int)currentTime.StaticMspt;
        currentTime.TickCount++;
    }

    private static void CalculateBatches(Dictionary<int, World.World> worlds)
    {
        batches = worlds.Select(x => x.Value).Chunk((int)Math.Ceiling(worlds.Count / (float)Environment.ProcessorCount)).ToArray();

        Logger.LogDebug($"Worlds: {worlds.Count}");
        Logger.LogDebug($"Batches: {batches.Length}");

        batchesNeedUpdating = false;
        worldTickCountdown?.Dispose();
        worldTickCountdown = new CountdownEvent(batches.Length);
    }

    private static void TickWorlds(Dictionary<int, World.World> worlds)
    {
        var enumerator = worlds.GetEnumerator();

        while (enumerator.MoveNext())
        {
            enumerator.Current.Value.Tick(currentTime);
        }

        enumerator.Dispose();
    }

    /// <summary>
    /// Uses the thread-pool to tick worlds on separate threads.
    /// Awaits for all worlds to finish ticking before exiting method.
    /// </summary>
    private static void TickWorldsMultithreaded(Dictionary<int, World.World> worlds)
    {
        if (batchesNeedUpdating)
        {
            CalculateBatches(worlds);
        }

        worldTickCountdown.Reset();

        for (var i = 0; i < batches.Length; i++)
        {
            // Adding to a queue and dequeing prevents a display class
            // Being created to capture i
            WorldTickWorkIndexQueue.Enqueue(i);

            ThreadPool.QueueUserWorkItem(_ =>
            {
                var dequeued = WorldTickWorkIndexQueue.TryDequeue(out var batchIndex);

                if (!dequeued)
                {
                    Logger.LogWarning("Could not dequeue work for world ticking");
                    worldTickCountdown.Signal();
                    return;
                }

                var batch = batches[batchIndex];

                // Tick all worlds in the batch
                for (var j = 0; j < batch.Length; j++)
                {
                    batch[j]?.Tick(currentTime);
                }

                worldTickCountdown.Signal();
            });
        }

        // Wait for threads to finish
        worldTickCountdown.Wait();
    }
}