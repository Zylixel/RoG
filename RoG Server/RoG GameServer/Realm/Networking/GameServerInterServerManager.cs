﻿using Database.InterServer;
using Microsoft.Extensions.Logging;
using RoGCore.Logging;
using RoGCore.SettingsModels;
using static Database.InterServer.NetworkMsg;

namespace GameServer.Realm.Networking;

internal class GameServerInterServerManager : InterServerManager
{
    private static readonly ILogger<GameServerInterServerManager> Logger = LogFactory.LoggingInstance<GameServerInterServerManager>();

    internal GameServerInterServerManager()
        : base(Logger, NetworkType.WorldServer, Program.AppSettings.GameServer.Name)
    {
        Run();
    }

    protected override NetworkMsg GenerateNetworkMessage(in NetworkCode code)
    {
        return new NetworkMsg(code, type, name, GenerateServerItem());
    }

    private RoGCore.Models.GameServer GenerateServerItem()
    {
        return new RoGCore.Models.GameServer(
            Program.AppSettings.GameServer.Name,
            Program.AppSettings.GameServer.Dns,
            RealmManager.Clients.Count / NetworkingSettings.MAXCONNECTIONS,
            Program.AppSettings.GameServer.AdminOnly,
            Program.AppSettings.GameServer.Port);
    }
}