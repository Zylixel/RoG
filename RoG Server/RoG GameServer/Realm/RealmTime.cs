﻿namespace GameServer.Realm;

internal struct RealmTime
{
    internal long TickCount;
    internal long TotalElapsedMs;
    internal float StaticMspt;
    internal double LastMspt;
}