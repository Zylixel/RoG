﻿using RoGCore.Models;

namespace GameServer.Realm.Terrain.Biome;

internal static class Biome
{
    internal const float InterpolateDistance = 0.005f;

    internal static readonly float[] BiomeElevationValues =
    [

        // Ocean //Beach //Midland //HighLand // Mountains
        0.2f, 0.25f, 0.45f, 0.62f,

        // Ocean:       07.79625596%
        // Beach:       04.75566196%
        // Midland:     28.65657384%
        // HighLand:    29.41538212%
        // Mountain:    29.37612612%
    ];

    internal static Elevation GetBiomeElevation(in Vector2Int position, Noise noise, float scale, out double biomeNoise)
    {
        biomeNoise = GetBiomeNoise(noise, position, 0.0005f * scale);

        // Will only spawn biomes next to each other (ex. Water -> Beach; Forest -> Beach || Savannah)
        if (biomeNoise < BiomeElevationValues[0])
        {
            return Elevation.Ocean;
        }
        else if (biomeNoise < BiomeElevationValues[1])
        {
            return Elevation.Beach;
        }
        else if (biomeNoise < BiomeElevationValues[2])
        {
            return Elevation.Midland;
        }
        else
        {
            return biomeNoise < BiomeElevationValues[3] ? Elevation.HighLand : Elevation.Mountain;
        }
    }

    internal static Type GetBiome(this World.Worlds.Realm owner, in Vector2Int position)
    {
        return GetBiome(position, owner.Noise, World.Worlds.Realm.MAPSCALE);
    }

    internal static Type GetBiome(in Vector2Int position, Noise noise, float scale)
    {
        var elevation = GetBiomeElevation(position, noise, scale, out var biomeNoise);
        var humidity = HumidityExtensions.GetHumidity(position, noise, scale, out var subBiomeNoise);

        var biome = Type.Ocean;
        switch (elevation)
        {
            case Elevation.Ocean:
                // Only areas close to land may have the Light_Water variant.
                // Oceans shall remain dark
                if (biomeNoise > 0.06)
                {
                    biome = humidity switch
                    {
                        Humidity.Low => Type.Ocean,
                        Humidity.Medium => Type.Ocean,
                        Humidity.High => Type.TropicalWater,
                        _ => throw new NotImplementedException(),
                    };
                }

                break;
            case Elevation.Beach:
                biome = humidity switch
                {
                    Humidity.Low => Type.Beach,
                    Humidity.Medium => Type.Swamp,
                    Humidity.High => Type.LightBeach,
                    _ => throw new NotImplementedException(),
                };

                break;
            case Elevation.Midland:
                biome = humidity switch
                {
                    Humidity.Low => Type.Forest,
                    Humidity.Medium => Type.Swamp,
                    Humidity.High => Type.Jungle,
                    _ => throw new NotImplementedException(),
                };

                break;
            case Elevation.HighLand:
                biome = subBiomeNoise < 0 ? Type.Tundra : Type.Savannah;
                break;
            case Elevation.Mountain:
                var ore = noise.Octave(position.X, position.Y, 1, 0.5f, 0.025f * scale) > 0.6f;
                biome = ore ? Type.MountainOre : Type.Mountain;
                break;
            default:
                throw new ArgumentOutOfRangeException(Enum.GetName(elevation));
        }

        if (biome is Type.Ocean or Type.TropicalWater or Type.Mountain or Type.MountainOre)
        {
            return biome;
        }

        var river = GetBiomeNoise(noise, position + new Vector2Int(-894894, 381920), 0.002f * scale, 5);
        return (river is < 0.48f or > 0.52f)
            ? biome
            : biome switch
            {
                Type.Tundra => Type.IceRiver,
                Type.LightBeach => Type.TropicalRiver,
                Type.Jungle => Type.TropicalRiver,
                _ => Type.River,
            };
    }

    internal static float GetBiomeNoise(Noise noise, in Vector2Int position, float scale, int octaves = 7)
    {
        // result is [-1, 1]
        var result = noise.Octave(position.X, position.Y, octaves, 0.5f, scale);

        switch (Math.Abs((int)(result * 1000)) % 2)
        {
            case 0:
                result += 0.0025f;
                break;
            case 1:
                result -= 0.0025f;
                break;
        }

        // Normalize result to [0, 1]
        result++;
        result *= 0.5f;

        return result;
    }
}