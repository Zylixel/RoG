﻿namespace GameServer.Realm.Terrain.Biome;

internal static class BiomeTypeExtensions
{
    private static readonly Dictionary<Type, Type[]> Neighbors = [];

    static BiomeTypeExtensions()
    {
        PopulateNeigborDictionary();
    }

    internal static bool SpawnEnemies(this Type biome)
    {
        return biome switch
        {
            Type.None => false,
            Type.Ocean => false,
            Type.TropicalWater => false,
            Type.Beach => false,
            Type.LightBeach => true,
            Type.Jungle => true,
            Type.Forest => true,
            Type.Swamp => true,
            Type.Savannah => true,
            Type.Mountain => true,
            Type.River => false,
            Type.TropicalRiver => false,
            Type.Tundra => true,
            Type.IceRiver => false,
            _ => false,
        };
    }

    internal static string GetEnemy(this Type biome, Random rand)
    {
        return biome switch
        {
            Type.Beach => Terrain.GetBeachEnemy(rand),
            Type.LightBeach => Terrain.GetBeachEnemy(rand),
            Type.Jungle => Terrain.GetGreenEnemies(rand),
            Type.Forest => Terrain.GetRedEnemies(rand),
            Type.Swamp => Terrain.GetRedEnemies(rand),
            Type.Savannah => Terrain.GetSavannahEnemies(rand),
            Type.Tundra => Terrain.GetTundraEnemies(rand),
            _ => string.Empty,
        };
    }

    internal static Type[] GetNeigbors(this Type biome)
    {
        return Neighbors[biome];
    }

    private static void PopulateNeigborDictionary()
    {
        Neighbors.Clear();
        var biomes = Enum.GetValues<Type>();
        for (var i = 0; i < biomes.Length; i++)
        {
            var biome = biomes[i];
            var biomeElevation = (int)biome.GetElevation();
            if (biomeElevation == (int)Elevation.None)
            {
                continue;
            }

            List<Type> neighbors = [];
            for (var j = 0; j < biomes.Length; j++)
            {
                var otherBiome = biomes[j];
                var otherBiomeElevation = (int)otherBiome.GetElevation();
                if (biomeElevation == otherBiomeElevation - 1 || biomeElevation == otherBiomeElevation + 1)
                {
                    neighbors.Add(otherBiome);
                }
            }

            Neighbors.Add(biome, neighbors.ToArray());
        }
    }

    private static Elevation GetElevation(this Type biome)
    {
        return biome switch
        {
            Type.None => Elevation.None,
            Type.Ocean => Elevation.Ocean,
            Type.TropicalWater => Elevation.Ocean,
            Type.Beach => Elevation.Beach,
            Type.LightBeach => Elevation.Beach,
            Type.Jungle => Elevation.Midland,
            Type.Forest => Elevation.Midland,
            Type.Swamp => Elevation.Midland,
            Type.Savannah => Elevation.HighLand,
            Type.Mountain => Elevation.Mountain,
            Type.River => Elevation.None,
            Type.TropicalRiver => Elevation.None,
            Type.Tundra => Elevation.HighLand,
            Type.IceRiver => Elevation.None,
            _ => Elevation.None,
        };
    }
}