﻿namespace GameServer.Realm.Terrain.Biome;

internal enum Elevation : byte
{
    None,
    Ocean,
    Beach,
    Midland,
    HighLand,
    Mountain,
}