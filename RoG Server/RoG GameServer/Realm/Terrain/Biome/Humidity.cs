﻿namespace GameServer.Realm.Terrain.Biome;

internal enum Humidity : byte
{
    Low,
    Medium,
    High,
}
