﻿using RoGCore.Models;

namespace GameServer.Realm.Terrain.Biome;

internal static class HumidityExtensions
{
    internal static Humidity GetHumidity(in Vector2Int position, Noise noise, float scale, out double humidityNoise)
    {
        humidityNoise = Biome.GetBiomeNoise(noise, position + new Vector2Int(28453, -321937), 0.0005f * scale);

        return humidityNoise switch
        {
            < 0.39 => Humidity.Low,
            > 0.61 => Humidity.High,
            _ => Humidity.Medium,
        };
    }
}
