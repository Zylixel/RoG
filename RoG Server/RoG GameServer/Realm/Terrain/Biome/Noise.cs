﻿// FastNoise.cs
//
// MIT License
//
// Copyright(c) 2017 Jordan Peck
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files(the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions :
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
// The developer's email is jorzixdan.me2@gzixmail.com (for great email, take
// off every 'zix'.)

using System.Numerics;

// Heavily modified to fit project needs
namespace GameServer.Realm.Terrain.Biome;

internal class Noise
{
    internal readonly int Seed;

    // Hashing
    private const int Xprime = 1619;
    private const int Yprime = 31337;
    private const float F2 = 1.0f / 2.0f;
    private const float G2 = 1.0f / 4.0f;

    private static readonly Vector2[] Grad2D =
    [
        new(-1, -1), new(1, -1), new(-1, 1), new(1, 1),
        new(0, -1), new(-1, 0), new(0, 1), new(1, 0),
    ];

    internal Noise(int seed)
    {
        Seed = seed;
    }

    internal static int FastFloor(float f)
    {
        return f >= 0 ? (int)f : (int)f - 1;
    }

    // Simplex Noise
    internal float Octave(
        int x,
        int y,
        int octaves,
        float persistence,
        float frequency)
    {
        float total = 0;
        float amplitude = 1;
        for (var i = 0; i < octaves; i++)
        {
            total += Simplex(x, y, frequency) * amplitude;
            amplitude *= persistence;
            frequency *= 2;
        }

        return total;
    }

    internal float Simplex(int x, int y, float frequency)
    {
        return SingleSimplex(Seed, x * frequency, y * frequency);
    }

    private static float GradCoord2D(int seed, int x, int y, float xd, float yd)
    {
        var hash = seed;
        hash ^= Xprime * x;
        hash ^= Yprime * y;
        hash = hash * hash * hash * 60493;
        hash = hash >> 13 ^ hash;
        var g = Grad2D[hash & 7];
        return (xd * g.X) + (yd * g.Y);
    }

    private static float SingleSimplex(int seed, float x, float y)
    {
        var t = (x + y) * F2;
        var i = FastFloor(x + t);
        var j = FastFloor(y + t);
        t = (i + j) * G2;
        var it = i - t;
        var jt = j - t;
        var x0 = x - it;
        var y0 = y - jt;
        int i1, j1;
        if (x0 > y0)
        {
            i1 = 1;
            j1 = 0;
        }
        else
        {
            i1 = 0;
            j1 = 1;
        }

        var x1 = x0 - i1 + G2;
        var y1 = y0 - j1 + G2;
        var x2 = x0 - 1 + F2;
        var y2 = y0 - 1 + F2;
        float n0, n1, n2;
        t = 0.5f - (x0 * x0) - (y0 * y0);
        if (t < 0)
        {
            n0 = 0;
        }
        else
        {
            t *= t;
            n0 = t * t * GradCoord2D(seed, i, j, x0, y0);
        }

        t = 0.5f - (x1 * x1) - (y1 * y1);
        if (t < 0)
        {
            n1 = 0;
        }
        else
        {
            t *= t;
            n1 = t * t * GradCoord2D(seed, i + i1, j + j1, x1, y1);
        }

        t = 0.5f - (x2 * x2) - (y2 * y2);
        if (t < 0)
        {
            n2 = 0;
        }
        else
        {
            t *= t;
            n2 = t * t * GradCoord2D(seed, i + 1, j + 1, x2, y2);
        }

        return 50 * (n0 + n1 + n2);
    }
}
