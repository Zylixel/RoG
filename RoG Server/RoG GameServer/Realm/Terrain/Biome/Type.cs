﻿namespace GameServer.Realm.Terrain.Biome;

internal enum Type : byte
{
    None,
    Ocean,
    TropicalWater,
    Beach,
    LightBeach,
    Jungle,
    Forest,
    Swamp,
    Savannah,
    Mountain,
    MountainOre,
    River,
    TropicalRiver,
    Tundra,
    IceRiver,
}
