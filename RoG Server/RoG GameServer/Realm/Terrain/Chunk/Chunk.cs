﻿using System.Collections;
using System.Numerics;
using GameServer.Realm.Terrain.Tile;
using RoGCore.Models;
using RoGCore.Vector;

namespace GameServer.Realm.Terrain.Chunk;

internal class Chunk : IEnumerable<RealmWmapTile>, IDisposable
{
    internal const int SIZE = 16;
    internal const float INVERSESIZE = 1f / SIZE;
    internal const bool COMPRESSDATA = false;

    // The last time the Chunk was refenenced or updated in milliseconds
    internal long LastReference;
    internal ChunkState State;

    private readonly RealmWmapTile[,] tiles;
    private readonly Vector2Int worldPosition;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Usage", "CA2245:Do not assign a property to itself", Justification = "Needed to Setup Tile Positions")]
    internal Chunk(in Vector2Int position, RealmTime time)
    {
        worldPosition = position * SIZE;
        LastReference = time.TotalElapsedMs;

        // TODO: Can we just use array pools?
        tiles = ChunkMemoryHelper.Rent(true);

        for (var i = 0; i < SIZE; i++)
        {
            for (var j = 0; j < SIZE; j++)
            {
                // This ensures the tiles positions are set properly
                this[i, j] = this[i, j];
            }
        }
    }

    internal RealmWmapTile this[int x, int y]
    {
        get => tiles[x, y];
        set
        {
            value.Position = new Vector2Int(x, y) + worldPosition;
            tiles[x, y] = value;
        }
    }

    internal RealmWmapTile this[in Vector2Int pos]
    {
        get => tiles[pos.X, pos.Y];
        set
        {
            value.Position = pos + worldPosition;
            tiles[pos.X, pos.Y] = value;
        }
    }

    public void Dispose()
    {
        ChunkMemoryHelper.Return(tiles);
    }

    public IEnumerator<RealmWmapTile> GetEnumerator()
    {
        for (var x = 0; x < SIZE; x++)
        {
            for (var y = 0; y < SIZE; y++)
            {
                var tile = tiles[x, y];
                if (tile is not null)
                {
                    yield return tile;
                }
            }
        }
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return tiles.GetEnumerator();
    }

    internal static Vector2Int RelativePosition(in Vector2Int pos)
    {
        return new(RelativePosition(pos.X), RelativePosition(pos.Y));
    }

    internal static Vector2Int RelativePosition(Vector2 pos)
    {
        return new(RelativePosition((int)pos.X), RelativePosition((int)pos.Y));
    }

    internal static int RelativePosition(int worldPosition)
    {
        int relativePos;
        if (worldPosition >= 0)
        {
            relativePos = worldPosition % SIZE;
        }
        else
        {
            // Map position to [-1, ChunkSize - 2]
            relativePos = SIZE + (worldPosition % SIZE) - 1;

            // Remap to [0, Chunksize - 1]
            if (relativePos == -1)
            {
                relativePos = SIZE - 1;
            }
        }

        return relativePos;
    }

    /// <summary>
    /// Returns the chunk a worldposition is in.
    /// </summary>
    /// <param name="pos">Position in world.</param>
    /// <returns>Chunk a worldposition is in.</returns>
    internal static Vector2Int ChunkPosition(Vector2 pos)
    {
        return new(ChunkPosition(pos.X), ChunkPosition(pos.Y));
    }

    internal static Vector4 ChunkPosition(Rectangle rect)
    {
        return new Vector4(ChunkPosition(rect.X), ChunkPosition(rect.Y), ChunkPosition(rect.Right - 1), ChunkPosition(rect.Bottom - 1));
    }

    internal static int ChunkPosition(float x)
    {
        return ChunkFloor(x * INVERSESIZE);
    }

    internal static IntVector2S ChunkPosition(ref IntVector2S positions)
    {
        // TODO: Would be cool to allow IntVector2S to be instantiated using array pools, or even spans
        var division = new IntVector2S(positions.Count);
        positions.Multiply(INVERSESIZE, ref division);
        return division;
    }

    private static int ChunkFloor(float input)
    {
        return (int)MathF.Floor(input);
    }
}