﻿using GameServer.Realm.Entities.GameObject;
using GameServer.Realm.Terrain.Biome;
using GameServer.Realm.Terrain.Foliage;
using GameServer.Realm.Terrain.Structures;
using RoGCore.Models;

namespace GameServer.Realm.Terrain.Chunk;

internal static class ChunkBuilder
{
    internal static void BuildChunk(this World.Worlds.Realm owner, Chunk chunk, ChunkState toState)
    {
        if (toState == chunk.State)
        {
            return;
        }

        if (chunk.State == ChunkState.None)
        {
            owner.CompleteBiomeStage(chunk);
        }

        if (toState == chunk.State)
        {
            return;
        }

        if (chunk.State == ChunkState.Biome)
        {
            owner.CompleteStructureStage(chunk);
        }

        if (toState == chunk.State)
        {
            return;
        }

        if (chunk.State == ChunkState.Structures)
        {
            for (var x = 0; x < Chunk.SIZE; x++)
            {
                for (var y = 0; y < Chunk.SIZE; y++)
                {
                    var tile = chunk[x, y];
                    var foliage = owner.GetFoliage(tile.Position, tile.Biome);

                    tile.TileDesc ??= foliage.Key;

                    if (tile.ObjDesc is null)
                    {
                        tile.ObjDesc = foliage.Value;
                    }
                    else if (tile.ObjDesc.Name == "StructureNoFill")
                    {
                        tile.ObjDesc = null;
                    }

                    chunk[x, y] = tile;
                }
            }

            chunk.State = ChunkState.Complete;
        }
    }

    private static void CompleteBiomeStage(this World.Worlds.Realm owner, Chunk chunk)
    {
        var onlyOneType = true;
        Biome.Type firstType;

        // Load [0,0] corner
        var tile = chunk[0, 0];
        tile.Biome = owner.GetBiome(tile.Position);
        firstType = tile.Biome;

        // Load [x, 0] edge
        for (var x = 2; x < Chunk.SIZE; x += 2)
        {
            tile = chunk[x, 0];
            tile.Biome = owner.GetBiome(tile.Position);

            if (firstType != tile.Biome)
            {
                onlyOneType = false;
            }
        }

        // Load [x, SIZE - 1] edge
        for (var x = 1; x < Chunk.SIZE; x += 2)
        {
            tile = chunk[x, Chunk.SIZE - 1];
            tile.Biome = owner.GetBiome(tile.Position);

            if (firstType != tile.Biome)
            {
                onlyOneType = false;
            }
        }

        // Load [0, y] edge
        for (var y = 2; y < Chunk.SIZE; y += 2)
        {
            tile = chunk[0, y];
            tile.Biome = owner.GetBiome(tile.Position);

            if (firstType != tile.Biome)
            {
                onlyOneType = false;
            }
        }

        // Load [Chunk.SIZE-1, y] edge
        for (var y = 0; y < Chunk.SIZE; y += 2)
        {
            tile = chunk[Chunk.SIZE - 1, y];
            tile.Biome = owner.GetBiome(tile.Position);

            if (firstType != tile.Biome)
            {
                onlyOneType = false;
            }
        }

        if (onlyOneType)
        {
            for (var x = 0; x < Chunk.SIZE; x++)
            {
                for (var y = 0; y < Chunk.SIZE; y++)
                {
                    tile = chunk[x, y];
                    tile.Biome = firstType;
                }
            }
        }
        else
        {
            for (var x = 0; x < Chunk.SIZE; x++)
            {
                for (var y = 0; y < Chunk.SIZE; y++)
                {
                    tile = chunk[x, y];

                    if (tile.Biome == Biome.Type.None)
                    {
                        tile.Biome = owner.GetBiome(tile.Position);
                    }
                }
            }
        }

        chunk.State = ChunkState.Biome;
    }

    private static void CompleteStructureStage(this World.Worlds.Realm owner, Chunk chunk)
    {
        foreach (var tile in chunk)
        {
            var worldPosition = tile.Position;
            var biome = tile.Biome;
            switch (Terrain.StructureHere(worldPosition))
            {
                case StructureType.CoralReef:
                    if (biome == Biome.Type.TropicalWater)
                    {
                        CoralReef.Generate(worldPosition, owner);
                    }

                    break;
                case StructureType.PirateShip:
                    if (biome == Biome.Type.Ocean)
                    {
                        PirateShip.Generate(worldPosition, owner);
                    }

                    break;
                case StructureType.Christmas:
                    if (biome == Biome.Type.Tundra)
                    {
                        Christmas.Generate(worldPosition, owner);
                    }

                    break;
                case StructureType.OldBlacksmith:
                    TryGenerateBlacksmith(owner, worldPosition, biome);

                    break;
                case StructureType.None:
                    break;
            }
        }

        chunk.State = ChunkState.Structures;
    }

    private static void TryGenerateBlacksmith(World.Worlds.Realm owner, in Vector2Int worldPosition, Biome.Type biome)
    {
        switch (biome)
        {
            case Biome.Type.Forest:
            case Biome.Type.Jungle:
                string objName = OldBlacksmith.GenerateT1(worldPosition, owner);
                var man = new Blacksmith(owner, objName, 1);
                man.Move(worldPosition);

                break;
            case Biome.Type.Savannah:
                objName = OldBlacksmith.GenerateT2(worldPosition, owner);
                man = new Blacksmith(owner, objName, 2);
                man.Move(worldPosition);

                break;
            case Biome.Type.Mountain:
                objName = OldBlacksmith.GenerateT3(worldPosition, owner);
                man = new Blacksmith(owner, objName, 3);
                man.Move(worldPosition);

                break;
            case Biome.Type.Tundra:
                objName = OldBlacksmith.GenerateTundra(worldPosition, owner);
                man = new Blacksmith(owner, objName, 2);
                man.Move(worldPosition);

                break;
        }
    }
}
