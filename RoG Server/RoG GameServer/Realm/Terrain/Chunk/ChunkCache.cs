﻿using System.Buffers;
using Microsoft.Extensions.Logging;
using RoGCore.Logging;
using RoGCore.Models;

namespace GameServer.Realm.Terrain.Chunk;

internal class ChunkCache
{
    // Length in milliseconds before saving chunks to Database.
    private const int LifeTime = 15 * 1000;

    private static readonly ILogger<ChunkCache> Logger = LogFactory.LoggingInstance<ChunkCache>();

    /// <summary>
    /// Stores chunks for a given amount of time after being resolved.
    /// </summary>
    private readonly Dictionary<Vector2Int, Chunk> chunkCache = [];

    internal void UnloadOldChunks(RealmTime time)
    {
        var arrayPool = ArrayPool<KeyValuePair<Vector2Int, Chunk>>.Shared;
        var oldChunks = arrayPool.Rent(chunkCache.Count);
        var oldChunkCount = 0;

        var enumerator = chunkCache.GetEnumerator();

        while (enumerator.MoveNext())
        {
            var chunk = enumerator.Current;

            if (chunk.Value.LastReference + LifeTime < time.TotalElapsedMs)
            {
                oldChunks[oldChunkCount++] = chunk;
            }
        }

        for (var i = 0; i < oldChunkCount; i++)
        {
            var chunk = oldChunks[i];
            chunkCache.Remove(chunk.Key);
            chunk.Value.Dispose();
        }

        arrayPool.Return(oldChunks);
    }

    internal void Add(in Vector2Int pos, Chunk chunk)
    {
        if (chunkCache.ContainsKey(pos))
        {
            Logger.LogWarning($"Tried to add a chunk that already exists into the chunkCache at {pos}");
        }

        if (!chunkCache.TryAdd(pos, chunk))
        {
            Logger.LogWarning($"Tried to add a chunk that already exists into the chunkCache at {pos}");
        }
    }

    internal bool TryRetrieve(in Vector2Int chunkPosition, RealmTime time, out Chunk chunk)
    {
        if (chunkCache.TryGetValue(chunkPosition, out chunk!))
        {
            chunk.LastReference = time.TotalElapsedMs;
            return true;
        }

        return false;
    }
}
