﻿using GameServer.Realm.Terrain.Tile;

namespace GameServer.Realm.Terrain.Chunk;

internal static class ChunkMemoryHelper
{
    private static readonly Stack<RealmWmapTile[,]> TilePool = new();

    internal static RealmWmapTile[,] Rent(bool clear = false)
    {
        if (TilePool.Count > 0)
        {
            var tiles = TilePool.Pop();

            if (clear)
            {
                for (var i = 0; i < Chunk.SIZE; i++)
                {
                    for (var j = 0; j < Chunk.SIZE; j++)
                    {
                        tiles[i, j].Clear();
                    }
                }
            }

            return tiles;
        }

        var ret = new RealmWmapTile[Chunk.SIZE, Chunk.SIZE];

        for (var i = 0; i < Chunk.SIZE; i++)
        {
            for (var j = 0; j < Chunk.SIZE; j++)
            {
                ret[i, j] = new RealmWmapTile();
            }
        }

        return ret;
    }

    internal static void Return(RealmWmapTile[,] tiles)
    {
        TilePool.Push(tiles);
    }
}
