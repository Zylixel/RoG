﻿namespace GameServer.Realm.Terrain.Chunk;

internal enum ChunkState : byte
{
    None,
    Structures,
    Biome,
    Complete,
}