﻿using static GameServer.Realm.EntityUtils;

namespace GameServer.Realm.Terrain.Foliage;

internal sealed class BeachFoliage : FoliageBase<BeachFoliage.BeachFoliageType>
{
    internal static readonly BeachFoliage Default = new();

    internal static readonly List<WeightedEntry<BeachFoliageType>> Weights =
    [
        new WeightedEntry<BeachFoliageType>(BeachFoliageType.PalmTree, 48),
        new WeightedEntry<BeachFoliageType>(BeachFoliageType.SandPebble, 480),
        new WeightedEntry<BeachFoliageType>(BeachFoliageType.Campfire, 3),
        new WeightedEntry<BeachFoliageType>(BeachFoliageType.SandCastle, 1),
    ];

    private static readonly string[] ObjectNameDictionary =
        [
            string.Empty, "Palm Tree", "Sand Pebble", "Campfire", "Sand Castle",
        ];

    private BeachFoliage()
    {
    }

    internal enum BeachFoliageType : byte
    {
        None,
        PalmTree,
        SandPebble,
        Campfire,
        SandCastle,
    }

    protected override int Rarity => 5;

    protected override string GetObjectName(BeachFoliageType foliage)
    {
        return ObjectNameDictionary[(int)foliage];
    }

    protected override BeachFoliageType PickEntry(Random rand)
    {
        return PickWeightedEntry(Weights);
    }
}