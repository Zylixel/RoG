﻿using RoGCore.Models;

namespace GameServer.Realm.Terrain.Foliage;

internal abstract class FoliageBase<T>
    where T : Enum
{
    internal const ushort FOLIAGEMAPSIZE = 200;

    protected readonly T[,] Map = new T[FOLIAGEMAPSIZE, FOLIAGEMAPSIZE];

    protected abstract int Rarity { get; }

    internal virtual void PopulateMap(Random rand)
    {
        for (var x = 0; x < FOLIAGEMAPSIZE / Rarity; x++)
        {
            for (var y = 0; y < FOLIAGEMAPSIZE / Rarity; y++)
            {
                Map[(x * Rarity) + rand.Next(0, Rarity), (y * Rarity) + rand.Next(0, Rarity)] = PickEntry(rand);
            }
        }
    }

    internal string GetFoilage(in Vector2Int pos)
    {
        return GetObjectName(Map[Math.Abs(pos.X % FOLIAGEMAPSIZE), Math.Abs(pos.Y % FOLIAGEMAPSIZE)]);
    }

    protected abstract string GetObjectName(T foliage);

    protected abstract T PickEntry(Random rand);
}