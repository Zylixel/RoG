﻿using Microsoft.Extensions.Logging;
using RoGCore.Assets;
using RoGCore.Logging;
using RoGCore.Models;
using RoGCore.Utils;

namespace GameServer.Realm.Terrain.Foliage;

internal static class FoliageResolver
{
    private static readonly ILogger Logger = LogFactory.LoggingInstance(nameof(FoliageResolver));

    internal static KeyValuePair<GroundDescription?, ObjectDescription?> GetFoliage(this World.Worlds.Realm owner, in Vector2Int position, Biome.Type biome)
    {
        var tileName = "Nothing";
        var objectName = string.Empty;
        switch (biome)
        {
            case Biome.Type.Beach:
                tileName = "Beach Sand";
                objectName = BeachFoliage.Default.GetFoilage(position);
                break;
            case Biome.Type.LightBeach:
                tileName = "Not Flowing Sand";
                objectName = BeachFoliage.Default.GetFoilage(position);
                break;
            case Biome.Type.Ocean:
                tileName = "Shallow Water";
                objectName = OceanFoliage.Default.GetFoilage(position);
                break;
            case Biome.Type.TropicalWater:
                tileName = "Tropical Water";
                objectName = TropicalFoliage.Default.GetFoilage(position);
                break;
            case Biome.Type.Jungle:
                tileName = "Jungle Grass";
                objectName = JungleFoliage.Default.GetFoilage(position);
                break;
            case Biome.Type.Forest:
                tileName = "Forest Grass";
                objectName = ForestFoliage.Default.GetFoilage(position);
                break;
            case Biome.Type.Swamp:
                tileName = "Swamp Grass";
                objectName = ForestFoliage.Default.GetFoilage(position);
                break;
            case Biome.Type.Savannah:
                tileName = "Dead Grass";
                objectName = SavannahFoliage.Default.GetFoilage(position);
                break;
            case Biome.Type.Tundra:
                tileName = "Snow";
                objectName = TundraFoliage.Default.GetFoilage(position);
                break;
            case Biome.Type.Mountain:
                tileName = "Mountain Rock";
                objectName = MountainFoliage.Default.GetFoilage(position);
                break;
            case Biome.Type.MountainOre:
                tileName = "Mountain Ore";
                objectName = MountainOreFoliage.Default.GetFoilage(position);
                break;
            case Biome.Type.River:
                tileName = "Shallow Water";
                break;
            case Biome.Type.TropicalRiver:
                tileName = "Tropical Water";
                break;
            case Biome.Type.IceRiver:
                tileName = "Ice";
                break;
        }

        if (!EmbeddedData.Tiles.TryGetValue(tileName, out var groundDescription))
        {
            Logger.LogError(
                $"ERROR loading tileDesc: '{tileName}' whilst initializig a world map");
            return new KeyValuePair<GroundDescription?, ObjectDescription?>(null, null);
        }

        if (objectName.IsNullOrWhiteSpace())
        {
            return new KeyValuePair<GroundDescription?, ObjectDescription?>(groundDescription, null);
        }

        if (!EmbeddedData.BetterObjects.TryGetValue(objectName, out var objectDescription))
        {
            Logger.LogError(
                $"ERROR loading objectDesc: '{objectName}' whilst initializig a world map");
            return new KeyValuePair<GroundDescription?, ObjectDescription?>(groundDescription, null);
        }

        return new KeyValuePair<GroundDescription?, ObjectDescription?>(groundDescription, objectDescription);
    }
}
