﻿using static GameServer.Realm.EntityUtils;

namespace GameServer.Realm.Terrain.Foliage;

internal sealed class ForestFoliage : FoliageBase<ForestFoliage.ForestFoliageType>
{
    internal static readonly ForestFoliage Default = new();

    internal static readonly List<WeightedEntry<ForestFoliageType>> Weights =
    [
        new WeightedEntry<ForestFoliageType>(ForestFoliageType.Tree, 50),
        new WeightedEntry<ForestFoliageType>(ForestFoliageType.Shrub, 200),
        new WeightedEntry<ForestFoliageType>(ForestFoliageType.Bush, 20),
        new WeightedEntry<ForestFoliageType>(ForestFoliageType.Rock, 10),
    ];

    private static readonly string[] ObjectNameDictionary =
    [
        string.Empty, "Forest Tree", "Forest Shrub", "Forest Bush", "Forest Rock",
    ];

    private ForestFoliage()
    {
    }

    internal enum ForestFoliageType : byte
    {
        None,
        Tree,
        Shrub,
        Bush,
        Rock,
    }

    protected override int Rarity => 2;

    protected override string GetObjectName(ForestFoliageType foliage)
    {
        return ObjectNameDictionary[(int)foliage];
    }

    protected override ForestFoliageType PickEntry(Random rand)
    {
        return PickWeightedEntry(Weights);
    }
}