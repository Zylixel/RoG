﻿using static GameServer.Realm.EntityUtils;

namespace GameServer.Realm.Terrain.Foliage;

internal sealed class JungleFoliage : FoliageBase<JungleFoliage.JungleFoliageType>
{
    internal static readonly JungleFoliage Default = new();

    internal static readonly List<WeightedEntry<JungleFoliageType>> Weights =
    [
        new WeightedEntry<JungleFoliageType>(JungleFoliageType.Tree, 20 * 2),
        new WeightedEntry<JungleFoliageType>(JungleFoliageType.Foliage, 35 * 2),
        new WeightedEntry<JungleFoliageType>(JungleFoliageType.GrassPlant, 35 * 2),
        new WeightedEntry<JungleFoliageType>(JungleFoliageType.Flower, 1),
        new WeightedEntry<JungleFoliageType>(JungleFoliageType.JungleRock, 5 * 2),
        new WeightedEntry<JungleFoliageType>(JungleFoliageType.ForestRock, 4 * 2),
    ];

    private static readonly string[] ObjectNameDictionary =
    [
        string.Empty, "Jungle Tree", "Jungle Foliage", "Jungle Grass Plant", "Jungle Flower", "Jungle Rock",
        "Forest Rock",
    ];

    internal enum JungleFoliageType : byte
    {
        None,
        Tree,
        Foliage,
        GrassPlant,
        Flower,
        JungleRock,
        ForestRock,
    }

    protected override int Rarity => 0;

    internal override void PopulateMap(Random rand)
    {
        for (var y = 0; y < FOLIAGEMAPSIZE; y++)
        {
            for (var x = 0; x < FOLIAGEMAPSIZE; x++)
            {
                if (rand.Next(0, 100) < 75)
                {
                    Map[x, y] = PickEntry(rand);
                }
            }
        }
    }

    protected override string GetObjectName(JungleFoliageType foliage)
    {
        return ObjectNameDictionary[(int)foliage];
    }

    protected override JungleFoliageType PickEntry(Random rand)
    {
        return PickWeightedEntry(Weights);
    }
}