﻿using static GameServer.Realm.EntityUtils;

namespace GameServer.Realm.Terrain.Foliage;

internal sealed class MountainFoliage : FoliageBase<MountainFoliage.MountainFoliageType>
{
    internal static readonly MountainFoliage Default = new();

    internal static readonly List<WeightedEntry<MountainFoliageType>> Weights =
    [
        new WeightedEntry<MountainFoliageType>(MountainFoliageType.Rock, 100),
    ];

    private static readonly string[] ObjectNameDictionary =
        [
            string.Empty, "Mountain Rock",
        ];

    private MountainFoliage()
    {
    }

    internal enum MountainFoliageType : byte
    {
        None,
        Rock,
    }

    protected override int Rarity => 5;

    protected override string GetObjectName(MountainFoliageType foliage)
    {
        return ObjectNameDictionary[(int)foliage];
    }

    protected override MountainFoliageType PickEntry(Random rand)
    {
        return PickWeightedEntry(Weights);
    }
}