﻿using static GameServer.Realm.EntityUtils;

namespace GameServer.Realm.Terrain.Foliage;

internal sealed class MountainOreFoliage : FoliageBase<MountainOreFoliage.MountainOreFoliageType>
{
    internal static readonly MountainOreFoliage Default = new();

    internal static readonly List<WeightedEntry<MountainOreFoliageType>> Weights =
    [
        new WeightedEntry<MountainOreFoliageType>(MountainOreFoliageType.Copper, 50),
        new WeightedEntry<MountainOreFoliageType>(MountainOreFoliageType.Iron, 40),
        new WeightedEntry<MountainOreFoliageType>(MountainOreFoliageType.Gold, 10),
    ];

    private static readonly string[] ObjectNameDictionary =
        [
            string.Empty, "Mountain Copper Ore", "Mountain Iron Ore", "Mountain Gold Ore",
        ];

    private MountainOreFoliage()
    {
    }

    internal enum MountainOreFoliageType : byte
    {
        None,
        Copper,
        Iron,
        Gold,
    }

    protected override int Rarity => 5;

    protected override string GetObjectName(MountainOreFoliageType foliage)
    {
        return ObjectNameDictionary[(int)foliage];
    }

    protected override MountainOreFoliageType PickEntry(Random rand)
    {
        return PickWeightedEntry(Weights);
    }
}