﻿using static GameServer.Realm.EntityUtils;

namespace GameServer.Realm.Terrain.Foliage;

internal sealed class OceanFoliage : FoliageBase<OceanFoliage.OceanFoliageType>
{
    internal static readonly OceanFoliage Default = new();

    internal static readonly List<WeightedEntry<OceanFoliageType>> Weights =
    [
        new WeightedEntry<OceanFoliageType>(OceanFoliageType.Rock, 50),
        new WeightedEntry<OceanFoliageType>(OceanFoliageType.Seaweed, 25),
        new WeightedEntry<OceanFoliageType>(OceanFoliageType.TopPlant, 25),
    ];

    private static readonly string[] ObjectNameDictionary =
        [
            string.Empty, "Water Rock", "Seaweed", "Water Topplant",
        ];

    private OceanFoliage()
    {
    }

    internal enum OceanFoliageType : byte
    {
        None,
        Rock,
        Seaweed,
        TopPlant,
    }

    protected override int Rarity => 10;

    protected override string GetObjectName(OceanFoliageType foliage)
    {
        return ObjectNameDictionary[(int)foliage];
    }

    protected override OceanFoliageType PickEntry(Random rand)
    {
        return PickWeightedEntry(Weights);
    }
}