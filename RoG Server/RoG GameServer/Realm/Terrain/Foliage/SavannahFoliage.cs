﻿using static GameServer.Realm.EntityUtils;

namespace GameServer.Realm.Terrain.Foliage;

internal sealed class SavannahFoliage : FoliageBase<SavannahFoliage.SavannahFoliageType>
{
    internal static readonly SavannahFoliage Default = new();

    internal static readonly List<WeightedEntry<SavannahFoliageType>> Weights =
    [
        new WeightedEntry<SavannahFoliageType>(SavannahFoliageType.Tree, 20),
        new WeightedEntry<SavannahFoliageType>(SavannahFoliageType.Shrub, 20),
        new WeightedEntry<SavannahFoliageType>(SavannahFoliageType.Cactus, 40),
        new WeightedEntry<SavannahFoliageType>(SavannahFoliageType.Rock, 20),
    ];

    private static readonly string[] ObjectNameDictionary =
        [
            string.Empty, "Savannah Tree", "Dead Shrub", "Saguaro Cactus", "Forest Rock",
        ];

    private SavannahFoliage()
    {
    }

    internal enum SavannahFoliageType : byte
    {
        None,
        Tree,
        Shrub,
        Cactus,
        Rock,
    }

    protected override int Rarity => 15;

    protected override string GetObjectName(SavannahFoliageType foliage)
    {
        return ObjectNameDictionary[(int)foliage];
    }

    protected override SavannahFoliageType PickEntry(Random rand)
    {
        return PickWeightedEntry(Weights);
    }
}