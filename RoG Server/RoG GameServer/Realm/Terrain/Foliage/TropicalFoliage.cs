﻿namespace GameServer.Realm.Terrain.Foliage;

internal sealed class TropicalFoliage : FoliageBase<TropicalFoliage.TropicalFoliageType>
{
    internal static readonly TropicalFoliage Default = new();

    private static readonly string[] ObjectNameDictionary = [string.Empty, "Seaweed"];

    private TropicalFoliage()
    {
    }

    internal enum TropicalFoliageType : byte
    {
        None,
        Seaweed,
    }

    protected override int Rarity => 12;

    protected override string GetObjectName(TropicalFoliageType foliage)
    {
        return ObjectNameDictionary[(int)foliage];
    }

    protected override TropicalFoliageType PickEntry(Random rand)
    {
        return TropicalFoliageType.Seaweed;
    }
}