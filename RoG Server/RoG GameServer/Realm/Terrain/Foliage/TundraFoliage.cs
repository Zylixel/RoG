﻿using static GameServer.Realm.EntityUtils;

namespace GameServer.Realm.Terrain.Foliage;

internal sealed class TundraFoliage : FoliageBase<TundraFoliage.TundraFoliageType>
{
    internal static readonly TundraFoliage Default = new();

    internal static readonly List<WeightedEntry<TundraFoliageType>> Weights =
    [
        new WeightedEntry<TundraFoliageType>(TundraFoliageType.Rock, 10),
        new WeightedEntry<TundraFoliageType>(TundraFoliageType.SnowyTree, 80),
        new WeightedEntry<TundraFoliageType>(TundraFoliageType.DeadTree, 10),
    ];

    private static readonly string[] ObjectNameDictionary =
        [
            string.Empty, "Snowy Rock", "Snowy Pine Tree", "Dead Pine Tree",
        ];

    private TundraFoliage()
    {
    }

    internal enum TundraFoliageType : byte
    {
        None,
        Rock,
        SnowyTree,
        DeadTree,
    }

    protected override int Rarity => 2;

    protected override string GetObjectName(TundraFoliageType foliage)
    {
        return ObjectNameDictionary[(int)foliage];
    }

    protected override TundraFoliageType PickEntry(Random rand)
    {
        return PickWeightedEntry(Weights);
    }
}