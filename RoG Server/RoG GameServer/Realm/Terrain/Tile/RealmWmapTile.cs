﻿using GameServer.Realm.Terrain.Structures;
using RoGCore.Models;

namespace GameServer.Realm.Terrain.Tile;

internal sealed class RealmWmapTile : WmapTile, IEquatable<RealmWmapTile>
{
    internal StructureType Structure;
    internal Biome.Type Biome;

    internal RealmWmapTile()
    {
        Structure = StructureType.None;
        Biome = Realm.Terrain.Biome.Type.None;
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(base.GetHashCode(), Biome, Structure);
    }

    public override bool Equals(object? obj)
    {
        return obj is RealmWmapTile tile && Equals(tile);
    }

    public bool Equals(RealmWmapTile? other)
    {
        if (other is null)
        {
            return this is null;
        }

        return other.GetType() == typeof(RealmWmapTile) && Structure == other.Structure && Biome == other.Biome && base.Equals(other);
    }

    internal void Clear()
    {
        Structure = StructureType.None;
        Biome = Realm.Terrain.Biome.Type.None;
        Region = TileRegion.None;
        Hp = 0;
        UpdateCount = 0;
        ObjDesc = null;
        TileDesc = null;
        Position = Vector2Int.Zero;
    }
}