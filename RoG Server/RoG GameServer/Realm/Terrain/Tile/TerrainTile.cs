﻿namespace GameServer.Realm.Terrain.Tile;

// TODO: record struct
internal struct TerrainTile : IEquatable<TerrainTile>
{
    public TileRegion Region { get; set; }

    public string TileId { get; set; }

    public string TileObj { get; set; }

    public bool Equals(TerrainTile other)
    {
        return TileId == other.TileId &&
          TileObj == other.TileObj &&
          Region == other.Region;
    }

    public override bool Equals(object? obj)
    {
        return obj is TerrainTile tile && Equals(tile);
    }

    public override int GetHashCode()
    {
        return HashCode.Combine((int)Region, TileId, TileObj);
    }
}