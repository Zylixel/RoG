﻿using System.Numerics;
using RoGCore.Assets;
using RoGCore.Models;

namespace GameServer.Realm.Terrain.Tile;

internal class WmapTile : IEquatable<WmapTile>
{
    // TODO: Maybe the HP field can just be a child of WmapTile
    internal int Hp;

    internal TileRegion Region;
    internal byte UpdateCount;
    internal GroundDescription? TileDesc;

    // TODO: Does this really need to be stored here?
    internal Vector2Int Position;

    private ObjectDescription? objDesc;

    internal WmapTile()
    {
    }

    internal ObjectDescription? ObjDesc
    {
        get => objDesc;
        set
        {
            objDesc = value;
            if (value is not null)
            {
                Hp = value.MaxHitPoints;
            }
        }
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(Region, ObjDesc, TileDesc);
    }

    public override bool Equals(object? obj)
    {
        return obj is WmapTile tile && Equals(tile);
    }

    public bool Equals(WmapTile? other)
    {
        return other is null
            ? this is null
            : Region == other.Region &&
          TileDesc == other.TileDesc && ObjDesc == other.ObjDesc;
    }

    internal ObjectDef ToDef(Vector2 pos)
    {
        if (ObjDesc is null)
        {
            return default;
        }

        List<KeyValuePair<byte, object>> stats = [];
        if (ObjDesc.Size.Length == 1)
        {
            stats.Add(new KeyValuePair<byte, object>(StatsType.Size, ObjDesc.Size[0]));
        }
        else
        {
            var minSize = ObjDesc.Size[0];
            var maxSize = ObjDesc.Size[^1];
            var sizeIncrease = pos.GetHashCode() % (maxSize - minSize + 1);

            stats.Add(
                new KeyValuePair<byte, object>(
                    StatsType.Size,
                    minSize + sizeIncrease));
        }

        return new ObjectDef(
            ObjDesc.ObjectType,
            new ObjectStats(
                -1,
                new Vector2(pos.X + 0.5f, pos.Y + 0.5f),
                [.. stats]));
    }

    internal WmapTile Clone()
    {
        return new()
        {
            UpdateCount = (byte)(UpdateCount + 1),
            Region = Region,
            TileDesc = TileDesc,
            ObjDesc = ObjDesc,
        };
    }

    internal bool Occupied(TileOccupency occupency)
    {
        return ((occupency & TileOccupency.OccupyFull) != 0 && ObjDesc?.OccupyFull == true)
                || ((occupency & TileOccupency.OccupyHalf) != 0 && (ObjDesc?.OccupyFull == true || ObjDesc?.OccupyHalf == true))
                || ((occupency & TileOccupency.NoWalk) != 0 && TileDesc?.NoWalk == true);
    }
}