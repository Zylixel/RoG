﻿namespace GameServer.Realm.Terrain.Structures;

internal enum StructureType : byte
{
    None,
    CoralReef,
    OldBlacksmith,
    PirateShip,
    Christmas,
}