﻿namespace GameServer.Realm.World.DungeonGeneration.Burrow;

internal class BurrowBoss : GenerationPiece
{
    internal override string[] MapFiles => ["BurrowBoss"];

    internal override GenerationPieceType Type => GenerationPieceType.Boss;
}