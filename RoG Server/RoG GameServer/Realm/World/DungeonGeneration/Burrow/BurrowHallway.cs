﻿namespace GameServer.Realm.World.DungeonGeneration.Burrow;

internal class BurrowHallway : GenerationPiece
{
    internal override string[] MapFiles => ["BurrowHallway1", "BurrowHallway2", "BurrowHallway3",];

    internal override GenerationPieceType Type => GenerationPieceType.Hallway;
}