﻿namespace GameServer.Realm.World.DungeonGeneration.Burrow;

internal class BurrowRoom : GenerationPiece
{
    internal override string[] MapFiles => ["BurrowRoom1"];

    internal override GenerationPieceType Type => GenerationPieceType.Room;
}