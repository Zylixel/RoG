﻿namespace GameServer.Realm.World.DungeonGeneration.Burrow;

internal class BurrowSpawn : GenerationPiece
{
    internal override string[] MapFiles => ["BurrowSpawn"];

    internal override GenerationPieceType Type => GenerationPieceType.Spawn;
}