﻿namespace GameServer.Realm.World.DungeonGeneration;

internal class DepthsBoss : GenerationPiece
{
    internal override string[] MapFiles => ["DepthsBoss"];

    internal override GenerationPieceType Type => GenerationPieceType.Boss;
}