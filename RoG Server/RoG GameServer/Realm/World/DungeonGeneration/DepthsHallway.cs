﻿namespace GameServer.Realm.World.DungeonGeneration;

internal class DepthsHallway : GenerationPiece
{
    internal override string[] MapFiles => ["DepthsHallway1", "DepthsHallway2", "DepthsHallway3", "DepthsHallway4"];

    internal override GenerationPieceType Type => GenerationPieceType.Hallway;
}