﻿namespace GameServer.Realm.World.DungeonGeneration;

internal class DepthsRoom : GenerationPiece
{
    internal override string[] MapFiles => ["DepthsRoom1", "DepthsRoom2", "DepthsRoom3"];

    internal override GenerationPieceType Type => GenerationPieceType.Room;
}