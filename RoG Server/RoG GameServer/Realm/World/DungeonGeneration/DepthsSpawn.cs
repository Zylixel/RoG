﻿namespace GameServer.Realm.World.DungeonGeneration;

internal class DepthsSpawn : GenerationPiece
{
    internal override string[] MapFiles => ["DepthsSpawn"];

    internal override GenerationPieceType Type => GenerationPieceType.Spawn;
}