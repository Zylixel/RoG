﻿using System.Reflection;
using GameServer.Realm.Terrain;
using GameServer.Realm.Terrain.Tile;
using Microsoft.Extensions.Logging;
using RoGCore.Logging;
using RoGCore.Models;

namespace GameServer.Realm.World.DungeonGeneration;

internal abstract class GenerationPiece
{
    internal Wmap Map;

    private static readonly ILogger<GenerationPiece> Logger = LogFactory.LoggingInstance<GenerationPiece>();

    private IntRectangle? rect;

    protected GenerationPiece()
    {
        if (MapFiles.Length > 0 && WmapData.LoadedMaps.TryGetValue(MapFiles[Random.Shared.Next(0, MapFiles.Length)], out var loadedMap))
        {
            // TODO: We technically dont have to clone the map,
            // but any modifications to the map will require the piece to clone it
            Map = loadedMap.Clone();
        }
        else
        {
            Map = new Wmap();
        }
    }

    internal abstract string[] MapFiles { get; }

    internal abstract GenerationPieceType Type { get; }

    internal IntRectangle Rectangle
    {
        get
        {
            rect ??= GetRect();
            return rect.Value;
        }
    }

    internal static void Initialize()
    {
        var count = 0;

        var initializers = Assembly.GetExecutingAssembly()
            .GetTypes()
            .Where(x => !x.IsAbstract && typeof(GenerationPiece).IsAssignableFrom(x))
            .Select(x => x.GetMethod("Initialize", BindingFlags.Public | BindingFlags.Static))
            .OfType<MethodInfo>()
            .ToArray();

        foreach (var initalizer in initializers)
        {
            Logger.LogStatus(initalizer.DeclaringType!.Name, count++, initializers.Length);

            initalizer.Invoke(null, null);
        }

        Logger.LogResults("Generation Piece", count);
    }

    internal void OrientRandomly()
    {
        for (var i = 0; i < Random.Shared.Next(0, 4); i++)
        {
            RotateClockWise();
        }
    }

    internal void RotateClockWise()
    {
        if (Map.Entities is not null)
        {
            var newHeight = Map.Tiles.GetLength(0);
            for (var i = 0; i < Map.Entities.Length; i++)
            {
                var entity = Map.Entities[i];
                Vector2Int position = new(entity.Item1.Y, newHeight - 1 - entity.Item1.X);
                Map.Entities[i] = Tuple.Create(position, entity.Item2);
            }
        }

        Map.Tiles = RotateCw(Map.Tiles);
        Map.Height = (ushort)Map.Tiles.GetLength(1);
        Map.Width = (ushort)Map.Tiles.GetLength(0);
        rect = null;
    }

    internal virtual bool RenderSetPiece(World world, in Vector2Int pos, out IntRectangle boundingBox)
    {
        boundingBox = IntRectangle.Empty;
        if (!TryInsert(world, pos))
        {
            return false;
        }

        boundingBox = new IntRectangle(pos.X + Rectangle.X, pos.Y + Rectangle.Y, Rectangle.Width, Rectangle.Height);

        foreach (var i in Map.InstantiateEntities(world))
        {
            i.Move(i.X + pos.X, i.Y + pos.Y);
        }

        return true;

        bool TryInsert(World world, in Vector2Int pos)
        {
            for (var x = 0; x < Map.Width; x++)
            {
                for (var y = 0; y < Map.Height; y++)
                {
                    var mapTile = Map[x, y];

                    if (mapTile is null)
                    {
                        continue;
                    }

                    if (mapTile.TileDesc is not null || mapTile.ObjDesc is not null)
                    {
                        var worldTile = world.Map[x + pos.X, y + pos.Y];

                        if (worldTile is null)
                        {
                            worldTile = new WmapTile();
                            world.Map[x + pos.X, y + pos.Y] = worldTile;
                        }
                        else if (worldTile.TileDesc is not null || worldTile.ObjDesc is not null)
                        {
                            return false;
                        }

                        worldTile.TileDesc = mapTile.TileDesc;
                        worldTile.ObjDesc = mapTile.ObjDesc;
                        worldTile.Region = mapTile.Region;
                    }
                }
            }

            return true;
        }
    }

    private static T[,] RotateCw<T>(T[,] mat)
    {
        var newHeight = mat.GetLength(0);
        var newWidth = mat.GetLength(1);
        var ret = new T[newWidth, newHeight];
        for (var y = 0; y < newHeight; y++)
        {
            for (var x = 0; x < newWidth; x++)
            {
                ret[x, newHeight - 1 - y] = mat[y, x];
            }
        }

        return ret;
    }

    private IntRectangle GetRect()
    {
        var lowestX = int.MaxValue;
        var lowestY = int.MaxValue;
        var highestX = int.MinValue;
        var highestY = int.MinValue;
        for (var y = 0; y < Map.Height; y++)
        {
            for (var x = 0; x < Map.Width; x++)
            {
                var tile = Map[x, y];

                if (tile is null || (tile.TileDesc is null && tile.ObjDesc is null))
                {
                    continue;
                }

                if (x < lowestX)
                {
                    lowestX = x;
                }

                if (x > highestX)
                {
                    highestX = x;
                }

                if (y < lowestY)
                {
                    lowestY = y;
                }

                if (y > highestY)
                {
                    highestY = y;
                }
            }
        }

        return new IntRectangle(lowestX, lowestY, highestX - lowestX + 1, highestY - lowestY + 1);
    }
}