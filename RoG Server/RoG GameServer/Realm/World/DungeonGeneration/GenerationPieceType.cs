﻿namespace GameServer.Realm.World.DungeonGeneration;

internal enum GenerationPieceType
{
    Spawn,
    Hallway,
    Room,
    Boss,
    Treasure,
    Other,
}