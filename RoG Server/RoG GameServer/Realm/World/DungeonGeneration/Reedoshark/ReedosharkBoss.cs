﻿namespace GameServer.Realm.World.DungeonGeneration.Reedoshark;

internal class ReedosharkBoss : GenerationPiece
{
    internal override string[] MapFiles => ["ReedosharkBoss"];

    internal override GenerationPieceType Type => GenerationPieceType.Boss;
}