﻿using GameServer.Realm.Terrain;
using GameServer.Realm.Terrain.Tile;
using GameServer.Realm.World.Worlds;
using RoGCore.Assets;

namespace GameServer.Realm.World.DungeonGeneration.Reedoshark;

internal class ReedosharkHallway : GenerationPiece
{
    private static GroundDescription? floor;
    private static ObjectDescription? wall;

    internal ReedosharkHallway(RandomWorld world)
    {
        Map = new Wmap()
        {
            Width = (ushort)Random.Shared.Next(1, 7),
            Height = 5,
        };
        Map.Tiles = new WmapTile[Map.Width, Map.Height];
        for (var x = 0; x < Map.Width; x++)
        {
            for (var y = 0; y < Map.Height; y++)
            {
                WmapTile tile = new()
                {
                    TileDesc = floor,
                };
                if (y == 0 || y == Map.Height - 1)
                {
                    tile.ObjDesc = wall;
                }
                else
                {
                    tile.Region = TileRegion.Hallway;
                }

                Map[x, y] = tile;
            }
        }
    }

    internal override string[] MapFiles => [];

    internal override GenerationPieceType Type => GenerationPieceType.Hallway;

    public static new void Initialize()
    {
        wall = EmbeddedData.BetterObjects["Wooden Wall"];
        floor = EmbeddedData.Tiles["Dark Wood Floor"];
    }
}