﻿using GameServer.Realm.Terrain;
using GameServer.Realm.Terrain.Tile;
using GameServer.Realm.World.Worlds;
using RoGCore.Assets;

namespace GameServer.Realm.World.DungeonGeneration.Reedoshark;

internal class ReedosharkRoom : GenerationPiece
{
    private static GroundDescription? floor;
    private static ObjectDescription? wall;
    private static ObjectDescription? crate;

    internal ReedosharkRoom(RandomWorld world)
    {
        Map = new Wmap()
        {
            Width = (ushort)Random.Shared.Next(8, 20),
            Height = (ushort)Random.Shared.Next(8, 20),
        };
        Map.Tiles = new WmapTile[Map.Width, Map.Height];
        for (var x = 0; x < Map.Width; x++)
        {
            for (var y = 0; y < Map.Height; y++)
            {
                WmapTile tile = new()
                {
                    TileDesc = floor,
                };
                if (x == 0 || y == 0 || y == Map.Height - 1 || x == Map.Width - 1)
                {
                    tile.ObjDesc = wall;
                    tile.Region = TileRegion.Door;
                }
                else if ((x == 1 || y == 1 || y == Map.Height - 2 || x == Map.Width - 2) &&
                         Random.Shared.Next(0, 35) == 0)
                {
                    tile.ObjDesc = crate;
                }
                else if (Random.Shared.Next(0, 26) == 0)
                {
                    tile.Region = TileRegion.Enemy;
                }

                Map[x, y] = tile;
            }
        }
    }

    internal override string[] MapFiles => [];

    internal override GenerationPieceType Type => GenerationPieceType.Room;

    public static new void Initialize()
    {
        wall = EmbeddedData.BetterObjects["Wooden Wall"];
        crate = EmbeddedData.BetterObjects["Wood Crate"];
        floor = EmbeddedData.Tiles["Dark Wood Floor"];
    }
}