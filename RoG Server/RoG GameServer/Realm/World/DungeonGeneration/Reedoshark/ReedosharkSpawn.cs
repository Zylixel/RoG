﻿namespace GameServer.Realm.World.DungeonGeneration.Reedoshark;

internal class ReedosharkSpawn : GenerationPiece
{
    internal override string[] MapFiles => ["ReedosharkSpawn"];

    internal override GenerationPieceType Type => GenerationPieceType.Spawn;
}