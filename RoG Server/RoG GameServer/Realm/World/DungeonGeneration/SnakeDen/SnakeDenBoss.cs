﻿namespace GameServer.Realm.World.DungeonGeneration.SnakeDen;

internal class SnakeDenBoss : GenerationPiece
{
    internal override string[] MapFiles => ["SnakeDenBoss"];

    internal override GenerationPieceType Type => GenerationPieceType.Boss;
}