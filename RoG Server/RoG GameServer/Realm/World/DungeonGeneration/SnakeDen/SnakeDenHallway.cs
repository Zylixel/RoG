﻿using GameServer.Realm.Terrain;
using GameServer.Realm.Terrain.Tile;
using GameServer.Realm.World.Worlds;
using RoGCore.Assets;
using static GameServer.Realm.EntityUtils;

namespace GameServer.Realm.World.DungeonGeneration.SnakeDen;

internal class SnakeDenHallway : GenerationPiece
{
    private static WeightedEntry<GroundDescription>[] floors = [];
    private static WeightedEntry<ObjectDescription?>[] objects = [];
    private static ObjectDescription? wall;

    internal SnakeDenHallway(RandomWorld world)
    {
        Map = new Wmap()
        {
            Width = (ushort)Random.Shared.Next(3, 10),
            Height = 5,
        };
        Map.Tiles = new WmapTile[Map.Width, Map.Height];

        for (var x = 0; x < Map.Width; x++)
        {
            for (var y = 0; y < Map.Height; y++)
            {
                WmapTile tile = new()
                {
                    TileDesc = PickWeightedEntry(floors),
                };

                if (y == 0 || y == Map.Height - 1)
                {
                    tile.ObjDesc = wall;
                }
                else
                {
                    tile.ObjDesc = PickWeightedEntry(objects);
                    tile.Region = TileRegion.Hallway;
                }

                Map[x, y] = tile;
            }
        }
    }

    internal override string[] MapFiles => [];

    internal override GenerationPieceType Type => GenerationPieceType.Hallway;

    public static new void Initialize()
    {
        floors =
        [
            new WeightedEntry<GroundDescription>(EmbeddedData.Tiles["Beach Sand"], 80),
            new WeightedEntry<GroundDescription>(EmbeddedData.Tiles["Castle Cobble"], 10),
            new WeightedEntry<GroundDescription>(EmbeddedData.Tiles["Mountain Rock"], 10),
        ];

        objects =
        [
            new WeightedEntry<ObjectDescription?>(null, 90),
            new WeightedEntry<ObjectDescription?>(EmbeddedData.BetterObjects["Sand Pebble"], 2),
            new WeightedEntry<ObjectDescription?>(EmbeddedData.BetterObjects["Jungle Grass Plant"], 3),
            new WeightedEntry<ObjectDescription?>(EmbeddedData.BetterObjects["Forest Rock"], 2),
            new WeightedEntry<ObjectDescription?>(EmbeddedData.BetterObjects["Seaweed"], 3),
        ];

        wall = EmbeddedData.BetterObjects["Cobble Wall"];
    }
}