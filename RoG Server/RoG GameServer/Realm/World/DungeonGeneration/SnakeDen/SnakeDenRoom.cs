﻿namespace GameServer.Realm.World.DungeonGeneration.SnakeDen;

internal class SnakeDenRoom : GenerationPiece
{
    internal override string[] MapFiles => ["SnakeDenRoom1", "SnakeDenRoom2"];

    internal override GenerationPieceType Type => GenerationPieceType.Room;
}