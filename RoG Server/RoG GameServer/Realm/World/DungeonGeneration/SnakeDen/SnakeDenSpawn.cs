﻿namespace GameServer.Realm.World.DungeonGeneration.SnakeDen;

internal class SnakeDenSpawn : GenerationPiece
{
    internal override string[] MapFiles => ["SnakeDenSpawn"];

    internal override GenerationPieceType Type => GenerationPieceType.Spawn;
}