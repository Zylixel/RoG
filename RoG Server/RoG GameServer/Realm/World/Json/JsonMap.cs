﻿using GameServer.Realm.Terrain;

namespace GameServer.Realm.World.Json;

[Serializable]
public struct JsonMap
{
    public byte[] Data { get; set; }

    public ushort Width { get; set; }

    public ushort Height { get; set; }

    public JsonMapTile[] Dict { get; set; }
}