﻿namespace GameServer.Realm.World.Json;

[Serializable]
public struct JsonMapObject
{
    public string Id { get; set; }
}