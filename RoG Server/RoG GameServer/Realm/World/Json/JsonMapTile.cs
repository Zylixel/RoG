﻿namespace GameServer.Realm.World.Json;

[Serializable]
public struct JsonMapTile
{
    public string? Ground { get; set; }

    public JsonMapObject? Obj { get; set; }

    public string? Region { get; set; }
}