﻿using System.Collections.Concurrent;
using GameServer.Realm.World.Worlds;
using Microsoft.Extensions.Logging;
using RoGCore.Logging;
using Type = System.Type;

namespace GameServer.Realm.World;

internal static class RandomWorldPool
{
    private static readonly ILogger Logger = LogFactory.LoggingInstance(nameof(RandomWorldPool));
    private static readonly ConcurrentDictionary<string, ConcurrentQueue<RandomWorld>> RandomWorldQueue = [];

    internal static void Initalize()
    {
        var t = typeof(RandomWorld);
        foreach (var world in t.Assembly.GetTypes().Where(x => t.IsAssignableFrom(x) && x != t))
        {
            EnqueueRandomWorld(world, 2);
        }
    }

    internal static RandomWorld DequeueRandomWorld(RandomWorld world)
    {
        if (RandomWorldQueue.TryGetValue(world.Name, out var worldQueue) &&
            worldQueue.TryDequeue(out var w))
        {
            Logger.LogDebug($"Dequeued {w.Name} from RandomWorld Queue.");
            EnqueueRandomWorld(world.GetType(), 1);
            RealmManager.AddWorld(w);
            return w;
        }

        Logger.LogWarning("Cannot Dequeue Random World! Either there is no entry for it in the dictionary, or the queue is empty.");
        RealmManager.AddWorld(world);
        return world;
    }

    private static void EnqueueRandomWorld(Type worldType, int count)
    {
        for (int i = 0; i < count; i++)
        {
            if (Program.AppSettings.GameServer.Multithreading)
            {
                new Task(() =>
                {
                    Thread.CurrentThread.Name = $"{worldType.Name} Generator";
                    GenerateAndEnqueueWorld(worldType);
                }).Start();
            }
            else
            {
                GenerateAndEnqueueWorld(worldType);
            }
        }
    }

    private static void GenerateAndEnqueueWorld(Type worldType)
    {
        // Create instance of RandomWorld
        var w = (RandomWorld?)Activator.CreateInstance(worldType)
            ?? throw new ArgumentException($"Could not create an instance of {worldType}");

        // Initialize RandomWorld
        w.Initialize();

        // Queue RandomWorld
        if (!RandomWorldQueue.TryGetValue(w.Name, out var queue))
        {
            queue = new ConcurrentQueue<RandomWorld>();
            queue.Enqueue(w);
            RandomWorldQueue.TryAdd(w.Name, queue);
        }
        else
        {
            queue.Enqueue(w);
        }

        Logger.LogDebug(
            $"[Thread {Thread.CurrentThread.ManagedThreadId}] Enqueued {w.Name} to RandomWorld Queue");
    }
}