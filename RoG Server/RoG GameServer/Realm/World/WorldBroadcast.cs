﻿using System.Numerics;
using GameServer.Realm.Entities;
using GameServer.Realm.Entities.Player;
using RoGCore.Networking;

namespace GameServer.Realm.World;

internal abstract partial class World
{
    internal void BroadcastPacket<T>(in T pkt)
        where T : IPacket
    {
        for (var i = 0; i < Players.Count; i++)
        {
            Players[i].Client.SendPacket(pkt);
        }
    }

    internal void BroadcastPacket<T>(in T pkt, Vector2 position)
        where T : IPacket
    {
        foreach (var player in InRange<Player>(position, Program.AppSettings.GameServer.Sight.Radius))
        {
            player.Client.SendPacket(pkt);
        }
    }

    internal void BroadcastPacket<T, TEntity>(in T pkt, TEntity sender, bool ignoreSelf)
        where T : IPacket
        where TEntity : Entity
    {
        for (var i = 0; i < Players.Count; i++)
        {
            var player = Players[i];

            if (ignoreSelf && player == sender)
            {
                return;
            }

            if (player.ClientEntities.Contains(sender))
            {
                player.Client.SendPacket(pkt);
            }
        }
    }

    internal void BroadcastPackets(IEnumerable<IPacket> pkts)
    {
        for (var i = 0; i < Players.Count; i++)
        {
            var player = Players[i];
            player.Client.SendPacket(pkts);
        }
    }

    internal void BroadcastPackets(IEnumerable<IPacket> pkt, Vector2 position)
    {
        foreach (var player in InRange<Player>(position, Program.AppSettings.GameServer.Sight.Radius))
        {
            player.Client.SendPacket(pkt);
        }
    }

    internal void BroadcastPackets(IEnumerable<IPacket> pkt, Entity sender)
    {
        for (var i = 0; i < Players.Count; i++)
        {
            var player = Players[i];
            if (player.ClientEntities.Contains(sender))
            {
                player.Client.SendPacket(pkt);
            }
        }
    }
}
