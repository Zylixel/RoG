﻿using System.Numerics;
using GameServer.Realm.Entities;
using RoGCore.Models;

namespace GameServer.Realm.World;

internal abstract partial class World
{
    internal T? GetNearest<T>(Vector2 position, float dist)
        where T : Entity
    {
        var entities = InRange<T>(position, dist);
        T? closestEntity = null;
        foreach (var entity in entities)
        {
            if (closestEntity is null || entity.DistSqr(position) < closestEntity.DistSqr(position))
            {
                closestEntity = entity;
            }
        }

        return closestEntity;
    }

    internal T? GetNearest<T>(Vector2 position, float dist, Predicate<Entity> predicate)
        where T : Entity
    {
        var entities = InRange<T>(position, dist, predicate);
        T? closestEntity = null;
        foreach (var entity in entities)
        {
            if (closestEntity is null || entity.DistSqr(position) < closestEntity.DistSqr(position))
            {
                closestEntity = entity;
            }
        }

        return closestEntity;
    }

    internal IEnumerable<T> InRange<T>(Vector2 position, float dist)
        where T : Entity
    {
        if (dist == float.MaxValue)
        {
            return Entities.Values.OfType<T>();
        }

        var distSqr = dist * dist;
        Rectangle range = new(position.X - dist, position.Y - dist, dist * 2, dist * 2);
        return EntitiesInRectangle(range).OfType<T>().Where(en => en.DistSqr(position) <= distSqr);
    }

    internal IEnumerable<T> InRange<T>(Vector2 position, float dist, Predicate<Entity>? predicate)
        where T : Entity
    {
        if (dist != float.MaxValue)
        {
            var distSqr = dist * dist;

            var condition = predicate is not null
                ? en => en.DistSqr(position) <= distSqr && predicate(en)
                : new Predicate<Entity>(en => en.DistSqr(position) <= distSqr);

            Rectangle range = new(position.X - dist, position.Y - dist, dist * 2, dist * 2);
            return EntitiesInRectangle(range).OfType<T>().Where(entity => condition(entity));
        }

        if (predicate is null)
        {
            return Entities.Values.OfType<T>();
        }

        return Entities.Values.OfType<T>().Where(entity => predicate(entity));
    }
}
