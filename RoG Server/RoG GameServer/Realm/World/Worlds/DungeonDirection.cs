﻿using System.Diagnostics;
using RoGCore.Utils;

namespace GameServer.Realm.World.Worlds;

internal static class DungeonDirection
{
    internal enum Direction
    {
        Up,
        Down,
        Left,
        Right,
    }

    internal static bool ValidDirection(this Direction current, Direction previous)
    {
        switch (previous)
        {
            case Direction.Up when current == Direction.Down:
            case Direction.Down when current == Direction.Up:
            case Direction.Left when current == Direction.Right:
                return false;
            default:
                return previous != Direction.Right || current != Direction.Left;
        }
    }

    internal static Direction GetNextDirection(this Direction lastDir, Random rand)
    {
        var ret = (Direction)rand.Next(0, 4);

        // Make sure it doesn't back track
        while (!ret.ValidDirection(lastDir))
        {
            ret = (Direction)rand.Next(0, 4);
        }

        return ret;
    }

    internal static List<Direction> GetNextDirections(this Direction lastDir)
    {
        List<Direction> result = lastDir switch
        {
            Direction.Down => [Direction.Left, Direction.Right, Direction.Down],
            Direction.Left => [Direction.Down, Direction.Left, Direction.Up],
            Direction.Right => [Direction.Right, Direction.Down, Direction.Up],
            _ => [Direction.Left, Direction.Right, Direction.Up],
        };

        result.Shuffle();
        return result;
    }

    internal static void GetUniqueRandomDirections(ref Span<Direction> data)
    {
        Debug.Assert(data.Length <= 4, "Cannot return >4 unique directions");

        for (var i = 0; i < data.Length; i++)
        {
            var dir = (Direction)Random.Shared.Next(0, 4);
            while (Contains(data[..i], dir))
            {
                dir = (Direction)Random.Shared.Next(0, 4);
            }

            data[i] = dir;
        }
    }

    private static bool Contains(Span<Direction> data, Direction value)
    {
        for (int i = 0; i < data.Length; i++)
        {
            if (data[i] == value)
            {
                return true;
            }
        }

        return false;
    }
}