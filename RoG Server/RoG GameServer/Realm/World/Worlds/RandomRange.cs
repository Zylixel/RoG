﻿namespace GameServer.Realm.World.Worlds;

internal readonly struct RandomRange
{
    internal readonly int Min;
    internal readonly int Max;

    internal RandomRange(int min, int max)
    {
        Min = min;
        Max = max;
    }
}