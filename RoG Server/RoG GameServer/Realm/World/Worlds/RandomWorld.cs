﻿using System.Numerics;
using GameServer.Realm.Terrain;
using GameServer.Realm.Terrain.Tile;
using GameServer.Realm.World.DungeonGeneration;
using Microsoft.Extensions.Logging;
using RoGCore.Logging;
using RoGCore.Models;
using static GameServer.Realm.EntityUtils;
using static GameServer.Realm.World.Worlds.DungeonDirection;

namespace GameServer.Realm.World.Worlds;

internal abstract class RandomWorld : World
{
    private static readonly ILogger<RandomWorld> Logger = LogFactory.LoggingInstance<RandomWorld>();

    private bool initialized;

    protected abstract int BranchProbability { get; }

    protected abstract int MaxBranchContinuation { get; }

    protected abstract RandomRange MainBranchContinuation { get; }

    protected abstract RandomRange StartingRooms { get; }

    protected abstract int MinRoomsBeforeBoss { get; }

    protected abstract bool ContinueBranchAfterBoss { get; }

    protected abstract RandomRange RoomSpawnVariance { get; }

    protected abstract bool RandomlyOrientRooms { get; }

    protected abstract Direction[] BlockedBossRoomEntrance { get; }

    internal override void Initialize()
    {
        if (initialized)
        {
            return;
        }

        while (!Generate())
        {
            Logger.LogDebug("Failed to Generate RandomWorld");
        }

        // Get a copy of the current entities to push into the quadmap.
        var entities = Entities.ToArray();
        entityMap = new QuadTreeEntityMap(this);

        for (var i = 0; i < entities.Length; i++)
        {
            var entity = entities[i].Value;
            entityMap.AddEntity(entity);
        }

        initialized = true;
    }

    protected void FixMapSize()
    {
        Map.PlayerSpawnPoints.Clear();
        Map.RealmPortalSpawnPoints.Clear();

        var mapSize = GetRect();
        var fixedMap = new WmapTile[mapSize.Width, mapSize.Height];
        for (var y = 0; y < mapSize.Height; y++)
        {
            for (var x = 0; x < mapSize.Width; x++)
            {
                var tile = Map.Tiles[x + mapSize.X, y + mapSize.Y];

                if (tile is null)
                {
                    continue;
                }

                fixedMap[x, y] = tile;
                if (tile.Region == TileRegion.Spawn)
                {
                    Map.PlayerSpawnPoints.Add(new Vector2(x, y));
                }
                else if (tile.Region == TileRegion.RealmPortals)
                {
                    Map.RealmPortalSpawnPoints.Add(new Vector2(x, y));
                }
            }
        }

        for (var i = 0; i < Enemies.Count; i++)
        {
            var enemy = Enemies[i];
            enemy.Move(enemy.X - mapSize.X, enemy.Y - mapSize.Y);
            enemy.SpawnPoint = new Vector2(enemy.X, enemy.Y);
        }

        for (var i = 0; i < Objects.Count; i++)
        {
            var gameObject = Objects[i];
            gameObject.Move(gameObject.X - mapSize.X, gameObject.Y - mapSize.Y);
        }

        Map.Height = (ushort)mapSize.Height;
        Map.Width = (ushort)mapSize.Width;
        Map.Tiles = fixedMap;
    }

    protected void AddMargins(int marginSize)
    {
        Map.PlayerSpawnPoints.Clear();
        Map.RealmPortalSpawnPoints.Clear();

        var fullMargin = marginSize * 2;
        var fixedMap = new WmapTile[Map.Width + fullMargin, Map.Height + fullMargin];
        for (var y = 0; y < Map.Height; y++)
        {
            for (var x = 0; x < Map.Width; x++)
            {
                var tile = Map[x, y];

                if (tile is null)
                {
                    continue;
                }

                fixedMap[x + marginSize, y + marginSize] = tile;
                if (tile.Region == TileRegion.Spawn)
                {
                    Map.PlayerSpawnPoints.Add(new Vector2(x + marginSize, y + marginSize));
                }
                else if (tile.Region == TileRegion.RealmPortals)
                {
                    Map.RealmPortalSpawnPoints.Add(new Vector2(x + marginSize, y + marginSize));
                }
            }
        }

        for (var i = 0; i < Enemies.Count; i++)
        {
            var enemy = Enemies[i];
            enemy.Move(enemy.X + marginSize, enemy.Y + marginSize);
            enemy.SpawnPoint = new Vector2(enemy.X, enemy.Y);
        }

        for (var i = 0; i < Objects.Count; i++)
        {
            var gameObject = Objects[i];
            gameObject.Move(gameObject.X + marginSize, gameObject.Y + marginSize);
        }

        Map.Width = (ushort)(Map.Width + fullMargin);
        Map.Height = (ushort)(Map.Height + fullMargin);
        Map.Tiles = fixedMap;
    }

    protected IntRectangle GetRect()
    {
        var lowestX = int.MaxValue;
        var lowestY = int.MaxValue;
        var highestX = int.MinValue;
        var highestY = int.MinValue;
        for (var y = 0; y < Map.Height; y++)
        {
            for (var x = 0; x < Map.Width; x++)
            {
                var tile = Map[x, y];
                if (tile is not null && (tile.TileDesc is not null || tile.ObjDesc is not null))
                {
                    if (x < lowestX)
                    {
                        lowestX = x;
                    }

                    if (x > highestX)
                    {
                        highestX = x;
                    }

                    if (y < lowestY)
                    {
                        lowestY = y;
                    }

                    if (y > highestY)
                    {
                        highestY = y;
                    }
                }
            }
        }

        return new IntRectangle(lowestX, lowestY, highestX - lowestX + 1, highestY - lowestY + 1);
    }

    protected virtual bool Generate()
    {
        Map = new Wmap();
        Map.Width = Map.Height = 1000;
        Map.Tiles = new WmapTile[Map.Width, Map.Height];
        Map.Entities = [];

        var spawn = GetGenerationPiece(GenerationPieceType.Spawn);
        spawn.RenderSetPiece(this, new Vector2Int(500, 500), out var spawnBoundingBox);
        var spawnedBoss = false;
        var startRooms = Random.Shared.Next(StartingRooms);

        Span<Direction> startDirections = stackalloc Direction[startRooms];
        GetUniqueRandomDirections(ref startDirections);
        for (var i = 0; i < startRooms; i++)
        {
            var roomCount = 0;
            var dir = startDirections[i];
            if (!GoDirectionProtected(spawnBoundingBox, dir, false, out var last, ref spawnedBoss))
            {
                break;
            }

            roomCount++;
            var continuationAmount = Random.Shared.Next(MainBranchContinuation);
            if (continuationAmount < MinRoomsBeforeBoss && !spawnedBoss)
            {
                continuationAmount = MinRoomsBeforeBoss;
            }

            Branch(last, dir, roomCount, continuationAmount, ref spawnedBoss);
        }

        if (!spawnedBoss)
        {
            Logger.LogDebug("Regenerating map because boss didn't spawn");
            return false;
        }

        FixMapSize();
        return true;
    }

    protected void Branch(
        IntRectangle last,
        Direction dir,
        int roomCount,
        int continuationAmount,
        ref bool spawnedBoss)
    {
        for (var j = 0; j < continuationAmount; j++)
        {
            // If j == 0, go to the requested direction
            if (j != 0)
            {
                var nextDirs = dir.GetNextDirections();
                dir = nextDirs[0];
                if (Random.Shared.Next(1, 101) <= BranchProbability)
                {
                    Branch(last, nextDirs[1], roomCount, Random.Shared.Next(1, MaxBranchContinuation + 1), ref spawnedBoss);
                }
            }

            var boss = !spawnedBoss && roomCount >= MinRoomsBeforeBoss;
            if (!GoDirectionProtected(last, dir, boss, out last, ref spawnedBoss))
            {
                break;
            }

            roomCount++;
            if (boss && !ContinueBranchAfterBoss)
            {
                break;
            }
        }
    }

    protected bool GoDirectionProtected(
        IntRectangle branchFrom,
        Direction dir,
        bool boss,
        out IntRectangle roomSpawned,
        ref bool spawnedBoss)
    {
        var backup = (WmapTile[,])Map.Tiles.Clone();

        boss = boss && !BlockedBossRoomEntrance.Contains(dir);

        if (GoDirection(branchFrom, dir, boss, out roomSpawned))
        {
            if (boss)
            {
                spawnedBoss = true;
            }

            return true;
        }

        Map.Tiles = backup;
        return false;
    }

    protected bool GoDirection(
        IntRectangle branchFrom,
        Direction dir,
        bool boss,
        out IntRectangle roomSpawned)
    {
        if (!SpawnRoom(
            GetGenerationPiece(GenerationPieceType.Hallway),
            branchFrom,
            dir,
            Random.Shared.Next(RoomSpawnVariance),
            out roomSpawned))
        {
            return false;
        }

        var piece = GetGenerationPiece(boss ? GenerationPieceType.Boss : GenerationPieceType.Room);
        if (piece.Type == GenerationPieceType.Room && RandomlyOrientRooms)
        {
            piece.OrientRandomly();
        }

        return SpawnRoom(piece, roomSpawned, dir, Random.Shared.Next(RoomSpawnVariance), out roomSpawned);
    }

    protected bool SpawnRoom(
        GenerationPiece piece,
        IntRectangle lastRoom,
        Direction dir,
        int variance,
        out IntRectangle newRoom)
    {
        newRoom = default;
        switch (dir)
        {
            case Direction.Left:
                return piece.RenderSetPiece(
                    this,
                    new Vector2Int(
                        lastRoom.Right,
                        (int)(lastRoom.Y + (lastRoom.Height / 2f) - (piece.Rectangle.Height / 2f)) + variance),
                    out newRoom);
            case Direction.Right:
                return piece.RenderSetPiece(
                    this,
                    new Vector2Int(
                        lastRoom.Left - piece.Rectangle.Width,
                        (int)(lastRoom.Y + (lastRoom.Height / 2f) - (piece.Rectangle.Height / 2f)) + variance),
                    out newRoom);
            case Direction.Down:
                if (piece.Type != GenerationPieceType.Boss)
                {
                    piece.RotateClockWise();
                }

                return piece.RenderSetPiece(
                    this,
                    new Vector2Int(
                        (int)(lastRoom.X + (lastRoom.Width / 2f) - (piece.Rectangle.Width / 2f)) + variance,
                        lastRoom.Bottom),
                    out newRoom);
            case Direction.Up:
                if (piece.Type != GenerationPieceType.Boss)
                {
                    piece.RotateClockWise();
                }

                return piece.RenderSetPiece(
                    this,
                    new Vector2Int(
                        (int)(lastRoom.X + (lastRoom.Width / 2f) - (piece.Rectangle.Width / 2f)) + variance,
                        lastRoom.Top - piece.Rectangle.Height),
                    out newRoom);
        }

        return false;
    }

    protected abstract GenerationPiece GetGenerationPiece(GenerationPieceType pieceType);

    protected abstract IEnumerable<WeightedEntry<string>> GetPossibleEnemies();

    protected string GetEnemyWeighted()
    {
        return PickWeightedEntry(GetPossibleEnemies());
    }
}