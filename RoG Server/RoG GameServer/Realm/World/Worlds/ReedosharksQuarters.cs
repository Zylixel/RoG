﻿using System.Numerics;
using GameServer.Realm.Entities;
using GameServer.Realm.Entities.Enemy;
using GameServer.Realm.Entities.GameObject;
using GameServer.Realm.Terrain.Tile;
using GameServer.Realm.World.DungeonGeneration;
using GameServer.Realm.World.DungeonGeneration.Reedoshark;
using RoGCore.Assets;
using RoGCore.Models;
using RoGCore.Utils;
using RoGCore.Vector;
using static GameServer.Realm.EntityUtils;

namespace GameServer.Realm.World.Worlds;

internal class ReedosharksQuarters : RandomWorld
{
    internal static readonly WeightedEntry<string>[] Floors =
    [
        new WeightedEntry<string>("Dark Wood Floor", 97),
        new WeightedEntry<string>("Puddle Dark Wood Floor", 3),
    ];

    internal static readonly WeightedEntry<string>[] Walls =
    [
        new WeightedEntry<string>("Wooden Wall", 90),
        new WeightedEntry<string>("Sea Port Wall", 10),
    ];

    private static readonly WeightedEntry<string>[] PossibleEnemies =
    [
        new WeightedEntry<string>("Pirate Sailor", 45),
        new WeightedEntry<string>("Pirate Brawler", 45),
        new WeightedEntry<string>("Pirate Captain", 10),
    ];

    private readonly GroundDescription puddleWater;
    private readonly ObjectDescription woodenWall;

    public ReedosharksQuarters()
    {
        Name = "Reedoshark's Quarters";
        Background = 2;
        Difficulty = 1;
        Dungeon = true;
        puddleWater = EmbeddedData.Tiles["Puddle Dark Wood Floor"];
        woodenWall = EmbeddedData.BetterObjects["Wooden Wall"];
    }

    protected override int BranchProbability => 75;

    protected override int MaxBranchContinuation => 4;

    protected override RandomRange MainBranchContinuation => new(4, 9);

    protected override RandomRange StartingRooms => new(3, 4);

    protected override int MinRoomsBeforeBoss => 5;

    protected override bool ContinueBranchAfterBoss => false;

    protected override RandomRange RoomSpawnVariance => new(-1, 1);

    protected override bool RandomlyOrientRooms => true;

    protected override DungeonDirection.Direction[] BlockedBossRoomEntrance => [DungeonDirection.Direction.Down, DungeonDirection.Direction.Up];

    protected override string DefaultMusic => Program.AppSettings.GameServer.Music.ReedosharksQuarters;

    internal override void Explosion(Vector2 position, float radius, Entity damager, int damage, bool ignoreDefense)
    {
        if (damager.Name == "Reedoshark the Pirate King")
        {
            IntVector2S nineByNine = new(CardinalOrdinalDirections.NineByNineArea.Count);
            CardinalOrdinalDirections.NineByNineArea.Add(position.ToInt(), ref nineByNine);
            foreach (var explodePosition in nineByNine)
            {
                if (!TryGetTile(explodePosition, out var tile) || tile!.TileDesc?.Name != "Dark Wood Floor" ||
                    Random.Shared.Next(0, 3) != 0)
                {
                    continue;
                }

                tile.TileDesc = puddleWater;
            }
        }

        base.Explosion(position, radius, damager, damage, ignoreDefense);
    }

    protected override bool Generate()
    {
        if (!base.Generate())
        {
            return false;
        }

        for (var x = 0; x < Map.Width; x++)
        {
            for (var y = 0; y < Map.Height; y++)
            {
                Vector2Int current = new(x, y);
                var tile = Map[current];

                if (tile is null)
                {
                    continue;
                }

                switch (tile.Region)
                {
                    case TileRegion.Enemy:
                        Enemy en = new(GetEnemyWeighted(), this);
                        en.Move(current + Vector2Extensions.Half);
                        break;

                    case TileRegion.Door:
                    case TileRegion.Hallway1:
                        for (var i = 0; i < CardinalOrdinalDirections.CardinalDirections.Count; i++)
                        {
                            if (!TryGetTile(current + CardinalOrdinalDirections.CardinalDirections[i], out var nearbyTile))
                            {
                                continue;
                            }

                            if (nearbyTile!.Region != TileRegion.Hallway)
                            {
                                continue;
                            }

                            tile.ObjDesc = null;
                            break;
                        }

                        break;
                }

                if (tile.TileDesc?.Name == "Dark Wood Floor")
                {
                    tile.TileDesc = EmbeddedData.Tiles[PickWeightedEntry(Floors)];
                }

                if (tile.ObjDesc?.Name == "Wooden Wall")
                {
                    tile.ObjDesc = EmbeddedData.BetterObjects[PickWeightedEntry(Walls)];
                }
            }
        }

        CratePass();
        SeaPortPass();
        CannonEval();
        return true;
    }

    protected override GenerationPiece GetGenerationPiece(GenerationPieceType pieceType)
    {
        return pieceType switch
        {
            GenerationPieceType.Spawn => new ReedosharkSpawn(),
            GenerationPieceType.Hallway => new ReedosharkHallway(this),
            GenerationPieceType.Room => new ReedosharkRoom(this),
            GenerationPieceType.Boss => new ReedosharkBoss(),
            GenerationPieceType.Treasure => throw new ArgumentException($"pieceType is unsupported: {pieceType}"),
            GenerationPieceType.Other => throw new ArgumentException($"pieceType is unsupported: {pieceType}"),
            _ => throw new ArgumentException($"pieceType is unsupported: {pieceType}"),
        };
    }

    protected override IEnumerable<WeightedEntry<string>> GetPossibleEnemies()
    {
        return PossibleEnemies;
    }

    private void CratePass()
    {
        for (var x = 0; x < Map.Width; x++)
        {
            for (var y = 0; y < Map.Height; y++)
            {
                Vector2Int current = new(x, y);
                var tile = Map[current];

                if (tile is null)
                {
                    continue;
                }

                if (tile.ObjDesc?.Name != "Wood Crate")
                {
                    continue;
                }

                for (var i = 0; i < CardinalOrdinalDirections.CardinalDirections.Count; i++)
                {
                    if (!TryGetTile(current + CardinalOrdinalDirections.CardinalDirections[i], out var nearbyTile))
                    {
                        continue;
                    }

                    if (nearbyTile!.Region != TileRegion.Door || nearbyTile.ObjDesc is not null)
                    {
                        continue;
                    }

                    tile.ObjDesc = null;
                    break;
                }
            }
        }
    }

    private void SeaPortPass()
    {
        for (var x = 0; x < Map.Width; x++)
        {
            for (var y = 0; y < Map.Height; y++)
            {
                Vector2Int current = new(x, y);
                var tile = Map[current];

                if (tile is null)
                {
                    continue;
                }

                if (tile.ObjDesc?.Name != "Sea Port Wall")
                {
                    continue;
                }

                var nullWallsNearby = 0;
                for (var i = 0; i < CardinalOrdinalDirections.CardinalDirections.Count; i++)
                {
                    if (!TryGetTile(current + CardinalOrdinalDirections.CardinalDirections[i], out var nearbyTile))
                    {
                        continue;
                    }

                    if (nearbyTile!.TileDesc == null || nearbyTile.ObjDesc is not null || ++nullWallsNearby < 2)
                    {
                        continue;
                    }

                    tile.ObjDesc = woodenWall;
                    break;
                }
            }
        }
    }

    private void CannonEval()
    {
        var lowestXValue = float.MaxValue;
        var cannons = Entities.Select(x => x.Value).OfType<Cannon>();

        foreach (var cannon in cannons)
        {
            if (lowestXValue > cannon.X)
            {
                lowestXValue = cannon.X;
            }
        }

        foreach (var cannon in cannons)
        {
            cannon.ShootOrder = (int)(cannon.X - lowestXValue);
        }
    }
}