﻿using GameServer.Realm.Entities.Enemy;
using GameServer.Realm.Terrain.Tile;
using GameServer.Realm.World.DungeonGeneration;
using GameServer.Realm.World.DungeonGeneration.Burrow;
using RoGCore.Assets;
using RoGCore.Models;
using static GameServer.Realm.EntityUtils;

namespace GameServer.Realm.World.Worlds;

internal class RodentsBurrow : RandomWorld
{
    private readonly WeightedEntry<string>[] possibleEnemies =
    [
        new("Burrow Rat", 25),
        new("Burrow Bat", 25),
        new("Burrow Green Slime", 25),
        new("Burrow Purple Slime", 25),
    ];

    private GroundDescription[]? fancyGrounds;
    private ObjectDescription[]? fancyWalls;

    public RodentsBurrow()
    {
        Name = "Rodent's Burrow";
        Background = 2;
        Difficulty = 2;
        Dungeon = true;
    }

    protected override int BranchProbability => 75;

    protected override int MaxBranchContinuation => 3;

    protected override RandomRange MainBranchContinuation => new(4, 9);

    protected override RandomRange StartingRooms => new(2, 3);

    protected override int MinRoomsBeforeBoss => 5;

    protected override bool ContinueBranchAfterBoss => false;

    protected override RandomRange RoomSpawnVariance => new(0, 0);

    protected override bool RandomlyOrientRooms => false;

    protected override DungeonDirection.Direction[] BlockedBossRoomEntrance => [];

    protected override string DefaultMusic => Program.AppSettings.GameServer.Music.RodentsBurrow;

    internal override void Initialize()
    {
        fancyWalls =
        [
            EmbeddedData.BetterObjects["Mud Wall With Green Waterfall"],
            EmbeddedData.BetterObjects["Mud Wall With Purple Waterfall"],
        ];

        fancyGrounds =
        [
            EmbeddedData.Tiles["Mud With Green Liquid"],
            EmbeddedData.Tiles["Mud With Purple Liquid"],
        ];

        base.Initialize();
    }

    protected override bool Generate()
    {
        if (!base.Generate())
        {
            return false;
        }

        for (var y = 0; y < Map.Height; y++)
        {
            for (var x = 0; x < Map.Width; x++)
            {
                Vector2Int current = new(x, y);
                var tile = Map[current];

                if (tile is null)
                {
                    continue;
                }

                switch (tile.Region)
                {
                    case TileRegion.Door:
                        if (GetTile(current + new Vector2Int(0, 1))?.Region == TileRegion.Hallway ||
                                GetTile(current + new Vector2Int(0, -1))?.Region == TileRegion.Hallway ||
                                GetTile(current + new Vector2Int(1, 0))?.Region == TileRegion.Hallway ||
                                GetTile(current + new Vector2Int(-1, 0))?.Region == TileRegion.Hallway)
                        {
                            tile.ObjDesc = null;
                        }

                        break;

                    case TileRegion.Hallway when Random.Shared.Next(1, 101) <= 8 &&
                                                 this.RegionUnblocked(
                                                     current.X,
                                                     current.Y,
                                                     TileOccupency.NoWalk | TileOccupency.OccupyHalf):
                        Enemy en = new(GetEnemyWeighted(), this);
                        en.Move(current);
                        break;
                }

                if (tile.TileDesc?.Name == "Mud" && Random.Shared.Next(1, 101) <= 2)
                {
                    tile.TileDesc = fancyGrounds?[Random.Shared.Next(0, fancyGrounds.Length)];
                }

                if (tile.ObjDesc?.Name == "Mud Wall" && Random.Shared.Next(1, 101) <= 4)
                {
                    tile.ObjDesc = fancyWalls?[Random.Shared.Next(0, fancyWalls.Length)];
                }
            }
        }

        return true;
    }

    protected override GenerationPiece GetGenerationPiece(GenerationPieceType pieceType)
    {
        return pieceType switch
        {
            GenerationPieceType.Spawn => new BurrowSpawn(),
            GenerationPieceType.Hallway => new BurrowHallway(),
            GenerationPieceType.Room => new BurrowRoom(),
            GenerationPieceType.Boss => new BurrowBoss(),
            GenerationPieceType.Treasure => throw new ArgumentException($"pieceType is unsupported: {pieceType}"),
            GenerationPieceType.Other => throw new ArgumentException($"pieceType is unsupported: {pieceType}"),
            _ => throw new ArgumentException($"pieceType is unsupported : {pieceType}"),
        };
    }

    protected override IEnumerable<WeightedEntry<string>> GetPossibleEnemies()
    {
        return possibleEnemies;
    }
}