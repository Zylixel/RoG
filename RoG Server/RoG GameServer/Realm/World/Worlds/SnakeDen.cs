﻿using GameServer.Realm.Entities.Enemy;
using GameServer.Realm.Terrain.Tile;
using GameServer.Realm.World.DungeonGeneration;
using GameServer.Realm.World.DungeonGeneration.SnakeDen;
using RoGCore.Assets;
using RoGCore.Models;
using static GameServer.Realm.EntityUtils;

namespace GameServer.Realm.World.Worlds;

internal class SnakeDen : RandomWorld
{
    private static readonly WeightedEntry<string>[] PossibleEnemies =
    [
        new WeightedEntry<string>("Blue Snake", 30),
        new WeightedEntry<string>("Green Snake", 30),
        new WeightedEntry<string>("Yellow Snake", 30),
        new WeightedEntry<string>("Purple Snake", 10),
    ];

    public SnakeDen()
    {
        Name = "Snake Den";
        Background = 2;
        AllowTeleport = true;
        Difficulty = 1;
        Dungeon = true;
    }

    protected override int BranchProbability => 25;

    protected override int MaxBranchContinuation => 3;

    protected override RandomRange MainBranchContinuation => new(2, 4);

    protected override RandomRange StartingRooms => new(1, 1);

    protected override int MinRoomsBeforeBoss => 5;

    protected override bool ContinueBranchAfterBoss => false;

    protected override RandomRange RoomSpawnVariance => new(-2, 2);

    protected override bool RandomlyOrientRooms => true;

    protected override DungeonDirection.Direction[] BlockedBossRoomEntrance => [];

    protected override bool Generate()
    {
        if (!base.Generate())
        {
            return false;
        }

        for (var y = 0; y < Map.Height; y++)
        {
            for (var x = 0; x < Map.Width; x++)
            {
                Vector2Int current = new(x, y);
                var tile = Map[current];

                if (tile is null)
                {
                    continue;
                }

                switch (tile.Region)
                {
                    case TileRegion.Enemy:
                        Enemy en = new(GetEnemyWeighted(), this);
                        en.Move(current);
                        break;

                    case TileRegion.Door:
                        if (GetTile(current + new Vector2Int(0, 1))?.Region == TileRegion.Hallway ||
                                GetTile(current + new Vector2Int(0, -1))?.Region == TileRegion.Hallway ||
                                GetTile(current + new Vector2Int(1, 0))?.Region == TileRegion.Hallway ||
                                GetTile(current + new Vector2Int(-1, 0))?.Region == TileRegion.Hallway)
                        {
                            tile.ObjDesc = EmbeddedData.BetterObjects["Cobble Wall Destructable"];
                        }

                        break;
                }
            }
        }

        return true;
    }

    protected override GenerationPiece GetGenerationPiece(GenerationPieceType pieceType)
    {
        return pieceType switch
        {
            GenerationPieceType.Spawn => new SnakeDenSpawn(),
            GenerationPieceType.Hallway => new SnakeDenHallway(this),
            GenerationPieceType.Room => new SnakeDenRoom(),
            GenerationPieceType.Boss => new SnakeDenBoss(),
            GenerationPieceType.Treasure => throw new ArgumentException($"pieceType is unsupported: {pieceType}"),
            GenerationPieceType.Other => throw new ArgumentException($"pieceType is unsupported: {pieceType}"),
            _ => throw new ArgumentException($"pieceType is unsupported: {pieceType}"),
        };
    }

    protected override IEnumerable<WeightedEntry<string>> GetPossibleEnemies()
    {
        return PossibleEnemies;
    }
}