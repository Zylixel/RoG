﻿using GameServer.Realm.Entities.Enemy;
using GameServer.Realm.Terrain.Tile;
using GameServer.Realm.World.DungeonGeneration;
using RoGCore.Assets;
using RoGCore.Models;
using static GameServer.Realm.EntityUtils;

namespace GameServer.Realm.World.Worlds;

internal class TheDepths : RandomWorld
{
    private static readonly WeightedEntry<string>[] PossibleEnemies =
    [
        new WeightedEntry<string>("Depths Knight", 20),
        new WeightedEntry<string>("Depths Lion", 15),
        new WeightedEntry<string>("Depths Dragon", 15),
        new WeightedEntry<string>("Depths Scorpion", 50),
    ];

    private static ObjectDescription[]? bgTrees;
    private static GroundDescription? bgGround;

    public TheDepths()
    {
        Name = "The Depths";
        Background = 2;
        Difficulty = 2;
        Dungeon = true;
        LightLevel = 0.025f;
        Lighting = true;
    }

    protected override int BranchProbability => 25;

    protected override int MaxBranchContinuation => 2;

    protected override RandomRange MainBranchContinuation => new(1, 4);

    protected override RandomRange StartingRooms => new(3, 4);

    protected override int MinRoomsBeforeBoss => 5;

    protected override bool ContinueBranchAfterBoss => false;

    protected override RandomRange RoomSpawnVariance => new(-2, 2);

    protected override bool RandomlyOrientRooms => true;

    protected override DungeonDirection.Direction[] BlockedBossRoomEntrance => [];

    internal override void Initialize()
    {
        if (bgTrees is null)
        {
            bgTrees = new ObjectDescription[4];
            bgTrees[0] = EmbeddedData.BetterObjects["Depths Tree"];
            bgTrees[1] = EmbeddedData.BetterObjects["Jungle Foliage"];
            bgTrees[2] = EmbeddedData.BetterObjects["Jungle Rock"];
            bgTrees[3] = EmbeddedData.BetterObjects["Jungle Grass Plant"];
        }

        bgGround ??= EmbeddedData.Tiles["Jungle Grass NoWalk"];
        base.Initialize();
    }

    protected override bool Generate()
    {
        if (!base.Generate())
        {
            return false;
        }

        FixMapSize();
        AddMargins(15);

        for (var y = 0; y < Map.Height; y++)
        {
            for (var x = 0; x < Map.Width; x++)
            {
                Vector2Int current = new(x, y);
                var tile = Map[current];

                if (tile is null)
                {
                    tile = new WmapTile();
                    Map[current] = tile;
                }

                if (tile.Region == TileRegion.Enemy)
                {
                    Enemy en = new(GetEnemyWeighted(), this);
                    en.Move(current);
                }

                if (tile.TileDesc != null || tile.ObjDesc != null)
                {
                    continue;
                }

                if (Random.Shared.Next(0, 101) <= 90)
                {
                    tile.ObjDesc = bgTrees?[Random.Shared.Next(0, bgTrees.Length)];
                }

                tile.TileDesc = bgGround;
            }
        }

        return true;
    }

    protected override GenerationPiece GetGenerationPiece(GenerationPieceType pieceType)
    {
        return pieceType switch
        {
            GenerationPieceType.Spawn => new DepthsSpawn(),
            GenerationPieceType.Hallway => new DepthsHallway(),
            GenerationPieceType.Room => new DepthsRoom(),
            GenerationPieceType.Boss => new DepthsBoss(),
            GenerationPieceType.Treasure => throw new ArgumentException($"pieceType is unsupported: {pieceType}"),
            GenerationPieceType.Other => throw new ArgumentException($"pieceType is unsupported: {pieceType}"),
            _ => throw new ArgumentException($"pieceType is unsupported: {pieceType}"),
        };
    }

    protected override IEnumerable<WeightedEntry<string>> GetPossibleEnemies()
    {
        return PossibleEnemies;
    }
}