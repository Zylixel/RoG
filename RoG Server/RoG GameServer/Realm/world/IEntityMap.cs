﻿using System.Numerics;
using GameServer.Realm.Entities;
using RoGCore.Models;

namespace GameServer.Realm.World;

internal interface IEntityMap
{
    Dictionary<long, Entity> Entities { get; }

    void AddEntity(Entity entity);

    void MoveEntity(Entity entity, Vector2 oldPosition);

    void RemoveEntity(Entity entity);

    bool TryGetEntity(long id, out Entity? entity);

    IEnumerable<Entity> GetEntitiesInArea(Rectangle rect);
}