﻿using GameServer.Realm.Entities;
using RoGCore.Models;

namespace GameServer.Realm.World;

internal sealed class InfiniteEntityMap : IEntityMap
{
    public Dictionary<long, Entity> Entities { get; } = [];

    public void AddEntity(Entity entity)
    {
        Entities.TryAdd(entity.Id, entity);
    }

    public void MoveEntity(Entity entity, System.Numerics.Vector2 oldPosition)
    {
        // Method intentionally left empty.
    }

    public void RemoveEntity(Entity entity)
    {
        Entities.Remove(entity.Id);
    }

    public bool TryGetEntity(long id, out Entity? entity)
    {
        return Entities.TryGetValue(id, out entity);
    }

    public IEnumerable<Entity> GetEntitiesInArea(Rectangle rect)
    {
        var enumerator = Entities.GetEnumerator();

        while (enumerator.MoveNext())
        {
            var entity = enumerator.Current;

            if (rect.Contains(entity.Value.Position))
            {
                yield return entity.Value;
            }
        }
    }
}