﻿using System.Diagnostics;
using System.Numerics;
using GameServer.Realm.Entities;
using QuadTree;

namespace GameServer.Realm.World;

internal sealed class QuadTreeEntityMap : IEntityMap
{
    private readonly QuadTree<Entity> quadTree;

    internal QuadTreeEntityMap(World world)
    {
        quadTree = world.TryGetBounds(out var bounds)
            ? new QuadTree<Entity>(bounds.ToVector4())
            : throw new ArgumentException($"{world} does not specify bounds for QuadTree.");
    }

    public Dictionary<long, Entity> Entities { get; } = [];

    public void AddEntity(Entity entity)
    {
        var inserted = quadTree.Insert(entity);
        Debug.Assert(inserted, $"Could not add {entity} to quadtree");

        inserted = Entities.TryAdd(entity.Id, entity);
        Debug.Assert(inserted, $"Could not add {entity} to quadtree dictionary");
    }

    public void MoveEntity(Entity entity, Vector2 oldPosition)
    {
        Debug.Assert(Entities.ContainsKey(entity.Id), "Cannot move entity not present in QuadTreeMap");

        var removed = quadTree.Remove(entity, oldPosition);
        Debug.Assert(removed, $"Could not move {entity} in quadtree while removal");

        var inserted = quadTree.Insert(entity);
        Debug.Assert(inserted, $"Could not move {entity} in quadtree while insertion");
    }

    public void RemoveEntity(Entity entity)
    {
        var removed = quadTree.Remove(entity);
        Debug.Assert(removed, $"Could not remove {entity} from quadtree");

        removed = Entities.Remove(entity.Id);
        Debug.Assert(removed, $"Could not move {entity} from quadtree dictionary");
    }

    public bool TryGetEntity(long id, out Entity? entity)
    {
        return Entities.TryGetValue(id, out entity);
    }

    public IEnumerable<Entity> GetEntitiesInArea(RoGCore.Models.Rectangle rect)
    {
        return quadTree.Query(rect.ToVector4());
    }
}