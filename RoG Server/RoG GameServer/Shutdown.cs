﻿using System.Diagnostics;
using GameServer.Realm;
using Microsoft.Extensions.Logging;
using RoGCore.Logging;
using RoGCore.Models;

namespace GameServer;

internal static class Shutdown
{
    private static readonly ILogger Logger = LogFactory.LoggingInstance(nameof(Shutdown));

    internal static event EventHandler? ShutdownEvent;

    internal static void Restart()
    {
        Thread parallelThread = new(() =>
        {
            const string message = "Server is restarting.";
            Logger.LogWarning(message);

            foreach (var k in RealmManager.Clients.Values)
            {
                ChatManager.Tell(k?.Player, "Gilgor", message);
            }

            Thread.Sleep(2000);
            foreach (var clients in RealmManager.Clients.Values)
            {
                clients?.Disconnect(DisconnectReason.Restart);
            }

            var process = new Process();
            process.StartInfo.UseShellExecute = true;
            process.StartInfo.CreateNoWindow = false;
            process.StartInfo.FileName = Program.AppSettings.GameServer.File;
            process.Start();
            Stop();
        });

        parallelThread.Start();
    }

    internal static void Stop(Task? task = null)
    {
        if (task != null)
        {
            Logger.LogCritical(task.Exception, null);
        }

        ShutdownEvent?.Invoke(null, EventArgs.Empty);
    }
}
