﻿using GameServer.Realm.Entities;

namespace GameServer.Logic.Engine;

internal abstract class Behavior : IChildren
{
    void IChildren.Resolve(State parent, Dictionary<string, State> states)
    {
        Resolve(parent, states);
    }

    bool IChildren.Tick(Entity host)
    {
        TickCore(host);
        return true;
    }

    void IChildren.OnStateEntry(Entity host)
    {
        OnStateEntry(host);
    }

    void IChildren.OnStateExit(Entity host)
    {
        OnStateExit(host);
    }

    internal static State? FindState(State state, string name)
    {
        if (state.Name == name)
        {
            return state;
        }

        State? ret;
        foreach (var i in state.States)
        {
            if ((ret = FindState(i, name)) != null)
            {
                return ret;
            }
        }

        return null;
    }

    internal virtual void TickCore(Entity host)
    {
    }

    internal virtual void OnStateEntry(Entity host)
    {
    }

    internal virtual void OnStateExit(Entity host)
    {
    }

    internal virtual void Resolve(State parent, Dictionary<string, State> states)
    {
    }
}
