﻿using System.Collections.Frozen;
using System.Reflection;
using GameServer.Logic.Loot;
using GameServer.Realm.Entities;
using GameServer.Realm.Entities.Enemy;
using Microsoft.Extensions.Logging;
using RoGCore.Logging;

namespace GameServer.Logic.Engine;

// TODO: We are mixing static and non-static stuff in here and it is confusing what its purpose is
public static class BehaviorDb
{
    private static readonly ILogger Logger = LogFactory.LoggingInstance(nameof(BehaviorDb));
    private static Dictionary<string, KeyValuePair<State, Loot.Loot?>>? loadingDefinitions;

    public delegate Ctor BehaviorList();

    internal static FrozenDictionary<string, KeyValuePair<State, Loot.Loot?>> Definitions { get; private set; } = new Dictionary<string, KeyValuePair<State, Loot.Loot?>>().ToFrozenDictionary();

    internal static void Load()
    {
        Logger.LogStatus("Loading BehaviorDb");

        loadingDefinitions = [];

        var fields = typeof(Monsters.BehaviorDb)
            .GetFields(BindingFlags.Public | BindingFlags.Static)
            .Where(field => field.FieldType == typeof(BehaviorList))
            .ToList();

        for (var i = 0; i < fields.Count; i++)
        {
            var field = fields[i];
            try
            {
                Logger.LogStatus(field.Name, i, fields.Count);
                (field.GetValue(null) as BehaviorList)?.Invoke();
            }
            catch (Exception ex)
            {
                Logger.LogError($"Error Whilst loading {field.Name} : {ex}");
            }
        }

        Definitions = loadingDefinitions.ToFrozenDictionary();
        loadingDefinitions = null;
    }

    internal static void ResolveBehavior(Entity entity)
    {
        if (Definitions.TryGetValue(entity.ObjectDescription.Name, out var def))
        {
            entity.SwitchTo(def.Key);
        }
    }

    internal static Ctor Behav()
    {
        return default;
    }

    public struct Ctor
    {
        internal readonly Ctor Init(string objName, State rootState, params ILootDef[] defs)
        {
            Dictionary<string, State> d = [];
            rootState.Resolve(d);
            rootState.ResolveChildren(d);

            if (loadingDefinitions is null)
            {
                Logger.LogError(
                    $"Error Whilst loading behavior for {objName}, loadingDefinitions is null");
                return this;
            }

            if (loadingDefinitions.ContainsKey(objName))
            {
                Logger.LogError(
                    $"Error Whilst loading behavior for {objName}, key already added");
                return this;
            }

            if (defs.Length > 0)
            {
                Loot.Loot loot = new(defs);
                rootState.Death += (sender, e) =>
                {
                    if (e.Host is Enemy enemy)
                    {
                        loot.Handle(enemy);
                    }
                };
                loadingDefinitions.TryAdd(objName, new KeyValuePair<State, Loot.Loot?>(rootState, loot));
                Definitions = loadingDefinitions.ToFrozenDictionary();
            }
            else
            {
                loadingDefinitions.TryAdd(objName, new KeyValuePair<State, Loot.Loot?>(rootState, null));
                Definitions = loadingDefinitions.ToFrozenDictionary();
            }

            return this;
        }
    }
}