﻿namespace GameServer.Logic.Engine;

internal readonly struct Cooldown(float cooldown, int variance)
{
    internal static readonly Cooldown Zero = new(0, 0);

    public static implicit operator Cooldown(float cooldown)
    {
        return new(cooldown, 0);
    }

    internal bool IsNonZero() => cooldown != 0;

    internal float Next()
    {
        return variance == 0 ? cooldown : Math.Clamp(cooldown + Random.Shared.Next(-variance, variance + 1), 0, float.MaxValue);
    }
}