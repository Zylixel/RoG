﻿namespace GameServer.Logic.Engine;

internal abstract class CycleBehavior : Behavior
{
    // TODO: This isn't stored in state storage... surely this cannot be correct because each entity would share this state
    public CycleStatus Status { get; protected set; }
}