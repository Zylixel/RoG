﻿using GameServer.Realm;
using GameServer.Realm.Entities.Enemy;
using GameServer.Realm.Entities.Player;

namespace GameServer.Logic.Engine;

internal class DamageCounter
{
    private Dictionary<Player, int>? hitters;

    internal DamageCounter(Enemy enemy)
    {
        Host = enemy;
    }

    internal Enemy Host { get; private set; }

    internal Projectile? LastProjectile { get; private set; }

    internal Player? LastHitter { get; private set; }

    internal DamageCounter? Corpse { get; set; }

    internal DamageCounter? Parent { get; set; }

    internal void UpdateEnemy(Enemy enemy)
    {
        Host = enemy;
    }

    internal void HitBy(Player player, Projectile? projectile, int dmg)
    {
        hitters ??= [];

        if (!hitters.TryGetValue(player, out var totalDmg))
        {
            totalDmg = 0;
        }

        totalDmg += dmg;
        hitters[player] = totalDmg;
        LastHitter = player;
        if (projectile != null)
        {
            LastProjectile = projectile;
        }
    }

    internal IEnumerable<KeyValuePair<Player, int>> GetPlayerData()
    {
        if (Parent != null)
        {
            foreach (var data in Parent.GetPlayerData())
            {
                yield return data;
            }
        }

        if (hitters is not null)
        {
            foreach (var i in hitters)
            {
                if (i.Key.Disposed)
                {
                    continue;
                }

                yield return new KeyValuePair<Player, int>(i.Key, i.Value);
            }
        }
    }

    internal void Death()
    {
        if (Corpse is not null)
        {
            Corpse.Parent = this;
            return;
        }

        var enemy = (Parent ?? this).Host;
        var hitters = (Parent ?? this).hitters;

        if (hitters is null)
        {
            return;
        }

        var totalDamage = hitters.Values.Sum();

        foreach ((var player, var damage) in hitters)
        {
            if (player.Disposed)
            {
                continue;
            }

            var damagePercent = (float)damage / totalDamage;
            var xpAwarded = (uint)(enemy.ObjectDescription.MaxHitPoints * damagePercent);

            player.EnemyKilled(enemy, xpAwarded);
        }
    }
}