﻿using GameServer.Realm.Entities;

namespace GameServer.Logic.Engine;

internal class State : IChildren
{
    internal State(params IChildren[] children)
        : this(string.Empty, children)
    {
    }

    internal State(string name, params IChildren[] children)
    {
        Name = name;
        States = new List<State>();
        Behaviors = new List<IChildren>();
        Transitions = new List<IChildren>();
        foreach (var i in children)
        {
            switch (i)
            {
                case State state:
                    state.Parent = this;
                    States.Add(state);
                    break;
                case Behavior behavior:
                    Behaviors.Add(behavior);
                    break;
                case Transition transition:
                    Transitions.Add(transition);
                    break;
                default:
                    throw new NotSupportedException("Unknown children type.");
            }
        }
    }

    internal event EventHandler<BehaviorEventArgs>? Death;

    internal string Name { get; }

    internal State? Parent { get; private set; }

    internal List<State> States { get; }

    internal List<IChildren> Behaviors { get; }

    internal List<IChildren> Transitions { get; }

    void IChildren.Resolve(State parent, Dictionary<string, State> states)
    {
        throw new NotImplementedException();
    }

    bool IChildren.Tick(Entity host)
    {
        throw new NotImplementedException();
    }

    void IChildren.OnStateEntry(Entity host)
    {
        throw new NotImplementedException();
    }

    void IChildren.OnStateExit(Entity host)
    {
        throw new NotImplementedException();
    }

    internal static State? CommonParent(State? a, State? b)
    {
        return a is null || b is null ? null : CommonParent(a, a, b);
    }

    internal static State? CommonParent(State current, State a, State b)
    {
        return b.Is(current) ? current : a.Parent is null || current.Parent is null ? null : CommonParent(current.Parent, a, b);
    }

    // child is parent
    // parent is not child
    internal bool Is(State? state)
    {
        return state is not null && (this == state || Parent?.Is(state) == true);
    }

    internal void OnDeath(BehaviorEventArgs e)
    {
        Death?.Invoke(this, e);
        Parent?.OnDeath(e);
    }

    internal void Resolve(Dictionary<string, State> states)
    {
        states[Name] = this;
        foreach (var i in States)
        {
            i.Resolve(states);
        }
    }

    internal void ResolveChildren(Dictionary<string, State> states)
    {
        foreach (var i in States)
        {
            i.ResolveChildren(states);
        }

        foreach (var j in Transitions.Concat(Behaviors))
        {
            j.Resolve(this, states);
        }
    }
}
