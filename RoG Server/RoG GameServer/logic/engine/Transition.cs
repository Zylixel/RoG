﻿using GameServer.Realm.Entities;

namespace GameServer.Logic.Engine;

internal abstract class Transition(params string[] targetStates) : IChildren
{
    protected readonly string[] TargetStates = targetStates;
    protected int selectedState;

    internal State[]? TargetState { get; private set; }

    void IChildren.Resolve(State parent, Dictionary<string, State> states)
    {
        var numStates = TargetStates.Length;
        TargetState = new State[numStates];
        for (var i = 0; i < numStates; i++)
        {
            TargetState[i] = states[TargetStates[i]];
        }
    }

    bool IChildren.Tick(Entity host)
    {
        if (TargetState is null)
        {
            return false;
        }

        var ret = TickCore(host);
        if (ret)
        {
            host.SwitchTo(TargetState[selectedState]);
        }

        return ret;
    }

    void IChildren.OnStateEntry(Entity host)
    {
        OnStateEntry(host);
    }

    void IChildren.OnStateExit(Entity host)
    {
        OnStateExit(host);
    }

    internal virtual bool TickCore(Entity host)
    {
        return false;
    }

    internal virtual void OnStateEntry(Entity host)
    {
    }

    internal virtual void OnStateExit(Entity host)
    {
    }
}