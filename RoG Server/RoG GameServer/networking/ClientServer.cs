﻿using System.Net;
using System.Net.Sockets;
using GameServer.Networking.Socket;
using GameServer.Realm;
using Microsoft.Extensions.Logging;
using RoGCore.Logging;
using RoGCore.Models;
using RoGCore.Networking;

namespace GameServer.Networking;

internal class ClientServer
{
    private static readonly ILogger<ClientServer> Logger = LogFactory.LoggingInstance<ClientServer>();

    private readonly Dictionary<IPEndPoint, Client> clients = new();
    private readonly UdpClient udpClient;

    internal ClientServer()
    {
        Logger.LogDebug("Initializing ClientServer...");
        udpClient = new UdpClient(LocalEndpoint);

        var udpListener = new UDPListener(udpClient);
        udpListener.BeginListening();
        udpListener.PacketReceived += PacketReceived;

        Logger.LogDebug($"ClientServer Initialized. Listening for Clients on port: {Program.AppSettings.GameServer.Port}");
    }

    private static IPEndPoint LocalEndpoint =>
        new(IPAddress.Any, Program.AppSettings.GameServer.Port);

    internal void Stop()
    {
        Logger.LogInformation("Stopping ClientServer...");
        foreach (var i in RealmManager.Clients.Values.ToArray())
        {
            i.Disconnect(DisconnectReason.StoppingClientserver);
        }

        udpClient.Close();
    }

    private void PacketReceived(object? sender, (IPEndPoint, byte[]) e)
    {
        if (!clients.TryGetValue(e.Item1, out var cli))
        {
            cli = new Client(udpClient, e.Item1);
            clients.Add(e.Item1, cli);
        }

        cli.IncomingNetworkHandler.IncomingMessage(e.Item2);
    }
}