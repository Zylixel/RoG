﻿using Database;
using GameServer.Networking.Socket;
using GameServer.Realm;
using GameServer.Realm.Entities.Player;
using Microsoft.Extensions.Logging;
using RoGCore.Assets;
using RoGCore.Game;
using RoGCore.Logging;
using RoGCore.Models;
using RoGCore.Networking;
using RoGCore.Networking.Packets;
using RoGCore.Utils;

namespace GameServer.Networking.Messages.Handlers;

internal class CreateHandler : PacketHandler<Create>
{
    private static readonly ILogger<CreateHandler> Logger = LogFactory.LoggingInstance<CreateHandler>();

    public override MessageId Id => MessageId.Create;

    public override bool RunOutsideOfLogicThread => false;

    protected static async Task<IPacket> ProcessPacket(Client client, Create packet)
    {
        const int skin = 0;

        var currectCharacters = (await RedisDatabase.GetAliveCharacters(client.Account)).ToList();

        if (NewCharacterRequirement.MaxCharactersReached(currectCharacters.Count))
        {
            return new Failure("Maximum Character Limit Reached.");
        }

        if (!NewCharacterRequirement.AverageSkillReached(currectCharacters.Count, client.Account.Skills.AverageLevel()))
        {
            return new Failure("Average Skill Level Not Sufficient.");
        }

        // TODO: Use betterObjects
        var desc = EmbeddedData.Objects[packet.ClassType];

        foreach (var character in currectCharacters)
        {
            if (character is null)
            {
                Logger.LogError("Character Check was Null in CreateHandler");
                continue;
            }

            if (character.Class == desc.Name)
            {
                return new Failure("Class already created.");
            }
        }

        var result = await RedisDatabase.CreateCharacter(client.Account, desc, skin);

        if (result is null || !RealmManager.Worlds.TryGetValue(client.TargetWorld, out var targetWorld))
        {
            return new Failure("Failed to load character.");
        }

        client.Character = result;
        client.Player = new Player(client, targetWorld);
        return new CreateSuccess(client.Player.Id, result.CharacterId);
    }

    protected override void HandlePacket(Client client, in Create createPacket)
    {
        var result = ProcessPacket(client, createPacket).Exec();
        if (result is Failure failure)
        {
            client.SendPacket(failure);
            client.Disconnect(DisconnectReason.FailedToLoadCharacter);
        }
        else if (result is CreateSuccess packet)
        {
            client.SendPacket(packet);
        }
        else
        {
            Logger.LogError($"Unhandled Result from CreateHandler.HandlePacket {nameof(result)}");
        }
    }
}