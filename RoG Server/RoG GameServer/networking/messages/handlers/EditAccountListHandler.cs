﻿using Database;
using GameServer.Networking.Socket;
using RoGCore.Models;
using RoGCore.Networking.Packets;

namespace GameServer.Networking.Messages.Handlers;

internal class EditAccountListHandler : PacketHandler<EditAccountList>
{
    public override MessageId Id => MessageId.EditAccountList;

    public override bool RunOutsideOfLogicThread => false;

    protected override void HandlePacket(Client client, in EditAccountList packet)
    {
        if (client.Account.AccountId == packet.AccountId)
        {
            client.SendPacket(new Failure("You cannot do that with yourself."));
            return;
        }

        switch (packet.List)
        {
            case EditAccountList.ListType.Locked:
                RedisDatabase.LockAccount(client.Account, packet.AccountId);
                break;
            case EditAccountList.ListType.Ignored:
                RedisDatabase.IgnoreAccount(client.Account, packet.AccountId);
                break;
        }

        client.SendPacket(new AccountList(client.Account.Locked, client.Account.Ignored));
    }
}