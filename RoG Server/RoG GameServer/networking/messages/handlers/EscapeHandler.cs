﻿using GameServer.Networking.Socket;
using GameServer.Realm.World;
using RoGCore.Models;
using RoGCore.Networking.Packets;

namespace GameServer.Networking.Messages.Handlers;

internal class EscapeHandler : PacketHandler<Escape>
{
    public override MessageId Id => MessageId.Escape;

    public override bool RunOutsideOfLogicThread => false;

    protected override void HandlePacket(Client client, in Escape packet)
    {
        client.Reconnect(new Reconnect("Nexus", World.NEXUSID, 0), false);
    }
}