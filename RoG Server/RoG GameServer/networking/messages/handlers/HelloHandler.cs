﻿using Database.Models;
using FluentResults;
using GameServer.Networking.Auth;
using GameServer.Networking.Socket;
using GameServer.Realm;
using RoGCore.Models;
using RoGCore.Networking;
using RoGCore.Networking.Packets;
using static RoGCore.Utils.ResultExtenstions;

namespace GameServer.Networking.Messages.Handlers;

internal class HelloHandler : PacketHandler<Hello>
{
    private const string FailureMessageHeader = "Failed to Connect";

    public override MessageId Id => MessageId.Hello;

    public override bool RunOutsideOfLogicThread => false;

    protected override void HandlePacket(Client client, in Hello packet)
    {
        var packetCopy = packet;
        Task.Run(async () =>
        {
            var accountResult = await IncomingValidation.ValidateIncomingClient(client, packetCopy.Username, packetCopy.Password, packetCopy.BuildVersion);
            IncomingNetworkHandler.PacketWorkQueue.Enqueue(() => OnClientVerification(client, packetCopy, accountResult));
        });
    }

    private static void OnClientVerification(Client client, Hello packet, Result<DbAccount> accountResult)
    {
        if (accountResult.IsFailed)
        {
            client.SendPacket(new Failure(accountResult.ErrorString(), FailureMessageHeader));
            client.Disconnect(DisconnectReason.BadLogin);
            return;
        }

        client.Account = accountResult.Value;

        var realmResult = RealmManager.TryAcceptNewClient(client);
        if (realmResult.IsFailed)
        {
            client.SendPacket(new Failure(realmResult.ErrorString(), FailureMessageHeader));
            client.Disconnect(DisconnectReason.BadLogin);
            return;
        }

        var world = RealmManager.GetWorld(packet.WorldId);
        if (world is null)
        {
            client.SendPacket(new Failure("Invalid world.", FailureMessageHeader));
            client.Disconnect(DisconnectReason.InvalidWorld);
            return;
        }

        client.TargetWorld = world.Id;

        client.SendPacket(new MapInfo(
            world.Map.Width,
            world.Map.Height,
            world.Name,
            world.Difficulty,
            world.Background,
            world.AllowTeleport,
            world.Music,
            (ushort)((Program.AppSettings.GameServer.Event.LootDropMultiplier * 100) - 100),
            world.MaxPlayers,
            world.Dungeon,
            world.Lighting));
    }
}