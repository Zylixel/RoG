﻿using Database;
using Database.Models;
using GameServer.Networking.Socket;
using GameServer.Realm;
using GameServer.Realm.Entities.Player;
using Microsoft.Extensions.Logging;
using RoGCore.Logging;
using RoGCore.Models;
using RoGCore.Networking;
using RoGCore.Networking.Packets;

namespace GameServer.Networking.Messages.Handlers;

internal class LoadHandler : PacketHandler<Load>
{
    private static readonly ILogger<LoadHandler> Logger = LogFactory.LoggingInstance<LoadHandler>();

    public override MessageId Id => MessageId.Load;

    public override bool RunOutsideOfLogicThread => true;

    protected override void HandlePacket(Client client, in Load packet)
    {
        var packetCopy = packet;
        Task.Run(async () =>
        {
            var character = await RedisDatabase.LoadCharacterAsync(client.Account, packetCopy.CharacterId);

            if (character is null)
            {
                Logger.LogError($"Couldn't load {client.Account}'s character {packetCopy.CharacterId}");
            }

            IncomingNetworkHandler.PacketWorkQueue.Enqueue(() => CharacterRetrieved(client, character));
        });
    }

    private static void CharacterRetrieved(Client client, DbChar? character)
    {
        if (character is null)
        {
            client.SendPacket(new Failure("Failed to load character"));
            client.Disconnect(DisconnectReason.FailedToLoadCharacter);
            return;
        }

        client.Character = character;

        var world = RealmManager.Worlds[client.TargetWorld];
        if (world is null)
        {
            client.SendPacket(new Failure("Could not find world"));
            client.Disconnect(DisconnectReason.FailedToLoadCharacter);
            return;
        }

        Player player = new(client, world);

        client.Player = player;
        client.SendPacket(new CreateSuccess(player.Id, client.Character!.CharacterId));
    }
}