﻿using GameServer.Networking.Socket;
using GameServer.Realm.Terrain.Tile;
using RoGCore.Models;
using RoGCore.Networking.Packets;

namespace GameServer.Networking.Messages.Handlers;

internal class MoveHandler : PacketHandler<Move>
{
    public override MessageId Id => MessageId.Move;

    public override bool RunOutsideOfLogicThread => false;

    protected override void HandlePacket(Client client, in Move packet)
    {
        var player = client.Player;
        if (player is null || player.Disposed || player.HasConditionEffect(ConditionEffectIndex.Paralyzed))
        {
            return;
        }

        if (client.Account.Rank < (int)AccountType.Rank.Vip &&
            player.Owner.GetTile((int)packet.Position.X, (int)packet.Position.Y)?.Region == TileRegion.SupporterRegion)
        {
            player.SendError("This area is for supporters only!");
            player.Client.SendPacket(new Move(player.Position));
            return;
        }

        player.Move(packet.Position);
    }
}