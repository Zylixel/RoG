﻿using GameServer.Networking.Socket;
using RoGCore.Models;
using RoGCore.Networking.Packets;

namespace GameServer.Networking.Messages.Handlers;

internal class PaintHandler : PacketHandler<Paint>
{
    private static readonly Dictionary<Paint.RequestType, int> Prices = new()
    {
        { Paint.RequestType.RemoveBig, 100 },
        { Paint.RequestType.RemoveSmall, 100 },
        { Paint.RequestType.SetBig, 500 },
        { Paint.RequestType.SetSmall, 500 },
    };

    public override MessageId Id => MessageId.Paint;

    public override bool RunOutsideOfLogicThread => false;

    protected override void HandlePacket(Client client, in Paint packet)
    {
        var player = client.Player;
        if (player is null || client.Character is null || !EnsurePrice(client, packet.Request))
        {
            return;
        }

        switch (packet.Request)
        {
            case Paint.RequestType.RemoveBig:
                client.Character.Tex1 = -1;
                break;
            case Paint.RequestType.RemoveSmall:
                client.Character.Tex2 = -1;
                break;
            case Paint.RequestType.SetBig:
                client.Character.Tex1 = packet.Color;
                break;
            case Paint.RequestType.SetSmall:
                client.Character.Tex2 = packet.Color;
                break;
        }
    }

    private static bool EnsurePrice(Client client, Paint.RequestType request)
    {
        var price = Prices[request];
        if (client.Account.Silver > price)
        {
            client.Account.Silver -= price;
            return true;
        }

        client.Player?.SendError("Insufficient Funds");
        return false;
    }
}