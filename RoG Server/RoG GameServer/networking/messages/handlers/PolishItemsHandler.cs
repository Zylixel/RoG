﻿using Database;
using GameServer.Networking.Socket;
using GameServer.Realm.Entities.GameObject;
using Microsoft.Extensions.Logging;
using RoGCore.Assets;
using RoGCore.Logging;
using RoGCore.Models;
using RoGCore.Networking.Packets;
using static GameServer.Realm.EntityUtils;

namespace GameServer.Networking.Messages.Handlers;

internal class PolishItemsHandler : PacketHandler<PolishItems>
{
    private static readonly ILogger<PolishItemsHandler> Logger = LogFactory.LoggingInstance<PolishItemsHandler>();

    private static readonly List<KeyValuePair<int, WeightedEntry<int>>> ItemChances =
    [
        new KeyValuePair<int, WeightedEntry<int>>(
            1,
            new WeightedEntry<int>(2, (int)(Math.Abs(Math.Pow(2, 2) - 45) / 1.39 * 100))),
        new KeyValuePair<int, WeightedEntry<int>>(
            1,
            new WeightedEntry<int>(3, (int)(Math.Abs(Math.Pow(3, 2) - 45) / 1.39 * 100))),
        new KeyValuePair<int, WeightedEntry<int>>(
            2,
            new WeightedEntry<int>(4, (int)(Math.Abs(Math.Pow(4, 2) - 45) / 1.39 * 100))),
        new KeyValuePair<int, WeightedEntry<int>>(
            2,
            new WeightedEntry<int>(5, (int)(Math.Abs(Math.Pow(5, 2) - 45) / 1.39 * 100))),
        new KeyValuePair<int, WeightedEntry<int>>(
            3,
            new WeightedEntry<int>(6, (int)(Math.Abs(Math.Pow(6, 2) - 45) / 1.39 * 100))),
        new KeyValuePair<int, WeightedEntry<int>>(
            3,
            new WeightedEntry<int>(7, (int)(Math.Abs(Math.Pow(7, 2) - 45) / 1.39 * 100))),
    ];

    public override MessageId Id => MessageId.PolishItems;

    public override bool RunOutsideOfLogicThread => false;

    protected override void HandlePacket(Client client, in PolishItems packet)
    {
        var packetCopy = packet;
        var blacksmith = (Blacksmith?)client.Player?.Owner.Objects.Find(x => x.Id == packetCopy.BlacksmithId);

        if (blacksmith is null || client.Player is null)
        {
            client.SendPacket(new Result(false, "Internal Error"));
            return;
        }

        if (client.Account.Silver < 10)
        {
            client.SendPacket(new Result(false, "Insufficient Funds"));
            return;
        }

        var totalChanged = 0;
        for (var i = 0; i < client.Player.Inventory.Length; i++)
        {
            var item = client.Player.Inventory[i];
            if (item is null || item.Value.BaseItem.Tier != 1)
            {
                continue;
            }

            var newTier =
                PickWeightedEntry(
                    ItemChances.Where(x => x.Key <= blacksmith.Tier).Select(x => x.Value));
            var itemPool =
                EmbeddedData.Items.Values.Where(x => x.Tier == newTier && x.Type == item.Value.BaseItem.Type);
            if (!itemPool.Any())
            {
                Logger.LogError($"Cannot find item with tier: {newTier} and type {item.Value.BaseItem.Type}");
                continue;
            }

            var selectedItem = itemPool.ElementAt(Random.Shared.Next(0, itemPool.Count()));
            client.Player.Inventory[i] = new Item(selectedItem);
            totalChanged++;
        }

        if (totalChanged == 0)
        {
            client.SendPacket(new Result(false, "No Polish-able Items"));
            return;
        }

        RedisDatabase.UpdateSilver(client.Account, -10);
        client.SendPacket(new Result(true));
    }
}