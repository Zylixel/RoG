﻿using GameServer.Networking.Socket;
using RoGCore.Models;
using RoGCore.Networking.Packets;

namespace GameServer.Networking.Messages.Handlers;

internal class SetSkillTreeHandler : PacketHandler<SetSkillTree>
{
    public override MessageId Id => MessageId.SetSkillTree;

    public override bool RunOutsideOfLogicThread => false;

    protected override void HandlePacket(Client client, in SetSkillTree packet)
    {
        if (client.Player is null)
        {
            return;
        }

        if (client.Player.SkillTree.ValidateAndSet(packet.TreeData))
        {
            client.Player.CalcSkillTreeBoost();
        }
        else
        {
            client.Player.SendError("Failed to update Skill Tree.");
            client.SendPacket(new SetSkillTree(client.Player.SkillTree.Data));
        }
    }
}