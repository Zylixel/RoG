﻿using GameServer.Networking.Socket;
using RoGCore.Models;
using RoGCore.Networking.Packets;

namespace GameServer.Networking.Messages.Handlers;

internal class TeleportHandler : PacketHandler<Teleport>
{
    public override MessageId Id => MessageId.Teleport;

    public override bool RunOutsideOfLogicThread => false;

    protected override void HandlePacket(Client client, in Teleport packet)
    {
        if (client.Player is null)
        {
            return;
        }

        client.Player.SendError(client.Player.Teleport(packet));
    }
}