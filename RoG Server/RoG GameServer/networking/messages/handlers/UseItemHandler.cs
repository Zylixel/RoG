﻿using GameServer.Networking.Socket;
using GameServer.Realm;
using GameServer.Realm.Entities.GameObject;
using GameServer.Realm.Entities.Models;
using RoGCore.Models;
using RoGCore.Networking.Packets;

namespace GameServer.Networking.Messages.Handlers;

internal class UseItemHandler : PacketHandler<UseItem>
{
    public override MessageId Id => MessageId.UseItem;

    public override bool RunOutsideOfLogicThread => false;

    protected override void HandlePacket(Client client, in UseItem packet)
    {
        if (client.Player?.Owner.GetEntity(packet.SlotObject.OwnerId) is not IContainer container)
        {
            return;
        }

        var item = container.Inventory[packet.SlotObject.SlotId];
        if (item is not null && !client.Player.Activate(LogicTicker.CurrentTime, item.Value, packet) &&
            item.Value.BaseItem.Consumable)
        {
            container.Inventory[packet.SlotObject.SlotId] = null;
            if (container is Container c)
            {
                c.DespawnIfEmpty();
            }
        }
    }
}