﻿using GameServer.Networking.Socket;
using GameServer.Realm;
using GameServer.Realm.Entities.GameObject;
using GameServer.Realm.World;
using GameServer.Realm.World.Worlds;
using Microsoft.Extensions.Logging;
using RoGCore.Logging;
using RoGCore.Models;
using RoGCore.Networking.Packets;

namespace GameServer.Networking.Messages.Handlers;

internal class UsePortalHandler : PacketHandler<UsePortal>
{
    private static readonly ILogger<UsePortalHandler> Logger = LogFactory.LoggingInstance<UsePortalHandler>();

    public override MessageId Id => MessageId.UsePortal;

    public override bool RunOutsideOfLogicThread => false;

    protected override void HandlePacket(Client client, in UsePortal packet)
    {
        var player = client.Player;
        if (player?.Owner.GetEntity(packet.ObjectId) is not Portal portal)
        {
            return;
        }

        if (!portal.Usable)
        {
            player.SendError("Realm is full!");
            return;
        }

        var world = portal.WorldInstance;
        if (world is null)
        {
            var setWorldInstance = true;
            var desc = portal.ObjectDescription;

            switch (portal.ObjectDescription.Name)
            {
                case "HomeBase Portal":
                    world = RealmManager.AddWorld(new HomeBase());
                    setWorldInstance = false;
                    break;
                case "Exit Portal":
                    if (RealmManager.LastWorld.TryGetValue(player.Client.Account.AccountId, out var lastWorld))
                    {
                        world = lastWorld is not null && RealmManager.Worlds.ContainsKey(lastWorld.Id)
                            ? lastWorld : RealmManager.GetWorld(World.NEXUSID);
                    }
                    else
                    {
                        world = RealmManager.GetWorld(World.NEXUSID);
                    }

                    setWorldInstance = false;
                    break;
                case "dep4": // Portal of cowardice
                case "dep5": // Tomb portal of Cowardice
                case "dep6": // Glowing portal of Cowardice
                    if (RealmManager.LastWorld.TryGetValue(player.Client.Account.AccountId, out lastWorld))
                    {
                        world = lastWorld is not null && RealmManager.Worlds.ContainsKey(lastWorld.Id)
                            ? lastWorld : RealmManager.GetWorld(World.NEXUSID);
                    }
                    else
                    {
                        world = RealmManager.GetWorld(World.NEXUSID);
                    }

                    setWorldInstance = false;
                    break;
                case "dep9":
                case "dep10":
                    world = RealmManager.GetWorld(World.NEXUSID);
                    break;
                case "dep11":
                    if (player.GuildManager != null)
                    {
                        if (player.GuildManager.GuildHall is null)
                        {
                            player.SendInfo("Guild Hall not yet created");
                        }
                        else
                        {
                            world = player.GuildManager.GuildHall;
                        }
                    }
                    else
                    {
                        player.SendInfo("Join a guild first!");
                    }

                    break;
                default:
                    {
                        if (desc.DungeonName is null)
                        {
                            player.SendError("Dungeon has no name");
                        }
                        else
                        {
                            var worldName = "GameServer.Realm.World.Worlds." +
                                               desc.DungeonName.Replace(" ", string.Empty).Replace("'", string.Empty);
                            var worldType = Type.GetType(worldName, false, true);
                            if (worldType is null)
                            {
                                player.SendError(
                                    $"Dungeon instance \"{worldName}\" isn't declared yet and under maintenance until further notice.");
                            }
                            else
                            {
                                try
                                {
                                    var w = (World?)Activator.CreateInstance(worldType);
                                    switch (w)
                                    {
                                        case null:
                                            player.SendError(
                                                $"There was an error loading dungeon instance \"{worldName}\".");
                                            break;
                                        case RandomWorld r:
                                            world = RandomWorldPool.DequeueRandomWorld(r);
                                            break;
                                        default:
                                            world = RealmManager.AddWorld(w);
                                            break;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    player.SendError($"There was an error loading dungeon instance \"{worldName}\".");
                                    Logger.LogError(ex, null);
                                }
                            }
                        }
                    }

                    break;
            }

            if (setWorldInstance && world != null)
            {
                portal.WorldInstance = world;
            }
        }

        if (world is null)
        {
            return;
        }

        if (world.IsFull)
        {
            player.SendError("{\"key\":\"server.dungeon_full\"}");
            return;
        }

        if (RealmManager.LastWorld.ContainsKey(client.Account.AccountId))
        {
            RealmManager.LastWorld.TryRemove(client.Account.AccountId, out _);
        }

        if (player.Owner is Nexus or Realm.World.Worlds.Realm)
        {
            RealmManager.LastWorld.TryAdd(client.Account.AccountId, player.Owner);
        }

        client.Reconnect(
            new Reconnect(world.Name, world.Id, world.Difficulty),
            true);
    }
}