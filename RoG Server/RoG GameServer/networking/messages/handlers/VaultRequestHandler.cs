﻿using GameServer.Networking.Socket;
using RoGCore.Assets;
using RoGCore.Models;
using RoGCore.Networking.Packets;
using RoGCore.Utils;
using static RoGCore.Networking.Packets.VaultRequest;

namespace GameServer.Networking.Messages.Handlers;

internal sealed class VaultRequestHandler : PacketHandler<VaultRequest>
{
    // TODO: better
    // Shared between client and server
    private const int VaultSize = 40;

    // End Share
    private static readonly Dictionary<Client, VaultRequest> SearchHistory = [];

    public override MessageId Id => MessageId.VaultRequest;

    public override bool RunOutsideOfLogicThread => false;

    internal static void RefreshClient(Client client)
    {
        if (!SearchHistory.TryGetValue(client, out var value))
        {
            return;
        }

        var search = value.SearchTerm.ToUpperInvariant();

        bool SearchFunc(RawItem x)
        {
            var itemName = EmbeddedData.Items[x.ObjectType].Name;
            if (itemName is null)
            {
                return false;
            }

            return search.IsNullOrWhiteSpace() || itemName.Contains(search, StringComparison.InvariantCultureIgnoreCase);
        }

        int TierFunc(RawItem x)
        {
            var tier = EmbeddedData.Items[x.ObjectType].Tier;
            if (tier > 0)
            {
                return tier;
            }

            tier *= -1;
            return tier + 100;
        }

        var sortEnumerable = client.Account.Vault.OrderByDescending(SearchFunc);

        // Sort by sortingType
        switch (value.SortMode)
        {
            case SortType.LevelDown:
                sortEnumerable = sortEnumerable.ThenBy(x => x.Level);
                break;
            case SortType.LevelUp:
                sortEnumerable = sortEnumerable.ThenByDescending(x => x.Level);
                break;
            case SortType.TierDown:
                sortEnumerable = sortEnumerable.ThenBy(TierFunc);
                break;
            case SortType.TierUp:
                sortEnumerable = sortEnumerable.ThenByDescending(TierFunc);
                break;
        }

        sortEnumerable = sortEnumerable.ThenBy(x => EmbeddedData.Items[x.ObjectType].Name);
        var sortedItems = sortEnumerable.ToList();
        var page = (value.Page - 1) * VaultSize < sortedItems.Count
            ? sortedItems.GetRange(
                (value.Page - 1) * VaultSize,
                Math.Min(sortedItems.Count - ((value.Page - 1) * VaultSize), VaultSize))
            : [];
        client.SendPacket(new VaultResult(
            page.ToArray(),
            (byte)Math.Ceiling((double)client.Account.VaultSlots / VaultSize),
            (byte)((value.Page * VaultSize) - client.Account.VaultSlots).Clamp(0, byte.MaxValue)));
    }

    protected override void HandlePacket(Client client, in VaultRequest packet)
    {
        SearchHistory[client] = packet;
        RefreshClient(client);
    }
}