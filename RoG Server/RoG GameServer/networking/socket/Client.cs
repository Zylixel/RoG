﻿using System.Net;
using System.Net.Sockets;
using Database;
using Database.Models;
using GameServer.Realm;
using GameServer.Realm.Entities.Player;
using Microsoft.Extensions.Logging;
using RoGCore.Logging;
using RoGCore.Models;
using RoGCore.Networking;
using RoGCore.Networking.Packets;

namespace GameServer.Networking.Socket;

internal sealed partial class Client : IDisposable, IClient
{
    internal readonly IPEndPoint Endpoint;
    internal readonly IncomingNetworkHandler IncomingNetworkHandler;
    internal readonly OutgoingNetworkHandler OutgoingNetworkHandler;

    private static readonly ILogger<Client> Logger = LogFactory.LoggingInstance<Client>();

    internal Client(UdpClient udpClient, IPEndPoint remoteEndpoint)
    {
        Endpoint = remoteEndpoint;
        Account = new DbAccount(-1);
        IncomingNetworkHandler = new IncomingNetworkHandler(this);
        OutgoingNetworkHandler = new OutgoingNetworkHandler(udpClient, remoteEndpoint);
    }

    internal DbChar? Character { get; set; }

    internal DbAccount Account { get; set; }

    internal int Id { get; set; }

    // TODO: This can be done away with
    internal int TargetWorld { get; set; }

    internal Player? Player { get; set; }

    internal bool Disposed { get; set; }

    public void Disconnect(DisconnectReason disconnectReason)
    {
        Disconnect(disconnectReason, true);
    }

    public void Dispose()
    {
        if (Disposed)
        {
            return;
        }

        Player?.Dispose();
        Disposed = true;
    }

    public void ProcessPacket(in IPacket packet)
    {
        try
        {
            var packetHandler = PacketHandlers.Handlers[(int)packet.Id];
            if (packetHandler is not null)
            {
                packetHandler.Handle(this, packet);
            }
            else
            {
                Logger.LogWarning($"Unhandled packet '{packet.Id}'.");
            }
        }
        catch (Exception e)
        {
            Logger.LogError($"Error when handling packet... {e}");
            Disconnect(DisconnectReason.ErrorWhenHandlingPacket);
        }
    }

    internal void SendPacket<T>(in T msg)
        where T : IPacket
    {
        try
        {
            OutgoingNetworkHandler.OutgoingMessage(msg);
        }
        catch (SocketException)
        {
            Disconnect(DisconnectReason.SocketIsNotConnected);
        }
    }

    internal void SendPacket(IEnumerable<IPacket> msgs)
    {
        try
        {
            foreach (var message in msgs)
            {
                OutgoingNetworkHandler.OutgoingMessage(message);
            }
        }
        catch (SocketException)
        {
            Disconnect(DisconnectReason.SocketIsNotConnected);
        }
    }

    internal void Reconnect(in Reconnect msg, bool sendPacket)
    {
        if (Account is null)
        {
            SendPacket(new Failure("An unknown error occurred.", "Connection Lost"));
            Disconnect(DisconnectReason.LostConnection);
            return;
        }

        Save(SaveReason.Reconnect);
        Player?.Owner.LeaveWorld(Player);

        ReconnectLog(Account.AccountId, msg);

        if (sendPacket)
        {
            SendPacket(msg);
        }
    }

    internal void Save(SaveReason reason)
    {
        try
        {
            if (Account is null)
            {
                return;
            }

            Player?.SaveToCharacter();
            Player?.SaveToAccount();

            Account.Flush();
            Account.ReloadFields();

            if (Account != null && Character != null)
            {
                var saveResult = RedisDatabase.SaveCharacter(Account, Character, false);
                saveResult.Wait();
                if (!saveResult.Result)
                {
                    Logger.LogError($"[Save] [Failure] [{reason}] account id {Account.AccountId}");
                }
                else
                {
                    Logger.LogInformation($"[Save] [{reason}] account id {Account.AccountId}");
                }
            }
        }
        catch (Exception ex)
        {
            Logger.LogError($"[Save] exception:\n{ex}");
        }
    }

    internal void Disconnect(DisconnectReason disconnectReason, bool save)
    {
        if (Disposed || Account is null)
        {
            return;
        }

        DisconnectLog(Account.AccountId, disconnectReason);

        try
        {
            if (save)
            {
                Save(SaveReason.Disconnect);
            }

            RealmManager.Disconnect(this);
        }
        catch (Exception e)
        {
            Logger.LogError($"Disconnect exception:\n{e}");
        }
    }

    private static void ReconnectLog(int accId, Reconnect msg)
    {
        Logger.LogInformation($"Reconnect\t->\tplayer id {accId} to {msg.Name} {msg.GameId}");
    }

    private void DisconnectLog(int accId, DisconnectReason type)
    {
        var endpointString = Endpoint.ToString();
        if (endpointString is not null)
        {
            Logger.LogError($"[{type}] Disconnect\t->\tplayer id {accId} to {endpointString.Split(':')[0]}");
        }
        else
        {
            Logger.LogError($"[{type}] Disconnect\t->\tplayer id {accId}");
        }
    }
}