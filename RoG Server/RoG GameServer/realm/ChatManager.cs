﻿using Database;
using Database.InterServer.Messages;
using Database.Models;
using GameServer.Realm.Entities.Player;
using Microsoft.Extensions.Logging;
using RoGCore.Logging;
using RoGCore.Models;
using RoGCore.Networking.Packets;
using RoGCore.Utils;
using static Database.InterServer.InterServerChannel;

namespace GameServer.Realm;

internal class ChatManager
{
    private static readonly ILogger<ChatManager> Logger = LogFactory.LoggingInstance<ChatManager>();

    internal ChatManager()
    {
        RealmManager.InterServer.AddHandler<Chat>(CHAT, HandleChat);
    }

    internal static void Say(Player src, string text)
    {
        src.Owner.BroadcastPacket(new Text(
            src.Name,
            src.Id,
            text,
            src.GlowColor == RGB.Black ? RGB.White : src.GlowColor,
            RGB.White));

        Logger.LogInformation($"[{src.Owner.Name} ({src.Owner.Id})] <{src.Name}> {text}");
    }

    internal static void Tell(Player? player, string botName, string callback)
    {
        if (player is null || player.Client is null)
        {
            return;
        }

        player.Client.SendPacket(new Text(
            botName,
            callback.ToSafeText(),
            new RGB(0x0080FF),
            new RGB(0x0080FF),
            player.Name));
    }

    // TODO
    internal void Announce(string text)
    {
        RealmManager.InterServer.Publish(CHAT, new Chat(Chat.ChatType.Announce, Program.AppSettings.GameServer.Name, 0, 0, string.Empty, string.Empty, text));
    }

    internal void PlayerAnnounce(string text)
    {
        RealmManager.InterServer.Publish(CHAT, new Chat(Chat.ChatType.Announce, string.Empty, 0, 0, string.Empty, string.Empty, text));
    }

    private async void HandleChat(object? sender, InterServerEventArgs<Chat> e)
    {
        switch (e.Content.Type)
        {
            case Chat.ChatType.Tell:
                {
                    var from = await RedisDatabase.ResolveIgn(e.Content.From);
                    var to = await RedisDatabase.ResolveIgn(e.Content.To);
                    foreach (var i in RealmManager.Clients.Values
                        .Where(x => x.Player != null && (x.Account.Name == from ||
                                                         x.Account.Name == to))
                        .Select(x => x.Player))
                    {
                        // i.TellReceived(
                        //    e.Content.Inst == manager.InstanceId ? e.Content.ObjId : -1,
                        //    e.Content.Stars, from, to, e.Content.Text);
                    }
                }

                break;
            case Chat.ChatType.Guild:
                {
                    var from = await RedisDatabase.ResolveIgn(e.Content.From);
                    foreach (var i in RealmManager.Clients.Values
                        .Where(x => x.Player != null && x.Account.GuildId.ToString() == e.Content.To)
                        .Select(x => x.Player))
                    {
                        // i.GuildReceived(
                        //     e.Content.Inst == manager.InstanceId ? e.Content.ObjId : -1,
                        //     e.Content.Stars, from, e.Content.Text);
                    }
                }

                break;
            case Chat.ChatType.Announce:
                {
                    foreach (var i in RealmManager.Clients.Values.Where(x => x.Player != null))
                    {
                        i.SendPacket(new Text(
                            "@ANNOUNCEMENT",
                            e.Content.Text,
                            new RGB(0xFFFF00),
                            new RGB(0xFFFF00)));
                    }
                }

                break;
        }
    }
}