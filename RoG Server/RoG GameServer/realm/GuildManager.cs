﻿using Database;
using Database.Models;
using GameServer.Realm.Entities.Player;
using GameServer.Realm.World;
using RoGCore.Networking.Packets;

namespace GameServer.Realm;

internal class GuildManager : List<Player>
{
    private readonly Dictionary<int, DbGuild> guildStructs = [];

    internal GuildManager(DbGuild guildInfo)
    {
        Guild = guildInfo;

        // GuildHall = guildInfo is null ? null : manager.AddWorld(new GuildHall(this));
    }

    internal static Dictionary<string, GuildManager> CurrentManagers { get; } = [];

    internal DbGuild Guild { get; }

    internal World.World? GuildHall { get; }

    public static void SendGuild(DbGuild to, string text)
    {
        foreach (var client in RealmManager.Clients.Values.Where(x =>
            x.Player?.GuildManager != null && x.Player.GuildManager.Guild.Name == to.Name))
        {
            client.SendPacket(new Text(
                text,
                new RoGCore.Models.RGB(0x0a9900),
                "*Guild*"));
        }
    }

    internal static GuildManager? Add(Player? player, DbGuild? guildStruct)
    {
        if (guildStruct is null || player?.Client is null)
        {
            return null;
        }

        if (CurrentManagers.TryGetValue(guildStruct.Name, out var guildManager))
        {
            guildManager.guildStructs[player.Client.Account.AccountId] = guildStruct;
            guildManager.Add(player);
        }
        else
        {
            guildManager = new GuildManager(guildStruct);
            guildManager.guildStructs.Add(player.Client.Account.AccountId, guildStruct);
            guildManager.Add(player);
            CurrentManagers.Add(guildStruct.Name, guildManager);
        }

        return guildManager;
    }

    internal static void GiftGuildCourage(DbGuild guild)
    {
        foreach (var accId in guild.Members)
        {
            var acc = RedisDatabase.GetAccount(accId);
            if (acc != null)
            {
                RedisDatabase.UpdateSilver(acc, 1);
            }
        }
    }

    internal void CheckWarEnd()
    {
        var war = RedisDatabase.GetGuildWar(Guild.WarId);
        if (war is null || war.Expiration > DateTime.Now)
        {
            return;
        }

        DbGuild? otherGuild;
        int thisFame;
        int otherFame;

        // Figures out the other guild
        if (war.ChallengingGuild == Guild.Id)
        {
            thisFame = war.ChallengingGuildPoints;
            otherGuild = RedisDatabase.GetGuild(war.ChallengedGuild);
            otherFame = war.ChallengedGuildPoints;
        }
        else
        {
            thisFame = war.ChallengedGuildPoints;
            otherGuild = RedisDatabase.GetGuild(war.ChallengingGuild);
            otherFame = war.ChallengingGuildPoints;
        }

        if (otherGuild is null)
        {
            var otherName = otherGuild?.Name ?? "a disbanded guild";
            SendGuild(Guild, $"Your Guild has won the war against {otherName}");
            if (otherGuild != null)
            {
                SendGuild(otherGuild, $"Your Guild has lost the war against {Guild.Name}");
            }
        }
        else if (thisFame > otherFame)
        {
            SendGuild(Guild, $"Your Guild has won the war against {otherGuild.Name}");
            SendGuild(otherGuild, $"Your Guild has lost the war against {Guild.Name}");
        }
        else if (thisFame < otherFame)
        {
            SendGuild(Guild, $"Your Guild has lost the war against {otherGuild.Name}");
            SendGuild(otherGuild, $"Your Guild has won the war against {Guild.Name}");
        }
        else
        {
            SendGuild(Guild, $"Your Guild has tied in the war against {otherGuild.Name}");
            SendGuild(otherGuild, $"Your Guild has tied in the war against {Guild.Name}");
        }

        RedisDatabase.EndGuildWar(war);
    }

    internal void Remove(int accountId)
    {
        guildStructs.Remove(accountId);
        RemoveAt(FindIndex(_ => _.Client != null && _.Client.Account.AccountId == accountId));
    }

    internal void Leave(DbAccount acc)
    {
        RedisDatabase.RemoveFromGuild(acc);
        var player = RealmManager.FindPlayer(acc.Name);
        if (player != null)
        {
            player.GuildManager = null;
            player.Client.Account.ReloadFields();
        }

        Remove(acc.AccountId);
    }

    internal void UpdateGuildHall()
    {
        WorldTimer ghallTimer = new(1000, (_, _) => { });
        ghallTimer = new WorldTimer(60 * 1000, (w, _) =>
        {
            if (GuildHall is null)
            {
                return;
            }

            if (w.Players.Count > 0)
            {
                ghallTimer.Reset();
                w.Timers.Add(ghallTimer);
            }
            else
            {
                RealmManager.RemoveWorld(GuildHall);

                // GuildHall = manager.AddWorld(new GuildHall(this))
            }
        });
        GuildHall?.Timers.Add(ghallTimer);
    }

    internal void Chat(Player sender, string text)
    {
        foreach (var p in this)
        {
            p.Client?.SendPacket(new Text(
                sender.Name,
                p.Owner == sender.Owner ? sender.Id : -1,
                text,
                new RoGCore.Models.RGB(0x0a9900),
                new RoGCore.Models.RGB(0x0a9900),
                "*Guild*"));
        }
    }
}