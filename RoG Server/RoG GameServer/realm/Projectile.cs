﻿using System.Diagnostics;
using System.Numerics;
using GameServer.Realm.Entities;
using GameServer.Realm.Entities.Enemy;
using GameServer.Realm.Entities.Models;
using GameServer.Realm.Entities.Player;
using GameServer.Realm.World;
using RoGCore.Assets;
using RoGCore.Game;
using RoGCore.Models;
using RoGCore.Networking.Packets;
using RoGCore.Utils;

namespace GameServer.Realm;

public sealed class Projectile : IProjectile
{
    internal readonly HashSet<Entity> HitEntities = [];

    internal IProjectileOwner Owner;
    internal DateTime BeginTime;
    internal Vector2 BeginPosition;
    internal Vector2 Position;
    internal Item? From;

    private const int UPDATEINTERVALMILLISECOND = 40;
    private const float HITRADIUS = 0.5f;

    private readonly HashSet<string> hitGroups;
    private readonly bool hitsEnemies;
    private readonly bool hitsAllEnemies;
    private readonly World.World world;

    private float positionTime;

    internal Projectile(
        CustomProjectile customDesc,
        IProjectileOwner owner,
        IReadOnlyList<string>? targetGroups,
        ushort damage,
        Angle angle,
        World.World world,
        ushort id,
        Item? from)
    {
        Owner = owner;
        CustomDesc = customDesc;
        Damage = damage;
        Angle = angle;
        this.world = world;
        Id = id;
        From = from;

        if (Owner.Self is Player player)
        {
            Lifetime = CustomDesc.GetLifetime(from?.BaseItem, player.SkillTree.CurrentSkills);
            Speed = CustomDesc.GetSpeed(from?.BaseItem, player.SkillTree.CurrentSkills);
        }
        else
        {
            Lifetime = CustomDesc.GetLifetime(from?.BaseItem, null);
            Speed = CustomDesc.GetSpeed(from?.BaseItem, null);
        }

        hitGroups = new HashSet<string>(targetGroups?.Count ?? 1);
        if (targetGroups != null)
        {
            for (var i = 0; i < targetGroups.Count; i++)
            {
                hitGroups.Add(targetGroups[i]);
            }
        }
        else
        {
            switch (owner.Self)
            {
                case Player:
                    hitGroups.Add("AllEnemy");
                    break;
                case Enemy:
                    hitGroups.Add("Player");
                    break;
            }
        }

        hitsAllEnemies = hitGroups.Any(x => x == "AllEnemy");
        hitsEnemies = hitGroups.Any(x => x != "Player");
    }

    public ushort Damage { get; }

    public ushort Id { get; }

    public Angle Angle { get; }

    public CustomProjectile CustomDesc { get; }

    UnityEngine.Vector2 IProjectile.BeginPosition => BeginPosition.ToUnity2();

    public float OrbitThetaChange { get; set; }

    public float RenderedTheta { get; set; }

    public float Speed { get; private set; }

    public float Lifetime { get; private set; }

    internal bool Destroyed { get; private set; }

    internal static bool HittingEntity(Vector2 pos, Entity entity)
    {
        var dist = Vector2.Abs(entity.Position - pos);
        return dist.X <= entity.HitRadius && dist.Y <= entity.HitRadius;
    }

    internal static bool HittingEntity(Vector2 pos, Entity entity, Vector2 positionOverride, float sizeMult = 1)
    {
        var hitRadius = entity.HitRadius * sizeMult;
        var dist = Vector2.Abs(positionOverride - pos);
        return dist.X <= hitRadius && dist.Y <= hitRadius;
    }

    internal static float GetHitBox(float size)
    {
        return HITRADIUS * MathsUtils.Clamp(size * 0.01f, 1f, float.MaxValue);
    }

    internal void Destroy()
    {
        if (Owner.Projectiles is not null)
        {
            Owner.Projectiles[Id] = null;
        }

        Destroyed = true;
    }

    internal void Tick()
    {
        var elapsedTime = (float)(DateTime.UtcNow - BeginTime).TotalMilliseconds;

        Debug.Assert(!Destroyed, "Projectile is being processed after destruction.");

        if (elapsedTime > Lifetime || OrbitThetaChange > MathF.PI * 2f)
        {
            Destroy();
            return;
        }

        while (positionTime < elapsedTime && world is not null)
        {
            // Ensure projectile traces through the minimum amount for precision
            positionTime += Math.Clamp(elapsedTime - positionTime, 0, UPDATEINTERVALMILLISECOND);
            positionTime = Math.Clamp(positionTime, 0, elapsedTime);
            UpdatePosition();
        }
    }

    internal bool CanHitEntity(Entity entity)
    {
        return !HitEntities.Contains(entity)
&& hitsEnemies &&
                 entity is Enemy &&
                 (hitsAllEnemies || (entity.ObjectDescription.Group is not null && hitGroups.Contains(entity.ObjectDescription.Group)));
    }

    internal void Hit(Entity entity)
    {
        HitEntities.Add(entity);

        if (entity.HitByProjectile(this) && !CustomDesc.MultiHit)
        {
            Destroy();
        }
    }

    private void UpdatePosition()
    {
        Position = this.GetPosition(positionTime).Convert();

        if (!world.RegionUnblocked(Position, TileOccupency.OccupyFull))
        {
            TryHitTile();
            Destroy();
            return;
        }

        // Convert to list because the entities could change depending on if one dies
        var entities = world.EntitiesInRectangle(
            new Rectangle(
            Position.X - world.LargestEntityHitbox,
            Position.Y - world.LargestEntityHitbox,
            world.LargestEntityHitbox * 2,
            world.LargestEntityHitbox * 2)).ToList();

        foreach (var entity in entities)
        {
            if (HittingEntity(Position, entity) && CanHitEntity(entity))
            {
                Hit(entity);
            }

            if (Destroyed)
            {
                break;
            }
        }
    }

    private void TryHitTile()
    {
        foreach (var collision in Position.Collision())
        {
            if (!world.TryGetTile(collision, out var positionalTile) || positionalTile!.ObjDesc is null || positionalTile.ObjDesc.MaxHitPoints <= 0)
            {
                continue;
            }

            positionalTile.Hp -= (short)Damage;
            if (positionalTile.Hp <= 0)
            {
                positionalTile.ObjDesc = null;
                if (world.Dungeon)
                {
                    foreach (var player in world.Players)
                    {
                        player.Sight.Update();
                    }
                }
            }

            world.BroadcastPacket(new ObjectDamage(collision, Damage));
        }
    }
}