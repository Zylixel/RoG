﻿using GameServer.Realm.World.Worlds;

namespace GameServer.Realm;

internal static class RandomExtensions
{
    internal static int Next(this Random random, RandomRange range)
    {
        return random.Next(range.Min, range.Max + 1);
    }
}