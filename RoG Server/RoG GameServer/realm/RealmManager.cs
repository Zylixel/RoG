﻿using System.Collections.Concurrent;
using System.Timers;
using Database.Models;
using FluentResults;
using GameServer.Logic.Engine;
using GameServer.Networking.Socket;
using GameServer.Realm.Commands;
using GameServer.Realm.Entities.Player;
using GameServer.Realm.Networking;
using GameServer.Realm.World;
using GameServer.Realm.World.DungeonGeneration;
using GameServer.Realm.World.Worlds;
using Microsoft.Extensions.Logging;
using RoGCore.Assets;
using RoGCore.Logging;
using RoGCore.Models;
using RoGCore.Networking;
using RoGCore.SettingsModels;
using Timer = System.Timers.Timer;

namespace GameServer.Realm;

internal static class RealmManager
{
    internal const int MAXREALMPLAYERS = 85;

    internal static readonly List<string> CurrentRealmNames = [];
    internal static readonly ConcurrentDictionary<int, Client> Clients = [];
    internal static readonly Dictionary<int, World.World> Worlds = [];
    internal static readonly ConcurrentDictionary<int, World.World> LastWorld = [];

    private static readonly ILogger Logger = LogFactory.LoggingInstance(nameof(RealmManager));

    // TODO: Move this logic elsewhere
    private static readonly Timer AntiDupeTimer = new(10 * 60 * 1000);
    private static int nextClientId;
    private static int nextWorldId;

    static RealmManager()
    {
        AntiDupe = new AntiDupe();
        AntiDupeTimer.Elapsed += FindDupes;
        AntiDupeTimer.AutoReset = true;
        AntiDupeTimer.Enabled = true;

        BehaviorDb.Load();
        CommandManager.Load();

        Packet.InitPackets();
        PacketHandlers.InitalizeHandlers();

        GenerationPiece.Initialize();
        Sight.Initialize();
        WmapData.Initialize();
        SkillTree.Initialize();
        RandomWorldPool.Initalize();
        Nexus nexus = new();
        AddWorld(World.World.NEXUSID, nexus);
        RealmPortalMonitor.Initialize(nexus);
        var realm = new World.Worlds.Realm();
        AddWorld(realm);

        InterServer = new GameServerInterServerManager();
        Chat = new ChatManager();
        EmbeddedData.ReleaseFiles();
        Logger.LogDebug("Realm Manager initialized.");

        Logger.LogDebug("Starting Realm Manager...");
        Task logic = new(LogicTicker.TickLoop, TaskCreationOptions.LongRunning);
        logic.ContinueWith(Shutdown.Stop, TaskContinuationOptions.OnlyOnFaulted);
        logic.Start();
        Logger.LogDebug("Realm Manager started.");
    }

    internal static ChatManager Chat { get; }

    internal static GameServerInterServerManager InterServer { get; }

    internal static AntiDupe AntiDupe { get; }

    internal static bool Terminating { get; private set; }

    internal static World.World AddWorld(World.World world)
    {
        world.Id = (short)Interlocked.Increment(ref nextWorldId);
        Worlds.TryAdd(world.Id, world);
        OnWorldAdded(world);
        return world;
    }

    internal static void Disconnect(Client client)
    {
        client.Player?.GuildManager?.Remove(client.Account.AccountId);
        Clients.TryRemove(client.Id, out _);
        client.Dispose();
    }

    internal static Player? FindPlayer(string name)
    {
        return (from i in Worlds
                from e in i.Value.Players
                where string.Equals(e.Client?.Account?.Name, name, StringComparison.CurrentCultureIgnoreCase)
                select e).FirstOrDefault();
    }

    internal static World.World? GetWorld(int id)
    {
        return Worlds.TryGetValue(id, out var ret) ? ret : null;
    }

    internal static void RemoveWorld(World.World world)
    {
        if (!Worlds.Remove(world.Id, out _))
        {
            return;
        }

        OnWorldRemoved(world);
    }

    internal static void Stop()
    {
        Logger.LogInformation("Disconnecting InterServerManager...");
        InterServer.Dispose();
        Logger.LogInformation("InterServerManager Disconnected.");

        Logger.LogInformation("Stopping Realm Manager...");
        Terminating = true;
        foreach (var c in Clients.Values)
        {
            c.Disconnect(DisconnectReason.StoppingRealmManager);
        }

        AntiDupe.ThreadRipThruDupes();
        Logger.LogInformation("Realm Manager stopped.");
    }

    internal static Result TryAcceptNewClient(Client client)
    {
        if (Clients.Count >= NetworkingSettings.MAXCONNECTIONS)
        {
            return Result.Fail($"[Server: {NetworkingSettings.MAXCONNECTIONS}/{NetworkingSettings.MAXCONNECTIONS} players] Server is <b>full</b> at this moment. Try again later.");
        }

        client.Id = Interlocked.Increment(ref nextClientId);
        DisconnectAccount(client.Account.AccountId, client.Id);
        return Result.FailIf(!Clients.TryAdd(client.Id, client), "Unknown Error");
    }

    private static void DisconnectAccount(int accountId, int ignoreClientId)
    {
        foreach (var c in Clients.Values.Where(x =>
            x.Account.AccountId == accountId && x.Id != ignoreClientId))
        {
            c.Disconnect(DisconnectReason.NewLocation);
        }
    }

    private static void FindDupes(object? source, ElapsedEventArgs? e)
    {
        if (!Clients.IsEmpty)
        {
            return;
        }

        AntiDupe.ThreadRipThruDupes();

        foreach (var acc in AntiDupe.BadAccounts)
        {
            var p = FindPlayer(acc.Name);
            p?.Client.Disconnect(DisconnectReason.DuperDisconnect, false);
        }
    }

    private static void AddWorld(short id, World.World world)
    {
        world.Id = id;
        Worlds.TryAdd(id, world);
        OnWorldAdded(world);
    }

    // TODO: Change this into an event, so listeners can decide what to do
    private static void OnWorldAdded(World.World world)
    {
        world.Initialize();

        if (world is World.Worlds.Realm)
        {
            RealmPortalMonitor.WorldAdded(world);
        }

        LogicTicker.WorldListChanged();
        Logger.LogInformation($"World {world.Id}({world.Name}) added.");
    }

    private static void OnWorldRemoved(World.World world)
    {
        if (world is World.Worlds.Realm)
        {
            RealmPortalMonitor.WorldRemoved(world);
        }

        LogicTicker.WorldListChanged();
        Logger.LogInformation($"World {world.Id}({world.Name}) removed.");
    }
}