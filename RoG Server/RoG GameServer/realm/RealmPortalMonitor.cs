﻿using System.Numerics;
using GameServer.Realm.Entities.GameObject;
using GameServer.Realm.World.Worlds;
using Microsoft.Extensions.Logging;
using RoGCore.Logging;

namespace GameServer.Realm;

internal static class RealmPortalMonitor
{
    internal static Dictionary<World.World, Portal> Portals = [];

    private static readonly ILogger Logger = LogFactory.LoggingInstance(nameof(RealmPortalMonitor));

    // TODO: idk whats going on with WorldLock
    private static readonly object WorldLock = new();

    // TODO: Do we really need to store a reference to the nexus here?
    // Maybe nexus can be a singleton
    private static Nexus nexus = null!;

    internal static void Initialize(Nexus nexus)
    {
        Logger.LogDebug("Initalizing Portal Monitor...");
        RealmPortalMonitor.nexus = nexus;

        lock (WorldLock)
        {
            foreach (var world in RealmManager.Worlds.Values)
            {
                if (world is World.Worlds.Realm)
                {
                    WorldAdded(world);
                }
            }
        }

        Logger.LogDebug("Portal Monitor initialized.");
    }

    internal static void WorldAdded(World.World world)
    {
        lock (WorldLock)
        {
            RealmManager.CurrentRealmNames.Add(world.Name);
            var pos = GetRealmPortalPosition();
            Portal portal = new("Gilgor's Realm Portal", 0, nexus)
            {
                RealmPortal = true,
                WorldInstance = world,
                Name = world.Name,
            };
            portal.Move(pos.X + 0.5f, pos.Y + 0.5f);

            Portals.TryAdd(world, portal);
            Logger.LogInformation($"World {world.Id}({world.Name}) added to monitor.");
        }
    }

    internal static void WorldRemoved(World.World world)
    {
        lock (WorldLock)
        {
            if (!Portals.TryGetValue(world, out var portal))
            {
                return;
            }

            nexus.LeaveWorld(portal);
            RealmManager.CurrentRealmNames.Remove(portal.PortalName);
            Portals.Remove(world, out _);
            Logger.LogInformation($"World {world.Id}({world.Name}) removed from monitor.");
        }
    }

    internal static World.World GetRealm()
    {
        lock (WorldLock)
        {
            var world = Portals.Keys.FirstOrDefault(x => x is World.Worlds.Realm);
            return world ?? RealmManager.Worlds[World.World.NEXUSID];
        }
    }

    private static Vector2 GetRealmPortalPosition()
    {
        lock (WorldLock)
        {
            Vector2 position;
            do
            {
                position = nexus.Map.RealmPortalSpawnPoints[Random.Shared.Next(0, nexus.Map.RealmPortalSpawnPoints.Count)];
            }
            while (Portals.Values.Any(_ => _.X == position.X && _.Y == position.Y));

            return position;
        }
    }
}