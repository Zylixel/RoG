﻿using System.Diagnostics;
using GameServer.Realm.Entities.Player;
using GameServer.Realm.Terrain.Chunk;
using GameServer.Realm.Terrain.Tile;
using Microsoft.Extensions.Logging;
using RoGCore.Logging;
using RoGCore.Models;
using RoGCore.Vector;

namespace GameServer.Realm;

internal class Sight
{
    // blocked line of sight vars
    private const float STARTANGLE = 0;
    private const float ENDANGLE = (float)(2 * Math.PI);
    private const float RAYSTEPSIZE = .5f;
    private const float ANGLESTEPSIZE = 0.05f;

    private static readonly object InitLock = new();
    private static readonly ILogger<Sight> Logger = LogFactory.LoggingInstance<Sight>();

    private static IntVector2S[] sightRays = [];
    private static int rayPoints;

    private static IntVector2S unblockedView;
    private static Rectangle unblockedViewRect;
    private static IntVector2S structureGenerationCircle;

    private readonly Rectangle currentUnblockedViewRect;
    private readonly Player player;

    private bool update = true;
    private IntVector2S currentUnblockedView;
    private Vector2Int currentUnblockedViewOrigin;
    private IntVector2S currentStructureCircle;
    private Vector2Int currentStructureCircleOrigin;

    private IntVector2S cachedSight;

    internal Sight(Player player)
    {
        this.player = player;
        cachedSight = new IntVector2S(0);
        lock (InitLock)
        {
            currentUnblockedView = unblockedView.GetCopy();
            currentUnblockedViewOrigin = Vector2Int.Zero;
            currentUnblockedViewRect = unblockedViewRect;
            currentStructureCircle = structureGenerationCircle.GetCopy();
            currentStructureCircleOrigin = Vector2Int.Zero;
        }
    }

    internal static void Initialize()
    {
        structureGenerationCircle = CreateViewCircle(Program.AppSettings.GameServer.StructureRadius);
        InitSightRays();
        unblockedView = ViewRectangle(Program.AppSettings.GameServer.Sight.RectangleSize, out unblockedViewRect);
        rayPoints = sightRays.Length * sightRays.FirstOrDefault().Count;
        Logger.LogResults("UnblockedViewTiles", unblockedView.Count);
        Logger.LogResults("StructureGenerationTiles", structureGenerationCircle.Count);
        Logger.LogResults("SightRays", sightRays.Length);
        Logger.LogResults("SightRayPoints", rayPoints);
    }

    internal static bool IsBlocking(WmapTile tile)
    {
        return tile.ObjDesc?.BlocksSight == true;
    }

    internal Rectangle? GetRect()
    {
        return player.Owner.Dungeon
            ? null
            : new Rectangle(currentUnblockedViewRect.X + player.IntPosition.X, currentUnblockedViewRect.Y + player.IntPosition.Y, currentUnblockedViewRect.Width, currentUnblockedViewRect.Height);
    }

    internal IntVector2S GetSight()
    {
        if (!update)
        {
            return cachedSight;
        }

        var owner = player.Owner;
        if (owner is not World.Worlds.Realm)
        {
            update = false;
        }

        if (!owner.Dungeon)
        {
            UpdateUnblockedView();
            cachedSight = currentUnblockedView;
            return currentUnblockedView;
        }

        cachedSight = CalcBlockedLineOfSight(owner);

        return cachedSight;
    }

    internal IntVector2S GetStructureCircle(bool force = false)
    {
        if (!update && !force)
        {
            return new IntVector2S(0);
        }

        update = false;
        var playerChunkPosition = Chunk.ChunkPosition(player.Position);
        var difference = playerChunkPosition - currentStructureCircleOrigin;
        currentStructureCircle.Add(difference, ref currentStructureCircle);
        currentStructureCircleOrigin = playerChunkPosition;
        return currentStructureCircle;
    }

    internal void Update()
    {
        update = true;
    }

    private static IntVector2S CreateViewCircle(int radius)
    {
        var radiusSqr = radius * radius;
        var x = 0;
        var y = 0;
        var i = 1;
        var j = 2;
        HashSet<Vector2Int> viewCircle = [new Vector2Int(0, 0)];
        do
        {
            ViewCircleTryAdd(++x, y, radiusSqr, ref viewCircle);
            for (var k = 0; k < i; k++)
            {
                ViewCircleTryAdd(x, --y, radiusSqr, ref viewCircle);
            }

            for (var k = 0; k < j; k++)
            {
                ViewCircleTryAdd(--x, y, radiusSqr, ref viewCircle);
            }

            for (var k = 0; k < j; k++)
            {
                ViewCircleTryAdd(x, ++y, radiusSqr, ref viewCircle);
            }

            for (var k = 0; k < j; k++)
            {
                ViewCircleTryAdd(++x, y, radiusSqr, ref viewCircle);
            }

            i += 2;
            j += 2;
        }
        while (j <= 2 * radius);

        IntVector2S ret = new(viewCircle.Count);
        for (i = 0; i < ret.Count; i++)
        {
            ret[i] = viewCircle.ElementAt(i);
        }

        return ret;
    }

    private static IntVector2S ViewRectangle(int size, out Rectangle rect)
    {
        Debug.Assert(size > 0, "Size must be a positive integer");
        Debug.Assert(size % 2 == 0, "Size must be an even integer");

        rect = new(-size * 0.5f, -size * 0.5f, size, size);
        var viewRect = new Vector2Int[(int)(rect.Width * rect.Height)];

        var i = 0;
        for (var x = (int)rect.X; x < (int)rect.Right; x++)
        {
            for (var y = (int)rect.Y; y < (int)rect.Bottom; y++)
            {
                viewRect[i] = new Vector2Int(x, y);
                i++;
            }
        }

        IntVector2S ret = new(viewRect.Length);
        for (i = 0; i < ret.Count; i++)
        {
            ret[i] = viewRect[i];
        }

        return ret;
    }

    private static void ViewCircleTryAdd(int x, int y, int maxDistSqr, ref HashSet<Vector2Int> generationCircle)
    {
        if ((x * x) + (y * y) <= maxDistSqr)
        {
            generationCircle.Add(new Vector2Int(x, y));
        }
    }

    private static void InitSightRays()
    {
        var sightRays = new List<IntVector2S>();

        var currentAngle = STARTANGLE;
        while (currentAngle < ENDANGLE)
        {
            var raySize = (int)Math.Floor((Program.AppSettings.GameServer.Sight.RectangleSize * 0.5f) - 1);
            HashSet<Vector2Int> ray = new(raySize);
            var dist = RAYSTEPSIZE;
            while (dist < raySize)
            {
                Vector2Int point = new(
                    (int)(dist * Math.Cos(currentAngle)),
                    (int)(dist * Math.Sin(currentAngle)));
                ray.Add(point);

                dist += RAYSTEPSIZE;
            }

            IntVector2S points = new(ray.Count);
            var i = 0;
            foreach (var point in ray)
            {
                points[i] = point;
                i++;
            }

            sightRays.Add(points);
            currentAngle += ANGLESTEPSIZE;
        }

        Sight.sightRays = [.. sightRays];
    }

    private void UpdateUnblockedView()
    {
        var difference = player.IntPosition - currentUnblockedViewOrigin;
        currentUnblockedView.Add(difference, ref currentUnblockedView);
        currentUnblockedViewOrigin = player.IntPosition;
    }

    private IntVector2S CalcBlockedLineOfSight(World.World world)
    {
        // Enforces that each point added to the return value is unique
        HashSet<Vector2Int> distinctHash = new(rayPoints);
        IntVector2S ret = new(rayPoints);
        var i = 0;

        void AddPoint(Vector2Int point)
        {
            if (!distinctHash.Add(point))
            {
                return;
            }

            ret[i] = point;
            i++;
        }

        for (var j = 0; j < sightRays.Length; j++)
        {
            var ray = sightRays[j];
            IntVector2S rayAddition = new(ray.Count);
            ray.Add(player.IntPosition, ref rayAddition);
            foreach (var point in rayAddition)
            {
                AddPoint(point);
                if (!world.TryGetTile(point, out var tile))
                {
                    break;
                }

                if (IsBlocking(tile!))
                {
                    break;
                }

                IntVector2S points = new(CardinalOrdinalDirections.SurroundingArea.Count);
                CardinalOrdinalDirections.SurroundingArea.Add(point, ref points);
                for (var k = 0; k < points.Count; k++)
                {
                    AddPoint(points[k]);
                }
            }
        }

        // Remove the excess empty points at the end of the array.
        ret.Resize(i);
        return ret;
    }
}