﻿using GameServer.Realm.Entities;
using GameServer.Realm.Entities.Player;
using RoGCore.Models;

namespace GameServer.Realm;

internal static class StatsManager
{
    private const float MinAttackFrequency = 0.0015f;
    private const float MaxAttackFrequency = 0.008f;

    internal static int GetStats(this Player player, int id)
    {
        return player.Stats[id] + player.Boosts[id] + player.SkillTreeStats[id];
    }

    internal static int GetAttackDamage(this Player player, float damage)
    {
        return (int)(damage * player.DamageModifier());
    }

    internal static float GetAttackFrequency(this Player player)
    {
        if (player.HasConditionEffect(ConditionEffects.Dazed))
        {
            return MinAttackFrequency;
        }

        var rof = MinAttackFrequency + (player.GetStats(7) / 75f * (MaxAttackFrequency - MinAttackFrequency));
        if (player.HasConditionEffect(ConditionEffects.Berserk))
        {
            rof *= 1.5f;
        }

        return rof;
    }

    internal static float GetDefenseDamage(this Entity host, int dmg, bool ignoreDef)
    {
        if (host.HasConditionEffect(ConditionEffectIndex.Invulnerable) ||
            host.HasConditionEffect(ConditionEffectIndex.Invincible))
        {
            return 0;
        }

        var def = ignoreDef ? 0 : host.GetDefense();

        if (host.HasConditionEffect(ConditionEffectIndex.Exposed))
        {
            def -= 20;
        }

        if (host.HasConditionEffect(ConditionEffectIndex.Armored))
        {
            def *= 2;
        }

        if (host.HasConditionEffect(ConditionEffectIndex.ArmorBroken))
        {
            def = 0;
        }

        var limit = dmg * 0.15f;
        var ret = dmg - def < limit ? limit : dmg - def;
        if (host.HasConditionEffect(ConditionEffectIndex.Curse))
        {
            ret *= 1.2f;
        }

        return ret;
    }

    internal static ushort GetDefenseDamage(this Player player, int dmg, bool noDef)
    {
        var def = player.GetStats(3);
        if (player.HasConditionEffect(ConditionEffectIndex.Exposed))
        {
            def -= 20;
        }

        if (player.HasConditionEffect(ConditionEffectIndex.Armored))
        {
            def *= 2;
        }

        if (player.HasConditionEffect(ConditionEffectIndex.ArmorBroken) ||
            noDef)
        {
            def = 0;
        }

        var limit = dmg * 0.15f;
        var ret = dmg - def < limit ? limit : dmg - def;
        if (player.HasConditionEffect(ConditionEffectIndex.Curse))
        {
            ret *= 1.2f;
        }

        if (player.HasConditionEffect(ConditionEffectIndex.Invulnerable) ||
            player.HasConditionEffect(ConditionEffectIndex.Invincible))
        {
            ret = 0;
        }

        return (ushort)ret;
    }

    internal static float GetSpeed(this Entity entity, float stat)
    {
        var ret = 8 + (stat / 10f);
        if (entity.HasConditionEffect(ConditionEffectIndex.Speedy))
        {
            ret *= 1.5f;
        }

        if (entity.HasConditionEffect(ConditionEffectIndex.Slowed))
        {
            ret = (8 + (stat / 10f)) / 4;
        }

        if (entity.HasConditionEffect(ConditionEffectIndex.Paralyzed))
        {
            ret = 0;
        }

        return ret;
    }

    internal static float GetSpeed(this Player player)
    {
        return GetSpeed(player, player.GetStats(4));
    }

    internal static float GetHpRegen(this Player player)
    {
        var vit = player.GetStats(5);
        if (player.HasConditionEffect(ConditionEffectIndex.Sick))
        {
            vit = 0;
        }
        else if (player.HasConditionEffect(ConditionEffectIndex.Recovering))
        {
            vit *= Program.AppSettings.GameServer.Combat.OutOfCombatVitalityMultiplier;
        }

        return 1 + (0.24f * vit);
    }

    internal static float GetMpRegen(this Player player)
    {
        var wis = player.GetStats(6);
        if (player.HasConditionEffect(ConditionEffectIndex.Quiet))
        {
            return 0;
        }

        if (player.HasConditionEffect(ConditionEffectIndex.Recovering))
        {
            wis *= Program.AppSettings.GameServer.Combat.OutOfCombatWisdomMultiplier;
        }

        return 0.25f + (0.24f * wis);
    }

    internal static float GetDex(this Player player)
    {
        var dex = player.GetStats(7);
        if (player.HasConditionEffect(ConditionEffectIndex.Dazed))
        {
            dex = 0;
        }

        var ret = 1.5f + (6.5f * (dex / 75f));
        if (player.HasConditionEffect(ConditionEffectIndex.Berserk))
        {
            ret *= 1.5f;
        }

        if (player.HasConditionEffect(ConditionEffectIndex.Stunned))
        {
            ret = 0;
        }

        return ret;
    }

    private static float DamageModifier(this Player player)
    {
        if (player.HasConditionEffect(ConditionEffectIndex.Weak))
        {
            return 0.5f;
        }

        var ret = 0.5f + (player.GetStats(2) / 75F * (2 - 0.5f));
        if (player.HasConditionEffect(ConditionEffectIndex.Damaging))
        {
            ret *= 1.5f;
        }

        return ret;
    }
}