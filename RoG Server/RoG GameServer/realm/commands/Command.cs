﻿using GameServer.Realm.Entities.Player;
using Microsoft.Extensions.Logging;
using RoGCore.Logging;
using RoGCore.Models;

namespace GameServer.Realm.Commands;

internal abstract class Command
{
    private static readonly ILogger<Command> Logger = LogFactory.LoggingInstance<Command>();

    protected Command(string name, AccountType.Rank permLevel = AccountType.Rank.Regular)
    {
        CommandNames = [name];
        PermissionLevel = permLevel;
    }

    protected Command(params string[] names)
    {
        CommandNames = names;
    }

    internal string[] CommandNames { get; init; }

    internal AccountType.Rank PermissionLevel { get; init; } = AccountType.Rank.Regular;

    internal abstract CommandResult Process(Player player, RealmTime time, string[] args);

    internal bool Execute(Player player, RealmTime time, string args)
    {
        if (!HasPermission(player))
        {
            player.SendInfo($"Invalid Permissions for {CommandNames[0]}.");
            return false;
        }

        try
        {
            var a = args.Split(' ');
            var result = Process(player, time, a);
            if (!string.IsNullOrWhiteSpace(result.Message))
            {
                if (result.IsSuccess)
                {
                    player.SendInfo(result.Message);
                }
                else
                {
                    player.SendError(result.Message);
                }
            }

            return result.IsSuccess;
        }
        catch (Exception ex)
        {
            Logger.LogError("Error when executing the command." + ex);
            player.SendError("Error when executing the command.");
            return false;
        }
    }

    private bool HasPermission(Player player)
    {
        return player.Client.Account.Rank >= (int)PermissionLevel;
    }
}