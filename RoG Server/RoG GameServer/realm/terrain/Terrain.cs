﻿using GameServer.Realm.Terrain.Chunk;
using GameServer.Realm.Terrain.Structures;
using Microsoft.Extensions.Logging;
using RoGCore.Assets;
using RoGCore.Logging;
using RoGCore.Models;
using RoGCore.Utils;
using RoGCore.Vector;
using static GameServer.Realm.EntityUtils;

namespace GameServer.Realm.Terrain;

internal static class Terrain
{
    private static readonly ILogger Logger = LogFactory.LoggingInstance(nameof(Terrain));

    private static readonly WeightedEntry<string>[] BeachEnemies =
    [
        new WeightedEntry<string>("Beach Snake", 50),
        new WeightedEntry<string>("Sand Man", 50),
    ];

    private static readonly WeightedEntry<string>[] RedEnemies =
    [
        new WeightedEntry<string>("Red Butcher", 33),
        new WeightedEntry<string>("Red Bandit", 33),
        new WeightedEntry<string>("Red Archer", 34),
    ];

    private static readonly WeightedEntry<string>[] GreenEnemies =
    [
        new WeightedEntry<string>("Green Spearman", 33),
        new WeightedEntry<string>("Green Archer", 33),
        new WeightedEntry<string>("Green Wizard", 34),
    ];

    private static readonly WeightedEntry<string>[] SavannahEnemies =
    [
        new WeightedEntry<string>("Savannah Wizard", 20),
        new WeightedEntry<string>("Savannah Bandit", 20),
        new WeightedEntry<string>("Savannah Priest", 20),
        new WeightedEntry<string>("Savannah Archer", 20),
        new WeightedEntry<string>("Savannah Rat", 15),
        new WeightedEntry<string>("Small Reaper", 5),
    ];

    private static readonly WeightedEntry<string>[] TundraEnemies =
    [
        new WeightedEntry<string>("Snowman", 25),
        new WeightedEntry<string>("Tundra Penguin", 25),
        new WeightedEntry<string>("Tundra Fox", 25),
        new WeightedEntry<string>("Tundra Owl", 25),
    ];

    internal static string GetBeachEnemy(Random rand)
    {
        return PickWeightedEntry(BeachEnemies);
    }

    internal static string GetRedEnemies(Random rand)
    {
        return PickWeightedEntry(RedEnemies);
    }

    internal static string GetGreenEnemies(Random rand)
    {
        return PickWeightedEntry(GreenEnemies);
    }

    internal static string GetSavannahEnemies(Random rand)
    {
        return PickWeightedEntry(SavannahEnemies);
    }

    internal static string GetTundraEnemies(Random rand)
    {
        return PickWeightedEntry(TundraEnemies);
    }

    internal static void InjectStructure(
        Dictionary<Vector2Int, KeyValuePair<string, string>> generations,
        World.Worlds.Realm world,
        StructureType structure)
    {
        IntVector2S generationKeys = new(generations.Count);
        var i = 0;
        foreach (var generation in generations)
        {
            generationKeys[i] = generation.Key;
            i++;
        }

        foreach (var chunkPosition in Chunk.Chunk.ChunkPosition(ref generationKeys).Distinct())
        {
            var chunk = world.GetChunk(chunkPosition, false);

            if (chunk.State == ChunkState.Complete)
            {
                Logger.LogDebug($"Adding {Enum.GetName(structure)} to complete chunk {chunkPosition}, consider increasing structure generation distance");
            }

            for (var x = 0; x < Chunk.Chunk.SIZE; x++)
            {
                for (var y = 0; y < Chunk.Chunk.SIZE; y++)
                {
                    var tile = chunk[x, y];
                    if (tile.Structure != StructureType.None
                        || !generations.TryGetValue(tile.Position, out var generation))
                    {
                        continue;
                    }

                    if (!generation.Key.IsNullOrWhiteSpace() &&
                        !EmbeddedData.Tiles.TryGetValue(generation.Key, out tile.TileDesc))
                    {
                        Logger.LogError(
                            $"ERROR loading tileDesc: '{generation.Key}' whilst resolving a tile");
                    }

                    if (!generation.Value.IsNullOrWhiteSpace())
                    {
                        if (!EmbeddedData.BetterObjects.TryGetValue(
                            generation.Value,
                            out var objDesc))
                        {
                            Logger.LogError(
                                $"ERROR loading objectDesc: '{generation.Value}' whilst resolving a tile");
                        }
                        else
                        {
                            tile.ObjDesc = objDesc;
                        }
                    }

                    tile.Structure = structure;
                    tile.UpdateCount++;
                }
            }
        }
    }

    internal static StructureType StructureHere(in Vector2Int pos)
    {
        return CoralReef.CoralReefHere(pos)
            ? StructureType.CoralReef
            : OldBlacksmith.OldBlacksmithHere(pos)
            ? StructureType.OldBlacksmith
            : PirateShip.PirateShipHere(pos)
            ? StructureType.PirateShip
            : Christmas.ChristmasHere(pos)
            ? StructureType.Christmas
            : StructureType.None;
    }
}