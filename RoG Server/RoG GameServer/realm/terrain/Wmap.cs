﻿using System.Numerics;
using GameServer.Realm.Entities;
using GameServer.Realm.Terrain.Tile;
using GameServer.Realm.World.Json;
using Ionic.Zlib;
using RoGCore.Assets;
using RoGCore.Models;

namespace GameServer.Realm.Terrain;

public class Wmap
{
    internal Tuple<Vector2Int, string>[] Entities = [];
    internal WmapTile?[,] Tiles;
    internal ushort Width;
    internal ushort Height;

    internal List<Vector2> PlayerSpawnPoints = [];
    internal List<Vector2> RealmPortalSpawnPoints = [];

    internal Wmap()
    {
        Tiles = new WmapTile[0, 0];
    }

    internal WmapTile? this[in Vector2Int i]
    {
        get => Tiles[i.X, i.Y];
        set
        {
            if (value is not null)
            {
                value.Position = i;
            }

            Tiles[i.X, i.Y] = value;
        }
    }

    internal WmapTile? this[int x, int y]
    {
        get => Tiles[x, y];
        set
        {
            if (value is not null)
            {
                value.Position = new Vector2Int(x, y);
            }

            Tiles[x, y] = value;
        }
    }

    /// <summary>
    /// Warning: Cloning GameWorlds is not supported due to an optimization.
    /// </summary>
    /// <returns>Clone of map.</returns>
    internal Wmap Clone()
    {
        return new()
        {
            Entities = Entities,
            Tiles = (WmapTile[,])Tiles.Clone(),
            Width = Width,
            Height = Height,
            PlayerSpawnPoints = PlayerSpawnPoints,
            RealmPortalSpawnPoints = RealmPortalSpawnPoints,
        };
    }

    internal void Load(JsonMap jsonData)
    {
        Width = jsonData.Width;
        Height = jsonData.Height;
        Tiles = new WmapTile[Width, Height];
        List<Tuple<Vector2Int, string>> entities = [];
        var dat = ZlibStream.UncompressBuffer(jsonData.Data);
        using var rdr = new NReader(new MemoryStream(dat));

        for (var y = 0; y < Height; y++)
        {
            for (var x = 0; x < Width; x++)
            {
                Vector2Int pos = new(x, y);
                var loc = jsonData.Dict[rdr.ReadInt16()];

                var tile = new WmapTile()
                {
                    ObjDesc = loc.Obj is not null ? EmbeddedData.BetterObjects[loc.Obj.Value.Id] : null,
                    TileDesc = loc.Ground is not null ? EmbeddedData.Tiles[loc.Ground] : null,
                    Region = loc.Region is null ? TileRegion.None : (TileRegion)Enum.Parse(typeof(TileRegion), loc.Region.Replace(' ', '_')),
                };

                if (tile.Region == TileRegion.Spawn)
                {
                    PlayerSpawnPoints.Add(pos);
                }
                else if (tile.Region == TileRegion.RealmPortals)
                {
                    RealmPortalSpawnPoints.Add(pos);
                }

                if (tile.ObjDesc?.Static == false)
                {
                    entities.Add(new Tuple<Vector2Int, string>(pos, tile.ObjDesc.Name));
                    tile.ObjDesc = null;
                }

                this[x, y] = tile;
            }
        }

        Entities = [.. entities];
    }

    internal IEnumerable<Entity> InstantiateEntities(World.World owner)
    {
        foreach ((var position, var objectName) in Entities)
        {
            var entity = Entity.Resolve(objectName, owner);
            entity.Move(position + new Vector2(0.5f, 0.5f));
            yield return entity;
        }
    }

    internal bool Contains(int x, int y)
    {
        return Width != 0 && Height != 0 && x >= 0 && x < Width && y >= 0 && y < Height;
    }

    internal bool Contains(Vector2Int p)
    {
        return Contains(p.X, p.Y);
    }
}