﻿using System.Numerics;
using RoGCore.Models;
using static GameServer.Realm.Terrain.Terrain;

namespace GameServer.Realm.Terrain.Structures;

internal static class Christmas
{
    private const int Rarity = 125;
    private const int PresentRadius = 5;
    private const int ClearRadius = 7;

    private static readonly bool[,] LocationMap = new bool[Program.AppSettings.GameServer.StructureMapSize, Program.AppSettings.GameServer.StructureMapSize];

    internal static void PopulateMap(Random rand)
    {
        for (var y = 0; y < Program.AppSettings.GameServer.StructureMapSize / Rarity; y++)
        {
            for (var x = 0; x < Program.AppSettings.GameServer.StructureMapSize / Rarity; x++)
            {
                var position = new Vector2Int(
                    (x * Rarity) + rand.Next(0, Rarity),
                    (y * Rarity) + rand.Next(0, Rarity));

                LocationMap[position.X, position.Y] = true;
            }
        }
    }

    internal static bool ChristmasHere(in Vector2Int pos)
    {
        return LocationMap[pos.X % Program.AppSettings.GameServer.StructureMapSize, pos.Y % Program.AppSettings.GameServer.StructureMapSize];
    }

    internal static string Generate(in Vector2Int point, World.Worlds.Realm world)
    {
        Dictionary<Vector2Int, KeyValuePair<string, string>> generations = [];
        Random rand = new(point.GetHashCode());
        for (var x = point.X - ClearRadius; x <= point.X + ClearRadius + 1; x++)
        {
            for (var y = point.Y - ClearRadius; y <= point.Y + ClearRadius + 1; y++)
            {
                Vector2Int pos = new(x, y);
                if (Vector2.DistanceSquared(pos, point) <= PresentRadius * PresentRadius)
                {
                    generations[pos] = rand.Next(0, 11) == 0
                        ? new KeyValuePair<string, string>(string.Empty, "Christmas Present")
                        : new KeyValuePair<string, string>(string.Empty, "StructureNoFill");
                }
                else if (Vector2.DistanceSquared(pos, point) <= ClearRadius * ClearRadius &&
                         !generations.ContainsKey(pos))
                {
                    generations[pos] = new KeyValuePair<string, string>(string.Empty, "StructureNoFill");
                }
            }
        }

        generations[point] = new KeyValuePair<string, string>(string.Empty, "Christmas Tree");
        InjectStructure(generations, world, StructureType.Christmas);
        return string.Empty;
    }
}