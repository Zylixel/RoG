﻿using System.Numerics;
using GameServer.Realm.Terrain.Biome;
using RoGCore.Models;
using static GameServer.Realm.Terrain.Terrain;

namespace GameServer.Realm.Terrain.Structures;

internal static class CoralReef
{
    private const int Rarity = 100;

    private static readonly bool[,] LocationMap = new bool[Program.AppSettings.GameServer.StructureMapSize, Program.AppSettings.GameServer.StructureMapSize];

    internal static void PopulateMap(Random rand)
    {
        for (var y = 0; y < Program.AppSettings.GameServer.StructureMapSize / Rarity; y++)
        {
            for (var x = 0; x < Program.AppSettings.GameServer.StructureMapSize / Rarity; x++)
            {
                var position = new Vector2Int(
                    (x * Rarity) + rand.Next(0, Rarity),
                    (y * Rarity) + rand.Next(0, Rarity));

                LocationMap[position.X, position.Y] = true;
            }
        }
    }

    internal static bool CoralReefHere(in Vector2Int pos)
    {
        return LocationMap[pos.X % Program.AppSettings.GameServer.StructureMapSize, pos.Y % Program.AppSettings.GameServer.StructureMapSize];
    }

    internal static void Generate(in Vector2Int point, World.Worlds.Realm world)
    {
        const int maxReefDistanceToBeach = 22;
        const int radius = 40;

        for (var x = point.X - maxReefDistanceToBeach; x <= point.X + maxReefDistanceToBeach + 1; x++)
        {
            for (var y = point.Y - maxReefDistanceToBeach; y <= point.Y + maxReefDistanceToBeach + 1; y++)
            {
                var biome = world.GetBiome(new Vector2Int(x, y));
                if (biome is not Biome.Type.TropicalWater and not Biome.Type.Ocean and not Biome.Type.River and
                    not Biome.Type.TropicalRiver)
                {
                    return;
                }
            }
        }

        Dictionary<Vector2Int, KeyValuePair<string, string>> generations = new()
        {
            { point, KeyValuePair.Create(string.Empty, "Coral") },
        };
        Random rand = new(point.GetHashCode() + world.Noise.Seed);
        for (var x = point.X - radius; x <= point.X + radius + 1; x++)
        {
            for (var y = point.Y - radius; y <= point.Y + radius + 1; y++)
            {
                Vector2Int pos = new(x, y);
                var biome = world.GetBiome(pos);
                if (biome is not Biome.Type.TropicalWater and not Biome.Type.Ocean and not Biome.Type.River and
                    not Biome.Type.TropicalRiver)
                {
                    continue;
                }

                var distSqr = Vector2.DistanceSquared(pos, point);
                if (distSqr > radius * radius)
                {
                    continue;
                }

                var r2 = Math.Max(distSqr / (5 * 4), 7);
                if (rand.Next(0, (int)r2) <= 4 - 1)
                {
                    generations[pos] = new KeyValuePair<string, string>(string.Empty, "Coral");
                }
            }
        }

        InjectStructure(generations, world, StructureType.CoralReef);
    }
}