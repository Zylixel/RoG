﻿using System.Numerics;
using RoGCore.Models;
using static GameServer.Realm.Terrain.Terrain;

namespace GameServer.Realm.Terrain.Structures;

internal static class OldBlacksmith
{
    private const int Rarity = 100;
    private const int HouseSize = 9;
    private const int Campoutblacksmithsize = 5;
    private const int Clearradius = 7;

    private const int T3Blacksmithsize = 11;
    private const int Tundrablacksmithsize = 11;

    private static readonly bool[,] LocationMap = new bool[Program.AppSettings.GameServer.StructureMapSize, Program.AppSettings.GameServer.StructureMapSize];

    private static readonly byte[,] CampoutBlacksmith =
    {
        { 2, 2, 2, 2, 2 },
        { 2, 0, 0, 0, 2 },
        { 2, 0, 1, 0, 2 },
        { 2, 0, 0, 0, 2 },
        { 2, 2, 2, 2, 2 },
    };

    private static readonly byte[,] House =
    {
        { 2, 3, 2, 1, 1, 1, 2, 3, 2 },
        { 2, 1, 1, 1, 1, 1, 1, 1, 2 },
        { 2, 1, 1, 1, 1, 1, 1, 1, 2 },
        { 3, 1, 1, 1, 1, 1, 1, 1, 3 },
        { 3, 1, 6, 1, 4, 1, 5, 1, 3 },
        { 3, 1, 1, 1, 1, 1, 1, 1, 3 },
        { 2, 1, 1, 1, 1, 1, 1, 1, 2 },
        { 2, 1, 1, 1, 1, 1, 1, 1, 2 },
        { 2, 2, 2, 3, 3, 3, 2, 2, 2 },
    };

    private static readonly byte[,] T3Blacksmith =
    {
        { 2, 2, 3, 2, 1, 1, 1, 2, 3, 2, 2 },
        { 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2 },
        { 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2 },
        { 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2 },
        { 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3 },
        { 3, 1, 1, 6, 1, 4, 1, 5, 1, 1, 3 },
        { 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3 },
        { 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2 },
        { 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2 },
        { 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2 },
        { 2, 2, 2, 2, 3, 3, 3, 2, 2, 2, 2 },
    };

    private static readonly byte[,] TundraBlacksmith =
    {
        { 0, 0, 2, 2, 1, 1, 1, 2, 2, 0, 0 },
        { 0, 2, 2, 1, 1, 1, 1, 1, 2, 2, 0 },
        { 2, 2, 1, 1, 1, 1, 1, 1, 1, 2, 2 },
        { 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2 },
        { 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2 },
        { 2, 1, 1, 6, 1, 1, 1, 5, 1, 1, 2 },
        { 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2 },
        { 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2 },
        { 2, 2, 1, 1, 1, 1, 1, 1, 1, 2, 2 },
        { 0, 2, 2, 1, 1, 1, 1, 1, 2, 2, 0 },
        { 0, 0, 2, 2, 2, 2, 2, 2, 2, 0, 0 },
    };

    internal static void PopulateMap(Random rand)
    {
        for (var y = 0; y < Program.AppSettings.GameServer.StructureMapSize / Rarity; y++)
        {
            for (var x = 0; x < Program.AppSettings.GameServer.StructureMapSize / Rarity; x++)
            {
                var position = new Vector2Int(
                    (x * Rarity) + rand.Next(0, Rarity),
                    (y * Rarity) + rand.Next(0, Rarity));

                LocationMap[position.X, position.Y] = true;
            }
        }
    }

    internal static bool OldBlacksmithHere(in Vector2Int pos)
    {
        return LocationMap[pos.X % Program.AppSettings.GameServer.StructureMapSize, pos.Y % Program.AppSettings.GameServer.StructureMapSize];
    }

    internal static string GenerateT1(Vector2 point, World.Worlds.Realm world)
    {
        Dictionary<Vector2Int, KeyValuePair<string, string>> generations = [];
        Random rand = new(point.GetHashCode());
        var spawnedTable = false;
        for (var x = 0; x < Campoutblacksmithsize; x++)
        {
            for (var y = 0; y < Campoutblacksmithsize; y++)
            {
                Vector2Int pos =
                    new(
                        (int)Math.Ceiling(x + point.X - (Campoutblacksmithsize / 2f)),
                        (int)Math.Ceiling(y + point.Y - (Campoutblacksmithsize / 2f)));
                switch (CampoutBlacksmith[y, x])
                {
                    case 1:
                        generations[pos] = new KeyValuePair<string, string>(string.Empty, "Campfire");
                        break;
                    case 2:
                        if (!spawnedTable && rand.Next(0, 15) == 0)
                        {
                            generations[pos] = new KeyValuePair<string, string>(string.Empty, "Polishing Table");
                            spawnedTable = true;
                        }

                        break;
                }
            }
        }

        if (!spawnedTable)
        {
            generations[
                    new Vector2Int(
                        (int)Math.Ceiling(2 + point.X - (Campoutblacksmithsize / 2f)),
                        (int)Math.Ceiling(1 + point.Y - (Campoutblacksmithsize / 2f)))]
                = new KeyValuePair<string, string>(string.Empty, "Polishing Table");
        }

        for (var x = (int)point.X - Clearradius; x <= point.X + Clearradius + 1; x++)
        {
            for (var y = (int)point.Y - Clearradius; y <= point.Y + Clearradius + 1; y++)
            {
                Vector2Int pos = new(x, y);
                if (Vector2.DistanceSquared(pos, point) <= Clearradius * Clearradius &&
                    !generations.ContainsKey(pos))
                {
                    generations[pos] = new KeyValuePair<string, string>(string.Empty, "StructureNoFill");
                }
            }
        }

        InjectStructure(generations, world, StructureType.OldBlacksmith);
        return "Old Blacksmith";
    }

    internal static string GenerateT2(Vector2 point, World.Worlds.Realm world)
    {
        Dictionary<Vector2Int, KeyValuePair<string, string>> generations = [];
        for (var x = 0; x < HouseSize; x++)
        {
            for (var y = 0; y < HouseSize; y++)
            {
                Vector2Int pos =
                    new(
                        (int)Math.Ceiling(x + point.X - (HouseSize / 2f)),
                        (int)Math.Ceiling(y + point.Y - (HouseSize / 2f)));

                var generation = House[y, x] switch
                {
                    1 => new KeyValuePair<string, string>("Wood Floor", "StructureNoFill"),
                    2 => new KeyValuePair<string, string>("Wood Floor", "Wooden Wall"),
                    3 => new KeyValuePair<string, string>("Wood Floor", "Wooden Wall Window"),
                    4 => new KeyValuePair<string, string>("Wood Floor", "Gothic Candelabra"),
                    5 => new KeyValuePair<string, string>("Wood Floor", "Broken Anvil"),
                    6 => new KeyValuePair<string, string>("Wood Floor", "Polishing Table"),
                    _ => new KeyValuePair<string, string>(string.Empty, string.Empty),
                };
                if (generation.Key != string.Empty || generation.Value != string.Empty)
                {
                    generations[pos] = generation;
                }
            }
        }

        InjectStructure(generations, world, StructureType.OldBlacksmith);
        return "Old Blacksmith";
    }

    internal static string GenerateT3(Vector2 point, World.Worlds.Realm world)
    {
        Dictionary<Vector2Int, KeyValuePair<string, string>> generations = [];
        for (var x = 0; x < T3Blacksmithsize; x++)
        {
            for (var y = 0; y < T3Blacksmithsize; y++)
            {
                Vector2Int pos =
                    new(
                        (int)Math.Ceiling(x + point.X - (T3Blacksmithsize / 2f)),
                        (int)Math.Ceiling(y + point.Y - (T3Blacksmithsize / 2f)));
                var generation = T3Blacksmith[y, x] switch
                {
                    1 => new KeyValuePair<string, string>("Wood Floor", "StructureNoFill"),
                    2 => new KeyValuePair<string, string>("Wood Floor", "Wooden Wall"),
                    3 => new KeyValuePair<string, string>("Wood Floor", "Wooden Wall Window"),
                    4 => new KeyValuePair<string, string>("Wood Floor", "Gothic Candelabra"),
                    5 => new KeyValuePair<string, string>("Wood Floor", "Anvil"),
                    6 => new KeyValuePair<string, string>("Wood Floor", "Polishing Table"),
                    _ => new KeyValuePair<string, string>(string.Empty, string.Empty),
                };
                if (generation.Key != string.Empty || generation.Value != string.Empty)
                {
                    generations[pos] = generation;
                }
            }
        }

        InjectStructure(generations, world, StructureType.OldBlacksmith);
        return "Old Blacksmith";
    }

    internal static string GenerateTundra(Vector2 point, World.Worlds.Realm world)
    {
        Dictionary<Vector2Int, KeyValuePair<string, string>> generations = [];
        for (var x = 0; x < Tundrablacksmithsize; x++)
        {
            for (var y = 0; y < Tundrablacksmithsize; y++)
            {
                Vector2Int pos =
                    new(
                        (int)Math.Ceiling(x + point.X - (Tundrablacksmithsize / 2f)),
                        (int)Math.Ceiling(y + point.Y - (Tundrablacksmithsize / 2f)));
                var generation = TundraBlacksmith[y, x] switch
                {
                    1 => new KeyValuePair<string, string>("Snow", "StructureNoFill"),
                    2 => new KeyValuePair<string, string>("Snow", "Snow Wall"),
                    5 => new KeyValuePair<string, string>("Snow", "Broken Anvil"),
                    6 => new KeyValuePair<string, string>("Snow", "Polishing Table"),
                    _ => new KeyValuePair<string, string>(string.Empty, string.Empty),
                };
                if (generation.Key != string.Empty || generation.Value != string.Empty)
                {
                    generations[pos] = generation;
                }
            }
        }

        InjectStructure(generations, world, StructureType.OldBlacksmith);
        return "Old Blacksmith With Coat";
    }
}