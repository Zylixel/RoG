﻿using RoGCore.Models;
using static GameServer.Realm.Terrain.Terrain;

namespace GameServer.Realm.Terrain.Structures;

internal static class PirateShip
{
    private const int Rarity = 50;
    private const int Maxpirateshipdistancetobeach = 15;
    private const int Pirateshipsizex = 11;
    private const int Pirateshipsizey = 25;

    private static readonly bool[,] LocationMap = new bool[Program.AppSettings.GameServer.StructureMapSize, Program.AppSettings.GameServer.StructureMapSize];

    private static readonly byte[,] PirateShipLayout =
    {
        { 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0 },
        { 0, 0, 0, 3, 1, 3, 0, 0, 0, 0, 0 },
        { 0, 0, 3, 1, 1, 1, 3, 0, 0, 0, 0 },
        { 0, 3, 1, 1, 2, 1, 1, 3, 0, 0, 0 },
        { 3, 1, 1, 1, 1, 1, 1, 1, 3, 0, 0 },
        { 3, 1, 1, 1, 1, 1, 1, 1, 3, 0, 0 },
        { 3, 1, 1, 1, 1, 1, 1, 1, 3, 0, 0 },
        { 3, 1, 1, 1, 2, 1, 1, 1, 3, 0, 0 },
        { 3, 1, 1, 1, 1, 1, 1, 1, 3, 0, 0 },
        { 3, 1, 1, 1, 1, 1, 1, 1, 3, 0, 0 },
        { 3, 1, 1, 1, 1, 1, 1, 1, 3, 0, 0 },
        { 3, 1, 1, 1, 2, 1, 1, 1, 0, 0, 0 },
        { 3, 1, 1, 1, 1, 1, 1, 1, 4, 4, 4 },
        { 3, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0 },
        { 3, 1, 1, 1, 1, 1, 1, 1, 3, 0, 0 },
        { 3, 1, 1, 1, 2, 1, 1, 1, 3, 0, 0 },
        { 3, 1, 1, 1, 1, 1, 1, 1, 3, 0, 0 },
        { 3, 1, 1, 1, 1, 1, 1, 1, 3, 0, 0 },
        { 3, 1, 1, 1, 1, 1, 1, 1, 3, 0, 0 },
        { 3, 1, 1, 1, 2, 1, 1, 1, 3, 0, 0 },
        { 3, 1, 1, 1, 1, 1, 1, 1, 3, 0, 0 },
        { 3, 1, 1, 1, 1, 1, 1, 1, 3, 0, 0 },
        { 3, 1, 1, 1, 2, 1, 1, 1, 3, 0, 0 },
        { 0, 3, 1, 1, 1, 1, 1, 3, 0, 0, 0 },
        { 0, 0, 3, 3, 3, 3, 3, 0, 0, 0, 0 },
    };

    internal static void PopulateMap(Random rand)
    {
        for (var y = 0; y < Program.AppSettings.GameServer.StructureMapSize / Rarity; y++)
        {
            for (var x = 0; x < Program.AppSettings.GameServer.StructureMapSize / Rarity; x++)
            {
                var position = new Vector2Int(
                    (x * Rarity) + rand.Next(0, Rarity),
                    (y * Rarity) + rand.Next(0, Rarity));

                LocationMap[position.X, position.Y] = true;
            }
        }
    }

    internal static bool PirateShipHere(in Vector2Int pos)
    {
        return LocationMap[pos.X % Program.AppSettings.GameServer.StructureMapSize, pos.Y % Program.AppSettings.GameServer.StructureMapSize];
    }

    internal static void Generate(in Vector2Int point, World.Worlds.Realm world)
    {
        for (var x = point.X - Maxpirateshipdistancetobeach;
            x <= point.X + Maxpirateshipdistancetobeach + 1;
            x++)
        {
            for (var y = point.Y - Maxpirateshipdistancetobeach;
                y <= point.Y + Maxpirateshipdistancetobeach + 1;
                y++)
            {
                var biome = Biome.Biome.GetBiome(new Vector2Int(x, y), world.Noise, World.Worlds.Realm.MAPSCALE);
                if (biome is not Biome.Type.TropicalWater and not Biome.Type.Ocean and not Biome.Type.River and not Biome.Type.TropicalRiver)
                {
                    return;
                }
            }
        }

        Dictionary<Vector2Int, KeyValuePair<string, string>> generations = [];
        for (var y = 0; y < Pirateshipsizey; y++)
        {
            for (var x = 0; x < Pirateshipsizex; x++)
            {
                Vector2Int pos =
                    new(
                        (int)Math.Ceiling(x + point.X - (Pirateshipsizex / 2f)),
                        (int)Math.Ceiling(y + point.Y - (Pirateshipsizey / 2f)));

                var generation = PirateShipLayout[y, x] switch
                {
                    1 => new KeyValuePair<string, string>(
                        "Wood Floor",
                        new Random(pos.GetHashCode() + world.Noise.Seed).Next(0, 20) == 0 ? "Wood Crate" : "StructureNoFill"),
                    2 => new KeyValuePair<string, string>("Metal Grate", "StructureNoFill"),
                    3 => new KeyValuePair<string, string>("Wood Floor", "Wooden Half Wall"),
                    4 => new KeyValuePair<string, string>("Dark Wood Floor", "StructureNoFill"),
                    _ => new KeyValuePair<string, string>(string.Empty, string.Empty),
                };
                if (generation.Key != string.Empty || generation.Value != string.Empty)
                {
                    generations[pos] = generation;
                }
            }
        }

        InjectStructure(generations, world, StructureType.PirateShip);
    }
}