﻿using System.Collections.Concurrent;
using Database;
using Database.Models;
using Microsoft.Extensions.Logging;
using RoGCore.Logging;
using RoGCore.Models;

namespace GameServer.Realm.World;

internal sealed class AntiDupe
{
    internal readonly HashSet<DbAccount> BadAccounts = [];

    private static readonly ILogger Logger = LogFactory.LoggingInstance(nameof(AntiDupe));
    private readonly ConcurrentDictionary<Guid, RawItem> badUuiDs = new();

    private readonly object searchLock = new();
    private int vaultItemsSearched;
    private int charsSearched;

    /// <summary>
    /// Scans entire database for dupes.
    /// If dupe found, it is wiped from database.
    /// </summary>
    internal void ThreadRipThruDupes()
    {
        Logger.LogStatus("Beginning a server-wide dupe check");
        badUuiDs.Clear();
        BadAccounts.Clear();
        ConcurrentDictionary<Guid, RawItem> allItems = new();
        charsSearched = 0;
        vaultItemsSearched = 0;

        var len = RedisDatabase.GetAccountLength();
        var processorCount = Math.Min(len, Environment.ProcessorCount);
        using CountdownEvent getAllItemsCountdown = new(processorCount);
        using CountdownEvent removeBadItemsCountdown = new(processorCount);
        var batch = (int)Math.Floor((float)len / processorCount);
        var remainder = len % processorCount;
        for (var i = 0; i < processorCount; i++)
        {
            var cur = i;
            ThreadPool.QueueUserWorkItem(async _ =>
            {
                var accountSearchStart = (cur * batch) + 1;
                var accountSearchStop = ((cur + 1) * batch) + 1 +
                                        (cur == processorCount - 1 ? remainder : 0);

                // Get all items in account range
                await foreach (var item in GetAllItems(accountSearchStart, accountSearchStop))
                {
                    // If an item with the same guid has already been added, assume dupe
                    if (!allItems.TryAdd(item.Uuid, item))
                    {
                        badUuiDs.TryAdd(item.Uuid, item);
                    }
                }

                // Signify completion
                getAllItemsCountdown.Signal();

                // Wait for other threads to finish
                getAllItemsCountdown.Wait();

                // Destory all duped items
                await RemoveBadGuids(accountSearchStart, accountSearchStop);

                // Signify completion
                removeBadItemsCountdown.Signal();
            });
        }

        Logger.LogStatus($"Created {processorCount} threads to check dupes");
        getAllItemsCountdown.Wait();
        Logger.LogStatus("Threads have finished getting all items");

        removeBadItemsCountdown.Wait();
        Logger.LogStatus("Threads have finished removing all items");
        Logger.LogResults("Accounts Searched", len, false);
        Logger.LogResults("Characters Searched", charsSearched, false);
        Logger.LogResults("Vault Items Searched", vaultItemsSearched, false);
        Logger.LogResults("Items Compared", allItems.Count, false);
    }

    internal async Task<bool> AccountHasDupedItems(DbAccount acc)
    {
        var foundDupe = false;
        ConcurrentDictionary<Guid, RawItem> uuiDs = new();
        ConcurrentDictionary<Guid, RawItem> allItems = new();
        foreach (var item in acc.Vault.Where(item => !allItems.TryAdd(item.Uuid, item)))
        {
            uuiDs.TryAdd(item!.Uuid, item);
        }

        foreach (var character in await RedisDatabase.GetAliveCharacters(acc))
        {
            if (character is null)
            {
                throw new OperationCanceledException();
            }

            for (var j = 0; j < character.Items.Length; j++)
            {
                var item = character.Items[j];
                if (item != null && !allItems.TryAdd(item.Uuid, item))
                {
                    uuiDs.TryAdd(item.Uuid, item);
                }
            }
        }

        foreach (var item in acc.Vault.Where(item => uuiDs.ContainsKey(item.Uuid)))
        {
            foundDupe = true;
            LogDupe(acc, $"vault item: {item}");
            RedisDatabase.RemoveVault(acc, item!);
        }

        foreach (var character in await RedisDatabase.GetAliveCharacters(acc))
        {
            var temp = character.Items;
            for (var c = 0; c < temp.Length; c++)
            {
                if (temp[c] is null || !uuiDs.ContainsKey(temp[c]!.Uuid))
                {
                    continue;
                }

                foundDupe = true;
                LogDupe(acc, $"character: {character.CharacterId}, item: {temp[c]!.ObjectType}");
                temp[c] = null;
            }

            character.Items = temp;
            character.Flush();
            character.ReloadFields();
        }

        return foundDupe;
    }

    private static void LogDupe(DbAccount? acc, string log)
    {
        Logger.LogError($"Dupe Located from accId: {acc?.AccountId ?? -1}, {log}");
    }

    private async IAsyncEnumerable<RawItem> GetAllItems(int start, int end)
    {
        var vaultItems = 0;
        var chars = 0;
        Logger.LogStatus($"[Thread {Environment.CurrentManagedThreadId}] Searching accounts {start} to {end - 1}");
        for (var i = start; i < end; i++)
        {
            var acc = RedisDatabase.GetAccount(i);
            if (acc is null)
            {
                Logger.LogError($"[Thread {Environment.CurrentManagedThreadId}] Account {i} is null");
                continue;
            }

            vaultItems += acc.Vault.Count;
            foreach (var item in acc.Vault)
            {
                yield return item;
            }

            foreach (var character in await RedisDatabase.GetAliveCharacters(acc))
            {
                chars++;
                for (var j = 0; j < character.Items.Length; j++)
                {
                    if (character.Items[j] != null)
                    {
                        yield return character.Items[j]!;
                    }
                }
            }
        }

        lock (searchLock)
        {
            vaultItemsSearched += vaultItems;
            charsSearched += chars;
        }
    }

    private async Task RemoveBadGuids(int start, int end)
    {
        for (var i = start; i < end; i++)
        {
            var acc = RedisDatabase.GetAccount(i);
            if (acc is null)
            {
                continue;
            }

            foreach (var item in acc.Vault.Where(item => badUuiDs.ContainsKey(item.Uuid)))
            {
                LogDupe(acc, $"vault item: {item}");
                BadAccounts.Add(acc);
                RedisDatabase.RemoveVault(acc, item!);
            }

            foreach (var character in await RedisDatabase.GetAliveCharacters(acc))
            {
                var temp = character.Items;
                for (var c = 0; c < temp.Length; c++)
                {
                    if (temp[c] is null || !badUuiDs.ContainsKey(temp[c]!.Uuid))
                    {
                        continue;
                    }

                    LogDupe(acc, $"character: {character.CharacterId}, item: {temp[c]!.ObjectType}");
                    BadAccounts.Add(acc);
                    temp[c] = null;
                }

                character.Items = temp;
                character.Flush();
                character.ReloadFields();
            }
        }
    }
}