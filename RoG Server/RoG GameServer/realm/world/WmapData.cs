﻿using GameServer.Realm.Terrain;
using GameServer.Realm.World.DungeonGeneration;
using GameServer.Realm.World.Json;
using GameServer.Realm.World.Worlds;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RoGCore.Logging;

namespace GameServer.Realm.World;

internal static class WmapData
{
    internal static readonly Dictionary<string, Wmap> LoadedMaps = [];

    private static readonly ILogger Logger = LogFactory.LoggingInstance(nameof(WmapData));

    internal static void Initialize()
    {
        var mapNames = GetMapNamesForWorlds().Concat(GetMapNamesForGenerationPieces());

        var totalMaps = 0;
        foreach (var mapName in mapNames)
        {
            var map = new Wmap();
            LoadMap(map, mapName);
            LoadedMaps.Add(mapName, map);

            totalMaps++;
        }

        Logger.LogResults("Maps", totalMaps);
    }

    private static List<string> GetMapNamesForWorlds()
    {
        var result = new List<string>();

        var t = typeof(World);
        var validWorlds = t.Assembly.GetTypes()
            .Where(x => t.IsAssignableFrom(x) && x != t && x != typeof(RandomWorld));
        foreach (var i in validWorlds)
        {
            try
            {
                var instance = (World?)Activator.CreateInstance(i) ?? throw new Exception("Instance was null");

                Logger.LogStatus(instance.Name);
                foreach (var mapName in instance.MapFiles)
                {
                    result.Add(mapName);
                }
            }
            catch (Exception e)
            {
                Logger.LogError($"ERROR loading {i.Name} : {e}");
            }
        }

        return result;
    }

    private static List<string> GetMapNamesForGenerationPieces()
    {
        var result = new List<string>();

        var t = typeof(GenerationPiece);
        var validPieces = t.Assembly.GetTypes()
            .Where(x => t.IsAssignableFrom(x) && x != t);
        foreach (var i in validPieces)
        {
            try
            {
                var instance = (GenerationPiece?)Activator.CreateInstance(i) ?? throw new Exception("Instance was null");

                Logger.LogStatus(instance.GetType().Name);

                foreach (var mapName in instance.MapFiles)
                {
                    result.Add(mapName);
                }
            }
            catch (MissingMethodException)
            {
                // Some of the generation pieces don't have empty constructors
                // Those ones don't use map data
            }
            catch (Exception e)
            {
                Logger.LogError($"ERROR loading {i.Name} : {e}");
            }
        }

        return result;
    }

    private static void LoadMap(Wmap map, string mapName)
    {
        var stream =
            typeof(RealmManager).Assembly.GetManifestResourceStream(
                $"GameServer.Realm.World.Maps.{mapName}.json")
            ?? throw new ArgumentException($"map resource " + mapName + " not found!");

        var jsonData = JsonConvert.DeserializeObject<JsonMap>(new StreamReader(stream).ReadToEnd());
        map.Load(jsonData);
    }
}