﻿using System.Numerics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using GameServer.Realm.Entities;
using GameServer.Realm.Entities.Enemy;
using GameServer.Realm.Entities.GameObject;
using GameServer.Realm.Entities.Player;
using GameServer.Realm.Terrain;
using GameServer.Realm.Terrain.Tile;
using GameServer.Realm.World.Worlds;
using Microsoft.Extensions.Logging;
using RoGCore.Logging;
using RoGCore.Models;
using RoGCore.Networking.Packets;
using RoGCore.Vector;

namespace GameServer.Realm.World;

internal abstract partial class World
{
    internal const short NEXUSID = -1;
    internal const short TESTID = -3;
    internal const short GUILDHALL = -5;

    internal float LightLevel;
    internal bool Lighting;
    internal short Id;
    internal byte Difficulty;
    internal string Name;
    internal List<WorldTimer> Timers = [];
    internal byte Background;
    internal bool AllowTeleport;
    internal bool Dungeon;
    internal ushort MaxPlayers;
    internal string[] MapFiles;
    internal RealmTime CurrentTime;

    protected IEntityMap entityMap;

    private static readonly ILogger<World> Logger = LogFactory.LoggingInstance<World>();

    private readonly List<Projectile> projectiles = [];
    private long entityInc;
    private bool canBeClosed;

    protected World()
    {
        AllowTeleport = true;
        MaxPlayers = 25;
        MapFiles = [];

        Music = DefaultMusic;
        Name = string.Empty;

        Map = new Wmap();

        // Mark world for removal after 1:30 minutes if the
        // world is a dungeon and if no players in there.
        if (IsDungeon)
        {
            Timers.Add(new WorldTimer(90 * 1000, (_, _) => canBeClosed = true));
        }

        entityMap = new InfiniteEntityMap();
    }

    internal List<Player> Players { get; } = [];

    internal List<Enemy> Enemies { get; } = [];

    internal List<GameObject> Objects { get; } = [];

    internal Wmap Map { get; set; }

    internal string Music { get; set; }

    internal float LargestEntityHitbox { get; private set; }

    internal bool IsFull => MaxPlayers != 0 && Players.Count >= MaxPlayers;

    internal Dictionary<long, Entity> Entities => entityMap.Entities;

    protected virtual string DefaultMusic => Program.AppSettings.GameServer.Music.Default;

    private bool IsDungeon => this is not Nexus and not Worlds.Realm;

    internal abstract void Initialize();

    internal void SwitchMusic(string? music)
    {
        Music = music ?? DefaultMusic;
        BroadcastPacket(new Audio(Audio.Type.Music, Music));
    }

    internal virtual long EnterWorld(Entity entity)
    {
        entity.Id = Interlocked.Increment(ref entityInc);
        entityMap.AddEntity(entity);

        switch (entity)
        {
            case Player player:
                Players.Add(player);
                break;
            case Enemy enemy:
                Enemies.Add(enemy);
                break;
            case GameObject staticObject:
                Objects.Add(staticObject);
                break;
            default:
                throw new ArgumentException($"Unexpected entity type {typeof(Entity)}");
        }

        return entity.Id;
    }

    internal List<Projectile> GatherProjectiles()
    {
        projectiles.Clear();

        var enumerator = Entities.GetEnumerator();

        while (enumerator.MoveNext())
        {
            var entity = enumerator.Current;
            var entitiyProjectiles = entity.Value.Projectiles;
            if (entitiyProjectiles is null)
            {
                continue;
            }

            for (var j = 0; j < entitiyProjectiles.Length; j++)
            {
                var projectile = entitiyProjectiles[j];
                if (projectile is null)
                {
                    continue;
                }

                projectiles.Add(projectile);
            }
        }

        return projectiles;
    }

    internal virtual void LeaveWorld<T>(T entity)
        where T : Entity
    {
        entityMap.RemoveEntity(entity);
        switch (entity)
        {
            case Player when !Players.Remove((entity as Player)!):
                Logger.LogWarning($"Could not remove {entity.Name} from world {Name}");
                break;
            case Player:
                Logger.LogWarning($"Removed {entity.Name} from world {Name}");
                break;
            case Enemy:
                if (!Enemies.Remove((entity as Enemy)!))
                {
                    Logger.LogWarning($"Could not remove {entity.Name} from world {Name}");
                }

                break;

            case GameObject:
                Objects.Remove((entity as GameObject)!);
                if (entity is Wall)
                {
                    var oldTile = Map[entity.IntPosition];

                    if (oldTile is not null)
                    {
                        var tile = oldTile.Clone();
                        tile.UpdateCount++;
                        tile.ObjDesc = null;
                        Map[entity.IntPosition] = tile;

                        if (entity.ObjectDescription.BlocksSight)
                        {
                            foreach (var plr in Players)
                            {
                                if (entity.DistSqr(plr) <= Program.AppSettings.GameServer.Sight.Radius * Program.AppSettings.GameServer.Sight.Radius)
                                {
                                    plr.Sight.Update();
                                }
                            }
                        }
                    }
                }

                break;
        }

        entity.Dispose();
    }

    internal Entity? GetEntity(long id)
    {
        entityMap.TryGetEntity(id, out var ret);
        return ret;
    }

    internal void UpdateEntityCollision(Entity entity, Vector2 oldPosition)
    {
        entityMap.MoveEntity(entity, oldPosition);
    }

    internal IEnumerable<Entity> EntitiesInRectangle(in Rectangle rect)
    {
        return entityMap.GetEntitiesInArea(rect);
    }

    /// <summary>
    /// Determines where a player should spawn in the world.
    /// </summary>
    /// <param name="name">client name.</param>
    /// <returns>spawn position.</returns>
    internal virtual Vector2 GetPlayerSpawn(string name)
    {
        return Map.PlayerSpawnPoints[Random.Shared.Next(Map.PlayerSpawnPoints.Count)];
    }

    internal Player? GetPlayerByName(string name)
    {
        return (from i in Players
                where i.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase)
                select i).FirstOrDefault();
    }

    internal virtual void Tick(RealmTime time)
    {
        CurrentTime = time;

        var timersCopy = Timers.ToArray();
        for (var i = 0; i < timersCopy.Length; i++)
        {
            var timer = timersCopy[i];
            if (timer.Tick(this))
            {
                Timers.Remove(timer);
            }
        }

        if (Players.Count == 0)
        {
            if (canBeClosed && IsDungeon && !RealmPortalMonitor.Portals.ContainsKey(this))
            {
                RealmManager.RemoveWorld(this);
            }

            if (this is not Worlds.Realm)
            {
                return;
            }
        }

        for (var i = 0; i < Enemies.Count; i++)
        {
            Enemies[i].Tick();
        }

        for (var i = 0; i < Objects.Count; i++)
        {
            Objects[i].Tick();
        }

        var projectiles = CollectionsMarshal.AsSpan(GatherProjectiles());
        for (var i = 0; i < projectiles.Length; i++)
        {
            projectiles[i].Tick();
        }

        for (var i = 0; i < Players.Count; i++)
        {
            Players[i].Tick();
        }
    }

    internal void MemLoadMap(string embeddedResource)
    {
        if (!WmapData.LoadedMaps.TryGetValue(embeddedResource, out var loadedMap))
        {
            throw new Exception($"{embeddedResource} was not found in WmapData");
        }
        else
        {
            FromWorldMap(loadedMap.Clone());
        }
    }

    internal virtual WmapTile? GetTile(in Vector2Int pos)
    {
        return Map.Contains(pos) ? Map[pos] : null;
    }

    internal WmapTile? GetTile(int x, int y)
    {
        return GetTile(new Vector2Int(x, y));
    }

    internal virtual bool TryGetTile(in Vector2Int pos, out WmapTile? tile)
    {
        if (Map.Contains(pos))
        {
            var mapTile = Map[pos];
            if (mapTile is null)
            {
                tile = null;
                return false;
            }

            tile = mapTile;
            return true;
        }
        else
        {
            tile = null;
            return false;
        }
    }

    [MethodImpl(MethodImplOptions.AggressiveOptimization)]
    internal virtual IEnumerable<WmapTile> GetTiles(IntVector2S positions)
    {
        foreach (var position in positions)
        {
            if (TryGetTile(position, out var tile))
            {
                // TODO: temp fix to a major problem
                tile!.Position = position;
                yield return tile!;
            }
        }
    }

    [MethodImpl(MethodImplOptions.AggressiveOptimization)]
    internal virtual IEnumerable<WmapTile> GetTiles(Rectangle rectangle)
    {
        var left = Math.Clamp((int)rectangle.X, 0, Map.Width);
        var top = Math.Clamp((int)rectangle.Y, 0, Map.Height);
        var right = Math.Clamp((int)rectangle.Right, 0, Map.Width);
        var bottom = Math.Clamp((int)rectangle.Bottom, 0, Map.Height);

        for (var x = left; x < right; x++)
        {
            for (var y = top; y < bottom; y++)
            {
                var tile = Map[x, y];

                if (tile is null)
                {
                    continue;
                }

                tile.Position = new Vector2Int(x, y);
                yield return tile;
            }
        }
    }

    internal void ChatReceived(string text)
    {
        var enumerator = Entities.GetEnumerator();

        while (enumerator.MoveNext())
        {
            var entity = enumerator.Current;
            entity.Value.OnChatTextReceived(text);
        }
    }

    internal void EntityChangedHitbox(Entity entity)
    {
        if (LargestEntityHitbox <= entity.HitRadius)
        {
            LargestEntityHitbox = entity.HitRadius;
        }
        else
        {
            LargestEntityHitbox = float.MinValue;

            var enumerator = Entities.GetEnumerator();

            while (enumerator.MoveNext())
            {
                var en = enumerator.Current.Value;
                if (en.HitRadius > LargestEntityHitbox)
                {
                    LargestEntityHitbox = en.HitRadius;
                }
            }
        }
    }

    internal virtual void Explosion(Vector2 position, float radius, Entity damager, int damage, bool ignoreDefense)
    {
        var radiusSqr = radius * radius;
        if (damage > 0)
        {
            foreach (var player in Players)
            {
                if (player.DistSqr(position) <= radiusSqr)
                {
                    player.Damage(damager, damage, ignoreDefense, out _);
                }
            }
        }
    }

    internal void Aoe<T>(Vector2 position, float radius, Action<T> callback)
        where T : Entity
    {
        foreach (var en in InRange<T>(position, radius))
        {
            callback(en);
        }
    }

    internal virtual bool TryGetBounds(out Rectangle bounds)
    {
        if (Map is null)
        {
            bounds = Rectangle.Empty;
            return false;
        }

        bounds = new(0, 0, Map.Width + 1, Map.Height + 1);
        return true;
    }

    internal Wmap FromWorldMap(Wmap map)
    {
        Map = map;
        entityMap = new QuadTreeEntityMap(this);
        Objects.Clear();
        Enemies.Clear();
        Players.Clear();
        Map.InstantiateEntities(this).ToList();

        return map;
    }
}