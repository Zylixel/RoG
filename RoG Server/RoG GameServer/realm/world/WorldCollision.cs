﻿using System.Numerics;
using RoGCore.Models;
using RoGCore.Vector;

namespace GameServer.Realm.World;

internal static class WorldCollision
{
    internal static bool RegionUnblocked(this World w, Vector2 position, TileOccupency occupency)
    {
        return w.RegionUnblocked(position.X, position.Y, occupency);
    }

    internal static bool RegionUnblocked(this World w, float x, float y, TileOccupency occupency)
    {
        Vector2Int intVector = new((int)x, (int)y);

        IntVector2S collisionArea = new(CardinalOrdinalDirections.NineByNineArea.Count);
        CardinalOrdinalDirections.NineByNineArea.Add(intVector, ref collisionArea);

        var occupiedTiles = w.TilesOccupied(collisionArea, occupency);

        if (occupiedTiles[(int)CardinalOrdinalDirections.Direction.Center])
        {
            return false;
        }

        var xFrac = x - intVector.X;
        var yFrac = y - intVector.Y;
        if (xFrac < 0.5)
        {
            if (occupiedTiles[(int)CardinalOrdinalDirections.Direction.Left])
            {
                return false;
            }

            if (yFrac < 0.5)
            {
                if (occupiedTiles[(int)CardinalOrdinalDirections.Direction.Bottom] ||
                    occupiedTiles[(int)CardinalOrdinalDirections.Direction.BottomLeft])
                {
                    return false;
                }
            }
            else if (yFrac > 0.5 && (occupiedTiles[(int)CardinalOrdinalDirections.Direction.Top] ||
                 occupiedTiles[(int)CardinalOrdinalDirections.Direction.TopLeft]))
            {
                return false;
            }

            return true;
        }

        if (xFrac > 0.5)
        {
            if (occupiedTiles[(int)CardinalOrdinalDirections.Direction.Right])
            {
                return false;
            }

            if (yFrac < 0.5)
            {
                if (occupiedTiles[(int)CardinalOrdinalDirections.Direction.Bottom] ||
                    occupiedTiles[(int)CardinalOrdinalDirections.Direction.BottomRight])
                {
                    return false;
                }
            }
            else if (yFrac > 0.5 && (occupiedTiles[(int)CardinalOrdinalDirections.Direction.Top] ||
                 occupiedTiles[(int)CardinalOrdinalDirections.Direction.TopRight]))
            {
                return false;
            }

            return true;
        }

        return yFrac < 0.5
            ? !occupiedTiles[(int)CardinalOrdinalDirections.Direction.Bottom]
            : yFrac <= 0.5 || !occupiedTiles[(int)CardinalOrdinalDirections.Direction.Top];
    }

    private static bool[] TilesOccupied(
        this World w,
        IntVector2S positions,
        TileOccupency occupency)
    {
        return w.GetTiles(positions).Select(x => x?.Occupied(occupency) != false).ToArray();
    }
}