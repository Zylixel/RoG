﻿using Microsoft.Extensions.Logging;
using RoGCore.Logging;

namespace GameServer.Realm.World;

internal class WorldTimer(int totalMs, Action<World, RealmTime> callback)
{
    private static readonly ILogger<WorldTimer> Logger = LogFactory.LoggingInstance<WorldTimer>();

    private float remaining = totalMs;

    internal void Reset()
    {
        remaining = totalMs;
    }

    internal bool Tick(World world)
    {
        remaining -= world.CurrentTime.StaticMspt;
        if (remaining <= 0)
        {
            try
            {
                callback(world, world.CurrentTime);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, null);
            }

            return true;
        }

        return false;
    }
}