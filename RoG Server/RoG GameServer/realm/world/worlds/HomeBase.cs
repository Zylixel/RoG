﻿namespace GameServer.Realm.World.Worlds;

internal class HomeBase : World
{
    public HomeBase()
    {
        Name = "Home";
        Background = 2;
        Difficulty = 0;
        MapFiles = ["HomeBase"];
    }

    protected override string DefaultMusic => Program.AppSettings.GameServer.Music.SafeArea;

    internal override void Initialize()
    {
        MemLoadMap(MapFiles[0]);
    }
}