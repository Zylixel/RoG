﻿using GameServer.Realm.Entities.Player;
using RoGCore.Models;

namespace GameServer.Realm.World.Worlds;

internal class Nexus : World
{
    // AccountID, Player
    internal static readonly Dictionary<int, Player> UserList = [];

    public Nexus()
    {
        Id = NEXUSID;
        Name = "Nexus";
        Background = 2;
        AllowTeleport = false;
        Difficulty = 0;
        MapFiles = ["Nexus"];
        Lighting = true;
        LightLevel = 0.75f;
    }

    protected override string DefaultMusic => Program.AppSettings.GameServer.Music.SafeArea;

    internal override void Initialize()
    {
        MemLoadMap(MapFiles[0]);
    }

    internal override void Tick(RealmTime time)
    {
        base.Tick(time);

        // LightLevel = ((MathF.Sin(time.TotalElapsedMs / 10000f) * 1.5f) + 0.5f).Clamp(0.15f, 0.6f)
        if (time.TotalElapsedMs % (Program.AppSettings.GameServer.Tps * 2) == 0)
        {
            UpdatePortals();
        }

        CheckDupers();
    }

    private static void UpdatePortals()
    {
        var enumerator = RealmPortalMonitor.Portals.GetEnumerator();

        while (enumerator.MoveNext())
        {
            var portal = enumerator.Current;
            foreach (var j in RealmManager.CurrentRealmNames)
            {
                if (portal.Value.Name.StartsWith(j))
                {
                    if (portal.Value.Name == j)
                    {
                        portal.Value.PortalName = portal.Value.Name;
                    }

                    portal.Value.Name = $"{j} ({portal.Key.Players.Count}/{portal.Key.MaxPlayers})";
                    break;
                }
            }
        }
    }

    private void CheckDupers()
    {
        UserList.Clear();
        var enumerator = RealmManager.Worlds.GetEnumerator();

        while (enumerator.MoveNext())
        {
            var world = enumerator.Current;

            for (var i = 0; i < world.Value.Players.Count; i++)
            {
                var player = world.Value.Players[i];
                if (player.Client.Account is null)
                {
                    continue;
                }

                if (UserList.TryGetValue(player.Client.Account.AccountId, out var p))
                {
                    p.Client.Disconnect(DisconnectReason.DuperDisconnect);
                    player.Client.Disconnect(DisconnectReason.DuperDisconnect);
                }
                else
                {
                    UserList.Add(player.Client.Account.AccountId, player);
                }
            }
        }
    }
}