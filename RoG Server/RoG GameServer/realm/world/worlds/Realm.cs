﻿using System.Numerics;
using System.Runtime.CompilerServices;
using GameServer.Realm.Entities;
using GameServer.Realm.Entities.Enemy;
using GameServer.Realm.Entities.Player;
using GameServer.Realm.Terrain;
using GameServer.Realm.Terrain.Biome;
using GameServer.Realm.Terrain.Chunk;
using GameServer.Realm.Terrain.Foliage;
using GameServer.Realm.Terrain.Structures;
using GameServer.Realm.Terrain.Tile;
using Microsoft.Extensions.Logging;
using RoGCore.Logging;
using RoGCore.Models;
using RoGCore.Utils;
using RoGCore.Vector;
using static GameServer.Realm.Terrain.Biome.BiomeTypeExtensions;

namespace GameServer.Realm.World.Worlds;

internal class Realm : World
{
    internal const float MAPSCALE = 1f;

    internal readonly Noise Noise;
    internal readonly Noise OreNoise;

    private const ushort PlayerSpawnPositions = 20;

    private static readonly ILogger<Realm> Logger = LogFactory.LoggingInstance<Realm>();

    private readonly int generationSeed;
    private readonly Random spawnRandom = new();
    private readonly ChunkCache chunkCache = new();

    /// <summary>
    /// Stores where clients left the world.
    /// </summary>
    /// Perhaps this should be changed to a character-based key system.
    private readonly Dictionary<string, Vector2> lastClientPositions = [];

    public Realm()
    {
        Name = "Gilgor's Realm";
        Background = 2;
        AllowTeleport = true;
        Difficulty = 1;
        generationSeed = Random.Shared.Next();
        Noise = new Noise(generationSeed);
        OreNoise = new Noise(new Random(generationSeed).Next());
    }

    protected override string DefaultMusic => Program.AppSettings.GameServer.Music.Realm;

    internal override void Initialize()
    {
        PreRealmGeneration();
        GenerateMap();
    }

    internal TileRegion GetRegion(in Vector2Int position)
    {
        return Biome.GetBiome(position, Noise, MAPSCALE) switch
        {
            Terrain.Biome.Type.Beach or Terrain.Biome.Type.LightBeach => TileRegion.Spawn,
            _ => TileRegion.None,
        };
    }

    internal override IEnumerable<WmapTile> GetTiles(IntVector2S positions)
    {
        var chunkPositions = Chunk.ChunkPosition(ref positions);
        foreach (var chunkPos in chunkPositions.Distinct())
        {
            var chunk = GetChunk(chunkPos);
            for (var i = 0; i < positions.Count; i++)
            {
                if (chunkPositions.X[i] != chunkPos.X || chunkPositions.Y[i] != chunkPos.Y)
                {
                    continue;
                }

                var position = positions[i];

                yield return chunk[Chunk.RelativePosition(position)];
            }
        }
    }

    [MethodImpl(MethodImplOptions.AggressiveOptimization)]
    internal override IEnumerable<WmapTile> GetTiles(Rectangle rectangle)
    {
        var chunkRectangle = Chunk.ChunkPosition(rectangle);

        for (var x = (int)chunkRectangle.X; x <= chunkRectangle.Z; x++)
        {
            for (var y = (int)chunkRectangle.Y; y <= chunkRectangle.W; y++)
            {
                if (x == chunkRectangle.X || x == chunkRectangle.Z || y == chunkRectangle.Y || y == chunkRectangle.W)
                {
                    foreach (var tile in GetChunk(new Vector2Int(x, y)))
                    {
                        if (rectangle.Contains(tile.Position.X, tile.Position.Y))
                        {
                            yield return tile;
                        }
                    }
                }
                else
                {
                    foreach (var tile in GetChunk(new Vector2Int(x, y)))
                    {
                        yield return tile;
                    }
                }
            }
        }
    }

    internal override WmapTile GetTile(in Vector2Int pos)
    {
        var chunk = GetChunk(Chunk.ChunkPosition(pos));
        var relativePosition = Chunk.RelativePosition(pos);
        return chunk[relativePosition];
    }

    internal override bool TryGetTile(in Vector2Int pos, out WmapTile tile)
    {
        tile = GetTile(pos);
        return true;
    }

    /// <summary>
    /// Returns a chunk if it exists in the cache or DB.
    /// If the chunk doesn't exist, returns a fresh chunk.
    /// </summary>
    /// <param name="chunkPosition">position of the requested chunk.</param>
    /// <param name="build">whether or not to build the retrieved chunk.</param>
    /// <returns>Chunk in chunkposition.</returns>
    internal Chunk GetChunk(in Vector2Int chunkPosition, bool build = true)
    {
        if (chunkCache.TryRetrieve(chunkPosition, CurrentTime, out var chunk))
        {
            if (build)
            {
                this.BuildChunk(chunk, ChunkState.Complete);
            }

            return chunk;
        }

        var ret = new Chunk(chunkPosition, CurrentTime);
        chunkCache.Add(chunkPosition, ret);

        if (build)
        {
            this.BuildChunk(ret, ChunkState.Complete);
        }

        return ret;
    }

    internal override void Tick(RealmTime time)
    {
        base.Tick(time);

        // Load the structures in radius around players
        // Some chunks will be checked multiple times if players are close together
        // This is okay because there is a chunk cache and the time to build the chunk
        // Will be 0 if it is already built.
        for (var i = 0; i < Players.Count; i++)
        {
            var player = Players[i];

            var structureCircle = player.Sight.GetStructureCircle();

            for (var j = 0; j < structureCircle.Count; j++)
            {
                var chunk = GetChunk(structureCircle[j], false);
                this.BuildChunk(chunk, ChunkState.Structures);
            }
        }

        // Every one second
        if (time.TickCount % Program.AppSettings.GameServer.Tps == 0)
        {
            // Despawning is currently in Enemy.Tick(),
            // But takes place before spawning
            for (var i = 0; i < Players.Count; i++)
            {
                var player = Players[i];
                if (player.Client.Character is null || player.RealmEnemyList.Count >= Program.AppSettings.GameServer.Enemy.MaxPerPlayer)
                {
                    continue;
                }

                var spawnAttemptCount = Program.AppSettings.GameServer.Enemy.MaxPerPlayer * player.Skills.GetLevel(PlayerSkills.Skill.Combat) / 50f;
                spawnAttemptCount = spawnAttemptCount.Clamp(1, Program.AppSettings.GameServer.Enemy.MaxPerPlayer - player.RealmEnemyList.Count);

                for (var j = 0; j < spawnAttemptCount; j++)
                {
                    var enemy = SpawnEnemyAround(player);
                    if (enemy != null)
                    {
                        player.RealmEnemyList.Add(enemy);
                    }
                }
            }

            chunkCache.UnloadOldChunks(time);
        }
    }

    internal override Vector2 GetPlayerSpawn(string name)
    {
        if (lastClientPositions.TryGetValue(name, out var ret))
        {
            lastClientPositions.Remove(name);
            return ret;
        }

        return base.GetPlayerSpawn(name);
    }

    internal override void LeaveWorld<T>(T entity)
    {
        if (entity is Player player)
        {
            lastClientPositions.Add(player.Name, player.Position);
        }

        base.LeaveWorld(entity);
    }

    internal Enemy? SpawnEnemyAround(Player player)
    {
        float radius = spawnRandom.Next(
            Program.AppSettings.GameServer.Enemy.MinSpawnDistance,
            Program.AppSettings.GameServer.Enemy.MaxSpawnDistance);

        var angle = spawnRandom.Next(0, 360) * MathF.PI / 180f;
        var spawnPoint = player.IntPosition + (new Vector2(MathF.Cos(angle), MathF.Sin(angle)) * radius);
        foreach (double distSqr in player.RealmEnemyList.Select(enemy => enemy.DistSqr(spawnPoint)))
        {
            // prevents enemies overspawning in small places, and spawning in walls or other obstructions
            if (distSqr < Program.AppSettings.GameServer.Enemy.Spread * Program.AppSettings.GameServer.Enemy.Spread)
            {
                return null;
            }
        }

        if (!this.RegionUnblocked(spawnPoint.X, spawnPoint.Y, TileOccupency.NoWalk | TileOccupency.OccupyHalf))
        {
            return null;
        }

        foreach (double distSqr in Players.Where(p => p.Id != player.Id).Select(p => p.DistSqr(spawnPoint)))
        {
            // prevents enemies from spawning on players
            if (distSqr < Program.AppSettings.GameServer.Enemy.MinSpawnDistance * Program.AppSettings.GameServer.Enemy.MinSpawnDistance)
            {
                return null;
            }
        }

        var enemyName = string.Empty;
        var tile = GetTile(new Vector2Int((int)spawnPoint.X, (int)spawnPoint.Y));
        if (tile is not RealmWmapTile realmTile)
        {
            throw new InvalidCastException(tile.ToString());
        }

        if (realmTile.Structure != StructureType.None)
        {
            enemyName = realmTile.Structure switch
            {
                StructureType.PirateShip => "Pirate",
                _ => enemyName,
            };
        }
        else if (realmTile.Biome.SpawnEnemies())
        {
            // 15% chance to spawn an enemy from a neighboring biome
            if (spawnRandom.Next(0, 100) < 15)
            {
                var neighbors = realmTile.Biome.GetNeigbors();
                var index = spawnRandom.Next(0, neighbors.Length);
                enemyName = neighbors[index].GetEnemy(spawnRandom);
            }
            else
            {
                enemyName = realmTile.Biome.GetEnemy(spawnRandom);
            }
        }

        if (enemyName.IsNullOrWhiteSpace() || Entity.Resolve(enemyName, this) is not Enemy ret)
        {
            return null;
        }

        ret.RealmOwner = player;
        ret.Move(spawnPoint);
        return ret;
    }

    internal override bool TryGetBounds(out Rectangle bounds)
    {
        bounds = Rectangle.Empty;
        return false;
    }

    private void GenerateMap()
    {
        Logger.LogDebug("Preparing Realm Map");
        Random foliageMapRand = new(generationSeed);
        BeachFoliage.Default.PopulateMap(foliageMapRand);
        ForestFoliage.Default.PopulateMap(foliageMapRand);
        SavannahFoliage.Default.PopulateMap(foliageMapRand);
        JungleFoliage.Default.PopulateMap(foliageMapRand);
        OceanFoliage.Default.PopulateMap(foliageMapRand);
        TropicalFoliage.Default.PopulateMap(foliageMapRand);
        TundraFoliage.Default.PopulateMap(foliageMapRand);
        MountainFoliage.Default.PopulateMap(foliageMapRand);
        MountainOreFoliage.Default.PopulateMap(foliageMapRand);

        Logger.LogDebug("Generated Foliage Maps");
        Random structureRand = new(generationSeed);
        PirateShip.PopulateMap(structureRand);
        OldBlacksmith.PopulateMap(structureRand);
        CoralReef.PopulateMap(structureRand);
        Christmas.PopulateMap(structureRand);
        Logger.LogDebug("Generated Structure Maps");

        // Find SpawnPoints around the map
        Random spawnRand = new(generationSeed);
        while (Map.PlayerSpawnPoints.Count < PlayerSpawnPositions)
        {
            Vector2Int possibleSpawnPoint = new(spawnRand.Next(5000, 10001), spawnRand.Next(5000, 10001));
            if (GetRegion(possibleSpawnPoint) == TileRegion.Spawn)
            {
                Map.PlayerSpawnPoints.Add(possibleSpawnPoint);
            }
        }

        Logger.LogDebug("Found Player Spawns");
        Logger.LogDebug("Realm Map Prepared");
    }

    private void PreRealmGeneration()
    {
        Map = new Wmap();
        Map.Width = Map.Height = 0;
        Objects.Clear();
        Enemies.Clear();
        Players.Clear();
    }
}